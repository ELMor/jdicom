// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   jdtVersion.java

package version;


public class jdtVersion
{

    public static int versionMajor = 1;
    public static int versionMinor = 7;
    public static int versionPatch = 35;
    public static String type = "beta";

    public jdtVersion()
    {
    }

    public static String getVersion()
    {
        return "jdt v" + versionMajor + "." + versionMinor + "." + versionPatch;
    }

}
