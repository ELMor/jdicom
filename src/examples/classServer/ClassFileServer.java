// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClassFileServer.java

package examples.classServer;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package examples.classServer:
//            ClassServer

public class ClassFileServer extends ClassServer
{

    private String classpath;
    private static int DefaultServerPort = 2001;

    public ClassFileServer(int i, String s)
        throws IOException
    {
        super(i);
        classpath = s;
    }

    public byte[] getBytes(String s)
        throws IOException, ClassNotFoundException
    {
        System.out.println("reading: " + s);
        File file = new File(classpath + File.separator + s.replace('.', File.separatorChar) + ".class");
        int i = (int)file.length();
        if(i == 0)
        {
            throw new IOException("File length is zero: " + s);
        } else
        {
            FileInputStream fileinputstream = new FileInputStream(file);
            DataInputStream datainputstream = new DataInputStream(((java.io.InputStream) (fileinputstream)));
            byte abyte0[] = new byte[i];
            datainputstream.readFully(abyte0);
            return abyte0;
        }
    }

    public static void main(String args[])
    {
        int i = DefaultServerPort;
        String s = "";
        if(args.length >= 1)
            i = Integer.parseInt(args[0]);
        if(args.length >= 2)
            s = args[1];
        try
        {
            new ClassFileServer(i, s);
        }
        catch(IOException ioexception)
        {
            System.out.println("Unable to start ClassServer: " + ((Throwable) (ioexception)).getMessage());
            ((Throwable) (ioexception)).printStackTrace();
        }
    }

}
