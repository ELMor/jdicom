// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TestJpeg14.java

package test.com.tiani.dicom.codec;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.tiani.dicom.legacy.TianiInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

public class TestJpeg14
{

    private int n;
    private int minSize;
    private int maxSize;
    private long sumSize;
    private float minRate;
    private float maxRate;
    private float sumRate;
    private long minDt;
    private long maxDt;
    private long sumDt;

    public TestJpeg14()
    {
        n = 0;
        minSize = 0x7fffffff;
        maxSize = 0;
        sumSize = 0L;
        minRate = 3.402823E+038F;
        maxRate = 0.0F;
        sumRate = 0.0F;
        minDt = 0x7fffffffffffffffL;
        maxDt = 0L;
        sumDt = 0L;
    }

    private void addLog(int i, float f, long l)
    {
        n++;
        minSize = Math.min(i, minSize);
        maxSize = Math.max(i, maxSize);
        sumSize += i;
        minRate = Math.min(f, minRate);
        maxRate = Math.max(f, maxRate);
        sumRate += f;
        minDt = Math.min(l, minDt);
        maxDt = Math.max(l, maxDt);
        sumDt += l;
    }

    private void writeSummary()
    {
        System.out.println("N=" + n + ", size ~" + (sumSize / (long)n >> 10) + "kB [" + (minSize >> 10) + "-" + (maxSize >> 10) + "]");
        System.out.println("rate ~" + sumRate / (float)n + " [" + minRate + "-" + maxRate + "]");
        System.out.println("time ~" + sumDt / (long)n + "ms [" + minDt + "-" + maxDt + "]");
    }

    public static void main(String args[])
    {
        if(args.length != 2)
        {
            System.out.println("Usage: TestJpeg14 <inFile> <outFile>");
            System.out.println("or     TestJpeg14 <inDir> <outDir>");
            return;
        }
        TestJpeg14 testjpeg14 = new TestJpeg14();
        try
        {
            File file = new File(args[0]);
            File file1 = new File(args[1]);
            if(!file.isDirectory())
            {
                testjpeg14.transform(file, file1);
                return;
            }
            if(!file1.isDirectory())
                throw new IOException("Error accesing directory " + file1);
            String args1[] = file.list();
            for(int i = 0; i < args1.length; i++)
                try
                {
                    testjpeg14.transform(new File(file, args1[i]), new File(file1, args1[i]));
                }
                catch(Exception exception1)
                {
                    ((Throwable) (exception1)).printStackTrace(System.out);
                }

            testjpeg14.writeSummary();
        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace(System.out);
        }
    }

    private void transform(File file, File file1)
        throws IOException, DicomException, UnknownUIDException
    {
        DicomObject dicomobject = new DicomObject();
        TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file))));
        try
        {
            tianiinputstream.read(dicomobject, true);
        }
        finally
        {
            ((FilterInputStream) (tianiinputstream)).close();
        }
        DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
        Compression compression = new Compression(dicomobject);
        int i = dicomobject1 == null ? 8193 : UID.getUIDEntry(dicomobject1.getS(31)).getConstant();
        int j = getPixelSize(dicomobject);
        long l = System.currentTimeMillis();
        int k;
        float f;
        long l1;
        switch(i)
        {
        case 8193: 
        case 8194: 
        case 8195: 
            compression.compress(8197);
            try
            {
                dicomobject1.set(31, ((Object) (UID.getUIDEntry(8197).getValue())));
            }
            catch(IllegalValueException illegalvalueexception) { }
            k = j;
            f = (float)j / (float)getPixelSize(dicomobject);
            l1 = System.currentTimeMillis() - l;
            System.out.println("compress " + file + " [" + f + " : 1] takes " + l1 + "ms");
            break;

        default:
            compression.decompress();
            k = getPixelSize(dicomobject);
            f = (float)k / (float)j;
            l1 = System.currentTimeMillis() - l;
            System.out.println("decompress " + file + " [1 : " + f + "] takes " + l1 + "ms");
            break;
        }
        addLog(k, f, l1);
        FileOutputStream fileoutputstream = new FileOutputStream(file1);
        try
        {
            dicomobject.write(((java.io.OutputStream) (fileoutputstream)), true);
        }
        finally
        {
            fileoutputstream.close();
        }
    }

    private static int getPixelSize(DicomObject dicomobject)
    {
        int i = 0;
        for(int j = dicomobject.getSize(1184); j-- > 0;)
            i += ((byte[])dicomobject.get(1184, j)).length;

        return i <= 0 ? 1 : i;
    }
}
