// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CreateTestImage.java

package test.com.tiani.dicom.codec;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.codec.Compression;
import com.tiani.dicom.legacy.TianiInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;

public class CreateTestImage
{

    private final boolean compress;
    private final boolean inverse;
    private final int type;
    private final int block;
    private final int white;
    private final int bitsStored;
    private final int bitsAllocated;
    private final Random random;
    private static final byte _FMI_VERSION[] = {
        0, 1
    };

    public CreateTestImage(String as[])
    {
        if(as.length < 4)
            throw new IllegalArgumentException("missing arguments");
        if(as.length > 4)
            throw new IllegalArgumentException("to many parameters");
        compress = as[0].startsWith("c");
        float f = Float.parseFloat(compress ? as[0].substring(1) : as[0]);
        if(inverse = f < 0.0F)
            f = -f;
        type = (int)f;
        block = (int)((f - (float)type) * 10F);
        bitsStored = Integer.parseInt(as[1]);
        if(bitsStored < 8 || bitsStored > 16)
        {
            throw new IllegalArgumentException("stored bits out of range [8,16] - " + bitsStored);
        } else
        {
            bitsAllocated = bitsStored <= 8 ? 8 : 16;
            white = 65535 >> 16 - bitsStored;
            random = new Random(type);
            return;
        }
    }

    public static void main(String args[])
    {
        try
        {
            CreateTestImage createtestimage = new CreateTestImage(args);
            DicomObject dicomobject = new DicomObject();
            TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(args[2]))));
            try
            {
                dicomobject.read(((java.io.InputStream) (tianiinputstream)));
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            createtestimage.process(dicomobject);
            FileOutputStream fileoutputstream = new FileOutputStream(args[3]);
            try
            {
                dicomobject.write(((OutputStream) (fileoutputstream)), true);
            }
            finally
            {
                ((OutputStream) (fileoutputstream)).close();
            }
        }
        catch(IllegalArgumentException illegalargumentexception)
        {
            System.out.println(((Object) (illegalargumentexception)));
            System.out.println();
            System.out.println("Usage: CreateTestImage [c][-]N[.<block>] <bitsStored> <attribFile> <outFile> ");
            System.out.println("                    c - compress jpeg lossless");
            System.out.println("                    0 - black image");
            System.out.println("                    1 - horizontal black to white");
            System.out.println("                   -1 - horizontal white to black");
            System.out.println("                    2 - vertical black to white");
            System.out.println("                   -2 - vertical white to black");
            System.out.println("                    3 - diagonal black to white");
            System.out.println("                   -3 - diagonal white to black");
            System.out.println("                    4 - white image");
            System.out.println("                    5 - horizontal black - white - black - ..");
            System.out.println("                   -5 - horizontal white - black - white - ..");
            System.out.println("                    6 - vertical black - white - black - ..");
            System.out.println("                   -6 - vertical white - black - white - ..");
            System.out.println("                    7 - diagonal black - white - black - ..");
            System.out.println("                   -7 - diagonal white - black - white - ..");
            System.out.println("                    N - random noise");
            System.out.println("                   -N - random black/white");
            System.out.println("           <block> .0 - continue gradation");
            System.out.println("           <block> .1 - 2 pixel block");
            System.out.println("           <block> .2 - 4 pixel block");
            System.out.println("           <block> .3 - 8 pixel block");
            System.out.println("           <block> .4 - 16 pixel block");
            System.out.println("           <block> .5 - 32 pixel block");
            System.out.println("           <block> .6 - 64 pixel block");
        }
        catch(IOException ioexception)
        {
            System.out.println(((Object) (ioexception)));
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(System.out);
        }
    }

    private void process(DicomObject dicomobject)
        throws DicomException, IllegalValueException, IOException
    {
        dicomobject.set(466, ((Object) (new Integer(256))));
        dicomobject.set(467, ((Object) (new Integer(256))));
        dicomobject.set(475, ((Object) (new Integer(bitsAllocated))));
        dicomobject.set(476, ((Object) (new Integer(bitsStored))));
        dicomobject.set(477, ((Object) (new Integer(bitsStored - 1))));
        dicomobject.set(478, ((Object) (new Integer(0))));
        dicomobject.set(462, "MONOCHROME2");
        dicomobject.deleteItem(463);
        dicomobject.set(1184, ((Object) (bitsAllocated != 8 ? ((Object) (create16BitPixelData())) : ((Object) (create8BitPixelData())))));
        char c = '\u2002';
        if(compress)
        {
            Compression compression = new Compression(dicomobject);
            compression.compress(((int) (c = '\u2005')));
        }
        dicomobject.setFileMetaInformation(createFileMetaInformation(dicomobject, UID.getUIDEntry(((int) (c))).getValue()));
    }

    private int block(int i)
    {
        return ((inverse ? 255 - i : i) >> block) << block;
    }

    private int bOrW(int i)
    {
        return ((inverse ? 255 - i : i) >> block & 1) != 0 ? white : 0;
    }

    private int random()
    {
        return inverse ? random.nextBoolean() ? 0 : white : random.nextInt(white);
    }

    private byte[] create8BitPixelData()
    {
        byte abyte0[] = new byte[0x10000];
        switch(type)
        {
        case 0: // '\0'
            break;

        case 1: // '\001'
            for(int i = 0; i < 256; i++)
            {
                for(int j = 0; j < 256; j++)
                    abyte0[256 * i + j] = (byte)block(j);

            }

            break;

        case 2: // '\002'
            for(int k = 0; k < 256; k++)
            {
                for(int l = 0; l < 256; l++)
                    abyte0[256 * k + l] = (byte)block(k);

            }

            break;

        case 3: // '\003'
            for(int i1 = 0; i1 < 256; i1++)
            {
                for(int j1 = 0; j1 < 256; j1++)
                    abyte0[256 * i1 + j1] = (byte)(block(i1) + block(j1) >> 1);

            }

            break;

        case 4: // '\004'
            Arrays.fill(abyte0, (byte)-1);
            break;

        case 5: // '\005'
            for(int k1 = 0; k1 < 256; k1++)
            {
                for(int l1 = 0; l1 < 256; l1++)
                    abyte0[256 * k1 + l1] = (byte)bOrW(l1);

            }

            break;

        case 6: // '\006'
            for(int i2 = 0; i2 < 256; i2++)
            {
                for(int j2 = 0; j2 < 256; j2++)
                    abyte0[256 * i2 + j2] = (byte)bOrW(i2);

            }

            break;

        case 7: // '\007'
            for(int k2 = 0; k2 < 256; k2++)
            {
                for(int l2 = 0; l2 < 256; l2++)
                    abyte0[256 * k2 + l2] = (byte)bOrW(k2 ^ l2);

            }

            break;

        default:
            for(int i3 = 0; i3 < 256; i3++)
            {
                for(int j3 = 0; j3 < 256; j3++)
                    abyte0[256 * i3 + j3] = (byte)random();

            }

            break;
        }
        return abyte0;
    }

    private byte[] create16BitPixelData()
    {
        byte abyte0[] = new byte[0x20000];
        int k3 = bitsStored - 8;
        switch(type)
        {
        case 0: // '\0'
            break;

        case 1: // '\001'
            for(int l3 = 0; l3 < 256; l3++)
            {
                for(int i4 = 0; i4 < 256; i4++)
                {
                    int i = (256 * l3 + i4) * 2;
                    int l1 = block(i4) << k3;
                    abyte0[i] = (byte)l1;
                    abyte0[i + 1] = (byte)(l1 >> 8);
                }

            }

            break;

        case 2: // '\002'
            for(int j4 = 0; j4 < 256; j4++)
            {
                for(int k4 = 0; k4 < 256; k4++)
                {
                    int j = (256 * j4 + k4) * 2;
                    int i2 = block(j4) << k3;
                    abyte0[j] = (byte)i2;
                    abyte0[j + 1] = (byte)(i2 >> 8);
                }

            }

            break;

        case 3: // '\003'
            for(int l4 = 0; l4 < 256; l4++)
            {
                for(int i5 = 0; i5 < 256; i5++)
                {
                    int k = (256 * l4 + i5) * 2;
                    int j2 = block(l4) + block(i5) << k3 - 1;
                    abyte0[k] = (byte)j2;
                    abyte0[k + 1] = (byte)(j2 >> 8);
                }

            }

            break;

        case 4: // '\004'
            Arrays.fill(abyte0, (byte)-1);
            break;

        case 5: // '\005'
            for(int j5 = 0; j5 < 256; j5++)
            {
                for(int k5 = 0; k5 < 256; k5++)
                {
                    int l = (256 * j5 + k5) * 2;
                    int k2 = bOrW(k5);
                    abyte0[l] = (byte)k2;
                    abyte0[l + 1] = (byte)(k2 >> 8);
                }

            }

            break;

        case 6: // '\006'
            for(int l5 = 0; l5 < 256; l5++)
            {
                for(int i6 = 0; i6 < 256; i6++)
                {
                    int i1 = (256 * l5 + i6) * 2;
                    int l2 = bOrW(l5);
                    abyte0[i1] = (byte)l2;
                    abyte0[i1 + 1] = (byte)(l2 >> 8);
                }

            }

            break;

        case 7: // '\007'
            for(int j6 = 0; j6 < 256; j6++)
            {
                for(int k6 = 0; k6 < 256; k6++)
                {
                    int j1 = (256 * j6 + k6) * 2;
                    int i3 = bOrW(j6 ^ k6);
                    abyte0[j1] = (byte)i3;
                    abyte0[j1 + 1] = (byte)(i3 >> 8);
                }

            }

            break;

        default:
            for(int l6 = 0; l6 < 256; l6++)
            {
                for(int i7 = 0; i7 < 256; i7++)
                {
                    int k1 = (256 * l6 + i7) * 2;
                    int j3 = random();
                    abyte0[k1] = (byte)j3;
                    abyte0[k1 + 1] = (byte)(j3 >> 8);
                }

            }

            break;
        }
        return abyte0;
    }

    private static DicomObject createFileMetaInformation(DicomObject dicomobject, String s)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        dicomobject1.set(28, ((Object) (_FMI_VERSION)));
        dicomobject1.set(29, dicomobject.get(62));
        dicomobject1.set(30, dicomobject.get(63));
        dicomobject1.set(31, ((Object) (s)));
        dicomobject1.set(32, "1.2.40.0.13.0.0.113");
        dicomobject1.set(33, "TIANI_JDICOM_113");
        return dicomobject1;
    }

}
