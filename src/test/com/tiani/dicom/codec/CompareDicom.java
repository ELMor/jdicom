// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CompareDicom.java

package test.com.tiani.dicom.codec;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

public final class CompareDicom
{

    public CompareDicom()
    {
    }

    public static void main(String args[])
    {
        if(args.length != 2)
        {
            System.out.println("Usage: CompareDicom <File1> <File2>");
            System.out.println("or     CompareDicom <Dir1>  <Dir2>");
            return;
        }
        try
        {
            File file = new File(args[0]);
            boolean flag = file.isDirectory();
            File file1 = new File(args[1]);
            if(!flag)
            {
                compare(file, file1);
                return;
            }
            if(!file1.isDirectory())
                throw new IOException("Error accessing directory " + file1);
            String args1[] = file.list();
            for(int i = 0; i < args1.length; i++)
                try
                {
                    compare(new File(file, args1[i]), new File(file1, args1[i]));
                }
                catch(Exception exception1)
                {
                    System.out.println(((Object) (exception1)));
                }

        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace();
        }
    }

    private static void compare(File file, File file1)
        throws IOException, DicomException
    {
        DicomObject dicomobject = new DicomObject();
        DicomObject dicomobject1 = new DicomObject();
        TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file))));
        TianiInputStream tianiinputstream1 = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file1))));
        try
        {
            tianiinputstream.read(dicomobject, true);
        }
        finally
        {
            ((FilterInputStream) (tianiinputstream)).close();
        }
        try
        {
            tianiinputstream1.read(dicomobject1, true);
        }
        finally
        {
            ((FilterInputStream) (tianiinputstream1)).close();
        }
        byte abyte0[] = (byte[])dicomobject.get(1184);
        byte abyte1[] = (byte[])dicomobject1.get(1184);
        int i = dicomobject.getI(475);
        int j = dicomobject.getI(476);
        int k = dicomobject.getI(477);
        int j1 = 0;
        int k1 = (1 << j) - 1;
        k1 <<= (k + 1) - j;
        byte byte0 = (byte)(k1 & 0xff);
        byte byte1 = (byte)(k1 >>> 8 & 0xff);
        if(i == 8)
        {
            for(int l = 0; l < abyte0.length; l++)
                if((abyte0[l] & byte0) != (abyte1[l] & byte0))
                {
                    System.out.println("*Error index:" + l + " " + abyte0[l] + "-" + abyte1[l]);
                    j1++;
                }

        } else
        if(i == 16)
        {
            for(int i1 = 0; i1 < abyte0.length / 2; i1 += 2)
                if((abyte0[i1] & byte0) != (abyte1[i1] & byte0) || (abyte0[i1 + 1] & byte1) != (abyte1[i1 + 1] & byte1))
                {
                    System.out.println("*Error index:" + i1 + " " + abyte0[i1] + "-" + abyte1[i1]);
                    j1++;
                }

        }
        System.out.println("Error#: " + j1 + " von:" + abyte0.length);
    }
}
