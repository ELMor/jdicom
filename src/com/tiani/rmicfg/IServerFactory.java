// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IServerFactory.java

package com.tiani.rmicfg;

import java.rmi.Remote;
import java.rmi.RemoteException;

// Referenced classes of package com.tiani.rmicfg:
//            IServer

public interface IServerFactory
    extends Remote
{

    public abstract IServer createServer(String s, String s1)
        throws RemoteException;

    public abstract IServer getServer(String s)
        throws RemoteException;

    public abstract IServer removeServer(String s)
        throws RemoteException;

    public abstract String[] listServerNames()
        throws RemoteException;

    public abstract String[] listClassNames()
        throws RemoteException;
}
