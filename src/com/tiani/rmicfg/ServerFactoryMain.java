// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerFactoryMain.java

package com.tiani.rmicfg;

import com.tiani.dicom.util.CommandLineParser;
import examples.classServer.ClassFileServer;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Hashtable;

// Referenced classes of package com.tiani.rmicfg:
//            IServer, ServerFactoryImpl

public class ServerFactoryMain
{

    private String name;
    private String httpHost;
    private int httpPort;
    private String classpath;
    private final String classnames[];
    private static final String USAGE = "Usage: ServerFactoryMain [-codebase <rmiCodebase>] [-http <host>[:<port>]] [-name <name>] -server <srvClass1> [<srvClass2> ..]\nwith\n-codebase              overwrites system property java.rmi.server.codebase with specified\n  <rmiCodebase>        URL or filepath\n-http <host>[:<port>]  start http server listen at specified port (default: 2001)\n-name <name>           name used for binding with the (local) rmi registry (default: \"ServerFactory\")\n-server <classname#>   server class name";
    private static final String OPTIONS[] = {
        "-codebase", "-http", "-name", "-server"
    };
    private static final int PARAMNUM[] = {
        1, 1, 1
    };

    public static void main(String args[])
    {
        try
        {
            LocateRegistry.createRegistry(1099);
            System.out.println("Start rmiregistry listening on port 1099");
        }
        catch(RemoteException remoteexception) { }
        try
        {
            for(int i = 0; i < args.length; i++)
                System.out.println("args[" + i + "]=" + args[i]);

            (new ServerFactoryMain(CommandLineParser.parse(args, OPTIONS, PARAMNUM))).execute();
        }
        catch(IllegalArgumentException illegalargumentexception)
        {
            System.out.println(((Object) (illegalargumentexception)));
            System.out.println("Usage: ServerFactoryMain [-codebase <rmiCodebase>] [-http <host>[:<port>]] [-name <name>] -server <srvClass1> [<srvClass2> ..]\nwith\n-codebase              overwrites system property java.rmi.server.codebase with specified\n  <rmiCodebase>        URL or filepath\n-http <host>[:<port>]  start http server listen at specified port (default: 2001)\n-name <name>           name used for binding with the (local) rmi registry (default: \"ServerFactory\")\n-server <classname#>   server class name");
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(System.out);
        }
    }

    private ServerFactoryMain(Hashtable hashtable)
        throws IllegalArgumentException
    {
        name = "ServerFactory";
        httpHost = null;
        httpPort = 2001;
        classpath = "";
        if(hashtable.containsKey("-name"))
            name = CommandLineParser.get(hashtable, "-name", 0);
        classnames = (String[])hashtable.get("-server");
        if(hashtable.containsKey("-http"))
        {
            httpHost = CommandLineParser.get(hashtable, "-http", 0);
            int i = httpHost.indexOf(':');
            if(i != -1)
            {
                httpPort = Integer.parseInt(httpHost.substring(i + 1));
                httpHost = httpHost.substring(0, i);
            }
        }
        if(classnames == null || classnames.length == 0)
            throw new IllegalArgumentException("Missing server class name(s)");
        if(hashtable.containsKey("-codebase"))
        {
            String s = CommandLineParser.get(hashtable, "-codebase", 0);
            try
            {
                if(s.indexOf(':') > 2)
                    new URL(s);
                else
                if(httpHost == null)
                {
                    s = (new File(s)).toURL().toString();
                } else
                {
                    classpath = s;
                    s = "http://" + httpHost + ':' + httpPort + '/';
                }
                System.setProperty("java.rmi.server.codebase", s);
                System.out.println("Set java.rmi.server.codebase=" + s);
            }
            catch(MalformedURLException malformedurlexception)
            {
                throw new IllegalArgumentException("Malformed codebase URL - " + s);
            }
        }
    }

    private void execute()
        throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, MalformedURLException, RemoteException
    {
        if(httpHost != null)
        {
            new ClassFileServer(httpPort, classpath);
            System.out.println("Start http server listening at port " + httpPort);
        }
        for(int i = 0; i < classnames.length; i++)
        {
            IServer iserver = (IServer)Class.forName(classnames[i]).newInstance();
        }

        ServerFactoryImpl serverfactoryimpl = new ServerFactoryImpl(classnames);
        Naming.rebind(name, ((java.rmi.Remote) (serverfactoryimpl)));
        System.out.println("Waiting for invocations from clients...");
    }

}
