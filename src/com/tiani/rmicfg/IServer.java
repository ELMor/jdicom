// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IServer.java

package com.tiani.rmicfg;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;

public interface IServer
    extends Remote
{

    public abstract String getType()
        throws RemoteException;

    public abstract void setProperties(Properties properties)
        throws RemoteException;

    public abstract Properties getProperties()
        throws RemoteException;

    public abstract String[] getPropertyNames()
        throws RemoteException;

    public abstract void start()
        throws RemoteException;

    public abstract void start(Properties properties)
        throws RemoteException;

    public abstract void stop(boolean flag, boolean flag1)
        throws RemoteException;

    public abstract boolean isRunning()
        throws RemoteException;
}
