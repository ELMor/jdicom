// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerFactoryImpl.java

package com.tiani.rmicfg;

import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package com.tiani.rmicfg:
//            ServerImpl, IServer, IServerFactory

public class ServerFactoryImpl extends UnicastRemoteObject
    implements IServerFactory, SCMEventListener
{

    private final Hashtable _registry = new Hashtable();
    private final String _classNames[];

    public ServerFactoryImpl(String as[])
        throws RemoteException
    {
        _classNames = as;
        SCMEventManager scmeventmanager = SCMEventManager.getInstance();
        scmeventmanager.addSCMEventListener(((SCMEventListener) (this)));
    }

    public IServer createServer(String s, String s1)
        throws RemoteException
    {
        try
        {
            ServerImpl serverimpl = new ServerImpl((IServer)Class.forName(s).newInstance());
            _registry.put(((Object) (s1)), ((Object) (serverimpl)));
            return ((IServer) (serverimpl));
        }
        catch(Exception exception)
        {
            System.err.println(((Object) (exception)));
            throw new RemoteException("", ((Throwable) (exception)));
        }
    }

    public String[] listServerNames()
        throws RemoteException
    {
        String as[] = new String[_registry.size()];
        Enumeration enumeration = _registry.keys();
        for(int i = 0; i < as.length; i++)
            as[i] = (String)enumeration.nextElement();

        return as;
    }

    public IServer getServer(String s)
        throws RemoteException
    {
        return (IServer)_registry.get(((Object) (s)));
    }

    public IServer removeServer(String s)
        throws RemoteException
    {
        return (IServer)_registry.remove(((Object) (s)));
    }

    public String[] listClassNames()
        throws RemoteException
    {
        return _classNames;
    }

    public void handleSCMEvent(SCMEvent scmevent)
    {
        if(scmevent.getID() != 1);
    }
}
