// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CStoreSCP.java

package com.tiani.dicom.storetoprint;

import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            PrintSCURelay

final class CStoreSCP extends DefCStoreSCP
{

    private PrintSCURelay _relay;

    public CStoreSCP(PrintSCURelay printscurelay)
    {
        _relay = printscurelay;
    }

    protected int store(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        return _relay.store(dimseexchange, s, s1, dicommessage, dicommessage1);
    }
}
