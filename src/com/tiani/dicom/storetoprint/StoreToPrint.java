// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StoreToPrint.java

package com.tiani.dicom.storetoprint;

import com.archimed.dicom.Debug;
import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.rmicfg.IServer;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.storetoprint:
//            Param, PrintSCURelay, CEchoSCP, CStoreSCP

public class StoreToPrint
    implements IServer, SCMEventListener
{

    private static final GregorianCalendar _calender = new GregorianCalendar();
    private Param _param;
    private Acceptor _acceptor;
    private Thread _thread;
    private CEchoSCP _cechoSCP;
    private CStoreSCP _cstoreSCP;
    private PrintSCURelay _printSCURelay;
    private DimseRqManager _rqManager;
    private DefAcceptorListener _acceptorListener;

    public StoreToPrint()
    {
        this(((Properties) (null)));
    }

    public StoreToPrint(Properties properties)
    {
        _acceptor = null;
        _thread = null;
        SCMEventManager scmeventmanager = SCMEventManager.getInstance();
        scmeventmanager.addSCMEventListener(((SCMEventListener) (this)));
        _param = new Param(properties == null ? loadDefProperties() : properties);
        Debug.DEBUG = _param.getVerbose();
        _printSCURelay = new PrintSCURelay(_param);
        _cechoSCP = new CEchoSCP(_printSCURelay);
        _cstoreSCP = new CStoreSCP(_printSCURelay);
        _rqManager = createRqManager(((IDimseRqListener) (_cechoSCP)), ((IDimseRqListener) (_cstoreSCP)));
    }

    public String getType()
        throws RemoteException
    {
        return "StoreToPrint";
    }

    public void setProperties(Properties properties)
        throws RemoteException
    {
        _param = new Param(properties);
        Debug.DEBUG = _param.getVerbose();
        synchronized(this)
        {
            if(_thread != null)
            {
                _acceptor.setARTIM(_param.getAssocTimeout(), _param.getReleaseTimeout());
                _acceptor.setStartThread(_param.isMultiThreadTCP());
                _acceptorListener.setARTIM(_param.getReleaseTimeout());
                _acceptorListener.setStartThread(_param.isMultiThreadAssoc());
            }
        }
        _printSCURelay.setParams(_param);
    }

    public String[] getPropertyNames()
        throws RemoteException
    {
        return Param.KEYS;
    }

    public Properties getProperties()
        throws RemoteException
    {
        return _param.getProperties();
    }

    public void start()
        throws RemoteException
    {
        if(_thread != null)
            throw new IllegalStateException("server is running");
        try
        {
            Debug.out.println("Start StoreToPrint v1.4.5 at " + ((Calendar) (_calender)).getTime() + " with " + _param);
            _acceptorListener = new DefAcceptorListener(_param.isMultiThreadAssoc(), _rqManager, false, false);
            _acceptor = new Acceptor(new ServerSocket(_param.getPort()), _param.isMultiThreadTCP(), ((com.tiani.dicom.framework.IAcceptancePolicy) (_printSCURelay)), ((com.tiani.dicom.framework.IAcceptorListener) (_acceptorListener)));
            _acceptor.addAssociationListener(((com.tiani.dicom.framework.IAssociationListener) (_printSCURelay)));
            _thread = new Thread(((Runnable) (_acceptor)));
            _thread.start();
            Debug.out.println("Waiting for invocations from clients...");
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    public void start(Properties properties)
        throws RemoteException
    {
        setProperties(properties);
        start();
    }

    public void stop(boolean flag, boolean flag1)
        throws RemoteException
    {
        try
        {
            if(_thread == null)
                throw new IllegalStateException("server is not running");
            _acceptor.stop(flag);
            if(flag1)
                _thread.join();
            synchronized(this)
            {
                _thread = null;
                _acceptor = null;
                _acceptorListener = null;
            }
            Debug.out.println("Stopped StoreToPrint v1.4 at " + ((Calendar) (_calender)).getTime());
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(Debug.out);
            throw new RemoteException("", throwable);
        }
    }

    public boolean isRunning()
        throws RemoteException
    {
        return _thread != null;
    }

    public void handleSCMEvent(SCMEvent scmevent)
    {
        if(scmevent.getID() == 1)
            try
            {
                if(isRunning())
                    stop(true, false);
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(System.out);
            }
    }

    public static void main(String args[])
    {
        try
        {
            String s = args.length <= 0 ? "StoreToPrint.properties" : args[0];
            (new StoreToPrint(loadProperties(s))).start();
        }
        catch(Throwable throwable)
        {
            System.out.println(((Object) (throwable)));
        }
    }

    static Properties loadProperties(String s)
    {
        try
        {
            Properties properties = new Properties();
            File file = new File(s);
            System.out.println("load properties from " + file.getAbsolutePath());
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                properties.load(((InputStream) (fileinputstream)));
                Properties properties1 = properties;
                return properties1;
            }
            finally
            {
                ((InputStream) (fileinputstream)).close();
            }
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return null;
    }

    public static void setLog(PrintStream printstream)
    {
        Debug.out = printstream;
    }

    private static Properties loadDefProperties()
    {
        Properties properties = new Properties();
        InputStream inputstream = (com.tiani.dicom.storetoprint.StoreToPrint.class).getResourceAsStream("StoreToPrint.properties");
        try
        {
            properties.load(inputstream);
            inputstream.close();
            Debug.out.println("Load default properties");
        }
        catch(Exception exception)
        {
            throw new RuntimeException("StoreToPrint.class.getResourceAsStream(\"StoreToPrint.properties\") failed");
        }
        return properties;
    }

    private static DimseRqManager createRqManager(IDimseRqListener idimserqlistener, IDimseRqListener idimserqlistener1)
    {
        DimseRqManager dimserqmanager = new DimseRqManager();
        dimserqmanager.regCEchoScp("1.2.840.10008.1.1", idimserqlistener);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.2", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.4", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.5", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.7", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.20", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.128", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.2", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.1", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.2", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.3", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.4", idimserqlistener1);
        dimserqmanager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.1", idimserqlistener1);
        return dimserqmanager;
    }

}
