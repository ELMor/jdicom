// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StoreToPrintException.java

package com.tiani.dicom.storetoprint;


final class StoreToPrintException extends Exception
{

    static final int NO_ASSOCIATION = 42752;
    static final int NO_GRAYSCALE_PRINTMGT = 42753;
    static final int NO_COLOR_PRINTMGT = 42754;
    static final int PRINTER_ERROR = 42755;
    static final int PRINTER_WARNING = 42756;
    static final int PRINTER_FAILURE = 42757;
    static final int FILMSESSION_ERROR = 42758;
    static final int FILMBOX_ERROR = 42759;
    static final int IMAGEBOX_ERROR = 42760;
    static final int CATCH_EXCEPTION = 43007;
    private final int _status;

    public StoreToPrintException(String s, int i)
    {
        super(s);
        _status = i;
    }

    public int getStatus()
    {
        return _status;
    }
}
