// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageCmtTableModel.java

package com.tiani.dicom.client;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.StatusEntry;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

class StorageCmtTableModel extends AbstractTableModel
{
    private class StorageListener
        implements IDimseRspListener
    {

        private RowData rowData;

        public void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
            throws IOException, DicomException, IllegalValueException, UnknownUIDException
        {
            if(Status.getStatusEntry(j).getType() != 5)
                SwingUtilities.invokeLater(new Runnable() {

                    public void run()
                    {
                        insert();
                    }

                });
        }

        private void insert()
        {
            int i = data.indexOf(((Object) (rowData)));
            if(i == -1)
            {
                int j = data.size();
                data.addElement(((Object) (rowData)));
                fireTableRowsInserted(j, j);
            }
        }


        public StorageListener(RowData rowdata)
        {
            rowData = rowdata;
        }
    }

    private class RowData
    {

        final String uid;
        final String attribs[] = new String[StorageCmtTableModel.HEADER.length];
        final DicomObject sopReference = new DicomObject();

        public int hashCode()
        {
            return uid.hashCode();
        }

        public boolean equals(Object obj)
        {
            return (obj instanceof RowData) && uid.equals(((Object) (((RowData)obj).uid)));
        }

        void setFailed(DicomObject dicomobject)
        {
            attribs[0] = "Unrecognized failure reason";
            try
            {
                switch(dicomobject.getI(119))
                {
                case 272: 
                    attribs[0] = "Processing failure";
                    break;

                case 274: 
                    attribs[0] = "No such object instance";
                    break;

                case 531: 
                    attribs[0] = "Resource limitation";
                    break;

                case 290: 
                    attribs[0] = "Referenced SOP Class not supported";
                    break;

                case 281: 
                    attribs[0] = "Class / Instance conflict";
                    break;

                case 305: 
                    attribs[0] = "Duplicate transaction UID";
                    break;
                }
            }
            catch(DicomException dicomexception)
            {
                log.println(((Throwable) (dicomexception)).getMessage());
            }
            attribs[9] = "";
            attribs[10] = "";
            attribs[11] = "";
        }

        void setSuccess(DicomObject dicomobject, DicomObject dicomobject1)
        {
            attribs[0] = "Success";
            attribs[9] = getString(79, dicomobject, dicomobject1);
            attribs[10] = getString(697, dicomobject, dicomobject1);
            attribs[11] = getString(698, dicomobject, dicomobject1);
        }

        RowData(DicomObject dicomobject, String s)
            throws DicomException
        {
            uid = (String)dicomobject.get(63);
            attribs[0] = "Not yet";
            attribs[1] = s;
            attribs[9] = "";
            attribs[10] = "";
            attribs[11] = "";
            for(int i = 0; i < StorageCmtTableModel.DNAMES.length; i++)
                attribs[i + 2] = dicomobject.getS(StorageCmtTableModel.DNAMES[i]);

            sopReference.set(115, dicomobject.get(62));
            sopReference.set(116, ((Object) (uid)));
        }
    }


    private static final String SUCCESS = "Success";
    private static final String NOT_YET = "Not yet";
    private static final String HEADER[] = {
        "Commitment", "File sent", "PatientID", "PatientName", "StudyID", "StudyDate", "Md", "S#", "I#", "RetrieveAET", 
        "FileSetID", "FileSetUID"
    };
    private static final int STATUS = 0;
    private static final int FILE_PATH = 1;
    private static final int DNAME_OFFSET = 2;
    private static final int RETRIEVE_AET = 9;
    private static final int FILESET_ID = 10;
    private static final int FILESET_UID = 11;
    private static final int DNAMES[] = {
        148, 147, 427, 64, 81, 428, 430
    };
    private final PrintStream log;
    private final Vector data = new Vector();
    private final IDimseListener cmtResultListener = new IDimseListener() {

        public void notify(DicomMessage dicommessage)
        {
            DicomObject dicomobject = dicommessage.getDataset();
            if(dicomobject == null)
            {
                log.println("Error: Missing Event Info in received N-EVENT-REPORT RQ");
                return;
            }
            int i = dicomobject.getSize(121);
            for(int j = 0; j < i; j++)
                setSuccess((DicomObject)dicomobject.get(121, j), dicomobject);

            int k = dicomobject.getSize(120);
            for(int l = 0; l < k; l++)
                setFailed((DicomObject)dicomobject.get(120, l));

        }

    };

    private void setSuccess(DicomObject dicomobject, DicomObject dicomobject1)
    {
        try
        {
            int i = rowIndex(dicomobject);
            if(i == -1)
            {
                log.println("Error: Did not request commitment of SOP instance - " + dicomobject.getS(116));
                return;
            }
            ((RowData)data.elementAt(i)).setSuccess(dicomobject, dicomobject1);
            ((AbstractTableModel)this).fireTableRowsUpdated(i, i);
        }
        catch(DicomException dicomexception)
        {
            log.println(((Throwable) (dicomexception)).getMessage());
        }
    }

    private void setFailed(DicomObject dicomobject)
    {
        try
        {
            int i = rowIndex(dicomobject);
            if(i == -1)
            {
                log.println("Error: Did not request commitment of SOP instance - " + dicomobject.getS(116));
                return;
            }
            ((RowData)data.elementAt(i)).setFailed(dicomobject);
            ((AbstractTableModel)this).fireTableRowsUpdated(i, i);
        }
        catch(DicomException dicomexception)
        {
            log.println(((Throwable) (dicomexception)).getMessage());
        }
    }

    private int rowIndex(DicomObject dicomobject)
    {
        String s = (String)dicomobject.get(116);
        int i = data.size();
        for(int j = 0; j < i; j++)
        {
            RowData rowdata = (RowData)data.elementAt(j);
            if(s.equals(((Object) (rowdata.uid))))
                return j;
        }

        return -1;
    }

    private String getString(int i, DicomObject dicomobject, DicomObject dicomobject1)
    {
        String s;
        try
        {
            if((s = (String)dicomobject.get(i)) != null || (s = (String)dicomobject1.get(i)) != null)
                return s;
            else
                return "";
        }
        catch(Exception exception)
        {
            log.println(((Throwable) (exception)).getMessage());
        }
        return "";
    }

    public StorageCmtTableModel(PrintStream printstream)
    {
        log = printstream;
    }

    public IDimseRspListener getStorageListener(DicomObject dicomobject, String s)
        throws DicomException
    {
        return ((IDimseRspListener) (new StorageListener(new RowData(dicomobject, s))));
    }

    public IDimseListener getCmtResultListener()
    {
        return cmtResultListener;
    }

    public String getColumnName(int i)
    {
        return HEADER[i];
    }

    public int getColumnCount()
    {
        return HEADER.length;
    }

    public int getRowCount()
    {
        return data.size();
    }

    public Object getValueAt(int i, int j)
    {
        return ((Object) (((RowData)data.elementAt(i)).attribs[j]));
    }

    public DicomObject getSopReferenceAt(int i)
    {
        return ((RowData)data.elementAt(i)).sopReference;
    }

    public boolean isCommited(int i)
    {
        return ((RowData)data.elementAt(i)).attribs[0] == "Success";
    }








}
