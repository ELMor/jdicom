// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageSCUParam.java

package com.tiani.dicom.client;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.UIDUtils;
import java.util.Hashtable;
import java.util.Properties;

final class StorageSCUParam
{

    private final Properties props;
    private final int port;
    private final int storageCmtPort;
    private final boolean storageCmtMultiThreadTCP;
    private final boolean storageCmtMultiThreadAssoc;
    private final boolean storageCmtCheckAET;
    private final boolean storageCmtAutoRelease;
    private final boolean storageCmtAbort;
    private final int maxInvoke;
    private final int assocTimeout;
    private final int releaseTimeout;
    private final int maxPduSize;
    private final int priority;
    private final boolean splitMultiFrame;
    private final boolean skipPrivate;
    private final boolean anonymize;
    private final boolean dicomize;
    private final boolean resample;
    private final int maxRows;
    private final int maxColumns;
    private final int verbose;
    private final int tsid;
    private final String uidPrefix;
    private final Request request;
    private final IconFactory iconFactory;
    public static final String KEYS[] = {
        "Host", "Port", "CalledTitle", "CallingTitle", "StorageCmt.Port", "StorageCmt.MultiThreadTCP", "StorageCmt.MultiThreadAssoc", "StorageCmt.CheckAET", "StorageCmt.AutoRelease", "StorageCmt.Abort", 
        "AssocTimeout[ms]", "ReleaseTimeout[ms]", "MaxInvoke", "MaxPduSize", "Priority", "TransferSyntax", "SkipPrivate", "SplitMultiFrame", "Anonymize", "Anonymize.NewName", 
        "Anonymize.NewID", "Dicomize", "Dicomize.UIDprefix", "Dicomize.StudyInstanceUID", "Dicomize.SeriesInstanceUID", "Dicomize.SOPInstanceUID", "Dicomize.SOPClassUID", "Dicomize.Photometric", "Resample", "Resample.MaxRows", 
        "Resample.MaxColumns", "Verbose", "DumpCmdsetIntoDir", "DumpDatasetIntoDir"
    };
    private static final int DEFAULT_TS_IDS[] = {
        8193
    };
    private static final int JPEG_TS_IDS[] = {
        8196
    };
    private static final String PRIORITIES[] = {
        "MEDIUM", "HIGH", "LOW"
    };
    private static final String SOP_CLASS_UIDS[] = {
        "1.2.840.10008.5.1.4.1.1.1", "1.2.840.10008.5.1.4.1.1.2", "1.2.840.10008.5.1.4.1.1.4", "1.2.840.10008.5.1.4.1.1.7", "1.2.840.10008.5.1.4.1.1.6.1", "1.2.840.10008.5.1.4.1.1.6"
    };
    private static final String PHOTOMETRIC[] = {
        "MONOCHROME1", "MONOCHROME2", "RGB"
    };
    private static final String IMAGE_TRANSFERSYNTAX[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian", "JPEGLossless", "JPEGBaseline"
    };
    private static final int TRANSFERSYNTAX_IDS[] = {
        8193, 8194, 8195, 8197, 8196
    };
    private static final String VERBOSE[] = {
        "0", "1", "2", "3", "4", "5"
    };
    public static Hashtable CHECKS;

    public StorageSCUParam(Properties properties)
        throws IllegalArgumentException
    {
        props = (Properties)((Hashtable) (properties)).clone();
        CheckParam.verify(props, CHECKS);
        port = Integer.parseInt(props.getProperty("Port"));
        storageCmtPort = Integer.parseInt(props.getProperty("StorageCmt.Port"));
        storageCmtMultiThreadTCP = parseBoolean(props.getProperty("StorageCmt.MultiThreadTCP"));
        storageCmtMultiThreadAssoc = parseBoolean(props.getProperty("StorageCmt.MultiThreadAssoc"));
        storageCmtCheckAET = parseBoolean(props.getProperty("StorageCmt.CheckAET"));
        storageCmtAutoRelease = parseBoolean(props.getProperty("StorageCmt.AutoRelease"));
        storageCmtAbort = parseBoolean(props.getProperty("StorageCmt.Abort"));
        assocTimeout = Integer.parseInt(props.getProperty("AssocTimeout[ms]"));
        releaseTimeout = Integer.parseInt(props.getProperty("ReleaseTimeout[ms]"));
        maxPduSize = Integer.parseInt(props.getProperty("MaxPduSize"));
        maxInvoke = Integer.parseInt(props.getProperty("MaxInvoke"));
        priority = indexOf(PRIORITIES, props.getProperty("Priority"));
        skipPrivate = parseBoolean(props.getProperty("SkipPrivate"));
        splitMultiFrame = parseBoolean(props.getProperty("SplitMultiFrame"));
        anonymize = parseBoolean(props.getProperty("Anonymize"));
        dicomize = parseBoolean(props.getProperty("Dicomize"));
        resample = parseBoolean(props.getProperty("Resample"));
        maxRows = Integer.parseInt(props.getProperty("Resample.MaxRows"));
        maxColumns = Integer.parseInt(props.getProperty("Resample.MaxColumns"));
        verbose = Integer.parseInt(props.getProperty("Verbose"));
        String s = props.getProperty("Dicomize.UIDprefix");
        uidPrefix = s == null || s.length() <= 0 ? null : s;
        tsid = TRANSFERSYNTAX_IDS[indexOf(IMAGE_TRANSFERSYNTAX, props.getProperty("TransferSyntax"))];
        try
        {
            request = createRequest(props.getProperty("CallingTitle"), props.getProperty("CalledTitle"), maxPduSize, maxInvoke, tsid);
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new RuntimeException(((Throwable) (illegalvalueexception)).getMessage());
        }
        iconFactory = tsid != 8196 ? null : createIconFactory(resample, maxRows, maxColumns);
        s = props.getProperty("DumpCmdsetIntoDir");
        Debug.dumpCmdsetIntoDir = s == null || s.length() <= 0 ? null : s;
        s = props.getProperty("DumpDatasetIntoDir");
        Debug.dumpDatasetIntoDir = s == null || s.length() <= 0 ? null : s;
    }

    private static IconFactory createIconFactory(boolean flag, int i, int j)
    {
        IconFactory iconfactory = flag ? new IconFactory(i, j) : new IconFactory();
        iconfactory.setJpeg(true);
        iconfactory.setJpegMultiframe(true);
        return iconfactory;
    }

    private static Request createRequest(String s, String s1, int i, int j, int k)
        throws IllegalValueException
    {
        int ai[] = {
            k, 8193
        };
        int ai1[] = DEFAULT_TS_IDS;
        switch(k)
        {
        case 8193: 
            ai = DEFAULT_TS_IDS;
            break;

        case 8194: 
        case 8195: 
            ai1 = ai;
            break;
        }
        Request request1 = new Request();
        request1.setCalledTitle(s1);
        request1.setCallingTitle(s);
        request1.setMaxPduSize(i);
        if(j != 1)
        {
            request1.setMaxOperationsInvoked(j);
            request1.setMaxOperationsPerformed(1);
        }
        request1.addPresentationContext(4097, ai1);
        request1.addPresentationContext(4100, ai1);
        request1.addPresentationContext(4165, ai1);
        request1.addPresentationContext(4166, ai1);
        request1.addPresentationContext(4167, ai1);
        request1.addPresentationContext(4168, ai1);
        request1.addPresentationContext(4194, ai1);
        request1.addPresentationContext(4196, ai1);
        request1.addPresentationContext(4195, ai1);
        if(k == 8196)
        {
            request1.addPresentationContext(4123, JPEG_TS_IDS);
        } else
        {
            request1.addPresentationContext(4118, ai);
            request1.addPresentationContext(4119, ai);
            request1.addPresentationContext(4120, ai);
            request1.addPresentationContext(4121, ai);
            request1.addPresentationContext(4122, ai);
            request1.addPresentationContext(4123, ai);
            request1.addPresentationContext(4128, ai);
            request1.addPresentationContext(4129, ai);
            request1.addPresentationContext(4130, ai);
            request1.addPresentationContext(4131, ai);
            request1.addPresentationContext(4148, ai);
            request1.addPresentationContext(4149, ai);
            request1.addPresentationContext(4151, ai);
            request1.addPresentationContext(4153, ai);
            request1.addPresentationContext(4157, ai);
            request1.addPresentationContext(4158, ai);
            request1.addPresentationContext(4159, ai);
            request1.addPresentationContext(4161, ai);
            request1.addPresentationContext(4162, ai);
            request1.addPresentationContext(4163, ai);
            request1.addPresentationContext(4164, ai);
            request1.addPresentationContext(4176, ai);
            request1.addPresentationContext(4177, ai);
            request1.addPresentationContext(4178, ai);
            request1.addPresentationContext(4179, ai);
            request1.addPresentationContext(4180, ai);
            request1.addPresentationContext(4181, ai);
        }
        request1.addPresentationContext(4124, ai1);
        request1.addPresentationContext(4125, ai1);
        request1.addPresentationContext(4126, ai1);
        request1.addPresentationContext(4127, ai1);
        request1.addPresentationContext(4152, ai1);
        request1.addPresentationContext(4154, ai1);
        request1.addPresentationContext(4155, ai1);
        request1.addPresentationContext(4156, ai1);
        request1.addPresentationContext(4182, ai1);
        request1.addPresentationContext(4183, ai1);
        request1.addPresentationContext(4184, ai1);
        request1.addPresentationContext(4147, ai1);
        request1.addPresentationContext(4099, ai1);
        request1.addPresentationContext(4197, ai1);
        return request1;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("Param:\n");
        for(int i = 0; i < KEYS.length; i++)
            stringbuffer.append(KEYS[i]).append('=').append(props.getProperty(KEYS[i])).append('\n');

        return stringbuffer.toString();
    }

    public String getHost()
    {
        return props.getProperty("Host");
    }

    public int getPort()
    {
        return port;
    }

    public int getStorageCmtPort()
    {
        return storageCmtPort;
    }

    public String getCallingTitle()
    {
        return props.getProperty("CallingTitle");
    }

    public String getCalledTitle()
    {
        return props.getProperty("CalledTitle");
    }

    public int getReleaseTimeout()
    {
        return releaseTimeout;
    }

    public int getAssocTimeout()
    {
        return assocTimeout;
    }

    public int getMaxPduSize()
    {
        return maxPduSize;
    }

    public int getMaxInvoke()
    {
        return maxInvoke;
    }

    public Request getRequest()
    {
        return request;
    }

    public int getTSID()
    {
        return tsid;
    }

    public int getPriority()
    {
        return priority;
    }

    public boolean isSkipPrivate()
    {
        return skipPrivate;
    }

    public boolean isSplitMultiFrame()
    {
        return splitMultiFrame;
    }

    public boolean isAnonymize()
    {
        return anonymize;
    }

    public boolean isStorageCmtMultiThreadTCP()
    {
        return storageCmtMultiThreadTCP;
    }

    public boolean isStorageCmtMultiThreadAssoc()
    {
        return storageCmtMultiThreadAssoc;
    }

    public boolean isStorageCmtCheckAET()
    {
        return storageCmtCheckAET;
    }

    public boolean isStorageCmtAutoRelease()
    {
        return storageCmtAutoRelease;
    }

    public boolean isStorageCmtAbort()
    {
        return storageCmtAbort;
    }

    public String getAnonymizeNewName()
    {
        return props.getProperty("Anonymize.NewName");
    }

    public String getAnonymizeNewID()
    {
        return props.getProperty("Anonymize.NewID");
    }

    public boolean isDicomize()
    {
        return dicomize;
    }

    public boolean isResample()
    {
        return resample;
    }

    public boolean isStudyInstanceUID()
    {
        return ((Hashtable) (props)).contains("Dicomize.StudyInstanceUID");
    }

    public boolean isSeriesInstanceUID()
    {
        return ((Hashtable) (props)).contains("Dicomize.SeriesInstanceUID");
    }

    public boolean isSOPInstanceUID()
    {
        return ((Hashtable) (props)).contains("Dicomize.SOPInstanceUID");
    }

    public String getStudyInstanceUID()
    {
        return getInstanceUID("Dicomize.StudyInstanceUID");
    }

    public String getSeriesInstanceUID()
    {
        return getInstanceUID("Dicomize.SeriesInstanceUID");
    }

    public String getSOPInstanceUID()
    {
        return getInstanceUID("Dicomize.SOPInstanceUID");
    }

    public String getInstanceUID(String s)
    {
        String s1 = props.getProperty(s);
        return s1 == null || s1.length() <= 0 ? UIDUtils.createUID(uidPrefix) : s1;
    }

    public String getSOPClassUID()
    {
        return props.getProperty("Dicomize.SOPClassUID");
    }

    public String getPhotometric()
    {
        return props.getProperty("Dicomize.Photometric");
    }

    public int getMaxRows()
    {
        return maxRows;
    }

    public int getMaxColumns()
    {
        return maxColumns;
    }

    public int getVerbose()
    {
        return verbose;
    }

    public IconFactory getIconFactory()
    {
        return iconFactory;
    }

    public Properties getProperties()
    {
        return props;
    }

    private static boolean parseBoolean(String s)
    {
        return s != null && "true".compareTo(s.toLowerCase()) == 0;
    }

    private int indexOf(String as[], String s)
    {
        int i;
        for(i = as.length; i-- > 0;)
            if(as[i].equals(((Object) (s))))
                break;

        return i;
    }

    static 
    {
        CHECKS = new Hashtable();
        CHECKS.put("Port", ((Object) (CheckParam.range(100, 65535))));
        CHECKS.put("StorageCmt.Port", ((Object) (CheckParam.range(100, 65535))));
        CHECKS.put("StorageCmt.MultiThreadTCP", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.MultiThreadAssoc", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.CheckAET", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.AutoRelease", ((Object) (CheckParam.bool())));
        CHECKS.put("StorageCmt.Abort", ((Object) (CheckParam.bool())));
        CHECKS.put("AssocTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("ReleaseTimeout[ms]", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("MaxPduSize", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("MaxInvoke", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("Priority", ((Object) (CheckParam.enum(PRIORITIES))));
        CHECKS.put("TransferSyntax", ((Object) (CheckParam.enum(IMAGE_TRANSFERSYNTAX))));
        CHECKS.put("SkipPrivate", ((Object) (CheckParam.bool())));
        CHECKS.put("SplitMultiFrame", ((Object) (CheckParam.bool())));
        CHECKS.put("Anonymize", ((Object) (CheckParam.bool())));
        CHECKS.put("Dicomize", ((Object) (CheckParam.bool())));
        CHECKS.put("Dicomize.SOPClassUID", ((Object) (CheckParam.enum(SOP_CLASS_UIDS))));
        CHECKS.put("Dicomize.Photometric", ((Object) (CheckParam.enum(PHOTOMETRIC))));
        CHECKS.put("Resample", ((Object) (CheckParam.bool())));
        CHECKS.put("Resample.MaxRows", ((Object) (CheckParam.range(32, 65535))));
        CHECKS.put("Resample.MaxColumns", ((Object) (CheckParam.range(32, 65535))));
        CHECKS.put("Verbose", ((Object) (CheckParam.enum(VERBOSE))));
    }
}
