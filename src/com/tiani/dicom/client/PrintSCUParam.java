// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintSCUParam.java

package com.tiani.dicom.client;

import com.archimed.dicom.Debug;
import com.tiani.dicom.util.CheckParam;
import java.util.Hashtable;
import java.util.Properties;

final class PrintSCUParam
{

    static final int LUT_SESSION_LEVEL = 0;
    static final int LUT_FILMBOX_LEVEL = 1;
    static final int LUT_IMAGEBOX_LEVEL = 2;
    private final int _port;
    private final int maxPduSize;
    private final boolean grouplens;
    private final boolean _verification;
    private final boolean _basicGrayscalePrintManagement;
    private final boolean _basicColorPrintManagement;
    private final boolean _basicAnnotationBox;
    private final boolean _basicPrintImageOverlayBox;
    private final boolean _presentationLUT;
    private final boolean _printJob;
    private final boolean _printerConfigurationRetrieval;
    private final boolean sendAspectRatioAlways;
    private final int _requestedZoom;
    private final int _burnInInfo;
    private final int bitDepth;
    private final int inflateBitsAlloc;
    private final int lutShape;
    private final double lutGamma;
    private final int lutLevel;
    private final boolean lutApplyBySCU;
    private final boolean lutScaleToFitBitDepth;
    private final boolean minMaxWindowing;
    private final int _verbose;
    private final Properties _prop;
    public static final String KEYS[] = {
        "Host", "Port", "CalledTitle", "CallingTitle", "MaxPduSize", "Grouplens", "SOP.Verification", "SOP.BasicGrayscalePrintManagement", "SOP.BasicColorPrintManagement", "SOP.BasicAnnotationBox", 
        "SOP.BasicPrintImageOverlayBox", "SOP.PresentationLUT", "SOP.PrintJob", "SOP.PrinterConfigurationRetrieval", "Session.NumberOfCopies", "Session.PrintPriority", "Session.MediumType", "Session.FilmDestination", "Session.FilmSessionLabel", "Session.MemoryAllocation", 
        "Session.OwnerID", "FilmBox.ImageDisplayFormat", "FilmBox.FilmOrientation", "FilmBox.FilmSizeID", "FilmBox.RequestedResolutionID", "FilmBox.AnnotationDisplayFormatID", "FilmBox.MagnificationType", "FilmBox.SmoothingType", "FilmBox.BorderDensity", "FilmBox.EmptyImageDensity", 
        "FilmBox.MinDensity", "FilmBox.MaxDensity", "FilmBox.Trim", "FilmBox.ConfigurationInformation", "FilmBox.Illumination", "FilmBox.ReflectedAmbientLight", "ImageBox.Polarity", "ImageBox.MagnificationType", "ImageBox.SmoothingType", "ImageBox.MinDensity", 
        "ImageBox.MaxDensity", "ImageBox.ConfigurationInformation", "ImageBox.RequestedDecimateCropBehavior", "ImageBox.RequestedImageSize", "LUT.Shape", "LUT.Gamma", "LUT.Level", "LUT.ScaleToFitBitDepth", "LUT.ApplyBySCU", "User.SendAspectRatio", 
        "User.RequestedZoom", "User.BurnInInfo", "User.BurnInInfo.Properties", "User.BitDepth", "User.InflateBitsAlloc", "User.MinMaxWindowing", "Verbose", "DumpCmdsetIntoDir", "DumpDatasetIntoDir"
    };
    public static Hashtable CHECKS;
    private static final String PRINT_PRIORITY[] = {
        "", "HIGH", "MED", "LOW"
    };
    private static final String MEDIUM_TYPE[] = {
        "", "PAPER", "CLEAR FILM", "BLUE FILM"
    };
    private static final String FILM_DESTINATION[] = {
        "", "MAGAZINE", "PROCESSOR", "BIN_1", "BIN_2", "BIN_3", "BIN_4", "BIN_5", "BIN_6", "BIN_7", 
        "BIN_8"
    };
    private static final String IMAGE_DISPLAY_FORMAT[] = {
        "STANDARD\\1,1", "STANDARD\\2,3", "ROW\\2", "COL\\2", "SLIDE", "SUPERSLIDE", "CUSTOM\\1"
    };
    private static final String FILM_ORIENTATION[] = {
        "", "PORTRAIT", "LANDSCAPE"
    };
    private static final String FILM_SIZE_ID[] = {
        "", "8INX10IN", "10INX12IN", "10INX14IN", "11INX14IN", "14INX14IN", "14INX17IN", "24CMX24CM", "24CMX30CM"
    };
    private static final String MAGNIFICATION_TYPE[] = {
        "", "REPLICATE", "BILINEAR", "CUBIC", "NONE"
    };
    private static final String DENSITY[] = {
        "", "BLACK", "WHITE"
    };
    private static final String YES_NO[] = {
        "", "YES", "NO"
    };
    private static final String REQUESTED_RESOLUTION_ID[] = {
        "", "STANDARD", "HIGH"
    };
    private static final String POLARITY[] = {
        "", "NORMAL", "REVERSE"
    };
    private static final String REQUESTED_DECIMATE_CROP_BEHAVIOR[] = {
        "", "DECIMATE", "CROP", "FAIL"
    };
    private static final String SEND_ASPECTRATIO[] = {
        "Always", "IfNot1/1"
    };
    private static final String BURNIN_INFO[] = {
        "No", "IfNoOverlays", "Always"
    };
    static final int LUT_FILE = 0;
    static final int LUT_GAMMA = 1;
    static final int LUT_IDENTITY = 2;
    static final int LUT_LIN_OD = 3;
    static final int LUT_INVERSE = 4;
    private static final String LUT_SHAPE[] = {
        "<file>", "<gamma>", "IDENTITY", "LIN OD", "INVERSE"
    };
    private static final String LUT_LEVEL[] = {
        "FilmSession", "FilmBox", "ImageBox"
    };
    private static final String INFLATE_BIT_DEPTH[] = {
        "Always", "IfNonLinear", "No"
    };
    private static final String VERBOSE[] = {
        "0", "1", "2", "3", "4", "5"
    };

    public PrintSCUParam(Properties properties)
        throws IllegalArgumentException
    {
        _prop = (Properties)((Hashtable) (properties)).clone();
        CheckParam.verify(_prop, CHECKS);
        _port = Integer.parseInt(_prop.getProperty("Port"));
        maxPduSize = Integer.parseInt(_prop.getProperty("MaxPduSize"));
        grouplens = parseBoolean(_prop.getProperty("Grouplens"));
        _verification = parseBoolean(_prop.getProperty("SOP.Verification"));
        _basicGrayscalePrintManagement = parseBoolean(_prop.getProperty("SOP.BasicGrayscalePrintManagement"));
        _basicColorPrintManagement = parseBoolean(_prop.getProperty("SOP.BasicColorPrintManagement"));
        _basicAnnotationBox = parseBoolean(_prop.getProperty("SOP.BasicAnnotationBox"));
        _basicPrintImageOverlayBox = parseBoolean(_prop.getProperty("SOP.BasicPrintImageOverlayBox"));
        _presentationLUT = parseBoolean(_prop.getProperty("SOP.PresentationLUT"));
        _printJob = parseBoolean(_prop.getProperty("SOP.PrintJob"));
        _printerConfigurationRetrieval = parseBoolean(_prop.getProperty("SOP.PrinterConfigurationRetrieval"));
        lutShape = indexOf(LUT_SHAPE, _prop.getProperty("LUT.Shape"));
        lutGamma = Double.parseDouble(_prop.getProperty("LUT.Gamma"));
        lutLevel = indexOf(LUT_LEVEL, _prop.getProperty("LUT.Level"));
        lutApplyBySCU = parseBoolean(_prop.getProperty("LUT.ApplyBySCU"));
        lutScaleToFitBitDepth = parseBoolean(_prop.getProperty("LUT.ScaleToFitBitDepth"));
        String s = _prop.getProperty("User.RequestedZoom");
        _requestedZoom = s == null || s.length() <= 0 ? 0 : Integer.parseInt(s);
        sendAspectRatioAlways = indexOf(SEND_ASPECTRATIO, _prop.getProperty("User.SendAspectRatio")) == 0;
        _burnInInfo = indexOf(BURNIN_INFO, _prop.getProperty("User.BurnInInfo"));
        bitDepth = Integer.parseInt(_prop.getProperty("User.BitDepth"));
        inflateBitsAlloc = indexOf(INFLATE_BIT_DEPTH, _prop.getProperty("User.InflateBitsAlloc"));
        minMaxWindowing = parseBoolean(_prop.getProperty("User.MinMaxWindowing"));
        _verbose = Integer.parseInt(_prop.getProperty("Verbose"));
        String s1 = _prop.getProperty("DumpCmdsetIntoDir");
        Debug.dumpCmdsetIntoDir = s1 == null || s1.length() <= 0 ? null : s1;
        s1 = _prop.getProperty("DumpDatasetIntoDir");
        Debug.dumpDatasetIntoDir = s1 == null || s1.length() <= 0 ? null : s1;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("Param:\n");
        for(int i = 0; i < KEYS.length; i++)
            stringbuffer.append(KEYS[i]).append('=').append(_prop.getProperty(KEYS[i])).append('\n');

        return stringbuffer.toString();
    }

    public String getHost()
    {
        return _prop.getProperty("Host");
    }

    public String getCallingTitle()
    {
        return _prop.getProperty("CallingTitle");
    }

    public String getCalledTitle()
    {
        return _prop.getProperty("CalledTitle");
    }

    public String getLUTShape()
    {
        return _prop.getProperty("LUT.Shape");
    }

    public int getMaxPduSize()
    {
        return maxPduSize;
    }

    public int getRequestedZoom()
    {
        return _requestedZoom;
    }

    public String getBurnInInfoProperties()
    {
        return _prop.getProperty("User.BurnInInfo.Properties");
    }

    public int getPort()
    {
        return _port;
    }

    public boolean isGrouplens()
    {
        return grouplens;
    }

    public boolean isVerification()
    {
        return _verification;
    }

    public boolean isBasicGrayscalePrintManagement()
    {
        return _basicGrayscalePrintManagement;
    }

    public boolean isBasicColorPrintManagement()
    {
        return _basicColorPrintManagement;
    }

    public boolean isBasicAnnotationBox()
    {
        return _basicAnnotationBox;
    }

    public boolean isBasicPrintImageOverlayBox()
    {
        return _basicPrintImageOverlayBox;
    }

    public boolean isPresentationLUT()
    {
        return _presentationLUT;
    }

    public boolean isPrintJob()
    {
        return _printJob;
    }

    public boolean isPrinterConfigurationRetrieval()
    {
        return _printerConfigurationRetrieval;
    }

    public int[] getAbstractSyntaxes()
    {
        int ai[] = new int[8];
        int i = 0;
        if(_verification)
            ai[i++] = 4097;
        if(_basicGrayscalePrintManagement)
            ai[i++] = 12292;
        if(_basicColorPrintManagement)
            ai[i++] = 12294;
        if(_basicAnnotationBox)
            ai[i++] = 4114;
        if(_basicPrintImageOverlayBox)
            ai[i++] = 4160;
        if(_presentationLUT)
            ai[i++] = 4145;
        if(_printJob)
            ai[i++] = 4113;
        if(_printerConfigurationRetrieval)
            ai[i++] = 4175;
        int ai1[] = new int[i];
        System.arraycopy(((Object) (ai)), 0, ((Object) (ai1)), 0, i);
        return ai1;
    }

    public boolean isLutFile()
    {
        return lutShape == 0;
    }

    public boolean isLutGamma()
    {
        return lutShape == 1;
    }

    public boolean isLutShape()
    {
        return lutShape == 2 || lutShape == 3 || lutShape == 4;
    }

    public double getLutGamma()
    {
        return lutGamma;
    }

    public boolean isLutApplyBySCU()
    {
        return lutApplyBySCU;
    }

    public boolean isLutScaleToFitBitDepth()
    {
        return lutScaleToFitBitDepth;
    }

    public boolean isSendAspectRatioAlways()
    {
        return sendAspectRatioAlways;
    }

    public int getBurnInInfo()
    {
        return _burnInInfo;
    }

    public int getLutLevel()
    {
        return lutLevel;
    }

    public int getBitDepth()
    {
        return bitDepth;
    }

    public int getInflateBitsAlloc()
    {
        return inflateBitsAlloc;
    }

    public boolean isMinMaxWindowing()
    {
        return minMaxWindowing;
    }

    public int getVerbose()
    {
        return _verbose;
    }

    public Properties getProperties()
    {
        return _prop;
    }

    private static boolean parseBoolean(String s)
    {
        return s != null && "true".compareTo(s.toLowerCase()) == 0;
    }

    private int indexOf(String as[], String s)
    {
        int i;
        for(i = as.length; i-- > 0;)
            if(as[i].equals(((Object) (s))))
                break;

        return i;
    }

    static 
    {
        CHECKS = new Hashtable();
        CHECKS.put("Port", ((Object) (CheckParam.range(100, 65535))));
        CHECKS.put("MaxPduSize", ((Object) (CheckParam.range(0, 65535))));
        CHECKS.put("Grouplens", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.Verification", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.BasicGrayscalePrintManagement", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.BasicColorPrintManagement", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.BasicAnnotationBox", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.BasicPrintImageOverlayBox", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.PresentationLUT", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.PrintJob", ((Object) (CheckParam.bool())));
        CHECKS.put("SOP.PrinterConfigurationRetrieval", ((Object) (CheckParam.bool())));
        CHECKS.put("Session.PrintPriority", ((Object) (CheckParam.defined(PRINT_PRIORITY, 3))));
        CHECKS.put("Session.MediumType", ((Object) (CheckParam.defined(MEDIUM_TYPE, 3))));
        CHECKS.put("Session.FilmDestination", ((Object) (CheckParam.defined(FILM_DESTINATION, 3))));
        CHECKS.put("FilmBox.ImageDisplayFormat", ((Object) (CheckParam.defined(IMAGE_DISPLAY_FORMAT, 1))));
        CHECKS.put("FilmBox.FilmOrientation", ((Object) (CheckParam.enum(FILM_ORIENTATION, 3))));
        CHECKS.put("FilmBox.FilmSizeID", ((Object) (CheckParam.defined(FILM_SIZE_ID, 3))));
        CHECKS.put("FilmBox.MagnificationType", ((Object) (CheckParam.defined(MAGNIFICATION_TYPE, 3))));
        CHECKS.put("FilmBox.BorderDensity", ((Object) (CheckParam.defined(DENSITY, 3))));
        CHECKS.put("FilmBox.EmptyImageDensity", ((Object) (CheckParam.defined(DENSITY, 3))));
        CHECKS.put("FilmBox.Trim", ((Object) (CheckParam.enum(YES_NO, 3))));
        CHECKS.put("FilmBox.RequestedResolutionID", ((Object) (CheckParam.defined(REQUESTED_RESOLUTION_ID, 3))));
        CHECKS.put("ImageBox.Polarity", ((Object) (CheckParam.enum(POLARITY, 3))));
        CHECKS.put("ImageBox.MagnificationType", ((Object) (CheckParam.defined(MAGNIFICATION_TYPE, 3))));
        CHECKS.put("ImageBox.RequestedDecimateCropBehavior", ((Object) (CheckParam.enum(REQUESTED_DECIMATE_CROP_BEHAVIOR, 3))));
        CHECKS.put("User.SendAspectRatio", ((Object) (CheckParam.enum(SEND_ASPECTRATIO))));
        CHECKS.put("User.RequestedZoom", ((Object) (CheckParam.range(10, 400, 3))));
        CHECKS.put("User.BurnInInfo", ((Object) (CheckParam.enum(BURNIN_INFO))));
        CHECKS.put("User.BitDepth", ((Object) (CheckParam.range(8, 16))));
        CHECKS.put("User.InflateBitsAlloc", ((Object) (CheckParam.enum(INFLATE_BIT_DEPTH))));
        CHECKS.put("User.MinMaxWindowing", ((Object) (CheckParam.bool())));
        CHECKS.put("LUT.Shape", ((Object) (CheckParam.enum(LUT_SHAPE))));
        CHECKS.put("LUT.Gamma", ((Object) (CheckParam.range(0.10000000000000001D, 10D))));
        CHECKS.put("LUT.Level", ((Object) (CheckParam.enum(LUT_LEVEL))));
        CHECKS.put("LUT.ApplyBySCU", ((Object) (CheckParam.bool())));
        CHECKS.put("LUT.ScaleToFitBitDepth", ((Object) (CheckParam.bool())));
        CHECKS.put("Verbose", ((Object) (CheckParam.enum(VERBOSE))));
    }
}
