/* StorageSCU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.tiani.dicom.client;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.TagValue;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.service.Resampler;
import com.tiani.dicom.service.StorageCmtSCU;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JFileListDialog;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.UIDUtils;

public class StorageSCU
    extends JApplet {
  private JButton connectButton;
  private JButton releaseButton;
  private JButton echoButton;
  private JButton sendButton;
  private JButton sendMultiButton;
  private JButton cancelButton;
  private JButton commitButton;
  private JButton startButton;
  private JButton stopButton;
  private JPanel controlPanel;
  private JTabbedPane tabbedPane;
  private JTextArea logTextArea;
  private Document logDoc;
  private PrintStream log;
  private StorageCmtTableModel tableModel;
  private JTable commitTable;
  private JProgressBar progressBar;
  private JFileChooser fileChooser;
  private JFileListDialog fileListDlg;
  private Properties properties;
  private StorageSCUParam param;
  private StorageCmtSCU scu;
  private File[] filesToSend;
  private DicomObject commitAttribs;
  private boolean canceled;
  private final IAssociationListener assocListener;
  private final ActionListener onConnect;
  private final ActionListener onRelease;
  private final ActionListener onEcho;
  private final ActionListener onStart;
  private final ActionListener onStop;
  private final ActionListener onSend;
  private final ActionListener onSendMulti;
  private final ActionListener onCancel;
  private final ActionListener onCommit;
  private final ChangeListener onTabbedPane;

  private abstract class SendActionListener
      extends MyActionListener {
    private SendActionListener() {
      super();
    }

    public void execute() throws Exception {
      progressBar.setMinimum(0);
      progressBar.setMaximum(filesToSend.length);
      canceled = false;
      for (int i = 0;
           i < filesToSend.length && !canceled && scu.isConnected();
           i++) {
        StorageSCU.this.sendFile(filesToSend[i]);
        final int v = i + 1;
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            progressBar.setValue(v);
          }
        });
      }
      filesToSend = null;
    }
  }

  private abstract class MyActionListener
      implements ActionListener, Runnable {
    private MyActionListener() {
      /* empty */
    }

    public void actionPerformed(ActionEvent actionevent) {
      try {
        param = new StorageSCUParam(properties);
        Debug.DEBUG = param.getVerbose();
        scu.setARTIM(param.getAssocTimeout(),
                     param.getReleaseTimeout());
        scu.setCheckAET(param.isStorageCmtCheckAET());
        scu.setMaxPduSize(param.getMaxPduSize());
        scu.setMultiThreadTCP(param.isStorageCmtMultiThreadTCP());
        scu.setMultiThreadAssoc(param.isStorageCmtMultiThreadAssoc());
        if (preprocess()) {
          new Thread(this).start();
        }
        tabbedPane.setSelectedIndex(2);
        StorageSCU.this.enableButtons();
      }
      catch (Exception exception) {
        log.println(exception.getMessage());
      }
    }

    boolean preprocess() throws Exception {
      return true;
    }

    public void run() {
      try {
        execute();
      }
      catch (Exception exception) {
        exception.printStackTrace(log);
        log.println(exception.getMessage());
      }
      StorageSCU.this.enableButtons();
    }

    abstract void execute() throws Exception;
  }

  private int loadDicomObject(DicomObject dicomobject, File file) throws
      IOException, DicomException, IllegalValueException,
      UnknownUIDException {
    TianiInputStream tianiinputstream
        = new TianiInputStream(new FileInputStream(file));
    try {
      tianiinputstream.read(dicomobject);
    }
    finally {
      tianiinputstream.close();
    }
    int i = tianiinputstream.getTransferSyntaxId(dicomobject);
    if (Debug.DEBUG > 0) {
      log.println("Load from " + file + '['
                  + tianiinputstream.getFormatName() + '-'
                  + UID.getUIDEntry(i).getName() + ']');
    }
    DicomObject dicomobject_2_ = dicomobject.getFileMetaInformation();
    return i;
  }

  private void sendFile(File file) {
    try {
      DicomObject dicomobject = new DicomObject();
      int i = loadDicomObject(dicomobject, file);
      if (param.isSkipPrivate()) {
        skipPrivate(dicomobject);
      }
      if (param.isAnonymize()) {
        anonymize(dicomobject);
      }
      if (param.isDicomize()) {
        dicomize(dicomobject);
      }
      if (dicomobject.getSize(1184) == 1) {
        if (param.getIconFactory() != null) {
          param.getIconFactory().createJpegDicomObject(dicomobject);
          i = 8196;
        }
        else if (param.isResample()) {
          resample(dicomobject);
        }
      }
      String string = dicomobject.getS(62);
      String string_3_ = dicomobject.getS(63);
      DimseExchange dimseexchange = scu.getConnection();
      int i_4_ = UID.getUIDEntry(string).getConstant();
      byte i_5_ = dimseexchange.getPresentationContext(i_4_);
      UIDEntry uidentry = dimseexchange.getTransferSyntax(i_5_);
      int i_6_ = uidentry.getConstant();
      if (i_6_ != i) {
        changeTS(dicomobject, i, i_6_);
      }
      if (param.isSplitMultiFrame() && dicomobject.getSize(464) > 0) {
        byte[] is = (byte[]) dicomobject.get(1184);
        int i_7_ = dicomobject.getI(464);
        byte[] is_8_ = new byte[is.length / i_7_];
        dicomobject.deleteItem(464);
        dicomobject.set(1184, is_8_);
        for (int i_9_ = 0; i_9_ < i_7_; i_9_++) {
          System.arraycopy(is, is_8_.length * i_9_, is_8_, 0,
                           is_8_.length);
          string_3_ = UIDUtils.createUID();
          dicomobject.set(63, string_3_);
          dimseexchange.cstoreAsync
              (i_5_, string, string_3_, param.getPriority(),
               dicomobject,
               tableModel.getStorageListener(dicomobject,
                                             file.toString()));
        }
      }
      else {
        dimseexchange.cstoreAsync
            (i_5_, string, string_3_, param.getPriority(), dicomobject,
             tableModel.getStorageListener(dicomobject,
                                           file.toString()));
      }
    }
    catch (Exception exception) {
      exception.printStackTrace(log);
    }
  }

  private boolean isPixelDataNative(int i) {
    switch (i) {
      case 8193:
      case 8194:
      case 8195:
        return true;
      default:
        return false;
    }
  }

  private void changeTS(DicomObject dicomobject, int i, int i_10_) throws
      DicomException, IOException {
    Compression compression = new Compression(dicomobject);
    if (!isPixelDataNative(i)) {
      if (Debug.DEBUG > 0) {
        log.println("Decompress pixel data");
      }
      compression.decompress();
    }
    if (!isPixelDataNative(i_10_)) {
      if (Debug.DEBUG > 0) {
        log.println("Compress pixel data");
      }
      compression.compress(i_10_);
    }
  }

  private void anonymize(DicomObject dicomobject) throws DicomException {
    dicomobject.set(147, param.getAnonymizeNewName());
    dicomobject.set(148, param.getAnonymizeNewID());
    if (Debug.DEBUG > 0) {
      log.println("Patient Name & Patient ID were anonymized");
    }
  }

  private void dicomize(DicomObject dicomobject) throws DicomException {
    if (dicomobject.getSize(425) <= 0 || param.isStudyInstanceUID()) {
      dicomobject.set(425, param.getStudyInstanceUID());
      if (Debug.DEBUG > 0) {
        log.println("Set StudyInstanceUID");
      }
    }
    if (dicomobject.getSize(426) <= 0 || param.isSeriesInstanceUID()) {
      dicomobject.set(426, param.getSeriesInstanceUID());
      if (Debug.DEBUG > 0) {
        log.println("Set StudyInstanceUID");
      }
    }
    if (dicomobject.getSize(62) <= 0) {
      dicomobject.set(62, param.getSOPClassUID());
      if (Debug.DEBUG > 0) {
        log.println("Set SOPClassUID");
      }
    }
    if (dicomobject.getSize(63) <= 0 || param.isSOPInstanceUID()) {
      dicomobject.set(63, param.getSOPInstanceUID());
      if (Debug.DEBUG > 0) {
        log.println("Set SOPInstanceUID");
      }
    }
    if (dicomobject.getSize(462) <= 0) {
      dicomobject.set(462, param.getPhotometric());
      if (Debug.DEBUG > 0) {
        log.println("Set Photometric Interpretation");
      }
    }
    if (dicomobject.getSize(461) <= 0) {
      dicomobject.set(461,
                      new Integer("RGB".equals(param.getPhotometric())
                                  ? 3 : 1));
      if (Debug.DEBUG > 0) {
        log.println("Set Samples Per Pixel");
      }
    }
  }

  private void resample(DicomObject dicomobject) throws DicomException {
    if (Resampler.resample(dicomobject, param.getMaxRows(),
                           param.getMaxColumns())
        && Debug.DEBUG > 0) {
      log.println("Resample Image");
    }
  }

  private void skipPrivate(DicomObject dicomobject) {
    boolean bool = false;
    Enumeration enumeration = dicomobject.enumerateVRs(true, true);
    while (enumeration.hasMoreElements()) {
      TagValue tagvalue = (TagValue) enumeration.nextElement();
      int i = tagvalue.getGroup();
      if ( (i & 0x1) != 0 && dicomobject.containsGroup(i)) {
        dicomobject.removeGroup(i);
        bool = true;
      }
    }
    if (bool && Debug.DEBUG > 0) {
      log.println("Private Tags were skipped");
    }
  }

  private File[] chooseFiles() {
    File file = chooseFile("Select Directory", "Select", 2);
    if (file == null) {
      return null;
    }
    if (fileListDlg == null) {
      fileListDlg
          = new JFileListDialog( (java.awt.Frame)null, "Send files");
    }
    return fileListDlg.getSelectedFiles(file.isDirectory() ? file
                                        : new File(file.getParent()));
  }

  private void autoSelectToCommit() {
    int i = tableModel.getRowCount();
    ListSelectionModel listselectionmodel
        = commitTable.getSelectionModel();
    listselectionmodel.setValueIsAdjusting(true);
    for (int i_11_ = 0; i_11_ < i; i_11_++) {
      if (tableModel.isCommited(i_11_)) {
        listselectionmodel.removeSelectionInterval(i_11_, i_11_);
      }
      else {
        listselectionmodel.addSelectionInterval(i_11_, i_11_);
      }
    }
    listselectionmodel.setValueIsAdjusting(false);
  }

  private void enableButtons() {
    try {
      connectButton.setEnabled(!scu.isConnected());
      releaseButton.setEnabled(scu.isConnected());
      echoButton.setEnabled(filesToSend == null && scu.isEnabled(4097));
      sendButton.setEnabled(filesToSend == null && scu.isConnected());
      sendMultiButton
          .setEnabled(filesToSend == null && scu.isConnected());
      cancelButton.setEnabled(filesToSend != null);
      startButton.setEnabled(!scu.isServerRunning());
      stopButton.setEnabled(scu.isServerRunning());
      commitButton.setEnabled(scu.isServerRunning()
                              && commitAttribs == null
                              && scu.isEnabled(4100));
    }
    catch (Exception exception) {
      exception.printStackTrace(log);
    }
  }

  private void initControlPanel() {
    controlPanel.add(connectButton);
    controlPanel.add(echoButton);
    controlPanel.add(sendButton);
    controlPanel.add(startButton);
    controlPanel.add(new JTianiButton(isApplet() ? this.getAppletContext()
                                      : null));
    controlPanel.add(releaseButton);
    controlPanel.add(cancelButton);
    controlPanel.add(sendMultiButton);
    controlPanel.add(stopButton);
    controlPanel.add(commitButton);
  }

  private void initActionListeners() {
    connectButton.addActionListener(onConnect);
    releaseButton.addActionListener(onRelease);
    echoButton.addActionListener(onEcho);
    sendButton.addActionListener(onSend);
    sendMultiButton.addActionListener(onSendMulti);
    cancelButton.addActionListener(onCancel);
    commitButton.addActionListener(onCommit);
    startButton.addActionListener(onStart);
    stopButton.addActionListener(onStop);
    tabbedPane.addChangeListener(onTabbedPane);
  }

  private boolean isApplet() {
    return! (this.getAppletContext()instanceof AppletFrame);
  }

  public static void main(String[] strings) {
    try {
      final String propFile
          = strings.length > 0 ? strings[0] : "StorageSCU.properties";
      final StorageSCU applet = new StorageSCU(loadProperties(propFile));
      new AppletFrame("StorageSCU v1.7.35", applet, 500, 400, new WindowAdapter() {
        public void windowClosing(WindowEvent windowevent) {
          try {
            storeProperties(propFile, applet.properties);
          }
          catch (Exception exception) {
            System.out.println(exception.getMessage());
          }
          System.exit(0);
        }
      });
    }
    catch (Throwable throwable) {
      throwable.printStackTrace(System.out);
    }
  }

  public StorageSCU() {
    connectButton = new JButton("Connect");
    releaseButton = new JButton("Release");
    echoButton = new JButton("Echo");
    sendButton = new JButton("Send");
    sendMultiButton = new JButton("Send *");
    cancelButton = new JButton("Cancel");
    commitButton = new JButton("Commit");
    startButton = new JButton("Start");
    stopButton = new JButton("Stop");
    controlPanel = new JPanel(new GridLayout(0, 5));
    tabbedPane = new JTabbedPane(2);
    logTextArea = new JTextArea();
    logDoc = logTextArea.getDocument();
    log = new PrintStream(new DocumentOutputStream(logDoc, 50000), true);
    tableModel = new StorageCmtTableModel(log);
    commitTable = new JSizeColumnsToFitTable(tableModel);
    progressBar = new JProgressBar();
    fileChooser = null;
    fileListDlg = null;
    properties = null;
    param = null;
    scu = new StorageCmtSCU();
    filesToSend = null;
    commitAttribs = null;
    canceled = false;
    assocListener = new IAssociationListener() {
      public void associateRequestReceived
          (VerboseAssociation verboseassociation, Request request) {
        /* empty */
      }

      public void associateResponseReceived
          (VerboseAssociation verboseassociation, Response response) {
        /* empty */
      }

      public void associateRequestSent
          (VerboseAssociation verboseassociation, Request request) {
        /* empty */
      }

      public void associateResponseSent
          (VerboseAssociation verboseassociation, Response response) {
        /* empty */
      }

      public void releaseRequestReceived
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseResponseReceived
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseRequestSent
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseResponseSent
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void abortReceived(VerboseAssociation verboseassociation,
                                Abort abort) {
        /* empty */
      }

      public void abortSent(VerboseAssociation verboseassociation, int i,
                            int i_13_) {
        /* empty */
      }

      public void socketClosed(VerboseAssociation verboseassociation) {
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            StorageSCU.this.enableButtons();
          }
        });
      }
    };
    onConnect = new MyActionListener() {
      void execute() throws Exception {
        try {
          scu.connect(param.getHost(),
                      param.getPort(),
                      param.getRequest());
          if (scu.isConnected()) {
            scu.getConnection()
                .addAssociationListener(assocListener);
          }
        }
        catch (SecurityException securityexception) {
          log.println(securityexception.getMessage());
          showPolicyFile();
        }
      }
    };
    onRelease = new MyActionListener() {
      void execute() throws Exception {
        scu.release();
      }
    };
    onEcho = new MyActionListener() {
      void execute() throws Exception {
        scu.echo();
      }
    };
    onStart = new MyActionListener() {
      void execute() throws Exception {
        scu.start(param.getStorageCmtPort());
      }
    };
    onStop = new MyActionListener() {
      void execute() throws Exception {
        scu.stop(param.isStorageCmtAbort(), false);
      }
    };
    onSend = new SendActionListener() {
      boolean preprocess() throws Exception {
        File file = chooseFile("Send file", "Send", 0);
        if (file == null) {
          return false;
        }
        filesToSend = new File[] {
            file};
        return true;
      }
    };
    onSendMulti = new SendActionListener() {
      boolean preprocess() throws Exception {
        filesToSend = chooseFiles();
        return filesToSend != null;
      }
    };
    onCancel = new MyActionListener() {
      void execute() throws Exception {
        canceled = true;
      }
    };
    onCommit = new MyActionListener() {
      boolean preprocess() throws Exception {
        if (tabbedPane.getSelectedIndex() != 1) {
          autoSelectToCommit();
        }
        int[] is = commitTable.getSelectedRows();
        if (is.length == 0) {
          log.println("no selected sop instances to commit");
          return false;
        }
        commitAttribs = new DicomObject();
        for (int i = 0; i < is.length; i++) {
          commitAttribs.append(121,
                               tableModel
                               .getSopReferenceAt(is[i]));
        }
        return true;
      }

      void execute() throws Exception {
        scu.commit(commitAttribs,
                   tableModel.getCmtResultListener());
        commitAttribs = null;
        if (param.isStorageCmtAutoRelease()) {
          scu.release();
        }
      }
    };
    onTabbedPane = new ChangeListener() {
      public void stateChanged(ChangeEvent changeevent) {
        if (tabbedPane.getSelectedIndex() == 1) {
          StorageSCU.this.autoSelectToCommit();
        }
      }
    };
  }

  protected StorageSCU(Properties properties) {
    connectButton = new JButton("Connect");
    releaseButton = new JButton("Release");
    echoButton = new JButton("Echo");
    sendButton = new JButton("Send");
    sendMultiButton = new JButton("Send *");
    cancelButton = new JButton("Cancel");
    commitButton = new JButton("Commit");
    startButton = new JButton("Start");
    stopButton = new JButton("Stop");
    controlPanel = new JPanel(new GridLayout(0, 5));
    tabbedPane = new JTabbedPane(2);
    logTextArea = new JTextArea();
    logDoc = logTextArea.getDocument();
    log = new PrintStream(new DocumentOutputStream(logDoc, 50000), true);
    tableModel = new StorageCmtTableModel(log);
    commitTable = new JSizeColumnsToFitTable(tableModel);
    progressBar = new JProgressBar();
    fileChooser = null;
    fileListDlg = null;
    this.properties = null;
    param = null;
    scu = new StorageCmtSCU();
    filesToSend = null;
    commitAttribs = null;
    canceled = false;
    assocListener = new IAssociationListener() {
      public void associateRequestReceived
          (VerboseAssociation verboseassociation, Request request) {
        /* empty */
      }

      public void associateResponseReceived
          (VerboseAssociation verboseassociation, Response response) {
        /* empty */
      }

      public void associateRequestSent
          (VerboseAssociation verboseassociation, Request request) {
        /* empty */
      }

      public void associateResponseSent
          (VerboseAssociation verboseassociation, Response response) {
        /* empty */
      }

      public void releaseRequestReceived
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseResponseReceived
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseRequestSent
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void releaseResponseSent
          (VerboseAssociation verboseassociation) {
        /* empty */
      }

      public void abortReceived(VerboseAssociation verboseassociation,
                                Abort abort) {
        /* empty */
      }

      public void abortSent(VerboseAssociation verboseassociation, int i,
                            int i_26_) {
        /* empty */
      }

      public void socketClosed(VerboseAssociation verboseassociation) {
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            StorageSCU.this.enableButtons();
          }
        });
      }
    };
    onConnect = new MyActionListener() {
      void execute() throws Exception {
        try {
          scu.connect(param.getHost(),
                      param.getPort(),
                      param.getRequest());
          if (scu.isConnected()) {
            scu.getConnection()
                .addAssociationListener(assocListener);
          }
        }
        catch (SecurityException securityexception) {
          log.println(securityexception.getMessage());
          showPolicyFile();
        }
      }
    };
    onRelease = new MyActionListener() {
      void execute() throws Exception {
        scu.release();
      }
    };
    onEcho = new MyActionListener() {
      void execute() throws Exception {
        scu.echo();
      }
    };
    onStart = new MyActionListener() {
      void execute() throws Exception {
        scu.start(param.getStorageCmtPort());
      }
    };
    onStop = new MyActionListener() {
      void execute() throws Exception {
        scu.stop(param.isStorageCmtAbort(), false);
      }
    };
    onSend = new SendActionListener() {
      boolean preprocess() throws Exception {
        File file = chooseFile("Send file", "Send", 0);
        if (file == null) {
          return false;
        }
        filesToSend = new File[] {
            file};
        return true;
      }
    };
    onSendMulti = new SendActionListener() {
      boolean preprocess() throws Exception {
        filesToSend = chooseFiles();
        return filesToSend != null;
      }
    };
    onCancel = new MyActionListener() {
      void execute() throws Exception {
        canceled = true;
      }
    };
    onCommit = new MyActionListener() {
      boolean preprocess() throws Exception {
        if (tabbedPane.getSelectedIndex() != 1) {
          autoSelectToCommit();
        }
        int[] is = commitTable.getSelectedRows();
        if (is.length == 0) {
          log.println("no selected sop instances to commit");
          return false;
        }
        commitAttribs = new DicomObject();
        for (int i = 0; i < is.length; i++) {
          commitAttribs.append(121,
                               tableModel
                               .getSopReferenceAt(is[i]));
        }
        return true;
      }

      void execute() throws Exception {
        scu.commit(commitAttribs,
                   tableModel.getCmtResultListener());
        commitAttribs = null;
        if (param.isStorageCmtAutoRelease()) {
          scu.release();
        }
      }
    };
    onTabbedPane = new ChangeListener() {
      public void stateChanged(ChangeEvent changeevent) {
        if (tabbedPane.getSelectedIndex() == 1) {
          StorageSCU.this.autoSelectToCommit();
        }
      }
    };
    this.properties = properties;
  }

  private void initProperties() {
    if (isApplet()) {
      getAppletParams();
    }
    if (properties == null || !verifyProperties()) {
      try {
        properties
            = loadProperties(StorageSCU.class.getResourceAsStream
                             ("StorageSCU.properties"));
      }
      catch (IOException ioexception) {
        throw new RuntimeException
            ("Failed to load StorageSCU.properties ressource");
      }
    }
  }

  private void initTabbedPane() {
    JPropertiesTable jpropertiestable
        = new JPropertiesTable(StorageSCUParam.KEYS, properties,
                               StorageSCUParam.CHECKS);
    tabbedPane.add("Props", new JScrollPane(jpropertiestable));
    tabbedPane.add("Commit", new JScrollPane(commitTable));
    tabbedPane.add("Log", new JAutoScrollPane(logTextArea));
  }

  public void init() {
    Debug.out = log;
    DicomObject.dumpLineLen = 80;
    initProperties();
    initTabbedPane();
    initControlPanel();
    initActionListeners();
    Container container = this.getContentPane();
    container.add(controlPanel, "North");
    container.add(tabbedPane, "Center");
    container.add(progressBar, "South");
    enableButtons();
  }

  private File chooseFile(String string, String string_38_, int i) {
    try {
      if (fileChooser == null) {
        fileChooser = new JFileChooser(".");
      }
      fileChooser.setDialogTitle(string);
      fileChooser.setFileSelectionMode(i);
      int i_39_ = fileChooser.showDialog(null, string_38_);
      File file = fileChooser.getSelectedFile();
      return i_39_ == 0 ? file : null;
    }
    catch (SecurityException securityexception) {
      log.println(securityexception.getMessage());
      showPolicyFile();
      return null;
    }
  }

  private void showPolicyFile() {
    try {
      URL url = new URL(this.getDocumentBase(),
                        this.getParameter("PolicyFile"));
      this.getAppletContext().showDocument(url, "_blank");
    }
    catch (Exception exception) {
      log.println(exception.getMessage());
    }
  }

  private static Properties loadProperties(String string) {
    try {
      return loadProperties(new FileInputStream(string));
    }
    catch (Exception exception) {
      System.out.println(exception.getMessage());
      return null;
    }
  }

  private static Properties loadProperties(InputStream inputstream) throws
      IOException {
    Properties properties = new Properties();
    try {
      properties.load(inputstream);
    }
    finally {
      inputstream.close();
    }
    return properties;
  }

  private boolean verifyProperties() {
    try {
      CheckParam.verify(properties, StorageSCUParam.CHECKS);
    }
    catch (IllegalArgumentException illegalargumentexception) {
      log.println(illegalargumentexception.getMessage());
      return false;
    }
    return true;
  }

  private static void storeProperties
      (String string, Properties properties) throws IOException {
    CheckParam.verify(properties, StorageSCUParam.CHECKS);
    FileOutputStream fileoutputstream = new FileOutputStream(string);
    try {
      properties.store(fileoutputstream, "Properties for StorageSCU");
    }
    finally {
      fileoutputstream.close();
    }
  }

  private void getAppletParams() {
    properties = new Properties();
    for (int i = 0; i < PrintSCUParam.KEYS.length; i++) {
      String string;
      if ( (string = this.getParameter(PrintSCUParam.KEYS[i])) != null) {
        properties.put(PrintSCUParam.KEYS[i], string);
      }
    }
  }
}
