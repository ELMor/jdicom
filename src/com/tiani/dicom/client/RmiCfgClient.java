// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   RmiCfgClient.java

package com.tiani.dicom.client;

import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.ui.PropertiesTableModel;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.rmicfg.IServer;
import com.tiani.rmicfg.IServerFactory;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public class RmiCfgClient
    extends JApplet {
  private static class Row {

    IServer _srv;
    String _srvName;
    String _srvType;
    boolean _srvRun;

    private Row() {
    }

  }

  private JFileChooser _fileChooser;
  private JTextField _url;
  private JButton _qrySrv;
  private JButton _delSrv;
  private JButton _newSrv;
  private JComboBox _classnames;
  private JButton _qryProp;
  private JButton _apply;
  private JButton _start;
  private JButton _stop;
  private JCheckBox _abort;
  private JCheckBox _join;
  private JButton _load;
  private JButton _save;
  private JTextArea _logTextArea;
  private Document _logDoc;
  private PrintStream _log;
  private IServerFactory _factory;
  private Vector _rows;
  private Properties _properties;
  public static final FileFilter FILE_FILTER = new ExampleFileFilter(
      "properties", "Properties");
  private static final PropertiesTableModel EMPTY_PROP_TAB_MODEL = new
      PropertiesTableModel();
  private static final String _HEADERS[] = {
      "Name", "Type", "run"
  };
  private final AbstractTableModel _srvTabModel = new AbstractTableModel() {

    public String getColumnName(int i) {
      return RmiCfgClient._HEADERS[i];
    }

    public int getColumnCount() {
      return 3;
    }

    public int getRowCount() {
      return _rows.size();
    }

    public Class getColumnClass(int i) {
      return i >= 2 ? java.lang.Boolean.class : java.lang.String.class;
    }

    public Object getValueAt(int i, int j) {
      Row row = (Row) _rows.elementAt(i);
      switch (j) {
        case 0: // '\0'
          return ( (Object) (row._srvName));

        case 1: // '\001'
          return ( (Object) (row._srvType));

        case 2: // '\002'
          return ( (Object) (new Boolean(row._srvRun)));
      }
      return ( (Object) (null));
    }

  };
  private JTable _srvTab;
  private JTable _propTab;

  public RmiCfgClient() {
    _url = new JTextField("rmi://localhost/ServerFactory");
    _qrySrv = new JButton("?");
    _delSrv = new JButton("-");
    _newSrv = new JButton("+");
    _classnames = new JComboBox();
    _qryProp = new JButton("?");
    _apply = new JButton("=");
    _start = new JButton(">");
    _stop = new JButton("||");
    _abort = new JCheckBox("abort");
    _join = new JCheckBox("join");
    _load = new JButton("L");
    _save = new JButton("S");
    _logTextArea = new JTextArea();
    _logDoc = ( (JTextComponent) (_logTextArea)).getDocument();
    _log = new PrintStream( ( (OutputStream) (new DocumentOutputStream(_logDoc,
        10000))), true);
    _factory = null;
    _rows = new Vector();
    _properties = null;
    _srvTab = new JTable( ( (javax.swing.table.TableModel) (_srvTabModel)));
    _propTab = new JTable( ( (javax.swing.table.TableModel) (
        EMPTY_PROP_TAB_MODEL)));
  }

  public static void main(String args[]) {
    RmiCfgClient rmicfgclient = new RmiCfgClient();
    new AppletFrame("RMI Configuration & Control Client 1.0",
                    ( (Applet) (rmicfgclient)), 500, 400);
  }

  public void init() {
    _url.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        queryServer();
      }

    });
    ( (AbstractButton) (_qrySrv)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        queryServer();
      }

    });
    ( (AbstractButton) (_delSrv)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        delServer();
      }

    });
    ( (AbstractButton) (_newSrv)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        newServer();
      }

    });
    ( (AbstractButton) (_qryProp)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        queryProperties();
      }

    });
    ( (AbstractButton) (_apply)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        apply();
      }

    });
    ( (AbstractButton) (_start)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        srvStart();
      }

    });
    ( (AbstractButton) (_stop)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        srvStop();
      }

    });
    ( (AbstractButton) (_load)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        loadProp();
      }

    });
    ( (AbstractButton) (_save)).addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent) {
        saveProp();
      }

    });
    ListSelectionModel listselectionmodel = _srvTab.getSelectionModel();
    listselectionmodel.setSelectionMode(0);
    listselectionmodel.addListSelectionListener(new ListSelectionListener() {

      public void valueChanged(ListSelectionEvent listselectionevent) {
        if (!listselectionevent.getValueIsAdjusting()) {
          enableButtons();
          queryProperties();
        }
      }

    });
    JPanel jpanel = new JPanel( ( (java.awt.LayoutManager) (new GridBagLayout())));
    GridBagConstraints gridbagconstraints = new GridBagConstraints();
    ( (Container) (jpanel)).add( ( (java.awt.Component) (new JLabel("factory: "))),
                                ( (Object) (gridbagconstraints)));
    gridbagconstraints.fill = 2;
    gridbagconstraints.weightx = 100D;
    ( (Container) (jpanel)).add( ( (java.awt.Component) (_url)),
                                ( (Object) (gridbagconstraints)));
    gridbagconstraints.fill = 0;
    gridbagconstraints.weightx = 0.0D;
    ( (Container) (jpanel)).add( ( (java.awt.Component) (new JTianiButton(
        isApplet() ? ( (Applet)this).getAppletContext() : null))),
                                ( (Object) (gridbagconstraints)));
    JSplitPane jsplitpane = new JSplitPane(1,
                                           ( (java.awt.Component) (createFactoryPane())),
                                           ( (java.awt.Component) (
        createServerPane())));
    ( (JComponent) (jsplitpane)).setPreferredSize(new Dimension(300, 250));
    JSplitPane jsplitpane1 = new JSplitPane(0,
                                            ( (java.awt.Component) (jsplitpane)),
                                            ( (java.awt.Component) (new
        JAutoScrollPane( ( (JTextComponent) (_logTextArea))))));
    jsplitpane1.setOneTouchExpandable(true);
    Container container = ( (JApplet)this).getContentPane();
    container.add( ( (java.awt.Component) (jpanel)), "North");
    container.add( ( (java.awt.Component) (jsplitpane1)), "Center");
    String s = ( (Applet)this).getParameter("AutoLoad");
    if (s != null && s.length() > 0) {
      ( (JTextComponent) (_url)).setText(s);
      queryServer();
    }
    enableButtons();
    if (System.getSecurityManager() == null) {
      System.setSecurityManager( ( (SecurityManager) (new RMISecurityManager())));
    }
  }

  private JPanel createFactoryPane() {
    JPanel jpanel = new JPanel( ( (java.awt.LayoutManager) (new BorderLayout())));
    Box box = Box.createHorizontalBox();
    ( (Container) (box)).add( ( (java.awt.Component) (_qrySrv)));
    ( (Container) (box)).add( ( (java.awt.Component) (_newSrv)));
    ( (Container) (box)).add( ( (java.awt.Component) (_delSrv)));
    ( (Container) (jpanel)).add( ( (java.awt.Component) (box)), "North");
    ( (Container) (jpanel)).add( ( (java.awt.Component) (new JScrollPane( ( (
        java.awt.Component) (_srvTab))))), "Center");
    return jpanel;
  }

  private JPanel createServerPane() {
    JPanel jpanel = new JPanel( ( (java.awt.LayoutManager) (new BorderLayout())));
    Box box = Box.createHorizontalBox();
    ( (Container) (box)).add( ( (java.awt.Component) (_qryProp)));
    ( (Container) (box)).add( ( (java.awt.Component) (_apply)));
    ( (Container) (box)).add( ( (java.awt.Component) (_start)));
    ( (Container) (box)).add( ( (java.awt.Component) (_stop)));
    ( (Container) (box)).add( ( (java.awt.Component) (_abort)));
    ( (Container) (box)).add( ( (java.awt.Component) (_join)));
    ( (Container) (box)).add(Box.createHorizontalGlue());
    ( (Container) (box)).add( ( (java.awt.Component) (_load)));
    ( (Container) (box)).add( ( (java.awt.Component) (_save)));
    ( (Container) (jpanel)).add( ( (java.awt.Component) (box)), "North");
    ( (Container) (jpanel)).add( ( (java.awt.Component) (new JScrollPane( ( (
        java.awt.Component) (_propTab))))), "Center");
    return jpanel;
  }

  private boolean isApplet() {
    return ( (Applet)this).getDocumentBase() != null;
  }

  private void queryServer() {
    String s = ( (JTextComponent) (_url)).getText();
    _log.println("Query Server Factory - " + s);
    try {
      _factory = (IServerFactory) Naming.lookup(s);
      String as[] = _factory.listServerNames();
      _rows.clear();
      for (int i = 0; i < as.length; i++) {
        Row row = new Row();
        row._srvName = as[i];
        row._srv = _factory.getServer(as[i]);
        row._srvType = row._srv.getType();
        row._srvRun = row._srv.isRunning();
        _rows.addElement( ( (Object) (row)));
      }

      _srvTabModel.fireTableDataChanged();
      _classnames.setModel( ( (javax.swing.ComboBoxModel) (new
          DefaultComboBoxModel( ( (Object[]) (_factory.listClassNames()))))));
    }
    catch (SecurityException securityexception) {
      _log.println( ( (Object) (securityexception)));
      showPolicyFile();
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
    enableButtons();
  }

  private void newServer() {
    if (_factory == null) {
      return;
    }
    String s = JOptionPane.showInputDialog( ( (java.awt.Component) (this)),
                                           ( (Object) (_classnames)),
                                           "New Server", 3);
    if (s != null && s.length() > 0) {
      try {
        String s1 = (String) _classnames.getSelectedItem();
        _log.println("Create Server - " + s + ":" + s1);
        _factory.createServer(s1, s);
      }
      catch (Exception exception) {
        _log.println( ( (Object) (exception)));
      }
      queryServer();
      selectServer(s);
    }
    enableButtons();
  }

  private void selectServer(String s) {
    int i = _rows.size();
    for (int j = 0; j < i; j++) {
      Row row = (Row) _rows.elementAt(j);
      if (s.equals( ( (Object) (row._srvName)))) {
        _srvTab.setRowSelectionInterval(j, j);
        return;
      }
    }

  }

  private void delServer() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    try {
      Row row = (Row) _rows.elementAt(i);
      _log.println("Delete Server - " + row._srvName);
      _factory.removeServer(row._srvName);
      _rows.remove(i);
      _srvTabModel.fireTableRowsDeleted(i, i);
      _propTab.setModel( ( (javax.swing.table.TableModel) (EMPTY_PROP_TAB_MODEL)));
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
    enableButtons();
  }

  private void queryProperties() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      _propTab.setModel( ( (javax.swing.table.TableModel) (EMPTY_PROP_TAB_MODEL)));
      return;
    }
    try {
      Row row = (Row) _rows.elementAt(i);
      _log.println("Query Properties from Server - " + row._srvName);
      String as[] = row._srv.getPropertyNames();
      _properties = row._srv.getProperties();
      _propTab.setModel( ( (javax.swing.table.TableModel) (new
          PropertiesTableModel(as, _properties))));
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
  }

  private void apply() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    try {
      Row row = (Row) _rows.elementAt(i);
      _log.println("Set Properties of Server - " + row._srvName);
      row._srv.setProperties(_properties);
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
  }

  private void srvStart() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    try {
      Row row = (Row) _rows.elementAt(i);
      _log.println("Start Server - " + row._srvName);
      row._srv.start(_properties);
      row._srvRun = row._srv.isRunning();
      _srvTabModel.fireTableCellUpdated(i, 2);
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
    enableButtons();
  }

  private void srvStop() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    try {
      Row row = (Row) _rows.elementAt(i);
      _log.println("Stop Server - " + row._srvName);
      row._srv.stop( ( (AbstractButton) (_abort)).isSelected(),
                    ( (AbstractButton) (_join)).isSelected());
      row._srvRun = row._srv.isRunning();
      _srvTabModel.fireTableCellUpdated(i, 2);
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
    enableButtons();
  }

  private void loadProp() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    Row row = (Row) _rows.elementAt(i);
    File file = chooseFile(row._srvName + ".properties",
                           "Load Properties for " + row._srvName, "Load");
    if (file == null) {
      return;
    }
    try {
      _log.println("Load Properties - " + file);
      FileInputStream fileinputstream = new FileInputStream(file);
      try {
        ( (Hashtable) (_properties)).clear();
        _properties.load( ( (InputStream) (fileinputstream)));
        ( (AbstractTableModel) _propTab.getModel()).fireTableDataChanged();
      }
      finally {
        ( (InputStream) (fileinputstream)).close();
      }
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
  }

  private void saveProp() {
    int i = _srvTab.getSelectedRow();
    if (i == -1) {
      return;
    }
    Row row = (Row) _rows.elementAt(i);
    File file = chooseFile(row._srvName + ".properties",
                           "Save Properties of " + row._srvName, "Save");
    if (file == null) {
      return;
    }
    try {
      _log.println("Save Properties - " + file);
      FileOutputStream fileoutputstream = new FileOutputStream(file);
      try {
        _properties.store( ( (OutputStream) (fileoutputstream)),
                          "Properties for " + row._srvType);
      }
      finally {
        ( (OutputStream) (fileoutputstream)).close();
      }
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
  }

  private File chooseFile(String s, String s1, String s2) {
    try {
      if (_fileChooser == null) {
        _fileChooser = new JFileChooser(".");
        _fileChooser.setFileSelectionMode(0);
        _fileChooser.setFileFilter(FILE_FILTER);
      }
      _fileChooser.setDialogTitle(s1);
      _fileChooser.setSelectedFile(new File(s));
      int i = _fileChooser.showDialog( ( (java.awt.Component) (JOptionPane.
          getFrameForComponent( ( (java.awt.Component) (this))))), s2);
      File file = _fileChooser.getSelectedFile();
      return i != 0 ? null : file;
    }
    catch (SecurityException securityexception) {
      _log.println( ( (Object) (securityexception)));
    }
    showPolicyFile();
    return null;
  }

  private void showPolicyFile() {
    try {
      URL url = new URL( ( (Applet)this).getDocumentBase(),
                        ( (Applet)this).getParameter("PolicyFile"));
      ( (Applet)this).getAppletContext().showDocument(url, "_blank");
    }
    catch (Exception exception) {
      _log.println( ( (Object) (exception)));
    }
  }

  private void enableButtons() {
    int i = _srvTab.getSelectedRow();
    Row row = i == -1 ? null : (Row) _rows.elementAt(i);
    ( (AbstractButton) (_newSrv)).setEnabled(_factory != null);
    ( (AbstractButton) (_delSrv)).setEnabled(row != null && !row._srvRun);
    ( (AbstractButton) (_qryProp)).setEnabled(row != null);
    ( (AbstractButton) (_apply)).setEnabled(row != null);
    ( (AbstractButton) (_start)).setEnabled(row != null && !row._srvRun);
    ( (AbstractButton) (_stop)).setEnabled(row != null && row._srvRun);
    ( (AbstractButton) (_abort)).setEnabled(row != null && row._srvRun);
    ( (AbstractButton) (_join)).setEnabled(row != null && row._srvRun);
    ( (AbstractButton) (_load)).setEnabled(row != null);
    ( (AbstractButton) (_save)).setEnabled(row != null);
  }

}
