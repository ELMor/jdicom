// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AppletFrame.java

package com.tiani.dicom.ui;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.applet.AudioClip;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import javax.swing.JFrame;

public class AppletFrame extends JFrame
    implements AppletStub, AppletContext
{

    public AppletFrame(String s, Applet applet, int i, int j)
    {
        ((Frame)this).setTitle(s);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        ((Component)this).setLocation((dimension.width - i) / 2, (dimension.height - j) / 2);
        ((Component)this).setSize(i, j);
        ((Window)this).addWindowListener(((WindowListener) (new WindowAdapter() {

            public void windowClosing(WindowEvent windowevent)
            {
                System.exit(0);
            }

        })));
        Container container = ((JFrame)this).getContentPane();
        container.add(((Component) (applet)));
        applet.setStub(((AppletStub) (this)));
        applet.init();
        ((Window)this).show();
        applet.start();
    }

    public AppletFrame(Applet applet, int i, int j)
    {
        ((Frame)this).setTitle(((Object) (applet)).getClass().getName());
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        ((Component)this).setLocation((dimension.width - i) / 2, (dimension.height - j) / 2);
        ((Component)this).setSize(i, j);
        ((Window)this).addWindowListener(((WindowListener) (new WindowAdapter() {

            public void windowClosing(WindowEvent windowevent)
            {
                System.exit(0);
            }

        })));
        Container container = ((JFrame)this).getContentPane();
        container.add(((Component) (applet)));
        applet.setStub(((AppletStub) (this)));
        applet.init();
        ((Window)this).show();
        applet.start();
    }

    public AppletFrame(String s, Applet applet, int i, int j, WindowListener windowlistener)
    {
        ((Frame)this).setTitle(s);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        ((Component)this).setLocation((dimension.width - i) / 2, (dimension.height - j) / 2);
        ((Component)this).setSize(i, j);
        ((Window)this).addWindowListener(windowlistener);
        Container container = ((JFrame)this).getContentPane();
        container.add(((Component) (applet)));
        applet.setStub(((AppletStub) (this)));
        applet.init();
        ((Window)this).show();
        applet.start();
    }

    public boolean isActive()
    {
        return true;
    }

    public URL getDocumentBase()
    {
        return null;
    }

    public URL getCodeBase()
    {
        return null;
    }

    public String getParameter(String s)
    {
        return "";
    }

    public AppletContext getAppletContext()
    {
        return ((AppletContext) (this));
    }

    public void appletResize(int i, int j)
    {
    }

    public AudioClip getAudioClip(URL url)
    {
        return null;
    }

    public Image getImage(URL url)
    {
        return null;
    }

    public Applet getApplet(String s)
    {
        return null;
    }

    public Enumeration getApplets()
    {
        return null;
    }

    public void showDocument(URL url)
    {
    }

    public void showDocument(URL url, String s)
    {
    }

    public void showStatus(String s)
    {
    }

    public void setStream(String s, InputStream inputstream)
    {
    }

    public InputStream getStream(String s)
    {
        return null;
    }

    public Iterator getStreamKeys()
    {
        return null;
    }
}
