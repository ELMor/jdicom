// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   JSizeColumnsToFitTable.java

package com.tiani.dicom.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.io.PrintWriter;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class JSizeColumnsToFitTable extends JTable
{

    private int _columnExtraWidth;

    public JSizeColumnsToFitTable(TableModel tablemodel)
    {
        super(tablemodel);
        _columnExtraWidth = 0;
        ((JTable)this).setAutoResizeMode(0);
        sizeColumnWidthToFit();
    }

    public void tableChanged(TableModelEvent tablemodelevent)
    {
        super.tableChanged(tablemodelevent);
        if(super.defaultRenderersByColumnClass != null)
            sizeColumnWidthToFit();
    }

    public void sizeColumnWidthToFit()
    {
        ((JTable)this).getColumnModel().setColumnMargin(5);
        int i = ((JTable)this).getColumnCount();
        for(int j = 0; j < i; j++)
            _sizeColumnWidthToFit(j);

        ((JTable)this).setPreferredScrollableViewportSize(new Dimension(((JTable)this).getColumnModel().getTotalColumnWidth(), (1 + ((JTable)this).getRowCount()) * ((JTable)this).getRowHeight() + 5));
        ((JTable)this).getTableHeader().resizeAndRepaint();
        resizeAndRepaint();
    }

    public void toHTML(PrintWriter printwriter, String s)
    {
        _writeHTMLHeader(printwriter, s);
        int i = ((JTable)this).getRowCount();
        for(int j = 0; j < i; j++)
            _writeHTMLRow(printwriter, j);

        _writeHTMLFooter(printwriter);
    }

    private void _writeHTMLHeader(PrintWriter printwriter, String s)
    {
        printwriter.println("<HTML>");
        printwriter.print("<HEAD><TITLE>");
        printwriter.print(s);
        printwriter.println("</TITLE></HEAD>");
        printwriter.println("<BODY>");
        printwriter.println("<TABLE WIDTH=\"100%\" BORDER>");
        printwriter.print("<TR>");
        int i = ((JTable)this).getColumnCount();
        for(int j = 0; j < i; j++)
        {
            printwriter.print("<TH>");
            printwriter.print(((JTable)this).getColumnName(j));
            printwriter.print("</TH>");
        }

        printwriter.println("</TR>");
    }

    private void _writeHTMLRow(PrintWriter printwriter, int i)
    {
        printwriter.print("<TR>");
        int j = ((JTable)this).getColumnCount();
        for(int k = 0; k < j; k++)
        {
            printwriter.print("<TD>");
            printwriter.print(((JTable)this).getValueAt(i, k));
            printwriter.print("</TD>");
        }

        printwriter.println("</TR>");
    }

    private void _writeHTMLFooter(PrintWriter printwriter)
    {
        printwriter.println("</TABLE>");
        printwriter.println("</BODY>");
        printwriter.println("</HTML>");
    }

    private void _sizeColumnWidthToFit(int i)
    {
        TableColumn tablecolumn = ((JTable)this).getColumnModel().getColumn(i);
        if(tablecolumn == null)
            return;
        TableCellRenderer tablecellrenderer = tablecolumn.getHeaderRenderer();
        if(tablecellrenderer == null)
        {
            tablecellrenderer = ((JTable)this).getDefaultRenderer(java.lang.String.class);
            _columnExtraWidth = 5;
        }
        int j = _columnHeaderWidth(tablecellrenderer, tablecolumn);
        int k = ((JTable)this).getRowCount();
        for(int l = 0; l < k; l++)
            j = Math.max(_columnCellWidth(l, i), j);

        tablecolumn.setWidth(j);
        tablecolumn.setPreferredWidth(j);
    }

    private int _columnHeaderWidth(TableCellRenderer tablecellrenderer, TableColumn tablecolumn)
    {
        Component component = tablecellrenderer.getTableCellRendererComponent(((JTable) (this)), tablecolumn.getHeaderValue(), false, false, -1, tablecolumn.getModelIndex());
        return component.getPreferredSize().width + _columnExtraWidth;
    }

    private int _columnCellWidth(int i, int j)
    {
        Component component = ((JTable)this).getCellRenderer(i, j).getTableCellRendererComponent(((JTable) (this)), ((JTable)this).getValueAt(i, j), false, false, i, j);
        return component.getPreferredSize().width + _columnExtraWidth;
    }
}
