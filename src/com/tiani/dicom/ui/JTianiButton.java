// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JTianiButton.java

package com.tiani.dicom.ui;

import java.applet.AppletContext;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class JTianiButton extends JButton
{

    public JTianiButton(final AppletContext acx)
    {
        super(((javax.swing.Icon) (new ImageIcon((com.tiani.dicom.ui.JTianiButton.class).getResource("/com/tiani/dicom/icons/Tiani.gif")))));
        ((AbstractButton)this).setMargin(new Insets(0, 0, 0, 0));
        ((AbstractButton)this).setBorderPainted(false);
        ((AbstractButton)this).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    if(acx != null)
                        acx.showDocument(new URL("http://www.tiani.com"), "_blank");
                    else
                        try
                        {
                            Runtime.getRuntime().exec("start http://www.tiani.com");
                        }
                        catch(Exception exception)
                        {
                            Runtime.getRuntime().exec("cmd.exe /c start http://www.tiani.com");
                        }
                }
                catch(Exception exception1) { }
            }

        });
    }
}
