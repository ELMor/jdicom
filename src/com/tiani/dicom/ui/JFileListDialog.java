// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JFileListDialog.java

package com.tiani.dicom.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class JFileListDialog extends JDialog
{

    private boolean _ok;
    private JList _list;
    private JButton _okButton;
    private JButton _cancelButton;

    public JFileListDialog(Frame frame, String s)
    {
        super(frame, s, true);
        _ok = false;
        _list = new JList();
        _okButton = new JButton("OK");
        _cancelButton = new JButton("Cancel");
        ((AbstractButton) (_okButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _ok = true;
                setVisible(false);
            }

        });
        ((AbstractButton) (_cancelButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                setVisible(false);
            }

        });
        JPanel jpanel = new JPanel();
        ((Container) (jpanel)).add(((Component) (_okButton)));
        ((Container) (jpanel)).add(((Component) (_cancelButton)));
        ((JDialog)this).getContentPane().add(((Component) (new JScrollPane(((Component) (_list))))), "Center");
        ((JDialog)this).getContentPane().add(((Component) (jpanel)), "South");
        ((Component)this).setSize(400, 400);
        ((Component)this).setLocation(20, 20);
    }

    public File[] getSelectedFiles(File file)
    {
        Vector vector = listFiles(file);
        if(vector.isEmpty())
            return null;
        _list.setListData(vector);
        _list.setSelectionInterval(0, vector.size() - 1);
        _ok = false;
        ((Dialog)this).show();
        if(!_ok)
            return null;
        Object aobj[] = _list.getSelectedValues();
        File afile[] = new File[aobj.length];
        for(int i = 0; i < aobj.length; i++)
            afile[i] = (File)aobj[i];

        return afile;
    }

    private static Vector listFiles(File file)
    {
        Vector vector = new Vector();
        File afile[] = file.listFiles();
        for(int i = 0; i < afile.length; i++)
            if(afile[i].isFile())
                vector.add(((Object) (afile[i])));
            else
                vector.addAll(((java.util.Collection) (listFiles(afile[i]))));

        return vector;
    }

}
