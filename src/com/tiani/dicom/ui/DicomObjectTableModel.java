// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomObjectTableModel.java

package com.tiani.dicom.ui;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.util.Tag;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

// Referenced classes of package com.tiani.dicom.ui:
//            DicomObjectTableRow

public class DicomObjectTableModel extends AbstractTableModel
{

    private DicomObject _dicomObject;
    private boolean _editable;
    private boolean _showFileMetaInfo;
    private boolean _showSequenceItems;
    private Vector _rowdata;
    private static final String _HEADERS[] = {
        "Y/N", "Tag", "Attribute Name", "VR", "VM", "Value"
    };

    public DicomObjectTableModel(boolean flag, boolean flag1, boolean flag2)
    {
        _editable = false;
        _rowdata = new Vector();
        _dicomObject = new DicomObject();
        _showFileMetaInfo = flag;
        _showSequenceItems = flag1;
        _editable = flag2;
    }

    public DicomObjectTableModel()
    {
        this(true, true, false);
    }

    public DicomObjectTableModel(DicomObject dicomobject, boolean flag, boolean flag1, boolean flag2)
        throws DicomException
    {
        _editable = false;
        _rowdata = new Vector();
        Assert.notNull(((Object) (dicomobject)));
        _dicomObject = dicomobject;
        _showFileMetaInfo = flag;
        _showSequenceItems = flag1;
        _editable = flag2;
        DicomObject dicomobject1 = _dicomObject.getFileMetaInformation();
        if(_showFileMetaInfo && dicomobject1 != null)
            _initRowData(dicomobject1, ((DicomObjectTableRow) (null)), -1);
        _initRowData(_dicomObject, ((DicomObjectTableRow) (null)), -1);
    }

    public DicomObjectTableModel(DicomObject dicomobject)
        throws DicomException
    {
        this(dicomobject, true, true, false);
    }

    public void setEditable(boolean flag)
    {
        if(_editable == flag)
            return;
        _editable = flag;
        for(Enumeration enumeration = _rowdata.elements(); enumeration.hasMoreElements(); ((DicomObjectTableRow)enumeration.nextElement()).setEditable(_editable));
    }

    public void addFileMetaInfoTags(Tag atag[], boolean flag)
        throws DicomException
    {
        Object obj = ((Object) (_dicomObject.getFileMetaInformation()));
        if(obj == null)
        {
            obj = ((Object) (new FileMetaInformation(_dicomObject, "1.2.840.10008.1.2")));
            _dicomObject.setFileMetaInformation(((DicomObject) (obj)));
        }
        int i = _addTags(atag, ((DicomObject) (obj)), ((DicomObjectTableRow) (null)), -1, flag, 0);
        ((AbstractTableModel)this).fireTableRowsInserted(0, i);
    }

    public void addFileMetaInfoTags(Tag atag[])
        throws DicomException
    {
        addFileMetaInfoTags(atag, _editable);
    }

    public void addTags(Tag atag[], boolean flag)
        throws DicomException
    {
        int i = _rowdata.size();
        int j;
        for(j = 0; j < i && getRowAt(j).getDicomObject() != _dicomObject; j++);
        int k = _addTags(atag, _dicomObject, ((DicomObjectTableRow) (null)), -1, flag, j);
        ((AbstractTableModel)this).fireTableRowsInserted(j, k);
    }

    public void addTags(Tag atag[])
        throws DicomException
    {
        addTags(atag, _editable);
    }

    public DicomObject getDicomObject()
    {
        return _dicomObject;
    }

    public Class getColumnClass(int i)
    {
        return i != 0 ? java.lang.String.class : java.lang.Boolean.class;
    }

    public String getColumnName(int i)
    {
        return _HEADERS[i];
    }

    public int getColumnCount()
    {
        return _HEADERS.length;
    }

    public int getRowCount()
    {
        return _rowdata.size();
    }

    public DicomObjectTableRow getRowAt(int i)
    {
        return (DicomObjectTableRow)_rowdata.elementAt(i);
    }

    public Object getValueAt(int i, int j)
    {
        try
        {
            return getRowAt(i).getValueAt(j);
        }
        catch(DicomException dicomexception)
        {
            System.out.println("DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
        return ((Object) (null));
    }

    public boolean isCellEditable(int i, int j)
    {
        return getRowAt(i).isCellEditable(j);
    }

    public void setValueAt(Object obj, int i, int j)
    {
        try
        {
            switch(j)
            {
            default:
                break;

            case 0: // '\0'
                if(((Boolean)obj).booleanValue())
                    _setNullValueAt(i);
                else
                    _deleteValueAt(i);
                break;

            case 5: // '\005'
                getRowAt(i).setMultiValue((String)obj);
                ((AbstractTableModel)this).fireTableRowsUpdated(i, i);
                break;
            }
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
    }

    private void _setNullValueAt(int i)
        throws DicomException
    {
        DicomObjectTableRow dicomobjecttablerow = getRowAt(i);
        if(dicomobjecttablerow.isItem())
        {
            DicomObjectTableRow dicomobjecttablerow1 = dicomobjecttablerow.getParent();
            dicomobjecttablerow1.append(((Object) (new DicomObject())));
            int j = _rowdata.indexOf(((Object) (dicomobjecttablerow1)));
            ((AbstractTableModel)this).fireTableRowsUpdated(j, j);
            dicomobjecttablerow.incItemIndex();
            int k = _addSeqTags(dicomobjecttablerow1, _editable, i, dicomobjecttablerow.getItemIndex() - 1);
            ((AbstractTableModel)this).fireTableRowsInserted(i, k - 1);
            return;
        }
        dicomobjecttablerow.setNull();
        ((AbstractTableModel)this).fireTableRowsUpdated(i, i);
        if(dicomobjecttablerow.isTypeSQ())
        {
            _rowdata.insertElementAt(((Object) (new DicomObjectTableRow(((Tag) (null)), ((DicomObject) (null)), dicomobjecttablerow, 0, _editable))), i + 1);
            ((AbstractTableModel)this).fireTableRowsInserted(i + 1, i + 1);
        }
    }

    private void _deleteValueAt(int i)
        throws DicomException
    {
        DicomObjectTableRow dicomobjecttablerow = getRowAt(i);
        if(dicomobjecttablerow.isItem())
        {
            DicomObjectTableRow dicomobjecttablerow1 = dicomobjecttablerow.getParent();
            dicomobjecttablerow1.deleteItem(dicomobjecttablerow.getItemIndex());
            int k = _rowdata.indexOf(((Object) (dicomobjecttablerow1)));
            ((AbstractTableModel)this).fireTableRowsUpdated(k, k);
            int i1 = dicomobjecttablerow.getLevel();
            int k1 = i;
            int l1 = k1;
            DicomObjectTableRow dicomobjecttablerow2;
            do
            {
                _rowdata.removeElementAt(k1);
                l1++;
                dicomobjecttablerow2 = getRowAt(k1);
            } while(!dicomobjecttablerow2.isItem() || dicomobjecttablerow2.getLevel() > i1);
            ((AbstractTableModel)this).fireTableRowsDeleted(k1, l1 - 1);
            do
            {
                if(dicomobjecttablerow2.isItem() && dicomobjecttablerow2.getLevel() == i1)
                {
                    dicomobjecttablerow2.decItemIndex();
                    ((AbstractTableModel)this).fireTableRowsUpdated(k1, k1);
                    if(dicomobjecttablerow2.getDicomObject() == null)
                        return;
                }
                dicomobjecttablerow2 = getRowAt(++k1);
            } while(true);
        }
        dicomobjecttablerow.deleteItem();
        ((AbstractTableModel)this).fireTableRowsUpdated(i, i);
        if(dicomobjecttablerow.isTypeSQ())
        {
            int j = dicomobjecttablerow.getLevel();
            int l = i + 1;
            int j1;
            for(j1 = l; l < _rowdata.size() && getRowAt(l).getLevel() > j; j1++)
                _rowdata.removeElementAt(l);

            ((AbstractTableModel)this).fireTableRowsDeleted(l, j1 - 1);
        }
    }

    private void _initRowData(DicomObject dicomobject, DicomObjectTableRow dicomobjecttablerow, int i)
        throws DicomException
    {
        Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, true);
        for(int j = 0; enumeration.hasMoreElements(); j++)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            Tag tag = new Tag(tagvalue.getGroup(), tagvalue.getElement());
            DicomObjectTableRow dicomobjecttablerow1 = new DicomObjectTableRow(tag, dicomobject, dicomobjecttablerow, i, _editable);
            _rowdata.addElement(((Object) (dicomobjecttablerow1)));
            if(_showSequenceItems && tag.isTypeSQ())
            {
                int k = tagvalue.size();
                for(int l = 0; l < k; l++)
                {
                    DicomObject dicomobject1 = (DicomObject)tagvalue.getValue(l);
                    _rowdata.addElement(((Object) (new DicomObjectTableRow(((Tag) (null)), dicomobject1, dicomobjecttablerow1, l, _editable))));
                    _initRowData(dicomobject1, dicomobjecttablerow1, l);
                }

                _rowdata.addElement(((Object) (new DicomObjectTableRow(((Tag) (null)), ((DicomObject) (null)), dicomobjecttablerow1, k, _editable))));
            }
        }

    }

    private int _addTag(Tag tag, DicomObject dicomobject, DicomObjectTableRow dicomobjecttablerow, int i, boolean flag, int j)
        throws DicomException
    {
        DicomObjectTableRow dicomobjecttablerow1 = new DicomObjectTableRow(tag, dicomobject, dicomobjecttablerow, i, flag);
        int k = dicomobjecttablerow1.getLevel();
        int l = _rowdata.size();
        int i1 = -1;
        int j1;
        for(j1 = j; j1 < l; j1++)
        {
            DicomObjectTableRow dicomobjecttablerow2 = getRowAt(j1);
            if(dicomobjecttablerow2.getLevel() > k)
                continue;
            if(dicomobjecttablerow2.getDicomObject() != dicomobject)
                break;
            Tag tag1 = dicomobjecttablerow2.getTag();
            if(tag1 == null)
                continue;
            i1 = tag1.compareTo(tag);
            if(i1 >= 0)
                break;
        }

        if(i1 == 0)
            _rowdata.setElementAt(((Object) (dicomobjecttablerow1)), j1++);
        else
            _rowdata.insertElementAt(((Object) (dicomobjecttablerow1)), j1++);
        if(tag.isTypeSQ())
            j1 = _addSeqTags(dicomobjecttablerow1, flag, j1, 0);
        return j1;
    }

    private int _addTags(Tag atag[], DicomObject dicomobject, DicomObjectTableRow dicomobjecttablerow, int i, boolean flag, int j)
        throws DicomException
    {
        int k = j;
        for(int l = 0; l < atag.length; l++)
            k = _addTag(atag[l], dicomobject, dicomobjecttablerow, i, flag, k);

        return k;
    }

    private int _addSeqTags(DicomObjectTableRow dicomobjecttablerow, boolean flag, int i, int j)
        throws DicomException
    {
        Tag atag[] = dicomobjecttablerow.getTag().getSQTags();
        int k = i;
        int l = dicomobjecttablerow.getSize();
        int i1 = dicomobjecttablerow.getLevel() + 1;
        for(; j < l; j++)
        {
            DicomObject dicomobject = (DicomObject)dicomobjecttablerow.getValue(j);
            if(k == _rowdata.size() || getRowAt(k).getItemIndex() != j)
                _rowdata.insertElementAt(((Object) (new DicomObjectTableRow(((Tag) (null)), dicomobject, dicomobjecttablerow, j, flag))), k++);
            if(atag != null)
                k = _addTags(atag, dicomobject, dicomobjecttablerow, j, flag, k);
            for(; k < _rowdata.size(); k++)
            {
                DicomObjectTableRow dicomobjecttablerow1 = getRowAt(k);
                if(dicomobjecttablerow1.getLevel() <= i1 && dicomobjecttablerow1.getDicomObject() != dicomobject)
                    break;
            }

        }

        if(l >= 0 && (k == _rowdata.size() || getRowAt(k).getItemIndex() != l))
            _rowdata.insertElementAt(((Object) (new DicomObjectTableRow(((Tag) (null)), ((DicomObject) (null)), dicomobjecttablerow, l, flag))), k++);
        return k;
    }

}
