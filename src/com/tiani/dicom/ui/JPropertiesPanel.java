// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JPropertiesPanel.java

package com.tiani.dicom.ui;

import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.ExampleFileFilter;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;

// Referenced classes of package com.tiani.dicom.ui:
//            PropertiesTableModel, JSizeColumnsToFitTable

public class JPropertiesPanel extends JPanel
{

    public static final FileFilter FILE_FILTER = new ExampleFileFilter("properties", "Properties");
    private String _header;
    private Properties _properties;
    private Hashtable _constraints;
    private PropertiesTableModel _model;
    private JSizeColumnsToFitTable _table;
    private JFileChooser _fileChooser;
    private JButton _loadButton;
    private JButton _saveButton;

    public JPropertiesPanel(File file, String s, Properties properties, String as[], Hashtable hashtable)
    {
        this(file, s, properties, as, hashtable, new JButton[0]);
    }

    public JPropertiesPanel(File file, String s, Properties properties, String as[], Hashtable hashtable, JButton jbutton)
    {
        this(file, s, properties, as, hashtable, new JButton[] {
            jbutton
        });
    }

    public JPropertiesPanel(File file, String s, Properties properties, String as[], Hashtable hashtable, JButton ajbutton[])
    {
        super(((java.awt.LayoutManager) (new BorderLayout())));
        _properties = null;
        _fileChooser = new JFileChooser(".");
        _loadButton = new JButton("Load");
        _saveButton = new JButton("Save");
        _header = s;
        _properties = properties;
        _constraints = hashtable;
        _model = new PropertiesTableModel(as, properties);
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_model)));
        _fileChooser.setSelectedFile(file);
        _fileChooser.setFileFilter(FILE_FILTER);
        ((AbstractButton) (_loadButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file1 = _chooseFile("Load Properties", "Open");
                if(file1 != null)
                    _load(file1);
            }

        });
        ((AbstractButton) (_saveButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(!verifyParams())
                    return;
                File file1 = _chooseFile("Save Properties", "Save");
                if(file1 != null)
                    _save(file1);
            }

        });
        JPanel jpanel = new JPanel();
        ((Container) (jpanel)).add(((java.awt.Component) (_loadButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_saveButton)));
        for(int i = 0; i < ajbutton.length; i++)
            ((Container) (jpanel)).add(((java.awt.Component) (ajbutton[i])));

        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_table))))), "Center");
        ((Container)this).add(((java.awt.Component) (jpanel)), "South");
    }

    public JSizeColumnsToFitTable getTable()
    {
        return _table;
    }

    public PropertiesTableModel getModel()
    {
        return _model;
    }

    public boolean verifyParams()
    {
        try
        {
            CheckParam.verify(_properties, _constraints);
            return true;
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return false;
    }

    private File _chooseFile(String s, String s1)
    {
        _fileChooser.setDialogTitle(s);
        int i = _fileChooser.showDialog(((java.awt.Component) (this)), s1);
        File file = _fileChooser.getSelectedFile();
        return i != 0 ? null : file;
    }

    public static Properties load(File file, Hashtable hashtable)
    {
        try
        {
            System.out.println("Load Properties from " + file);
            FileInputStream fileinputstream = new FileInputStream(file);
            Properties properties = load(((InputStream) (fileinputstream)), hashtable);
            fileinputstream.close();
            return properties;
        }
        catch(IOException ioexception)
        {
            System.out.println("JPropertiesPanel.load(" + file + ")->Exception: " + ((Throwable) (ioexception)).getMessage());
        }
        return null;
    }

    public static Properties load(InputStream inputstream, Hashtable hashtable)
    {
        try
        {
            Properties properties = new Properties();
            properties.load(inputstream);
            CheckParam.verify(properties, hashtable);
            return properties;
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return null;
    }

    private void _load(File file)
    {
        Properties properties = load(file, _constraints);
        if(properties == null)
            return;
        ((Hashtable) (_properties)).clear();
        String s;
        for(Enumeration enumeration = ((Hashtable) (properties)).keys(); enumeration.hasMoreElements(); ((Hashtable) (_properties)).put(((Object) (s)), ((Hashtable) (properties)).get(((Object) (s)))))
            s = (String)enumeration.nextElement();

        ((AbstractTableModel) (_model)).fireTableDataChanged();
    }

    private void _save(File file)
    {
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            try
            {
                _properties.store(((java.io.OutputStream) (fileoutputstream)), _header);
                System.out.println("Save Properties to " + file);
            }
            finally
            {
                fileoutputstream.close();
            }
        }
        catch(IOException ioexception)
        {
            System.out.println("JPropertiesPanel.store(" + file + ")->Exception: " + ((Throwable) (ioexception)).getMessage());
        }
    }




}
