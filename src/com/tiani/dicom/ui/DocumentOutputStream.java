// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DocumentOutputStream.java

package com.tiani.dicom.ui;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class DocumentOutputStream extends OutputStream
{

    private byte buffer[];
    private int writePos;
    private Document doc;
    private AttributeSet a;
    private int maxLength;

    public DocumentOutputStream(Document document, int i, AttributeSet attributeset)
    {
        buffer = new byte[1024];
        writePos = 0;
        doc = document;
        maxLength = i;
        a = attributeset;
    }

    public DocumentOutputStream(Document document, int i)
    {
        this(document, i, ((AttributeSet) (null)));
    }

    public DocumentOutputStream(Document document)
    {
        this(document, 0x7fffffff, ((AttributeSet) (null)));
    }

    public final void write(int i)
        throws IOException
    {
        buffer[writePos] = (byte)i;
        if(++writePos == buffer.length)
            flush();
    }

    public void flush()
        throws IOException
    {
        try
        {
            int i = doc.getLength();
            int j = (i + writePos) - maxLength;
            if(j > 0)
            {
                doc.remove(0, j);
                i -= j;
            }
            doc.insertString(i, new String(buffer, 0, writePos), a);
        }
        catch(BadLocationException badlocationexception)
        {
            throw new IOException(((Throwable) (badlocationexception)).getMessage());
        }
        writePos = 0;
    }

    public void write(byte abyte0[], int i, int j)
        throws IOException
    {
        while(j-- > 0) 
            write(((int) (abyte0[i++])));
    }
}
