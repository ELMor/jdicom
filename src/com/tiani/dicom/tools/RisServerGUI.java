// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RisServerGUI.java

package com.tiani.dicom.tools;

import com.tiani.dicom.tools.risserver.JTreePanel;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesPanel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.ExampleFileFilter;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.tiani.dicom.tools:
//            RisServer

public class RisServerGUI extends JFrame
{

    private JFileChooser _fileChooser;
    private final FileFilter _HTMLFileFilter = new ExampleFileFilter("html", "HTML");
    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();
    private JSizeColumnsToFitTable _table;
    private JTreePanel _treePanel;
    private JButton _startButton;
    private JButton _stopButton;
    private JButton _saveHtmlButton;
    private JTabbedPane tabbedPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    private RisServer _server;

    public static void main(String args[])
    {
        RisServerGUI risservergui = new RisServerGUI(args);
        ((Window) (risservergui)).show();
    }

    public RisServerGUI(String as[])
    {
        _fileChooser = new JFileChooser(".");
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_NULL_MODEL)));
        _startButton = new JButton("Start");
        _stopButton = new JButton("Stop");
        _saveHtmlButton = new JButton("HTML");
        tabbedPane = new JTabbedPane(2);
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 30000))), true);
        _server = new RisServer();
        ((Frame)this).setTitle("DICOM RisServer 1.7.26");
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        ((Component)this).setSize(700, 500);
        ((Component)this).setLocation((dimension.width - 700) / 2, (dimension.height - 500) / 2);
        ((Window)this).addWindowListener(((java.awt.event.WindowListener) (new WindowAdapter() {

            public void windowClosing(WindowEvent windowevent)
            {
                System.exit(0);
            }

        })));
        ((AbstractButton) (_startButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _server.init(new Properties[] {
                    _server.getParams()
                });
                _server.start();
            }

        });
        ((AbstractButton) (_stopButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _server.stop();
            }

        });
        ((AbstractButton) (_saveHtmlButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                StringBuffer stringbuffer = new StringBuffer("Save ");
                File file = _treePanel.getCurFile();
                if(file != null)
                {
                    stringbuffer.append(file.getName());
                    _fileChooser.setSelectedFile(new File(file.getName() + ".html"));
                }
                stringbuffer.append(" as HTML File");
                File file1 = _chooseFile(stringbuffer.toString());
                if(file1 != null)
                    _saveHtml(file1, file == null ? "unknown source" : file.getName());
            }

        });
        _fileChooser.setFileSelectionMode(0);
        _fileChooser.setFileFilter(_HTMLFileFilter);
        JPanel jpanel = new JPanel();
        ((Container) (jpanel)).add(((Component) (_startButton)));
        ((Container) (jpanel)).add(((Component) (_stopButton)));
        ((Container) (jpanel)).add(((Component) (_saveHtmlButton)));
        JPanel jpanel1 = new JPanel(((java.awt.LayoutManager) (new BorderLayout())));
        ((Container) (jpanel1)).add(((Component) (new JTianiButton(((java.applet.AppletContext) (null))))), "East");
        ((Container) (jpanel1)).add(((Component) (jpanel)), "Center");
        ((JFrame)this).getContentPane().add(((Component) (jpanel1)), "North");
        System.setOut(_log);
        _server.parseCmdLine(as);
        JPropertiesPanel jpropertiespanel = new JPropertiesPanel(_server.getParamFile(), "Properties for " + ((Frame)this).getTitle(), _server.getParams(), RisServer.PARAM_NAMES, RisServer.PARAM_CHECKS);
        _treePanel = new JTreePanel(_server.getParams(), _table, _server.getRepository());
        tabbedPane.add("Props", ((Component) (new JScrollPane(((Component) (jpropertiespanel))))));
        JSplitPane jsplitpane = new JSplitPane(1, ((Component) (_treePanel)), ((Component) (new JScrollPane(((Component) (_table))))));
        tabbedPane.add("Result", ((Component) (jsplitpane)));
        tabbedPane.add("Log", ((Component) (new JAutoScrollPane(((JTextComponent) (_logTextArea))))));
        ((JFrame)this).getContentPane().add(((Component) (tabbedPane)), "Center");
    }

    private File _chooseFile(String s)
    {
        _fileChooser.setDialogTitle(s);
        int i = _fileChooser.showSaveDialog(((Component) (this)));
        File file = _fileChooser.getSelectedFile();
        return i != 0 ? null : file;
    }

    private void _saveHtml(File file, String s)
    {
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            PrintWriter printwriter = new PrintWriter(((java.io.OutputStream) (fileoutputstream)));
            try
            {
                _table.toHTML(printwriter, s);
            }
            finally
            {
                printwriter.close();
            }
            _log.println("Save as HTML to " + file);
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }






}
