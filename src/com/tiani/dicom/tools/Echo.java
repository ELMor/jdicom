// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Echo.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class Echo
{

    public static final int CECHORQ = 48;
    public static final int CECHORSP = 32816;
    public static final int NODATASET = 257;
    private static Echo _instance = new Echo();
    private Socket _sock;
    private Association _assoc;
    private Request _request;
    private Response _response;
    private int _messageID;

    public Echo()
    {
        _sock = null;
        _assoc = null;
        _request = new Request();
        _response = null;
        _messageID = 0;
    }

    public static void main(String args[])
    {
        if(args.length != 4)
        {
            System.out.println("Usage: Echo <calledAET> <callingAET> <host> <port>");
            return;
        }
        boolean flag = false;
        try
        {
            _instance.open(args[0], args[1], args[2], Integer.parseInt(args[3]));
            if(_instance._response instanceof Acknowledge)
            {
                System.out.println("Association accepted");
                flag = true;
                _instance.send();
                _instance.release();
                System.out.println("Association released");
                flag = false;
            } else
            {
                System.out.println("Association not accepted: " + _instance._response);
            }
        }
        catch(Exception exception)
        {
            if(flag)
                try
                {
                    _instance._assoc.sendAbort(0, 0);
                }
                catch(Exception exception1) { }
            System.out.println(((Object) (exception)));
        }
    }

    public void open(String s, String s1, String s2, int i)
        throws Exception
    {
        _request.setCalledTitle(s);
        _request.setCallingTitle(s1);
        int ai[] = {
            8193
        };
        _request.addPresentationContext(4097, ai);
        _sock = new Socket(s2, i);
        _assoc = new Association(_sock.getInputStream(), _sock.getOutputStream());
        _assoc.sendAssociateRequest(_request);
        _response = _assoc.receiveAssociateResponse();
    }

    public void send()
        throws IllegalValueException, UnknownUIDException, DicomException, IOException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(3, ((Object) (new Integer(48))));
        dicomobject.set(4, ((Object) (new Integer(++_messageID))));
        dicomobject.set(8, ((Object) (new Integer(257))));
        dicomobject.set(1, "1.2.840.10008.1.1");
        System.out.println("Send C-ECHO-RQ");
        _assoc.send(4097, dicomobject, ((DicomObject) (null)));
        DicomObject dicomobject1 = _assoc.receiveCommand();
        int i;
        if(dicomobject1.getI(3) != 32816)
            System.out.println("ERROR: Received Dicom Message is not a C-ECHO-RSP");
        else
        if((i = dicomobject1.getI(9)) != 0)
            System.out.println("ERROR: Received C-ECHO-RSP with Status - " + i);
        else
            System.out.println("Received C-ECHO-RSP");
    }

    public void release()
        throws Exception
    {
        _assoc.sendReleaseRequest();
        _assoc.receiveReleaseResponse();
        _sock.close();
    }

}
