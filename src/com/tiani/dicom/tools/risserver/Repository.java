// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Repository.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.Debug;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public final class Repository
{

    static final String MWL = "ModalityWorklist";
    static final String MPPS = "ModalityPPS";
    static final String SCN = "StudyContentNotification";
    private File _mwlDir;
    private File _mppsDir;
    private File _scnDir;

    public Repository()
    {
        _mwlDir = null;
        _mppsDir = null;
        _scnDir = null;
    }

    public void init(File file)
        throws IOException
    {
        if(!file.exists() || !file.isDirectory() || !file.canWrite())
        {
            throw new IOException(file + " is not a writeable directory!");
        } else
        {
            _mwlDir = new File(file, "ModalityWorklist");
            _mppsDir = new File(file, "ModalityPPS");
            _scnDir = new File(file, "StudyContentNotification");
            return;
        }
    }

    public File[] listMWLFiles()
    {
        return listFiles(_mwlDir);
    }

    public File[] listMPPSFiles()
    {
        return listFiles(_mppsDir);
    }

    public File[] listSCNFiles()
    {
        return listFiles(_scnDir);
    }

    public File createMWLFile(String s)
        throws IOException
    {
        return createFile(_mwlDir, s);
    }

    public File createMPPSFile(String s)
        throws IOException
    {
        return createFile(_mppsDir, s);
    }

    public File createSCNFile(String s)
        throws IOException
    {
        return createFile(_scnDir, s);
    }

    private static File[] listFiles(File file)
    {
        if(!file.exists())
            return new File[0];
        String as[] = file.list();
        File afile[] = new File[as.length];
        for(int i = 0; i < as.length; i++)
            afile[i] = new File(file, as[i]);

        return afile;
    }

    private File createFile(File file, String s)
        throws IOException
    {
        if(!file.exists())
        {
            if(!file.mkdir())
                throw new IOException("Creating " + file + " failed!");
            if(Debug.DEBUG > 0)
                Debug.out.println(file + "created");
        }
        return new File(file, s);
    }
}
