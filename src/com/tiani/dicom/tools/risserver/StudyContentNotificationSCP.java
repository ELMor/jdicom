// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StudyContentNotificationSCP.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.FileMetaInformation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

// Referenced classes of package com.tiani.dicom.tools.risserver:
//            Repository

public class StudyContentNotificationSCP extends DefCStoreSCP
{

    private Repository _repository;
    private boolean _validate;

    public StudyContentNotificationSCP(Repository repository)
    {
        _repository = repository;
    }

    protected int store(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        try
        {
            DicomObject dicomobject = dicommessage.getDataset();
            dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject))));
            File file = _repository.createSCNFile(s1);
            if(file.exists())
                System.out.println("WARNING: Received Basic Study Content Notification SOP instance [" + s1 + "] overwrites existing one");
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            try
            {
                dicomobject.write(((java.io.OutputStream) (fileoutputstream)), true);
            }
            finally
            {
                fileoutputstream.close();
            }
            if(Debug.DEBUG > 0)
                Debug.out.println("Store Basic Study Content Notification SOP instance to " + file);
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return 3;
    }
}
