// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JTreePanel.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.ModalityWorklist;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

// Referenced classes of package com.tiani.dicom.tools.risserver:
//            Repository

public final class JTreePanel extends JPanel
{
    private static class DirNode extends DefaultMutableTreeNode
    {

        private String _prompt;

        public String toString()
        {
            return _prompt;
        }

        public DirNode(String s, Tag atag[][])
        {
            super(((Object) (IOD.accumulate(atag))));
            _prompt = s;
        }
    }

    private static class FileNode extends DefaultMutableTreeNode
    {

        public String toString()
        {
            return ((File)super.userObject).getName();
        }

        public FileNode(File file)
        {
            super(((Object) (file)));
        }
    }


    private JButton _addButton;
    private JButton _delButton;
    private JButton _updateButton;
    private JButton _loadButton;
    private JButton _saveButton;
    private DefaultMutableTreeNode _root;
    private DirNode _mwlNode;
    private DirNode _mppsNode;
    private DirNode _scnNode;
    private JTree _tree;
    private DefaultTreeModel _model;
    private DefaultMutableTreeNode _selNode;
    private File _curFile;
    private Properties _params;
    private JSizeColumnsToFitTable _recordTable;
    private Repository _repository;
    private static final SimpleDateFormat _dateFormater = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeFormater = new SimpleDateFormat("HHmmss");
    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();

    public JTreePanel(Properties properties, JSizeColumnsToFitTable jsizecolumnstofittable, Repository repository)
    {
        super(((java.awt.LayoutManager) (new BorderLayout())));
        _addButton = new JButton("+");
        _delButton = new JButton("-");
        _updateButton = new JButton("?");
        _loadButton = new JButton(">>");
        _saveButton = new JButton("<<");
        _root = new DefaultMutableTreeNode("Repository");
        _mwlNode = new DirNode("ModalityWorklist", IOD.MODALITY_WORKLIST_ITEM);
        _mppsNode = new DirNode("ModalityPPS", IOD.MODALITY_WORKLIST_ITEM);
        _scnNode = new DirNode("StudyContentNotification", IOD.BASIC_STUDY_DESCRIPTOR);
        _tree = new JTree(((javax.swing.tree.TreeNode) (_root)));
        _model = (DefaultTreeModel)_tree.getModel();
        _selNode = null;
        _curFile = null;
        _repository = null;
        ((AbstractButton) (_loadButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                load();
                addIOD();
            }

        });
        ((AbstractButton) (_saveButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                save();
            }

        });
        ((AbstractButton) (_addButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                addTo((DirNode)(_selNode.getLevel() != 2 ? _selNode : _selNode.getParent()));
            }

        });
        ((AbstractButton) (_delButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                del(_selNode);
                _selNode = null;
                ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (JTreePanel._NULL_MODEL)));
            }

        });
        ((AbstractButton) (_updateButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                update();
            }

        });
        _tree.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent treeselectionevent)
            {
                TreePath treepath = treeselectionevent.getNewLeadSelectionPath();
                _selNode = null;
                if(treepath != null)
                {
                    _selNode = (DefaultMutableTreeNode)treepath.getLastPathComponent();
                    if(_selNode.getLevel() == 2)
                        load();
                }
                enableButtons();
            }

        });
        JPanel jpanel = new JPanel(((java.awt.LayoutManager) (new GridLayout(0, 3))));
        ((Container) (jpanel)).add(((java.awt.Component) (_updateButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_saveButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_loadButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_addButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_delButton)));
        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))));
        ((Container)this).add(((java.awt.Component) (jpanel)), "South");
        _tree.setVisibleRowCount(5);
        _root.add(((javax.swing.tree.MutableTreeNode) (_mwlNode)));
        _root.add(((javax.swing.tree.MutableTreeNode) (_mppsNode)));
        _root.add(((javax.swing.tree.MutableTreeNode) (_scnNode)));
        _params = properties;
        _recordTable = jsizecolumnstofittable;
        _repository = repository;
        update();
        enableButtons();
    }

    public File getCurFile()
    {
        return _curFile;
    }

    private void enableButtons()
    {
        int i = _selNode != null ? _selNode.getLevel() : -1;
        ((AbstractButton) (_addButton)).setEnabled(i > 0);
        ((AbstractButton) (_delButton)).setEnabled(i >= 0);
        ((AbstractButton) (_saveButton)).setEnabled(i > 1);
        ((AbstractButton) (_loadButton)).setEnabled(i > 1);
    }

    private void save()
    {
        try
        {
            DicomObjectTableModel dicomobjecttablemodel = (DicomObjectTableModel)((JTable) (_recordTable)).getModel();
            DicomObject dicomobject = dicomobjecttablemodel.getDicomObject();
            save(dicomobject, _curFile = (File)_selNode.getUserObject());
            ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (new DicomObjectTableModel(dicomobject, false, true, true))));
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    private static void save(DicomObject dicomobject, File file)
        throws IOException, DicomException
    {
        FileOutputStream fileoutputstream = new FileOutputStream(file);
        try
        {
            dicomobject.write(((java.io.OutputStream) (fileoutputstream)), dicomobject.getFileMetaInformation() != null);
        }
        finally
        {
            fileoutputstream.close();
        }
    }

    private void load()
    {
        try
        {
            _curFile = (File)_selNode.getUserObject();
            FileInputStream fileinputstream = new FileInputStream(_curFile);
            DicomObject dicomobject = new DicomObject();
            try
            {
                dicomobject.read(((java.io.InputStream) (fileinputstream)));
            }
            finally
            {
                fileinputstream.close();
            }
            DicomObjectTableModel dicomobjecttablemodel = new DicomObjectTableModel(dicomobject, false, true, true);
            ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (dicomobjecttablemodel)));
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    private void del(DefaultMutableTreeNode defaultmutabletreenode)
    {
        if(defaultmutabletreenode.getLevel() == 2)
        {
            File file = (File)defaultmutabletreenode.getUserObject();
            if(!file.delete())
                Debug.out.println("ERROR: Could not delete - " + file);
            else
                _model.removeNodeFromParent(((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode)));
        } else
        {
            for(int i = defaultmutabletreenode.getChildCount(); i-- > 0;)
                del((DefaultMutableTreeNode)defaultmutabletreenode.getChildAt(i));

        }
    }

    private void addTo(DirNode dirnode)
    {
        try
        {
            if(dirnode == _mppsNode)
                return;
            DicomObject dicomobject = new DicomObject();
            File file = dirnode != _mwlNode ? createSCN(dicomobject) : createMWLitem(dicomobject);
            save(dicomobject, file);
            FileNode filenode = new FileNode(file);
            _model.insertNodeInto(((javax.swing.tree.MutableTreeNode) (filenode)), ((javax.swing.tree.MutableTreeNode) (dirnode)), ((DefaultMutableTreeNode) (dirnode)).getChildCount());
            _tree.setSelectionPath(new TreePath(((Object []) (((DefaultMutableTreeNode) (filenode)).getPath()))));
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    private File createMWLitem(DicomObject dicomobject)
        throws DicomException, IOException
    {
        String s = UIDUtils.createID(8);
        Date date = new Date();
        DicomObject dicomobject1 = ModalityWorklist.createSPS(s, ((DateFormat) (_dateFormater)).format(date), ((DateFormat) (_timeFormater)).format(date), "OT", "StationAET", "ScheduledProcedureStepDescription");
        ModalityWorklist.initItem(dicomobject, "Patient^Name", UIDUtils.createID(8), UIDUtils.createID(8), UIDUtils.createUID(), UIDUtils.createID(8), "RequestedProcedureDescription", dicomobject1);
        return _repository.createMWLFile(s);
    }

    private File createSCN(DicomObject dicomobject)
        throws DicomException, IOException
    {
        String s = UIDUtils.createUID();
        dicomobject.set(62, "1.2.840.10008.1.9");
        dicomobject.set(63, ((Object) (s)));
        dicomobject.set(147, "Patient^Name");
        dicomobject.set(148, ((Object) (UIDUtils.createID(8))));
        dicomobject.set(427, ((Object) (UIDUtils.createID(8))));
        dicomobject.set(425, ((Object) (UIDUtils.createUID())));
        dicomobject.set(109, ((Object) (null)));
        dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject))));
        return _repository.createSCNFile(s);
    }

    private void addIOD()
    {
        try
        {
            DicomObjectTableModel dicomobjecttablemodel = (DicomObjectTableModel)((JTable) (_recordTable)).getModel();
            dicomobjecttablemodel.addTags((Tag[])((DefaultMutableTreeNode)_selNode.getParent()).getUserObject());
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    private void update()
    {
        try
        {
            _repository.init(new File(_params.getProperty("Repository.Path")));
            update(((DefaultMutableTreeNode) (_mwlNode)), _repository.listMWLFiles());
            update(((DefaultMutableTreeNode) (_mppsNode)), _repository.listMPPSFiles());
            update(((DefaultMutableTreeNode) (_scnNode)), _repository.listSCNFiles());
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        _model.nodeStructureChanged(((javax.swing.tree.TreeNode) (_mwlNode)));
        _model.nodeStructureChanged(((javax.swing.tree.TreeNode) (_mppsNode)));
        _model.nodeStructureChanged(((javax.swing.tree.TreeNode) (_scnNode)));
    }

    private static void update(DefaultMutableTreeNode defaultmutabletreenode, File afile[])
    {
        for(int i = defaultmutabletreenode.getChildCount(); i-- > 0;)
            defaultmutabletreenode.remove(i);

        for(int j = 0; j < afile.length; j++)
            defaultmutabletreenode.insert(((javax.swing.tree.MutableTreeNode) (new FileNode(afile[j]))), defaultmutabletreenode.getChildCount());

    }












}
