// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModalityPPSCreateSCP.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.tools.risserver:
//            Repository

public class ModalityPPSCreateSCP extends DefNCreateSCP
{

    private static final int INVALID_ATTRIBUTE_VALUE = 262;
    private static final int PROCESSING_FAILURE = 272;
    private static final int DUPLICATE_SOP_INSTANCE = 273;
    private static final int MISSING_ATTRIBUTE = 288;
    private static final int MISSING_ATTRIBUTE_VALUE = 289;
    private static final int _REQ_VALUE[] = {
        1210, 1206, 1198, 1201, 1202, 1205, 81
    };
    private static final int _REQ_ATTRIB[] = {
        1210, 1206, 1198, 1201, 1202, 1205, 81, 147, 148, 150, 
        152, 110, 1199, 1200, 1207, 1208, 96, 1203, 1204, 427, 
        1209, 1227
    };
    private static final int _REQ_VALUE_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE[] = {
        425
    };
    private static final int _REQ_ATTRIB_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE[] = {
        425, 107, 77, 589, 547, 582, 580
    };
    private Repository _repository;
    private boolean _validate;

    public ModalityPPSCreateSCP(Repository repository)
    {
        _repository = repository;
    }

    protected int create(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        try
        {
            DicomObject dicomobject = dicommessage.getDataset();
            if(s1 == null)
                ((DicomObject) (dicommessage1)).set(13, ((Object) (s1 = UIDUtils.createUID())));
            dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(s, s1))));
            File file = _repository.createMPPSFile(s1);
            if(file.exists())
            {
                Debug.out.println("ERROR: Duplicate SOP instance - " + s1);
                return 273;
            }
            int i = check(dicomobject, dicommessage1);
            switch(i)
            {
            case 0: // '\0'
                FileOutputStream fileoutputstream = new FileOutputStream(file);
                try
                {
                    dicomobject.write(((java.io.OutputStream) (fileoutputstream)), true);
                }
                finally
                {
                    fileoutputstream.close();
                }
                if(Debug.DEBUG > 0)
                    Debug.out.println("Create Modality PPS SOP instance - " + file);
                break;
            }
            return i;
        }
        catch(Exception exception)
        {
            Debug.out.println("ERROR: Processing failure - " + exception);
        }
        return 272;
    }

    private int check(DicomObject dicomobject, DicomMessage dicommessage)
    {
        Vector vector = checkMissingAttrib(dicomobject, _REQ_ATTRIB);
        Vector vector1 = checkMissingValue(dicomobject, _REQ_VALUE);
        int i = dicomobject.getSize(1210);
        switch(i)
        {
        case -1: 
            return 288;

        case 0: // '\0'
            return vector.isEmpty() ? '\u0121' : 288;
        }
        Vector avector[] = new Vector[i];
        Vector avector1[] = new Vector[i];
        for(int j = 0; j < i; j++)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(1210, j);
            avector[j] = checkMissingAttrib(dicomobject1, _REQ_ATTRIB_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE);
            avector1[j] = checkMissingValue(dicomobject1, _REQ_VALUE_SCHEDULED_STEP_ATTRIBUTE_SEQUENCE);
        }

        return vector.isEmpty() && !isAnyNotEmpty(avector) ? vector1.isEmpty() && !isAnyNotEmpty(avector1) ? '\0' : '\u0121' : 288;
    }

    private static boolean isAnyNotEmpty(Vector avector[])
    {
        int i = avector.length;
        for(int j = 0; j < i; j++)
            if(!avector[j].isEmpty())
                return true;

        return false;
    }

    private static Vector checkMissingValue(DicomObject dicomobject, int ai[])
    {
        Vector vector = new Vector();
        for(int i = 0; i < ai.length; i++)
            if(dicomobject.getSize(ai[i]) <= 0)
            {
                vector.addElement(((Object) (new Integer(ai[i]))));
                Debug.out.println("Missing attribute value: " + new Tag(DDict.getGroup(ai[i]), DDict.getElement(ai[i])));
            }

        return vector;
    }

    private static Vector checkMissingAttrib(DicomObject dicomobject, int ai[])
    {
        Vector vector = new Vector();
        for(int i = 0; i < ai.length; i++)
            if(dicomobject.getSize(ai[i]) < 0)
            {
                vector.addElement(((Object) (new Integer(ai[i]))));
                Debug.out.println("Missing attribute: " + new Tag(DDict.getGroup(ai[i]), DDict.getElement(ai[i])));
            }

        return vector;
    }

}
