// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModalityPPSSetSCP.java

package com.tiani.dicom.tools.risserver;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.util.Tag;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.tools.risserver:
//            Repository

public class ModalityPPSSetSCP extends DefNSetSCP
{

    private static final String IN_PROGRESS = "IN PROGRESS";
    private static final int PROCESSING_FAILURE = 272;
    private static final int NO_SUCH_ATTRIBUTE = 261;
    private static final int NO_SUCH_OBJECT_INSTANCE = 274;
    private static final int MISSING_ATTRIBUTE = 288;
    private static final int MISSING_ATTRIBUTE_VALUE = 289;
    private static final int MAY_NO_LONGER_BE_UPDATED = 42768;
    private static final int _REQ_NOT_ALLOWED[] = {
        1210, 1206, 1198, 1201, 1202, 147, 148, 150, 152, 110, 
        1199, 1200, 81, 427
    };
    private static final int _FINAL_REQ_VALUE[] = {
        1203, 1204, 1227
    };
    private static final int _FINAL_REQ_VALUE_PERFORMED_SERIES_SEQUENCE[] = {
        239, 426
    };
    private static final int _FINAL_REQ_ATTRIB_PERFORMED_SERIES_SEQUENCE[] = {
        100, 239, 102, 426, 97, 79
    };
    private Repository _repository;
    private boolean _validate;

    public ModalityPPSSetSCP(Repository repository)
    {
        _repository = repository;
    }

    protected int set(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
    {
        try
        {
            DicomObject dicomobject = dicommessage.getDataset();
            File file = _repository.createMPPSFile(s1);
            if(!file.exists())
            {
                Debug.out.println("ERROR: No Such object instance - " + s1);
                return 274;
            }
            DicomObject dicomobject1 = new DicomObject();
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                dicomobject1.read(((java.io.InputStream) (fileinputstream)), true);
            }
            finally
            {
                fileinputstream.close();
            }
            if(!"IN PROGRESS".equals(((Object) (dicomobject1.getS(1205)))))
            {
                Debug.out.println("ERROR: Performed Procedure Step Object may no longer be updated");
                ((DicomObject) (dicommessage1)).set(12, ((Object) (new Integer(42768))));
                return 272;
            }
            int i = set(dicomobject1, dicommessage, dicommessage1);
            switch(i)
            {
            case 0: // '\0'
            case 261: 
                if(!"IN PROGRESS".equals(((Object) (dicomobject1.getS(1205)))))
                    i = checkFinal(dicomobject1, i);
                break;
            }
            switch(i)
            {
            case 0: // '\0'
            case 261: 
                FileOutputStream fileoutputstream = new FileOutputStream(file);
                try
                {
                    dicomobject1.write(((java.io.OutputStream) (fileoutputstream)), true);
                }
                finally
                {
                    fileoutputstream.close();
                }
                if(Debug.DEBUG > 0)
                    Debug.out.println("Set Modality PPS SOP instance - " + file);
                break;
            }
            return i;
        }
        catch(Exception exception)
        {
            Debug.out.println("ERROR: Processing failure - " + exception);
        }
        return 272;
    }

    private static int set(DicomObject dicomobject, DicomMessage dicommessage, DicomMessage dicommessage1)
        throws DicomException
    {
        DicomObject dicomobject1 = dicommessage.getDataset();
        Vector vector = checkNotAllowAttrib(dicomobject1, _REQ_NOT_ALLOWED);
        Vector vector1 = new Vector();
        for(Enumeration enumeration = ((GroupList) (dicomobject1)).enumerateVRs(false, false); enumeration.hasMoreElements();)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            int i = tagvalue.getGroup();
            int j = tagvalue.getElement();
            if(dicomobject.getSize_ge(i, j) >= 0)
            {
                dicomobject.set_ge(i, j, ((Object) (null)));
                int k = tagvalue.size();
                for(int l = 0; l < k; l++)
                    dicomobject.append_ge(i, j, tagvalue.getValue(l));

            } else
            {
                Debug.out.println("WARNING: Try to set not-created attribute: " + new Tag(i, j));
                vector1.addElement(((Object) (tagvalue)));
            }
        }

        return !vector.isEmpty() || !vector1.isEmpty() ? 261 : '\0';
    }

    private static Vector checkNotAllowAttrib(DicomObject dicomobject, int ai[])
    {
        Vector vector = new Vector();
        for(int i = 0; i < ai.length; i++)
            if(dicomobject.getSize(ai[i]) >= 0)
            {
                dicomobject.deleteItem(ai[i]);
                vector.addElement(((Object) (new Integer(ai[i]))));
                Debug.out.println("WARNING: Try to set not-allowed attribute: " + new Tag(DDict.getGroup(ai[i]), DDict.getElement(ai[i])));
            }

        return vector;
    }

    private static int checkFinal(DicomObject dicomobject, int i)
        throws DicomException
    {
        Vector vector = checkMissingValue(dicomobject, _FINAL_REQ_VALUE);
        switch(dicomobject.getSize(1227))
        {
        case -1: 
            return 288;

        case 0: // '\0'
            return 289;
        }
        DicomObject dicomobject1 = (DicomObject)dicomobject.get(1227);
        Vector vector1 = checkMissingAttrib(dicomobject1, _FINAL_REQ_ATTRIB_PERFORMED_SERIES_SEQUENCE);
        Vector vector2 = checkMissingValue(dicomobject1, _FINAL_REQ_VALUE_PERFORMED_SERIES_SEQUENCE);
        return vector1.isEmpty() ? vector.isEmpty() && vector2.isEmpty() ? i : 289 : 288;
    }

    private static Vector checkMissingValue(DicomObject dicomobject, int ai[])
    {
        Vector vector = new Vector();
        for(int i = 0; i < ai.length; i++)
            if(dicomobject.getSize(ai[i]) <= 0)
            {
                vector.addElement(((Object) (new Integer(ai[i]))));
                Debug.out.println("Missing attribute value: " + new Tag(DDict.getGroup(ai[i]), DDict.getElement(ai[i])));
            }

        return vector;
    }

    private static Vector checkMissingAttrib(DicomObject dicomobject, int ai[])
    {
        Vector vector = new Vector();
        for(int i = 0; i < ai.length; i++)
            if(dicomobject.getSize(ai[i]) < 0)
            {
                vector.addElement(((Object) (new Integer(ai[i]))));
                Debug.out.println("Missing attribute: " + new Tag(DDict.getGroup(ai[i]), DDict.getElement(ai[i])));
            }

        return vector;
    }

}
