// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JTreePanel.java

package com.tiani.dicom.tools.queryretrievescu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

// Referenced classes of package com.tiani.dicom.tools.queryretrievescu:
//            QueryResult, JFilterPanel

public final class JTreePanel extends JPanel
{

    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();
    private DefaultMutableTreeNode _root;
    private JTree _tree;
    private DefaultTreeModel _model;
    private DefaultMutableTreeNode _selNode;
    private JButton _delButton;
    private JButton _clearButton;
    private JFilterPanel _filter;
    private JButton _findButton;
    private JButton _moveButton;
    private JSizeColumnsToFitTable _recordTable;

    public JTreePanel(JButton jbutton, JButton jbutton1, JSizeColumnsToFitTable jsizecolumnstofittable, JFilterPanel jfilterpanel)
    {
        super(((java.awt.LayoutManager) (new BorderLayout())));
        _root = new DefaultMutableTreeNode("Query Results");
        _tree = new JTree(((javax.swing.tree.TreeNode) (_root)));
        _model = (DefaultTreeModel)_tree.getModel();
        _selNode = null;
        _delButton = new JButton("-");
        _clearButton = new JButton("C");
        _filter = null;
        _findButton = null;
        _moveButton = null;
        _recordTable = null;
        _findButton = jbutton;
        _moveButton = jbutton1;
        _recordTable = jsizecolumnstofittable;
        _filter = jfilterpanel;
        ((AbstractButton) (_delButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(_selNode == _root)
                    clear();
                else
                    _model.removeNodeFromParent(((javax.swing.tree.MutableTreeNode) (_selNode)));
                _selNode = null;
                ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (JTreePanel._NULL_MODEL)));
                enableButtons(0);
            }

        });
        ((AbstractButton) (_clearButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                clear();
                _selNode = null;
                ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (JTreePanel._NULL_MODEL)));
                enableButtons(0);
            }

        });
        _tree.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent treeselectionevent)
            {
                TreePath treepath = treeselectionevent.getNewLeadSelectionPath();
                DicomObjectTableModel dicomobjecttablemodel = JTreePanel._NULL_MODEL;
                _selNode = null;
                int i = treepath == null ? 0 : treepath.getPathCount() - 1;
                if(i > 0)
                {
                    _selNode = (DefaultMutableTreeNode)treepath.getLastPathComponent();
                    try
                    {
                        QueryResult queryresult = (QueryResult)_selNode.getUserObject();
                        dicomobjecttablemodel = new DicomObjectTableModel(queryresult.getDataset());
                    }
                    catch(DicomException dicomexception) { }
                }
                ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (dicomobjecttablemodel)));
                enableButtons(i);
            }

        });
        JPanel jpanel = new JPanel();
        ((Container) (jpanel)).add(((java.awt.Component) (_delButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (_clearButton)));
        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))), "Center");
        ((Container)this).add(((java.awt.Component) (jpanel)), "South");
    }

    public JTree getTree()
    {
        return _tree;
    }

    public DicomObject getFindIdentifer(int i)
        throws DicomException
    {
        Assert.isTrue(i == 4132 || i == 4135 || i == 4138);
        int j = _selNode == null ? 0 : _selNode.getLevel();
        if(i == 4138 && j > 1)
            throw new DicomException("Cannot perform " + JFilterPanel.QR_LEVEL[j] + " level query using Patient Study Only Query Retrieve Information Model FIND SOP Class");
        else
            return i != 4135 ? getFindFilter(j) : getStudyRootFindFilter(j);
    }

    private DicomObject getFindFilter(int i)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        copyDicomObject(_filter.getFilter(i), dicomobject);
        dicomobject.set(78, ((Object) (JFilterPanel.QR_LEVEL[i])));
        if(i > 0)
            addUniqueKeys(_selNode, dicomobject);
        return dicomobject;
    }

    private DicomObject getStudyRootFindFilter(int i)
        throws DicomException
    {
        if(i > 1)
        {
            DicomObject dicomobject = getFindFilter(i);
            dicomobject.deleteItem(148);
            return dicomobject;
        }
        DicomObject dicomobject1 = new DicomObject();
        copyDicomObject(_filter.getFilter(0), dicomobject1);
        copyDicomObject(_filter.getFilter(1), dicomobject1);
        dicomobject1.deleteItem(453);
        dicomobject1.deleteItem(454);
        dicomobject1.deleteItem(455);
        dicomobject1.set(78, "STUDY");
        if(i > 0)
            addUniqueKeys(_selNode, dicomobject1);
        return dicomobject1;
    }

    private void addUniqueKeys(DefaultMutableTreeNode defaultmutabletreenode, DicomObject dicomobject)
        throws DicomException
    {
        DefaultMutableTreeNode defaultmutabletreenode1 = defaultmutabletreenode;
        for(int i = defaultmutabletreenode1.getLevel(); i-- > 0;)
        {
            QueryResult queryresult = (QueryResult)defaultmutabletreenode1.getUserObject();
            dicomobject.set(QueryResult.UNIQUE_KEYS[i], ((Object) (queryresult.getUniqueKey())));
            defaultmutabletreenode1 = (DefaultMutableTreeNode)defaultmutabletreenode1.getParent();
        }

    }

    public DicomObject getMoveIdentifer(int i)
        throws DicomException
    {
        Assert.isTrue(i == 4133 || i == 4136 || i == 4139);
        int j = _selNode.getLevel() - 1;
        if(i == 4139 && j > 1)
            throw new DicomException("Cannot perform " + JFilterPanel.QR_LEVEL[j] + " level retrieve using Patient Study Only Query Retrieve Information Model MOVE SOP Class");
        if(i == 4136 && j == 0)
            throw new DicomException("Cannot perform " + JFilterPanel.QR_LEVEL[j] + " level retrieve using Study Root Query Retrieve Information Model MOVE SOP Class");
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(78, ((Object) (JFilterPanel.QR_LEVEL[j])));
        addUniqueKeys(_selNode, dicomobject);
        if(i == 4136)
            dicomobject.deleteItem(148);
        return dicomobject;
    }

    private static void copyDicomObject(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements(); copyTagValue((TagValue)enumeration.nextElement(), dicomobject1));
    }

    private static void copyTagValue(TagValue tagvalue, DicomObject dicomobject)
        throws DicomException
    {
        int i = tagvalue.getGroup();
        int j = tagvalue.getElement();
        dicomobject.deleteItem_ge(i, j);
        int k = tagvalue.size();
        if(k > 0)
        {
            for(int l = 0; l < k; l++)
                dicomobject.append_ge(i, j, tagvalue.getValue(l));

        } else
        {
            dicomobject.set_ge(i, j, ((Object) (null)));
        }
    }

    public void addQueryResult(DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject.getSize(78) <= 0)
            throw new DicomException("Missing Attribute Query Retrieve Level in C-FIND-RSP");
        String s = dicomobject.getS(78);
        int i = JFilterPanel.levelToInt(s);
        if(i == -1)
            throw new DicomException(s + "is not a valid value for Attribute Query Retrieve Level in C-FIND-RSP");
        if(i == 0)
        {
            addQueryResult(_root, new QueryResult(dicomobject, 0), true);
            return;
        }
        DefaultMutableTreeNode defaultmutabletreenode = getStudyNode(dicomobject, i == 1);
        for(int j = 2; j <= i; j++)
            defaultmutabletreenode = addQueryResult(defaultmutabletreenode, new QueryResult(dicomobject, j), i == j);

    }

    private DefaultMutableTreeNode getStudyNode(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        if(dicomobject.getSize(425) <= 0)
            throw new DicomException("Missing Attribute Study Instance UID in C-FIND-RSP");
        QueryResult queryresult = new QueryResult(dicomobject, 1);
        int i = _root.getChildCount();
        for(int j = 0; j < i; j++)
        {
            DefaultMutableTreeNode defaultmutabletreenode = getChildNode((DefaultMutableTreeNode)_root.getChildAt(j), queryresult, flag);
            if(defaultmutabletreenode != null)
                return defaultmutabletreenode;
        }

        DefaultMutableTreeNode defaultmutabletreenode1 = addQueryResult(_root, new QueryResult(dicomobject, 0), false);
        return addQueryResult(defaultmutabletreenode1, queryresult, flag);
    }

    private DefaultMutableTreeNode getChildNode(DefaultMutableTreeNode defaultmutabletreenode, QueryResult queryresult, boolean flag)
    {
        int i = defaultmutabletreenode.getChildCount();
        for(int j = 0; j < i; j++)
        {
            DefaultMutableTreeNode defaultmutabletreenode1 = (DefaultMutableTreeNode)defaultmutabletreenode.getChildAt(j);
            if(queryresult.equals(defaultmutabletreenode1.getUserObject()))
            {
                if(flag)
                {
                    defaultmutabletreenode1.setUserObject(((Object) (queryresult)));
                    _model.nodeChanged(((javax.swing.tree.TreeNode) (defaultmutabletreenode1)));
                }
                return defaultmutabletreenode1;
            }
        }

        return null;
    }

    private DefaultMutableTreeNode addQueryResult(DefaultMutableTreeNode defaultmutabletreenode, QueryResult queryresult, boolean flag)
        throws DicomException
    {
        DefaultMutableTreeNode defaultmutabletreenode1 = getChildNode(defaultmutabletreenode, queryresult, flag);
        if(defaultmutabletreenode1 == null)
        {
            defaultmutabletreenode1 = new DefaultMutableTreeNode(((Object) (queryresult)));
            _model.insertNodeInto(((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode1)), ((javax.swing.tree.MutableTreeNode) (defaultmutabletreenode)), defaultmutabletreenode.getChildCount());
            _tree.expandPath(new TreePath(((Object []) (defaultmutabletreenode.getPath()))));
        }
        return defaultmutabletreenode1;
    }

    private void clear()
    {
        _root.removeAllChildren();
        _model.nodeStructureChanged(((javax.swing.tree.TreeNode) (_root)));
    }

    public void enableButtons(int i)
    {
        ((AbstractButton) (_findButton)).setEnabled(i < 4);
        ((AbstractButton) (_moveButton)).setEnabled(i > 0);
        ((AbstractButton) (_delButton)).setEnabled(i > 0);
    }








}
