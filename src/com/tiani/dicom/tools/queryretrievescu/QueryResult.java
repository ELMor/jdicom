// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   QueryResult.java

package com.tiani.dicom.tools.queryretrievescu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

class QueryResult
{

    public static final int LABELS[] = {
        147, 64, 428, 430
    };
    public static final int UNIQUE_KEYS[] = {
        148, 425, 426, 63
    };
    private DicomObject _dataset;
    private int _level;
    private String _label;
    private String _uniqueKey;

    private String nullStr(String s)
    {
        return s != null ? s : "";
    }

    public QueryResult(DicomObject dicomobject, int i)
        throws DicomException
    {
        _dataset = dicomobject;
        _level = i;
        _uniqueKey = nullStr(_dataset.getS(UNIQUE_KEYS[i]));
        switch(i)
        {
        case 0: // '\0'
            _label = nullStr(_dataset.getS(147));
            break;

        case 1: // '\001'
            _label = nullStr(_dataset.getS(64));
            break;

        case 2: // '\002'
            _label = nullStr(_dataset.getS(81)) + " #" + nullStr(_dataset.getS(428));
            break;

        case 3: // '\003'
            _label = "#" + nullStr(_dataset.getS(430));
            break;
        }
    }

    public String toString()
    {
        return _label;
    }

    public int hashCode()
    {
        return _uniqueKey.hashCode();
    }

    public boolean equals(Object obj)
    {
        if(obj.getClass() != (com.tiani.dicom.tools.queryretrievescu.QueryResult.class))
            return false;
        QueryResult queryresult = (QueryResult)obj;
        if(_uniqueKey.length() > 0)
            return _uniqueKey.equals(((Object) (queryresult._uniqueKey)));
        if(_label.length() > 0)
            return _label.equals(((Object) (queryresult._label)));
        else
            return queryresult._uniqueKey.length() == 0 && queryresult._label.length() == 0;
    }

    public String getUniqueKey()
    {
        return _uniqueKey;
    }

    public DicomObject getDataset()
    {
        return _dataset;
    }

}
