// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JFilterPanel.java

package com.tiani.dicom.tools.queryretrievescu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public final class JFilterPanel extends JTabbedPane
{

    public static final String QR_LEVEL[] = {
        "PATIENT", "STUDY", "SERIES", "IMAGE"
    };
    private DicomObject _filter[];
    private static final int _PAT_INIT_KEYS[] = {
        147, 148, 152, 150, 151, 154, 155, 170, 177, 453, 
        454, 455
    };
    private static final int _STUDY_INIT_KEYS[] = {
        64, 70, 77, 427, 425, 88, 95, 99, 101, 103, 
        157, 158, 159, 171, 173, 82, 456, 457
    };
    private static final int _SERIES_INIT_KEYS[] = {
        81, 428, 426, 458
    };
    private static final int _IMAGE_INIT_KEYS[] = {
        430, 63
    };
    private static final int _RECORD_INIT_KEYS[][] = {
        _PAT_INIT_KEYS, _STUDY_INIT_KEYS, _SERIES_INIT_KEYS, _IMAGE_INIT_KEYS
    };
    private static final Tag _PAT_TAGS[];
    private static final Tag _STUDY_TAGS[];
    private static final Tag _SERIES_TAGS[];
    private static final Tag _IMAGE_TAGS[];
    private static final Tag _RECORD_TAGS[][];

    public static final int levelToInt(String s)
    {
        for(int i = QR_LEVEL.length; --i >= 0;)
            if(QR_LEVEL[i].equals(((Object) (s))))
                return i;

        return -1;
    }

    public JFilterPanel()
    {
        _filter = new DicomObject[4];
        try
        {
            for(int i = 0; i < QR_LEVEL.length; i++)
            {
                _filter[i] = createFilter(i);
                DicomObjectTableModel dicomobjecttablemodel = new DicomObjectTableModel(_filter[i], false, true, true);
                dicomobjecttablemodel.addTags(_RECORD_TAGS[i]);
                ((JTabbedPane)this).add(QR_LEVEL[i], ((java.awt.Component) (new JScrollPane(((java.awt.Component) (new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (dicomobjecttablemodel)))))))));
            }

        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).getMessage());
        }
    }

    public DicomObject getFilter(int i)
    {
        return _filter[i];
    }

    private static DicomObject createFilter(int i)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        int ai[] = _RECORD_INIT_KEYS[i];
        for(int j = 0; j < ai.length; j++)
            dicomobject.set(ai[j], ((Object) (null)));

        return dicomobject;
    }

    static 
    {
        _PAT_TAGS = IOD.accumulate(new Tag[][] {
            IOD.PATIENT_MODULE, new Tag[] {
                new Tag(32, 4608), new Tag(32, 4610), new Tag(32, 4612)
            }
        });
        _STUDY_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_STUDY_MODULE, IOD.PATIENT_STUDY_MODULE, new Tag[] {
                new Tag(8, 97), new Tag(32, 4614), new Tag(32, 4616)
            }
        });
        _SERIES_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_SERIES_MODULE, new Tag[] {
                new Tag(32, 4617)
            }
        });
        _IMAGE_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_IMAGE_MODULE, IOD.MULTIFRAME_MODULE, IOD.IMAGE_PIXEL_MODULE, IOD.PRESENTATION_STATE_MODULE, IOD.SR_DOCUMENT_GENERAL_MODULE, IOD.SR_DOCUMENT_CONTENT_MODULE
        });
        _RECORD_TAGS = (new Tag[][] {
            _PAT_TAGS, _STUDY_TAGS, _SERIES_TAGS, _IMAGE_TAGS
        });
    }
}
