// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UserObject.java

package com.tiani.dicom.tools.modalityscu;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

class UserObject
{

    public static final int WL_KEY[] = {
        587, 582
    };
    public static final int WL_LABELS[][] = {
        {
            147
        }, {
            77
        }, {
            589
        }, {
            587, 582
        }
    };
    public static final int PPS_LABELS[][] = {
        {
            147
        }, {
            1210, 77
        }, {
            1210, 589
        }, {
            1206
        }
    };
    private DicomObject _dataset;
    private int _labels[][];

    public UserObject(DicomObject dicomobject, int ai[][])
        throws DicomException
    {
        _dataset = dicomobject;
        _labels = ai;
    }

    public DicomObject getDataset()
    {
        return _dataset;
    }

    public void setDataset(DicomObject dicomobject)
    {
        _dataset = dicomobject;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(getS(_dataset, _labels[0]));
        stringbuffer.append('|');
        stringbuffer.append(getS(_dataset, _labels[1]));
        stringbuffer.append('|');
        stringbuffer.append(getS(_dataset, _labels[2]));
        stringbuffer.append('|');
        stringbuffer.append(getS(_dataset, _labels[3]));
        return stringbuffer.toString();
    }

    public boolean equalsPatient(UserObject userobject)
    {
        if(_dataset.getSize(148) > 0)
            return _dataset.get(148).equals(userobject._dataset.get(148));
        if(userobject._dataset.getSize(148) > 0)
            return false;
        if(_dataset.getSize(147) > 0)
            return _dataset.get(147).equals(userobject._dataset.get(147));
        else
            return userobject._dataset.getSize(148) <= 0;
    }

    public boolean equalsWLitem(UserObject userobject)
        throws DicomException
    {
        Object obj = get(_dataset, WL_KEY);
        return obj != null && obj.equals(get(userobject._dataset, WL_KEY));
    }

    private Object get(DicomObject dicomobject, int ai[])
        throws DicomException
    {
        Object obj = ((Object) (dicomobject));
        for(int i = 0; obj != null && i < ai.length; i++)
            obj = ((DicomObject)obj).get(ai[i]);

        return obj;
    }

    private String getS(DicomObject dicomobject, int ai[])
    {
        try
        {
            return get(dicomobject, ai).toString();
        }
        catch(Exception exception)
        {
            return "";
        }
    }

}
