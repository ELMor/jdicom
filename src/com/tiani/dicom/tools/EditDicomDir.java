// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EditDicomDir.java

package com.tiani.dicom.tools;

import com.archimed.dicom.DDate;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.Person;
import com.archimed.dicom.TagValue;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DicomDir;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.media.FileSet;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JFileListDialog;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.IOD;
import com.tiani.dicom.util.Tag;
import com.tiani.dicom.util.UIDUtils;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

// Referenced classes of package com.tiani.dicom.tools:
//            MyTreeCellRenderer

public class EditDicomDir extends JApplet
{
    private class MyTreeNode extends DefaultMutableTreeNode
    {

        public String toString()
        {
            int i = ((DefaultMutableTreeNode)this).getLevel();
            if(i > 0)
                try
                {
                    DicomObject dicomobject = (DicomObject)((DefaultMutableTreeNode)this).getUserObject();
                    String s = dicomobject.getS(LABELS[i - 1]);
                    return this != _lastRecNode ? s : s + "*";
                }
                catch(DicomException dicomexception) { }
            return super.toString();
        }

        public void addDR()
            throws DicomException
        {
            int i = ((DefaultMutableTreeNode)this).getLevel();
            DicomObject dicomobject = _createInitRec(i);
            if(i > 0)
                _dirInfo.addLowerDR(dicomobject, (DicomObject)((DefaultMutableTreeNode)this).getUserObject());
            else
                _dirInfo.addRootDR(dicomobject);
            MyTreeNode mytreenode = _lastRecNode;
            _lastRecNode = new MyTreeNode(((Object) (dicomobject)));
            int j = ((DefaultMutableTreeNode)this).getChildCount();
            _treeModel.insertNodeInto(((javax.swing.tree.MutableTreeNode) (_lastRecNode)), ((javax.swing.tree.MutableTreeNode) (this)), j);
            if(mytreenode != null)
                _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (mytreenode)));
            _tree.setSelectionPath(new TreePath(((Object []) (((DefaultMutableTreeNode)((DefaultMutableTreeNode)this).getChildAt(j)).getPath()))));
        }

        public Vector deleteDR()
            throws DicomException
        {
            DicomObject dicomobject = (DicomObject)((DefaultMutableTreeNode)this).getUserObject();
            Vector vector = _dirInfo.deleteDR(dicomobject);
            _treeModel.removeNodeFromParent(((javax.swing.tree.MutableTreeNode) (this)));
            return vector;
        }

        public void replaceDR()
            throws DicomException
        {
            DicomObject dicomobject = (DicomObject)((DefaultMutableTreeNode)this).getUserObject();
            DicomObject dicomobject1 = EditDicomDir._cloneDicomObject(dicomobject);
            _dirInfo.replaceDR(dicomobject, dicomobject1);
            ((DefaultMutableTreeNode)this).setUserObject(((Object) (dicomobject1)));
            int i = ((DefaultMutableTreeNode)this).getLevel();
            DicomObjectTableModel dicomobjecttablemodel = new DicomObjectTableModel(dicomobject1);
            ((AbstractTableModel) (dicomobjecttablemodel)).addTableModelListener(new TableModelListener() {

                public void tableChanged(TableModelEvent tablemodelevent)
                {
                    _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (_selNode)));
                }

            });
            ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (dicomobjecttablemodel)));
            MyTreeNode mytreenode = _lastRecNode;
            _lastRecNode = this;
            if(mytreenode != null)
                _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (mytreenode)));
            _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (this)));
        }


        public MyTreeNode(Object obj)
        {
            super(obj);
        }
    }


    private static final Tag _EDITABLE_META_INFO_TAGS[] = {
        new Tag(2, 2), new Tag(2, 3), new Tag(2, 16), new Tag(2, 18), new Tag(2, 19), new Tag(2, 22), new Tag(2, 256), new Tag(2, 258)
    };
    private static final Tag _EDITABLE_DICOMDIR_ID_TAGS[] = {
        new Tag(4, 4400), new Tag(4, 4417), new Tag(4, 4418)
    };
    private static final Tag _EDITABLE_PAT_TAGS[];
    private static final Tag _EDITABLE_STUDY_TAGS[];
    private static final Tag _EDITABLE_SERIES_TAGS[];
    private static final Tag _EDITABLE_IMAGE_TAGS[];
    private static final Tag _EDITABLE_RECORD_TAGS[][];
    private static final SimpleDateFormat _dateToString = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat _timeToString = new SimpleDateFormat("HHmmss");
    private static int _seriesNo = 0;
    private static int _imageNo = 0;
    private int _verbose;
    private JFrame _frame;
    private FileSet _fileSet;
    private DicomDir _dirInfo;
    private JFileChooser _fileChooser;
    private JFileListDialog _fileListdlg;
    private DRFactory _drFactory;
    private JButton _newFileButton;
    private JButton _loadFileButton;
    private JButton _saveFileButton;
    private JButton _compactButton;
    private JButton _importImageButton;
    private JButton _importMultiImageButton;
    private JButton _deleteFromFSButton;
    private JButton _replaceDRButton;
    private JButton _addDRButton;
    private JButton _deleteDRButton;
    private JButton _clearDRsButton;
    private JSplitPane _idRecSplitPane;
    private JSplitPane _treeSplitPane;
    private JSplitPane _logTextSplitPane;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;
    private static final DicomObjectTableModel _NULL_MODEL = new DicomObjectTableModel();
    private DicomObjectTableModel _identificationTableModel;
    private DicomObjectTableModel _recordTableModel;
    private JSizeColumnsToFitTable _identificationTable;
    private JSizeColumnsToFitTable _recordTable;
    private JPanel _recordPanel;
    private MyTreeNode _selNode;
    private MyTreeNode _rootNode;
    private MyTreeNode _lastRecNode;
    private JTree _tree;
    private TreePath _rootPath;
    private DefaultTreeModel _treeModel;
    private String _REC_TYPE[] = {
        "PATIENT", "STUDY", "SERIES", "IMAGE"
    };
    private int LABELS[] = {
        147, 64, 428, 430
    };

    public EditDicomDir()
    {
        _verbose = 1;
        _frame = null;
        _fileSet = null;
        _fileChooser = null;
        _fileListdlg = null;
        _drFactory = new DRFactory();
        _newFileButton = new JButton("New");
        _loadFileButton = new JButton("Load");
        _saveFileButton = new JButton("Save");
        _compactButton = new JButton("Compact");
        _importImageButton = new JButton("Import");
        _importMultiImageButton = new JButton("Import *");
        _deleteFromFSButton = new JButton("Remove");
        _replaceDRButton = new JButton("*");
        _addDRButton = new JButton("+");
        _deleteDRButton = new JButton("-");
        _clearDRsButton = new JButton("C");
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 10000))), true);
        _identificationTableModel = _NULL_MODEL;
        _recordTableModel = _NULL_MODEL;
        _selNode = null;
        _rootNode = new MyTreeNode("root");
        _lastRecNode = null;
        _tree = new JTree(((javax.swing.tree.TreeNode) (_rootNode)));
        _rootPath = new TreePath(((Object) (_rootNode)));
        _treeModel = (DefaultTreeModel)_tree.getModel();
    }

    public static void main(String args[])
    {
        EditDicomDir editdicomdir = new EditDicomDir();
        editdicomdir._frame = ((JFrame) (new AppletFrame("EditDicomDir v1.7.26", ((Applet) (editdicomdir)), 700, 500)));
    }

    public void init()
    {
        ((AbstractButton) (_newFileButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _newFile();
            }

        });
        ((AbstractButton) (_loadFileButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = _chooseFile("Open DICOM Directory File", "Open", 0);
                if(file != null)
                    _load(file);
            }

        });
        ((AbstractButton) (_saveFileButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = _chooseFile("Save DICOM Directory File", "Save", 0);
                if(file != null)
                    _save(file);
            }

        });
        ((AbstractButton) (_compactButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                try
                {
                    _dirInfo.compact();
                    _updateIdentificationModel();
                    MyTreeNode mytreenode = _lastRecNode;
                    _lastRecNode = _findLastRecNode();
                    if(mytreenode != null)
                        _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (mytreenode)));
                    if(_lastRecNode != null)
                        _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (_lastRecNode)));
                    ((AbstractButton) (_replaceDRButton)).setEnabled(_selNode != _rootNode && _selNode != _lastRecNode);
                }
                catch(DicomException dicomexception)
                {
                    System.out.println("DicomException: " + ((Throwable) (dicomexception)).getMessage());
                }
            }

        });
        ((AbstractButton) (_importImageButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = _chooseFile("Select Image File", "Import", 0);
                if(file != null)
                    _importImage(file);
            }

        });
        ((AbstractButton) (_importMultiImageButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                File file = _chooseFile("Select Directory", "Select", 1);
                if(file != null)
                {
                    if(_fileListdlg == null)
                        _fileListdlg = new JFileListDialog(((java.awt.Frame) (_frame)), "Select files");
                    File afile[] = _fileListdlg.getSelectedFiles(file);
                    if(afile != null)
                        _importImages(afile);
                }
            }

        });
        ((AbstractButton) (_deleteFromFSButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _deleteFromFS();
            }

        });
        ((AbstractButton) (_clearDRsButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _clearDRs();
            }

        });
        ((AbstractButton) (_deleteDRButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _deleteDR();
            }

        });
        ((AbstractButton) (_addDRButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _addDR();
            }

        });
        ((AbstractButton) (_replaceDRButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                _replaceDR();
                ((AbstractButton) (_replaceDRButton)).setEnabled(false);
            }

        });
        _tree.addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent treeselectionevent)
            {
                TreePath treepath = treeselectionevent.getNewLeadSelectionPath();
                if(treepath != null)
                {
                    _selNode = (MyTreeNode)treepath.getLastPathComponent();
                    int i = ((DefaultMutableTreeNode) (_selNode)).getLevel();
                    DicomObjectTableModel dicomobjecttablemodel = EditDicomDir._NULL_MODEL;
                    boolean flag = false;
                    if(i > 0)
                        try
                        {
                            DicomObject dicomobject = (DicomObject)((DefaultMutableTreeNode) (_selNode)).getUserObject();
                            dicomobjecttablemodel = new DicomObjectTableModel(dicomobject);
                            if(_selNode == _lastRecNode)
                                dicomobjecttablemodel.addTags(EditDicomDir._EDITABLE_RECORD_TAGS[i - 1], true);
                            ((AbstractTableModel) (dicomobjecttablemodel)).addTableModelListener(new TableModelListener() {

                                public void tableChanged(TableModelEvent tablemodelevent)
                                {
                                    _treeModel.nodeChanged(((javax.swing.tree.TreeNode) (_selNode)));
                                }

                            });
                        }
                        catch(DicomException dicomexception)
                        {
                            System.out.println("DicomException: " + ((Throwable) (dicomexception)).getMessage());
                        }
                    ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (dicomobjecttablemodel)));
                    ((AbstractButton) (_addDRButton)).setEnabled(i != 4);
                    ((AbstractButton) (_deleteDRButton)).setEnabled(i != 0);
                    ((AbstractButton) (_deleteFromFSButton)).setEnabled(i != 0 && _fileSet != null);
                    ((AbstractButton) (_replaceDRButton)).setEnabled(i != 0 && _selNode != _lastRecNode);
                }
            }


        });
        AppletContext appletcontext = ((Applet)this).getAppletContext();
        if(appletcontext instanceof AppletFrame)
            appletcontext = null;
        Debug.out = _log;
        Box box = Box.createHorizontalBox();
        ((Container) (box)).add(((java.awt.Component) (_newFileButton)));
        ((Container) (box)).add(((java.awt.Component) (_loadFileButton)));
        ((Container) (box)).add(((java.awt.Component) (_saveFileButton)));
        ((Container) (box)).add(((java.awt.Component) (_compactButton)));
        ((Container) (box)).add(((java.awt.Component) (_importImageButton)));
        ((Container) (box)).add(((java.awt.Component) (_importMultiImageButton)));
        ((Container) (box)).add(((java.awt.Component) (_deleteFromFSButton)));
        ((Container) (box)).add(Box.createGlue());
        ((Container) (box)).add(((java.awt.Component) (new JTianiButton(appletcontext))));
        JPanel jpanel = new JPanel(((java.awt.LayoutManager) (new BorderLayout())));
        JPanel jpanel1 = new JPanel();
        ((Container) (jpanel1)).add(((java.awt.Component) (_replaceDRButton)));
        ((Container) (jpanel1)).add(((java.awt.Component) (_addDRButton)));
        ((Container) (jpanel1)).add(((java.awt.Component) (_deleteDRButton)));
        ((Container) (jpanel1)).add(((java.awt.Component) (_clearDRsButton)));
        ((Container) (jpanel)).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))));
        ((Container) (jpanel)).add(((java.awt.Component) (jpanel1)), "South");
        _identificationTable = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_identificationTableModel)));
        JScrollPane jscrollpane = new JScrollPane(((java.awt.Component) (_identificationTable)));
        ((JComponent) (jscrollpane)).setPreferredSize(new Dimension(200, 150));
        _recordTable = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_NULL_MODEL)));
        _idRecSplitPane = new JSplitPane(0, ((java.awt.Component) (jscrollpane)), ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_recordTable))))));
        _idRecSplitPane.setOneTouchExpandable(true);
        _treeSplitPane = new JSplitPane(1, ((java.awt.Component) (jpanel)), ((java.awt.Component) (_idRecSplitPane)));
        _treeSplitPane.setOneTouchExpandable(true);
        _logTextSplitPane = new JSplitPane(0, ((java.awt.Component) (_treeSplitPane)), ((java.awt.Component) (new JAutoScrollPane(((JTextComponent) (_logTextArea))))));
        _logTextSplitPane.setOneTouchExpandable(true);
        Container container = ((JApplet)this).getContentPane();
        container.add(((java.awt.Component) (box)), "North");
        container.add(((java.awt.Component) (_logTextSplitPane)), "Center");
        _tree.setVisibleRowCount(16);
        ((JComponent) (_tree)).putClientProperty("JTree.lineStyle", "Angled");
        _tree.setCellRenderer(((javax.swing.tree.TreeCellRenderer) (new MyTreeCellRenderer())));
        _newFile();
    }

    private MyTreeNode _findLastRecNode()
    {
        DicomObject dicomobject = _lastRec();
        if(dicomobject == null)
            return null;
        Enumeration enumeration = ((DefaultMutableTreeNode) (_rootNode)).preorderEnumeration();
        enumeration.nextElement();
        while(enumeration.hasMoreElements()) 
        {
            MyTreeNode mytreenode = (MyTreeNode)enumeration.nextElement();
            if(((DefaultMutableTreeNode) (mytreenode)).getUserObject() == dicomobject)
                return mytreenode;
        }
        return null;
    }

    private DicomObject _lastRec()
    {
        int i = ((DicomObject) (_dirInfo)).getSize(44);
        return i <= 0 ? null : (DicomObject)((DicomObject) (_dirInfo)).get(44, i - 1);
    }

    private void _importImage(File file)
    {
        try
        {
            _insertImage(file);
            _updateIdentificationModel();
            _clear();
            _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
            _tree.expandPath(new TreePath(((Object []) (((DefaultMutableTreeNode) (_rootNode)).getPath()))));
            _treeSplitPane.resetToPreferredSizes();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _importImages(File afile[])
    {
        try
        {
            for(int i = 0; i < afile.length; i++)
                _insertImage(afile[i]);

            _updateIdentificationModel();
            _clear();
            _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
            _tree.expandPath(new TreePath(((Object []) (((DefaultMutableTreeNode) (_rootNode)).getPath()))));
            _treeSplitPane.resetToPreferredSizes();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
    }

    private void _setNoSelection()
    {
        ((JTable) (_recordTable)).setModel(((javax.swing.table.TableModel) (_NULL_MODEL)));
        ((AbstractButton) (_replaceDRButton)).setEnabled(false);
        ((AbstractButton) (_deleteDRButton)).setEnabled(false);
        ((AbstractButton) (_deleteFromFSButton)).setEnabled(false);
        _tree.setSelectionPath(_rootPath);
    }

    private void _enableButtons()
    {
        ((AbstractButton) (_importImageButton)).setEnabled(_fileSet != null);
        ((AbstractButton) (_importMultiImageButton)).setEnabled(_fileSet != null);
    }

    private String[] _fileID(File file)
    {
        String as[] = null;
        try
        {
            String s = (new File(_fileSet.getDirFile().getParent())).getCanonicalPath();
            String s1 = (new File(file.getParent())).getCanonicalPath();
            if(!s1.startsWith(s))
                return null;
            StringTokenizer stringtokenizer = new StringTokenizer(file.getCanonicalPath().substring(s.length()), File.separator);
            as = new String[stringtokenizer.countTokens()];
            for(int i = 0; i < as.length; i++)
                as[i] = stringtokenizer.nextToken();

        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
        return as;
    }

    private void _insertImage(File file)
    {
        String as[] = _fileID(file);
        if(as == null)
            return;
        try
        {
            DicomObject dicomobject = new DicomObject();
            TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (new FileInputStream(file))));
            try
            {
                tianiinputstream.read(dicomobject);
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
            if(dicomobject1 == null)
                dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject))));
            com.tiani.dicom.util.DicomObjectPath dicomobjectpath = _drFactory.createInstanceDRPath(dicomobject, as);
            EditDicomDir _tmp = this;
            com.tiani.dicom.util.DicomObjectFilterPath dicomobjectfilterpath = DRFactory.createRefSOPInstanceFilterPath(dicomobject);
            com.tiani.dicom.util.DicomObjectPath dicomobjectpath1 = _dirInfo.addRootDRpaths(dicomobjectpath, dicomobjectfilterpath);
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _newFile()
    {
        try
        {
            _clear();
            _fileSet = null;
            _dirInfo = new DicomDir(((String) (null)));
            _updateIdentificationModel();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private File _chooseFile(String s, String s1, int i)
    {
        try
        {
            if(_fileChooser == null)
                _fileChooser = new JFileChooser(".");
            _fileChooser.setDialogTitle(s);
            _fileChooser.setFileSelectionMode(i);
            if(_fileSet != null)
                _fileChooser.setSelectedFile(_fileSet.getDirFile());
            else
                _fileChooser.setSelectedFile(new File("DICOMDIR"));
            int j = _fileChooser.showDialog(((java.awt.Component) (_frame)), s1);
            File file = _fileChooser.getSelectedFile();
            return j != 0 ? null : file;
        }
        catch(SecurityException securityexception)
        {
            _log.println(((Object) (securityexception)));
        }
        showPolicyFile();
        return null;
    }

    private void showPolicyFile()
    {
        try
        {
            URL url = new URL(((Applet)this).getDocumentBase(), ((Applet)this).getParameter("PolicyFile"));
            ((Applet)this).getAppletContext().showDocument(url, "_blank");
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private void _load(File file)
    {
        try
        {
            _clear();
            _fileSet = new FileSet(file);
            _fileSet.read(_dirInfo);
            int i = ((DicomObject) (_dirInfo)).getSize(44);
            _addNodes(_dirInfo.getFirstDR(), _rootNode, _lastRec());
            _updateIdentificationModel();
            _tree.expandPath(new TreePath(((Object []) (((DefaultMutableTreeNode) (_rootNode)).getPath()))));
            _treeSplitPane.resetToPreferredSizes();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _updateIdentificationModel()
        throws DicomException
    {
        _identificationTableModel = new DicomObjectTableModel(((DicomObject) (_dirInfo)), true, false, false);
        if(((DicomObject) (_dirInfo)).getSize(44) == 0)
        {
            _identificationTableModel.addFileMetaInfoTags(_EDITABLE_META_INFO_TAGS, true);
            _identificationTableModel.addTags(_EDITABLE_DICOMDIR_ID_TAGS, true);
        }
        ((JTable) (_identificationTable)).setModel(((javax.swing.table.TableModel) (_identificationTableModel)));
        _idRecSplitPane.resetToPreferredSizes();
    }

    private void _clear()
        throws DicomException
    {
        ((DefaultMutableTreeNode) (_rootNode)).removeAllChildren();
        _lastRecNode = null;
        _treeModel.reload();
        _setNoSelection();
    }

    private void _save(File file)
    {
        try
        {
            _fileSet = new FileSet(file);
            _fileSet.write(_dirInfo);
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _addDR()
    {
        try
        {
            (_selNode == null ? _rootNode : _selNode).addDR();
            ((AbstractTableModel) (_identificationTableModel)).fireTableDataChanged();
            _updateIdentificationModel();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _clearDRs()
    {
        try
        {
            _dirInfo.clearDRs();
            ((AbstractTableModel) (_identificationTableModel)).fireTableDataChanged();
            _clear();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _deleteDR()
    {
        try
        {
            _selNode.deleteDR();
            _setNoSelection();
            _enableButtons();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _deleteFromFS()
    {
        try
        {
            Vector vector = _selNode.deleteDR();
            _setNoSelection();
            _enableButtons();
            int i = 0;
            for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements();)
                if(_fileSet.delete((DicomObject)enumeration.nextElement(), true) != null)
                    i++;

        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _replaceDR()
    {
        try
        {
            _selNode.replaceDR();
            ((AbstractTableModel) (_identificationTableModel)).fireTableDataChanged();
        }
        catch(Exception exception)
        {
            System.out.println(((Throwable) (exception)).toString() + ((Throwable) (exception)).getMessage());
        }
    }

    private void _addNodes(DicomObject dicomobject, MyTreeNode mytreenode, DicomObject dicomobject1)
        throws DicomException
    {
        for(; dicomobject != null; dicomobject = _dirInfo.getNextDR(dicomobject))
        {
            MyTreeNode mytreenode1 = new MyTreeNode(((Object) (dicomobject)));
            if(dicomobject == dicomobject1)
                _lastRecNode = mytreenode1;
            _treeModel.insertNodeInto(((javax.swing.tree.MutableTreeNode) (mytreenode1)), ((javax.swing.tree.MutableTreeNode) (mytreenode)), ((DefaultMutableTreeNode) (mytreenode)).getChildCount());
            _addNodes(_dirInfo.getLowerDR(dicomobject), mytreenode1, dicomobject1);
        }

    }

    private DicomObject _createInitRec(int i)
        throws DicomException
    {
        DicomObject dicomobject = DRFactory.create(_REC_TYPE[i]);
        switch(i)
        {
        case 0: // '\0'
            dicomobject.set(147, "NEW^PATIENT");
            dicomobject.set(148, ((Object) (UIDUtils.createID(16))));
            break;

        case 1: // '\001'
            Date date = new Date();
            dicomobject.set(64, ((Object) (((DateFormat) (_dateToString)).format(date))));
            dicomobject.set(70, ((Object) (((DateFormat) (_timeToString)).format(date))));
            dicomobject.set(77, ((Object) (UIDUtils.createID(16))));
            dicomobject.set(427, ((Object) (UIDUtils.createID(16))));
            dicomobject.set(425, ((Object) (UIDUtils.createUID())));
            break;

        case 2: // '\002'
            dicomobject.set(81, "OT");
            dicomobject.set(428, ((Object) (new Integer(--_seriesNo))));
            dicomobject.set(426, ((Object) (UIDUtils.createUID())));
            break;

        case 3: // '\003'
            dicomobject.set(430, ((Object) (new Integer(--_imageNo))));
            break;
        }
        return dicomobject;
    }

    private static DicomObject _cloneDicomObject(DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements(); _copyTagValue((TagValue)enumeration.nextElement(), dicomobject1));
        return dicomobject1;
    }

    private static void _copyTagValue(TagValue tagvalue, DicomObject dicomobject)
        throws DicomException
    {
        int i = tagvalue.getGroup();
        int j = tagvalue.getElement();
        int k = tagvalue.size();
        for(int l = 0; l < k; l++)
            dicomobject.append_ge(i, j, _cloneValue(tagvalue.getValue(l)));

    }

    private static Object _cloneValue(Object obj)
        throws DicomException
    {
        if(obj instanceof String)
            return ((Object) (new String((String)obj)));
        if(obj instanceof Integer)
            return ((Object) (new Integer(((Integer)obj).intValue())));
        if(obj instanceof Long)
            return ((Object) (new Long(((Long)obj).longValue())));
        if(obj instanceof DDate)
            return ((Object) (((DDate)obj).toDICOMString()));
        if(obj instanceof Person)
            return ((Object) (((Person)obj).toDICOMString()));
        if(obj instanceof DicomObject)
            return ((Object) (_cloneDicomObject((DicomObject)obj)));
        else
            return ((Object) (obj.toString()));
    }

    static 
    {
        _EDITABLE_PAT_TAGS = IOD.accumulate(new Tag[][] {
            IOD.PATIENT_MODULE, new Tag[] {
                new Tag(8, 5)
            }
        });
        _EDITABLE_STUDY_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_STUDY_MODULE, IOD.PATIENT_STUDY_MODULE, new Tag[] {
                new Tag(8, 5)
            }
        });
        _EDITABLE_SERIES_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_SERIES_MODULE, new Tag[] {
                new Tag(8, 5)
            }
        });
        _EDITABLE_IMAGE_TAGS = IOD.accumulate(new Tag[][] {
            IOD.GENERAL_IMAGE_MODULE, new Tag[] {
                new Tag(4, 5376), new Tag(4, 5392), new Tag(4, 5393), new Tag(4, 5394), new Tag(8, 5)
            }
        });
        _EDITABLE_RECORD_TAGS = (new Tag[][] {
            _EDITABLE_PAT_TAGS, _EDITABLE_STUDY_TAGS, _EDITABLE_SERIES_TAGS, _EDITABLE_IMAGE_TAGS
        });
    }



































}
