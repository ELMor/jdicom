// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Decaps.java

package com.tiani.dicom.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class Decaps
{

    private static final String USAGE = "Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n";
    private static final byte SH_INIT_SWAP = 1;
    private static final byte SH_INIT_NOSWAP = 2;
    private static final byte CH_INIT = 3;
    private static final byte CH_END = -2;
    private static final byte SH_END = -1;
    private static final byte SH_REP = 10;
    private static final byte SH_NIB_DIF = 12;
    private static final byte SH_CHAR_DIF = 14;
    private static final byte SH_INCOMP_12 = 16;
    private static final byte SH_INCOMP = 18;
    private static final byte CH_REP = 110;
    private static final byte CH_NIB_DIF = 112;
    private static final byte CH_INCOMP = 118;
    private final DataInputStream in;
    private OutputStream out;
    private boolean endOfDecompress;
    private byte lB;
    private short lS;
    private int headerSize;
    private boolean compressed;

    private Decaps(String s, InputStream inputstream)
        throws IOException
    {
        endOfDecompress = false;
        lB = 0;
        lS = 0;
        headerSize = -1;
        compressed = false;
        in = new DataInputStream(inputstream);
        init(s);
    }

    private void init(String s)
        throws IOException
    {
        String s1 = in.readLine();
        if(!s1.startsWith("MAGIC"))
            throw new IOException("Not an Encapsed File - " + s);
        while(!"ENDINFO".equals(((Object) (s1 = in.readLine())))) 
        {
            if(s1.startsWith("FILETYPE") && !"IMAGE".equals(((Object) (s1.substring(9)))))
                throw new IOException("Not an Image File - " + s);
            if(s1.startsWith("COMPRESSION"))
                compressed = !"NONE".equals(((Object) (s1.substring(12))));
            if(s1.startsWith("HEADERSIZE"))
                try
                {
                    headerSize = Integer.parseInt(s1.substring(11));
                }
                catch(IllegalArgumentException illegalargumentexception)
                {
                    throw new IOException("Header Format Error - " + s);
                }
        }
        if(headerSize == -1)
            throw new IOException("Header Format Error" + s);
        else
            return;
    }

    private void writeTo(OutputStream outputstream)
        throws IOException
    {
        int i;
        if(compressed)
        {
            while(headerSize-- > 0) 
                outputstream.write(((FilterInputStream) (in)).read());
            out = outputstream;
            decompress_init();
            while(!endOfDecompress) 
                decompress_more();
        } else
        {
            while((i = ((FilterInputStream) (in)).read()) != -1) 
                outputstream.write(i);
        }
    }

    private void decompress_init()
        throws IOException
    {
        switch(in.readByte())
        {
        case 3: // '\003'
            in.readInt();
            out(in.readByte());
            break;

        case 1: // '\001'
        case 2: // '\002'
            in.readInt();
            out(in.readShort());
            break;
        }
    }

    private void decompress_more()
        throws IOException
    {
        if(endOfDecompress)
            return;
        byte byte4 = in.readByte();
        switch(byte4)
        {
        case -2: 
            endOfDecompress = true;
            return;

        case -1: 
            endOfDecompress = true;
            return;
        }
        int l = in.readByte() & 0xff;
label0:
        switch(byte4)
        {
        case 118: // 'v'
            while(l-- > 0) 
                out(in.readByte());
            break;

        case 18: // '\022'
            while(l-- > 0) 
                out(in.readShort());
            break;

        case 16: // '\020'
            do
            {
                byte byte0 = in.readByte();
                byte byte3 = in.readByte();
                out((short)((byte0 << 4 & 0xf00) + (byte0 << 4 & 0xf0) + (byte3 >> 4 & 0xf)));
                if(--l == 0)
                    break label0;
                if(l < 0)
                    throw new IOException("length decoding mismatch");
                out((short)((byte3 << 8 & 0xf00) + (in.readByte() & 0xff)));
                if(--l == 0)
                    break label0;
            } while(l >= 0);
            throw new IOException("length decoding mismatch");

        case 14: // '\016'
            int i = lS & 0xffff;
            while(l-- > 0) 
            {
                i += (in.readByte() & 0xff) - 127;
                out((short)i);
            }
            break;

        case 112: // 'p'
            int j = lB & 0xff;
            do
            {
                byte byte1 = in.readByte();
                j += (byte1 >> 4 & 0xf) - 7;
                out((byte)j);
                if(--l == 0)
                    break label0;
                j += (byte1 & 0xf) - 7;
                out((byte)j);
            } while(--l != 0);
            break;

        case 12: // '\f'
            int k = lS & 0xffff;
            do
            {
                byte byte2 = in.readByte();
                k += (byte2 >> 4 & 0xf) - 7;
                out((short)k);
                if(--l == 0)
                    break label0;
                k += (byte2 & 0xf) - 7;
                out((short)k);
            } while(--l != 0);
            break;

        case 110: // 'n'
            while(l-- > 0) 
                out(lB);
            break;

        case 10: // '\n'
            while(l-- > 0) 
                out(lS);
            break;

        default:
            throw new IOException("illegal function code while decompressing: " + byte4);
        }
    }

    private void out(byte byte0)
        throws IOException
    {
        out.write(((int) (lB = byte0)));
    }

    private void out(short word0)
        throws IOException
    {
        lS = word0;
        out.write(((int) ((byte)word0)));
        out.write(((int) ((byte)(word0 >> 8))));
    }

    private static void process(File file, File file1)
    {
        String s = "stdin";
        String s1 = "stdout";
        Object obj = ((Object) (System.in));
        Object obj1 = ((Object) (System.out));
        try
        {
            if(file != null)
            {
                s = "" + file;
                obj = ((Object) (new BufferedInputStream(((InputStream) (new FileInputStream(file))))));
            }
            if(file1 != null)
            {
                s1 = "" + file1;
                obj1 = ((Object) (new BufferedOutputStream(((OutputStream) (new FileOutputStream(file1))))));
            }
            (new Decaps(s, ((InputStream) (obj)))).writeTo(((OutputStream) (obj1)));
            System.err.println("Decapsed " + s + " to " + s1);
        }
        catch(IOException ioexception)
        {
            System.err.println("Failed to decaps " + s + " to " + s1);
            System.err.println(((Object) (ioexception)));
        }
        finally
        {
            if(obj != null)
                try
                {
                    ((InputStream) (obj)).close();
                }
                catch(IOException ioexception1) { }
            if(obj1 != null)
                try
                {
                    ((OutputStream) (obj1)).close();
                }
                catch(IOException ioexception2) { }
        }
    }

    public static void main(String args[])
    {
        if(args.length == 0)
        {
            System.err.println("Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n");
            System.exit(1);
        }
        File file = "--stdin".equals(((Object) (args[0]))) ? null : new File(args[0]);
        File file2 = "--stdout".equals(((Object) (args[args.length - 1]))) ? null : new File(args[args.length - 1]);
        boolean flag = file != null && file.isDirectory();
        boolean flag1 = file2 != null && file2.isDirectory();
        if(!flag1)
        {
            if(flag || args.length > 2)
            {
                System.err.println("to process several files, the last parameter must be a directory");
                System.err.println("Usage: java -jar decaps.jar source|--stdin [dest|--stdout]\n            (to decaps one file)\n   or  java -jar decaps.jar source... directory\n            (to decaps several files)\n");
                System.exit(1);
            }
            process(file, file2);
        } else
        {
            for(int i = 0; i < args.length - 1; i++)
            {
                File file1 = new File(args[i]);
                if(file1.isDirectory())
                    processDir(file1, file2);
                else
                    process(new File(args[i]), new File(file2, args[i].substring(args[i].lastIndexOf(((int) (File.separatorChar))) + 1)));
            }

        }
    }

    private static void processDir(File file, File file1)
    {
        String as[] = file.list();
        for(int i = 0; i < as.length; i++)
        {
            File file2 = new File(file, as[i]);
            File file3 = new File(file1, as[i]);
            if(file2.isDirectory())
            {
                file3.mkdir();
                processDir(file2, file3);
            } else
            {
                process(file2, file3);
            }
        }

    }
}
