// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   XMLWriter.java

package com.tiani.dicom.tools.sr;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.StringTokenizer;

public class XMLWriter extends BufferedWriter
{

    private static final String PNAME_ATTRB[] = {
        "last", "first", "middle", "prefix", "suffix"
    };

    public XMLWriter(OutputStream outputstream)
        throws UnsupportedEncodingException
    {
        super(((Writer) (new OutputStreamWriter(outputstream, "UTF8"))));
    }

    public synchronized void write(DicomObject dicomobject)
        throws IOException, DicomException
    {
        ((Writer)this).write("<?xml version='1.0' encoding='UTF-8'?>");
        nl(0);
        ((Writer)this).write("<srDocument>");
        writeConceptName(dicomobject, "1", 1);
        writeValue(dicomobject, "1", 1);
        nl(0);
        ((Writer)this).write("</srDocument>");
    }

    private void writeConceptName(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        if(dicomobject.getSize(1322) <= 0)
            return;
        nl(i);
        ((Writer)this).write("<concept>");
        writeCodeAttribs((DicomObject)dicomobject.get(1322), i + 1);
        int j = dicomobject.getSize(1451);
        for(int k = 0; k < j; k++)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(1451, k);
            if(dicomobject1.getSize(1425) > 0 && dicomobject1.getS(1425).equals("HAS CONCEPT MOD"))
                writeContent("HasConceptMod", dicomobject1, s + '.' + (k + 1), i + 1);
        }

        nl(i);
        ((Writer)this).write("</concept>");
    }

    protected void writeCodeAttribs(DicomObject dicomobject, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<code");
        if(dicomobject.getSize(91) > 0)
            writeEncodedAttrib("value", dicomobject.getS(91));
        if(dicomobject.getSize(92) > 0)
            writeEncodedAttrib("scheme", dicomobject.getS(92));
        if(dicomobject.getSize(93) > 0)
            writeEncodedAttrib("meaning", dicomobject.getS(93));
        ((Writer)this).write("/>");
    }

    protected void writeValue(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        if(dicomobject.getSize(1429) <= 0)
            return;
        String s1 = dicomobject.getS(1429);
        if(s1.equals("CONTAINER"))
            writeContainer(dicomobject, s, i);
        else
        if(s1.equals("TEXT"))
            writeText(dicomobject, s, i);
        else
        if(s1.equals("DATETIME"))
            writeDateTime(dicomobject, s, i);
        else
        if(s1.equals("DATE"))
            writeDate(dicomobject, s, i);
        else
        if(s1.equals("TIME"))
            writeTime(dicomobject, s, i);
        else
        if(s1.equals("PNAME"))
            writePname(dicomobject, s, i);
        else
        if(s1.equals("UIDREF"))
            writeUIDref(dicomobject, s, i);
        else
        if(s1.equals("NUM"))
            writeNum(dicomobject, s, i);
        else
        if(s1.equals("CODE"))
            writeCode(dicomobject, s, i);
        else
        if(s1.equals("COMPOSITE"))
            writeComposite(dicomobject, s, i);
        else
        if(s1.equals("IMAGE"))
            writeImage(dicomobject, s, i);
        else
        if(s1.equals("WAVEFORM"))
            writeWaveform(dicomobject, s, i);
        else
        if(s1.equals("SCOORD"))
            writeScoord(dicomobject, s, i);
        else
        if(s1.equals("TCOORD"))
            writeTcoord(dicomobject, s, i);
    }

    private void writeContainer(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Container");
        if(dicomobject.getSize(1430) > 0)
            writeEncodedAttrib("Continuity", dicomobject.getS(1430));
        ((Writer)this).write(">");
        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Container>");
    }

    private void writeEncodedAttrib(String s, String s1)
        throws IOException
    {
        ((BufferedWriter)this).write(32);
        ((Writer)this).write(s);
        ((Writer)this).write("=\"");
        writeEncoded(s1);
        ((BufferedWriter)this).write(34);
    }

    private void writeEncoded(String s)
        throws IOException
    {
        int i = s.length();
        for(int j = 0; j < i; j++)
        {
            char c = s.charAt(j);
            switch(c)
            {
            case 38: // '&'
                ((Writer)this).write("&amp;");
                break;

            case 60: // '<'
                ((Writer)this).write("&lt;");
                break;

            case 62: // '>'
                ((Writer)this).write("&gt;");
                break;

            case 34: // '"'
                ((Writer)this).write("&quot;");
                break;

            case 39: // '\''
                ((Writer)this).write("&apos;");
                break;

            default:
                ((BufferedWriter)this).write(((int) (c)));
                break;
            }
        }

    }

    private void writeText(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Text>");
        if(dicomobject.getSize(1327) > 0)
        {
            nl(i + 1);
            ((Writer)this).write("<body>");
            writeEncoded(dicomobject.getS(1327));
            ((Writer)this).write("</body>");
        }
        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Text>");
    }

    private void writeDateTime(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<DateTime");
        if(dicomobject.getSize(1434) > 0)
            writeDateTimeAttribs(dicomobject.getS(1434));
        ((Writer)this).write("/>");
    }

    private void writeDateTimeAttribs(String s)
        throws IOException
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, "&");
        String s1 = stringtokenizer.nextToken();
        int i = s1.length();
        if(i >= 4)
        {
            writeEncodedAttrib("year", s1.substring(0, 4));
            if(i >= 6)
            {
                writeEncodedAttrib("month", s1.substring(4, 6));
                if(i >= 8)
                {
                    writeEncodedAttrib("day", s1.substring(6, 8));
                    if(i >= 10)
                    {
                        writeEncodedAttrib("hour", s1.substring(8, 10));
                        if(i >= 12)
                        {
                            writeEncodedAttrib("min", s1.substring(10, 12));
                            if(i >= 14)
                            {
                                writeEncodedAttrib("sec", s1.substring(12, 14));
                                if(i > 15)
                                    writeEncodedAttrib("frac", s1.substring(15));
                            }
                        }
                    }
                }
            }
        }
    }

    private void writeDate(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Date>");
        if(dicomobject.getSize(1323) > 0)
        {
            String s1 = dicomobject.getS(1323);
            int j = s1.length();
            if(j >= 4)
            {
                writeEncodedAttrib("year", s1.substring(0, 4));
                if(j >= 6)
                {
                    writeEncodedAttrib("month", s1.substring(4, 6));
                    if(j >= 8)
                        writeEncodedAttrib("day", s1.substring(6, 8));
                }
            }
        }
        ((Writer)this).write("/>");
    }

    private void writeTime(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Time>");
        if(dicomobject.getSize(1324) > 0)
        {
            String s1 = dicomobject.getS(1324);
            int j = s1.length();
            if(j >= 2)
            {
                writeEncodedAttrib("hour", s1.substring(0, 2));
                if(j >= 4)
                {
                    writeEncodedAttrib("min", s1.substring(2, 4));
                    if(j >= 6)
                    {
                        writeEncodedAttrib("sec", s1.substring(4, 6));
                        if(j > 7)
                            writeEncodedAttrib("frac", s1.substring(7));
                    }
                }
            }
        }
        ((Writer)this).write("/>");
    }

    private void writePname(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Pname");
        if(dicomobject.getSize(1325) > 0)
        {
            StringTokenizer stringtokenizer = new StringTokenizer(dicomobject.getS(1325), "^", true);
            for(int j = 0; stringtokenizer.hasMoreTokens() && j < PNAME_ATTRB.length; j++)
            {
                String s1 = stringtokenizer.nextToken();
                if(s1.equals("^"))
                    continue;
                writeEncodedAttrib(PNAME_ATTRB[j], s1);
                if(!stringtokenizer.hasMoreTokens())
                    break;
                stringtokenizer.nextToken();
            }

        }
        ((Writer)this).write("/>");
    }

    private void writeUIDref(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<UIDref");
        if(dicomobject.getSize(1435) > 0)
            writeEncodedAttrib("value", dicomobject.getS(1435));
        ((Writer)this).write("/>");
    }

    private void writeNum(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Num");
        if(dicomobject.getSize(1440) > 0)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(1440);
            if(dicomobject1.getSize(1329) > 0)
                writeEncodedAttrib("value", dicomobject1.getS(1329));
            ((Writer)this).write(">");
            nl(i + 1);
            ((Writer)this).write("<unit>");
            if(dicomobject1.getSize(1321) > 0)
                writeCodeAttribs((DicomObject)dicomobject1.get(1321), i + 2);
            nl(i + 1);
            ((Writer)this).write("</unit>");
        } else
        {
            ((Writer)this).write(">");
        }
        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Num>");
    }

    private void writeCode(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Code>");
        if(dicomobject.getSize(1328) > 0)
            writeCodeAttribs((DicomObject)dicomobject.get(1328), i + 1);
        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Code>");
    }

    private void writeComposite(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Composite>");
        int j = dicomobject.getSize(121);
        for(int k = 0; k < j; k++)
        {
            nl(i + 1);
            ((Writer)this).write("<refComposite");
            writeSOPAttrib((DicomObject)dicomobject.get(121, k));
            ((Writer)this).write("/>");
        }

        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Composite>");
    }

    private void writeSOPAttrib(DicomObject dicomobject)
        throws IOException, DicomException
    {
        if(dicomobject.getSize(115) > 0)
            writeEncodedAttrib("classUID", dicomobject.getS(115));
        if(dicomobject.getSize(116) > 0)
            writeEncodedAttrib("instanceUID", dicomobject.getS(116));
    }

    private void writeImage(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Image>");
        int j = dicomobject.getSize(121);
        for(int k = 0; k < j; k++)
        {
            nl(i + 1);
            ((Writer)this).write("<refImage");
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(121, k);
            writeSOPAttrib(dicomobject1);
            ((Writer)this).write(">");
            int l = dicomobject1.getSize(117);
            for(int i1 = 0; i1 < l; i1++)
            {
                nl(i + 2);
                ((Writer)this).write("<frameNo");
                writeEncodedAttrib("value", dicomobject1.getS(117, i1));
                ((Writer)this).write("/>");
            }

            int j1 = dicomobject1.getSize(121);
            for(int k1 = 0; k1 < j1; k1++)
            {
                nl(i + 2);
                ((Writer)this).write("<refPresentation");
                writeSOPAttrib((DicomObject)dicomobject1.get(121, k1));
                ((Writer)this).write("/>");
            }

            nl(i + 1);
            ((Writer)this).write("</refImage>");
        }

        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Image>");
    }

    private void writeWaveform(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Waveform>");
        int j = dicomobject.getSize(121);
        for(int k = 0; k < j; k++)
        {
            nl(i + 1);
            ((Writer)this).write("<refWaveform");
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(121, k);
            writeSOPAttrib(dicomobject1);
            ((Writer)this).write(">");
            int l = dicomobject1.getSize_ge(64, 41136) >> 1;
            for(int i1 = 0; i1 < l; i1++)
            {
                nl(i + 2);
                ((Writer)this).write("<channel");
                writeEncodedAttrib("M", dicomobject1.getS_ge(64, 41136, i1 << 1));
                writeEncodedAttrib("C", dicomobject1.getS_ge(64, 41136, (i1 << 1) + 1));
                ((Writer)this).write("/>");
            }

            nl(i + 1);
            ((Writer)this).write("</refWaveform>");
        }

        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Waveform>");
    }

    private void writeScoord(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Scoord");
        if(dicomobject.getSize(1396) > 0)
            writeEncodedAttrib("type", dicomobject.getS(1396));
        ((Writer)this).write(">");
        int j = dicomobject.getSize(1395) >> 1;
        for(int k = 0; k < j; k++)
        {
            nl(i + 1);
            ((Writer)this).write("<graphicData");
            writeEncodedAttrib("column", dicomobject.getS(1395, k << 1));
            writeEncodedAttrib("row", dicomobject.getS(1395, (k << 1) + 1));
            ((Writer)this).write("/>");
        }

        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Scoord>");
    }

    private void writeTcoord(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<Tcoord");
        if(dicomobject.getSize(1436) > 0)
            writeEncodedAttrib("type", dicomobject.getS(1436));
        ((Writer)this).write(">");
        int j = dicomobject.getSize(1437);
        for(int k = 0; k < j; k++)
        {
            nl(i + 1);
            ((Writer)this).write("<position");
            writeEncodedAttrib("value", dicomobject.getS(1437, k));
            ((Writer)this).write("/>");
        }

        int l = dicomobject.getSize(1438);
        for(int i1 = 0; i1 < l; i1++)
        {
            nl(i + 1);
            ((Writer)this).write("<offset");
            writeEncodedAttrib("value", dicomobject.getS(1438, i1));
            ((Writer)this).write("/>");
        }

        int j1 = dicomobject.getSize(1439);
        for(int k1 = 0; k1 < j1; k1++)
        {
            nl(i + 1);
            ((Writer)this).write("<DateTime");
            writeDateTimeAttribs(dicomobject.getS(1439, k1));
            ((Writer)this).write("/>");
        }

        writeRelations(dicomobject, s, i + 1);
        nl(i);
        ((Writer)this).write("</Tcoord>");
    }

    private void writeRelations(DicomObject dicomobject, String s, int i)
        throws IOException, DicomException
    {
        int j = dicomobject.getSize(1451);
        for(int k = 0; k < j; k++)
        {
            String s1 = s + '.' + (k + 1);
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(1451, k);
            if(dicomobject1.getSize(1425) > 0)
            {
                String s2 = dicomobject1.getS(1425);
                if(s2.equals("CONTAINS"))
                    writeContent("Contains", dicomobject1, s1, i);
                else
                if(s2.equals("HAS PROPERTIES"))
                    writeContent("HasProperties", dicomobject1, s1, i);
                else
                if(s2.equals("HAS OBS CONTEXT"))
                    writeContent("HasObsContext", dicomobject1, s1, i);
                else
                if(s2.equals("HAS ACQ CONTEXT"))
                    writeContent("HasAcqContext", dicomobject1, s1, i);
                else
                if(s2.equals("INFERRED FROM"))
                    writeContent("InferredFrom", dicomobject1, s1, i);
                else
                if(s2.equals("SELECTED FROM"))
                    writeContent("SelectedFrom", dicomobject1, s1, i);
            }
        }

    }

    private void writeContent(String s, DicomObject dicomobject, String s1, int i)
        throws IOException, DicomException
    {
        nl(i);
        ((Writer)this).write("<" + s + " id=\"" + s1 + "\">");
        int j = dicomobject.getSize(1458);
        if(j > 0)
        {
            nl(i + 1);
            ((Writer)this).write("<see id=\"1");
            for(int k = 1; k < j; k++)
                ((Writer)this).write("." + dicomobject.getS(1458, k));

            ((Writer)this).write("\"/>");
        } else
        {
            writeConceptName(dicomobject, s1, i + 1);
            writeValue(dicomobject, s1, i + 1);
        }
        nl(i);
        ((Writer)this).write("</" + s + ">");
    }

    private void nl(int i)
        throws IOException
    {
        ((BufferedWriter)this).write(10);
        for(int j = 0; j < i; j++)
            ((Writer)this).write("  ");

    }

}
