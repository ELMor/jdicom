// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EditDicomObject.java

package com.tiani.dicom.tools;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.myassert.Assert;
import com.tiani.dicom.ui.AppletFrame;
import com.tiani.dicom.ui.DicomObjectTableModel;
import com.tiani.dicom.ui.DocumentOutputStream;
import com.tiani.dicom.ui.JAutoScrollPane;
import com.tiani.dicom.ui.JPropertiesTable;
import com.tiani.dicom.ui.JSizeColumnsToFitTable;
import com.tiani.dicom.ui.JTianiButton;
import com.tiani.dicom.util.CheckParam;
import com.tiani.dicom.util.ExampleFileFilter;
import com.tiani.dicom.util.UIDUtils;
import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;

// Referenced classes of package com.tiani.dicom.tools:
//            TS, IODNode

public class EditDicomObject extends JApplet
{
    private final class JSelectIODPanel extends JPanel
    {

        private DefaultMutableTreeNode _root;
        private JTree _tree;
        private JButton _removeButton;
        private JButton _addButton;

        private boolean checkSOPClassUID(String s)
            throws DicomException
        {
            if(s == null)
                return true;
            String s1 = _dicomObject.getS(62);
            if(s1 != null && s1.length() != 0)
                return s1.equals(((Object) (s)));
            _dicomObject.set(62, ((Object) (s)));
            String s2 = _dicomObject.getS(63);
            if(s2 == null || s2.length() == 0)
                _dicomObject.set(63, ((Object) (UIDUtils.createUID())));
            return true;
        }





        public JSelectIODPanel()
        {
            super(((java.awt.LayoutManager) (new BorderLayout())));
            _root = IODNode.root();
            _tree = new JTree(((javax.swing.tree.TreeNode) (_root)));
            _removeButton = new JButton("<<");
            _addButton = new JButton(">>");
            ((AbstractButton) (_addButton)).addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent actionevent)
                {
                    if(!verifyParams())
                        return;
                    Object obj = _tree.getLastSelectedPathComponent();
                    if(obj != null && obj != _root)
                    {
                        IODNode iodnode = (IODNode)obj;
                        com.tiani.dicom.util.Tag atag[] = (com.tiani.dicom.util.Tag[])((DefaultMutableTreeNode) (iodnode)).getUserObject();
                        try
                        {
                            if(iodnode.isFileMetaInfo())
                            {
                                DicomObject dicomobject = _dicomObject.getFileMetaInformation();
                                if(dicomobject == null)
                                {
                                    FileMetaInformation filemetainformation = new FileMetaInformation(_dicomObject, UID.getUIDEntry(TS.ID[TS.indexOf(((Object) (_params.getProperty("Save.TransferSyntax"))))]).getValue());
                                    _dicomObject.setFileMetaInformation(((DicomObject) (filemetainformation)));
                                }
                                _tableModel.addFileMetaInfoTags(atag);
                            } else
                            if(checkSOPClassUID(iodnode.getSOPClassUID()))
                                _tableModel.addTags(atag);
                            else
                                _log.println("SOPClassUID mismatch");
                        }
                        catch(Exception exception)
                        {
                            _log.println(((Object) (exception)));
                        }
                    }
                }

            });
            ((AbstractButton) (_removeButton)).addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent actionevent)
                {
                    updateTable();
                }

            });
            ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (_tree))))), "Center");
            JPanel jpanel = new JPanel();
            ((Container) (jpanel)).add(((java.awt.Component) (_removeButton)));
            ((Container) (jpanel)).add(((java.awt.Component) (_addButton)));
            ((Container)this).add(((java.awt.Component) (jpanel)), "South");
            _tree.setVisibleRowCount(10);
            ((JComponent) (_tree)).putClientProperty("JTree.lineStyle", "Angled");
        }
    }


    static final String PARAM_NAMES[] = {
        "Show.FileMetaInfo", "Show.SequenceItem", "Load.Append", "Load.PixelData", "Save.FileMetaInfo", "Save.TransferSyntax", "Save.UndefSeqLen", "Verbose"
    };
    static Hashtable PARAM_CHECKS;
    private Properties _params;
    private final FileFilter _HTMLFileFilter;
    private JFrame _frame;
    private JFileChooser _fileChooser;
    private int _curTSindex;
    private File _curFile;
    private JButton _newButton;
    private JButton _loadButton;
    private JButton _saveButton;
    private JButton _htmlButton;
    private DicomObjectTableModel _tableModel;
    private DicomObject _dicomObject;
    private JSizeColumnsToFitTable _table;
    private JTextArea _logTextArea;
    private Document _logDoc;
    private PrintStream _log;

    public static void main(String args[])
    {
        final String propSpec = args.length <= 0 ? "EditDicomObject.properties" : args[0];
        Properties properties = null;
        try
        {
            properties = loadProperties(((InputStream) (new FileInputStream(propSpec))));
        }
        catch(Exception exception) { }
        final EditDicomObject instance = new EditDicomObject(properties);
        instance._frame = ((JFrame) (new AppletFrame("EditDicomObject v1.7.26", ((Applet) (instance)), 700, 500, ((java.awt.event.WindowListener) (new WindowAdapter() {

            public void windowClosing(WindowEvent windowevent)
            {
                try
                {
                    instance.storeParams(propSpec);
                }
                catch(Exception exception1) { }
                System.exit(0);
            }

        })))));
    }

    public EditDicomObject()
    {
        _params = null;
        _HTMLFileFilter = ((FileFilter) (new ExampleFileFilter("html", "HTML")));
        _frame = null;
        _fileChooser = null;
        _curTSindex = -1;
        _curFile = null;
        _newButton = new JButton("New");
        _loadButton = new JButton("Load");
        _saveButton = new JButton("Save");
        _htmlButton = new JButton("HTML");
        _tableModel = new DicomObjectTableModel();
        _dicomObject = _tableModel.getDicomObject();
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_tableModel)));
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 30000))), true);
    }

    protected EditDicomObject(Properties properties)
    {
        _params = null;
        _HTMLFileFilter = ((FileFilter) (new ExampleFileFilter("html", "HTML")));
        _frame = null;
        _fileChooser = null;
        _curTSindex = -1;
        _curFile = null;
        _newButton = new JButton("New");
        _loadButton = new JButton("Load");
        _saveButton = new JButton("Save");
        _htmlButton = new JButton("HTML");
        _tableModel = new DicomObjectTableModel();
        _dicomObject = _tableModel.getDicomObject();
        _table = new JSizeColumnsToFitTable(((javax.swing.table.TableModel) (_tableModel)));
        _logTextArea = new JTextArea();
        _logDoc = ((JTextComponent) (_logTextArea)).getDocument();
        _log = new PrintStream(((java.io.OutputStream) (new DocumentOutputStream(_logDoc, 30000))), true);
        _params = properties;
    }

    public void init()
    {
        Debug.out = _log;
        ((AbstractButton) (_newButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(!verifyParams())
                {
                    return;
                } else
                {
                    _dicomObject = new DicomObject();
                    _curTSindex = -1;
                    updateTable();
                    return;
                }
            }

        });
        ((AbstractButton) (_loadButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(!verifyParams())
                    return;
                File file = chooseFile("Load DICOM File", "Load", false);
                if(file != null)
                    load(file);
            }

        });
        ((AbstractButton) (_saveButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(!verifyParams())
                    return;
                if(_curTSindex == TS.MG1_1)
                {
                    _log.println("Cannot store file with with MG1.1 compressed Pixeldata!");
                    return;
                }
                int i = TS.indexOf(((Object) (_params.getProperty("Save.TransferSyntax"))));
                boolean flag = getBooleanParam("Save.FileMetaInfo");
                if(!flag && i != 0)
                {
                    _log.println("Cannot store file without File Meta Information with Transfersyntax " + TS.LABEL[i]);
                    return;
                }
                File file = chooseFile("Save as DICOM File", "Save", false);
                if(file != null)
                    save(file, flag, i);
            }

        });
        ((AbstractButton) (_htmlButton)).addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(_curFile != null)
                    _fileChooser.setSelectedFile(new File(_curFile.getParent(), _curFile.getName() + ".html"));
                File file = chooseFile("Save as HTML file", "Save", true);
                if(file != null)
                    saveHtml(file, _curFile == null ? "unknown source" : _curFile.getName());
            }

        });
        AppletContext appletcontext = ((Applet)this).getAppletContext();
        if(appletcontext instanceof AppletFrame)
            appletcontext = null;
        Box box = Box.createHorizontalBox();
        ((Container) (box)).add(((java.awt.Component) (_newButton)));
        ((Container) (box)).add(((java.awt.Component) (_loadButton)));
        ((Container) (box)).add(((java.awt.Component) (_saveButton)));
        ((Container) (box)).add(((java.awt.Component) (_htmlButton)));
        ((Container) (box)).add(Box.createGlue());
        ((Container) (box)).add(((java.awt.Component) (new JTianiButton(appletcontext))));
        if(appletcontext != null)
            getAppletParams();
        if(_params == null || !verifyParams())
            try
            {
                _params = loadProperties((com.tiani.dicom.tools.EditDicomObject.class).getResourceAsStream("EditDicomObject.properties"));
            }
            catch(IOException ioexception)
            {
                Assert.fail("Failed to load EditDicomObject.properties ressource");
            }
        JPropertiesTable jpropertiestable = new JPropertiesTable(PARAM_NAMES, _params, PARAM_CHECKS);
        JSplitPane jsplitpane = new JSplitPane(0, ((java.awt.Component) (new JScrollPane(((java.awt.Component) (jpropertiestable))))), ((java.awt.Component) (new JSelectIODPanel())));
        jsplitpane.setOneTouchExpandable(true);
        JSplitPane jsplitpane1 = new JSplitPane(1, ((java.awt.Component) (jsplitpane)), ((java.awt.Component) (new JScrollPane(((java.awt.Component) (_table))))));
        jsplitpane1.setOneTouchExpandable(true);
        JSplitPane jsplitpane2 = new JSplitPane(0, ((java.awt.Component) (jsplitpane1)), ((java.awt.Component) (new JAutoScrollPane(((JTextComponent) (_logTextArea))))));
        jsplitpane2.setOneTouchExpandable(true);
        Container container = ((JApplet)this).getContentPane();
        container.add(((java.awt.Component) (box)), "North");
        container.add(((java.awt.Component) (jsplitpane2)), "Center");
        updateTable();
    }

    static Properties loadProperties(InputStream inputstream)
        throws IOException
    {
        Properties properties = new Properties();
        try
        {
            properties.load(inputstream);
        }
        finally
        {
            inputstream.close();
        }
        return properties;
    }

    private void getAppletParams()
    {
        _params = new Properties();
        for(int i = 0; i < PARAM_NAMES.length; i++)
        {
            String s;
            if((s = ((Applet)this).getParameter(PARAM_NAMES[i])) != null)
                ((Hashtable) (_params)).put(((Object) (PARAM_NAMES[i])), ((Object) (s)));
        }

    }

    private void storeParams(String s)
        throws IOException
    {
        CheckParam.verify(_params, PARAM_CHECKS);
        FileOutputStream fileoutputstream = new FileOutputStream(s);
        try
        {
            _params.store(((java.io.OutputStream) (fileoutputstream)), "Properties for EditDicomObject");
        }
        finally
        {
            fileoutputstream.close();
        }
    }

    private File chooseFile(String s, String s1, boolean flag)
    {
        try
        {
            if(_fileChooser == null)
            {
                _fileChooser = new JFileChooser(".");
                _fileChooser.addChoosableFileFilter(_HTMLFileFilter);
            }
            _fileChooser.setFileFilter(flag ? _HTMLFileFilter : _fileChooser.getAcceptAllFileFilter());
            _fileChooser.setDialogTitle(s);
            int i = _fileChooser.showDialog(((java.awt.Component) (_frame)), s1);
            File file = _fileChooser.getSelectedFile();
            return i != 0 ? null : file;
        }
        catch(SecurityException securityexception)
        {
            _log.println(((Object) (securityexception)));
        }
        showPolicyFile();
        return null;
    }

    private void showPolicyFile()
    {
        try
        {
            URL url = new URL(((Applet)this).getDocumentBase(), ((Applet)this).getParameter("PolicyFile"));
            ((Applet)this).getAppletContext().showDocument(url, "_blank");
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private void updateTable()
    {
        try
        {
            _tableModel = new DicomObjectTableModel(_dicomObject, getBooleanParam("Show.FileMetaInfo"), getBooleanParam("Show.SequenceItem"), true);
            ((JTable) (_table)).setModel(((javax.swing.table.TableModel) (_tableModel)));
        }
        catch(DicomException dicomexception)
        {
            _log.println(((Object) (dicomexception)));
        }
    }

    private boolean verifyParams()
    {
        try
        {
            CheckParam.verify(_params, PARAM_CHECKS);
            Debug.DEBUG = getIntParam("Verbose");
            return true;
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
        return false;
    }

    private boolean getBooleanParam(String s)
    {
        String s1 = _params.getProperty(s);
        return s1 != null && "true".compareTo(s1.toLowerCase()) == 0;
    }

    private int getIntParam(String s)
    {
        return Integer.parseInt(_params.getProperty(s));
    }

    private void load(File file)
    {
        if(!getBooleanParam("Load.Append"))
            _dicomObject = new DicomObject();
        try
        {
            TianiInputStream tianiinputstream = new TianiInputStream(((InputStream) (new FileInputStream(file))));
            try
            {
                tianiinputstream.read(_dicomObject, getBooleanParam("Load.PixelData"));
            }
            finally
            {
                ((FilterInputStream) (tianiinputstream)).close();
            }
            _curFile = file;
            _curTSindex = TS.indexOf(tianiinputstream.getFormat(), _dicomObject);
            _log.println("Load from " + file + '[' + tianiinputstream.getFormatName() + '-' + TS.LABEL[_curTSindex] + ']');
            int i = _dicomObject.getPixelDataLength();
            if(i > 0)
            {
                _log.println("Pixel data length = " + Integer.toHexString(i) + "H");
                _log.println("RxC = " + Integer.toHexString(_dicomObject.getI(466) * _dicomObject.getI(467)) + "H" + ", bits allocated = " + _dicomObject.getI(475));
            }
            if(Debug.DEBUG > 2)
                _dicomObject.dumpVRs(((java.io.OutputStream) (_log)), true);
        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace(_log);
        }
        finally
        {
            updateTable();
        }
    }

    private void save(File file, boolean flag, int i)
    {
        try
        {
            TS.changeCompression(_dicomObject, _curTSindex, i);
            if(getBooleanParam("Save.FileMetaInfo"))
                ajustFileMetaInfo(_curTSindex = i);
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            try
            {
                _dicomObject.write(((java.io.OutputStream) (fileoutputstream)), flag, TS.ID[i], getBooleanParam("Save.UndefSeqLen"));
            }
            finally
            {
                fileoutputstream.close();
            }
            _curFile = file;
            _log.println("Save to " + file + '[' + TS.LABEL[_curTSindex] + ']');
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private void saveHtml(File file, String s)
    {
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            PrintWriter printwriter = new PrintWriter(((java.io.OutputStream) (fileoutputstream)));
            try
            {
                _table.toHTML(printwriter, s);
            }
            finally
            {
                printwriter.close();
            }
            _log.println("Save as HTML to " + file);
        }
        catch(Exception exception)
        {
            _log.println(((Object) (exception)));
        }
    }

    private void ajustFileMetaInfo(int i)
        throws DicomException, IllegalValueException
    {
        String s = UID.getUIDEntry(TS.ID[i]).getValue();
        Object obj = ((Object) (_dicomObject.getFileMetaInformation()));
        if(obj == null)
        {
            obj = ((Object) (new FileMetaInformation(_dicomObject, s)));
            ((DicomObject) (obj)).set(34, "EditDicomObject");
            _dicomObject.setFileMetaInformation(((DicomObject) (obj)));
        } else
        {
            ((DicomObject) (obj)).set(31, ((Object) (s)));
            String s1 = _dicomObject.getS(62);
            if(s1 != null && s1.length() != 0)
                ((DicomObject) (obj)).set(29, ((Object) (s1)));
            String s2 = _dicomObject.getS(63);
            if(s2 != null && s2.length() != 0)
                ((DicomObject) (obj)).set(30, ((Object) (s2)));
        }
        updateTable();
        FileMetaInformation.check(((DicomObject) (obj)));
    }

    static 
    {
        PARAM_CHECKS = new Hashtable();
        PARAM_CHECKS.put("Show.FileMetaInfo", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Show.SequenceItem", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Load.PixelData", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Load.Append", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Save.FileMetaInfo", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Save.TransferSyntax", ((Object) (CheckParam.enum(TS.SAVE))));
        PARAM_CHECKS.put("Save.UndefSeqLen", ((Object) (CheckParam.bool())));
        PARAM_CHECKS.put("Verbose", ((Object) (CheckParam.enum(new String[] {
            "0", "1", "2", "3", "4", "5"
        }))));
    }

















}
