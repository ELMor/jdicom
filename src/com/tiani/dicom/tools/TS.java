// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TS.java

package com.tiani.dicom.tools;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import java.io.IOException;
import java.io.PrintStream;

public final class TS
{

    public static final int ID[] = {
        8193, 8194, 8195, 8198, 8197, 8205, 8206, 8213, 8214, 8196, 
        8199, 8200, 8201, 8202, 8203, 8204, 8207, 8208, 8209, 8210, 
        8211, 8212
    };
    public static final String SAVE[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian", "JPEG_14_SelectionValue1"
    };
    public static final String LABEL[] = {
        "ImplicitVRLittleEndian", "ExplicitVRLittleEndian", "ExplicitVRBigEndian", "RLELossless", "JPEG_14_SelectionValue1", "JPEG_14", "JPEG_15", "JPEG_28", "JPEG_29", "JPEG_1", 
        "JPEG_2_4", "JPEG_3_5", "JPEG_6_8", "JPEG_7_9", "JPEG_10_12", "JPEG_11_13", "JPEG_16_18", "JPEG_17_19", "JPEG_20_22", "JPEG_21_23", 
        "JPEG_24_26", "JPEG_25_27", "MG1.1"
    };
    public static final int IVR_LE = 0;
    public static final int EVR_LE = 1;
    public static final int EVR_BE = 2;
    public static final int MG1_1 = LABEL.length - 1;

    public TS()
    {
    }

    public static boolean isCompress(int i)
    {
        return i > 2;
    }

    public static int indexOf(Object obj)
    {
        int i;
        for(i = LABEL.length; i-- > 0;)
            if(LABEL[i].equals(obj))
                return i;

        return i;
    }

    public static int indexOf(int i)
    {
        int j;
        for(j = ID.length; j-- > 0;)
            if(ID[j] == i)
                return j;

        return j;
    }

    public static int indexOf(int i, DicomObject dicomobject)
        throws DicomException, UnknownUIDException
    {
        switch(i)
        {
        case 1: // '\001'
            return indexOf(UID.getUIDEntry(dicomobject.getFileMetaInformation().getS(31)).getConstant());

        case 2: // '\002'
            return 0;

        case 3: // '\003'
            return 1;

        case 4: // '\004'
            return 2;

        case 8: // '\b'
            return MG1_1;

        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        default:
            return 0;
        }
    }

    private static int getPixelSize(DicomObject dicomobject)
    {
        int i = 0;
        for(int j = dicomobject.getSize(1184); j-- > 0;)
            i += ((byte[])dicomobject.get(1184, j)).length;

        return i <= 0 ? 1 : i;
    }

    public static void changeCompression(DicomObject dicomobject, int i, int j)
        throws IOException, DicomException
    {
        if(i != j)
        {
            Compression compression = new Compression(dicomobject);
            if(isCompress(i))
            {
                int k = getPixelSize(dicomobject);
                if(Debug.DEBUG > 0)
                    Debug.out.println("Start Decompression from " + LABEL[i]);
                compression.decompress();
                if(Debug.DEBUG > 0)
                    Debug.out.println("Finished Decompression 1 : " + (float)getPixelSize(dicomobject) / (float)k);
            }
            if(isCompress(j))
            {
                int l = getPixelSize(dicomobject);
                if(Debug.DEBUG > 0)
                    Debug.out.println("Start Compression to " + LABEL[j]);
                compression.compress(ID[j]);
                if(Debug.DEBUG > 0)
                    Debug.out.println("Finished Compression " + (float)l / (float)getPixelSize(dicomobject) + " : 1");
            }
        }
    }

}
