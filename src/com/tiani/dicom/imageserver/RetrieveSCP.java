// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RetrieveSCP.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefCMoveSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.SingleResponse;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.MoveStorageSCU;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import com.tiani.dicom.util.AndDicomObjectFilter;
import com.tiani.dicom.util.DOFFactory;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.io.PrintStream;
import java.util.ArrayList;

// Referenced classes of package com.tiani.dicom.imageserver:
//            MoveStgSCUStrategy, StorageManagerFactory, StorageManager, Param

class RetrieveSCP extends DefCMoveSCP
{

    private AETable aets;
    private StorageManagerFactory storageMgrFty;
    private Param param;

    public RetrieveSCP(Param param1, StorageManagerFactory storagemanagerfactory, AETable aetable)
    {
        aets = aetable;
        storageMgrFty = storagemanagerfactory;
        setParam(param1);
    }

    public void setParam(Param param1)
    {
        param = param1;
    }

    protected IMultiResponse query(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
    {
        String s1 = dimseexchange.localAET();
        String s2 = (String)((DicomObject) (dicommessage)).get(6);
        if(s2 == null || s2.length() == 0)
            return ((IMultiResponse) (new SingleResponse(43264)));
        AET aet = aets.lookup(s2);
        if(aet == null)
            return ((IMultiResponse) (new SingleResponse(43009)));
        ArrayList arraylist;
        StorageManager storagemanager;
        try
        {
            storagemanager = storageMgrFty.getStorageManagerForAET(s1, false);
            if(storagemanager == null)
                return ((IMultiResponse) (SingleResponse.NO_MATCH));
            DicomObject dicomobject = dicommessage.getDataset();
            String s3 = dicomobject.getS(78);
            IDicomObjectFilter aidicomobjectfilter[] = new IDicomObjectFilter[4];
            aidicomobjectfilter[0] = DOFFactory.getPatientDOFFactory().getFilter(dicomobject);
            if(!"PATIENT".equals(((Object) (s3))))
            {
                aidicomobjectfilter[1] = DOFFactory.getStudyDOFFactory().getFilter(dicomobject);
                if(!"STUDY".equals(((Object) (s3))))
                {
                    aidicomobjectfilter[2] = DOFFactory.getSeriesDOFFactory().getFilter(dicomobject);
                    if(!"SERIES".equals(((Object) (s3))))
                    {
                        aidicomobjectfilter[3] = DOFFactory.getImageDOFFactory().getFilter(dicomobject);
                        IDicomObjectFilter idicomobjectfilter = DRFactory.createRefSOPFilter((String)dicomobject.get(62), (String)dicomobject.get(63));
                        if(idicomobjectfilter != null)
                            aidicomobjectfilter[3] = ((IDicomObjectFilter) (new AndDicomObjectFilter(new IDicomObjectFilter[] {
                                aidicomobjectfilter[3], idicomobjectfilter
                            })));
                    }
                }
            }
            DicomDir2 dicomdir2 = storagemanager.getDirInfo();
            dicomdir2.addReadLock();
            try
            {
                arraylist = dicomdir2.listNodePaths(DRFactory.INSTANCE_TYPE_PATH, aidicomobjectfilter);
            }
            finally
            {
                dicomdir2.releaseReadLock();
            }
            if(arraylist.isEmpty())
                return ((IMultiResponse) (SingleResponse.NO_MATCH));
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
            return ((IMultiResponse) (new SingleResponse(42753)));
        }
        try
        {
            MoveStgSCUStrategy movestgscustrategy = new MoveStgSCUStrategy(arraylist, storagemanager.getFileset());
            MoveStorageSCU movestoragescu = new MoveStorageSCU(((com.tiani.dicom.service.MoveStorageSCU.IStrategy) (movestgscustrategy)), dimseexchange.remoteAET(), ((DicomObject) (dicommessage)).getI(4), movestgscustrategy.getInstanceUIDs(), movestgscustrategy.getASIDs());
            movestoragescu.setMaxNumOpInvoked(param.getMaxOperationsInvoked());
            movestoragescu.setMaxPduSize(param.getMaxPduSize());
            movestoragescu.setPriority(param.getMovePriority());
            movestoragescu.setARTIM(param.getReleaseTimeout());
            com.archimed.dicom.network.Response response = movestoragescu.connect(aet.host, aet.port, aet.title, dimseexchange.localAET(), movestgscustrategy.getASIDs());
            if(!movestoragescu.isConnected())
            {
                Debug.out.println("Could not connect to move destination: " + aet);
                return ((IMultiResponse) (new SingleResponse(49152)));
            } else
            {
                return ((IMultiResponse) (movestoragescu));
            }
        }
        catch(Exception exception1)
        {
            Debug.out.println(((Object) (exception1)));
        }
        return ((IMultiResponse) (new SingleResponse(49152)));
    }
}
