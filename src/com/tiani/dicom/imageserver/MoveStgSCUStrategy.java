// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MoveStgSCUStrategy.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.FileSet2;
import com.tiani.dicom.service.MoveStorageSCU;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

final class MoveStgSCUStrategy
    implements com.tiani.dicom.service.MoveStorageSCU.IStrategy
{

    private final Hashtable table = new Hashtable();
    private final String instanceUIDs[];
    private final int asids[];
    private final FileSet2 fileset;

    public MoveStgSCUStrategy(ArrayList arraylist, FileSet2 fileset2)
        throws UnknownUIDException
    {
        instanceUIDs = new String[arraylist.size()];
        asids = new int[instanceUIDs.length];
        for(int i = 0; i < instanceUIDs.length; i++)
        {
            DicomObject dicomobject = ((DRNode[])arraylist.get(i))[3].getDataset();
            instanceUIDs[i] = (String)dicomobject.get(53);
            table.put(((Object) (instanceUIDs[i])), ((Object) (dicomobject)));
            asids[i] = UID.getUIDEntry((String)dicomobject.get(52)).getConstant();
        }

        fileset = fileset2;
    }

    public final String[] getInstanceUIDs()
    {
        return instanceUIDs;
    }

    public final int[] getASIDs()
    {
        return asids;
    }

    public int load(DicomObject dicomobject, String s)
        throws UnknownUIDException, DicomException, IOException
    {
        DicomObject dicomobject1 = (DicomObject)table.get(((Object) (s)));
        fileset.read(dicomobject, dicomobject1);
        return 0;
    }
}
