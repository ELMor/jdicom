// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageManager.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileSet2;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.imageserver:
//            Param

final class StorageManager
{
    private static class StudyFolderInfo
    {

        private final String studyInstUID;
        private long size;
        private long lastModified;

        public void add(File file)
        {
            size += file.length();
            lastModified = Math.max(lastModified, file.lastModified());
        }

        public void delete(File file)
        {
            size -= file.length();
        }

        public String studyInstUID()
        {
            return studyInstUID;
        }

        public long size()
        {
            return size;
        }

        public long lastModified()
        {
            return lastModified;
        }

        public StudyFolderInfo(String s)
        {
            size = 0L;
            lastModified = 0L;
            studyInstUID = s;
        }
    }


    private Param param;
    private final DicomDir2 dirInfo;
    private final FileSet2 fileSet;
    private boolean dirty;
    private int deleteCount;
    private long usedSize;
    private Hashtable studyFolderInfos;
    private Vector sortedInfos;

    public StorageManager(Param param1, DicomDir2 dicomdir2, FileSet2 fileset2)
    {
        dirty = false;
        deleteCount = 0;
        usedSize = 0L;
        studyFolderInfos = null;
        sortedInfos = null;
        param = param1;
        dirInfo = dicomdir2;
        fileSet = fileset2;
    }

    public DicomDir2 getDirInfo()
    {
        return dirInfo;
    }

    public FileSet2 getFileset()
    {
        return fileSet;
    }

    public File getDirFile()
    {
        return fileSet.getDirFile();
    }

    public File getRootDir()
    {
        return fileSet.getRootDir();
    }

    public void setDirty(boolean flag)
    {
        dirty = flag;
    }

    public boolean isDirty()
    {
        return dirty;
    }

    public void setParam(Param param1)
    {
        param = param1;
    }

    public void write(DicomObject dicomobject, DicomObject dicomobject1, String s)
    {
        try
        {
            if(studyFolderInfos == null)
                initInfos();
            File file = fileSet.write(dicomobject, dicomobject1, param.isFileMetaInformation());
            StudyFolderInfo studyfolderinfo = (StudyFolderInfo)studyFolderInfos.get(((Object) (s)));
            if(studyfolderinfo == null)
            {
                studyfolderinfo = new StudyFolderInfo(s);
                studyFolderInfos.put(((Object) (s)), ((Object) (studyfolderinfo)));
            } else
            {
                sortedInfos.removeElement(((Object) (studyfolderinfo)));
            }
            sortedInfos.addElement(((Object) (studyfolderinfo)));
            studyfolderinfo.add(file);
            usedSize += file.length();
            runFIFO();
        }
        catch(Exception exception)
        {
            Debug.out.println(((Throwable) (exception)).getMessage());
        }
    }

    public void writeDicomDir()
        throws IOException, DicomException
    {
        if(!dirty)
            return;
        dirInfo.addReadLock();
        try
        {
            fileSet.getRootDir().mkdirs();
            fileSet.write(dirInfo);
            dirty = false;
        }
        finally
        {
            dirInfo.releaseReadLock();
        }
    }

    public void delete(DicomObject dicomobject, String s)
    {
        try
        {
            if(studyFolderInfos == null)
                initInfos();
            StudyFolderInfo studyfolderinfo = (StudyFolderInfo)studyFolderInfos.get(((Object) (s)));
            if(studyfolderinfo != null)
                studyfolderinfo.delete(fileSet.getRefFile(dicomobject));
            else
                Debug.out.println("Internal error concerning FIFO Storage Management");
            fileSet.delete(dicomobject, true);
        }
        catch(Exception exception)
        {
            Debug.out.println(((Throwable) (exception)).getMessage());
        }
    }

    public boolean compact()
    {
        if(deleteCount == 0)
            return false;
        if(Debug.DEBUG > 0)
            Debug.out.println(fileSet.getFileSetID() + ": Compact Directory Information");
        dirInfo.addWriteLock();
        try
        {
            dirty = true;
            dirInfo.compact();
            deleteCount = 0;
        }
        catch(DicomException dicomexception)
        {
            Debug.out.println("DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
        finally
        {
            dirInfo.releaseWriteLock();
        }
        return true;
    }

    private boolean runFIFO()
        throws IOException, DicomException
    {
        long l = param.getFilesetMaxSize();
        if(Debug.DEBUG > 0)
            Debug.out.println(fileSet.getFileSetID() + ": " + usedSize + " Bytes (" + (100L * usedSize) / l + "%) used");
        boolean flag;
        for(flag = false; usedSize >= l; flag = true)
            deleteStudy((StudyFolderInfo)sortedInfos.firstElement());

        return flag;
    }

    private DRNode[] findStudyNodePath(String s)
    {
        DREntity drentity = dirInfo.getRootEntity();
        for(Iterator iterator = drentity.iterator("PATIENT"); iterator.hasNext();)
        {
            DRNode drnode = (DRNode)iterator.next();
            DREntity drentity1 = drnode.getLowerEntity();
            if(drentity1 != null)
            {
                DRNode drnode1 = drentity1.find("STUDY", s);
                if(drnode1 != null)
                    return (new DRNode[] {
                        drnode, drnode1
                    });
            }
        }

        return null;
    }

    private void deleteStudy(StudyFolderInfo studyfolderinfo)
        throws IOException, DicomException
    {
        String s = studyfolderinfo.studyInstUID();
        sortedInfos.removeElement(((Object) (studyfolderinfo)));
        studyFolderInfos.remove(((Object) (s)));
        ArrayList arraylist = new ArrayList();
        try
        {
            dirInfo.addWriteLock();
            DRNode adrnode[] = findStudyNodePath(s);
            if(adrnode == null)
            {
                Debug.out.println("ERROR: Missing Directory Record for Study - " + s);
                return;
            }
            DREntity drentity = adrnode[1].getLowerEntity();
            if(drentity == null)
            {
                Debug.out.println("WARNING: No Series Directory Records for Study - " + s);
            } else
            {
                for(Iterator iterator1 = drentity.iterator(); iterator1.hasNext();)
                {
                    DRNode drnode1 = (DRNode)iterator1.next();
                    DREntity drentity1 = drnode1.getLowerEntity();
                    if(drentity1 == null)
                    {
                        Debug.out.println("WARNING: No Object Directory Records for Series - " + drnode1.getKey());
                    } else
                    {
                        DRNode drnode;
                        for(Iterator iterator2 = drentity1.iterator(); iterator2.hasNext(); arraylist.add(((Object) (drnode.getDataset()))))
                            drnode = (DRNode)iterator2.next();

                    }
                }

            }
            if(adrnode[0].getLowerEntity().getCount() > 1)
                adrnode[1].delete();
            else
                adrnode[0].delete();
        }
        finally
        {
            dirInfo.releaseWriteLock();
        }
        for(Iterator iterator = ((AbstractList) (arraylist)).iterator(); iterator.hasNext(); fileSet.delete((DicomObject)iterator.next(), true));
        usedSize -= studyfolderinfo.size();
        deleteCount++;
    }

    private void initInfos()
        throws DicomException
    {
        usedSize = 0L;
        studyFolderInfos = new Hashtable();
        sortedInfos = new Vector();
        DREntity drentity = dirInfo.getRootEntity();
        for(Iterator iterator = drentity.iterator("PATIENT"); iterator.hasNext();)
        {
            DRNode drnode = (DRNode)iterator.next();
            DREntity drentity1 = drnode.getLowerEntity();
            if(drentity1 != null)
            {
                for(Iterator iterator1 = drentity1.iterator("STUDY"); iterator1.hasNext();)
                {
                    DRNode drnode1 = (DRNode)iterator1.next();
                    DREntity drentity2 = drnode1.getLowerEntity();
                    if(drentity2 != null)
                    {
                        DicomObject dicomobject = drnode1.getDataset();
                        String s = (String)dicomobject.get(425);
                        for(Iterator iterator2 = drentity2.iterator("SERIES"); iterator2.hasNext();)
                        {
                            DRNode drnode2 = (DRNode)iterator2.next();
                            DREntity drentity3 = drnode2.getLowerEntity();
                            if(drentity3 != null)
                            {
                                DicomObject dicomobject1;
                                StudyFolderInfo studyfolderinfo;
                                for(Iterator iterator3 = drentity3.iterator(); iterator3.hasNext(); studyfolderinfo.add(fileSet.getRefFile(dicomobject1)))
                                {
                                    DRNode drnode3 = (DRNode)iterator3.next();
                                    dicomobject1 = drnode3.getDataset();
                                    studyfolderinfo = (StudyFolderInfo)studyFolderInfos.get(((Object) (s)));
                                    if(studyfolderinfo == null)
                                    {
                                        studyfolderinfo = new StudyFolderInfo(s);
                                        studyFolderInfos.put(((Object) (s)), ((Object) (studyfolderinfo)));
                                        sortedInfos.addElement(((Object) (studyfolderinfo)));
                                    }
                                }

                            }
                        }

                    }
                }

            }
        }

        for(Enumeration enumeration = sortedInfos.elements(); enumeration.hasMoreElements();)
            usedSize += ((StudyFolderInfo)enumeration.nextElement()).size();

        sortInfos();
    }

    private void sortInfos()
    {
        for(int i = 0; i < sortedInfos.size() - 1; i++)
        {
            StudyFolderInfo studyfolderinfo = (StudyFolderInfo)sortedInfos.elementAt(i);
            int j = i;
            for(int k = i + 1; k < sortedInfos.size(); k++)
            {
                StudyFolderInfo studyfolderinfo1 = (StudyFolderInfo)sortedInfos.elementAt(k);
                if(studyfolderinfo.lastModified() > studyfolderinfo1.lastModified())
                {
                    studyfolderinfo = studyfolderinfo1;
                    j = k;
                }
            }

            if(j != i)
            {
                sortedInfos.setElementAt(sortedInfos.elementAt(i), j);
                sortedInfos.setElementAt(((Object) (studyfolderinfo)), i);
            }
        }

    }
}
