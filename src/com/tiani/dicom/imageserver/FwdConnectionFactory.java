// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FwdConnectionFactory.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.service.DimseRouter;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import java.io.IOException;
import java.net.Socket;

// Referenced classes of package com.tiani.dicom.imageserver:
//            Param, FwdConnectionPool

abstract class FwdConnectionFactory
    implements com.tiani.dicom.service.DimseRouter.IConnectionFactory
{
    static class Retrieve extends FwdConnectionFactory
    {

        protected boolean isFwd()
        {
            return param.isFwdRetrieve();
        }

        Retrieve(Param param1, AETable aetable, FwdConnectionPool fwdconnectionpool)
        {
            super(param1, aetable, fwdconnectionpool);
        }
    }

    static class Query extends FwdConnectionFactory
    {

        protected boolean isFwd()
        {
            return param.isFwdQuery();
        }

        Query(Param param1, AETable aetable, FwdConnectionPool fwdconnectionpool)
        {
            super(param1, aetable, fwdconnectionpool);
        }
    }

    static class StorageCmt extends FwdConnectionFactory
    {

        protected boolean isFwd()
        {
            return param.isFwdStorageCmt();
        }

        StorageCmt(Param param1, AETable aetable, FwdConnectionPool fwdconnectionpool)
        {
            super(param1, aetable, fwdconnectionpool);
        }
    }


    protected Param param;
    private final AETable aets;
    private final FwdConnectionPool pool;
    private static final int AS_IDS[] = {
        4132, 4135, 4138, 4133, 4136, 4139, 4100
    };
    private static final int ONLY_DEF_TS[] = {
        8193
    };

    protected FwdConnectionFactory(Param param1, AETable aetable, FwdConnectionPool fwdconnectionpool)
    {
        aets = aetable;
        pool = fwdconnectionpool;
        setParam(param1);
    }

    protected abstract boolean isFwd();

    public void setParam(Param param1)
    {
        param = param1;
    }

    public void receivedRSP(DimseExchange dimseexchange, DimseExchange dimseexchange1, int i, int j, DicomMessage dicommessage)
    {
        if(!param.isFwdKeepOpen() && !Status.isPending(j))
            pool.removeAndRelease(dimseexchange.getAssociation());
    }

    private Request createRequest(VerboseAssociation verboseassociation)
        throws IllegalValueException
    {
        Request request = new Request();
        request.setCalledTitle(param.getFwdAET());
        request.setCallingTitle(param.isFwdCallingTitle() ? verboseassociation.remoteAET() : verboseassociation.localAET());
        request.setMaxPduSize(param.getMaxPduSize());
        if(param.getMaxOperationsInvoked() != 1)
        {
            request.setMaxOperationsInvoked(param.getMaxOperationsInvoked());
            request.setMaxOperationsPerformed(1);
        }
        for(int i = 0; i < AS_IDS.length; i++)
            request.addPresentationContext(AS_IDS[i], ONLY_DEF_TS);

        return request;
    }

    public DimseExchange getConnectionForDimseRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, IllegalValueException, UnknownUIDException
    {
        if(!isFwd())
            return null;
        VerboseAssociation verboseassociation = dimseexchange.getAssociation();
        DimseExchange dimseexchange1 = pool.get(verboseassociation);
        if(dimseexchange1 != null)
            return dimseexchange1;
        AET aet = aets.lookup(param.getFwdAET());
        if(aet == null)
            throw new IOException("Unknown AET: " + param.getFwdAET());
        Socket socket = new Socket(aet.host, aet.port);
        Requestor requestor = new Requestor(socket, createRequest(verboseassociation));
        VerboseAssociation verboseassociation1 = requestor.openAssoc();
        if(verboseassociation1 == null)
        {
            throw new IOException("Association rejected: " + requestor.response());
        } else
        {
            Acknowledge acknowledge = (Acknowledge)requestor.response();
            DimseExchange dimseexchange2 = new DimseExchange(verboseassociation1, ((com.tiani.dicom.framework.DimseRqManager) (null)), false, false, acknowledge.getMaxOperationsInvoked());
            (new Thread(((Runnable) (dimseexchange2)))).start();
            pool.put(verboseassociation, dimseexchange2);
            return dimseexchange2;
        }
    }

    public DimseExchange getConnectionForCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException
    {
        if(!isFwd())
            return null;
        DimseExchange dimseexchange1 = pool.get(dimseexchange.getAssociation());
        if(dimseexchange1 != null)
            return dimseexchange1;
        else
            throw new IOException("No connection for cancel request available");
    }

}
