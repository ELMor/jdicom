// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StudyContentNotificationSCU.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.SimpleStorageSCU;
import com.tiani.dicom.util.AET;
import com.tiani.dicom.util.AETable;
import com.tiani.dicom.util.BasicStudyDescriptor;
import com.tiani.dicom.util.UIDUtils;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

// Referenced classes of package com.tiani.dicom.imageserver:
//            Param, StorageManagerFactory, StorageManager

class StudyContentNotificationSCU
{
    private class AssocMapItem
    {

        DicomDir2 dirInfo;
        DicomObject retrImageInfo;
        final Hashtable studyDescriptors = new Hashtable();

        public boolean addRetrImage(DicomObject dicomobject)
            throws DicomException
        {
            String s = (String)dicomobject.get(425);
            BasicStudyDescriptor basicstudydescriptor = (BasicStudyDescriptor)studyDescriptors.get(((Object) (s)));
            if(basicstudydescriptor == null)
            {
                basicstudydescriptor = new BasicStudyDescriptor(UIDUtils.createUID());
                studyDescriptors.put(((Object) (s)), ((Object) (basicstudydescriptor)));
            }
            return basicstudydescriptor.addRetrImage(dicomobject, retrImageInfo);
        }

        public Hashtable getStudyDescriptors()
        {
            if(param.isSCNTotal() && dirInfo != null)
                try
                {
                    return loadStudyDescriptors();
                }
                catch(Exception exception)
                {
                    Debug.out.println(((Object) (exception)));
                }
            return studyDescriptors;
        }

        private DRNode findStudyNode(String s, String s1)
        {
            DREntity drentity = dirInfo.getRootEntity();
            DRNode drnode = drentity.find("PATIENT", s);
            if(drnode == null)
                return null;
            DREntity drentity1 = drnode.getLowerEntity();
            if(drentity1 == null)
                return null;
            else
                return drentity1.find("STUDY", s1);
        }

        private Hashtable loadStudyDescriptors()
            throws DicomException
        {
            Hashtable hashtable = new Hashtable();
            dirInfo.addReadLock();
            try
            {
                for(Enumeration enumeration = studyDescriptors.elements(); enumeration.hasMoreElements();)
                {
                    BasicStudyDescriptor basicstudydescriptor = (BasicStudyDescriptor)enumeration.nextElement();
                    String s = ((DicomObject) (basicstudydescriptor)).getS(148);
                    String s1 = ((DicomObject) (basicstudydescriptor)).getS(147);
                    String s2 = ((DicomObject) (basicstudydescriptor)).getS(427);
                    String s3 = ((DicomObject) (basicstudydescriptor)).getS(425);
                    DRNode drnode = findStudyNode(s, s3);
                    if(drnode != null)
                    {
                        BasicStudyDescriptor basicstudydescriptor1 = new BasicStudyDescriptor(UIDUtils.createUID());
                        hashtable.put(((Object) (s3)), ((Object) (basicstudydescriptor1)));
                        DREntity drentity = drnode.getLowerEntity();
                        if(drentity != null)
                        {
                            for(Iterator iterator = drentity.iterator("SERIES"); iterator.hasNext();)
                            {
                                DRNode drnode1 = (DRNode)iterator.next();
                                DicomObject dicomobject = drnode1.getDataset();
                                String s4 = (String)dicomobject.get(426);
                                DREntity drentity1 = drnode1.getLowerEntity();
                                if(drentity1 != null)
                                {
                                    DicomObject dicomobject2;
                                    for(Iterator iterator1 = drentity1.iterator(); iterator1.hasNext(); basicstudydescriptor1.addRetrImage(dicomobject2, retrImageInfo))
                                    {
                                        DRNode drnode2 = (DRNode)iterator1.next();
                                        DicomObject dicomobject1 = drnode2.getDataset();
                                        dicomobject2 = new DicomObject();
                                        dicomobject2.set(147, ((Object) (s1)));
                                        dicomobject2.set(148, ((Object) (s)));
                                        dicomobject2.set(425, ((Object) (s3)));
                                        dicomobject2.set(427, ((Object) (s2)));
                                        dicomobject2.set(426, ((Object) (s4)));
                                        dicomobject2.set(62, dicomobject1.get(52));
                                        dicomobject2.set(63, dicomobject1.get(53));
                                    }

                                }
                            }

                        }
                    }
                }

            }
            finally
            {
                dirInfo.releaseReadLock();
            }
            return hashtable;
        }

        public AssocMapItem(VerboseAssociation verboseassociation)
        {
            dirInfo = null;
            retrImageInfo = null;
            try
            {
                StorageManager storagemanager = storageMgrFty.getStorageManagerForAET(verboseassociation.localAET(), false);
                if(storagemanager != null)
                {
                    dirInfo = storagemanager.getDirInfo();
                    retrImageInfo = BasicStudyDescriptor.getRetrieveInfo(verboseassociation.localAET(), dirInfo.getFileSetID(), dirInfo.getFileSetUID());
                }
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
            }
        }
    }


    private static final int ONLY_SCN_ASID[] = {
        4099
    };
    private Param param;
    private final AETable aets;
    private final StorageManagerFactory storageMgrFty;
    private final Hashtable assocMap = new Hashtable();

    public StudyContentNotificationSCU(Param param1, StorageManagerFactory storagemanagerfactory, AETable aetable)
    {
        aets = aetable;
        storageMgrFty = storagemanagerfactory;
        setParam(param1);
    }

    public void setParam(Param param1)
    {
        param = param1;
    }

    public void add(VerboseAssociation verboseassociation, DicomObject dicomobject)
        throws DicomException
    {
        AssocMapItem assocmapitem = (AssocMapItem)assocMap.get(((Object) (verboseassociation)));
        if(assocmapitem == null)
        {
            assocmapitem = new AssocMapItem(verboseassociation);
            assocMap.put(((Object) (verboseassociation)), ((Object) (assocmapitem)));
        }
        assocmapitem.addRetrImage(dicomobject);
    }

    public void execute(VerboseAssociation verboseassociation)
    {
        AssocMapItem assocmapitem = (AssocMapItem)assocMap.remove(((Object) (verboseassociation)));
        if(assocmapitem == null || !param.isSCN())
            return;
        String s = param.getSCNAET();
        AET aet = aets.lookup(s);
        if(aet == null)
        {
            Debug.out.println("WARNING: SCN.SCP[" + s + "] is not a specified AET.");
            Debug.out.println("Cannot perform Study Content Notification.");
            return;
        }
        try
        {
            Hashtable hashtable = assocmapitem.getStudyDescriptors();
            VerboseAssociation verboseassociation1 = SimpleStorageSCU.openAssociation(aet.host, aet.port, aet.title, verboseassociation.localAET(), ONLY_SCN_ASID);
            if(verboseassociation1 == null)
            {
                Debug.out.println("Could not open Association for Study Content Notification to " + aet.title);
                return;
            }
            try
            {
                for(Enumeration enumeration = hashtable.elements(); enumeration.hasMoreElements(); SimpleStorageSCU.sendDicomObject(verboseassociation1, (DicomObject)enumeration.nextElement()));
            }
            finally
            {
                SimpleStorageSCU.closeAssociation(verboseassociation1);
            }
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }



}
