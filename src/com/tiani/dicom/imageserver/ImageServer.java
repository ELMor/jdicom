// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   ImageServer.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.kcmultimedia.demo.SCMEvent;
import com.kcmultimedia.demo.SCMEventListener;
import com.kcmultimedia.demo.SCMEventManager;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.AdvancedAcceptancePolicy;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.service.DimseRouter;
import com.tiani.dicom.service.StorageCmtSCP;
import com.tiani.dicom.util.AETable;
import com.tiani.rmicfg.IServer;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.imageserver:
//            Param, StorageManagerFactory, StudyContentNotificationSCU, StorageSCP,
//            QuerySCP, RetrieveSCP, StgCmtSCPStrategy, FwdConnectionPool,
//            FwdConnectionFactory

public class ImageServer
    implements IServer, SCMEventListener
{

    private Param param;
    private final AETable aets;
    private final StorageManagerFactory storageMgrFty;
    private final StorageSCP storageSCP;
    private final QuerySCP querySCP;
    private final RetrieveSCP retrieveSCP;
    private final StgCmtSCPStrategy stgCmtSCPStrategy;
    private final StorageCmtSCP storageCmtSCP;
    private final StudyContentNotificationSCU scnSCU;
    private final FwdConnectionPool fwdConnectionPool;
    private final FwdConnectionFactory fwdStorageCmtConnectionFactory;
    private final FwdConnectionFactory fwdQueryConnectionFactory;
    private final FwdConnectionFactory fwdRetrieveConnectionFactory;
    private final DimseRouter storageCmtRouter;
    private final DimseRouter queryRouter;
    private final DimseRouter retrieveRouter;
    private Acceptor acceptor;
    private Thread thread;
    private final DimseRqManager rqManager;
    private final AdvancedAcceptancePolicy acceptancePolicy;
    private DefAcceptorListener acceptorListener;
    static final int IMAGE_STORAGE_AS[] = {
        4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4130, 4131,
        4148, 4149, 4151, 4153, 4157, 4158, 4159, 4161, 4162, 4163,
        4164, 4176, 4177, 4178, 4179, 4180, 4181
    };
    static final int NON_IMAGE_STORAGE_AS[] = {
        4165, 4154, 4155, 4156, 4182, 4183, 4184, 4166, 4167, 4168,
        4194, 4124, 4125, 4126, 4127, 4152, 4147, 4196, 4195
    };

    public ImageServer()
    {
        this(((Properties) (null)));
    }

    public ImageServer(Properties properties)
    {
        aets = new AETable();
        acceptor = null;
        thread = null;
        rqManager = new DimseRqManager();
        acceptancePolicy = new AdvancedAcceptancePolicy();
        acceptorListener = null;
        SCMEventManager scmeventmanager = SCMEventManager.getInstance();
        scmeventmanager.addSCMEventListener(((SCMEventListener) (this)));
        param = new Param(properties == null ? loadDefProperties() : properties);
        Debug.DEBUG = param.getVerbose();
        updateAETs(aets, param.getAETProperties());
        storageMgrFty = new StorageManagerFactory(param);
        scnSCU = new StudyContentNotificationSCU(param, storageMgrFty, aets);
        storageSCP = new StorageSCP(param, storageMgrFty, scnSCU);
        querySCP = new QuerySCP(param, storageMgrFty);
        retrieveSCP = new RetrieveSCP(param, storageMgrFty, aets);
        stgCmtSCPStrategy = new StgCmtSCPStrategy(storageMgrFty, aets);
        storageCmtSCP = new StorageCmtSCP(((com.tiani.dicom.service.StorageCmtSCP.IStrategy) (stgCmtSCPStrategy)), aets);
        fwdConnectionPool = new FwdConnectionPool();
        fwdStorageCmtConnectionFactory = ((FwdConnectionFactory) (new FwdConnectionFactory.StorageCmt(param, aets, fwdConnectionPool)));
        fwdQueryConnectionFactory = ((FwdConnectionFactory) (new FwdConnectionFactory.Query(param, aets, fwdConnectionPool)));
        fwdRetrieveConnectionFactory = ((FwdConnectionFactory) (new FwdConnectionFactory.Retrieve(param, aets, fwdConnectionPool)));
        storageCmtRouter = new DimseRouter(((com.tiani.dicom.service.DimseRouter.IConnectionFactory) (fwdStorageCmtConnectionFactory)), ((com.tiani.dicom.framework.IDimseRqListener) (storageCmtSCP)));
        queryRouter = new DimseRouter(((com.tiani.dicom.service.DimseRouter.IConnectionFactory) (fwdQueryConnectionFactory)), ((com.tiani.dicom.framework.IDimseRqListener) (querySCP)));
        retrieveRouter = new DimseRouter(((com.tiani.dicom.service.DimseRouter.IConnectionFactory) (fwdRetrieveConnectionFactory)), ((com.tiani.dicom.framework.IDimseRqListener) (retrieveSCP)));
        updateStorageCmtSCP(param);
        initRqManager();
        updateAcceptancePolicy();
        acceptorListener = new DefAcceptorListener(param.isMultiThreadAssoc(), rqManager, param.isQueueRQ(), false);
        acceptorListener.setARTIM(param.getReleaseTimeout());
    }

    private void updateAcceptancePolicy()
    {
        try
        {
            acceptancePolicy.setCalledTitles(param.getCalledTitles());
            acceptancePolicy.setCallingTitles(param.getCallingTitles());
            acceptancePolicy.setMaxPduSize(param.getMaxPduSize());
            acceptancePolicy.setMaxOperationsInvoked(param.getMaxOperationsInvoked());
            if(param.isVerification())
                acceptancePolicy.addPresentationContext(4097, param.getVerificationTS());
            if(param.isStorage())
            {
                for(int i = 0; i < IMAGE_STORAGE_AS.length; i++)
                    acceptancePolicy.addPresentationContext(IMAGE_STORAGE_AS[i], param.getImageStorageTS());

                for(int j = 0; j < NON_IMAGE_STORAGE_AS.length; j++)
                    acceptancePolicy.addPresentationContext(NON_IMAGE_STORAGE_AS[j], param.getNonImageStorageTS());

            }
            if(param.isStorageCmt())
                acceptancePolicy.addPresentationContext(4100, param.getStorageCmtTS());
            if(param.isQRPatRoot())
            {
                acceptancePolicy.addPresentationContext(4132, param.getQueryRetrieveTS());
                acceptancePolicy.addPresentationContext(4133, param.getQueryRetrieveTS());
            }
            if(param.isQRStudyRoot())
            {
                acceptancePolicy.addPresentationContext(4135, param.getQueryRetrieveTS());
                acceptancePolicy.addPresentationContext(4136, param.getQueryRetrieveTS());
            }
            if(param.isQRPatStudyOnly())
            {
                acceptancePolicy.addPresentationContext(4138, param.getQueryRetrieveTS());
                acceptancePolicy.addPresentationContext(4139, param.getQueryRetrieveTS());
            }
        }
        catch(Exception exception)
        {
            throw new RuntimeException(((Throwable) (exception)).toString());
        }
    }

    private void initRqManager()
    {
        rqManager.regCEchoScp("1.2.840.10008.1.1", DefCEchoSCP.getInstance());
        rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.1.1", ((com.tiani.dicom.framework.IDimseRqListener) (queryRouter)));
        rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.1.2", ((com.tiani.dicom.framework.IDimseRqListener) (retrieveRouter)));
        rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.2.1", ((com.tiani.dicom.framework.IDimseRqListener) (queryRouter)));
        rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.2.2", ((com.tiani.dicom.framework.IDimseRqListener) (retrieveRouter)));
        rqManager.regCFindScp("1.2.840.10008.5.1.4.1.2.3.1", ((com.tiani.dicom.framework.IDimseRqListener) (queryRouter)));
        rqManager.regCMoveScp("1.2.840.10008.5.1.4.1.2.3.2", ((com.tiani.dicom.framework.IDimseRqListener) (retrieveRouter)));
        rqManager.regNActionScp("1.2.840.10008.1.20.1", ((com.tiani.dicom.framework.IDimseRqListener) (storageCmtRouter)));
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.2", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.4", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.7", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.2", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.12.3", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.20", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.1.29", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.1.20", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.128", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.3", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.5", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.6", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.2", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.3", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.77.1.4", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.1.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.2.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.1.3.1", storageSCP.getImageSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.11.1", storageSCP.getGspsSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.2", storageSCP.getRtDoseSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.3", storageSCP.getRtStructuredSetSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.5", storageSCP.getRtPlanSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.4", storageSCP.getRtTreatRecordSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.6", storageSCP.getRtTreatRecordSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.481.7", storageSCP.getRtTreatRecordSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.11", storageSCP.getSrSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.22", storageSCP.getSrSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.33", storageSCP.getSrSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.88.59", storageSCP.getKoSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.8", storageSCP.getOverlaySCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.9", storageSCP.getCurveSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.10", storageSCP.getModalityLutSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.11", storageSCP.getVoiLutSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.129", storageSCP.getCurveSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.1.27", storageSCP.getStoredPrintSCP());
        rqManager.regCStoreScp("1.2.840.10008.5.1.4.1.1.66", storageSCP.getRawDataSCP());
        rqManager.regCStoreScp("1.2.826.0.1.3680043.2.242.1.1", storageSCP.getRawDataSCP());
    }

    static void updateAETs(AETable aetable, String s)
    {
        ((Hashtable) (aetable)).clear();
        if(s == null)
            return;
        try
        {
            Object obj = s.indexOf(':') <= 1 ? ((Object) (new FileInputStream(s))) : ((Object) ((new URL(s)).openStream()));
            try
            {
                ((Properties) (aetable)).load(((InputStream) (obj)));
                if(Debug.DEBUG > 0)
                    Debug.out.println("Load AET properties from " + s);
            }
            finally
            {
                ((InputStream) (obj)).close();
            }
            if(Debug.DEBUG > 0)
                ((Properties) (aetable)).list(Debug.out);
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    private void updateStorageCmtSCP(Param param1)
    {
        storageCmtSCP.setTiming(param1.getStorageCmtTiming());
        storageCmtSCP.setAssocMode(param1.getStorageCmtAssocMode());
        storageCmtSCP.setMaxPduSize(param1.getMaxPduSize());
        storageCmtSCP.setARTIM(param1.getReleaseTimeout());
    }

    public String getType()
        throws RemoteException
    {
        return "ImageServer";
    }

    public void setProperties(Properties properties)
        throws RemoteException
    {
        param = new Param(properties);
        Debug.DEBUG = param.getVerbose();
        updateAETs(aets, param.getAETProperties());
        updateStorageCmtSCP(param);
        storageMgrFty.setParam(param);
        scnSCU.setParam(param);
        storageSCP.setParam(param);
        querySCP.setParam(param);
        retrieveSCP.setParam(param);
        fwdQueryConnectionFactory.setParam(param);
        fwdRetrieveConnectionFactory.setParam(param);
        updateAcceptancePolicy();
        acceptorListener.setQueueRQ(param.isQueueRQ());
        acceptorListener.setARTIM(param.getReleaseTimeout());
        acceptorListener.setStartThread(param.isMultiThreadAssoc());
        if(acceptor != null)
        {
            acceptor.setARTIM(param.getReleaseTimeout());
            acceptor.setStartThread(param.isMultiThreadTCP());
        }
    }

    public String[] getPropertyNames()
        throws RemoteException
    {
        return Param.KEYS;
    }

    public Properties getProperties()
        throws RemoteException
    {
        return param.getProperties();
    }

    public void start()
        throws RemoteException
    {
        if(thread != null)
            throw new IllegalStateException("server is running");
        try
        {
            Debug.out.println("Start ImageServer v1.7.29 at " + ((Calendar) (new GregorianCalendar())).getTime() + " with " + param);
            acceptor = new Acceptor(new ServerSocket(param.getPort()), param.isMultiThreadTCP(), ((com.tiani.dicom.framework.IAcceptancePolicy) (acceptancePolicy)), ((com.tiani.dicom.framework.IAcceptorListener) (acceptorListener)));
            acceptor.setARTIM(param.getAssocTimeout(), param.getReleaseTimeout());
            acceptor.addAssociationListener(((com.tiani.dicom.framework.IAssociationListener) (storageSCP)));
            thread = new Thread(((Runnable) (acceptor)));
            thread.start();
            Debug.out.println("Waiting for invocations from clients...");
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    public void start(Properties properties)
        throws RemoteException
    {
        setProperties(properties);
        start();
    }

    public void stop(boolean flag, boolean flag1)
        throws RemoteException
    {
        try
        {
            if(thread == null)
                throw new IllegalStateException("server is not running");
            acceptor.stop(flag);
            if(flag1)
                thread.join();
            synchronized(this)
            {
                thread = null;
                acceptor = null;
            }
            Debug.out.println("Stopped ImageServer v1.7.31 at " + ((Calendar) (new GregorianCalendar())).getTime());
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(Debug.out);
            throw new RemoteException("", throwable);
        }
    }

    public boolean isRunning()
        throws RemoteException
    {
        return thread != null;
    }

    public static void setLog(PrintStream printstream)
    {
        Debug.out = printstream;
    }

    private static Properties loadDefProperties()
    {
        Properties properties = new Properties();
        InputStream inputstream = (com.tiani.dicom.imageserver.ImageServer.class).getResourceAsStream("ImageServer.properties");
        try
        {
            properties.load(inputstream);
            inputstream.close();
            Debug.out.println("Load default properties");
        }
        catch(Exception exception)
        {
            throw new RuntimeException("ImageServer.class.getResourceAsStream(\"ImageServer.properties\") failed!");
        }
        return properties;
    }

    static Properties loadProperties(String s)
    {
        try
        {
            Properties properties = new Properties();
            File file = new File(s);
            System.out.println("load properties from " + file.getAbsolutePath());
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                properties.load(((InputStream) (fileinputstream)));
                Properties properties1 = properties;
                return properties1;
            }
            finally
            {
                ((InputStream) (fileinputstream)).close();
            }
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return null;
    }

    public static void main(String args[])
    {
        try
        {
            String s = args.length <= 0 ? "ImageServer.properties" : args[0];
            (new ImageServer(loadProperties(s))).start();
        }
        catch(Throwable throwable)
        {
           throwable.printStackTrace();
        }
    }

    public void handleSCMEvent(SCMEvent scmevent)
    {
        if(scmevent.getID() == 1)
            try
            {
                if(isRunning())
                    stop(true, false);
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(System.out);
            }
    }

}
