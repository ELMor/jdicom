// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageSCP.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DefCStoreSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.iod.CompositeIOD;
import com.tiani.dicom.iod.IODError;
import com.tiani.dicom.iod.IODErrorSummary;
import com.tiani.dicom.media.DRFactory;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.service.IconFactory;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.imageserver:
//            Param, StorageManagerFactory, StorageManager, StudyContentNotificationSCU, 
//            WebsiteBuilder

class StorageSCP
    implements IAssociationListener
{
    private final class RawDataSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "RAW DATA";
        }

        private RawDataSCP()
        {
        }

    }

    private final class StoredPrintSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "STORED PRINT";
        }

        private StoredPrintSCP()
        {
        }

    }

    private final class KoStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "KEY OBJECT DOC";
        }

        private KoStoreSCP()
        {
        }

    }

    private final class SrStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "SR DOCUMENT";
        }

        private SrStoreSCP()
        {
        }

    }

    private final class ModalityLutStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "MODALITY LUT";
        }

        private ModalityLutStoreSCP()
        {
        }

    }

    private final class VoiLutStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "VOI LUT";
        }

        private VoiLutStoreSCP()
        {
        }

    }

    private final class CurveStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "CURVE";
        }

        private CurveStoreSCP()
        {
        }

    }

    private final class OverlayStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "OVERLAY";
        }

        private OverlayStoreSCP()
        {
        }

    }

    private final class RtTreatRecordStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "RT TREAT RECORD";
        }

        private RtTreatRecordStoreSCP()
        {
        }

    }

    private final class RtStructuredSetStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "RT STRUCTURE SET";
        }

        private RtStructuredSetStoreSCP()
        {
        }

    }

    private final class RtPlanStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "RT PLAN";
        }

        private RtPlanStoreSCP()
        {
        }

    }

    private final class RtDoseStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "RT DOSE";
        }

        private RtDoseStoreSCP()
        {
        }

    }

    private final class GspsStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "PRESENTATION";
        }

        private GspsStoreSCP()
        {
        }

    }

    private final class ImageStoreSCP extends MyCStoreSCP
    {

        public String getDRType()
        {
            return "IMAGE";
        }

        protected void adjustTS(StoreContext storecontext)
            throws DicomException, UnknownUIDException, IOException
        {
            UIDEntry uidentry = param.getMediaTS4Img();
            if(uidentry == null)
                return;
            DicomObject dicomobject = storecontext.ds.getFileMetaInformation();
            String s = dicomobject.getS(31);
            if(uidentry.getValue().equals(((Object) (s))))
                return;
            int i = UID.getUIDEntry(s).getConstant();
            if(i != 8195 && i != 8194 && i != 8193)
            {
                float f = StorageSCP.getPixelSize(storecontext.ds);
                Compression compression1 = new Compression(storecontext.ds);
                compression1.decompress();
                if(Debug.DEBUG > 1)
                    Debug.out.println("Finished Decompression 1 : " + (float)StorageSCP.getPixelSize(storecontext.ds) / f);
            }
            storecontext.pm = PixelMatrix.create(storecontext.ds);
            if(param.isCompress())
            {
                Compression compression = new Compression(storecontext.ds);
                compression.compress(uidentry.getConstant());
                if(Debug.DEBUG > 1)
                    Debug.out.println("Finished Compression " + (float)storecontext.pm.getPixelData().length / (float)StorageSCP.getPixelSize(storecontext.ds) + " : 1");
            }
            storecontext.ds.getFileMetaInformation().set(31, ((Object) (param.getMediaTS4Img().getValue())));
        }

        protected void setPixelMatrixAndLUT(StoreContext storecontext)
            throws DicomException, UnknownUIDException, IOException
        {
            if(storecontext.pm == null)
            {
                DicomObject dicomobject = storecontext.ds.getFileMetaInformation();
                String s = dicomobject.getS(31);
                int i = UID.getUIDEntry(s).getConstant();
                if(i != 8195 && i != 8194 && i != 8193)
                {
                    byte abyte0[][] = StorageSCP.getPixelData(storecontext.ds);
                    float f = StorageSCP.getPixelSize(storecontext.ds);
                    Compression compression = new Compression(storecontext.ds);
                    compression.decompress();
                    if(Debug.DEBUG > 1)
                        Debug.out.println("Finished Decompression 1 : " + (float)StorageSCP.getPixelSize(storecontext.ds) / f);
                    storecontext.pm = PixelMatrix.create(storecontext.ds);
                    StorageSCP.setPixelData(storecontext.ds, abyte0);
                    dicomobject.set(31, ((Object) (s)));
                } else
                {
                    storecontext.pm = PixelMatrix.create(storecontext.ds);
                }
            }
            if(storecontext.lut == null && storecontext.pm.isMonochrome())
                storecontext.lut = LUTFactory.createByteLUT(storecontext.pm, storecontext.ds);
        }

        protected void createIcon(DicomObject dicomobject, StoreContext storecontext)
        {
            IconFactory iconfactory = param.getIconFactory();
            if(iconfactory == null)
                return;
            try
            {
                setPixelMatrixAndLUT(storecontext);
                dicomobject.set(699, ((Object) (iconfactory.createIconDicomObject(storecontext.pm, storecontext.lut))));
                if(Debug.DEBUG > 1)
                    Debug.out.println("Created icon for received image");
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(Debug.out);
                Debug.out.println("Failed to create icon for received image: " + exception);
            }
        }

        private ImageStoreSCP()
        {
        }

    }

    private abstract class MyCStoreSCP extends DefCStoreSCP
    {

        public abstract String getDRType();

        protected void adjustTS(StoreContext storecontext)
            throws DicomException, UnknownUIDException, IOException
        {
            UIDEntry uidentry = param.getMediaTS4NonImg();
            if(uidentry == null)
            {
                return;
            } else
            {
                storecontext.ds.getFileMetaInformation().set(31, ((Object) (uidentry.getValue())));
                return;
            }
        }

        protected void setPixelMatrixAndLUT(StoreContext storecontext)
            throws DicomException, UnknownUIDException, IOException
        {
        }

        private boolean storeToFileset(String s, StoreContext storecontext)
            throws StorageException
        {
            try
            {
                String s1 = (String)storecontext.ds.get(425);
                StorageManager storagemanager = storageMgrFty.getStorageManagerForAET(s, true);
                DicomDir2 dicomdir2 = storagemanager.getDirInfo();
                DRNode adrnode[] = param.getDRFactory().createInstanceDRNodePath(getDRType(), storecontext.ds, createFileSetID(storagemanager.getRootDir(), storecontext.ds, param.getFilenameExt()));
                dicomdir2.addWriteLock();
                try
                {
                    int i = dicomdir2.addNodePath(adrnode);
                    if(i == 0)
                    {
                        if(Debug.DEBUG > 0)
                            Debug.out.println("Received data object already in fileset. - Rejected");
                        boolean flag = false;
                        return flag;
                    }
                    createIcon(adrnode[3].getDataset(), storecontext);
                    storagemanager.setDirty(true);
                }
                finally
                {
                    dicomdir2.releaseWriteLock();
                }
                storagemanager.write(storecontext.ds, adrnode[3].getDataset(), s1);
            }
            catch(Exception exception)
            {
                Debug.out.println(((Object) (exception)));
                throw new StorageException(42753, ((String) (null)));
            }
            return true;
        }

        protected void createIcon(DicomObject dicomobject, StoreContext storecontext)
        {
        }

        protected int store(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
            throws IOException, DicomException, IllegalValueException, UnknownUIDException
        {
            ((DicomObject) (dicommessage1)).set(9, ((Object) (new Integer(0))));
            DicomObject dicomobject = dicommessage.getDataset();
            try
            {
                checkUIDs(s, s1, dicomobject);
                if(param.isStorageValidate())
                    validate(s, dicomobject, dicommessage1);
                dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject, ((Association) (dimseexchange.getAssociation())).getTransferSyntax(dicommessage.getPresentationContext()).getValue()))));
                String s2 = dimseexchange.localAET();
                WebsiteBuilder websitebuilder = param.getWebsiteBuilder();
                StoreContext storecontext = new StoreContext(dicomobject);
                if(param.isFileset())
                {
                    adjustTS(storecontext);
                    adjustDRKeyAttribs(dicomobject, dicommessage1);
                    if(!storeToFileset(s2, storecontext))
                        websitebuilder = null;
                }
                if(websitebuilder != null)
                    try
                    {
                        setPixelMatrixAndLUT(storecontext);
                        websitebuilder.store(s2, dicomobject, storecontext.pm, storecontext.lut, getDRType());
                    }
                    catch(Exception exception)
                    {
                        Debug.out.println("Failed to create icon for received image: ");
                        ((Throwable) (exception)).printStackTrace(Debug.out);
                    }
                scnSCU.add(dimseexchange.getAssociation(), dicomobject);
            }
            catch(StorageException storageexception)
            {
                storageexception.copyTo(dicommessage1);
            }
            return ((DicomObject) (dicommessage1)).getI(9);
        }

        private MyCStoreSCP()
        {
        }

    }

    private static final class StoreContext
    {

        DicomObject ds;
        PixelMatrix pm;
        com.tiani.dicom.util.LUT.Byte1 lut;

        StoreContext(DicomObject dicomobject)
        {
            ds = dicomobject;
        }
    }

    private static class StorageException extends Exception
    {

        final int status;

        void copyTo(DicomMessage dicommessage)
        {
            try
            {
                ((DicomObject) (dicommessage)).set(9, ((Object) (new Integer(status))));
                if(((Throwable)this).getMessage() != null)
                    ((DicomObject) (dicommessage)).set(11, ((Object) (((Throwable)this).getMessage())));
            }
            catch(DicomException dicomexception)
            {
                throw new RuntimeException(((Throwable) (dicomexception)).toString());
            }
        }

        StorageException(int i, String s)
        {
            super(s);
            status = i;
        }
    }


    private final StudyContentNotificationSCU scnSCU;
    private StorageManagerFactory storageMgrFty;
    private Param param;
    static final int ERR_RESSOURCE_ACCESS_FILESET = 42753;
    static final int ERR_MISSING_CLASS_UID = 43265;
    static final int ERR_MISSING_INST_UID = 43266;
    static final int ERR_MISSING_SERIES_UID = 43267;
    static final int ERR_MISSING_STUDY_UID = 43268;
    static final int ERR_UNMATCH_CLASS_UID = 43281;
    static final int ERR_UNMATCH_INST_UID = 43282;
    static final int ERR_UNMATCH_TYPE1_ATTR = 43283;
    static final int WARN_COERCION_OF_DATA = 45056;
    static final int WARN_UNMATCH_ATTR = 45063;
    private final IDimseRqListener imageSCP = new ImageStoreSCP();
    private final IDimseRqListener gspsSCP = new GspsStoreSCP();
    private final IDimseRqListener rtDoseSCP = new RtDoseStoreSCP();
    private final IDimseRqListener rtPlanSCP = new RtPlanStoreSCP();
    private final IDimseRqListener rtStructuredSetSCP = new RtStructuredSetStoreSCP();
    private final IDimseRqListener rtTreatRecordSCP = new RtTreatRecordStoreSCP();
    private final IDimseRqListener overlaySCP = new OverlayStoreSCP();
    private final IDimseRqListener curveSCP = new CurveStoreSCP();
    private final IDimseRqListener voiLutSCP = new VoiLutStoreSCP();
    private final IDimseRqListener modalityLutSCP = new ModalityLutStoreSCP();
    private final IDimseRqListener srSCP = new SrStoreSCP();
    private final IDimseRqListener koSCP = new KoStoreSCP();
    private final IDimseRqListener storedPrintSCP = new StoredPrintSCP();
    private final IDimseRqListener rawDataSCP = new RawDataSCP();

    public StorageSCP(Param param1, StorageManagerFactory storagemanagerfactory, StudyContentNotificationSCU studycontentnotificationscu)
    {
        storageMgrFty = storagemanagerfactory;
        scnSCU = studycontentnotificationscu;
        setParam(param1);
    }

    public void setParam(Param param1)
    {
        param = param1;
    }

    public IDimseRqListener getImageSCP()
    {
        return imageSCP;
    }

    public IDimseRqListener getGspsSCP()
    {
        return gspsSCP;
    }

    public IDimseRqListener getRtDoseSCP()
    {
        return rtDoseSCP;
    }

    public IDimseRqListener getRtPlanSCP()
    {
        return rtPlanSCP;
    }

    public IDimseRqListener getRtStructuredSetSCP()
    {
        return rtStructuredSetSCP;
    }

    public IDimseRqListener getRtTreatRecordSCP()
    {
        return rtTreatRecordSCP;
    }

    public IDimseRqListener getOverlaySCP()
    {
        return overlaySCP;
    }

    public IDimseRqListener getCurveSCP()
    {
        return curveSCP;
    }

    public IDimseRqListener getVoiLutSCP()
    {
        return voiLutSCP;
    }

    public IDimseRqListener getModalityLutSCP()
    {
        return modalityLutSCP;
    }

    public IDimseRqListener getSrSCP()
    {
        return srSCP;
    }

    public IDimseRqListener getKoSCP()
    {
        return koSCP;
    }

    public IDimseRqListener getStoredPrintSCP()
    {
        return storedPrintSCP;
    }

    public IDimseRqListener getRawDataSCP()
    {
        return rawDataSCP;
    }

    private void checkExist(int i, DicomObject dicomobject, int j)
        throws StorageException, DicomException
    {
        if(dicomobject.getSize(i) <= 0)
            throw new StorageException(j, "Missing " + DDict.getDescription(i) + " in data set");
        else
            return;
    }

    private void checkUIDs(String s, String s1, DicomObject dicomobject)
        throws StorageException, DicomException
    {
        checkExist(62, dicomobject, 43265);
        checkExist(63, dicomobject, 43266);
        checkExist(425, dicomobject, 43268);
        checkExist(426, dicomobject, 43267);
        if(!s.equals(((Object) ((String)dicomobject.get(62)))))
            throw new StorageException(43281, "SOP Class UID in data set differs from value in command");
        if(!s1.equals(((Object) ((String)dicomobject.get(63)))))
            throw new StorageException(43282, "SOP Instance UID in data set differs from value in command");
        else
            return;
    }

    private void validate(String s, DicomObject dicomobject, DicomMessage dicommessage)
        throws StorageException, DicomException
    {
        CompositeIOD compositeiod = CompositeIOD.getIOD(s, dicomobject);
        if(compositeiod == null)
        {
            if(Debug.DEBUG > 0)
                Debug.out.println("Validation of SOPclass " + s + " not supported by this version of ImageServer");
            return;
        }
        Vector vector = new Vector();
        if(!compositeiod.validate(dicomobject, vector, ((com.tiani.dicom.iod.ICallbackUser) (null))))
        {
            if(Debug.DEBUG > 0)
            {
                Debug.out.println("ERROR detected for SOPclass " + s + " :");
                for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); Debug.out.println(enumeration.nextElement()));
            }
            IODErrorSummary ioderrorsummary = new IODErrorSummary(vector);
            for(Enumeration enumeration1 = ioderrorsummary.offendingElements(); enumeration1.hasMoreElements(); ((DicomObject) (dicommessage)).append(10, enumeration1.nextElement()));
            boolean flag = ioderrorsummary.type(1) > 0;
            int i = vector.size();
            StorageException storageexception = new StorageException(flag ? 43283 : 45063, i != 1 ? "" + i + "elements does not match SOP class" : ((IODError)vector.elementAt(0)).toString());
            if(flag)
                throw storageexception;
            storageexception.copyTo(dicommessage);
        }
    }

    private static int getPixelSize(DicomObject dicomobject)
    {
        int i = 0;
        for(int j = dicomobject.getSize(1184); j-- > 0;)
            i += ((byte[])dicomobject.get(1184, j)).length;

        return i <= 0 ? 1 : i;
    }

    private static byte[][] getPixelData(DicomObject dicomobject)
    {
        byte abyte0[][] = new byte[dicomobject.getSize(1184)][];
        for(int i = 0; i < abyte0.length; i++)
            abyte0[i] = (byte[])dicomobject.get(1184, i);

        return abyte0;
    }

    private static void setPixelData(DicomObject dicomobject, byte abyte0[][])
        throws DicomException
    {
        dicomobject.deleteItem(1184);
        for(int i = 0; i < abyte0.length; i++)
            dicomobject.append(1184, ((Object) (abyte0[i])));

    }

    private void adjustDRKeyAttribs(DicomObject dicomobject, DicomMessage dicommessage)
    {
        assureDRKeyAttrib(148, dicomobject, dicommessage, "<null>");
        assureDRKeyAttrib(427, dicomobject, dicommessage, "<null>");
        assureDRKeyAttrib(428, dicomobject, dicommessage, ((Object) (new Integer(0))));
        assureDRKeyAttrib(430, dicomobject, dicommessage, ((Object) (new Integer(0))));
    }

    private void assureDRKeyAttrib(int i, DicomObject dicomobject, DicomMessage dicommessage, Object obj)
    {
        if(dicomobject.getSize(i) > 0)
            return;
        String s = "set " + DDict.getDescription(i) + " to " + obj;
        try
        {
            dicomobject.set(i, obj);
            if(!param.isWarningAsSuccess())
            {
                ((DicomObject) (dicommessage)).set(9, ((Object) (new Integer(45056))));
                ((DicomObject) (dicommessage)).set(11, ((Object) (s)));
            }
        }
        catch(DicomException dicomexception)
        {
            Debug.out.println(s + " throws " + dicomexception);
        }
    }

    private String[] createFileSetID(File file, DicomObject dicomobject, String s)
        throws DicomException
    {
        String as[] = new String[param.isFilesetSplitSeries() ? 4 : 3];
        int i = 0;
        File file1 = new File(file, as[i++] = toFileID(dicomobject.getS(147)));
        file1 = new File(file1, as[i++] = toFileID(dicomobject.getS(64)));
        if(param.isFilesetSplitSeries())
            file1 = new File(file1, as[i++] = toFileID(dicomobject.getS(81) + dicomobject.getS(428)));
        File file2 = new File(file1, as[i] = leadingZeros(toFileID(dicomobject.getS(430))) + s);
        for(int j = 1; file2.exists(); j++)
            file2 = new File(file1, as[i] = leadingZeros(String.valueOf(j)) + s);

        return as;
    }

    private String leadingZeros(String s)
    {
        return "00000000".substring(s.length()) + s;
    }

    private String toFileID(String s)
    {
        StringBuffer stringbuffer = new StringBuffer();
        if(s != null)
        {
            for(int i = 0; i < s.length() && stringbuffer.length() < 8; i++)
            {
                char c;
                if(Character.isLetterOrDigit(c = s.charAt(i)))
                    stringbuffer.append(Character.toUpperCase(c));
            }

        }
        return stringbuffer.length() <= 0 ? "NULL" : stringbuffer.toString();
    }

    public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void releaseRequestReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseRequestSent(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseSent(VerboseAssociation verboseassociation)
    {
    }

    public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
    {
    }

    public void abortSent(VerboseAssociation verboseassociation, int i, int j)
    {
    }

    public void socketClosed(VerboseAssociation verboseassociation)
    {
        try
        {
            StorageManager storagemanager = storageMgrFty.getStorageManagerForAET(verboseassociation.localAET(), false);
            if(storagemanager != null)
            {
                storagemanager.compact();
                storagemanager.writeDicomDir();
            }
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        scnSCU.execute(verboseassociation);
    }










}
