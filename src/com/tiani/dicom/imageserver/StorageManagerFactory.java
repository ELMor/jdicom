// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageManagerFactory.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.media.FileSet2;
import com.tiani.dicom.util.UIDUtils;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.imageserver:
//            StorageManager, Param

public class StorageManagerFactory
{

    private Param param;
    private final Hashtable table = new Hashtable();

    public StorageManagerFactory(Param param1)
    {
        setParam(param1);
    }

    public void setParam(Param param1)
    {
        param = param1;
        for(Enumeration enumeration = table.elements(); enumeration.hasMoreElements(); ((StorageManager)enumeration.nextElement()).setParam(param1));
    }

    public File getDicomDirFileForAET(String s)
    {
        return new File(param.isFilesetSeparate() ? new File(param.getFilesetPath(), s) : new File(param.getFilesetPath()), "DICOMDIR");
    }

    public StorageManager createFileset(File file)
        throws IOException, DicomException, IllegalValueException
    {
        String s = param.getFilesetID();
        String s1 = UIDUtils.createUID();
        FileSet2 fileset2 = new FileSet2(file, s);
        DicomDir2 dicomdir2 = param.isFilesetIconJpeg() ? new DicomDir2(s, s1, "1.2.40.0.13.0.1.3.10", 8196) : new DicomDir2(s, s1);
        StorageManager storagemanager = new StorageManager(param, dicomdir2, fileset2);
        storagemanager.setDirty(true);
        storagemanager.writeDicomDir();
        table.put(((Object) (file)), ((Object) (storagemanager)));
        return storagemanager;
    }

    public StorageManager getStorageManager(File file, boolean flag)
        throws IOException, DicomException, IllegalValueException
    {
        if(!file.exists())
            return flag ? createFileset(file) : null;
        StorageManager storagemanager;
        if((storagemanager = (StorageManager)table.get(((Object) (file)))) != null)
        {
            return storagemanager;
        } else
        {
            FileSet2 fileset2 = new FileSet2(file);
            DicomDir2 dicomdir2 = new DicomDir2();
            fileset2.read(dicomdir2);
            StorageManager storagemanager1 = new StorageManager(param, dicomdir2, fileset2);
            table.put(((Object) (file)), ((Object) (storagemanager1)));
            return storagemanager1;
        }
    }

    public StorageManager getStorageManagerForAET(String s, boolean flag)
        throws IOException, DicomException, IllegalValueException
    {
        return getStorageManager(getDicomDirFileForAET(s), flag);
    }
}
