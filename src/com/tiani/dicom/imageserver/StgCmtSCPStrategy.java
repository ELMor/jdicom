// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StgCmtSCPStrategy.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.media.DREntity;
import com.tiani.dicom.media.DRNode;
import com.tiani.dicom.media.DicomDir2;
import com.tiani.dicom.service.StorageCmtSCP;
import com.tiani.dicom.util.AETable;
import java.io.PrintStream;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

// Referenced classes of package com.tiani.dicom.imageserver:
//            StorageManagerFactory, StorageManager

class StgCmtSCPStrategy
    implements com.tiani.dicom.service.StorageCmtSCP.IStrategy
{
    private static final class ToCommitItem
    {

        final String uid;
        final DicomObject refSop;

        ToCommitItem(String s, DicomObject dicomobject)
        {
            uid = s;
            refSop = dicomobject;
        }
    }


    private AETable aets;
    private StorageManagerFactory storageMgrFty;
    private static final String DIRINFO = "DIRINFO";
    private static final Integer NO_SUCH_OBJECT_INSTANCE = new Integer(274);
    private static final Integer CLASS_INSTANCE_CONFLICT = new Integer(281);

    public StgCmtSCPStrategy(StorageManagerFactory storagemanagerfactory, AETable aetable)
    {
        aets = aetable;
        storageMgrFty = storagemanagerfactory;
    }

    public int prepare(DimseExchange dimseexchange, DicomObject dicomobject, DicomMessage dicommessage, Hashtable hashtable)
    {
        try
        {
            StorageManager storagemanager = storageMgrFty.getStorageManagerForAET(dimseexchange.localAET(), false);
            if(storagemanager != null)
            {
                hashtable.put("DIRINFO", ((Object) (storagemanager.getDirInfo())));
                return 0;
            }
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        try
        {
            ((DicomObject) (dicommessage)).set(11, "access file set directory failed");
        }
        catch(DicomException dicomexception) { }
        return 272;
    }

    public int commit(DimseExchange dimseexchange, DicomObject dicomobject, DicomMessage dicommessage, Hashtable hashtable, DicomObject dicomobject1)
    {
        try
        {
            ArrayList arraylist = refSopSeqToArrayList(dicomobject);
            DicomDir2 dicomdir2 = (DicomDir2)hashtable.get("DIRINFO");
            dicomdir2.addReadLock();
            try
            {
                DREntity drentity = dicomdir2.getRootEntity();
                for(Iterator iterator1 = drentity.iterator("PATIENT"); iterator1.hasNext();)
                {
                    DRNode drnode = (DRNode)iterator1.next();
                    DREntity drentity1 = drnode.getLowerEntity();
                    if(drentity1 != null)
                    {
                        for(Iterator iterator2 = drentity1.iterator("STUDY"); iterator2.hasNext();)
                        {
                            DRNode drnode1 = (DRNode)iterator2.next();
                            DREntity drentity2 = drnode1.getLowerEntity();
                            if(drentity2 != null)
                            {
                                for(Iterator iterator3 = drentity2.iterator("SERIES"); iterator3.hasNext();)
                                {
                                    DRNode drnode2 = (DRNode)iterator3.next();
                                    DREntity drentity3 = drnode2.getLowerEntity();
                                    if(drentity3 != null)
                                    {
                                        for(Iterator iterator4 = ((AbstractList) (arraylist)).iterator(); iterator4.hasNext();)
                                        {
                                            ToCommitItem tocommititem = (ToCommitItem)iterator4.next();
                                            DRNode drnode3 = drentity3.find(((String) (null)), tocommititem.uid);
                                            if(drnode3 != null)
                                            {
                                                iterator4.remove();
                                                DicomObject dicomobject4 = drnode3.getDataset();
                                                DicomObject dicomobject2 = tocommititem.refSop;
                                                if(dicomobject2.get(115).equals(dicomobject4.get(52)))
                                                {
                                                    dicomobject1.append(121, ((Object) (dicomobject2)));
                                                } else
                                                {
                                                    dicomobject2.set(119, ((Object) (CLASS_INSTANCE_CONFLICT)));
                                                    dicomobject1.append(120, ((Object) (dicomobject2)));
                                                }
                                            }
                                        }

                                    }
                                }

                            }
                        }

                    }
                }

            }
            finally
            {
                dicomdir2.releaseReadLock();
            }
            DicomObject dicomobject3;
            for(Iterator iterator = ((AbstractList) (arraylist)).iterator(); iterator.hasNext(); dicomobject1.append(120, ((Object) (dicomobject3))))
            {
                ToCommitItem tocommititem1 = (ToCommitItem)iterator.next();
                dicomobject3 = tocommititem1.refSop;
                dicomobject3.set(119, ((Object) (NO_SUCH_OBJECT_INSTANCE)));
            }

            if(dicomobject1.getSize(121) > 0)
            {
                dicomobject1.set(79, ((Object) (dimseexchange.localAET())));
                dicomobject1.set(697, ((Object) (dicomdir2.getFileSetID())));
                dicomobject1.set(698, ((Object) (dicomdir2.getFileSetUID())));
            }
            dicomobject1.set(118, dicomobject.get(118));
            return 0;
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
        if(dicommessage != null)
            try
            {
                ((DicomObject) (dicommessage)).set(11, "query file set directory failed");
            }
            catch(DicomException dicomexception) { }
        return 272;
    }

    private ArrayList refSopSeqToArrayList(DicomObject dicomobject)
        throws DicomException
    {
        ArrayList arraylist = new ArrayList();
        int i = dicomobject.getSize(121);
        for(int j = 0; j < i; j++)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(121, j);
            String s = (String)dicomobject1.get(116);
            DicomObject dicomobject2 = new DicomObject();
            dicomobject2.set(116, ((Object) (s)));
            dicomobject2.set(115, dicomobject1.get(115));
            arraylist.add(((Object) (new ToCommitItem(s, dicomobject2))));
        }

        return arraylist;
    }

}
