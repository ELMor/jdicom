// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FwdConnectionPool.java

package com.tiani.dicom.imageserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.VerboseAssociation;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Hashtable;

class FwdConnectionPool
    implements IAssociationListener
{

    private final Hashtable table = new Hashtable();

    public FwdConnectionPool()
    {
    }

    public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void releaseRequestReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseRequestSent(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseSent(VerboseAssociation verboseassociation)
    {
    }

    public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
    {
    }

    public void abortSent(VerboseAssociation verboseassociation, int i, int j)
    {
    }

    public void socketClosed(VerboseAssociation verboseassociation)
    {
        removeAndRelease(verboseassociation);
    }

    public DimseExchange get(VerboseAssociation verboseassociation)
    {
        return (DimseExchange)table.get(((Object) (verboseassociation)));
    }

    public void put(VerboseAssociation verboseassociation, DimseExchange dimseexchange)
        throws IOException
    {
        if(table.contains(((Object) (verboseassociation))))
            throw new RuntimeException();
        if(!verboseassociation.isOpen())
        {
            throw new IOException();
        } else
        {
            verboseassociation.addAssociationListener(((IAssociationListener) (this)));
            table.put(((Object) (verboseassociation)), ((Object) (dimseexchange)));
            return;
        }
    }

    public DimseExchange remove(VerboseAssociation verboseassociation)
    {
        DimseExchange dimseexchange = (DimseExchange)table.get(((Object) (verboseassociation)));
        if(dimseexchange != null)
            verboseassociation.removeAssociationListener(((IAssociationListener) (this)));
        return dimseexchange;
    }

    public void removeAndRelease(VerboseAssociation verboseassociation)
    {
        DimseExchange dimseexchange = remove(verboseassociation);
        if(dimseexchange != null && dimseexchange.isOpen())
            try
            {
                dimseexchange.releaseAssoc();
            }
            catch(Exception exception)
            {
                Debug.out.println("jdicom: " + exception);
            }
    }
}
