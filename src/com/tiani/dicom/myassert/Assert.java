// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Assert.java

package com.tiani.dicom.myassert;


// Referenced classes of package com.tiani.dicom.myassert:
//            AssertionFailedError

public class Assert
{

    public Assert()
    {
    }

    public static void isTrue(String s, boolean flag)
    {
        if(!flag)
            fail(s);
    }

    public static void isTrue(boolean flag)
    {
        isTrue(((String) (null)), flag);
    }

    public static void equals(double d, double d1, double d2)
    {
        equals(((String) (null)), d, d1, d2);
    }

    public static void equals(long l, long l1)
    {
        equals(((String) (null)), l, l1);
    }

    public static void equals(Object obj, Object obj1)
    {
        equals(((String) (null)), obj, obj1);
    }

    public static void equals(String s, double d, double d1, double d2)
    {
        if(Math.abs(d - d1) > d2)
            _failNotEquals(s, ((Object) (new Double(d))), ((Object) (new Double(d1))));
    }

    public static void equals(String s, long l, long l1)
    {
        equals(s, ((Object) (new Long(l))), ((Object) (new Long(l1))));
    }

    public static void equals(String s, Object obj, Object obj1)
    {
        if(obj == null && obj1 == null)
            return;
        if(obj != null && obj.equals(obj1))
        {
            return;
        } else
        {
            _failNotEquals(s, obj, obj1);
            return;
        }
    }

    public static void notNull(Object obj)
    {
        notNull(((String) (null)), obj);
    }

    public static void notNull(String s, Object obj)
    {
        isTrue(s, obj != null);
    }

    public static void isNull(Object obj)
    {
        isNull(((String) (null)), obj);
    }

    public static void isNull(String s, Object obj)
    {
        isTrue(s, obj == null);
    }

    public static void same(Object obj, Object obj1)
    {
        same(((String) (null)), obj, obj1);
    }

    public static void same(String s, Object obj, Object obj1)
    {
        if(obj == obj1)
        {
            return;
        } else
        {
            _failNotSame(s, obj, obj1);
            return;
        }
    }

    public static void fail()
    {
        fail(((String) (null)));
    }

    public static void fail(String s)
    {
        throw new AssertionFailedError(s);
    }

    private static void _failNotEquals(String s, Object obj, Object obj1)
    {
        String s1 = "";
        if(s != null)
            s1 = s + " ";
        fail(s1 + "expected:<" + obj + "> but was:<" + obj1 + ">");
    }

    private static void _failNotSame(String s, Object obj, Object obj1)
    {
        String s1 = "";
        if(s != null)
            s1 = s + " ";
        fail(s1 + "expected same");
    }
}
