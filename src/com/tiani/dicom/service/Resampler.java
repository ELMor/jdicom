// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Resampler.java

package com.tiani.dicom.service;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.PixelMatrix;
import com.tiani.dicom.util.UIDUtils;

public class Resampler
{

    public Resampler()
    {
    }

    public static boolean resample(DicomObject dicomobject, int i, int j)
        throws DicomException
    {
        if(i <= 0 || j <= 0 || dicomobject.getSize(1184) != 1)
            throw new IllegalArgumentException();
        PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
        if(pixelmatrix.getRows() <= i && pixelmatrix.getColumns() <= j)
        {
            return false;
        } else
        {
            com.tiani.dicom.util.PixelMatrix.Dimension dimension = pixelmatrix.adjustRescaleDimension(i, j);
            setDerived(dicomobject, pixelmatrix.rescale(dimension.rows, dimension.columns, ((byte []) (null))));
            return true;
        }
    }

    private static void setDerived(DicomObject dicomobject, PixelMatrix pixelmatrix)
        throws DicomException
    {
        dicomobject.set(58, "DERIVED", 0);
        dicomobject.set(122, ((Object) ("Reduced pixel resolution. Original: rows=" + dicomobject.get(466) + ", columns=" + dicomobject.get(467))));
        DicomObject dicomobject1 = new DicomObject();
        dicomobject1.set(115, dicomobject.get(62));
        dicomobject1.set(116, dicomobject.get(63));
        dicomobject.set(123, ((Object) (dicomobject1)));
        dicomobject.set(63, ((Object) (UIDUtils.createUID())));
        dicomobject.set(504, "01");
        dicomobject.set(1313, ((Object) (new Float((float)(dicomobject.getI(466) * dicomobject.getI(467)) / (float)(pixelmatrix.getRows() * pixelmatrix.getColumns())))));
        dicomobject.set(466, ((Object) (new Integer(pixelmatrix.getRows()))));
        dicomobject.set(467, ((Object) (new Integer(pixelmatrix.getColumns()))));
        setPixelAspectRatio(dicomobject, pixelmatrix.getAspectRatio());
        setPixelSpacing(dicomobject, pixelmatrix.getPixelSpacing());
        dicomobject.set(1184, ((Object) (pixelmatrix.getPixelData())));
    }

    private static void setPixelAspectRatio(DicomObject dicomobject, int ai[])
        throws DicomException
    {
        dicomobject.set(473, ((Object) (new Integer(ai[0]))), 0);
        dicomobject.set(473, ((Object) (new Integer(ai[1]))), 1);
    }

    private static void setPixelSpacing(DicomObject dicomobject, float af[])
        throws DicomException
    {
        char c;
        if(af != null && (dicomobject.getSize(((int) (c = '\u01D6'))) == 2 || dicomobject.getSize(((int) (c = '\u0134'))) == 2))
        {
            dicomobject.set(((int) (c)), ((Object) (new Float(af[0]))), 0);
            dicomobject.set(((int) (c)), ((Object) (new Float(af[1]))), 1);
        }
    }
}
