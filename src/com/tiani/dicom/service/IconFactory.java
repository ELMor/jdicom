// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IconFactory.java

package com.tiani.dicom.service;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.sun.image.codec.jpeg.ImageFormatException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;
import com.tiani.dicom.util.UIDUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class IconFactory
{

    public static final int COLOR_TO_GRAY = 0;
    public static final int COLOR_AS_ORIGINAL = 1;
    public static final int COLOR_TO_PALETTE = 2;
    private int maxRows;
    private int maxColumns;
    private int convertColor;
    private boolean jpeg;
    private float jpegQuality;
    private boolean jpegMultiframe;
    private static final float MM_PER_INCH = 25.4F;

    public IconFactory()
    {
        maxRows = -1;
        maxColumns = -1;
        convertColor = 1;
        jpeg = false;
        jpegQuality = 0.5F;
        jpegMultiframe = false;
    }

    public IconFactory(int i, int j)
    {
        maxRows = -1;
        maxColumns = -1;
        convertColor = 1;
        jpeg = false;
        jpegQuality = 0.5F;
        jpegMultiframe = false;
        maxRows = i;
        maxColumns = j;
    }

    public void setMaxRows(int i)
    {
        maxRows = i;
    }

    public int getMaxRows()
    {
        return maxRows;
    }

    public void setMaxColumns(int i)
    {
        maxColumns = i;
    }

    public int getMaxColumns()
    {
        return maxColumns;
    }

    public final int getColorConvert()
    {
        return convertColor;
    }

    public final boolean isJpeg()
    {
        return jpeg;
    }

    public final float getJpegQuality()
    {
        return jpegQuality;
    }

    public final boolean isJpegMultiframe()
    {
        return jpegMultiframe;
    }

    public void setColorConvert(int i)
    {
        convertColor = i;
    }

    public void setJpeg(boolean flag)
    {
        jpeg = flag;
    }

    public void setJpegQuality(float f)
    {
        jpegQuality = f;
    }

    public void setJpegMultiframe(boolean flag)
    {
        jpegMultiframe = flag;
    }

    public PixelMatrix createIconMatrix(PixelMatrix pixelmatrix, com.tiani.dicom.util.LUT.Byte1 byte1)
        throws DicomException, IOException
    {
        if(maxRows > 0 && maxColumns > 0)
        {
            com.tiani.dicom.util.PixelMatrix.Dimension dimension = pixelmatrix.adjustRescaleDimension(Math.min(maxRows, pixelmatrix.getRows()), Math.min(maxColumns, pixelmatrix.getColumns()));
            if(dimension.columns != pixelmatrix.getColumns() || dimension.rows != pixelmatrix.getRows())
                pixelmatrix = !jpeg || !jpegMultiframe ? pixelmatrix.rescaleFrame(pixelmatrix.getRepresentativeFrameNumber() - 1, dimension.rows, dimension.columns, ((byte []) (null))) : pixelmatrix.rescale(dimension.rows, dimension.columns, ((byte []) (null)));
        }
        if(pixelmatrix.isMonochrome())
            return pixelmatrix.preformatGrayscale(byte1, ((byte []) (null)));
        switch(convertColor)
        {
        case 1: // '\001'
            return pixelmatrix;

        case 0: // '\0'
            return pixelmatrix.convertToGrayscale(((byte []) (null)));

        case 2: // '\002'
            return pixelmatrix.convertToPaletteColor(((byte []) (null)));
        }
        throw new RuntimeException();
    }

    public DicomObject createIconDicomObject(DicomObject dicomobject)
        throws DicomException, IOException
    {
        PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
        com.tiani.dicom.util.LUT.Byte1 byte1 = pixelmatrix.isMonochrome() ? LUTFactory.createByteLUT(pixelmatrix, dicomobject) : null;
        return createIconDicomObject(pixelmatrix, byte1);
    }

    public DicomObject createIconDicomObject(PixelMatrix pixelmatrix, com.tiani.dicom.util.LUT.Byte1 byte1)
        throws DicomException, IOException
    {
        DicomObject dicomobject = new DicomObject();
        setIconDicomObject(dicomobject, pixelmatrix, byte1);
        return dicomobject;
    }

    public void createJpegDicomObject(DicomObject dicomobject)
        throws DicomException, IOException
    {
        PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
        com.tiani.dicom.util.LUT.Byte1 byte1 = pixelmatrix.isMonochrome() ? LUTFactory.createByteLUT(pixelmatrix, dicomobject) : null;
        createJpegDicomObject(dicomobject, pixelmatrix, byte1);
    }

    public void createJpegDicomObject(DicomObject dicomobject, PixelMatrix pixelmatrix, com.tiani.dicom.util.LUT.Byte1 byte1)
        throws DicomException, IOException
    {
        if(!jpeg)
        {
            throw new IllegalStateException("setJpeg(true)");
        } else
        {
            float f = ((byte[])dicomobject.get(1184)).length;
            int i = pixelmatrix.getRows();
            int j = pixelmatrix.getColumns();
            setIconDicomObject(dicomobject, pixelmatrix, byte1);
            dicomobject.set(58, "DERIVED", 0);
            dicomobject.set(122, ((Object) (i <= dicomobject.getI(466) && j <= dicomobject.getI(467) ? "8-bit rendering, JPEG lossy compression" : "Reduced pixel resolution, 8-bit rendering, JPEG lossy compression")));
            DicomObject dicomobject1 = new DicomObject();
            dicomobject1.set(115, dicomobject.get(62));
            dicomobject1.set(116, dicomobject.get(63));
            dicomobject.set(123, ((Object) (dicomobject1)));
            dicomobject.set(62, "1.2.840.10008.5.1.4.1.1.7");
            String s = UIDUtils.createUID();
            dicomobject.set(63, ((Object) (s)));
            dicomobject.set(83, "WKS");
            dicomobject.set(504, "01");
            dicomobject.set(1313, ((Object) (new Float(f / (float)calcPixelDataSize(dicomobject)))));
            dicomobject.deleteItem(490);
            dicomobject.deleteItem(489);
            dicomobject.deleteItem(491);
            dicomobject.deleteItem(505);
            dicomobject.deleteItem(487);
            dicomobject.deleteItem(488);
            dicomobject.deleteItem(492);
            dicomobject.deleteItem(510);
            dicomobject.deleteItem(470);
            dicomobject.deleteItem(308);
            dicomobject.deleteItem(473);
            dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation("1.2.840.10008.5.1.4.1.1.7", s, "1.2.840.10008.1.2.4.50"))));
            return;
        }
    }

    private static int calcPixelDataSize(DicomObject dicomobject)
    {
        int i = 0;
        int j = dicomobject.getSize(1184);
        for(int k = 0; k < j; k++)
            i += ((byte[])dicomobject.get(1184, k)).length;

        return i;
    }

    void setIconDicomObject(DicomObject dicomobject, PixelMatrix pixelmatrix, com.tiani.dicom.util.LUT.Byte1 byte1)
        throws DicomException, IOException
    {
        pixelmatrix = createIconMatrix(pixelmatrix, byte1);
        dicomobject.set(461, ((Object) (new Integer(1))));
        dicomobject.set(462, ((Object) (pixelmatrix.getPhotometricInterpretation())));
        dicomobject.set(466, ((Object) (new Integer(pixelmatrix.getRows()))));
        dicomobject.set(467, ((Object) (new Integer(pixelmatrix.getColumns()))));
        dicomobject.set(475, ((Object) (new Integer(8))));
        dicomobject.set(476, ((Object) (new Integer(8))));
        dicomobject.set(477, ((Object) (new Integer(7))));
        dicomobject.set(478, ((Object) (new Integer(0))));
        if(!jpeg)
        {
            dicomobject.set(1184, ((Object) (pixelmatrix.getPixelData())));
            return;
        }
        int i = pixelmatrix.getNumberOfFrames();
        if(i > 1)
            dicomobject.set(464, ((Object) (new Integer(i))));
        dicomobject.set(1184, ((Object) (new byte[0])), 0);
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        for(int j = 0; j < i; j++)
        {
            writeJpeg(((OutputStream) (bytearrayoutputstream)), pixelmatrix, j, jpegQuality);
            if((bytearrayoutputstream.size() & 1) != 0)
                bytearrayoutputstream.write(0);
            dicomobject.set(1184, ((Object) (bytearrayoutputstream.toByteArray())), j + 1);
            bytearrayoutputstream.reset();
        }

    }

    public static void storeJpeg(File file, PixelMatrix pixelmatrix, int i, float f)
        throws IOException, ImageFormatException
    {
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: store jpeg to " + file);
        FileOutputStream fileoutputstream = new FileOutputStream(file);
        try
        {
            writeJpeg(((OutputStream) (fileoutputstream)), pixelmatrix, i, f);
        }
        finally
        {
            ((OutputStream) (fileoutputstream)).close();
        }
    }

    public static void writeJpeg(OutputStream outputstream, PixelMatrix pixelmatrix, int i, float f)
        throws IOException, ImageFormatException
    {
        java.awt.image.BufferedImage bufferedimage = pixelmatrix.createBufferedImage(i);
        JPEGImageEncoder jpegimageencoder = JPEGCodec.createJPEGEncoder(outputstream);
        if(pixelmatrix.isPaletteColor())
        {
            jpegimageencoder.encode(bufferedimage);
            return;
        }
        JPEGEncodeParam jpegencodeparam = JPEGCodec.getDefaultJPEGEncodeParam(bufferedimage);
        jpegencodeparam.setQuality(f, false);
        float af[] = pixelmatrix.getPixelSpacing();
        if(af != null)
        {
            jpegencodeparam.setDensityUnit(1);
            jpegencodeparam.setXDensity((int)(25.4F / af[1]));
            jpegencodeparam.setYDensity((int)(25.4F / af[0]));
        } else
        {
            jpegencodeparam.setDensityUnit(0);
            jpegencodeparam.setXDensity(pixelmatrix.getAspectRatio()[0]);
            jpegencodeparam.setYDensity(pixelmatrix.getAspectRatio()[1]);
        }
        jpegimageencoder.encode(bufferedimage, jpegencodeparam);
    }
}
