// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DimseRouter.java

package com.tiani.dicom.service;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.VerboseAssociation;
import java.io.IOException;
import java.io.PrintStream;

public class DimseRouter
    implements IDimseRqListener
{
    private class BackwardRsp
        implements IDimseRspListener
    {

        private final DimseExchange rqSrc;
        private final byte pcid;

        public void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
            throws IOException, DicomException, IllegalValueException, UnknownUIDException
        {
            rqSrc.getAssociation().sendInPresentationContext(pcid, ((com.archimed.dicom.DicomObject) (dicommessage)), dicommessage.getDataset());
            factory.receivedRSP(rqSrc, dimseexchange, i, j, dicommessage);
        }

        BackwardRsp(DimseExchange dimseexchange, byte byte0)
        {
            rqSrc = dimseexchange;
            pcid = byte0;
        }
    }

    public static interface IConnectionFactory
    {

        public abstract DimseExchange getConnectionForDimseRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
            throws IOException, IllegalValueException, UnknownUIDException;

        public abstract DimseExchange getConnectionForCancelRQ(DimseExchange dimseexchange, int i)
            throws IOException, IllegalValueException, UnknownUIDException;

        public abstract void receivedRSP(DimseExchange dimseexchange, DimseExchange dimseexchange1, int i, int j, DicomMessage dicommessage);
    }


    private final IConnectionFactory factory;
    private final IDimseRqListener localHandler;
    private int curMessageID;

    public DimseRouter(IConnectionFactory iconnectionfactory, IDimseRqListener idimserqlistener)
    {
        curMessageID = 0;
        factory = iconnectionfactory;
        localHandler = idimserqlistener;
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        curMessageID = i;
        DimseExchange dimseexchange1 = factory.getConnectionForDimseRQ(dimseexchange, i, s, dicommessage);
        if(dimseexchange1 == null)
        {
            localHandler.handleRQ(dimseexchange, i, s, dicommessage);
            return;
        }
        try
        {
            dimseexchange1.sendRQ(i, dicommessage.getAbstractSyntax().getConstant(), dicommessage, ((IDimseRspListener) (new BackwardRsp(dimseexchange, dicommessage.getPresentationContext()))));
        }
        catch(InterruptedException interruptedexception)
        {
            Debug.out.println("jdicom: " + interruptedexception);
        }
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        if(curMessageID != i)
            return false;
        try
        {
            DimseExchange dimseexchange1 = factory.getConnectionForCancelRQ(dimseexchange, i);
            if(dimseexchange1 == null)
                return localHandler.handleCancelRQ(dimseexchange, i);
            dimseexchange1.sendCancelRQ(i, dimseexchange.getCurrentAbstractSyntax().getConstant());
        }
        catch(Exception exception)
        {
            Debug.out.println("jdicom: " + exception);
        }
        return true;
    }

}
