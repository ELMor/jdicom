// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MoveStorageSCU.java

package com.tiani.dicom.service;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.IMultiResponse;
import com.tiani.dicom.framework.Status;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.service:
//            StorageCmtSCU

public class MoveStorageSCU
    implements IMultiResponse
{
    public static interface IStrategy
    {

        public abstract int load(DicomObject dicomobject, String s)
            throws UnknownUIDException, DicomException, IOException;
    }


    private final IStrategy strategy;
    private final String moveOriginatorAET;
    private final int moveOriginatorMsgID;
    private final String instanceUIDs[];
    private final int asids[];
    private final StorageCmtSCU scu;
    private final Vector failedSOPInstanceUIDList;
    private final Vector moveRspListener;
    private int index;
    private boolean canceled;

    public MoveStorageSCU(IStrategy istrategy, String s, int i, String as[])
    {
        this(istrategy, s, i, as, ((int []) (null)));
    }

    public MoveStorageSCU(IStrategy istrategy, String s, int i, String as[], int ai[])
    {
        scu = new StorageCmtSCU();
        failedSOPInstanceUIDList = new Vector();
        moveRspListener = new Vector();
        index = 0;
        canceled = false;
        if(ai != null && ai.length != as.length)
        {
            throw new IllegalArgumentException("Number of instanceUIDs is not equal number of asids");
        } else
        {
            strategy = istrategy;
            moveOriginatorAET = s;
            moveOriginatorMsgID = i;
            instanceUIDs = as;
            asids = ai;
            return;
        }
    }

    public void addMoveRspListener(IDimseRspListener idimsersplistener)
    {
        if(moveRspListener.indexOf(((Object) (idimsersplistener))) == -1)
            moveRspListener.addElement(((Object) (idimsersplistener)));
    }

    public void removeMoveRspListener(IDimseRspListener idimsersplistener)
    {
        moveRspListener.removeElement(((Object) (idimsersplistener)));
    }

    public void setARTIM(int i)
    {
        scu.setARTIM(i);
    }

    public void setMaxNumOpInvoked(int i)
    {
        scu.setMaxNumOpInvoked(i);
    }

    public void setMaxPduSize(int i)
    {
        scu.setMaxPduSize(i);
    }

    public void setPriority(int i)
    {
        scu.setPriority(i);
    }

    public Response connect(String s, int i, String s1, String s2, int ai[])
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        return scu.connect(s, i, s1, s2, ai);
    }

    public Response connect(String s, int i, Request request)
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        return scu.connect(s, i, request);
    }

    public boolean isConnected()
    {
        return scu.isConnected();
    }

    public boolean isEnabled(int i)
    {
        return scu.isEnabled(i);
    }

    public UIDEntry[] listAcceptedTransferSyntaxes(int i)
        throws IllegalValueException
    {
        return scu.listAcceptedTransferSyntaxes(i);
    }

    public int nextResponse(DimseExchange dimseexchange, DicomMessage dicommessage)
    {
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Enter MoveStorageSCU.nextResponse");
        int i = nextRsp(dimseexchange, dicommessage);
        try
        {
            ((DicomObject) (dicommessage)).set(9, ((Object) (new Integer(i))));
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
        int j = ((Integer)((DicomObject) (dicommessage)).get(5)).intValue();
        int k = moveRspListener.size();
        for(int l = 0; l < k; l++)
            try
            {
                ((IDimseRspListener)moveRspListener.elementAt(l)).handleRSP(dimseexchange, j, i, dicommessage);
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(Debug.out);
            }

        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Leave MoveStorageSCU.nextResponse with " + Status.toString(i));
        return i;
    }

    private int nextRsp(DimseExchange dimseexchange, DicomMessage dicommessage)
    {
        try
        {
            boolean flag = scu.isConnected();
            if(Debug.DEBUG > 2)
                Debug.out.println("jdicom: Enter MoveStorageSCU.nextRsp, isConnected=" + flag);
            if(flag)
                if(canceled || index >= instanceUIDs.length)
                {
                    if(Debug.DEBUG > 2)
                        Debug.out.println("jdicom: Enter StorageCmtSCU.release");
                    scu.release();
                    if(Debug.DEBUG > 2)
                        Debug.out.println("jdicom: Leave StorageCmtSCU.release");
                } else
                {
                    sendNext();
                    index++;
                }
            int i = scu.getNumFailed() + failedSOPInstanceUIDList.size();
            int j = scu.getNumCompleted();
            int k = scu.getNumWarning();
            flag = scu.isConnected();
            if(!flag)
            {
                if(Debug.DEBUG > 2)
                    Debug.out.println("jdicom: Create result C-MOVE-RSP");
                completeFailedSOPInstanceUIDList();
                if((i = failedSOPInstanceUIDList.size()) > 0)
                    dicommessage.setDataset(createRspIdentifer());
                if(Debug.DEBUG > 2)
                    Debug.out.println("jdicom: Result C-MOVE-RSP created");
            }
            int l = instanceUIDs.length - i - j - k;
            if(l > 0)
                ((DicomObject) (dicommessage)).set(19, ((Object) (new Integer(l))));
            ((DicomObject) (dicommessage)).set(20, ((Object) (new Integer(j))));
            ((DicomObject) (dicommessage)).set(22, ((Object) (new Integer(k))));
            ((DicomObject) (dicommessage)).set(21, ((Object) (new Integer(i))));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Leave MoveStorageSCU.nextRsp, isConnected=" + flag);
            return flag ? 65280 : canceled ? 65024 : index >= instanceUIDs.length ? i <= 0 ? 0 : 45056 : 42754;
        }
        catch(Exception exception)
        {
            ((Throwable) (exception)).printStackTrace(Debug.out);
        }
        return 49152;
    }

    public void cancel()
    {
        canceled = true;
    }

    private void sendNext()
    {
        DicomObject dicomobject = new DicomObject();
        String s = instanceUIDs[index];
        if(asids == null || isEnabled(asids[index]))
            try
            {
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Enter MoveStorageSCU.IStrategy.load with Instance UID " + s);
                strategy.load(dicomobject, s);
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Leave MoveStorageSCU.IStrategy.load");
                DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
                if(dicomobject1 != null)
                {
                    String s2 = dicomobject1.getS(31);
                    switch(UID.getUIDEntry(s2).getConstant())
                    {
                    default:
                        Compression compression = new Compression(dicomobject);
                        compression.decompress();
                        if(Debug.DEBUG > 1)
                            Debug.out.println("Decompressed image data before send");
                        break;

                    case 8193: 
                    case 8194: 
                    case 8195: 
                        break;
                    }
                }
                scu.storeAsync(dicomobject, 8193, moveOriginatorAET, moveOriginatorMsgID);
                return;
            }
            catch(Exception exception)
            {
                Debug.out.println("jdicom: " + exception);
            }
        else
        if(Debug.DEBUG > 0)
        {
            String s1;
            try
            {
                s1 = UID.getUIDEntry(asids[index]).getName();
            }
            catch(IllegalValueException illegalvalueexception)
            {
                s1 = ((Throwable) (illegalvalueexception)).getMessage();
            }
            Debug.out.println("jdicom: Skip storage of instance " + s + " (" + s1 + " was rejected by SCP)");
        }
        failedSOPInstanceUIDList.addElement(((Object) (s)));
    }

    private void completeFailedSOPInstanceUIDList()
    {
        DicomObject dicomobject = scu.getStoreResult();
        int i = dicomobject.getSize(120);
        for(int j = 0; j < i; j++)
        {
            DicomObject dicomobject1 = (DicomObject)dicomobject.get(120, j);
            failedSOPInstanceUIDList.addElement(((Object) ((String)dicomobject1.get(116))));
        }

        if(canceled)
            return;
        for(int k = index; k < instanceUIDs.length; k++)
            failedSOPInstanceUIDList.addElement(((Object) (instanceUIDs[k])));

    }

    private DicomObject createRspIdentifer()
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        int i = failedSOPInstanceUIDList.size();
        for(int j = 0; j < i; j++)
            dicomobject.append(80, failedSOPInstanceUIDList.elementAt(j));

        return dicomobject;
    }
}
