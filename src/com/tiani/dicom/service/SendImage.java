// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SendImage.java

package com.tiani.dicom.service;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Reject;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.util.CheckParam;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

public class SendImage
{
    private static final class SendImageException extends Exception
    {

        private final int _errcode;

        public int errcode()
        {
            return _errcode;
        }

        public SendImageException(int i, String s)
        {
            super(s);
            _errcode = i;
        }
    }


    public static final int OK_STATUS = 0;
    public static final int ERR_URL_MALFORMED = -1;
    public static final int ERR_SOCK_IO = -10;
    public static final int ERR_SOCK_UNKNOWNHOST = -11;
    public static final int ERR_ASSOC_IO = -20;
    public static final int ERR_ASSOC_ISOPEN = -21;
    public static final int ERR_ASSOC_ISCLOSED = -22;
    public static final int ERR_ASSOC_ILLEGALVALUE = -23;
    public static final int ERR_ASSOC_UNKNOWNUID = -24;
    public static final int ERR_IMAGE_IO = -30;
    public static final int ERR_IMAGE_OUTOFMEMORY = -31;
    public static final int ERR_IMAGE_DICOM = -32;
    public static final int ERR_IMAGE_UNKNOWNUID = -33;
    public static final int ERR_SEND_IO = -40;
    public static final int ERR_SEND_ILLEGALVALUE = -41;
    public static final int ERR_SEND_DICOM = -42;
    public static final int ERR_RECEIVE_IO = -50;
    public static final int ERR_RECEIVE_ILLEGALVALUE = -51;
    public static final int ERR_RECEIVE_DICOM = -52;
    public static final int ERR_RECEIVE_UNKNOWNUID = -53;
    public static final int ERR_RECEIVE_WRONGMSGID = -54;
    public static final int ERR_FILE_IO = -60;
    public static final int DEL_NONE = 0;
    public static final int DEL_STUDY = 1;
    public static final int DEL_IMAGE = 2;
    public static final int ON_ERROR_CONTINUE = 0;
    public static final int ON_ERROR_SKIP_STUDY = 1;
    public static final int ON_ERROR_EXIT = 2;
    public static final int ON_WARNING_CONTINUE = 0;
    public static final int ON_WARNING_SKIP_STUDY = 1;
    public static final int ON_WARNING_EXIT = 2;
    private static SendImage _instance = new SendImage();
    private static String OPTIONS[] = {
        "0", "1", "2"
    };
    private static Hashtable PARAM_CHECKS;
    private String _url;
    private int _delOpt;
    private int _onErrorOpt;
    private int _onWarningOpt;
    private File _rootDir;
    private File _dirList[];
    private File _fileList[][];
    private int _msgID;
    private String _callingTitle;
    private String _calledTitle;
    private String _host;
    private int _port;
    private String _classUID;
    private int _sopClass;
    private String _instanceUID;
    private int _status;
    private Socket _sock;
    private Association _assoc;
    private Request _request;
    private Response _response;
    private DicomObject _dcmObj;

    public SendImage()
    {
        _url = null;
        _delOpt = 0;
        _onErrorOpt = 0;
        _onWarningOpt = 0;
        _rootDir = null;
        _dirList = null;
        _fileList = null;
        _msgID = 0;
        _callingTitle = null;
        _calledTitle = null;
        _host = null;
        _port = 0;
        _classUID = null;
        _sopClass = 0;
        _instanceUID = null;
        _status = 0;
        _sock = null;
        _assoc = null;
        _request = null;
        _response = null;
        _dcmObj = null;
    }

    public static void main(String args[])
    {
        String s = "SendImage.properties";
        switch(args.length)
        {
        case 1: // '\001'
            s = args[0];
            // fall through

        case 0: // '\0'
            instance().send(s);
            return;

        default:
            System.out.println("Wrong number of Commandline Parameters");
            return;
        }
    }

    public void send(String s)
    {
        if(!_init(s))
            return;
        if(open(_url) != 0)
            return;
label0:
        for(int i = 0; i < _dirList.length; i++)
        {
            int j = _fileList[i].length;
label1:
            for(int k = 0; k < _fileList[i].length; k++)
            {
                File file = _fileList[i][k];
label2:
                switch(send(file))
                {
                case 45056: 
                case 45062: 
                case 45063: 
                    switch(_onWarningOpt)
                    {
                    case 1: // '\001'
                        break label1;

                    case 2: // '\002'
                        break label0;
                    }
                    // fall through

                case 0: // '\0'
                    j--;
                    System.out.println("Sent Image: " + file + " ; Status: " + _status);
                    if(_delOpt == 2)
                        _delFile(file);
                    break;

                default:
                    if(!_isOpen())
                        return;
                    switch(_onErrorOpt)
                    {
                    default:
                        break label2;

                    case 1: // '\001'
                        break label1;

                    case 2: // '\002'
                        break;
                    }
                    break label0;
                }
            }

            if(j == 0)
                switch(_delOpt)
                {
                default:
                    break;

                case 1: // '\001'
                case 2: // '\002'
                    if(_delDir(_dirList[i]))
                        _delLog(_dirList[i].getName() + '.');
                    break;
                }
        }

        close();
    }

    public static SendImage instance()
    {
        return _instance;
    }

    public String openS(String s)
    {
        return String.valueOf(open(s));
    }

    public int open(String s)
    {
        try
        {
            if(_isOpen())
                throw new SendImageException(-21, "Association already open");
            if(!_parseURL(s))
                throw new SendImageException(-1, "MalformedURLException: " + s);
            _createRequest();
            _openSocket();
            _openAssoc();
        }
        catch(SendImageException sendimageexception)
        {
            System.out.println(((Throwable) (sendimageexception)).getMessage());
            return sendimageexception.errcode();
        }
        return 0;
    }

    public int send(DicomObject dicomobject)
    {
        try
        {
            _dcmObj = dicomobject;
            _checkDicomObject();
            _sendReceive();
            return _status;
        }
        catch(SendImageException sendimageexception)
        {
            System.out.println(((Throwable) (sendimageexception)).getMessage());
            return sendimageexception.errcode();
        }
    }

    public String sendS(byte abyte0[], int i)
    {
        return String.valueOf(send(abyte0, i));
    }

    public int send(byte abyte0[], int i)
    {
        try
        {
            _createDicomObject(abyte0, i);
            _sendReceive();
            return _status;
        }
        catch(SendImageException sendimageexception)
        {
            System.out.println(((Throwable) (sendimageexception)).getMessage());
            return sendimageexception.errcode();
        }
    }

    public int send(File file)
    {
        try
        {
            _createDicomObject(file);
            _sendReceive();
            return _status;
        }
        catch(SendImageException sendimageexception)
        {
            System.out.println(((Throwable) (sendimageexception)).getMessage());
            return sendimageexception.errcode();
        }
    }

    public String closeS()
    {
        return String.valueOf(close());
    }

    public int close()
    {
        try
        {
            if(!_isOpen())
                throw new SendImageException(-22, "Association is closed");
            _close();
        }
        catch(SendImageException sendimageexception)
        {
            System.out.println(((Throwable) (sendimageexception)).getMessage());
            return sendimageexception.errcode();
        }
        return 0;
    }

    private boolean _init(String s)
    {
        Properties properties = new Properties();
        try
        {
            FileInputStream fileinputstream = new FileInputStream(s);
            try
            {
                properties.load(((InputStream) (fileinputstream)));
            }
            finally
            {
                fileinputstream.close();
            }
            CheckParam.verify(properties, PARAM_CHECKS);
            _url = properties.getProperty("URL");
            _delOpt = Integer.parseInt(properties.getProperty("DeleteOption"));
            _onErrorOpt = Integer.parseInt(properties.getProperty("OnErrorOption"));
            _onWarningOpt = Integer.parseInt(properties.getProperty("OnWarningOption"));
            return _initDirList(properties.getProperty("DirectoryPath"));
        }
        catch(Exception exception)
        {
            System.out.println(((Object) (exception)));
        }
        return false;
    }

    private boolean _initDirList(String s)
        throws IOException
    {
        _rootDir = new File(s);
        if(!_rootDir.isDirectory())
        {
            System.out.println(s + " is not a directory!");
            return false;
        }
        String as[] = _rootDir.list(new FilenameFilter() {

            public boolean accept(File file1, String s1)
            {
                File file2 = new File(file1, s1);
                return file2.isDirectory();
            }

        });
        _dirList = new File[as.length];
        _fileList = new File[as.length][];
        for(int i = 0; i < _dirList.length; i++)
        {
            File file = _dirList[i] = new File(_rootDir, as[i]);
            String as1[] = file.list();
            if(as1 == null)
                throw new IOException("Couldnot list " + file);
            _fileList[i] = new File[as1.length];
            for(int j = 0; j < as1.length; j++)
                _fileList[i][j] = new File(file, as1[j]);

        }

        return true;
    }

    private boolean _delFile(File file)
    {
        try
        {
            file.delete();
            System.out.println("Deleted " + file);
            return true;
        }
        catch(SecurityException securityexception)
        {
            System.out.println("Failed to delete " + file);
        }
        return false;
    }

    private boolean _delDir(File file)
    {
        try
        {
            String as[] = file.list();
            for(int i = 0; i < as.length; i++)
                if(!_delFile(new File(file, as[i])))
                    return false;

            file.delete();
            System.out.println("Deleted " + file);
            return true;
        }
        catch(SecurityException securityexception)
        {
            System.out.println("Failed to delete " + file);
        }
        return false;
    }

    private void _delLog(final String namePrefix)
    {
        String as[] = _rootDir.list(new FilenameFilter() {

            public boolean accept(File file, String s)
            {
                return s.startsWith(namePrefix);
            }

        });
        for(int i = 0; i < as.length; i++)
            _delFile(new File(_rootDir, as[i]));

    }

    private boolean _parseURL(String s)
    {
        int i = s.indexOf("@dicom://");
        if(i == -1)
            return false;
        String s1 = s.substring(0, i);
        String s2 = s.substring(i + 9);
        StringTokenizer stringtokenizer = new StringTokenizer(s1, "/");
        if(stringtokenizer.countTokens() != 2)
            return false;
        _callingTitle = stringtokenizer.nextToken();
        StringTokenizer stringtokenizer1 = new StringTokenizer(s2, "/:");
        if(stringtokenizer1.countTokens() != 3)
            return false;
        _host = stringtokenizer1.nextToken();
        try
        {
            _port = Integer.parseInt(stringtokenizer1.nextToken());
        }
        catch(NumberFormatException numberformatexception)
        {
            return false;
        }
        _calledTitle = stringtokenizer1.nextToken();
        return true;
    }

    private void _createRequest()
        throws SendImageException
    {
        try
        {
            _request = new Request();
            _request.setCalledTitle(_calledTitle);
            _request.setCallingTitle(_callingTitle);
            int ai[] = {
                8193
            };
            _request.addPresentationContext(4118, ai);
            _request.addPresentationContext(4119, ai);
            _request.addPresentationContext(4121, ai);
            _request.addPresentationContext(4120, ai);
            _request.addPresentationContext(4159, ai);
            _request.addPresentationContext(4122, ai);
            _request.addPresentationContext(4123, ai);
            _request.addPresentationContext(4128, ai);
            _request.addPresentationContext(4129, ai);
            _request.addPresentationContext(4131, ai);
            _request.addPresentationContext(4151, ai);
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new SendImageException(-23, "IllegalValueException: " + ((Throwable) (illegalvalueexception)).getMessage());
        }
    }

    private void _openSocket()
        throws SendImageException
    {
        try
        {
            _sock = new Socket(_host, _port);
        }
        catch(UnknownHostException unknownhostexception)
        {
            _sock = null;
            throw new SendImageException(-11, "UnknownHostException: " + ((Throwable) (unknownhostexception)).getMessage());
        }
        catch(IOException ioexception)
        {
            _sock = null;
            throw new SendImageException(-10, "IOException: " + ((Throwable) (ioexception)).getMessage());
        }
    }

    private void _openAssoc()
        throws SendImageException
    {
        try
        {
            _msgID = 0;
            _assoc = new Association(_sock.getInputStream(), _sock.getOutputStream());
            _assoc.sendAssociateRequest(_request);
            _response = _assoc.receiveAssociateResponse();
        }
        catch(IllegalValueException illegalvalueexception)
        {
            _assoc = null;
            throw new SendImageException(-23, "IllegalValueException: " + ((Throwable) (illegalvalueexception)).getMessage());
        }
        catch(UnknownUIDException unknownuidexception)
        {
            _assoc = null;
            throw new SendImageException(-24, "UnknownUIDException: " + ((Throwable) (unknownuidexception)).getMessage());
        }
        catch(IOException ioexception)
        {
            _assoc = null;
            throw new SendImageException(-20, "IOException: " + ((Throwable) (ioexception)).getMessage());
        }
        if(_response instanceof Abort)
        {
            _assoc = null;
            _closeSocket();
            throw new SendImageException(((Abort)_response).getReason(), "Association aborted: " + _response);
        }
        if(_response instanceof Reject)
        {
            _assoc = null;
            _closeSocket();
            throw new SendImageException(((Reject)_response).getReason(), "Association rejected: " + _response);
        } else
        {
            return;
        }
    }

    private void _close()
        throws SendImageException
    {
        try
        {
            _assoc.sendReleaseRequest();
            _assoc.receiveReleaseResponse();
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new SendImageException(-23, "IllegalValueException: " + ((Throwable) (illegalvalueexception)).getMessage());
        }
        catch(IOException ioexception)
        {
            throw new SendImageException(-20, "IOException: " + ((Throwable) (ioexception)).getMessage());
        }
        finally
        {
            _assoc = null;
            _closeSocket();
        }
    }

    private void _abort()
    {
        try
        {
            _assoc.sendAbort(Abort.DICOM_UL_SERVICE_USER, Abort.REASON_NOT_SPECIFIED);
        }
        catch(IOException ioexception)
        {
            System.out.println("Send Abort throws IOException: " + ((Throwable) (ioexception)).getMessage());
        }
        finally
        {
            _assoc = null;
            _closeSocket();
        }
    }

    private void _closeSocket()
    {
        try
        {
            _sock.close();
        }
        catch(IOException ioexception)
        {
            System.out.println("Close Socket throws IOException: " + ((Throwable) (ioexception)).getMessage());
        }
        _sock = null;
    }

    private void _createDicomObject(byte abyte0[], int i)
        throws SendImageException
    {
        byte abyte1[] = new byte[i];
        System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), 0, i);
        ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(abyte1);
        _createDicomObject(((InputStream) (bytearrayinputstream)));
    }

    private void _createDicomObject(File file)
        throws SendImageException
    {
        try
        {
            FileInputStream fileinputstream = new FileInputStream(file);
            try
            {
                _createDicomObject(((InputStream) (fileinputstream)));
            }
            finally
            {
                try
                {
                    fileinputstream.close();
                }
                catch(IOException ioexception1) { }
            }
        }
        catch(IOException ioexception)
        {
            throw new SendImageException(-60, "Open File throws IOException: " + ((Throwable) (ioexception)).getMessage());
        }
    }

    private void _createDicomObject(InputStream inputstream)
        throws SendImageException
    {
        try
        {
            _dcmObj = new DicomObject();
            _dcmObj.read(inputstream);
            _checkDicomObject();
        }
        catch(OutOfMemoryError outofmemoryerror)
        {
            throw new SendImageException(-31, "Parsing Image throws OutOfMemoryError : " + ((Throwable) (outofmemoryerror)).getMessage());
        }
        catch(IOException ioexception)
        {
            throw new SendImageException(-30, "Parsing Image throws IOException: " + ((Throwable) (ioexception)).getMessage());
        }
        catch(DicomException dicomexception)
        {
            throw new SendImageException(-32, "Parsing Image throws DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
    }

    private void _checkDicomObject()
        throws SendImageException
    {
        try
        {
            _classUID = _dcmObj.getS(62);
            _instanceUID = _dcmObj.getS(63);
            _sopClass = UID.getUIDEntry(_classUID).getConstant();
        }
        catch(DicomException dicomexception)
        {
            throw new SendImageException(-32, "Parsing Image throws DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
        catch(UnknownUIDException unknownuidexception)
        {
            throw new SendImageException(-33, "Parsing Image throws UnknownUIDException: " + ((Throwable) (unknownuidexception)).getMessage());
        }
    }

    private void _sendReceive()
        throws SendImageException
    {
        if(!_isOpen())
        {
            throw new SendImageException(-22, "Association is closed");
        } else
        {
            _sendImage();
            _receiveStatus();
            return;
        }
    }

    private void _sendImage()
        throws SendImageException
    {
        try
        {
            DicomMessage dicommessage = new DicomMessage(1, _nextMsgID(), _dcmObj);
            dicommessage.affectedSOP(_classUID, _instanceUID);
            dicommessage.priority(0);
            _assoc.send(_sopClass, ((DicomObject) (dicommessage)), _dcmObj);
        }
        catch(IllegalValueException illegalvalueexception)
        {
            _abort();
            throw new SendImageException(-41, "IllegalValueException: " + ((Throwable) (illegalvalueexception)).getMessage());
        }
        catch(DicomException dicomexception)
        {
            _abort();
            throw new SendImageException(-42, "DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
        catch(IOException ioexception)
        {
            _abort();
            throw new SendImageException(-40, "IOException: " + ((Throwable) (ioexception)).getMessage());
        }
    }

    private void _receiveStatus()
        throws SendImageException
    {
        try
        {
            DicomObject dicomobject = _assoc.receiveCommand();
            int i = dicomobject.getI(5);
            if(i != _msgID)
            {
                _abort();
                throw new SendImageException(-54, "Wrong Message ID in Response: " + i);
            }
            _status = dicomobject.getI(9);
            if(_status != 0)
                System.out.println(_promptStatus());
        }
        catch(UnknownUIDException unknownuidexception)
        {
            _abort();
            throw new SendImageException(-53, "UnknownUIDException: " + ((Throwable) (unknownuidexception)).getMessage());
        }
        catch(IllegalValueException illegalvalueexception)
        {
            _abort();
            throw new SendImageException(-51, "IllegalValueException: " + ((Throwable) (illegalvalueexception)).getMessage());
        }
        catch(DicomException dicomexception)
        {
            _abort();
            throw new SendImageException(-52, "DicomException: " + ((Throwable) (dicomexception)).getMessage());
        }
        catch(IOException ioexception)
        {
            _abort();
            throw new SendImageException(-50, "IOException: " + ((Throwable) (ioexception)).getMessage());
        }
    }

    private boolean _isOpen()
    {
        return _assoc != null;
    }

    private String _promptStatus()
    {
        switch(_status)
        {
        case 0: // '\0'
            return "Success";

        case 65024: 
            return "Cancel";

        case 263: 
            return "Warning: Attribute list error";

        case 278: 
            return "Warning: Attribute Value Out of Range";

        case 290: 
            return "Refused: SOP class not supported";

        case 281: 
            return "Error: Class-instance conflict";

        case 273: 
            return "Error: Duplicate SOP instance";

        case 528: 
            return "Error: Duplicate invocation";

        case 277: 
            return "Error: Invalid argument value";

        case 262: 
            return "Error: Invalid attribute value";

        case 279: 
            return "Error: Invalid object instance";

        case 288: 
            return "Error: Missing attribute";

        case 289: 
            return "Error: Missing attribute value";

        case 530: 
            return "Error: Mistyped argument";

        case 276: 
            return "Error: No such argument";

        case 261: 
            return "Error: No such attribute";

        case 275: 
            return "Error: No such event type";

        case 274: 
            return "Error: No such object instance";

        case 280: 
            return "Error: No Such SOP class";

        case 272: 
            return "Error: Processing failure";

        case 531: 
            return "Error: Resource limitation";

        case 529: 
            return "Error: Unrecognized operation";

        case 42752: 
            return "Error: Out of Resources";

        case 42753: 
            return "Refused: Out of Resources - Unable to calculate number of matches";

        case 42754: 
            return "Refused: Out of Resources - Unable to perform sub-operations";

        case 43009: 
            return "Refused: Move Destination unknown";

        case 43264: 
            return "Failed: Data Set/Identifier does not match SOP Class";

        case 45056: 
            return "Warning: Coercion of Data Elements";

        case 45063: 
            return "Warning: Data Set does not match SOP Class";

        case 45062: 
            return "Warning: Elements Discarded";

        case 49152: 
            return "Error: Cannot understand/Unable to process/Failed operation";

        case 1: // '\001'
            return "Success: Partial Study Content exists on system supporting SCP";

        case 2: // '\002'
            return "Success: None of the Study Content exists on system supporting SCP";

        case 3: // '\003'
            return "Success: It is unknown whether or not study content exists on system supporting SCP";

        case 65280: 
            return "Pending";

        case 65281: 
            return "Pending - one or more Optional Keys were not supported";
        }
        switch(_status & 0xff00)
        {
        case 42752: 
            return "Refused: Out of Resources";

        case 43264: 
            return "Error: Data Set does not match SOP";
        }
        switch(_status & 0xf000)
        {
        case 49152: 
            return "Error: Cannot understand";
        }
        return "Unrecognized Error: " + _status;
    }

    private int _nextMsgID()
    {
        _msgID++;
        _msgID &= 0xffff;
        return _msgID;
    }

    static 
    {
        PARAM_CHECKS = new Hashtable();
        PARAM_CHECKS.put("DeleteOption", ((Object) (CheckParam.enum(OPTIONS))));
        PARAM_CHECKS.put("OnErrorOption", ((Object) (CheckParam.enum(OPTIONS))));
        PARAM_CHECKS.put("OnWarningOption", ((Object) (CheckParam.enum(OPTIONS))));
    }
}
