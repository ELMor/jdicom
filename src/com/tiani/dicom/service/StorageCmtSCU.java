// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StorageCmtSCU.java

package com.tiani.dicom.service;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.Acceptor;
import com.tiani.dicom.framework.AdvancedAcceptancePolicy;
import com.tiani.dicom.framework.DefAcceptorListener;
import com.tiani.dicom.framework.DefCEchoSCP;
import com.tiani.dicom.framework.DefNEventReportSCU;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.framework.IDimseRspListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.StatusEntry;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.UIDUtils;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.service:
//            StorageCmtConstants

public class StorageCmtSCU
    implements StorageCmtConstants
{
    private class MyStoreRspListener
        implements IDimseRspListener
    {

        private DicomObject item;

        public void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
            throws IOException, DicomException, IllegalValueException, UnknownUIDException
        {
            int k = Status.getStatusEntry(j).getType();
            if(k == 5)
            {
                item.set(119, ((Object) (new Integer(j))));
                storeResult.append(120, ((Object) (item)));
                ++numFailed;
            } else
            {
                storeResult.append(121, ((Object) (item)));
                if(k == 1)
                    ++numCompleted;
                else
                    ++numWarning;
            }
        }

        MyStoreRspListener(String s, String s1)
            throws DicomException
        {
            item = new DicomObject();
            item.set(115, ((Object) (s)));
            item.set(116, ((Object) (s1)));
        }
    }


    public static final int CMT_SOP_CLASSES[] = {
        4097, 4100
    };
    public static final int CMT_STORE_SOP_CLASSES[] = {
        4097, 4100, 4165, 4166, 4167, 4168, 4194, 4118, 4119, 4120, 
        4121, 4122, 4123, 4128, 4129, 4130, 4131, 4148, 4149, 4151, 
        4153, 4120, 4158, 4159, 4161, 4162, 4163, 4164, 4176, 4177, 
        4178, 4179, 4180, 4181, 4124, 4125, 4126, 4127, 4152, 4154, 
        4155, 4156, 4147, 4196, 4195
    };
    public static final int CMT_NON_IMAGE_STORE_SOP_CLASSES[] = {
        4097, 4100, 4165, 4166, 4167, 4168, 4194, 4154, 4155, 4156, 
        4147
    };
    public static final int IMAGE_STORE_SOP_CLASSES[] = {
        4118, 4119, 4120, 4121, 4122, 4123, 4128, 4129, 4130, 4131, 
        4148, 4149, 4151, 4153, 4120, 4158, 4159, 4161, 4162, 4163, 
        4164, 4176, 4177, 4178, 4179, 4180, 4181
    };
    public static final int SC_IMAGE_STORAGE_SOP_CLASS[] = {
        4123
    };
    static final int DEFAULT_TS[] = {
        8193
    };
    static final int NATIVE_TS[] = {
        8194, 8193
    };
    static final int JPEG_LOSSLESS_TS[] = {
        8197, 8194, 8193
    };
    static final int JPEG_BASELINE[] = {
        8196
    };
    private final Hashtable cmtRQinProcess = new Hashtable();
    private DicomObject storeResult;
    private int numCompleted;
    private int numFailed;
    private int numWarning;
    private Requestor requestor;
    private DimseExchange connection;
    private Thread thread;
    private Thread server;
    private Acceptor acceptor;
    private final AdvancedAcceptancePolicy policy = new AdvancedAcceptancePolicy();
    private final DimseRqManager rqManager = new DimseRqManager();
    private final DefAcceptorListener acceptorListener;
    private final DefNEventReportSCU myNEventReportSCU = new DefNEventReportSCU() {

        protected int eventReport(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
            throws DicomException
        {
            DicomObject dicomobject;
            String s2;
            IDimseListener idimselistener;
            if((dicomobject = dicommessage.getDataset()) == null || (s2 = (String)dicomobject.get(118)) == null || (idimselistener = (IDimseListener)cmtRQinProcess.remove(((Object) (s2)))) == null)
                return 277;
            idimselistener.notify(dicommessage);
            synchronized(this)
            {
                ((Object)this).notifyAll();
            }
            return 0;
        }

    };
    private boolean checkAET;
    private boolean multiThreadTCP;
    private int maxPduSize;
    private int assocTimeout;
    private int releaseTimeout;
    private int maxNumOpInvoked;
    private int priority;
    private final IAssociationListener assocListener = new IAssociationListener() {

        public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
        {
        }

        public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
        {
        }

        public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
        {
        }

        public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
        {
        }

        public void releaseRequestReceived(VerboseAssociation verboseassociation)
        {
        }

        public void releaseResponseReceived(VerboseAssociation verboseassociation)
        {
        }

        public void releaseRequestSent(VerboseAssociation verboseassociation)
        {
        }

        public void releaseResponseSent(VerboseAssociation verboseassociation)
        {
        }

        public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
        {
        }

        public void abortSent(VerboseAssociation verboseassociation, int i, int j)
        {
        }

        public void socketClosed(VerboseAssociation verboseassociation)
        {
            thread = null;
            connection = null;
        }

    };

    public StorageCmtSCU()
    {
        storeResult = new DicomObject();
        numCompleted = 0;
        numFailed = 0;
        numWarning = 0;
        requestor = null;
        connection = null;
        thread = null;
        server = null;
        acceptor = null;
        acceptorListener = new DefAcceptorListener(false, rqManager, false, false);
        checkAET = false;
        multiThreadTCP = false;
        maxPduSize = 16352;
        assocTimeout = 3000;
        releaseTimeout = 1000;
        maxNumOpInvoked = 1;
        priority = 0;
        try
        {
            policy.addPresentationContext(4097, DEFAULT_TS);
            policy.addPresentationContext(4100, DEFAULT_TS);
            policy.setScuScpRoleSelection(4100, 0, 1);
            rqManager.regCEchoScp(UID.toString(4097), DefCEchoSCP.getInstance());
            rqManager.regNEventReportScu("1.2.840.10008.1.20.1", ((IDimseRqListener) (myNEventReportSCU)));
        }
        catch(IllegalValueException illegalvalueexception)
        {
            throw new RuntimeException(((Throwable) (illegalvalueexception)).getMessage());
        }
    }

    public void regCStoreScp(int i, IDimseRqListener idimserqlistener)
        throws IllegalValueException
    {
        policy.addPresentationContext(i, DEFAULT_TS);
        rqManager.regCStoreScp(UID.toString(i), idimserqlistener);
    }

    public void setPriority(int i)
    {
        priority = i;
    }

    public void setARTIM(int i)
    {
        setARTIM(i, i);
    }

    public void setARTIM(int i, int j)
    {
        assocTimeout = i;
        releaseTimeout = j;
        if(acceptor != null)
            acceptor.setARTIM(i, j);
        acceptorListener.setARTIM(j);
        if(connection != null)
            connection.setARTIM(j);
    }

    public DicomObject getStoreResult()
    {
        return storeResult;
    }

    public int getNumCompleted()
    {
        return numCompleted;
    }

    public int getNumWarning()
    {
        return numWarning;
    }

    public int getNumFailed()
    {
        return numFailed;
    }

    public void clearStoreResult()
    {
        storeResult = new DicomObject();
        numCompleted = 0;
        numWarning = 0;
        numFailed = 0;
    }

    public void setMaxNumOpInvoked(int i)
    {
        maxNumOpInvoked = i;
    }

    public void setCheckAET(boolean flag)
    {
        checkAET = flag;
        if(!flag)
        {
            policy.setCallingTitles(((String []) (null)));
            policy.setCalledTitle(((String) (null)));
        }
    }

    public int countCmtRQinProcess()
    {
        return cmtRQinProcess.size();
    }

    public void clearCmtRQinProcess()
    {
        cmtRQinProcess.clear();
    }

    public void setMultiThreadAssoc(boolean flag)
    {
        acceptorListener.setStartThread(flag);
    }

    public void setMultiThreadTCP(boolean flag)
    {
        multiThreadTCP = flag;
        if(acceptor != null)
            acceptor.setStartThread(flag);
    }

    public void setMaxPduSize(int i)
    {
        maxPduSize = i;
        policy.setMaxPduSize(i);
    }

    public void start(int i)
    {
        if(server != null)
            throw new IllegalStateException("server is running");
        try
        {
            if(Debug.DEBUG > 0)
                Debug.out.println("jdicom: Start Server listening on port " + i);
            acceptor = new Acceptor(new ServerSocket(i), multiThreadTCP, ((com.tiani.dicom.framework.IAcceptancePolicy) (policy)), ((com.tiani.dicom.framework.IAcceptorListener) (acceptorListener)));
            acceptor.setARTIM(assocTimeout, releaseTimeout);
            server = new Thread(((Runnable) (acceptor)));
            server.start();
        }
        catch(Exception exception)
        {
            Debug.out.println("jdicom: " + exception);
            server = null;
            acceptor = null;
        }
    }

    public void stop(boolean flag, boolean flag1)
    {
        try
        {
            if(server == null)
                throw new IllegalStateException("server is not running");
            if(!flag)
                synchronized(this)
                {
                    for(; cmtRQinProcess.size() > 0; ((Object)this).wait());
                }
            acceptor.stop(flag);
            if(flag1)
                server.join();
            server = null;
            acceptor = null;
            if(Debug.DEBUG > 0)
                Debug.out.println("jdicom: Server stopped");
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(Debug.out);
        }
    }

    public boolean isServerRunning()
    {
        return server != null;
    }

    public Response connect(String s, int i, String s1, String s2, int ai[])
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        return connect(s, i, s1, s2, new int[][] {
            ai
        }, new int[][] {
            DEFAULT_TS
        });
    }

    public Response connect(String s, int i, String s1, String s2, int ai[][], int ai1[][])
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        if(ai.length != ai1.length)
            throw new IllegalArgumentException();
        Request request = new Request();
        request.setCalledTitle(s1);
        request.setCallingTitle(s2);
        request.setMaxPduSize(maxPduSize);
        if(checkAET)
        {
            policy.setCalledTitle(s2);
            policy.addCallingTitle(s1);
        }
        if(maxNumOpInvoked != 1)
        {
            request.setMaxOperationsInvoked(maxNumOpInvoked);
            request.setMaxOperationsPerformed(1);
        }
        for(int j = 0; j < ai.length; j++)
            addPresContextsTo(request, ai[j], ai1[j]);

        return connect(s, i, request);
    }

    private void addPresContextsTo(Request request, int ai[], int ai1[])
        throws IllegalValueException
    {
        Arrays.sort(ai);
        int i = 0;
        for(int j = 0; j < ai.length; j++)
            if(i != ai[j])
                request.addPresentationContext(i = ai[j], ai1);

    }

    public Response connect(String s, int i, Request request)
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        if(connection != null)
            throw new IllegalStateException("existing connection");
        Socket socket = new Socket(s, i);
        requestor = new Requestor(socket, request);
        requestor.addAssociationListener(assocListener);
        VerboseAssociation verboseassociation = requestor.openAssoc();
        if(verboseassociation != null)
        {
            try
            {
                if(thread != null)
                    thread.join(5000L);
            }
            catch(InterruptedException interruptedexception) { }
            Acknowledge acknowledge = (Acknowledge)requestor.response();
            connection = new DimseExchange(verboseassociation, rqManager, false, false, acknowledge.getMaxOperationsInvoked());
            connection.setARTIM(releaseTimeout);
            (thread = new Thread(((Runnable) (connection)))).start();
        }
        return requestor.response();
    }

    public boolean isConnected()
    {
        return connection != null;
    }

    public boolean isEnabled(int i)
    {
        try
        {
            return isConnected() && connection.listAcceptedPresentationContexts(i).length > 0;
        }
        catch(IllegalValueException illegalvalueexception)
        {
            return false;
        }
    }

    public boolean isEnabled(int i, int j)
    {
        try
        {
            if(isConnected())
            {
                UIDEntry auidentry[] = connection.listAcceptedTransferSyntaxes(i);
                for(int k = 0; k < auidentry.length; k++)
                    if(auidentry[k].getConstant() == j)
                        return true;

            }
        }
        catch(IllegalValueException illegalvalueexception) { }
        return false;
    }

    public DimseExchange getConnection()
    {
        return connection;
    }

    public byte[] listAcceptedPresentationContexts(int i)
        throws IllegalValueException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.listAcceptedPresentationContexts(i);
    }

    public UIDEntry[] listAcceptedTransferSyntaxes(int i)
        throws IllegalValueException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.listAcceptedTransferSyntaxes(i);
    }

    public byte getPresentationContext(int i)
        throws IllegalValueException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.getPresentationContext(i);
    }

    public byte getPresentationContext(int i, int j)
        throws IllegalValueException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.getPresentationContext(i, j);
    }

    public UIDEntry getTransferSyntax(byte byte0)
        throws IllegalValueException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.getTransferSyntax(byte0);
    }

    public DicomMessage echo()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        else
            return connection.cecho();
    }

    public void storeAsync(DicomObject dicomobject, int i)
        throws IOException, IllegalValueException, DicomException, InterruptedException, UnknownUIDException
    {
        storeAsync(dicomobject, i, ((String) (null)), 0);
    }

    public void storeAsync(DicomObject dicomobject, int i, String s, int j)
        throws IOException, IllegalValueException, DicomException, InterruptedException, UnknownUIDException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        String s1 = dicomobject.getS(62);
        String s2 = dicomobject.getS(63);
        int k = UID.getUIDEntry(s1).getConstant();
        byte byte0 = getPresentationContext(k, i);
        MyStoreRspListener mystorersplistener = new MyStoreRspListener(s1, s2);
        if(s == null)
            connection.cstoreAsync(byte0, s1, s2, priority, dicomobject, ((IDimseRspListener) (mystorersplistener)));
        else
            connection.cstoreAsync(byte0, s1, s2, priority, s, j, dicomobject, ((IDimseRspListener) (mystorersplistener)));
    }

    public void waitForAllRSP()
        throws InterruptedException
    {
        if(connection == null)
        {
            throw new IllegalStateException("no connection");
        } else
        {
            connection.waitForAllRSP();
            return;
        }
    }

    public DicomMessage commit(DicomObject dicomobject, IDimseListener idimselistener, int i)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        byte byte0 = getPresentationContext(4100, i);
        return doCommit(dicomobject, idimselistener, byte0);
    }

    public DicomMessage commit(DicomObject dicomobject, IDimseListener idimselistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        byte byte0 = getPresentationContext(4100);
        return doCommit(dicomobject, idimselistener, byte0);
    }

    private DicomMessage doCommit(DicomObject dicomobject, IDimseListener idimselistener, byte byte0)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(server == null)
            throw new IllegalStateException("server is not running");
        String s = (String)dicomobject.get(118);
        if(s == null)
        {
            s = UIDUtils.createUID();
            dicomobject.set(118, ((Object) (s)));
        }
        dicomobject.deleteItem(120);
        cmtRQinProcess.put(((Object) (s)), ((Object) (idimselistener)));
        DicomMessage dicommessage = connection.naction(byte0, "1.2.840.10008.1.20.1", "1.2.840.10008.1.20.1.1", 1, dicomobject);
        if(Status.getStatusEntry(((DicomObject) (dicommessage)).getI(9)).getType() == 5)
            cmtRQinProcess.remove(((Object) (s)));
        return dicommessage;
    }

    public void release()
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        if(connection != null)
        {
            connection.releaseAssoc();
            connection = null;
        }
    }

    protected void finalize()
    {
        try
        {
            if(connection != null)
                release();
            if(server != null)
                stop(true, false);
        }
        catch(Exception exception) { }
    }








}
