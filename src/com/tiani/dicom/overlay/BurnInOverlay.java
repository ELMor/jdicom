// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BurnInOverlay.java

package com.tiani.dicom.overlay;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.tiani.dicom.util.EnumPMI;

// Referenced classes of package com.tiani.dicom.overlay:
//            Overlay

public class BurnInOverlay
{

    private DicomObject _dcmObj;
    private int _rows;
    private int _columns;
    private int _pixelRepresentation;
    private int _planarConfiguration;
    private int _pmi;
    private int _samplesPerPixel;
    private int _sampleGap;
    private int _bitsAllocated;
    private int _bitsStored;
    private int _highBit;
    private byte _pixeldata[];
    private int _bytesAllocated;
    private int _bandOffset;
    private int _bytesPerCell;
    private byte _whiteLoByte;
    private byte _whiteHiByte;

    public static void burnInAll(DicomObject dicomobject)
        throws DicomException
    {
        int ai[] = Overlay.listOverlayGroups(dicomobject);
        if(ai.length == 0)
            return;
        BurnInOverlay burninoverlay = new BurnInOverlay(dicomobject);
        for(int i = 0; i < ai.length; i++)
            burninoverlay.burnIn(ai[i]);

    }

    public BurnInOverlay(DicomObject dicomobject)
        throws DicomException
    {
        setDicomObject(dicomobject);
    }

    public DicomObject getDicomObject()
    {
        return _dcmObj;
    }

    public void setDicomObject(DicomObject dicomobject)
        throws DicomException
    {
        _dcmObj = dicomobject;
        _rows = dicomobject.getI(466);
        _columns = dicomobject.getI(467);
        _pixelRepresentation = dicomobject.getI(478);
        _pmi = EnumPMI.getConstant(dicomobject.getS(462));
        _samplesPerPixel = dicomobject.getI(461);
        _planarConfiguration = _samplesPerPixel <= 1 ? 0 : dicomobject.getI(463);
        _bitsAllocated = dicomobject.getI(475);
        _bitsStored = dicomobject.getI(476);
        _highBit = dicomobject.getI(477);
        _pixeldata = (byte[])dicomobject.get(1184);
        if(_samplesPerPixel != EnumPMI.SAMPLES[_pmi])
            throw new DicomException("Samples Per Pixel does not match Photometric Interpretion");
        if(_pmi != 0 && _pmi != 1 && _pmi != 3)
            throw new DicomException("Photometric Interpretion not support by current version of BurnInOverlay");
        if(_bitsAllocated != 8 && _bitsAllocated != 16)
            throw new DicomException("" + _bitsAllocated + "bits allocated not support by current version of BurnInOverlay");
        _bytesAllocated = _bitsAllocated / 8;
        _bandOffset = (_planarConfiguration != 0 ? _rows * _columns : 1) * _bytesAllocated;
        _bytesPerCell = (_planarConfiguration != 0 ? 1 : _samplesPerPixel) * _bytesAllocated;
        int i = (1 << _bitsStored) - 1;
        int j = 0;
        if(_pixelRepresentation != 0)
        {
            i >>= 1;
            j = -(i + 1);
        }
        int k = _pmi != 0 ? i : j;
        k <<= _bitsStored - _highBit - 1;
        _whiteLoByte = (byte)k;
        _whiteHiByte = (byte)(k >> 8);
    }

    public void burnIn(int i)
        throws DicomException
    {
        Overlay overlay = new Overlay(_dcmObj, i);
        int j = overlay.getOriginRow() - 1;
        int k = overlay.getOriginColumn() - 1;
        int l = overlay.getRows();
        int i1 = overlay.getColumns();
        for(int j1 = 0; j1 < l; j1++)
        {
            for(int k1 = 0; k1 < i1; k1++)
                if(overlay.isOverlayAt(j1, k1))
                {
                    int l1 = ((j + j1) * _columns + (k + k1)) * _bytesPerCell;
                    for(int i2 = 0; i2 < _samplesPerPixel;)
                    {
                        _pixeldata[l1] = _whiteLoByte;
                        if(_bytesAllocated == 2)
                            _pixeldata[l1 + 1] = _whiteHiByte;
                        i2++;
                        l1 += _bandOffset;
                    }

                }

        }

        ((GroupList) (_dcmObj)).removeGroup(i);
    }
}
