// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AttribOverlayFactory.java

package com.tiani.dicom.overlay;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.RectangularShape;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

public class AttribOverlayFactory
{

    private static final String POS_PO_CH[] = {
        "L", "P", "H"
    };
    private static final String NEG_PO_CH[] = {
        "R", "A", "F"
    };
    private final Font _font;
    private final float _minFontSize;
    private final float _maxFontSize;
    private final float _relFontSize;
    private final float _relLineGap;
    private final float _patOrient;
    private final float _relTickLen;
    private final float _relTickWidth;
    private final float _relInset;
    private final Properties _prop;
    private static final Properties _defProp;
    private static final int LEFT = 0;
    private static final int CENTER = 1;
    private static final int RIGHT = 2;
    private static final byte REVERSE_BYTE[] = {
        0, -128, 64, -64, 32, -96, 96, -32, 16, -112, 
        80, -48, 48, -80, 112, -16, 8, -120, 72, -56, 
        40, -88, 104, -24, 24, -104, 88, -40, 56, -72, 
        120, -8, 4, -124, 68, -60, 36, -92, 100, -28, 
        20, -108, 84, -44, 52, -76, 116, -12, 12, -116, 
        76, -52, 44, -84, 108, -20, 28, -100, 92, -36, 
        60, -68, 124, -4, 2, -126, 66, -62, 34, -94, 
        98, -30, 18, -110, 82, -46, 50, -78, 114, -14, 
        10, -118, 74, -54, 42, -86, 106, -22, 26, -102, 
        90, -38, 58, -70, 122, -6, 6, -122, 70, -58, 
        38, -90, 102, -26, 22, -106, 86, -42, 54, -74, 
        118, -10, 14, -114, 78, -50, 46, -82, 110, -18, 
        30, -98, 94, -34, 62, -66, 126, -2, 1, -127, 
        65, -63, 33, -95, 97, -31, 17, -111, 81, -47, 
        49, -79, 113, -15, 9, -119, 73, -55, 41, -87, 
        105, -23, 25, -103, 89, -39, 57, -71, 121, -7, 
        5, -123, 69, -59, 37, -91, 101, -27, 21, -107, 
        85, -43, 53, -75, 117, -11, 13, -115, 77, -51, 
        45, -83, 109, -19, 29, -99, 93, -35, 61, -67, 
        125, -3, 3, -125, 67, -61, 35, -93, 99, -29, 
        19, -109, 83, -45, 51, -77, 115, -13, 11, -117, 
        75, -53, 43, -85, 107, -21, 27, -101, 91, -37, 
        59, -69, 123, -5, 7, -121, 71, -57, 39, -89, 
        103, -25, 23, -105, 87, -41, 55, -73, 119, -9, 
        15, -113, 79, -49, 47, -81, 111, -17, 31, -97, 
        95, -33, 63, -65, 127, -1
    };

    public AttribOverlayFactory(Properties properties)
    {
        _prop = properties == null ? _defProp : (Properties)((Hashtable) (properties)).clone();
        _font = Font.decode(_prop.getProperty("font"));
        _relFontSize = (float)_font.getSize() / 512F;
        _minFontSize = ((Hashtable) (_prop)).containsKey("font.min") ? Float.parseFloat(_prop.getProperty("font.min")) : 8F;
        _maxFontSize = ((Hashtable) (_prop)).containsKey("font.max") ? Float.parseFloat(_prop.getProperty("font.max")) : 100F;
        _relLineGap = ((Hashtable) (_prop)).containsKey("lineGap") ? Float.parseFloat(_prop.getProperty("lineGap")) : 0.0F;
        _patOrient = ((Hashtable) (_prop)).containsKey("patOrient") ? Float.parseFloat(_prop.getProperty("patOrient")) : 0.0F;
        _relTickLen = ((Hashtable) (_prop)).containsKey("tick.len") ? Float.parseFloat(_prop.getProperty("tick.len")) / 512F : 0.016F;
        _relTickWidth = ((Hashtable) (_prop)).containsKey("tick.width") ? Float.parseFloat(_prop.getProperty("tick.width")) / 512F : 0.004F;
        _relInset = ((Hashtable) (_prop)).containsKey("inset") ? Float.parseFloat(_prop.getProperty("inset")) / 512F : 0.008F;
    }

    public void createOverlay(int i, int j, DicomObject dicomobject, int k)
        throws DicomException
    {
        if((j & 3) != 0)
        {
            throw new IllegalArgumentException("only columns = 4xN supported");
        } else
        {
            byte abyte0[] = createOverlayData(i, j, dicomobject);
            dicomobject.set_ge(k, 16, ((Object) (new Integer(i))));
            dicomobject.set_ge(k, 17, ((Object) (new Integer(j))));
            dicomobject.set_ge(k, 64, "G");
            dicomobject.set_ge(k, 69, "AUTOMATED");
            dicomobject.set_ge(k, 80, ((Object) (new Integer(1))), 0);
            dicomobject.set_ge(k, 80, ((Object) (new Integer(1))), 1);
            dicomobject.set_ge(k, 256, ((Object) (new Integer(1))));
            dicomobject.set_ge(k, 258, ((Object) (new Integer(0))));
            dicomobject.set_ge(k, 12288, ((Object) (abyte0)));
            return;
        }
    }

    private void addPatOrient(DicomObject dicomobject)
        throws DicomException
    {
        dicomobject.set(436, ((Object) (getPatOrient(dicomobject, 0, 0) + getPatOrient(dicomobject, 1, 1) + getPatOrient(dicomobject, 2, 2))));
        dicomobject.set(436, ((Object) (getPatOrient(dicomobject, 3, 0) + getPatOrient(dicomobject, 4, 1) + getPatOrient(dicomobject, 5, 2))), 1);
    }

    private String getPatOrient(DicomObject dicomobject, int i, int j)
    {
        float f = ((Float)dicomobject.get(441, i)).floatValue();
        if(f > _patOrient)
            return POS_PO_CH[j];
        if(-f > _patOrient)
            return NEG_PO_CH[j];
        else
            return "";
    }

    private byte[] createOverlayData(int i, int j, DicomObject dicomobject)
    {
        if(_patOrient > 0.0F && dicomobject.getSize(436) <= 0 && dicomobject.getSize(441) == 6)
            try
            {
                addPatOrient(dicomobject);
            }
            catch(DicomException dicomexception)
            {
                Debug.out.println("jdicom: " + dicomexception);
            }
        float f = Math.min(_maxFontSize, Math.max(_minFontSize, _relFontSize * (float)i));
        float f1 = f + _relLineGap * (float)i;
        float f2 = _relInset * (float)i;
        BufferedImage bufferedimage = new BufferedImage(j, i, 12);
        Graphics2D graphics2d = bufferedimage.createGraphics();
        Font font = _font.deriveFont(f);
        ((Graphics) (graphics2d)).setFont(font);
        ((Graphics) (graphics2d)).setPaintMode();
        float f3 = (float)i - f2;
        float f4 = (float)j - f2;
        float f5 = (f3 - (float)countLines("e.") * f1) / 2.0F;
        float f6 = (f3 - (float)countLines("w.") * f1) / 2.0F;
        float f7 = f3 - (float)countLines("se.") * f1;
        float f8 = f3 - (float)countLines("sw.") * f1;
        float f9 = f3 - (float)countLines("s.") * f1;
        draw("nw.", dicomobject, graphics2d, font, f2, f2, f1, 0);
        draw("n.", dicomobject, graphics2d, font, j / 2, f2, f1, 1);
        draw("ne.", dicomobject, graphics2d, font, f4, f2, f1, 2);
        draw("w.", dicomobject, graphics2d, font, f2, f6, f1, 0);
        draw("e.", dicomobject, graphics2d, font, f4, f5, f1, 2);
        draw("sw.", dicomobject, graphics2d, font, f2, f8, f1, 0);
        draw("s.", dicomobject, graphics2d, font, j / 2, f9, f1, 1);
        draw("se.", dicomobject, graphics2d, font, f4, f7, f1, 2);
        float f10 = getVScaleStep(dicomobject);
        if(f10 != 0.0F)
        {
            int k = Math.max((int)(_relTickWidth * (float)i), 1);
            int l = Math.max((int)(_relTickLen * (float)i), 3);
            int j1;
            if((j1 = getIntParam("scale.nee")) > 0)
                drawScale(dicomobject, graphics2d, f4, f5, f4 - (float)l, j1, f10, l, k);
            if((j1 = getIntParam("scale.see")) > 0)
                drawScale(dicomobject, graphics2d, f4, f7, f4 - (float)l, j1, f10, l, k);
            if((j1 = getIntParam("scale.sww")) > 0)
                drawScale(dicomobject, graphics2d, f2, f8, f2, j1, f10, l, k);
            if((j1 = getIntParam("scale.nww")) > 0)
                drawScale(dicomobject, graphics2d, f2, f6, f2, j1, f10, l, k);
        }
        byte abyte0[] = ((DataBufferByte)((Raster) (bufferedimage.getRaster())).getDataBuffer()).getData();
        for(int i1 = 0; i1 < abyte0.length; i1++)
            abyte0[i1] = REVERSE_BYTE[abyte0[i1] & 0xff];

        return abyte0;
    }

    private void drawScale(DicomObject dicomobject, Graphics2D graphics2d, float f, float f1, float f2, int i, float f3, 
            int j, int k)
    {
        float f4 = f1;
        for(int l = 0; l <= i;)
        {
            ((Graphics) (graphics2d)).fillRect((int)f2, (int)f4, j, k);
            l++;
            f4 -= f3;
        }

        f4 += f3;
        ((Graphics) (graphics2d)).fillRect((int)f, (int)f4, k, (int)((f1 - f4) + (float)k));
    }

    private void draw(String s, DicomObject dicomobject, Graphics2D graphics2d, Font font, float f, float f1, float f2, 
            int i)
    {
        float f3 = f1 + f2;
        int j = 1;
        String s1;
        while((s1 = _prop.getProperty(s + j)) != null) 
        {
            try
            {
                String s2 = createLine(s + j + '.', s1, dicomobject);
                if(s2.length() > 0)
                {
                    java.awt.geom.Rectangle2D rectangle2d = font.getStringBounds(s2, graphics2d.getFontRenderContext());
                    graphics2d.drawString(s2, f - ((float)((RectangularShape) (rectangle2d)).getWidth() * (float)i) / 2.0F, f3);
                }
            }
            catch(Exception exception)
            {
                Debug.out.println("jdicom: " + exception);
            }
            j++;
            f3 += f2;
        }
    }

    private static float getVScaleStep(DicomObject dicomobject)
    {
        try
        {
            Float float1 = (Float)dicomobject.get(470, 0);
            if(float1 == null)
                float1 = (Float)dicomobject.get(308, 0);
            return float1 == null ? 0.0F : 10F / float1.floatValue();
        }
        catch(Exception exception)
        {
            Debug.out.println("jdicom: " + exception);
        }
        return 0.0F;
    }

    private int getIntParam(String s)
    {
        String s1 = _prop.getProperty(s);
        return s1 == null ? -1 : Integer.parseInt(s1);
    }

    private int countLines(String s)
    {
        int i;
        for(i = 0; ((Hashtable) (_prop)).containsKey(((Object) (s + (i + 1)))); i++);
        return i;
    }

    private String createLine(String s, String s1, DicomObject dicomobject)
        throws DicomException
    {
        if(s1.indexOf('%') == -1)
            return s1;
        StringBuffer stringbuffer = new StringBuffer();
        StringTokenizer stringtokenizer = new StringTokenizer(s1, "%");
        if(s1.charAt(0) != '%')
            stringbuffer.append(stringtokenizer.nextToken());
        String s2;
        for(; stringtokenizer.hasMoreTokens(); stringbuffer.append(s2.substring(1)))
        {
            s2 = stringtokenizer.nextToken();
            String s3 = s + s2.charAt(0);
            String s4 = _prop.getProperty(s3);
            if(s4 == null || s4.length() == 0)
                throw new IllegalArgumentException("Missing property - " + s3);
            stringbuffer.append(getStrFrom(dicomobject, s4));
        }

        return stringbuffer.toString();
    }

    private static String getStrFrom(DicomObject dicomobject, String s)
        throws DicomException
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, "-");
        int i = Integer.parseInt(stringtokenizer.nextToken(), 16);
        int j = i >> 16;
        int k = i & 0xffff;
        int l = stringtokenizer.hasMoreTokens() ? Integer.parseInt(stringtokenizer.nextToken()) - 1 : 0;
        String s1 = dicomobject.getS_ge(j, k, l);
        return s1 == null ? "" : s1;
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(((Throwable) (classnotfoundexception)).getMessage());
        }
    }

    static 
    {
        _defProp = new Properties();
        try
        {
            InputStream inputstream = (com.tiani.dicom.overlay.AttribOverlayFactory.class).getResourceAsStream("AttribOverlayFactory.properties");
            _defProp.load(inputstream);
            inputstream.close();
        }
        catch(IOException ioexception)
        {
            throw new RuntimeException(((Throwable) (ioexception)).toString());
        }
    }
}
