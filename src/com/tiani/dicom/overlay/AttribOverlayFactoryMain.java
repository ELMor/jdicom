// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AttribOverlayFactoryMain.java

package com.tiani.dicom.overlay;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.util.CommandLineParser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.tiani.dicom.overlay:
//            AttribOverlayFactory, Overlay, BurnInOverlay

public class AttribOverlayFactoryMain
{

    private final AttribOverlayFactory _factory;
    private final int _rows;
    private final int _columns;
    private final int _repeat;
    private final boolean _burnin;
    private final String _src;
    private final String _dest;
    private static final String USAGE = "Usage: AttribOverlayFactoryMain <src> <dest> [-prop <propFile>] [-burnin]\n                         [-dim <rows> <columns>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n-prop        use specified properties instead default\n <propFile>  properties file path\n-burnin      burn overlays into pixeldata\n-dim         specifies overlay dimension (default equal as pixel data)\n <rows>      number of rows\n <columns>   number of columns\n-repeat      repeat operation <count> times";
    private static final String OPTIONS[] = {
        "", "-prop", "-burnin", "-dim", "-repeat"
    };
    private static final int PARAMNUM[] = {
        2, 1, 0, 2, 1
    };

    public static void main(String args[])
    {
        try
        {
            (new AttribOverlayFactoryMain(CommandLineParser.parse(args, OPTIONS, PARAMNUM))).execute();
        }
        catch(IllegalArgumentException illegalargumentexception)
        {
            ((Throwable) (illegalargumentexception)).printStackTrace(System.out);
            System.out.println(((Object) (illegalargumentexception)));
            System.out.println("Usage: AttribOverlayFactoryMain <src> <dest> [-prop <propFile>] [-burnin]\n                         [-dim <rows> <columns>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n-prop        use specified properties instead default\n <propFile>  properties file path\n-burnin      burn overlays into pixeldata\n-dim         specifies overlay dimension (default equal as pixel data)\n <rows>      number of rows\n <columns>   number of columns\n-repeat      repeat operation <count> times");
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(System.out);
        }
    }

    private AttribOverlayFactoryMain(Hashtable hashtable)
        throws IOException, DicomException
    {
        _src = CommandLineParser.get(hashtable, "", 0);
        _dest = CommandLineParser.get(hashtable, "", 1);
        _factory = new AttribOverlayFactory(hashtable.containsKey("-prop") ? loadProperties(CommandLineParser.get(hashtable, "-prop", 0)) : null);
        if(hashtable.containsKey("-dim"))
        {
            _rows = Integer.parseInt(CommandLineParser.get(hashtable, "-dim", 0));
            _columns = Integer.parseInt(CommandLineParser.get(hashtable, "-dim", 1));
        } else
        {
            _rows = 0;
            _columns = 0;
        }
        _burnin = hashtable.containsKey("-burnin");
        _repeat = hashtable.containsKey("-repeat") ? Integer.parseInt(CommandLineParser.get(hashtable, "-repeat", 0)) : 1;
    }

    private void execute()
        throws IOException, DicomException
    {
        for(int i = 0; i < _repeat; i++)
        {
            FileInputStream fileinputstream = new FileInputStream(_src);
            DicomObject dicomobject = new DicomObject();
            try
            {
                TianiInputStream tianiinputstream = new TianiInputStream(((InputStream) (fileinputstream)));
                tianiinputstream.read(dicomobject, true);
            }
            finally
            {
                ((InputStream) (fileinputstream)).close();
            }
            int j = _rows <= 0 ? dicomobject.getI(466) : _rows;
            int k = _columns <= 0 ? dicomobject.getI(467) & -8 : _columns;
            long l = System.currentTimeMillis();
            _factory.createOverlay(j, k, dicomobject, Overlay.getFreeOverlayGroup(dicomobject));
            long l1 = System.currentTimeMillis();
            System.out.println("createOverlay takes " + (float)(l1 - l) / 1000F + " s");
            if(_burnin)
            {
                BurnInOverlay.burnInAll(dicomobject);
                long l2 = System.currentTimeMillis();
                System.out.println("burn in takes " + (float)(l2 - l1) / 1000F + " s");
            }
            FileOutputStream fileoutputstream = new FileOutputStream(_dest);
            try
            {
                dicomobject.write(((OutputStream) (fileoutputstream)), true);
            }
            finally
            {
                ((OutputStream) (fileoutputstream)).close();
            }
        }

    }

    private static Properties loadProperties(String s)
        throws IOException
    {
        FileInputStream fileinputstream = new FileInputStream(s);
        Properties properties = new Properties();
        try
        {
            properties.load(((InputStream) (fileinputstream)));
        }
        finally
        {
            ((InputStream) (fileinputstream)).close();
        }
        return properties;
    }

}
