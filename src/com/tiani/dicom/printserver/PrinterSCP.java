// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrinterSCP.java

package com.tiani.dicom.printserver;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DefNGetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;

// Referenced classes of package com.tiani.dicom.printserver:
//            Param

class PrinterSCP
{

    private Param _param;

    public PrinterSCP(Param param)
    {
        _param = param;
    }

    public void setParam(Param param)
    {
        _param = param;
    }

    public IDimseRqListener getNGetListener()
    {
        return ((IDimseRqListener) (new DefNGetSCP() {

            protected int get(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
                throws DicomException
            {
                DicomObject dicomobject = createAttributs();
                DicomObject dicomobject1 = dicomobject;
                int i = ((DicomObject) (dicommessage)).getSize(16);
                if(i > 0)
                {
                    dicomobject1 = new DicomObject();
                    for(int j = 0; j < i; j++)
                        PrinterSCP.copyAttrib(((DicomObject) (dicommessage)).getI(16, j), dicomobject, dicomobject1);

                }
                dicommessage1.setDataset(dicomobject1);
                return 0;
            }

        }));
    }

    private DicomObject createAttributs()
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(760, ((Object) (_param.getPrinterStatus())));
        dicomobject.set(761, ((Object) (_param.getPrinterStatusInfo())));
        dicomobject.set(762, ((Object) (_param.getPrinterName())));
        dicomobject.set(84, ((Object) (_param.getManufacturer())));
        dicomobject.set(105, ((Object) (_param.getManufacturerModelName())));
        dicomobject.set(228, ((Object) (_param.getDeviceSerialNumber())));
        dicomobject.set(236, ((Object) (_param.getSoftwareVersion())));
        dicomobject.set(317, ((Object) (_param.getDateOfLastCalibration())));
        dicomobject.set(318, ((Object) (_param.getTimeOfLastCalibration())));
        return dicomobject;
    }

    private static void copyAttrib(int i, DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        int j = i >> 16;
        int k = i & 0xffff;
        dicomobject1.set_ge(j, k, dicomobject.get_ge(j, k));
    }


}
