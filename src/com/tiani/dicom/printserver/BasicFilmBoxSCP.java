// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BasicFilmBoxSCP.java

package com.tiani.dicom.printserver;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UIDEntry;
import com.tiani.dicom.framework.DefNCreateSCP;
import com.tiani.dicom.framework.DefNSetSCP;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.IDimseRqListener;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.UIDUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.printserver:
//            Param

class BasicFilmBoxSCP
{

    private Param param;
    private static final String DISPLAY_FORMATS[] = {
        "STANDARD", "ROW", "COL"
    };

    public BasicFilmBoxSCP(Param param1)
    {
        param = param1;
    }

    public void setParam(Param param1)
    {
        param = param1;
    }

    public IDimseRqListener getColorImageBoxNSetListener()
    {
        return ((IDimseRqListener) (new DefNSetSCP() {

            protected int set(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
                throws DicomException
            {
                DicomObject dicomobject = dicommessage.getDataset();
                if(dicomobject == null)
                    return 288;
                else
                    return store(s1, "1.2.840.10008.5.1.1.30", (DicomObject)dicomobject.get(733));
            }

        }));
    }

    public IDimseRqListener getGrayscaleImageBoxNSetListener()
    {
        return ((IDimseRqListener) (new DefNSetSCP() {

            protected int set(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
                throws DicomException
            {
                DicomObject dicomobject = dicommessage.getDataset();
                if(dicomobject == null)
                    return 288;
                else
                    return store(s1, "1.2.840.10008.5.1.1.29", (DicomObject)dicomobject.get(732));
            }

        }));
    }

    private int store(String s, String s1, DicomObject dicomobject)
    {
        if(dicomobject == null)
            return 288;
        if(param.isStoreHC())
            try
            {
                storeHardcopyImage(s, makeHardcopyImage(s1, dicomobject));
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(Debug.out);
            }
        return 0;
    }

    private DicomObject makeHardcopyImage(String s, DicomObject dicomobject)
        throws DicomException
    {
        dicomobject.set(147, ((Object) (null)));
        dicomobject.set(148, ((Object) (null)));
        dicomobject.set(150, ((Object) (null)));
        dicomobject.set(152, ((Object) (null)));
        dicomobject.set(425, ((Object) (UIDUtils.createUID())));
        dicomobject.set(64, ((Object) (null)));
        dicomobject.set(70, ((Object) (null)));
        dicomobject.set(88, ((Object) (null)));
        dicomobject.set(427, ((Object) (null)));
        dicomobject.set(77, ((Object) (null)));
        dicomobject.set(81, "HC");
        dicomobject.set(426, ((Object) (UIDUtils.createUID())));
        dicomobject.set(428, ((Object) (null)));
        dicomobject.set(84, ((Object) (null)));
        dicomobject.set(430, ((Object) (null)));
        dicomobject.set(62, ((Object) (s)));
        dicomobject.set(63, ((Object) (UIDUtils.createUID())));
        return dicomobject;
    }

    private void storeHardcopyImage(String s, DicomObject dicomobject)
        throws DicomException, IOException
    {
        File file = new File(param.getStoreHCPath());
        if(!file.exists())
            file.mkdirs();
        UIDEntry uidentry = param.getStoreHCTS();
        FileMetaInformation filemetainformation = new FileMetaInformation(dicomobject, uidentry.getValue());
        dicomobject.setFileMetaInformation(((DicomObject) (filemetainformation)));
        File file1 = new File(file, s);
        FileOutputStream fileoutputstream = new FileOutputStream(file1);
        try
        {
            dicomobject.write(((OutputStream) (fileoutputstream)), true, uidentry.getConstant(), false);
            if(Debug.DEBUG > 0)
                Debug.out.println("Stored Hardcopy image to " + file1);
        }
        finally
        {
            ((OutputStream) (fileoutputstream)).close();
        }
    }

    public IDimseRqListener getNCreateListener()
    {
        return ((IDimseRqListener) (new DefNCreateSCP() {

            protected int create(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
                throws DicomException
            {
                if(s1 == null)
                    ((DicomObject) (dicommessage1)).set(13, ((Object) (UIDUtils.createUID())));
                DicomObject dicomobject = dicommessage.getDataset();
                switch(dicommessage.getAbstractSyntax().getConstant())
                {
                case 12292: 
                    setImageBoxSeq(dicomobject, "1.2.840.10008.5.1.1.4");
                    break;

                case 12294: 
                    setImageBoxSeq(dicomobject, "1.2.840.10008.5.1.1.4.1");
                    break;
                }
                dicommessage1.setDataset(dicomobject);
                return 0;
            }

        }));
    }

    private void setImageBoxSeq(DicomObject dicomobject, String s)
        throws DicomException
    {
        int i = 0;
        switch(Param.indexOfIn(dicomobject.getS(713), DISPLAY_FORMATS))
        {
        case 0: // '\0'
            i = getProduct(dicomobject.getS(713, 1));
            break;

        case 1: // '\001'
        case 2: // '\002'
            i = getSum(dicomobject.getS(713, 1));
            break;
        }
        while(i-- > 0) 
        {
            DicomObject dicomobject1 = new DicomObject();
            dicomobject1.set(115, ((Object) (s)));
            dicomobject1.set(116, ((Object) (UIDUtils.createUID())));
            dicomobject.append(726, ((Object) (dicomobject1)));
        }
    }

    private static int getProduct(String s)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, ", ");
        try
        {
            if(stringtokenizer.countTokens() == 2)
                return Integer.parseInt(stringtokenizer.nextToken()) * Integer.parseInt(stringtokenizer.nextToken());
        }
        catch(NumberFormatException numberformatexception) { }
        return 0;
    }

    private static int getSum(String s)
    {
        int i = 0;
        StringTokenizer stringtokenizer = new StringTokenizer(s, ", ");
        try
        {
            while(stringtokenizer.hasMoreTokens()) 
                i += Integer.parseInt(stringtokenizer.nextToken());
            return i;
        }
        catch(NumberFormatException numberformatexception)
        {
            return 0;
        }
    }



}
