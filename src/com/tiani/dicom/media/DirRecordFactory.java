// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DirRecordFactory.java

package com.tiani.dicom.media;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.media:
//            FileMetaInformation

/**
 * @deprecated Class DirRecordFactory is deprecated
 */

public class DirRecordFactory
{

    private Hashtable _iods;
    private static Hashtable _defaultIODs;

    public DirRecordFactory()
    {
        _iods = _defaultIODs;
    }

    public DirRecordFactory(Hashtable hashtable)
    {
        _iods = _defaultIODs;
        _iods = hashtable;
    }

    public DicomObject create(String s, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, _iods);
        return dicomobject1;
    }

    public void init(DicomObject dicomobject, String s, DicomObject dicomobject1)
        throws DicomException
    {
        init(dicomobject, s, dicomobject1, _iods);
    }

    public static IDicomObjectFilter createFilter(final String type)
    {
        return new IDicomObjectFilter() {

            public boolean accept(DicomObject dicomobject)
                throws DicomException
            {
                return type.equals(((Object) (dicomobject.getS(48))));
            }

        };
    }

    public static IDicomObjectFilter createFilter(final String type, final int dname, final String val)
    {
        return new IDicomObjectFilter() {

            public boolean accept(DicomObject dicomobject)
                throws DicomException
            {
                return type.equals(((Object) (dicomobject.getS(48)))) && val.equals(((Object) (dicomobject.getS(dname))));
            }

        };
    }

    public static DicomObjectFilterPath createImageFilterPath(DicomObject dicomobject)
        throws DicomException
    {
        DicomObjectFilterPath dicomobjectfilterpath = new DicomObjectFilterPath();
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("PATIENT", 148, dicomobject.getS(148)))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("STUDY", 425, dicomobject.getS(425)))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("SERIES", 426, dicomobject.getS(426)))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("IMAGE", 53, dicomobject.getS(63)))));
        return dicomobjectfilterpath;
    }

    public DicomObject createImageDR(DicomObject dicomobject, String as[])
        throws DicomException
    {
        DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
        FileMetaInformation.check(dicomobject1);
        DicomObject dicomobject2 = create("IMAGE", dicomobject);
        initReferencedSOP(dicomobject2, as, dicomobject1.getS(29), dicomobject1.getS(30), dicomobject1.getS(31));
        return dicomobject2;
    }

    public DicomObjectPath createImageDRPath(DicomObject dicomobject, String as[])
        throws DicomException
    {
        DicomObjectPath dicomobjectpath = new DicomObjectPath();
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("PATIENT", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("STUDY", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("SERIES", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (createImageDR(dicomobject, as))));
        return dicomobjectpath;
    }

    public static void init(DicomObject dicomobject, String s)
        throws DicomException
    {
        dicomobject.set(48, ((Object) (s)));
        dicomobject.set(46, ((Object) (new Integer(65535))));
        dicomobject.set(45, ((Object) (new Integer(0))));
        dicomobject.set(47, ((Object) (new Integer(0))));
    }

    public static DicomObject create(String s)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        init(dicomobject, s);
        return dicomobject;
    }

    public static void init(DicomObject dicomobject, String s, DicomObject dicomobject1, int ai[])
        throws DicomException
    {
        init(dicomobject, s);
        copy(dicomobject, dicomobject1, ai);
    }

    public static DicomObject create(String s, DicomObject dicomobject, int ai[])
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, ai);
        return dicomobject1;
    }

    public static void init(DicomObject dicomobject, String s, DicomObject dicomobject1, Hashtable hashtable)
        throws DicomException
    {
        init(dicomobject, s, dicomobject1, (int[])hashtable.get(((Object) (s))));
    }

    public static DicomObject create(String s, DicomObject dicomobject, Hashtable hashtable)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, hashtable);
        return dicomobject1;
    }

    public static void initReferencedSOP(DicomObject dicomobject, String as[], String s, String s1, String s2)
        throws DicomException
    {
        dicomobject.deleteItem(50);
        for(int i = 0; i < as.length; i++)
            dicomobject.append(50, ((Object) (as[i])));

        dicomobject.set(52, ((Object) (s)));
        dicomobject.set(53, ((Object) (s1)));
        dicomobject.set(54, ((Object) (s2)));
    }

    public static void copy(DicomObject dicomobject, DicomObject dicomobject1, int ai[])
        throws DicomException
    {
        for(int i = 0; i < ai.length; i++)
            copy(dicomobject, dicomobject1, ai[i]);

    }

    public static void copy(DicomObject dicomobject, DicomObject dicomobject1, int i)
        throws DicomException
    {
        if(dicomobject1.getSize(i) >= 0)
            dicomobject.set(i, dicomobject1.get(i));
    }

    static 
    {
        _defaultIODs = new Hashtable();
        _defaultIODs.put("PATIENT", ((Object) (new int[] {
            57, 147, 148
        })));
        _defaultIODs.put("STUDY", ((Object) (new int[] {
            57, 64, 70, 77, 427, 425, 95
        })));
        _defaultIODs.put("SERIES", ((Object) (new int[] {
            57, 81, 428, 426, 699
        })));
        _defaultIODs.put("IMAGE", ((Object) (new int[] {
            57, 430, 699
        })));
        _defaultIODs.put("OVERLAY", ((Object) (new int[] {
            57, 437, 699
        })));
        _defaultIODs.put("MODALITY LUT", ((Object) (new int[] {
            57, 439
        })));
        _defaultIODs.put("VOI LUT", ((Object) (new int[] {
            57, 439
        })));
        _defaultIODs.put("CURVE", ((Object) (new int[] {
            57, 438
        })));
        _defaultIODs.put("TOPIC", ((Object) (new int[] {
            57, 700, 701, 702, 703
        })));
        _defaultIODs.put("VISIT", ((Object) (new int[] {
            57, 562, 554, 85
        })));
        _defaultIODs.put("RESULTS", ((Object) (new int[] {
            57, 1104, 59
        })));
        _defaultIODs.put("INTERPRETATION", ((Object) (new int[] {
            57, 1111, 1115, 1120, 1121, 1125, 1127, 1128
        })));
        _defaultIODs.put("STUDY COMPONENT", ((Object) (new int[] {
            57, 81, 95, 96, 100
        })));
        _defaultIODs.put("STORED PRINT", ((Object) (new int[] {
            57, 430, 699
        })));
    }
}
