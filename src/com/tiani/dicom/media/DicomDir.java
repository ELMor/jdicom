// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomDir.java

package com.tiani.dicom.media;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;
import com.tiani.dicom.util.UIDUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.media:
//            FileMetaInformation

/**
 * @deprecated Class DicomDir is deprecated
 */

public class DicomDir extends DicomObject
{

    private Hashtable _records;
    private int _readLock;
    private int _writeLock;

    public DicomDir()
    {
        _records = new Hashtable(179);
        _readLock = 0;
        _writeLock = 0;
    }

    public DicomDir(String s, String s1)
        throws DicomException
    {
        _records = new Hashtable(179);
        _readLock = 0;
        _writeLock = 0;
        ((DicomObject)this).set(38, ((Object) (s)));
        ((DicomObject)this).setFileMetaInformation(((DicomObject) (new FileMetaInformation("1.2.840.10008.1.3.10", s1, "1.2.840.10008.1.2.1"))));
        reset();
    }

    public void reset()
        throws DicomException
    {
        ((DicomObject)this).set(44, ((Object) (null)));
        ((DicomObject)this).set(41, ((Object) (new Long(0L))));
        ((DicomObject)this).set(42, ((Object) (new Long(0L))));
        ((DicomObject)this).set(43, ((Object) (new Integer(0))));
        _records.clear();
    }

    public DicomDir(String s)
        throws DicomException
    {
        this(s, UIDUtils.createUID());
    }

    public String getFileSetID()
    {
        return (String)((DicomObject)this).get(38);
    }

    public String getFileSetUID()
    {
        DicomObject dicomobject = ((DicomObject)this).getFileMetaInformation();
        return dicomobject == null ? null : (String)dicomobject.get(30);
    }

    public void write(OutputStream outputstream)
        throws DicomException, IOException
    {
        super.write(outputstream, true, 8194, false, false);
    }

    public void write(OutputStream outputstream, boolean flag, int i, boolean flag1, boolean flag2)
        throws DicomException
    {
        throw new DicomException("Invalid use of DicomObject.write instead DicomDir.write");
    }

    public void init()
    {
        int i = ((DicomObject)this).getSize(44);
        _records = new Hashtable(i <= 64 ? 64 : i);
        for(int j = 0; j < i; j++)
            _records.put(((Object) (new Long(((DicomObject)this).getOffset(44, j)))), ((DicomObject)this).get(44, j));

    }

    public void addRootDR(DicomObject dicomobject)
        throws DicomException
    {
        Long long1 = _appendDR(dicomobject);
        Long long2 = (Long)((DicomObject)this).get(41);
        if(long2.longValue() == 0L)
            ((DicomObject)this).set(41, ((Object) (long1)));
        else
            _setNextOffset(long2, long1);
    }

    public void addLowerDR(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        Long long1 = _appendDR(dicomobject);
        Long long2 = (Long)dicomobject1.get(47);
        if(long2.longValue() == 0L)
            dicomobject1.set(47, ((Object) (long1)));
        else
            _setNextOffset(long2, long1);
    }

    public DicomObjectPath addRootDRpaths(DicomObjectPath dicomobjectpath, DicomObjectFilterPath dicomobjectfilterpath)
        throws DicomException
    {
        DicomObject adicomobject[] = listRootDRs(dicomobjectfilterpath.first());
        DicomObject dicomobject;
        switch(adicomobject.length)
        {
        case 0: // '\0'
            addRootDR(dicomobject = dicomobjectpath.first());
            break;

        case 1: // '\001'
            dicomobject = adicomobject[0];
            break;

        default:
            return null;
        }
        DicomObjectPath dicomobjectpath1 = dicomobjectpath.remaining();
        return ((Vector) (dicomobjectpath1)).isEmpty() ? new DicomObjectPath(dicomobject) : new DicomObjectPath(dicomobject, addLowerDRpaths(dicomobjectpath1, dicomobjectfilterpath.remaining(), dicomobject));
    }

    public DicomObjectPath addLowerDRpaths(DicomObjectPath dicomobjectpath, DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject adicomobject[] = listLowerDRs(dicomobjectfilterpath.first(), dicomobject);
        DicomObject dicomobject1;
        switch(adicomobject.length)
        {
        case 0: // '\0'
            addLowerDR(dicomobject1 = dicomobjectpath.first(), dicomobject);
            break;

        case 1: // '\001'
            dicomobject1 = adicomobject[0];
            break;

        default:
            return null;
        }
        DicomObjectPath dicomobjectpath1 = dicomobjectpath.remaining();
        return ((Vector) (dicomobjectpath1)).isEmpty() ? new DicomObjectPath(dicomobject1) : new DicomObjectPath(dicomobject1, addLowerDRpaths(dicomobjectpath1, dicomobjectfilterpath.remaining(), dicomobject1));
    }

    public DicomObject getLastDR()
        throws DicomException
    {
        return _findNext((Long)((DicomObject)this).get(42), _records, false);
    }

    public DicomObject getFirstDR(boolean flag)
        throws DicomException
    {
        return _findNext((Long)((DicomObject)this).get(41), _records, flag);
    }

    public DicomObject getFirstDR()
        throws DicomException
    {
        return getFirstDR(true);
    }

    public DicomObject getNextDR(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        return _findNext((Long)dicomobject.get(45), _records, flag);
    }

    public DicomObject getNextDR(DicomObject dicomobject)
        throws DicomException
    {
        return getNextDR(dicomobject, true);
    }

    public DicomObject getLowerDR(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        return _findNext((Long)dicomobject.get(47), _records, flag);
    }

    public DicomObject getLowerDR(DicomObject dicomobject)
        throws DicomException
    {
        return getLowerDR(dicomobject, true);
    }

    public DicomObject[] listRootDRs(boolean flag)
        throws DicomException
    {
        return listNextDRs(getFirstDR(flag), flag);
    }

    public DicomObject[] listRootDRs()
        throws DicomException
    {
        return listRootDRs(true);
    }

    public DicomObject[] listRootDRs(IDicomObjectFilter idicomobjectfilter, boolean flag)
        throws DicomException
    {
        return listNextDRs(idicomobjectfilter, getFirstDR(flag), flag);
    }

    public DicomObject[] listRootDRs(IDicomObjectFilter idicomobjectfilter)
        throws DicomException
    {
        return listRootDRs(idicomobjectfilter, true);
    }

    public DicomObject[] listLowerDRs(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        return listNextDRs(getLowerDR(dicomobject, flag), flag);
    }

    public DicomObject[] listLowerDRs(DicomObject dicomobject)
        throws DicomException
    {
        return listLowerDRs(dicomobject, true);
    }

    public DicomObject[] listLowerDRs(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        return listNextDRs(idicomobjectfilter, getLowerDR(dicomobject, flag), flag);
    }

    public DicomObject[] listLowerDRs(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject)
        throws DicomException
    {
        return listLowerDRs(idicomobjectfilter, dicomobject, true);
    }

    public DicomObject[] listNextDRs(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        return listNextDRs(((IDicomObjectFilter) (null)), dicomobject, flag);
    }

    public DicomObject[] listNextDRs(DicomObject dicomobject)
        throws DicomException
    {
        return listNextDRs(dicomobject, true);
    }

    public DicomObjectPath[] listRootDRpaths(DicomObjectFilterPath dicomobjectfilterpath, boolean flag)
        throws DicomException
    {
        Vector vector = new Vector();
        if(!((Vector) (dicomobjectfilterpath)).isEmpty())
        {
            DicomObject adicomobject[] = listRootDRs(dicomobjectfilterpath.first(), flag);
            DicomObjectFilterPath dicomobjectfilterpath1 = dicomobjectfilterpath.remaining();
            for(int i = 0; i < adicomobject.length; i++)
                if(!((Vector) (dicomobjectfilterpath1)).isEmpty())
                {
                    DicomObjectPath adicomobjectpath1[] = listLowerDRpaths(dicomobjectfilterpath1, adicomobject[i], flag);
                    if(adicomobjectpath1.length > 0)
                    {
                        for(int j = 0; j < adicomobjectpath1.length; j++)
                            vector.addElement(((Object) (new DicomObjectPath(adicomobject[i], adicomobjectpath1[j]))));

                    }
                } else
                {
                    vector.addElement(((Object) (new DicomObjectPath(adicomobject[i]))));
                }

        }
        DicomObjectPath adicomobjectpath[] = new DicomObjectPath[vector.size()];
        vector.copyInto(((Object []) (adicomobjectpath)));
        return adicomobjectpath;
    }

    public DicomObjectPath[] listRootDRpaths(DicomObjectFilterPath dicomobjectfilterpath)
        throws DicomException
    {
        return listRootDRpaths(dicomobjectfilterpath, true);
    }

    public DicomObjectPath[] listLowerDRpaths(DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        Vector vector = new Vector();
        if(!((Vector) (dicomobjectfilterpath)).isEmpty())
        {
            DicomObject adicomobject[] = listLowerDRs(dicomobjectfilterpath.first(), dicomobject, flag);
            DicomObjectFilterPath dicomobjectfilterpath1 = dicomobjectfilterpath.remaining();
            for(int i = 0; i < adicomobject.length; i++)
                if(!((Vector) (dicomobjectfilterpath1)).isEmpty())
                {
                    DicomObjectPath adicomobjectpath1[] = listLowerDRpaths(dicomobjectfilterpath1, adicomobject[i], flag);
                    if(adicomobjectpath1.length > 0)
                    {
                        for(int j = 0; j < adicomobjectpath1.length; j++)
                            vector.addElement(((Object) (new DicomObjectPath(adicomobject[i], adicomobjectpath1[j]))));

                    }
                } else
                {
                    vector.addElement(((Object) (new DicomObjectPath(adicomobject[i]))));
                }

        }
        DicomObjectPath adicomobjectpath[] = new DicomObjectPath[vector.size()];
        vector.copyInto(((Object []) (adicomobjectpath)));
        return adicomobjectpath;
    }

    public DicomObjectPath[] listLowerDRpaths(DicomObjectFilterPath dicomobjectfilterpath, DicomObject dicomobject)
        throws DicomException
    {
        return listLowerDRpaths(dicomobjectfilterpath, dicomobject, true);
    }

    public DicomObject[] listNextDRs(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        Vector vector = new Vector();
        for(DicomObject dicomobject1 = dicomobject; dicomobject1 != null; dicomobject1 = getNextDR(dicomobject1, flag))
            if(idicomobjectfilter == null || idicomobjectfilter.accept(dicomobject1))
                vector.addElement(((Object) (dicomobject1)));

        DicomObject adicomobject[] = new DicomObject[vector.size()];
        vector.copyInto(((Object []) (adicomobject)));
        return adicomobject;
    }

    public DicomObject[] listNextDRs(IDicomObjectFilter idicomobjectfilter, DicomObject dicomobject)
        throws DicomException
    {
        return listNextDRs(idicomobjectfilter, dicomobject, true);
    }

    public Vector deleteDR(DicomObject dicomobject)
        throws DicomException
    {
        Vector vector = new Vector();
        vector.addElement(((Object) (dicomobject)));
        for(DicomObject dicomobject1 = getLowerDR(dicomobject, true); dicomobject1 != null; dicomobject1 = getNextDR(dicomobject1, true))
        {
            for(Enumeration enumeration = deleteDR(dicomobject1).elements(); enumeration.hasMoreElements(); vector.addElement(enumeration.nextElement()));
        }

        dicomobject.set(46, ((Object) (new Integer(0))));
        return vector;
    }

    public Vector deleteDRs(DicomObjectFilterPath dicomobjectfilterpath)
        throws DicomException
    {
        Vector vector = new Vector();
        DicomObjectPath adicomobjectpath[] = listRootDRpaths(dicomobjectfilterpath);
        for(int i = 0; i < adicomobjectpath.length; i++)
        {
            for(Enumeration enumeration = deleteDR(adicomobjectpath[i].last()).elements(); enumeration.hasMoreElements(); vector.addElement(enumeration.nextElement()));
        }

        return vector;
    }

    public void replaceDR(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        Long long1 = (Long)dicomobject.get(47);
        dicomobject.set(46, ((Object) (new Integer(0))));
        dicomobject.set(47, ((Object) (new Long(0L))));
        dicomobject1.set(47, ((Object) (long1)));
        Long long2 = _appendDR(dicomobject1);
        Long long3 = (Long)dicomobject.get(45);
        if(long3.longValue() == 0L)
            dicomobject.set(45, ((Object) (long2)));
        else
            _setNextOffset(long3, long2);
    }

    public void clearDRs()
        throws DicomException
    {
        _records.clear();
        ((DicomObject)this).set(44, ((Object) (null)));
        ((DicomObject)this).set(41, ((Object) (new Long(0L))));
        ((DicomObject)this).set(42, ((Object) (new Long(0L))));
    }

    public boolean isCompactable()
        throws DicomException
    {
        for(Enumeration enumeration = _records.elements(); enumeration.hasMoreElements();)
        {
            DicomObject dicomobject = (DicomObject)enumeration.nextElement();
            if(dicomobject.getI(46) == 0)
                return true;
        }

        return false;
    }

    public void compact()
        throws DicomException
    {
        DicomObject dicomobject = getFirstDR(true);
        Hashtable hashtable = _records;
        _records = new Hashtable(179);
        clearDRs();
        Vector vector = new Vector();
        Long long1;
        for(; dicomobject != null; dicomobject = _findNext(long1, hashtable, true))
        {
            vector.addElement(((Object) (dicomobject)));
            addRootDR(dicomobject);
            long1 = (Long)dicomobject.get(45);
            dicomobject.set(45, ((Object) (new Long(0L))));
        }

        DicomObject dicomobject1;
        for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); _compactChilds(dicomobject1, hashtable))
            dicomobject1 = (DicomObject)enumeration.nextElement();

    }

    public synchronized int addReadLock()
    {
        while(_writeLock > 0) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException interruptedexception) { }
        return ++_readLock;
    }

    public synchronized int addWriteLock()
    {
        while(_writeLock > 0 || _readLock > 0) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException interruptedexception) { }
        return ++_writeLock;
    }

    public synchronized int releaseReadLock()
    {
        _readLock--;
        ((Object)this).notifyAll();
        return _readLock;
    }

    public synchronized int releaseWriteLock()
    {
        _writeLock--;
        ((Object)this).notifyAll();
        return _writeLock;
    }

    private Long _appendDR(DicomObject dicomobject)
        throws DicomException
    {
        ((DicomObject)this).append(44, ((Object) (dicomobject)));
        Long long1 = new Long(((DicomObject)this).calculateOffset(44, _records.size(), 8194, false, false));
        ((DicomObject)this).set(42, ((Object) (long1)));
        _records.put(((Object) (long1)), ((Object) (dicomobject)));
        return long1;
    }

    private void _setNextOffset(Long long1, Long long2)
        throws DicomException
    {
        DicomObject dicomobject = null;
        do
            if((dicomobject = (DicomObject)_records.get(((Object) (long1)))) == null)
                throw new DicomException("No Directory Record at offset " + long1);
        while((long1 = (Long)dicomobject.get(45)).longValue() != 0L);
        dicomobject.set(45, ((Object) (long2)));
    }

    private void _compactChilds(DicomObject dicomobject, Hashtable hashtable)
        throws DicomException
    {
        Long long1 = (Long)dicomobject.get(47);
        dicomobject.set(47, ((Object) (new Long(0L))));
        for(DicomObject dicomobject1 = _findNext(long1, hashtable, true); dicomobject1 != null; dicomobject1 = _findNext(long1, hashtable, true))
        {
            addLowerDR(dicomobject1, dicomobject);
            _compactChilds(dicomobject1, hashtable);
            long1 = (Long)dicomobject1.get(45);
            dicomobject1.set(45, ((Object) (new Long(0L))));
        }

    }

    private DicomObject _findNext(Long long1, Hashtable hashtable, boolean flag)
        throws DicomException
    {
        if(long1.longValue() == 0L)
            return null;
        DicomObject dicomobject = (DicomObject)hashtable.get(((Object) (long1)));
        if(dicomobject == null)
            throw new DicomException("No Directory Record at offset " + long1);
        else
            return flag && dicomobject.getI(46) == 0 ? _findNext((Long)dicomobject.get(45), hashtable, flag) : dicomobject;
    }
}
