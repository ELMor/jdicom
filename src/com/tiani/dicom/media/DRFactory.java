// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DRFactory.java

package com.tiani.dicom.media;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.tiani.dicom.util.DicomObjectFilterPath;
import com.tiani.dicom.util.DicomObjectPath;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.media:
//            DRNode, FileMetaInformation

public class DRFactory
{

    public static final String PATIENT = "PATIENT";
    public static final String STUDY = "STUDY";
    public static final String SERIES = "SERIES";
    public static final String IMAGE = "IMAGE";
    public static final String OVERLAY = "OVERLAY";
    public static final String MODALITY_LUT = "MODALITY LUT";
    public static final String VOI_LUT = "VOI LUT";
    public static final String CURVE = "CURVE";
    public static final String TOPIC = "TOPIC";
    public static final String VISIT = "VISIT";
    public static final String RESULT = "RESULT";
    public static final String INTERPRETATION = "INTERPRETATION";
    public static final String STUDY_COMPONENT = "STUDY COMPONENT";
    public static final String STORED_PRINT = "STORED PRINT";
    public static final String RT_DOSE = "RT DOSE";
    public static final String RT_STRUCTURE_SET = "RT STRUCTURE SET";
    public static final String RT_PLAN = "RT PLAN";
    public static final String RT_TREAT_RECORD = "RT TREAT RECORD";
    public static final String PRESENTATION = "PRESENTATION";
    public static final String SR_DOCUMENT = "SR DOCUMENT";
    public static final String KO_DOCUMENT = "KEY OBJECT DOC";
    public static final String RAW_DATA = "RAW DATA";
    public static final String IMAGE_TYPE_PATH[] = {
        "PATIENT", "STUDY", "SERIES", "IMAGE"
    };
    public static final String INSTANCE_TYPE_PATH[] = {
        "PATIENT", "STUDY", "SERIES", null
    };
    public static final int IN_USE = 65535;
    public static final int INACTIVE = 0;
    private static final String IMAGE_UIDS[] = {
        "1.2.840.10008.5.1.4.1.1.1", "1.2.840.10008.5.1.4.1.1.2", "1.2.840.10008.5.1.4.1.1.3.1", "1.2.840.10008.5.1.4.1.1.4", "1.2.840.10008.5.1.4.1.1.6.1", "1.2.840.10008.5.1.4.1.1.7", "1.2.840.10008.5.1.4.1.1.12.1", "1.2.840.10008.5.1.4.1.1.12.2", "1.2.840.10008.5.1.4.1.1.12.3", "1.2.840.10008.5.1.4.1.1.20", 
        "1.2.840.10008.5.1.1.29", "1.2.840.10008.5.1.1.20", "1.2.840.10008.5.1.4.1.1.128", "1.2.840.10008.5.1.4.1.1.481.1", "1.2.840.10008.5.1.4.1.1.3", "1.2.840.10008.5.1.4.1.1.5", "1.2.840.10008.5.1.4.1.1.6", "1.2.840.10008.5.1.4.1.1.77.1.1", "1.2.840.10008.5.1.4.1.1.77.1.2", "1.2.840.10008.5.1.4.1.1.77.1.3", 
        "1.2.840.10008.5.1.4.1.1.77.1.4", "1.2.840.10008.5.1.4.1.1.1.1", "1.2.840.10008.5.1.4.1.1.1.1.1", "1.2.840.10008.5.1.4.1.1.1.2", "1.2.840.10008.5.1.4.1.1.1.2.1", "1.2.840.10008.5.1.4.1.1.1.3", "1.2.840.10008.5.1.4.1.1.1.3.1"
    };
    private static final String PRESENTATION_UID = "1.2.840.10008.5.1.4.1.1.11.1";
    private static final String RT_DOSE_UID = "1.2.840.10008.5.1.4.1.1.481.2";
    private static final String RT_STRUCTURE_SET_UID = "1.2.840.10008.5.1.4.1.1.481.3";
    private static final String RT_PLAN_UID = "1.2.840.10008.5.1.4.1.1.481.5";
    private static final String RT_TREAT_RECORD_UIDS[] = {
        "1.2.840.10008.5.1.4.1.1.481.4", "1.2.840.10008.5.1.4.1.1.481.6", "1.2.840.10008.5.1.4.1.1.481.7"
    };
    private static final String SR_DOCUMENT_UIDS[] = {
        "1.2.840.10008.5.1.4.1.1.88.11", "1.2.840.10008.5.1.4.1.1.88.22", "1.2.840.10008.5.1.4.1.1.88.33"
    };
    private static final String OVERLAY_UID = "1.2.840.10008.5.1.4.1.1.8";
    private static final String KO_DOCUMENT_UID = "1.2.840.10008.5.1.4.1.1.88.59";
    private static final String RAW_DATA_UIDs[] = {
        "1.2.840.10008.5.1.4.1.1.66", "1.2.826.0.1.3680043.2.242.1.1"
    };
    private static final String CURVE_UIDS[] = {
        "1.2.840.10008.5.1.4.1.1.9", "1.2.840.10008.5.1.4.1.1.129"
    };
    private static final String MODALITY_LUT_UID = "1.2.840.10008.5.1.4.1.1.10";
    private static final String VOI_LUT_UID = "1.2.840.10008.5.1.4.1.1.11";
    private static final String STORED_PRINT_UID = "1.2.840.10008.5.1.1.27";
    public static final IDicomObjectFilter PATIENT_FILTER = createFilter("PATIENT");
    public static final IDicomObjectFilter STUDY_FILTER = createFilter("STUDY");
    public static final IDicomObjectFilter SERIES_FILTER = createFilter("SERIES");
    private Hashtable _tags;
    private Hashtable _groups;

    public static String typeOf(String s)
    {
        if(Arrays.asList(((Object []) (IMAGE_UIDS))).indexOf(((Object) (s))) != -1)
            return "IMAGE";
        if("1.2.840.10008.5.1.4.1.1.88.59".equals(((Object) (s))))
            return "KEY OBJECT DOC";
        if("1.2.840.10008.5.1.4.1.1.8".equals(((Object) (s))))
            return "OVERLAY";
        if("1.2.840.10008.5.1.4.1.1.10".equals(((Object) (s))))
            return "MODALITY LUT";
        if("1.2.840.10008.5.1.4.1.1.11".equals(((Object) (s))))
            return "VOI LUT";
        if(Arrays.asList(((Object []) (CURVE_UIDS))).indexOf(((Object) (s))) != -1)
            return "CURVE";
        if("1.2.840.10008.5.1.1.27".equals(((Object) (s))))
            return "STORED PRINT";
        if("1.2.840.10008.5.1.4.1.1.481.2".equals(((Object) (s))))
            return "RT DOSE";
        if("1.2.840.10008.5.1.4.1.1.481.5".equals(((Object) (s))))
            return "RT PLAN";
        if("1.2.840.10008.5.1.1.27".equals(((Object) (s))))
            return "STORED PRINT";
        if(Arrays.asList(((Object []) (RT_TREAT_RECORD_UIDS))).indexOf(((Object) (s))) != -1)
            return "RT TREAT RECORD";
        if("1.2.840.10008.5.1.4.1.1.11.1".equals(((Object) (s))))
            return "PRESENTATION";
        if(Arrays.asList(((Object []) (SR_DOCUMENT_UIDS))).indexOf(((Object) (s))) != -1)
            return "SR DOCUMENT";
        if(Arrays.asList(((Object []) (RAW_DATA_UIDs))).indexOf(((Object) (s))) != -1)
            return "RAW DATA";
        else
            return null;
    }

    public DRFactory()
    {
        this(loadDefProperties());
    }

    public DRFactory(Hashtable hashtable, Hashtable hashtable1)
    {
        _tags = hashtable;
        _groups = hashtable1;
    }

    public final Hashtable getTags()
    {
        return _tags;
    }

    public DRFactory(Properties properties)
    {
        _tags = parseProperties(properties, "");
        _groups = parseProperties(properties, "gr.");
    }

    public DicomObject create(String s, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, _tags, _groups);
        return dicomobject1;
    }

    public void init(DicomObject dicomobject, String s, DicomObject dicomobject1)
        throws DicomException
    {
        init(dicomobject, s, dicomobject1, _tags, _groups);
    }

    public static IDicomObjectFilter createFilter(final String type)
    {
        return new IDicomObjectFilter() {

            public boolean accept(DicomObject dicomobject)
                throws DicomException
            {
                return type.equals(((Object) (dicomobject.getS(48))));
            }

        };
    }

    public static IDicomObjectFilter createRefSOPInstanceFilter(final String uid)
    {
        if(uid == null || uid.length() == 0)
            return null;
        else
            return new IDicomObjectFilter() {

                public boolean accept(DicomObject dicomobject)
                    throws DicomException
                {
                    return uid.equals(((Object) (dicomobject.getS(53))));
                }

            };
    }

    public static IDicomObjectFilter createRefSOPClassFilter(final String uid)
    {
        if(uid == null || uid.length() == 0)
            return null;
        else
            return new IDicomObjectFilter() {

                public boolean accept(DicomObject dicomobject)
                    throws DicomException
                {
                    return uid.equals(((Object) (dicomobject.getS(52))));
                }

            };
    }

    public static IDicomObjectFilter createRefSOPFilter(final String classUID, final String instUID)
    {
        if(classUID == null || classUID.length() == 0)
            return createRefSOPInstanceFilter(instUID);
        if(instUID == null || instUID.length() == 0)
            return createRefSOPClassFilter(classUID);
        else
            return new IDicomObjectFilter() {

                public boolean accept(DicomObject dicomobject)
                    throws DicomException
                {
                    return classUID.equals(((Object) (dicomobject.getS(52)))) && instUID.equals(((Object) (dicomobject.getS(53))));
                }

            };
    }

    public static IDicomObjectFilter createFilter(final String type, final String val, final int dname, final int index)
    {
        if(val == null || val.length() == 0)
            return createFilter(type);
        else
            return new IDicomObjectFilter() {

                public boolean accept(DicomObject dicomobject)
                    throws DicomException
                {
                    return type.equals(((Object) (dicomobject.getS(48)))) && val.equals(((Object) (dicomobject.getS(dname, index))));
                }

            };
    }

    public static IDicomObjectFilter createFilter(String s, String s1, int i)
    {
        return createFilter(s, s1, i, 0);
    }

    public static DicomObjectFilterPath createRefSOPInstanceFilterPath(DicomObject dicomobject)
        throws DicomException
    {
        DicomObjectFilterPath dicomobjectfilterpath = new DicomObjectFilterPath();
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("PATIENT", dicomobject.getS(148), 148))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("STUDY", dicomobject.getS(425), 425))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createFilter("SERIES", dicomobject.getS(426), 426))));
        ((Vector) (dicomobjectfilterpath)).addElement(((Object) (createRefSOPInstanceFilter(dicomobject.getS(63)))));
        return dicomobjectfilterpath;
    }

    public DicomObject createInstanceDR(DicomObject dicomobject, String as[])
        throws DicomException
    {
        String s = typeOf((String)dicomobject.get(62));
        if(s == null)
            throw new DicomException("Could not recognized type of DICOM Object");
        else
            return createInstanceDR(s, dicomobject, as);
    }

    public DicomObject createInstanceDR(String s, DicomObject dicomobject, String as[])
        throws DicomException
    {
        DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
        FileMetaInformation.check(dicomobject1);
        DicomObject dicomobject2 = create(s, dicomobject);
        initReferencedSOP(dicomobject2, as, dicomobject1.getS(29), dicomobject1.getS(30), dicomobject1.getS(31));
        return dicomobject2;
    }

    public DicomObjectPath createInstanceDRPath(DicomObject dicomobject, String as[])
        throws DicomException
    {
        String s = typeOf((String)dicomobject.get(62));
        if(s == null)
            throw new DicomException("Could not recognized type of DICOM Object");
        else
            return createInstanceDRPath(s, dicomobject, as);
    }

    public DicomObjectPath createInstanceDRPath(String s, DicomObject dicomobject, String as[])
        throws DicomException
    {
        DicomObjectPath dicomobjectpath = new DicomObjectPath();
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("PATIENT", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("STUDY", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (create("SERIES", dicomobject))));
        ((Vector) (dicomobjectpath)).addElement(((Object) (createInstanceDR(s, dicomobject, as))));
        return dicomobjectpath;
    }

    public DRNode[] createInstanceDRNodePath(DicomObject dicomobject, String as[])
        throws DicomException
    {
        String s = typeOf((String)dicomobject.get(62));
        if(s == null)
            throw new DicomException("Could not recognized type of DICOM Object");
        else
            return createInstanceDRNodePath(s, dicomobject, as);
    }

    public DRNode[] createInstanceDRNodePath(String s, DicomObject dicomobject, String as[])
        throws DicomException
    {
        return (new DRNode[] {
            new DRNode(create("PATIENT", dicomobject)), new DRNode(create("STUDY", dicomobject)), new DRNode(create("SERIES", dicomobject)), new DRNode(createInstanceDR(s, dicomobject, as))
        });
    }

    public static void init(DicomObject dicomobject, String s)
        throws DicomException
    {
        dicomobject.set(48, ((Object) (s)));
        dicomobject.set(46, ((Object) (new Integer(65535))));
        dicomobject.set(45, ((Object) (new Integer(0))));
        dicomobject.set(47, ((Object) (new Integer(0))));
    }

    public static DicomObject create(String s)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        init(dicomobject, s);
        return dicomobject;
    }

    public static void init(DicomObject dicomobject, String s, DicomObject dicomobject1, int ai[], int ai1[])
        throws DicomException
    {
        init(dicomobject, s);
        copyGroups(dicomobject, dicomobject1, ai1);
        copy(dicomobject, dicomobject1, ai);
    }

    public static DicomObject create(String s, DicomObject dicomobject, int ai[], int ai1[])
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, ai, ai1);
        return dicomobject1;
    }

    public static void init(DicomObject dicomobject, String s, DicomObject dicomobject1, Hashtable hashtable, Hashtable hashtable1)
        throws DicomException
    {
        init(dicomobject, s, dicomobject1, (int[])hashtable.get(((Object) (s))), hashtable1 == null ? null : (int[])hashtable1.get(((Object) (s))));
    }

    public static DicomObject create(String s, DicomObject dicomobject, Hashtable hashtable, Hashtable hashtable1)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        init(dicomobject1, s, dicomobject, hashtable, hashtable1);
        return dicomobject1;
    }

    public static DicomObject createPrivateDR(String as[], String s)
        throws DicomException
    {
        DicomObject dicomobject = create("PRIVATE");
        initReferencedFile(dicomobject, as, s);
        return dicomobject;
    }

    public static void initReferencedFile(DicomObject dicomobject, String as[], String s)
        throws DicomException
    {
        dicomobject.deleteItem(50);
        for(int i = 0; i < as.length; i++)
            dicomobject.append(50, ((Object) (as[i])));

        dicomobject.set(52, ((Object) (s)));
    }

    public static void initReferencedSOP(DicomObject dicomobject, String as[], String s, String s1, String s2)
        throws DicomException
    {
        initReferencedFile(dicomobject, as, s);
        dicomobject.set(53, ((Object) (s1)));
        dicomobject.set(54, ((Object) (s2)));
    }

    public static void copy(DicomObject dicomobject, DicomObject dicomobject1, int ai[])
        throws DicomException
    {
        for(int i = 0; i < ai.length; i++)
            copy(dicomobject, dicomobject1, ai[i]);

    }

    public static void copy(DicomObject dicomobject, DicomObject dicomobject1, int i)
        throws DicomException
    {
        int j = i >> 16;
        int k = i & 0xffff;
        int l = dicomobject1.getSize_ge(j, k);
        for(int i1 = 0; i1 < l; i1++)
            try
            {
                dicomobject.append_ge(j, k, dicomobject1.get_ge(j, k, i1));
            }
            catch(DicomException dicomexception)
            {
                dicomobject.append_ge(j, k, ((Object) (dicomobject1.getS_ge(j, k, i1))));
            }

    }

    public static void copyGroups(DicomObject dicomobject, DicomObject dicomobject1, int ai[])
        throws DicomException
    {
        if(ai != null)
        {
            for(int i = 0; i < ai.length; i++)
                if(((GroupList) (dicomobject1)).containsGroup(ai[i]))
                    ((GroupList) (dicomobject)).addGroups(((GroupList) (dicomobject1)).copyGroup(ai[i]));

        }
    }

    private static Properties loadDefProperties()
    {
        try
        {
            Properties properties = new Properties();
            InputStream inputstream = (com.tiani.dicom.media.DRFactory.class).getResourceAsStream("DRFactory.properties");
            properties.load(inputstream);
            inputstream.close();
            return properties;
        }
        catch(Exception exception)
        {
            throw new RuntimeException(((Throwable) (exception)).getMessage());
        }
    }

    public static Hashtable parseProperties(Properties properties, String s)
    {
        Hashtable hashtable = new Hashtable();
        hashtable.put("PATIENT", ((Object) (parseIntProperties(properties, "pat." + s))));
        hashtable.put("STUDY", ((Object) (parseIntProperties(properties, "sty." + s))));
        hashtable.put("SERIES", ((Object) (parseIntProperties(properties, "ser." + s))));
        hashtable.put("IMAGE", ((Object) (parseIntProperties(properties, "img." + s))));
        hashtable.put("OVERLAY", ((Object) (parseIntProperties(properties, "ovy." + s))));
        hashtable.put("MODALITY LUT", ((Object) (parseIntProperties(properties, "mod." + s))));
        hashtable.put("VOI LUT", ((Object) (parseIntProperties(properties, "voi." + s))));
        hashtable.put("CURVE", ((Object) (parseIntProperties(properties, "crv." + s))));
        hashtable.put("TOPIC", ((Object) (parseIntProperties(properties, "tpc." + s))));
        hashtable.put("VISIT", ((Object) (parseIntProperties(properties, "vis." + s))));
        hashtable.put("RESULT", ((Object) (parseIntProperties(properties, "res." + s))));
        hashtable.put("INTERPRETATION", ((Object) (parseIntProperties(properties, "int." + s))));
        hashtable.put("STUDY COMPONENT", ((Object) (parseIntProperties(properties, "cmp." + s))));
        hashtable.put("STORED PRINT", ((Object) (parseIntProperties(properties, "prt." + s))));
        hashtable.put("RT DOSE", ((Object) (parseIntProperties(properties, "rtd." + s))));
        hashtable.put("RT STRUCTURE SET", ((Object) (parseIntProperties(properties, "rts." + s))));
        hashtable.put("RT PLAN", ((Object) (parseIntProperties(properties, "rtp." + s))));
        hashtable.put("RT TREAT RECORD", ((Object) (parseIntProperties(properties, "rtt." + s))));
        hashtable.put("PRESENTATION", ((Object) (parseIntProperties(properties, "prs." + s))));
        hashtable.put("SR DOCUMENT", ((Object) (parseIntProperties(properties, "srd." + s))));
        hashtable.put("KEY OBJECT DOC", ((Object) (parseIntProperties(properties, "kod." + s))));
        hashtable.put("RAW DATA", ((Object) (parseIntProperties(properties, "raw." + s))));
        return hashtable;
    }

    private static int[] parseIntProperties(Properties properties, String s)
    {
        Vector vector = new Vector();
        String s1;
        for(int i = 1; (s1 = properties.getProperty(s + i)) != null; i++)
            vector.addElement(((Object) (Integer.valueOf(s1, 16))));

        int ai[] = new int[vector.size()];
        for(int j = 0; j < ai.length; j++)
            ai[j] = ((Integer)vector.elementAt(j)).intValue();

        return ai;
    }

}
