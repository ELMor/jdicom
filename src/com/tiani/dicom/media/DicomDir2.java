// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomDir2.java

package com.tiani.dicom.media;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.util.IDicomObjectFilter;
import com.tiani.dicom.util.UIDUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

// Referenced classes of package com.tiani.dicom.media:
//            FileMetaInformation, DRNode, DREntity

public class DicomDir2 extends DicomObject
{

    public static final String TIANI_DICOMDIR_UID = "1.2.40.0.13.0.1.3.10";
    public static final String TIANI_RECORD_UID = "1.2.40.0.13.0.1.3.10.1";
    static final Long LONG0 = new Long(0L);
    private int readLock;
    private int writeLock;
    private DREntity root;
    private int size;
    private boolean itemOffset;

    public DicomDir2()
    {
        readLock = 0;
        writeLock = 0;
        root = null;
        size = 0;
        itemOffset = false;
    }

    public DicomDir2(String s, String s1)
        throws DicomException, IllegalValueException
    {
        this(s, s1, "1.2.840.10008.1.3.10", 8194);
    }

    public DicomDir2(String s, String s1, String s2, int i)
        throws DicomException, IllegalValueException
    {
        readLock = 0;
        writeLock = 0;
        root = null;
        size = 0;
        itemOffset = false;
        ((DicomObject)this).set(38, ((Object) (s)));
        ((DicomObject)this).setFileMetaInformation(((DicomObject) (new FileMetaInformation(s2, s1, UID.getUIDEntry(i).getValue()))));
        reset();
    }

    public void reset()
        throws DicomException
    {
        ((DicomObject)this).set(44, ((Object) (null)));
        ((DicomObject)this).set(41, ((Object) (LONG0)));
        ((DicomObject)this).set(42, ((Object) (LONG0)));
        ((DicomObject)this).set(43, ((Object) (new Integer(0))));
        root = null;
        size = 0;
    }

    public DicomDir2(String s)
        throws DicomException, IllegalValueException
    {
        this(s, UIDUtils.createUID());
    }

    public final String getFileSetID()
    {
        return (String)((DicomObject)this).get(38);
    }

    public final String getFileSetUID()
    {
        DicomObject dicomobject = ((DicomObject)this).getFileMetaInformation();
        return dicomobject == null ? null : (String)dicomobject.get(30);
    }

    public final String getFileSetTransferSyntaxUID()
    {
        DicomObject dicomobject = ((DicomObject)this).getFileMetaInformation();
        return dicomobject == null ? null : (String)dicomobject.get(31);
    }

    private int getTransferSyntax()
    {
        DicomObject dicomobject = ((DicomObject)this).getFileMetaInformation();
        String s = (String)dicomobject.get(31);
        try
        {
            return UID.getUIDEntry(s).getConstant();
        }
        catch(UnknownUIDException unknownuidexception)
        {
            throw new RuntimeException(((Throwable) (unknownuidexception)).getMessage());
        }
    }

    public void write(OutputStream outputstream)
        throws DicomException, IOException
    {
        int i = getTransferSyntax();
        if(itemOffset)
        {
            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
            super.write(((OutputStream) (bytearrayoutputstream)), true, i, false, false);
            ((GroupList)this).clear();
            ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(bytearrayoutputstream.toByteArray());
            super.read(((java.io.InputStream) (bytearrayinputstream)), false);
            init();
        }
        super.write(outputstream, true, i, false, false);
    }

    public void write(OutputStream outputstream, boolean flag, int i, boolean flag1, boolean flag2)
        throws DicomException
    {
        throw new DicomException("Invalid use of DicomObject.write instead DicomDir.write");
    }

    private Long resolveItemOffset(DicomObject dicomobject, int i)
        throws DicomException
    {
        Long long1 = (Long)dicomobject.get(i);
        if(long1.longValue() < 0L)
        {
            long1 = new Long(((DicomObject)this).getOffset(44, -long1.intValue()));
            dicomobject.set(i, ((Object) (long1)));
        }
        return long1;
    }

    public void init()
        throws DicomException
    {
        size = ((DicomObject)this).getSize(44);
        if(size <= 0)
        {
            size = 0;
            return;
        }
        DRNode drnode = new DRNode((DicomObject)((DicomObject)this).get(44, 0));
        HashMap hashmap = new HashMap((size * 4) / 3 | 1);
        for(int i = 1; i < size; i++)
            hashmap.put(((Object) (new Long(((DicomObject)this).getOffset(44, i)))), ((Object) (new DRNode((DicomObject)((DicomObject)this).get(44, i)))));

        resolve(drnode, hashmap);
        if(!hashmap.isEmpty())
        {
            throw new DicomException("unreferenced DRs detected");
        } else
        {
            root = new DREntity(drnode);
            resolveItemOffset(((DicomObject) (this)), 41);
            resolveItemOffset(((DicomObject) (this)), 42);
            itemOffset = false;
            return;
        }
    }

    public DREntity getRootEntity()
    {
        return root;
    }

    private void resolve(DRNode drnode, HashMap hashmap)
        throws DicomException
    {
        DicomObject dicomobject = drnode.getDataset();
        Long long1 = resolveItemOffset(dicomobject, 47);
        if(long1.longValue() != 0L)
        {
            DRNode drnode1 = (DRNode)hashmap.remove(((Object) (long1)));
            if(drnode1 == null)
                throw new DicomException("cannot resolve reference to lower level DR (offset: " + Long.toHexString(long1.longValue()) + "H )");
            resolve(drnode1, hashmap);
            drnode.setLowerEntity(new DREntity(drnode1));
        }
        Long long2 = resolveItemOffset(dicomobject, 45);
        if(long2.longValue() != 0L)
        {
            DRNode drnode2 = (DRNode)hashmap.remove(((Object) (long2)));
            if(drnode2 == null)
                throw new DicomException("cannot resolve reference to next DR (offset: " + Long.toHexString(long2.longValue()) + "H )");
            drnode.setNext(drnode2);
            resolve(drnode2, hashmap);
        }
    }

    private Long appendDR(DicomObject dicomobject)
        throws DicomException
    {
        ((DicomObject)this).set(44, ((Object) (dicomobject)), size);
        Long long1 = new Long(size <= 0 ? ((DicomObject)this).calculateOffset(44, size, getTransferSyntax(), false, false) : -size);
        size++;
        itemOffset = true;
        return long1;
    }

    public DRNode addRootNode(DRNode drnode)
        throws DicomException
    {
        Long long1 = appendDR(drnode.getDataset());
        if(root == null)
        {
            root = new DREntity(drnode);
            ((DicomObject)this).set(41, ((Object) (long1)));
        } else
        {
            root.add(drnode, long1);
        }
        ((DicomObject)this).set(42, ((Object) (long1)));
        return drnode;
    }

    public DRNode addRootNodeIfNew(DRNode drnode)
        throws DicomException
    {
        DRNode drnode1;
        if(root != null && (drnode1 = root.find(drnode)) != null)
        {
            return drnode1;
        } else
        {
            addRootNode(drnode);
            return drnode;
        }
    }

    public DRNode findRootNode(String s, String s1)
    {
        return root == null ? null : root.find(s, s1);
    }

    public void addLowerNode(DRNode drnode, DRNode drnode1)
        throws DicomException
    {
        Long long1 = appendDR(drnode.getDataset());
        drnode1.addLower(drnode, long1);
    }

    public DRNode addLowerNodeIfNew(DRNode drnode, DRNode drnode1)
        throws DicomException
    {
        DRNode drnode2;
        if(drnode1.getLowerEntity() != null && (drnode2 = drnode1.getLowerEntity().find(drnode)) != null)
        {
            return drnode2;
        } else
        {
            addLowerNode(drnode, drnode1);
            return drnode;
        }
    }

    public DRNode findLowerNode(String s, String s1, DRNode drnode)
    {
        return drnode.getLowerEntity() == null ? null : drnode.getLowerEntity().find(s, s1);
    }

    public int addNodePath(DRNode adrnode[])
        throws DicomException
    {
        int i = 0;
        DRNode drnode = addRootNodeIfNew(adrnode[0]);
        if(drnode != adrnode[0])
            adrnode[0] = drnode;
        else
            i++;
        for(int j = 1; j < adrnode.length; j++)
        {
            DRNode drnode1 = addLowerNodeIfNew(adrnode[j], adrnode[j - 1]);
            if(drnode1 != adrnode[j])
                adrnode[j] = drnode1;
            else
                i++;
        }

        return i;
    }

    private DRNode[] cloneNodePath(DRNode adrnode[])
    {
        DRNode adrnode1[] = new DRNode[adrnode.length];
        System.arraycopy(((Object) (adrnode)), 0, ((Object) (adrnode1)), 0, adrnode.length);
        return adrnode1;
    }

    private void fillNodePathsList(DREntity drentity, int i, DRNode adrnode[], String as[], IDicomObjectFilter aidicomobjectfilter[], ArrayList arraylist)
        throws DicomException
    {
        int j = i + 1;
        for(Iterator iterator = drentity.iterator(as[i], aidicomobjectfilter == null ? null : aidicomobjectfilter[i]); iterator.hasNext();)
        {
            DRNode drnode;
            adrnode[i] = drnode = (DRNode)iterator.next();
            DREntity drentity1;
            if(j == adrnode.length)
                arraylist.add(((Object) (cloneNodePath(adrnode))));
            else
            if((drentity1 = drnode.getLowerEntity()) != null)
                fillNodePathsList(drentity1, j, adrnode, as, aidicomobjectfilter, arraylist);
        }

    }

    public ArrayList listNodePaths(String as[], IDicomObjectFilter aidicomobjectfilter[])
        throws DicomException
    {
        if(as == null)
            throw new NullPointerException();
        if(as.length == 0)
            throw new IllegalArgumentException();
        if(aidicomobjectfilter != null && aidicomobjectfilter.length != as.length)
            throw new IllegalArgumentException();
        ArrayList arraylist = new ArrayList();
        if(root == null)
        {
            return arraylist;
        } else
        {
            DRNode adrnode[] = new DRNode[as.length];
            fillNodePathsList(root, 0, adrnode, as, aidicomobjectfilter, arraylist);
            return arraylist;
        }
    }

    public void compact()
        throws DicomException
    {
        Iterator iterator = root.iterator();
        reset();
        while(iterator.hasNext()) 
        {
            DRNode drnode = (DRNode)iterator.next();
            DicomObject dicomobject = drnode.getDataset();
            dicomobject.set(45, ((Object) (LONG0)));
            dicomobject.set(47, ((Object) (LONG0)));
            DRNode drnode1 = new DRNode(dicomobject);
            addRootNode(drnode1);
            DREntity drentity;
            if((drentity = drnode.getLowerEntity()) != null)
                compactLowerEntity(drentity, drnode1);
        }
    }

    private void compactLowerEntity(DREntity drentity, DRNode drnode)
        throws DicomException
    {
        for(Iterator iterator = drentity.iterator(); iterator.hasNext();)
        {
            DRNode drnode1 = (DRNode)iterator.next();
            DicomObject dicomobject = drnode1.getDataset();
            dicomobject.set(45, ((Object) (LONG0)));
            dicomobject.set(47, ((Object) (LONG0)));
            DRNode drnode2 = new DRNode(dicomobject);
            addLowerNode(drnode2, drnode);
            DREntity drentity1;
            if((drentity1 = drnode1.getLowerEntity()) != null)
                compactLowerEntity(drentity1, drnode2);
        }

    }

    public synchronized int addReadLock()
    {
        while(writeLock > 0) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException interruptedexception) { }
        return ++readLock;
    }

    public synchronized int addWriteLock()
    {
        while(writeLock > 0 || readLock > 0) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException interruptedexception) { }
        return ++writeLock;
    }

    public synchronized int releaseReadLock()
    {
        readLock--;
        ((Object)this).notifyAll();
        return readLock;
    }

    public synchronized int releaseWriteLock()
    {
        writeLock--;
        ((Object)this).notifyAll();
        return writeLock;
    }

}
