// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DREntity.java

package com.tiani.dicom.media;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.tiani.dicom.util.IDicomObjectFilter;
import java.util.Iterator;

// Referenced classes of package com.tiani.dicom.media:
//            DRNode

public class DREntity
{
    class DRIterator
        implements Iterator
    {

        private DRNode next;
        private DRNode remove;
        private final String type;
        private final IDicomObjectFilter filter;

        public Object next()
        {
            remove = next;
            next = next.getNext();
            if(next != null)
                try
                {
                    next = next.find(type, filter);
                }
                catch(DicomException dicomexception)
                {
                    ((Throwable) (dicomexception)).printStackTrace(Debug.out);
                    next = null;
                }
            return ((Object) (remove));
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public void remove()
        {
            if(remove == null)
            {
                throw new IllegalStateException();
            } else
            {
                remove.delete();
                remove = null;
                return;
            }
        }

        DRIterator(String s, IDicomObjectFilter idicomobjectfilter)
            throws DicomException
        {
            type = s;
            filter = idicomobjectfilter;
            next = first.find(type, filter);
            remove = null;
        }
    }


    private final DRNode first;
    private DRNode last;

    public DREntity(DRNode drnode)
    {
        first = drnode;
        last = drnode.getLast();
    }

    public final DRNode getFirst()
    {
        return first;
    }

    public void add(DRNode drnode, Long long1)
    {
        last.setNext(drnode, long1);
        last = drnode.getLast();
    }

    public DRNode find(DRNode drnode)
    {
        return first.find(drnode);
    }

    public DRNode find(String s, String s1)
    {
        return first.find(s, s1);
    }

    public int getCount()
    {
        int i = 0;
        for(DRNode drnode = first; drnode != null; drnode = drnode.getNext())
            i++;

        return i;
    }

    public int getCount(String s)
    {
        try
        {
            return getCount(s, ((IDicomObjectFilter) (null)));
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public int getCount(String s, IDicomObjectFilter idicomobjectfilter)
        throws DicomException
    {
        int i = 0;
        for(DRNode drnode = first; drnode != null && (drnode = drnode.find(s, idicomobjectfilter)) != null; drnode = drnode.getNext())
            i++;

        return i;
    }

    public Iterator iterator()
    {
        try
        {
            return ((Iterator) (new DRIterator(((String) (null)), ((IDicomObjectFilter) (null)))));
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public Iterator iterator(String s)
    {
        try
        {
            return ((Iterator) (new DRIterator(s, ((IDicomObjectFilter) (null)))));
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public Iterator iterator(String s, IDicomObjectFilter idicomobjectfilter)
        throws DicomException
    {
        return ((Iterator) (new DRIterator(s, idicomobjectfilter)));
    }

}
