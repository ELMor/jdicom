// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IPrintManagement.java

package com.tiani.dicom.print;

import com.archimed.dicom.DicomObject;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.IDimseListener;

public interface IPrintManagement
{

    public abstract String getFilmSessionUID();

    public abstract String getFilmBoxUID();

    public abstract int countImageBoxes();

    public abstract int countAnnotationBoxes();

    public abstract boolean isEnabled(int i);

    public abstract boolean isFilmSession();

    public abstract boolean isGrayscaleFilmSession();

    public abstract boolean isColorFilmSession();

    public abstract boolean isFilmBox();

    public abstract DicomMessage getGrayscalePrinterStatus()
        throws Exception;

    public abstract DicomMessage getColorPrinterStatus()
        throws Exception;

    public abstract DicomMessage getPrinterConfiguration()
        throws Exception;

    public abstract void addPrinterStatusListener(IDimseListener idimselistener);

    public abstract void removePrinterStatusListener(IDimseListener idimselistener);

    public abstract void addPrinterJobListener(IDimseListener idimselistener);

    public abstract void removePrinterJobListener(IDimseListener idimselistener);

    public abstract DicomMessage getPrintJobStatus(String s)
        throws Exception;

    public abstract DicomMessage createGrayscaleFilmSession(DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage createColorFilmSession(DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage setFilmSession(DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage printFilmSession()
        throws Exception;

    public abstract DicomMessage deleteFilmSession()
        throws Exception;

    public abstract DicomMessage createFilmBox(DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage setFilmBox(DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage printFilmBox()
        throws Exception;

    public abstract DicomMessage deleteFilmBox()
        throws Exception;

    public abstract DicomMessage setGrayscaleImageBox(int i, DicomObject dicomobject, DicomObject dicomobject1)
        throws Exception;

    public abstract DicomMessage setColorImageBox(int i, DicomObject dicomobject, DicomObject dicomobject1)
        throws Exception;

    public abstract DicomMessage setAnnotationBox(int i, String s)
        throws Exception;

    public abstract DicomMessage createPresentationLUT(String s, DicomObject dicomobject)
        throws Exception;

    public abstract DicomMessage createPresentationLUT(String s, int i, int j, int ai[], String s1)
        throws Exception;

    public abstract DicomMessage createShapePresentationLUT(String s, String s1)
        throws Exception;

    public abstract DicomMessage deletePresentationLUT(String s)
        throws Exception;

    public abstract DicomMessage createOverlayBox(String s, DicomObject dicomobject, DicomObject dicomobject1)
        throws Exception;

    public abstract DicomMessage setOverlayBox(String s, DicomObject dicomobject, DicomObject dicomobject1)
        throws Exception;

    public abstract DicomMessage deleteOverlayBox(String s)
        throws Exception;
}
