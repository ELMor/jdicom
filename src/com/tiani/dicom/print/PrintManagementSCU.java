// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintManagementSCU.java

package com.tiani.dicom.print;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.tiani.dicom.framework.DefNEventReportSCU;
import com.tiani.dicom.framework.DicomMessage;
import com.tiani.dicom.framework.DimseExchange;
import com.tiani.dicom.framework.DimseRqManager;
import com.tiani.dicom.framework.IAssociationListener;
import com.tiani.dicom.framework.IDimseListener;
import com.tiani.dicom.framework.Requestor;
import com.tiani.dicom.framework.Status;
import com.tiani.dicom.framework.StatusEntry;
import com.tiani.dicom.framework.VerboseAssociation;
import com.tiani.dicom.util.UIDUtils;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.print:
//            IPrintManagement, PrintManagementUtils

public class PrintManagementSCU
    implements IPrintManagement
{
    private static final class MyNEventReportSCU extends DefNEventReportSCU
    {

        final Vector _listeners;

        protected int eventReport(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
            throws DicomException
        {
            synchronized(_listeners)
            {
                for(Enumeration enumeration = _listeners.elements(); enumeration.hasMoreElements(); ((IDimseListener)enumeration.nextElement()).notify(dicommessage));
            }
            return 0;
        }

        MyNEventReportSCU(Vector vector)
        {
            _listeners = vector;
        }
    }


    public static final int SOP_CLASSES[] = {
        4097, 12292, 12294, 4160, 4114, 4145, 4113, 4175
    };
    static final int ONLY_DEF_TS[] = {
        8193
    };
    private final IAssociationListener _assocListener = new IAssociationListener() {

        public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
        {
        }

        public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
        {
        }

        public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
        {
        }

        public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
        {
        }

        public void releaseRequestReceived(VerboseAssociation verboseassociation)
        {
        }

        public void releaseResponseReceived(VerboseAssociation verboseassociation)
        {
        }

        public void releaseRequestSent(VerboseAssociation verboseassociation)
        {
        }

        public void releaseResponseSent(VerboseAssociation verboseassociation)
        {
        }

        public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
        {
        }

        public void abortSent(VerboseAssociation verboseassociation, int i, int j)
        {
        }

        public void socketClosed(VerboseAssociation verboseassociation)
        {
            connection = null;
            _sessionUID = null;
            _curFilmBoxUID = null;
            _curFilmBoxAttribs = null;
        }

    };
    private boolean grouplens;
    private Requestor _requestor;
    private DimseExchange connection;
    private int _metaSOPid;
    private byte _metaPCid;
    private String _sessionUID;
    private DicomObject _refSessionSqItem;
    private String _curFilmBoxUID;
    private DicomObject _curFilmBoxAttribs;
    private Vector _statusListener;
    private Vector _jobListener;
    private int maxPduSize;
    private int artim;

    public PrintManagementSCU()
    {
        grouplens = true;
        _requestor = null;
        connection = null;
        _metaSOPid = -1;
        _metaPCid = 0;
        _sessionUID = null;
        _refSessionSqItem = new DicomObject();
        _curFilmBoxUID = null;
        _curFilmBoxAttribs = null;
        _statusListener = new Vector();
        _jobListener = new Vector();
        maxPduSize = 32768;
        artim = 1000;
    }

    public void setGrouplens(boolean flag)
    {
        grouplens = flag;
    }

    public void setMaxPduSize(int i)
    {
        maxPduSize = i;
    }

    public void setARTIM(int i)
    {
        artim = i;
        if(connection != null)
            connection.setARTIM(i);
    }

    public Response connect(String s, int i, String s1, String s2, int ai[])
        throws UnknownHostException, IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        if(connection != null)
            throw new IllegalStateException("existing connection");
        Socket socket = new Socket(s, i);
        Request request = new Request();
        request.setCalledTitle(s1);
        request.setCallingTitle(s2);
        request.setMaxPduSize(maxPduSize);
        for(int j = 0; j < ai.length; j++)
            request.addPresentationContext(ai[j], ONLY_DEF_TS);

        _requestor = new Requestor(socket, request);
        _requestor.addAssociationListener(_assocListener);
        VerboseAssociation verboseassociation = _requestor.openAssoc();
        if(verboseassociation != null)
        {
            ((Association) (verboseassociation)).setGrouplens(grouplens);
            DimseRqManager dimserqmanager = new DimseRqManager();
            dimserqmanager.regNEventReportScu(UID.toString(4115), ((com.tiani.dicom.framework.IDimseRqListener) (new MyNEventReportSCU(_statusListener))));
            dimserqmanager.regNEventReportScu(UID.toString(4113), ((com.tiani.dicom.framework.IDimseRqListener) (new MyNEventReportSCU(_jobListener))));
            connection = new DimseExchange(verboseassociation, dimserqmanager, false, false, 1);
            connection.setARTIM(artim);
            (new Thread(((Runnable) (connection)))).start();
        }
        return _requestor.response();
    }

    public boolean isConnected()
    {
        return connection != null;
    }

    public boolean isEnabled(int i)
    {
        try
        {
            return isConnected() && connection.listAcceptedPresentationContexts(i).length > 0;
        }
        catch(IllegalValueException illegalvalueexception)
        {
            return false;
        }
    }

    public String getFilmSessionUID()
    {
        return _sessionUID;
    }

    public String getFilmBoxUID()
    {
        return _curFilmBoxUID;
    }

    public boolean isFilmSession()
    {
        return _sessionUID != null;
    }

    public boolean isGrayscaleFilmSession()
    {
        return isFilmSession(12292);
    }

    public boolean isColorFilmSession()
    {
        return isFilmSession(12294);
    }

    public boolean isFilmSession(int i)
    {
        return isFilmSession() && _metaSOPid == i;
    }

    public boolean isFilmBox()
    {
        return _curFilmBoxUID != null;
    }

    public void addPrinterStatusListener(IDimseListener idimselistener)
    {
        _statusListener.addElement(((Object) (idimselistener)));
    }

    public void removePrinterStatusListener(IDimseListener idimselistener)
    {
        _statusListener.removeElement(((Object) (idimselistener)));
    }

    public void addPrinterJobListener(IDimseListener idimselistener)
    {
        _jobListener.addElement(((Object) (idimselistener)));
    }

    public void removePrinterJobListener(IDimseListener idimselistener)
    {
        _jobListener.removeElement(((Object) (idimselistener)));
    }

    public DicomMessage echo()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return connection.cecho();
    }

    public DicomMessage getColorPrinterStatus()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return getPrinterStatus(12294);
    }

    public DicomMessage getGrayscalePrinterStatus()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return getPrinterStatus(12292);
    }

    public DicomMessage getPrinterStatus(int i)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        byte byte0 = connection.getPresentationContext(i);
        return connection.nget(byte0, UID.toString(4115), UID.toString(16387), ((int []) (null)));
    }

    public DicomMessage getPrintJobStatus(String s)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        byte byte0 = connection.getPresentationContext(4113);
        return connection.nget(byte0, UID.toString(4113), s, ((int []) (null)));
    }

    public DicomMessage getPrinterConfiguration()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
        {
            throw new IllegalStateException("no connection");
        } else
        {
            byte byte0 = connection.getPresentationContext(4175);
            return connection.nget(byte0, UID.toString(4175), UID.toString(16389), ((int []) (null)));
        }
    }

    public DicomMessage createGrayscaleFilmSession(DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return createFilmSession(12292, dicomobject);
    }

    public DicomMessage createColorFilmSession(DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return createFilmSession(12294, dicomobject);
    }

    public DicomMessage createFilmSession(int i, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID != null)
            throw new IllegalStateException("Existing FilmSession");
        _metaSOPid = i;
        _metaPCid = connection.getPresentationContext(_metaSOPid);
        String s = UIDUtils.createUID();
        DicomMessage dicommessage = connection.ncreate(_metaPCid, UID.toString(4108), s, dicomobject);
        StatusEntry statusentry = Status.getStatusEntry(4108, ((DicomObject) (dicommessage)).getI(9));
        if(statusentry != null && statusentry.getType() != 5)
        {
            _sessionUID = s;
            _refSessionSqItem.set(115, ((Object) (UID.toString(4108))));
            _refSessionSqItem.set(116, ((Object) (_sessionUID)));
        }
        return dicommessage;
    }

    public DicomMessage setFilmSession(DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
            throw new IllegalStateException("Missing FilmSession");
        else
            return connection.nset(_metaPCid, UID.toString(4108), _sessionUID, dicomobject);
    }

    public DicomMessage printFilmSession()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
            throw new IllegalStateException("Missing FilmSession");
        else
            return connection.naction(_metaPCid, UID.toString(4108), _sessionUID, 1, ((DicomObject) (null)));
    }

    public DicomMessage deleteFilmSession()
        throws IOException, IllegalValueException, InterruptedException, DicomException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
        {
            throw new IllegalStateException("Missing FilmSession");
        } else
        {
            DicomMessage dicommessage = connection.ndelete(_metaPCid, UID.toString(4108), _sessionUID);
            _sessionUID = null;
            _curFilmBoxUID = null;
            _curFilmBoxAttribs = null;
            return dicommessage;
        }
    }

    public DicomMessage createFilmBox(DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
            throw new IllegalStateException("Missing FilmSession");
        String s = UIDUtils.createUID();
        dicomobject.set(725, ((Object) (_refSessionSqItem)));
        DicomMessage dicommessage = connection.ncreate(_metaPCid, UID.toString(4109), s, dicomobject);
        if(Status.getStatusEntry(4109, ((DicomObject) (dicommessage)).getI(9)).getType() != 5)
        {
            _curFilmBoxUID = s;
            _curFilmBoxAttribs = dicommessage.getDataset();
        }
        return dicommessage;
    }

    public DicomMessage setFilmBox(DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_curFilmBoxUID == null)
            throw new IllegalStateException("Missing FilmBox");
        else
            return connection.nset(_metaPCid, UID.toString(4109), _curFilmBoxUID, dicomobject);
    }

    public DicomMessage printFilmBox()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_curFilmBoxUID == null)
            throw new IllegalStateException("Missing FilmBox");
        else
            return connection.naction(_metaPCid, UID.toString(4109), _curFilmBoxUID, 1, ((DicomObject) (null)));
    }

    public DicomMessage deleteFilmBox()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_curFilmBoxUID == null)
        {
            throw new IllegalStateException("Missing FilmBox");
        } else
        {
            DicomMessage dicommessage = connection.ndelete(_metaPCid, UID.toString(4109), _curFilmBoxUID);
            _curFilmBoxUID = null;
            _curFilmBoxAttribs = null;
            return dicommessage;
        }
    }

    public DicomMessage createPresentationLUT(String s, int i, int j, int ai[], String s1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(506, ((Object) (new Integer(ai.length))), 0);
        dicomobject.set(506, ((Object) (new Integer(i))), 1);
        dicomobject.set(506, ((Object) (new Integer(j))), 2);
        if(s1 != null)
            dicomobject.set(507, ((Object) (s1)));
        for(int k = 0; k < ai.length; k++)
            dicomobject.set(509, ((Object) (new Integer(ai[k]))), k);

        DicomObject dicomobject1 = new DicomObject();
        dicomobject1.set(1239, ((Object) (dicomobject)));
        return createPresentationLUT(s, dicomobject1);
    }

    public DicomMessage createShapePresentationLUT(String s, String s1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.set(1240, ((Object) (s1)));
        return createPresentationLUT(s, dicomobject);
    }

    public DicomMessage createPresentationLUT(String s, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
        {
            throw new IllegalStateException("no connection");
        } else
        {
            byte byte0 = connection.getPresentationContext(4145);
            return connection.ncreate(byte0, UID.toString(4145), s, PrintManagementUtils.getPresentationLUTModule(dicomobject));
        }
    }

    public DicomMessage deletePresentationLUT(String s)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
        {
            throw new IllegalStateException("Missing FilmSession");
        } else
        {
            byte byte0 = connection.getPresentationContext(4145);
            return connection.ndelete(byte0, UID.toString(4145), s);
        }
    }

    public DicomMessage createOverlayBox(String s, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
        {
            throw new IllegalStateException("Missing FilmSession");
        } else
        {
            dicomobject1.set(1366, ((Object) (dicomobject)));
            byte byte0 = connection.getPresentationContext(4160);
            return connection.ncreate(byte0, UID.toString(4160), s, dicomobject1);
        }
    }

    public DicomMessage setOverlayBox(String s, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
            throw new IllegalStateException("Missing FilmSession");
        if(dicomobject != null)
            dicomobject1.set(1366, ((Object) (dicomobject)));
        byte byte0 = connection.getPresentationContext(4160);
        return connection.nset(byte0, UID.toString(4160), s, dicomobject1);
    }

    public DicomMessage deleteOverlayBox(String s)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(_sessionUID == null)
        {
            throw new IllegalStateException("Missing FilmSession");
        } else
        {
            byte byte0 = connection.getPresentationContext(4160);
            return connection.ndelete(byte0, UID.toString(4160), s);
        }
    }

    public int countImageBoxes()
    {
        if(_curFilmBoxAttribs == null)
            throw new IllegalStateException("Missing FilmBox");
        else
            return _curFilmBoxAttribs.getSize(726);
    }

    public int countAnnotationBoxes()
    {
        if(_curFilmBoxAttribs == null)
            throw new IllegalStateException("Missing FilmBox");
        else
            return Math.max(0, _curFilmBoxAttribs.getSize(727));
    }

    private DicomMessage setImageBox(int i, DicomObject dicomobject, DicomObject dicomobject1, int j, int k)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(i <= 0 || i > countImageBoxes())
        {
            throw new IllegalArgumentException("Invalid ImagePosition - " + i);
        } else
        {
            DicomObject dicomobject2 = (DicomObject)_curFilmBoxAttribs.get(726, i - 1);
            dicomobject1.set(729, ((Object) (new Integer(i))));
            dicomobject1.set(j, ((Object) (PrintManagementUtils.getPixelModule(dicomobject))));
            return connection.nset(_metaPCid, UID.getUIDEntry(k).getValue(), dicomobject2.getS(116), dicomobject1);
        }
    }

    public DicomMessage setGrayscaleImageBox(int i, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return setImageBox(i, dicomobject, dicomobject1, 732, 4110);
    }

    public DicomMessage setColorImageBox(int i, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        return setImageBox(i, dicomobject, dicomobject1, 733, 4111);
    }

    public DicomMessage setAnnotationBox(int i, String s)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        if(connection == null)
            throw new IllegalStateException("no connection");
        if(i <= 0 || i > countAnnotationBoxes())
        {
            throw new IllegalArgumentException("Invalid AnnotationPosition - " + i);
        } else
        {
            DicomObject dicomobject = (DicomObject)_curFilmBoxAttribs.get(727, i - 1);
            DicomObject dicomobject1 = new DicomObject();
            dicomobject1.set(737, ((Object) (new Integer(i))));
            dicomobject1.set(738, ((Object) (s)));
            byte byte0 = connection.getPresentationContext(4114);
            return connection.nset(byte0, dicomobject.getS(115), dicomobject.getS(116), dicomobject1);
        }
    }

    public void release()
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        if(_sessionUID != null)
            deleteFilmSession();
        if(connection != null)
        {
            connection.releaseAssoc();
            connection = null;
        }
    }

    protected void finalize()
    {
        if(connection != null)
            try
            {
                release();
            }
            catch(Exception exception) { }
    }





}
