// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PreformatMain.java

package com.tiani.dicom.print;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.tiani.dicom.legacy.TianiInputStream;
import com.tiani.dicom.media.FileMetaInformation;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

// Referenced classes of package com.tiani.dicom.print:
//            PrintManagementUtils

public class PreformatMain
{

    private static final String USAGE = "Usage: PreformatMain <src> <dest> <bitDepth> [<pLUT>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n<bitDepth>   maximal number of stored bits of resulting pixel data\n<pLUT>       file path of Presentation LUT to apply on image";

    public PreformatMain()
    {
    }

    public static void main(String args[])
    {
        if(args.length < 3)
        {
            System.out.println("Usage: PreformatMain <src> <dest> <bitDepth> [<pLUT>]\nwith:\n<src>        file path of source dicom object\n<dest>       file path of result dicom object\n<bitDepth>   maximal number of stored bits of resulting pixel data\n<pLUT>       file path of Presentation LUT to apply on image");
            return;
        }
        try
        {
            Debug.DEBUG = 3;
            DicomObject dicomobject = load(args[0]);
            int i = Integer.parseInt(args[2]);
            DicomObject dicomobject1 = args.length <= 3 ? null : load(args[3]);
            DicomObject dicomobject2 = PrintManagementUtils.preformatGrayscale(dicomobject, dicomobject1, i, 0);
            DicomObject dicomobject3 = PrintManagementUtils.getPixelModule(((GroupList) (dicomobject2)).removeGroup(40));
            ((GroupList) (dicomobject2)).addGroups(dicomobject3);
            save(args[1], dicomobject2);
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace(System.out);
        }
    }

    private static DicomObject load(String s)
        throws IOException, DicomException
    {
        DicomObject dicomobject = new DicomObject();
        FileInputStream fileinputstream = new FileInputStream(s);
        TianiInputStream tianiinputstream = new TianiInputStream(((java.io.InputStream) (fileinputstream)));
        try
        {
            tianiinputstream.read(dicomobject);
        }
        finally
        {
            ((FilterInputStream) (tianiinputstream)).close();
        }
        return dicomobject;
    }

    private static void save(String s, DicomObject dicomobject)
        throws IOException, DicomException
    {
        FileOutputStream fileoutputstream = new FileOutputStream(s);
        dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject, "1.2.840.10008.1.2.1"))));
        try
        {
            dicomobject.write(((OutputStream) (fileoutputstream)), true, 8194, false);
        }
        finally
        {
            ((OutputStream) (fileoutputstream)).close();
        }
    }
}
