// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PrintManagementUtils.java

package com.tiani.dicom.print;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.tiani.dicom.util.EnumPMI;
import com.tiani.dicom.util.LUT;
import com.tiani.dicom.util.LUTFactory;
import com.tiani.dicom.util.PixelMatrix;
import java.io.PrintStream;
import java.util.Properties;

public class PrintManagementUtils
{

    private static final int PLUTMODULE_DNAMES[] = {
        1239, 1240
    };
    private static final int PIXELMODULE_DNAMES[] = {
        461, 462, 466, 467, 473, 475, 476, 477, 478, 1184
    };
    public static final int INFLATE_ALWAYS = 0;
    public static final int INFLATE_IF_NONLINEAR = 1;
    public static final int INFLATE_NEVER = 2;

    private PrintManagementUtils()
    {
    }

    private static void set(DicomObject dicomobject, int i, String s)
        throws DicomException
    {
        if(s != null && s.length() > 0)
            dicomobject.set(i, ((Object) (s)));
    }

    public static DicomObject createGammaPresentationLUT(int i, int j, double d)
    {
        try
        {
            DicomObject dicomobject = new DicomObject();
            dicomobject.set(1239, ((Object) (createGammaLUTSequenceItem(i, j, d))));
            return dicomobject;
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace(Debug.out);
            throw new RuntimeException(((Throwable) (dicomexception)).getMessage());
        }
    }

    public static DicomObject createGammaLUTSequenceItem(int i, int j, double d)
    {
        if(i <= 0 || i > 65535)
            throw new IllegalArgumentException("Illegal value for LUT len " + i);
        if(j < 8 || j > 16)
            throw new IllegalArgumentException("Illegal value for LUT bitDepth " + j);
        if(d < 0.10000000000000001D || d > 10D)
            throw new IllegalArgumentException("Illegal value for LUT gamma " + d);
        int k = (1 << j) - 1;
        double d1 = 1.0D / d;
        double d2 = (double)(1 << j) / Math.pow(i - 1, d1);
        try
        {
            DicomObject dicomobject = new DicomObject();
            byte abyte0[] = new byte[i << 1];
            int l = 0;
            for(int j1 = 0; j1 < i; j1++)
            {
                int i1 = Math.min(k, (int)(d2 * Math.pow(j1, d1)));
                abyte0[l++] = (byte)i1;
                abyte0[l++] = (byte)(i1 >> 8);
            }

            dicomobject.set(509, ((Object) (abyte0)));
            dicomobject.set(506, ((Object) (new Integer(i))), 0);
            dicomobject.set(506, ((Object) (new Integer(0))), 1);
            dicomobject.set(506, ((Object) (new Integer(j))), 2);
            dicomobject.set(507, ((Object) ("Gamma [" + d + "] correction LUT")));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Generate Gamma LUT: len=" + i + ", bitDepth=" + j + ", gamma=" + d);
            return dicomobject;
        }
        catch(DicomException dicomexception)
        {
            ((Throwable) (dicomexception)).printStackTrace(Debug.out);
            throw new RuntimeException(((Throwable) (dicomexception)).getMessage());
        }
    }

    public static DicomObject preformatGrayscale(DicomObject dicomobject, DicomObject dicomobject1, int i, int j, boolean flag)
        throws DicomException, IllegalValueException
    {
        if(i < 8 || i > 16)
            throw new IllegalArgumentException("Illegal bitDepth - " + i);
        String s = (String)dicomobject.get(462);
        int k = EnumPMI.getConstant(s);
        if(!EnumPMI.isGrayscale(k))
            throw new IllegalArgumentException("Cannot preformat images with Photometric Interpretation - " + s);
        boolean flag1 = k == 0;
        int l = dicomobject.getI(461);
        if(l != 1)
            throw new DicomException("Illegal Samples Per Pixel - " + l);
        int i1 = dicomobject.getI(475);
        if(i1 != 8 && i1 != 16)
            throw new IllegalArgumentException("Cannot preformat images with " + i1 + " bits allocated");
        int j1 = Math.min(i, i1);
        if(j == 0 || j == 1 && (dicomobject.getSize(505) > 0 || dicomobject.getSize(510) > 0 || dicomobject1 != null && dicomobject1.getSize(1239) > 0))
            j1 = i;
        PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: " + s + "-" + pixelmatrix);
        boolean flag2 = LUTFactory.isXRaySOPClass(dicomobject);
        if(dicomobject1 == null && flag && dicomobject.getSize(510) <= 0 && (dicomobject.getSize(487) <= 0 || dicomobject.getSize(488) <= 0))
        {
            DicomObject dicomobject2 = null;
            Float float1 = null;
            Float float2 = null;
            if(!flag2)
            {
                dicomobject2 = (DicomObject)dicomobject.get(505);
                float1 = (Float)dicomobject.get(490);
                float2 = (Float)dicomobject.get(489);
            }
            int ai[] = dicomobject2 == null ? pixelmatrix.calcMinMax() : LUTFactory.calcMinMax(dicomobject2);
            float f = (float)(ai[1] + ai[0]) / 2.0F;
            float f1 = ai[1] - ai[0];
            if(float1 != null && float2 != null)
            {
                f = f * float1.floatValue() + float2.floatValue();
                f1 *= float1.floatValue();
            }
            dicomobject.set(487, ((Object) (new Float(f))));
            dicomobject.set(488, ((Object) (new Float(f1))));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Generate VOI LUT: c=" + f + ", w=" + f1);
        }
        int k1 = (1 << j1) - 1;
        int l1 = pixelmatrix.getNumberOfSamples();
        byte abyte0[];
        if(i1 > 8)
        {
            if(j1 > 8)
            {
                com.tiani.dicom.util.LUT.Short short1 = LUTFactory.createShortLUT(pixelmatrix.getMinVal(), pixelmatrix.getMaxVal(), k1, flag1, dicomobject, dicomobject, dicomobject1);
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Apply Total LUT:" + short1);
                abyte0 = new byte[2 * l1];
                int i2 = 0;
                if(pixelmatrix instanceof com.tiani.dicom.util.PixelMatrix.UnsignedShort)
                {
                    com.tiani.dicom.util.PixelMatrix.UnsignedShort unsignedshort1 = (com.tiani.dicom.util.PixelMatrix.UnsignedShort)pixelmatrix;
                    for(int k3 = 0; k3 < l1; k3++)
                    {
                        short word0 = short1.lookupShort(unsignedshort1.getSample(k3));
                        abyte0[i2++] = (byte)word0;
                        abyte0[i2++] = (byte)(word0 >> 8);
                    }

                } else
                {
                    com.tiani.dicom.util.PixelMatrix.SignedShort signedshort1 = (com.tiani.dicom.util.PixelMatrix.SignedShort)pixelmatrix;
                    for(int l3 = 0; l3 < l1; l3++)
                    {
                        short word1 = short1.lookupShort(signedshort1.getSample(l3));
                        abyte0[i2++] = (byte)word1;
                        abyte0[i2++] = (byte)(word1 >> 8);
                    }

                }
                dicomobject.set(475, ((Object) (new Integer(16))));
            } else
            {
                com.tiani.dicom.util.LUT.Byte1 byte1 = LUTFactory.createByteLUT(pixelmatrix.getMinVal(), pixelmatrix.getMaxVal(), k1, flag1, flag2 ? null : dicomobject, dicomobject, dicomobject1);
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: Apply Total LUT:" + byte1);
                abyte0 = new byte[l1];
                if(pixelmatrix instanceof com.tiani.dicom.util.PixelMatrix.UnsignedShort)
                {
                    com.tiani.dicom.util.PixelMatrix.UnsignedShort unsignedshort = (com.tiani.dicom.util.PixelMatrix.UnsignedShort)pixelmatrix;
                    for(int j2 = 0; j2 < l1; j2++)
                        abyte0[j2] = byte1.lookupByte(unsignedshort.getSample(j2));

                } else
                {
                    com.tiani.dicom.util.PixelMatrix.SignedShort signedshort = (com.tiani.dicom.util.PixelMatrix.SignedShort)pixelmatrix;
                    for(int k2 = 0; k2 < l1; k2++)
                        abyte0[k2] = byte1.lookupByte(signedshort.getSample(k2));

                }
                dicomobject.set(475, ((Object) (new Integer(8))));
            }
        } else
        if(j1 > 8)
        {
            com.tiani.dicom.util.LUT.Short short2 = LUTFactory.createShortLUT(pixelmatrix.getMinVal(), pixelmatrix.getMaxVal(), k1, flag1, dicomobject, dicomobject, dicomobject1);
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Apply Total LUT:" + short2);
            abyte0 = new byte[2 * l1];
            int l2 = 0;
            if(pixelmatrix instanceof com.tiani.dicom.util.PixelMatrix.UnsignedByte)
            {
                com.tiani.dicom.util.PixelMatrix.UnsignedByte unsignedbyte1 = (com.tiani.dicom.util.PixelMatrix.UnsignedByte)pixelmatrix;
                for(int i4 = 0; i4 < l1; i4++)
                {
                    short word2 = short2.lookupShort(unsignedbyte1.getSample(i4));
                    abyte0[l2++] = (byte)word2;
                    abyte0[l2++] = (byte)(word2 >> 8);
                }

            } else
            {
                com.tiani.dicom.util.PixelMatrix.SignedByte signedbyte1 = (com.tiani.dicom.util.PixelMatrix.SignedByte)pixelmatrix;
                for(int j4 = 0; j4 < l1; j4++)
                {
                    short word3 = short2.lookupShort(signedbyte1.getSample(j4));
                    abyte0[l2++] = (byte)word3;
                    abyte0[l2++] = (byte)(word3 >> 8);
                }

            }
            dicomobject.set(475, ((Object) (new Integer(16))));
        } else
        {
            com.tiani.dicom.util.LUT.Byte1 byte1_1 = LUTFactory.createByteLUT(pixelmatrix.getMinVal(), pixelmatrix.getMaxVal(), k1, flag1, dicomobject, dicomobject, dicomobject1);
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: Apply Total LUT:" + byte1_1);
            abyte0 = new byte[l1];
            if(pixelmatrix instanceof com.tiani.dicom.util.PixelMatrix.UnsignedByte)
            {
                com.tiani.dicom.util.PixelMatrix.UnsignedByte unsignedbyte = (com.tiani.dicom.util.PixelMatrix.UnsignedByte)pixelmatrix;
                for(int i3 = 0; i3 < l1; i3++)
                    abyte0[i3] = byte1_1.lookupByte(unsignedbyte.getSample(i3));

            } else
            {
                com.tiani.dicom.util.PixelMatrix.SignedByte signedbyte = (com.tiani.dicom.util.PixelMatrix.SignedByte)pixelmatrix;
                for(int j3 = 0; j3 < l1; j3++)
                    abyte0[j3] = byte1_1.lookupByte(signedbyte.getSample(j3));

            }
            dicomobject.set(475, ((Object) (new Integer(8))));
        }
        dicomobject.set(462, "MONOCHROME2");
        dicomobject.set(476, ((Object) (new Integer(j1))));
        dicomobject.set(477, ((Object) (new Integer(j1 - 1))));
        dicomobject.set(478, ((Object) (new Integer(0))));
        dicomobject.set(1184, ((Object) (abyte0)));
        return dicomobject;
    }

    public static DicomObject preformatGrayscale(DicomObject dicomobject, DicomObject dicomobject1, int i, int j)
        throws DicomException, IllegalValueException
    {
        return preformatGrayscale(dicomobject, dicomobject1, i, j, true);
    }

    public static boolean setPixelAspectRatio(DicomObject dicomobject, boolean flag)
        throws DicomException
    {
        if(dicomobject.getSize(473) == 2)
            return false;
        Float float1;
        Float float2;
        if(dicomobject.getSize(470) == 2)
        {
            float1 = (Float)dicomobject.get(470, 0);
            float2 = (Float)dicomobject.get(470, 1);
        } else
        if(dicomobject.getSize(308) == 2)
        {
            float1 = (Float)dicomobject.get(308, 0);
            float2 = (Float)dicomobject.get(308, 1);
        } else
        {
            return false;
        }
        int i = (int)(float1.floatValue() * 1000F);
        int j = (int)(float2.floatValue() * 1000F);
        if(i == j)
        {
            if(!flag)
                return false;
            i = j = 1;
        }
        dicomobject.set(473, ((Object) (new Integer(i))), 0);
        dicomobject.set(473, ((Object) (new Integer(j))), 1);
        return true;
    }

    public static boolean setPixelAspectRatio(DicomObject dicomobject)
        throws DicomException
    {
        return setPixelAspectRatio(dicomobject, true);
    }

    public static DicomObject getPixelModule(DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        copyAttributs(dicomobject, dicomobject1, PIXELMODULE_DNAMES);
        if(dicomobject.getI(461) > 1)
            dicomobject1.set(463, dicomobject.get(463));
        return dicomobject1;
    }

    public static DicomObject getPresentationLUTModule(DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        copyAttributs(dicomobject, dicomobject1, PLUTMODULE_DNAMES);
        return dicomobject1;
    }

    static void copyAttributs(DicomObject dicomobject, DicomObject dicomobject1, int ai[])
        throws DicomException
    {
        for(int k = 0; k < ai.length; k++)
        {
            int i;
            int j = dicomobject.getSize(i = ai[k]);
            dicomobject1.deleteItem(i);
            for(int l = 0; l < j; l++)
                dicomobject1.append(i, dicomobject.get(i, l));

        }

    }

    public static String getRequestedImageSize(DicomObject dicomobject, float f)
        throws DicomException
    {
        Float float1 = (Float)dicomobject.get(470, 1);
        if(float1 == null)
            float1 = (Float)dicomobject.get(308, 1);
        if(float1 != null)
            return "" + (float)dicomobject.getI(467) * float1.floatValue() * f;
        else
            return null;
    }

    /**
     * @deprecated Method setFilmSession is deprecated
     */

    public static void setFilmSession(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        setFilmSessionCreateAttribs(dicomobject, properties);
    }

    public static void setFilmSessionCreateAttribs(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        set(dicomobject, 705, properties.getProperty("Session.NumberOfCopies"));
        set(dicomobject, 706, properties.getProperty("Session.PrintPriority"));
        set(dicomobject, 707, properties.getProperty("Session.MediumType"));
        set(dicomobject, 708, properties.getProperty("Session.FilmDestination"));
        set(dicomobject, 709, properties.getProperty("Session.FilmSessionLabel"));
        set(dicomobject, 710, properties.getProperty("Session.MemoryAllocation"));
        set(dicomobject, 756, properties.getProperty("Session.OwnerID"));
    }

    public static void setFilmSessionSetAttribs(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        setFilmSessionCreateAttribs(dicomobject, properties);
    }

    /**
     * @deprecated Method setFilmBox is deprecated
     */

    public static void setFilmBox(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        setFilmBoxCreateAttribs(dicomobject, properties);
    }

    public static void setFilmBoxCreateAttribs(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        set(dicomobject, 713, properties.getProperty("FilmBox.ImageDisplayFormat"));
        set(dicomobject, 715, properties.getProperty("FilmBox.FilmOrientation"));
        set(dicomobject, 716, properties.getProperty("FilmBox.FilmSizeID"));
        set(dicomobject, 1363, properties.getProperty("FilmBox.RequestedResolutionID"));
        set(dicomobject, 714, properties.getProperty("FilmBox.AnnotationDisplayFormatID"));
        setFilmBoxSetAttribs(dicomobject, properties);
    }

    public static void setFilmBoxSetAttribs(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        set(dicomobject, 717, properties.getProperty("FilmBox.MagnificationType"));
        set(dicomobject, 718, properties.getProperty("FilmBox.SmoothingType"));
        set(dicomobject, 719, properties.getProperty("FilmBox.BorderDensity"));
        set(dicomobject, 720, properties.getProperty("FilmBox.EmptyImageDensity"));
        set(dicomobject, 721, properties.getProperty("FilmBox.MinDensity"));
        set(dicomobject, 722, properties.getProperty("FilmBox.MaxDensity"));
        set(dicomobject, 723, properties.getProperty("FilmBox.Trim"));
        set(dicomobject, 724, properties.getProperty("FilmBox.ConfigurationInformation"));
        set(dicomobject, 1236, properties.getProperty("FilmBox.Illumination"));
        set(dicomobject, 1237, properties.getProperty("FilmBox.ReflectedAmbientLight"));
    }

    /**
     * @deprecated Method setImageBox is deprecated
     */

    public static void setImageBox(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        setImageBoxSetAttribs(dicomobject, properties);
    }

    public static void setImageBoxSetAttribs(DicomObject dicomobject, Properties properties)
        throws DicomException
    {
        set(dicomobject, 730, properties.getProperty("ImageBox.Polarity"));
        set(dicomobject, 717, properties.getProperty("ImageBox.MagnificationType"));
        set(dicomobject, 718, properties.getProperty("ImageBox.SmoothingType"));
        set(dicomobject, 721, properties.getProperty("ImageBox.MinDensity"));
        set(dicomobject, 722, properties.getProperty("ImageBox.MaxDensity"));
        set(dicomobject, 724, properties.getProperty("ImageBox.ConfigurationInformation"));
        set(dicomobject, 1362, properties.getProperty("ImageBox.RequestedDecimateCropBehavior"));
        set(dicomobject, 731, properties.getProperty("ImageBox.RequestedImageSize"));
    }

    public static void addReferencedPresentationLUT(DicomObject dicomobject, String s)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject1 = new DicomObject();
        dicomobject1.set(115, ((Object) (UID.toString(4145))));
        dicomobject1.set(116, ((Object) (s)));
        dicomobject.append(1241, ((Object) (dicomobject1)));
    }

    public static void addReferencedImageOverlayBox(DicomObject dicomobject, String s)
        throws DicomException, IllegalValueException
    {
        DicomObject dicomobject1 = new DicomObject();
        dicomobject1.set(62, ((Object) (UID.toString(4160))));
        dicomobject1.set(63, ((Object) (s)));
        dicomobject.append(734, ((Object) (dicomobject1)));
    }

    public static DicomObject rescaleLUTtoFitBitDepth(DicomObject dicomobject, int i)
        throws DicomException
    {
        if(dicomobject.getSize(506) != 3)
            throw new DicomException("Missing or Invalid LUT Descriptor");
        int j = 1 << i;
        int k = dicomobject.getI(506, 0);
        if(j == k)
            return dicomobject;
        com.tiani.dicom.util.LUT.Short short1 = (com.tiani.dicom.util.LUT.Short)LUTFactory.createLUT(dicomobject);
        com.tiani.dicom.util.LUT.Short short2 = (com.tiani.dicom.util.LUT.Short)short1.rescaleLength(j);
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Rescaled LUT to Fit Bit Depth: " + short2);
        dicomobject.set(506, ((Object) (new Integer(j))), 0);
        dicomobject.deleteItem(509);
        byte abyte0[] = new byte[j << 1];
        int l = 0;
        for(int i1 = 0; i1 < j; i1++)
        {
            short word0 = short2.lookupShort(i1);
            abyte0[l++] = (byte)word0;
            abyte0[l++] = (byte)(word0 >> 8);
        }

        dicomobject.set(509, ((Object) (abyte0)));
        return dicomobject;
    }

    public static DicomObject preformatColor(DicomObject dicomobject)
        throws DicomException
    {
        PixelMatrix pixelmatrix = PixelMatrix.create(dicomobject);
        if(pixelmatrix.isRGB())
            if(!(pixelmatrix instanceof com.tiani.dicom.util.PixelMatrix.UnsignedByte))
                throw new UnsupportedOperationException("RGB with " + pixelmatrix);
            else
                return pixelmatrix.isColorByPlane() ? dicomobject : fromPixelToPlane(dicomobject);
        if(!pixelmatrix.isPaletteColor())
            throw new IllegalArgumentException("Photometric Interpretation " + pixelmatrix.getPhotometricInterpretation());
        byte abyte0[] = new byte[pixelmatrix.getPixelsPerFrame() * 3];
        fillPaneFromPalette(pixelmatrix, pixelmatrix.getRedPalette(), abyte0, 0);
        fillPaneFromPalette(pixelmatrix, pixelmatrix.getGreenPalette(), abyte0, 1);
        fillPaneFromPalette(pixelmatrix, pixelmatrix.getBluePalette(), abyte0, 2);
        dicomobject.set(461, ((Object) (new Integer(3))));
        dicomobject.set(462, "RGB");
        dicomobject.set(463, ((Object) (new Integer(1))));
        dicomobject.set(475, ((Object) (new Integer(8))));
        dicomobject.set(476, ((Object) (new Integer(8))));
        dicomobject.set(477, ((Object) (new Integer(7))));
        dicomobject.set(1184, ((Object) (abyte0)));
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Convert pixel data from PALETTE COLOR to RGB");
        return dicomobject;
    }

    private static DicomObject fromPixelToPlane(DicomObject dicomobject)
        throws DicomException
    {
        byte abyte0[] = (byte[])dicomobject.get(1184);
        byte abyte1[] = new byte[abyte0.length];
        int i = abyte0.length / 3;
        int j = 0;
        int k = 0;
        int l = i;
        int i1 = i << 1;
        for(int j1 = 0; j1 < i; j1++)
        {
            abyte1[k++] = abyte0[j++];
            abyte1[l++] = abyte0[j++];
            abyte1[i1++] = abyte0[j++];
        }

        dicomobject.set(463, ((Object) (new Integer(1))));
        dicomobject.set(1184, ((Object) (abyte1)));
        if(Debug.DEBUG > 1)
            Debug.out.println("jdicom: Convert Planar Configuration from pixel to plane");
        return dicomobject;
    }

    private static void fillPaneFromPalette(PixelMatrix pixelmatrix, byte abyte0[], byte abyte1[], int i)
        throws DicomException
    {
        int j = 0;
        int k = pixelmatrix.getPixelsPerFrame();
        for(int l = k * i; j < k; l++)
        {
            abyte1[l] = abyte0[pixelmatrix.getSample(j)];
            j++;
        }

    }

    public static DicomObject colorToGrayscale(DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject.getI(461) != 3)
            throw new IllegalArgumentException("SamplesPerPixel != 3");
        if(!"RGB".equals(((Object) (dicomobject.getS(462)))))
            throw new IllegalArgumentException("dPhotometricInterpretation != \"RGB\"");
        if(dicomobject.getI(475) != 8)
            throw new IllegalArgumentException("dBitsAllocated != 8");
        if(dicomobject.getI(476) != 8)
            throw new IllegalArgumentException("dBitsStored != 8");
        if(dicomobject.getI(477) != 7)
            throw new IllegalArgumentException("dHighBit != 7");
        if(dicomobject.getI(478) != 0)
            throw new IllegalArgumentException("dPixelRepresentation != 0");
        int i = dicomobject.getI(466) * dicomobject.getI(467);
        int j = 1;
        byte byte0 = 3;
        if(dicomobject.getI(463) != 0)
        {
            j = i;
            byte0 = 1;
        }
        byte abyte0[] = (byte[])dicomobject.get(1184);
        byte abyte1[] = new byte[i];
        int k = 0;
        int l = k + j;
        int i1 = l + j;
        for(int j1 = 0; j1 < i;)
        {
            abyte1[j1] = (byte)(((abyte0[k] & 0xff) + (abyte0[l] & 0xff) + (abyte0[i1] & 0xff)) / 3);
            j1++;
            k += ((int) (byte0));
            l += ((int) (byte0));
            i1 += ((int) (byte0));
        }

        dicomobject.set(461, ((Object) (new Integer(1))));
        dicomobject.set(462, "MONOCHROME2");
        dicomobject.deleteItem(463);
        dicomobject.deleteItem(490);
        dicomobject.deleteItem(489);
        dicomobject.deleteItem(487);
        dicomobject.deleteItem(488);
        dicomobject.set(1184, ((Object) (abyte1)));
        return dicomobject;
    }

    public static DicomObject grayscaleToColor(DicomObject dicomobject)
        throws DicomException
    {
        if(dicomobject.getI(461) != 1)
            throw new IllegalArgumentException("SamplesPerPixel != 1");
        if(!"MONOCHROME2".equals(((Object) (dicomobject.getS(462)))))
            throw new IllegalArgumentException("dPhotometricInterpretation != \"MONOCHROME2\"");
        if(dicomobject.getI(475) != 8)
            throw new IllegalArgumentException("dBitsAllocated != 8");
        if(dicomobject.getI(476) != 8)
            throw new IllegalArgumentException("dBitsStored != 8");
        if(dicomobject.getI(477) != 7)
            throw new IllegalArgumentException("dHighBit != 7");
        if(dicomobject.getI(478) != 0)
        {
            throw new IllegalArgumentException("dPixelRepresentation != 0");
        } else
        {
            byte abyte0[] = (byte[])dicomobject.get(1184);
            int i = abyte0.length;
            byte abyte1[] = new byte[i * 3];
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), 0, i);
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), i, i);
            System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), i * 2, i);
            dicomobject.set(461, ((Object) (new Integer(3))));
            dicomobject.set(462, "RGB");
            dicomobject.set(463, ((Object) (new Integer(1))));
            dicomobject.set(1184, ((Object) (abyte1)));
            return dicomobject;
        }
    }

}
