// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BasicDirectoryIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, Attribute, IfOr, ICondition, 
//            IfPresent, IfEqualInt, IfEqual, IfNot, 
//            IfSizeEqual, GeneralModules, CommonImage

public final class BasicDirectoryIOD
{

    static final UserOption cFileSetCharacterSet;
    public static final Attribute FILE_SET_IDENTIFICATION[];
    static final UserOption cReferencedObject;
    static final UserOption cIndirectReferencedObject;
    static final ICondition IF_REFERENCED_FILE;
    public static final Attribute DIRECTORY_RECORD_SEQUENCE[];
    public static final Attribute DIRECTORY_INFORMATION[];
    public static final Attribute PATIENT_KEYS[];
    public static final Attribute STUDY_KEYS[];
    public static final Attribute SERIES_KEYS[];
    public static final Attribute IMAGE_KEYS[];
    public static final UserOption options[];

    private BasicDirectoryIOD()
    {
    }

    static 
    {
        cFileSetCharacterSet = new UserOption("C:Specific File-set Character Set");
        FILE_SET_IDENTIFICATION = (new Attribute[] {
            new Attribute(38, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(39, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(40, 1, ((ICondition) (cFileSetCharacterSet)), ((ICondition) (null)))
        });
        cReferencedObject = new UserOption("C:Referenced Object");
        cIndirectReferencedObject = new UserOption("C:Indirect Referenced Object");
        IF_REFERENCED_FILE = ((ICondition) (new IfOr(new ICondition[] {
            new IfPresent(50), new IfPresent(51)
        })));
        DIRECTORY_RECORD_SEQUENCE = (new Attribute[] {
            new Attribute(45, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(46, 1, ((ICondition) (null)), ((ICondition) (new IfEqualInt(46, new int[] {
                0, 65535
            })))), new Attribute(47, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(48, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(48, ((Object []) (new String[] {
                "PATIENT", "STUDY", "SERIES", "IMAGE", "OVERLAY", "MODALITY LUT", "VOI LUT", "CURVE", "TOPIC", "VISIT", 
                "RESULTS", "INTERPRETATION", "STUDY COMPONENT", "STORED PRINT", "PRESENTATION", "PRIVATE", "MRDR"
            })))))), new Attribute(49, 1, ((ICondition) (new IfEqual(48, "PRIVATE"))), ((ICondition) (null))), new Attribute(50, 1, ((ICondition) (cReferencedObject)), ((ICondition) (null))), new Attribute(51, 1, ((ICondition) (cIndirectReferencedObject)), ((ICondition) (null))), new Attribute(52, 1, IF_REFERENCED_FILE, ((ICondition) (null))), new Attribute(53, 1, IF_REFERENCED_FILE, ((ICondition) (null))), new Attribute(54, 1, IF_REFERENCED_FILE, ((ICondition) (null)))
        });
        DIRECTORY_INFORMATION = (new Attribute[] {
            new Attribute(41, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(42, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(43, 1, ((ICondition) (null)), ((ICondition) (new IfEqualInt(43, new int[] {
                0, 65535
            })))), new Attribute(44, 2, ((ICondition) (null)), ((ICondition) (null)), DIRECTORY_RECORD_SEQUENCE)
        });
        PATIENT_KEYS = (new Attribute[] {
            new Attribute(57, 1, ((ICondition) (GeneralModules.cSpecificCharacterSet)), ((ICondition) (null))), new Attribute(147, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(148, 1, ((ICondition) (null)), ((ICondition) (null)))
        });
        STUDY_KEYS = (new Attribute[] {
            new Attribute(57, 1, ((ICondition) (GeneralModules.cSpecificCharacterSet)), ((ICondition) (null))), new Attribute(64, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(70, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(95, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(425, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(53)))))), ((ICondition) (null))), new Attribute(427, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(77, 2, ((ICondition) (null)), ((ICondition) (null)))
        });
        SERIES_KEYS = (new Attribute[] {
            new Attribute(57, 1, ((ICondition) (GeneralModules.cSpecificCharacterSet)), ((ICondition) (null))), new Attribute(81, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(426, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(428, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(699, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(699, 1))), CommonImage.imagePixelModule)
        });
        IMAGE_KEYS = (new Attribute[] {
            new Attribute(57, 1, ((ICondition) (GeneralModules.cSpecificCharacterSet)), ((ICondition) (null))), new Attribute(430, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(699, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(699, 1))), CommonImage.imagePixelModule)
        });
        options = (new UserOption[] {
            cFileSetCharacterSet, cReferencedObject, cIndirectReferencedObject
        });
    }
}
