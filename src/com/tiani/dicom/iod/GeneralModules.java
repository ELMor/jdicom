// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   GeneralModules.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, Attribute

final class GeneralModules
{

    static final UserOption cSpecificCharacterSet;
    static final Attribute SOP_COMMON[];

    private GeneralModules()
    {
    }

    static 
    {
        cSpecificCharacterSet = new UserOption("C:Specific Character Set");
        SOP_COMMON = (new Attribute[] {
            new Attribute(62, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(63, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(57, 1, ((ICondition) (cSpecificCharacterSet)), ((ICondition) (null))), new Attribute(59, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(60, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(61, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(430, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
    }
}
