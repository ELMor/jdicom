// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CommonImage.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, UserOption, IfEqual, IfSizeEqual, 
//            IfSizeInRange, IfMultiEqual, IfNot, IfOr, 
//            ICondition, IfPresent, IfEqualInt, IfAnyEqual, 
//            IfAllEqual, IfInRange, ICallbackUser

final class CommonImage
{

    public static final Attribute referencedSOPSequence[] = {
        new Attribute(115, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(116, 1, ((ICondition) (null)), ((ICondition) (null)))
    };
    public static final Attribute codeSequence[] = {
        new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cRefMultiframeImage;
    public static final Attribute referencedMFImageSequence[];
    static final ICondition enumPatientSex;
    static final Attribute patientModule[];
    static final Attribute generalStudyModule[] = {
        new Attribute(425, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(64, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(70, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(88, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(427, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(77, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(95, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(99, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(101, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(107, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(107, 0, 1))), referencedSOPSequence)
    };
    static final UserOption uPatientStudy = new UserOption("U:Patient Study");
    static final Attribute patientStudyModule[] = {
        new Attribute(103, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(157, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(158, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(159, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(171, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(173, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition requirePatientPositionModality;
    static final ICondition enumLaterality;
    static final ICondition requireLateralityBodyPart;
    static final Attribute requestAttributesSequence[] = {
        new Attribute(589, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(582, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(580, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(581, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(581, 1, 0x7fffffff))), codeSequence)
    };
    static final Attribute generalSeriesModule[];
    static final UserOption uFrameOfReference = new UserOption("U:Frame Of Reference");
    static final Attribute frameOfReferenceModule[] = {
        new Attribute(442, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(450, 2, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption uGeneralEquipment = new UserOption("U:General Equipment");
    static final Attribute generalEquipmentModule[] = {
        new Attribute(84, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(85, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(86, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(94, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(98, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(105, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(228, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(236, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(250, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(317, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(318, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(485, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cImagesTemporallyRelated;
    static final UserOption cLossyImageCompression = new UserOption("C:Lossy Image Compression");
    static final ICondition enumImageType;
    static final ICondition enumLossyImageCompression;
    static final Attribute generalImageModule[];
    static final Attribute imagePlaneModule[] = {
        new Attribute(470, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(441, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(440, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(205, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(451, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition isMonochrome2 = new IfEqual(462, "MONOCHROME2");
    static final ICondition isMonochrome2OrPaletteColor = new IfEqual(462, ((Object []) (new String[] {
        "MONOCHROME2", "PALETTE COLOR"
    })));
    static final ICondition isMonochrome = new IfEqual(462, ((Object []) (new String[] {
        "MONOCHROME1", "MONOCHROME2"
    })));
    static final ICondition enumPI1sample = new IfEqual(462, ((Object []) (new String[] {
        "MONOCHROME1", "MONOCHROME2", "PALETTE COLOR"
    })));
    static final ICondition enumPI3sample = new IfEqual(462, ((Object []) (new String[] {
        "RGB", "HSV", "YBR_FULL", "YBR_FULL_422", "YBR_PARTIAL_422"
    })));
    static final ICondition enumPI4sample = new IfEqual(462, ((Object []) (new String[] {
        "ARGB", "CMYK"
    })));
    static final ICondition requirePaletteColorLUT;
    static final ICondition is1SamplePerPixel = new IfEqualInt(461, 1);
    static final ICondition samplesPerPixelValueFilter;
    static final ICondition is16BitsAllocated = new IfEqualInt(475, 16);
    static final ICondition is8or16BitsAllocated = new IfEqualInt(475, new int[] {
        8, 16
    });
    static final ICondition bitsStoredValueFilter;
    static final ICondition isBitsStoredEqualsBitsAllocated = new ICondition() {

        public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
            throws DicomException
        {
            int i = dicomobject.getI(475);
            int j = dicomobject.getI(476);
            return i == 0x7fffffff || j == i;
        }

    };
    static final ICondition highBitValueFilter;
    static final ICondition isHighBitEqualsBitsStoredLess1 = new ICondition() {

        public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
            throws DicomException
        {
            int i = dicomobject.getI(476);
            int j = dicomobject.getI(477);
            return i == 0x7fffffff || j + 1 == i;
        }

    };
    static final ICondition isPixelRepresentation0 = new IfEqualInt(478, 0);
    static final UserOption cPixelAspectRatioNot11;
    static final Attribute imagePixelModule[];
    static final UserOption cContrastBolusUsed = new UserOption("C:Contrast/Bolus Used");
    static final Attribute contrastBolusAdministrationRouteSequence[] = {
        new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(193, 3, ((ICondition) (null)), ((ICondition) (null)), codeSequence)
    };
    static final Attribute contrastBolusModule[] = {
        new Attribute(179, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(180, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(180, 1))), codeSequence), new Attribute(240, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(181, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(181, 1))), contrastBolusAdministrationRouteSequence), new Attribute(241, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(242, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(243, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(244, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(246, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(247, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(248, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(249, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cMultiFrameCineImage = new UserOption("C:Multi-Frame Cine Image");
    static final Attribute cineModule[] = {
        new Attribute(322, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(254, 1, ((ICondition) (new IfEqualInt(465, 0x181063))), ((ICondition) (null))), new Attribute(256, 1, ((ICondition) (new IfEqualInt(465, 0x181065))), ((ICondition) (null))), new Attribute(132, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(133, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(134, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(204, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(257, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(209, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(320, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cMultiFrameImage = new UserOption("C:Multi-Frame Image");
    static final Attribute multiFrameModule[] = {
        new Attribute(464, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(465, 1, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption uFramePointers = new UserOption("U:Frame Pointers");
    static final Attribute framePointersModule[] = {
        new Attribute(512, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(513, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(514, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cImageMayBeSubtracted = new UserOption("C:Image May Be Subtracted");
    static final Attribute maskSubtractionSequence[] = {
        new Attribute(518, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(519, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(520, 1, ((ICondition) (new IfEqual(518, "AVG_SUB"))), ((ICondition) (null))), new Attribute(521, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(522, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(523, 2, ((ICondition) (new IfEqual(518, "TID"))), ((ICondition) (null))), new Attribute(524, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute maskModule[] = {
        new Attribute(517, 1, ((ICondition) (null)), ((ICondition) (null)), maskSubtractionSequence), new Attribute(493, 2, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption uDisplayShutter = new UserOption("U:Display Shutter");
    static final UserOption cDisplayShutter = new UserOption("C:Display Shutter");
    static final ICondition ifShutterRectangular;
    static final ICondition ifShutterCircular;
    static final ICondition ifShutterPolygonal;
    static final Attribute displayShutterModule[];
    static final UserOption uDevice = new UserOption("U:Device");
    static final Attribute deviceSequence[] = {
        new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(612, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(613, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(614, 2, ((ICondition) (new IfPresent(613))), ((ICondition) (null))), new Attribute(615, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(616, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(617, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute deviceModule[] = {
        new Attribute(611, 3, ((ICondition) (null)), ((ICondition) (null)), deviceSequence)
    };
    static final UserOption uTherapy = new UserOption("U:Therapy");
    static final Attribute interventionalTherapySequence[] = {
        new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(202, 2, ((ICondition) (null)), ((ICondition) (new IfEqual(202, ((Object []) (new String[] {
            "PRE", "INTERMEDIATE", "POST", "NONE"
        })))))), new Attribute(192, 3, ((ICondition) (null)), ((ICondition) (null)), codeSequence), new Attribute(199, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(190, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(661, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(661, 1))), codeSequence), new Attribute(203, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute therapyModule[] = {
        new Attribute(200, 3, ((ICondition) (null)), ((ICondition) (null)), interventionalTherapySequence)
    };
    static final UserOption cBitmapDisplayShutter = new UserOption("C:Bitmap Display Shutter");
    static final Attribute bitmapDisplayShutter[] = {
        new Attribute(352, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(352, "BITMAP")))), new Attribute(1378, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1377, 3, ((ICondition) (null)), ((ICondition) (new IfInRange(1377, 0, 65535))))
    };
    static final Attribute patientSummaryModule[] = {
        new Attribute(147, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(148, 2, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final UserOption cSeriesRetrieveAET;
    static final UserOption cSeriesFilesetUID;
    static final UserOption cImageRetrieveAET;
    static final UserOption cImageFilesetUID;
    static final Attribute referencedImageSequence[];
    static final Attribute referencedSeriesSequence[];
    static final Attribute studyContentModule[];
    static final Attribute paletteColorLUTModule[];

    private CommonImage()
    {
    }

    static 
    {
        cRefMultiframeImage = new UserOption("C:Ref Multiframe Image");
        referencedMFImageSequence = (new Attribute[] {
            new Attribute(115, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(116, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(117, 1, ((ICondition) (cRefMultiframeImage)), ((ICondition) (null)))
        });
        enumPatientSex = ((ICondition) (new IfEqual(152, ((Object []) (new String[] {
            "M", "F", "O"
        })))));
        patientModule = (new Attribute[] {
            new Attribute(147, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(148, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(150, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(152, 2, ((ICondition) (null)), enumPatientSex), new Attribute(110, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(110, 1))), referencedSOPSequence), new Attribute(151, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(154, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(155, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(170, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(177, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        requirePatientPositionModality = ((ICondition) (new IfEqual(81, ((Object []) (new String[] {
            "CT", "MR"
        })))));
        enumLaterality = ((ICondition) (new IfEqual(443, ((Object []) (new String[] {
            "L", "R"
        })))));
        requireLateralityBodyPart = ((ICondition) (new IfEqual(182, ((Object []) (new String[] {
            "CLAVICLE", "BREAST", "HIP", "SHOULDER", "ELBOW", "KNEE", "ANKLE", "HAND", "FOOT", "EXTREMITY", 
            "HEAD", "LEG", "ARM"
        })))));
        generalSeriesModule = (new Attribute[] {
            new Attribute(81, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(426, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(428, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(443, 2, requireLateralityBodyPart, enumLaterality), new Attribute(65, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(71, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(100, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(239, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(97, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(102, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(108, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(108, 0, 1))), referencedSOPSequence), new Attribute(182, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(380, 2, requirePatientPositionModality, ((ICondition) (null))), new Attribute(481, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(482, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1211, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(1211, 1, 0x7fffffff))), requestAttributesSequence), new Attribute(1206, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1201, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1202, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1207, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        cImagesTemporallyRelated = new UserOption("C:Images Temporally Related");
        enumImageType = ((ICondition) (new IfMultiEqual(58, ((Object [][]) (new String[][] {
            new String[] {
                "ORIGINAL", "DERIVED"
            }, new String[] {
                "PRIMARY", "SECONDARY"
            }
        })))));
        enumLossyImageCompression = ((ICondition) (new IfEqual(504, ((Object []) (new String[] {
            "00", "01"
        })))));
        generalImageModule = (new Attribute[] {
            new Attribute(430, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(436, 2, ((ICondition) (new IfNot(((ICondition) (new IfOr(new ICondition[] {
                new IfPresent(441), new IfPresent(666)
            })))))), ((ICondition) (null))), new Attribute(67, 2, ((ICondition) (cImagesTemporallyRelated)), ((ICondition) (null))), new Attribute(73, 2, ((ICondition) (cImagesTemporallyRelated)), ((ICondition) (null))), new Attribute(58, 3, ((ICondition) (null)), enumImageType), new Attribute(429, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(66, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(72, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 3, ((ICondition) (null)), ((ICondition) (null)), referencedSOPSequence), new Attribute(122, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(123, 3, ((ICondition) (null)), ((ICondition) (null)), referencedSOPSequence), new Attribute(448, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(459, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(504, 3, ((ICondition) (null)), enumLossyImageCompression)
        });
        requirePaletteColorLUT = ((ICondition) (new IfEqual(462, ((Object []) (new String[] {
            "PALETTE COLOR", "ARGB"
        })))));
        samplesPerPixelValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                byte byte0 = 0;
                if(CommonImage.enumPI1sample.isTrue(dicomobject, icallbackuser))
                    byte0 = 1;
                else
                if(CommonImage.enumPI3sample.isTrue(dicomobject, icallbackuser))
                    byte0 = 3;
                else
                if(CommonImage.enumPI4sample.isTrue(dicomobject, icallbackuser))
                    byte0 = 4;
                else
                    return true;
                return dicomobject.getI(461) == byte0;
            }

        };
        bitsStoredValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(475);
                int j = dicomobject.getI(476);
                return j > 0 && j <= i;
            }

        };
        highBitValueFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(475);
                int j = dicomobject.getI(476);
                int k = dicomobject.getI(477);
                return j == 0x7fffffff || k >= j - 1 && k <= i - 1;
            }

        };
        cPixelAspectRatioNot11 = new UserOption("C:Pixel Aspect Ratio not 1:1");
        imagePixelModule = (new Attribute[] {
            new Attribute(461, 1, ((ICondition) (null)), samplesPerPixelValueFilter), new Attribute(462, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(466, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(467, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(475, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(476, 1, ((ICondition) (null)), bitsStoredValueFilter), new Attribute(477, 1, ((ICondition) (null)), highBitValueFilter), new Attribute(478, 1, ((ICondition) (null)), ((ICondition) (new IfEqualInt(478, new int[] {
                0, 1
            })))), new Attribute(1184, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(463, 1, ((ICondition) (new IfNot(((ICondition) (new IfEqualInt(461, 1)))))), ((ICondition) (new IfEqualInt(463, new int[] {
                0, 1
            })))), 
            new Attribute(473, 1, ((ICondition) (cPixelAspectRatioNot11)), ((ICondition) (null))), new Attribute(479, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(480, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(494, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(495, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(496, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(498, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(499, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(500, 1, requirePaletteColorLUT, ((ICondition) (null)))
        });
        ifShutterRectangular = ((ICondition) (new IfAnyEqual(352, "RECTANGULAR")));
        ifShutterCircular = ((ICondition) (new IfAnyEqual(352, "CIRCULAR")));
        ifShutterPolygonal = ((ICondition) (new IfAnyEqual(352, "POLYGONAL")));
        displayShutterModule = (new Attribute[] {
            new Attribute(352, 1, ((ICondition) (null)), ((ICondition) (new IfAllEqual(352, ((Object []) (new String[] {
                "RECTANGULAR", "CIRCULAR", "POLYGONAL"
            })))))), new Attribute(353, 1, ifShutterRectangular, ((ICondition) (null))), new Attribute(354, 1, ifShutterRectangular, ((ICondition) (null))), new Attribute(355, 1, ifShutterRectangular, ((ICondition) (null))), new Attribute(356, 1, ifShutterRectangular, ((ICondition) (null))), new Attribute(357, 1, ifShutterCircular, ((ICondition) (null))), new Attribute(358, 1, ifShutterCircular, ((ICondition) (null))), new Attribute(359, 1, ifShutterPolygonal, ((ICondition) (null))), new Attribute(1377, 3, ((ICondition) (null)), ((ICondition) (new IfInRange(1377, 0, 65535))))
        });
        cSeriesRetrieveAET = new UserOption("C:Series Retrieve AET");
        cSeriesFilesetUID = new UserOption("C:Series Fileset (U)ID");
        cImageRetrieveAET = new UserOption("C:Image Retrieve AET");
        cImageFilesetUID = new UserOption("C:Image Fileset (U)ID");
        referencedImageSequence = (new Attribute[] {
            new Attribute(115, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(116, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(79, 2, ((ICondition) (cImageRetrieveAET)), ((ICondition) (null))), new Attribute(697, 2, ((ICondition) (cImageFilesetUID)), ((ICondition) (null))), new Attribute(698, 2, ((ICondition) (cImageFilesetUID)), ((ICondition) (null)))
        });
        referencedSeriesSequence = (new Attribute[] {
            new Attribute(426, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(79, 2, ((ICondition) (cSeriesRetrieveAET)), ((ICondition) (null))), new Attribute(697, 2, ((ICondition) (cSeriesFilesetUID)), ((ICondition) (null))), new Attribute(698, 2, ((ICondition) (cSeriesFilesetUID)), ((ICondition) (null))), new Attribute(113, 1, ((ICondition) (null)), ((ICondition) (null)), referencedImageSequence)
        });
        studyContentModule = (new Attribute[] {
            new Attribute(427, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(425, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(109, 1, ((ICondition) (null)), ((ICondition) (null)), referencedSeriesSequence)
        });
        paletteColorLUTModule = (new Attribute[] {
            new Attribute(473, 1, ((ICondition) (cPixelAspectRatioNot11)), ((ICondition) (null))), new Attribute(479, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(480, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(494, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(495, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(496, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(497, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(498, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(499, 1, requirePaletteColorLUT, ((ICondition) (null))), new Attribute(500, 1, requirePaletteColorLUT, ((ICondition) (null)))
        });
    }
}
