// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CTImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfInRange, IfEqual, UserOption, 
//            CommonImage, CompositeIOD, OverlayModules, LUTModules, 
//            GeneralModules

public final class CTImageIOD
{

    static final Attribute imageModule[];
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    public CTImageIOD()
    {
    }

    static 
    {
        imageModule = (new Attribute[] {
            new Attribute(58, 1, ((ICondition) (null)), CommonImage.enumImageType), new Attribute(461, 1, ((ICondition) (null)), CommonImage.is1SamplePerPixel), new Attribute(462, 1, ((ICondition) (null)), CommonImage.isMonochrome), new Attribute(475, 1, ((ICondition) (null)), CommonImage.is16BitsAllocated), new Attribute(476, 1, ((ICondition) (null)), ((ICondition) (new IfInRange(476, 12, 16)))), new Attribute(477, 1, ((ICondition) (null)), CommonImage.isHighBitEqualsBitsStoredLess1), new Attribute(489, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(490, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(206, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(429, 2, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(185, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(276, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(278, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(280, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(282, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(289, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(289, ((Object []) (new String[] {
                "CW", "CC"
            })))))), new Attribute(298, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(299, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(300, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(305, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(310, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(316, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(319, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), 
            new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, OverlayModules.uOverlayPlane, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
