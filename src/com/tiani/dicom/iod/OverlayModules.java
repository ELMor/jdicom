// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   OverlayModules.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, UserOption, CommonImage

final class OverlayModules
{

    static final Attribute overlayIdentificationModule[];
    static final UserOption uOverlayPlane = new UserOption("U:Overlay Plane");
    static final UserOption cOverlayPlane = new UserOption("C:Apply Overlay Plane");
    static final Attribute overlayPlaneModule[] = new Attribute[0];
    static final UserOption uMultiFrameOverlay = new UserOption("U:Multi-Frame Overlay");
    static final UserOption cMultiFrameOverlay = new UserOption("C:Multi-Frame Overlay");
    static final Attribute multiFrameOverlayModule[] = new Attribute[0];

    private OverlayModules()
    {
    }

    static 
    {
        overlayIdentificationModule = (new Attribute[] {
            new Attribute(437, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(68, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(74, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence)
        });
    }
}
