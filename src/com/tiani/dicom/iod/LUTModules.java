// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LUTModules.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, Attribute, IfPresent, IfNot, 
//            IfSizeEqual, IfEqual, CommonImage, ICondition, 
//            ICallbackUser

final class LUTModules
{

    static final UserOption cModalityLUT = new UserOption("C:Apply Modality LUT");
    static final UserOption uModalityLUT = new UserOption("U:Modality LUT");
    static final Attribute modalityLUTSequence[] = {
        new Attribute(506, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(507, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(508, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(509, 1, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition ifRescaleInterceptPresent;
    static final Attribute modalityLUTModule[];
    static final UserOption uVOILUT = new UserOption("U:VOI LUT");
    static final Attribute LUTSequence[] = {
        new Attribute(506, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(507, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(509, 1, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition isWindowWidthVM;
    static final Attribute VOILUTModule[];
    static final Attribute LUTIdentificationModule[];
    static final String SHAPE_VALUES[] = {
        "IDENTITY", "INVERSE"
    };
    static final Attribute presentationLUTModule[] = {
        new Attribute(1239, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(1240)))))), ((ICondition) (new IfSizeEqual(1239, 1))), LUTSequence), new Attribute(1240, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(1239)))))), ((ICondition) (new IfEqual(1240, ((Object []) (SHAPE_VALUES))))))
    };
    static final UserOption cSoftcopyVOILUT = new UserOption("C:Apply VOI LUT");
    static final UserOption cVOILUTNotApplyToAll;
    static final Attribute softcopyVOILUTSequenceModule[];
    static final Attribute softcopyVOILUTModule[];

    private LUTModules()
    {
    }

    static 
    {
        ifRescaleInterceptPresent = ((ICondition) (new IfPresent(489)));
        modalityLUTModule = (new Attribute[] {
            new Attribute(505, 3, ((ICondition) (null)), ((ICondition) (null)), modalityLUTSequence), new Attribute(489, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(505)))))), ((ICondition) (null))), new Attribute(490, 1, ifRescaleInterceptPresent, ((ICondition) (null))), new Attribute(491, 1, ifRescaleInterceptPresent, ((ICondition) (null)))
        });
        isWindowWidthVM = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                return dicomobject.getSize(488) == dicomobject.getSize(487);
            }

        };
        VOILUTModule = (new Attribute[] {
            new Attribute(510, 3, ((ICondition) (null)), ((ICondition) (null)), LUTSequence), new Attribute(487, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(488, 1, ((ICondition) (new IfPresent(487))), isWindowWidthVM), new Attribute(492, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        LUTIdentificationModule = (new Attribute[] {
            new Attribute(439, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence)
        });
        cVOILUTNotApplyToAll = new UserOption("C:VOI LUT not apply to all");
        softcopyVOILUTSequenceModule = (new Attribute[] {
            new Attribute(113, 1, ((ICondition) (cVOILUTNotApplyToAll)), ((ICondition) (null)), CommonImage.referencedMFImageSequence), new Attribute(510, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(487)))))), ((ICondition) (new IfSizeEqual(510, 1))), LUTSequence), new Attribute(487, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(488, 1, ((ICondition) (new IfPresent(487))), isWindowWidthVM), new Attribute(492, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        softcopyVOILUTModule = (new Attribute[] {
            new Attribute(1408, 1, ((ICondition) (null)), ((ICondition) (null)), softcopyVOILUTSequenceModule)
        });
    }
}
