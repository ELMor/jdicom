// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfAnd.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfAnd
    implements ICondition
{

    private ICondition args[];

    public IfAnd(ICondition aicondition[])
    {
        args = aicondition;
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        for(int i = 0; i < args.length; i++)
            if(!args[i].isTrue(dicomobject, icallbackuser))
                return false;

        return true;
    }
}
