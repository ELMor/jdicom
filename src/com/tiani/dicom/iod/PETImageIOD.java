// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PETImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            PETCommonIOD, Attribute, IfSizeInRange, IfMultiEqual, 
//            IfNot, IfEqual, IfSizeEqual, UserOption, 
//            CommonImage, CompositeIOD, NMImageIOD, OverlayModules, 
//            LUTModules, GeneralModules

public final class PETImageIOD extends PETCommonIOD
{

    public static final Attribute anatomicRegionSequence[];
    public static final Attribute primaryAnatomicStructureSequence[];
    static final Attribute imageModule[];
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private PETImageIOD()
    {
    }

    static 
    {
        anatomicRegionSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(139, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(139, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        primaryAnatomicStructureSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(141, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(141, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        imageModule = (new Attribute[] {
            new Attribute(58, 1, ((ICondition) (null)), ((ICondition) (new IfMultiEqual(58, ((Object [][]) (new String[][] {
                new String[] {
                    "ORIGINAL", "DERIVED"
                }, new String[] {
                    "PRIMARY"
                }
            })))))), new Attribute(461, 1, ((ICondition) (null)), CommonImage.is1SamplePerPixel), new Attribute(462, 1, ((ICondition) (null)), CommonImage.isMonochrome2), new Attribute(475, 1, ((ICondition) (null)), CommonImage.is16BitsAllocated), new Attribute(476, 1, ((ICondition) (null)), CommonImage.isBitsStoredEqualsBitsAllocated), new Attribute(477, 1, ((ICondition) (null)), CommonImage.isHighBitEqualsBitsStoredLess1), new Attribute(489, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(490, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(685, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(251, 1, PETCommonIOD.ifSeriesTypeGated, ((ICondition) (null))), 
            new Attribute(254, 1, PETCommonIOD.ifSeriesTypeGated, ((ICondition) (null))), new Attribute(267, 1, PETCommonIOD.ifSeriesTypeGatedAndBeatRejection, ((ICondition) (null))), new Attribute(268, 1, PETCommonIOD.ifSeriesTypeGatedAndBeatRejection, ((ICondition) (null))), new Attribute(504, 1, ((ICondition) (CommonImage.cLossyImageCompression)), CommonImage.enumLossyImageCompression), new Attribute(693, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(66, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(72, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(320, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(253, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(269, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(270, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(686, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(687, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(688, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(689, 1, ((ICondition) (new IfNot(((ICondition) (new IfEqual(675, "NONE")))))), ((ICondition) (null))), new Attribute(690, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(692, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(112, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(114, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(138, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(138, 1))), anatomicRegionSequence), 
            new Attribute(140, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(140, 1, 0x7fffffff))), primaryAnatomicStructureSequence)
        });
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.seriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.isotopeModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.multiGatedAcquisitionModule, PETCommonIOD.ifSeriesTypeGated), new CompositeIOD.ModuleTableItem(NMImageIOD.patientOrientationModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (CommonImage.uFrameOfReference))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), 
            new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePlaneModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cLossyImageCompression, OverlayModules.uOverlayPlane, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
