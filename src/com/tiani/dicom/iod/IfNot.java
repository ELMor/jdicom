// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfNot.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfNot
    implements ICondition
{

    private ICondition arg;

    public IfNot(ICondition icondition)
    {
        arg = icondition;
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        return !arg.isTrue(dicomobject, icallbackuser);
    }
}
