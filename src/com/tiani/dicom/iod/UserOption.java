// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UserOption.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

public final class UserOption
    implements ICondition
{

    private String _option;

    public UserOption(String s)
    {
        _option = s;
    }

    public String option()
    {
        return _option;
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        return icallbackuser == null ? false : icallbackuser.isTrue(_option, dicomobject);
    }
}
