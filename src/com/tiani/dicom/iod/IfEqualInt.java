// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfEqualInt.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfEqualInt
    implements ICondition
{

    private int dname;
    private int index;
    private int values[];

    public IfEqualInt(int i, int j, int ai[])
    {
        dname = i;
        index = j;
        values = ai;
    }

    public IfEqualInt(int i, int ai[])
    {
        this(i, 0, ai);
    }

    public IfEqualInt(int i, int j, int k)
    {
        this(i, j, new int[] {
            k
        });
    }

    public IfEqualInt(int i, int j)
    {
        this(i, 0, j);
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        int i = dicomobject.getI(dname, index);
        for(int j = 0; j < values.length; j++)
            if(i == values[j])
                return true;

        return false;
    }
}
