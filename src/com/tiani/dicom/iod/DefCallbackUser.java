// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefCallbackUser.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.iod:
//            ICallbackUser

public class DefCallbackUser extends Hashtable
    implements ICallbackUser
{

    static final ICallbackUser yes = new ICallbackUser() {

        public boolean isTrue(String s, DicomObject dicomobject)
            throws DicomException
        {
            return true;
        }

    };
    static final ICallbackUser no = new ICallbackUser() {

        public boolean isTrue(String s, DicomObject dicomobject)
            throws DicomException
        {
            return false;
        }

    };

    public DefCallbackUser()
    {
    }

    public boolean isTrue(String s, DicomObject dicomobject)
        throws DicomException
    {
        ICallbackUser icallbackuser = (ICallbackUser)((Hashtable)this).get(((Object) (s)));
        return icallbackuser != null && icallbackuser.isTrue(s, dicomobject);
    }

    public void put(String s, boolean flag)
    {
        super.put(((Object) (s)), ((Object) (flag ? ((Object) (yes)) : ((Object) (no)))));
    }

    public boolean isTrue(String s)
    {
        return ((Hashtable)this).get(((Object) (s))) == yes;
    }

}
