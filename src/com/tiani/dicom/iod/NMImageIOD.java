// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NMImageIOD.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            UserOption, Attribute, IfSizeInRange, IfAnyEqualInt, 
//            IfEqual, IfMultiEqual, IfSizeEqual, IfAnd, 
//            ICondition, IfNot, IfPresent, CommonImage, 
//            CompositeIOD, OverlayModules, CurveModules, LUTModules, 
//            GeneralModules, ICallbackUser

public final class NMImageIOD
{

    static final UserOption cPatientOrientationModifier;
    public static final Attribute patientOrientationCodeSequence[];
    static final Attribute patientOrientationModule[];
    static final Attribute imagePixelModule[];
    static final ICondition frameIncrementPointerValueFilter = new ICondition() {

        public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
            throws DicomException
        {
            String s = dicomobject.getS(58, 2);
            if(s == null)
                return true;
            if(s.equals("STATIC") || s.equals("WHOLE BODY"))
                return dicomobject.getI(465, 0) == 0x540010 && dicomobject.getI(465, 1) == 0x540020;
            if(s.equals("DYNAMIC"))
                return dicomobject.getI(465, 0) == 0x540010 && dicomobject.getI(465, 1) == 0x540020 && dicomobject.getI(465, 2) == 0x540030 && dicomobject.getI(465, 3) == 0x540100;
            if(s.equals("GATED"))
                return dicomobject.getI(465, 0) == 0x540010 && dicomobject.getI(465, 1) == 0x540020 && dicomobject.getI(465, 2) == 0x540060 && dicomobject.getI(465, 3) == 0x540070;
            if(s.equals("TOMO"))
                return dicomobject.getI(465, 0) == 0x540010 && dicomobject.getI(465, 1) == 0x540020 && dicomobject.getI(465, 2) == 0x540050 && dicomobject.getI(465, 3) == 0x540090;
            if(s.equals("GATED TOMO"))
                return dicomobject.getI(465, 0) == 0x540010 && dicomobject.getI(465, 1) == 0x540020 && dicomobject.getI(465, 2) == 0x540050 && dicomobject.getI(465, 3) == 0x540060 && dicomobject.getI(465, 4) == 0x540070 && dicomobject.getI(465, 5) == 0x540090;
            if(s.equals("RECON TOMO"))
                return dicomobject.getI(465, 0) == 0x540080;
            if(s.equals("RECON GATED TOMO"))
                return dicomobject.getI(465, 0) == 0x540060 && dicomobject.getI(465, 1) == 0x540070 && dicomobject.getI(465, 2) == 0x540080;
            else
                return true;
        }

    };
    static final ICondition ifEnergyWindowVector;
    static final ICondition ifDetectorVector;
    static final ICondition ifPhaseVector;
    static final ICondition ifRotationVector;
    static final ICondition ifRRIntervalVector;
    static final ICondition ifTimeSlotVector;
    static final ICondition ifSliceVector;
    static final ICondition ifAngularViewVector;
    static final ICondition ifTimeSliceVector;
    static final Attribute multiFrameModule[];
    static final ICondition ifWholeBodyImage;
    public static final Attribute anatomicRegionSequence[];
    public static final Attribute primaryAnatomicStructureSequence[];
    static final ICondition enumImageType;
    static final Attribute imageModule[];
    static final Attribute energyWindowRangeSequence[] = {
        new Attribute(623, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(624, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute energyWindowInformationSequence[] = {
        new Attribute(627, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(622, 3, ((ICondition) (null)), ((ICondition) (null)), energyWindowRangeSequence)
    };
    static final Attribute calibrationDataSequence[] = {
        new Attribute(664, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(245, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(626, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute radiopharmaceuticalInformationSequence[];
    static final Attribute interventionDrugInformationSequence[];
    static final ICondition energyWindowInformationSequenceSizeFilter;
    static final Attribute isotopeModule[];
    static final UserOption cViewAngulationModifier;
    static final Attribute viewCodeSequence[];
    static final Attribute detectorInformationSequence[];
    static final ICondition detectorInformationSequenceSizeFilter;
    static final Attribute detectorModule[];
    static final Attribute rotationInformationSequence[] = {
        new Attribute(654, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(293, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(289, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(289, ((Object []) (new String[] {
            "CW", "CC"
        })))))), new Attribute(292, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(320, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(291, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 2, ((ICondition) (new IfEqual(58, 3, "TRANSMISSION"))), ((ICondition) (null))), new Attribute(640, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(283, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(282, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final ICondition rotationInformationSequenceSizeFilter;
    static final Attribute tomoAcquisitionModule[];
    static final Attribute timeSlotInformationSequence[] = {
        new Attribute(648, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute dataInformationSequence[] = {
        new Attribute(254, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(253, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(267, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(268, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(269, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(270, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(647, 2, ((ICondition) (null)), ((ICondition) (null)), timeSlotInformationSequence)
    };
    static final Attribute gatedInformationSequence[] = {
        new Attribute(251, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(255, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(644, 2, ((ICondition) (null)), ((ICondition) (null)), dataInformationSequence)
    };
    static final ICondition gatedInformationSequenceSizeFilter;
    static final Attribute multiGatedAcquisitionModule[];
    static final Attribute phaseInformationSequence[] = {
        new Attribute(635, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(320, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(636, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(634, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(656, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(657, 1, ((ICondition) (new IfPresent(656))), ((ICondition) (null)))
    };
    static final ICondition phaseInformationSequenceSizeFilter;
    static final Attribute phaseModule[];
    static final Attribute reconstructionModule[] = {
        new Attribute(221, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(276, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(319, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(205, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(451, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private NMImageIOD()
    {
    }

    static 
    {
        cPatientOrientationModifier = new UserOption("C:Patient Orientation Modifier");
        patientOrientationCodeSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(667, 2, ((ICondition) (cPatientOrientationModifier)), ((ICondition) (new IfSizeInRange(667, 0, 1))), CommonImage.codeSequence)
        });
        patientOrientationModule = (new Attribute[] {
            new Attribute(666, 2, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(666, 0, 1))), patientOrientationCodeSequence), new Attribute(668, 2, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(668, 0, 1))), CommonImage.codeSequence)
        });
        imagePixelModule = (new Attribute[] {
            new Attribute(461, 1, ((ICondition) (null)), CommonImage.is1SamplePerPixel), new Attribute(462, 1, ((ICondition) (null)), CommonImage.isMonochrome2OrPaletteColor), new Attribute(475, 1, ((ICondition) (null)), CommonImage.is8or16BitsAllocated), new Attribute(476, 1, ((ICondition) (null)), CommonImage.isBitsStoredEqualsBitsAllocated), new Attribute(477, 1, ((ICondition) (null)), CommonImage.isHighBitEqualsBitsStoredLess1), new Attribute(470, 2, ((ICondition) (null)), ((ICondition) (null)))
        });
        ifEnergyWindowVector = ((ICondition) (new IfAnyEqualInt(465, 0x540010)));
        ifDetectorVector = ((ICondition) (new IfAnyEqualInt(465, 0x540020)));
        ifPhaseVector = ((ICondition) (new IfAnyEqualInt(465, 0x540030)));
        ifRotationVector = ((ICondition) (new IfAnyEqualInt(465, 0x540050)));
        ifRRIntervalVector = ((ICondition) (new IfAnyEqualInt(465, 0x540060)));
        ifTimeSlotVector = ((ICondition) (new IfAnyEqualInt(465, 0x540070)));
        ifSliceVector = ((ICondition) (new IfAnyEqualInt(465, 0x540080)));
        ifAngularViewVector = ((ICondition) (new IfAnyEqualInt(465, 0x540090)));
        ifTimeSliceVector = ((ICondition) (new IfAnyEqualInt(465, 0x540100)));
        multiFrameModule = (new Attribute[] {
            new Attribute(465, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(619, 1, ifEnergyWindowVector, ((ICondition) (null))), new Attribute(620, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(628, 1, ifDetectorVector, ((ICondition) (null))), new Attribute(629, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(631, 1, ifPhaseVector, ((ICondition) (null))), new Attribute(632, 1, ifPhaseVector, ((ICondition) (null))), new Attribute(637, 1, ifRotationVector, ((ICondition) (null))), new Attribute(638, 1, ((ICondition) (new IfEqual(58, 2, ((Object []) (new String[] {
                "TOMO", "GATED TOMO", "RECON TOMO", "RECON GATED TOMO"
            }))))), ((ICondition) (null))), new Attribute(641, 1, ifRRIntervalVector, ((ICondition) (null))), 
            new Attribute(642, 1, ifRRIntervalVector, ((ICondition) (null))), new Attribute(645, 1, ifTimeSlotVector, ((ICondition) (null))), new Attribute(646, 1, ifTimeSlotVector, ((ICondition) (null))), new Attribute(649, 1, ifSliceVector, ((ICondition) (null))), new Attribute(650, 1, ifSliceVector, ((ICondition) (null))), new Attribute(651, 1, ifAngularViewVector, ((ICondition) (null))), new Attribute(652, 1, ifTimeSliceVector, ((ICondition) (null)))
        });
        ifWholeBodyImage = ((ICondition) (new IfEqual(58, 2, "WHOLE BODY")));
        anatomicRegionSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(139, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(139, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        primaryAnatomicStructureSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(141, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(141, 1, 0x7fffffff))), CommonImage.codeSequence)
        });
        enumImageType = ((ICondition) (new IfMultiEqual(58, ((Object [][]) (new String[][] {
            new String[] {
                "ORIGINAL", "DERIVED"
            }, new String[] {
                "PRIMARY"
            }, new String[] {
                "STATIC", "DYNAMIC", "GATED", "WHOLE BODY", "TOMO", "GATED TOMO", "RECON TOMO", "RECON GATED TOMO"
            }, new String[] {
                "EMISSION", "TRANSMISSION"
            }
        })))));
        imageModule = (new Attribute[] {
            new Attribute(58, 1, ((ICondition) (null)), enumImageType), new Attribute(665, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(504, 1, ((ICondition) (CommonImage.cLossyImageCompression)), CommonImage.enumLossyImageCompression), new Attribute(207, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(208, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(282, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(283, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(320, 1, ((ICondition) (new IfEqual(58, 2, ((Object []) (new String[] {
                "STATIC", "WHOLE BODY"
            }))))), ((ICondition) (null))), new Attribute(321, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(371, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(474, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(328, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(328, ((Object []) (new String[] {
                "1PS", "2PS", "PCN", "MSP"
            })))))), new Attribute(327, 2, ifWholeBodyImage, ((ICondition) (null))), new Attribute(329, 2, ifWholeBodyImage, ((ICondition) (null))), new Attribute(112, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(114, 3, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedSOPSequence), new Attribute(252, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(138, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(138, 1))), anatomicRegionSequence), new Attribute(140, 3, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(140, 1, 0x7fffffff))), primaryAnatomicStructureSequence)
        });
        radiopharmaceuticalInformationSequence = (new Attribute[] {
            new Attribute(660, 2, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(660, 1))), CommonImage.codeSequence), new Attribute(258, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(661, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(661, 1))), CommonImage.codeSequence), new Attribute(259, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(260, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(261, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(262, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(663, 3, ((ICondition) (null)), ((ICondition) (null)), calibrationDataSequence), new Attribute(195, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(662, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(662, 1))), CommonImage.codeSequence)
        });
        interventionDrugInformationSequence = (new Attribute[] {
            new Attribute(198, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(192, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(192, 1))), CommonImage.codeSequence), new Attribute(661, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(661, 1))), CommonImage.codeSequence), new Attribute(199, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(190, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(191, 3, ((ICondition) (null)), ((ICondition) (null)))
        });
        energyWindowInformationSequenceSizeFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(620);
                if(i == 0x7fffffff)
                    return true;
                else
                    return i == dicomobject.getSize(621);
            }

        };
        isotopeModule = (new Attribute[] {
            new Attribute(621, 2, ((ICondition) (null)), energyWindowInformationSequenceSizeFilter, energyWindowInformationSequence), new Attribute(625, 2, ((ICondition) (null)), ((ICondition) (new IfSizeInRange(625, 1, 0x7fffffff))), radiopharmaceuticalInformationSequence), new Attribute(189, 3, ((ICondition) (null)), ((ICondition) (null)), interventionDrugInformationSequence)
        });
        cViewAngulationModifier = new UserOption("C:View Angulation Modifier");
        viewCodeSequence = (new Attribute[] {
            new Attribute(91, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(92, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(93, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(659, 2, ((ICondition) (cViewAngulationModifier)), ((ICondition) (new IfSizeEqual(659, 1))), CommonImage.codeSequence)
        });
        detectorInformationSequence = (new Attribute[] {
            new Attribute(311, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(312, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(296, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(297, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(313, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(314, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(315, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(472, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(471, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(294, 3, ((ICondition) (null)), ((ICondition) (null))), 
            new Attribute(280, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 2, ((ICondition) (new IfAnd(new ICondition[] {
                new IfEqual(58, 3, "TRANSMISSION"), new IfNot(((ICondition) (new IfEqual(58, 2, "TOMO"))))
            }))), ((ICondition) (null))), new Attribute(654, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(291, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(441, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(440, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(658, 3, ((ICondition) (null)), ((ICondition) (new IfSizeEqual(658, 1))), viewCodeSequence)
        });
        detectorInformationSequenceSizeFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(629);
                if(i == 0x7fffffff)
                    return true;
                else
                    return i == dicomobject.getSize(630);
            }

        };
        detectorModule = (new Attribute[] {
            new Attribute(630, 2, detectorInformationSequenceSizeFilter, ((ICondition) (null)), detectorInformationSequence)
        });
        rotationInformationSequenceSizeFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(638);
                if(i == 0x7fffffff)
                    return true;
                else
                    return i == dicomobject.getSize(639);
            }

        };
        tomoAcquisitionModule = (new Attribute[] {
            new Attribute(639, 2, ((ICondition) (null)), rotationInformationSequenceSizeFilter, rotationInformationSequence), new Attribute(655, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(655, ((Object []) (new String[] {
                "STEP AND SHOOT", "CONTINUOUS", "ACQ DURING STEP"
            }))))))
        });
        gatedInformationSequenceSizeFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(642);
                if(i == 0x7fffffff)
                    return true;
                else
                    return i == dicomobject.getSize(643);
            }

        };
        multiGatedAcquisitionModule = (new Attribute[] {
            new Attribute(266, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(266, ((Object []) (new String[] {
                "Y", "N"
            })))))), new Attribute(271, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(272, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(273, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(643, 2, ifRRIntervalVector, gatedInformationSequenceSizeFilter, gatedInformationSequence)
        });
        phaseInformationSequenceSizeFilter = new ICondition() {

            public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
                throws DicomException
            {
                int i = dicomobject.getI(632);
                if(i == 0x7fffffff)
                    return true;
                else
                    return i == dicomobject.getSize(633);
            }

        };
        phaseModule = (new Attribute[] {
            new Attribute(633, 2, ((ICondition) (null)), phaseInformationSequenceSizeFilter, phaseInformationSequence)
        });
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(patientOrientationModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.frameOfReferenceModule, ((ICondition) (CommonImage.uFrameOfReference))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(imagePixelModule, ((ICondition) (null))), 
            new CompositeIOD.ModuleTableItem(CommonImage.multiFrameModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(multiFrameModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(isotopeModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(detectorModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(tomoAcquisitionModule, ((ICondition) (new IfEqual(58, 2, ((Object []) (new String[] {
                "TOMO", "GATED TOMO", "RECON TOMO", "RECON GATED TOMO"
            })))))), new CompositeIOD.ModuleTableItem(multiGatedAcquisitionModule, ((ICondition) (new IfEqual(58, 2, ((Object []) (new String[] {
                "GATED", "GATED TOMO", "RECON GATED TOMO"
            })))))), new CompositeIOD.ModuleTableItem(phaseModule, ((ICondition) (new IfEqual(58, 2, "DYNAMIC")))), new CompositeIOD.ModuleTableItem(reconstructionModule, ((ICondition) (new IfEqual(58, 2, ((Object []) (new String[] {
                "RECON TOMO", "RECON GATED TOMO"
            })))))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), 
            new CompositeIOD.ModuleTableItem(OverlayModules.multiFrameOverlayModule, ((ICondition) (OverlayModules.uMultiFrameOverlay))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (CurveModules.uCurve))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, cPatientOrientationModifier, CommonImage.uFrameOfReference, CommonImage.cImagesTemporallyRelated, CommonImage.cLossyImageCompression, CommonImage.cPixelAspectRatioNot11, cViewAngulationModifier, OverlayModules.uOverlayPlane, OverlayModules.uMultiFrameOverlay, CurveModules.uCurve, 
            LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
