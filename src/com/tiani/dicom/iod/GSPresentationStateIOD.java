// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   GSPresentationStateIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfEqual, UserOption, IfNot, 
//            IfPresent, IfEqualInt, IfSizeInRange, CommonImage, 
//            CompositeIOD, OverlayModules, LUTModules, GeneralModules, 
//            ICondition

public class GSPresentationStateIOD
{

    static final Attribute seriesModule[] = {
        new Attribute(81, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(81, "PR"))))
    };
    static final UserOption cDisplayedAreaNotApplyToAll;
    static final String presentationSizeModes[] = {
        "SCALE TO FIT", "TRUE SIZE", "MAGNIFY"
    };
    static final Attribute displayedAeraSelectionSequence[];
    static final Attribute displayedAeraModule[];
    static final UserOption cGraphicAnnotationModule;
    static final String YN[] = {
        "Y", "N"
    };
    static final String PIXEL_OR_DISPLAY[] = {
        "PIXEL", "DISPLAY"
    };
    static final String LEFT_RIGHT_CENTER[] = {
        "LEFT", "RIGHT", "CENTER"
    };
    static final String GRAPHIC_TYPES[] = {
        "POINT", "POLYLINE", "INTERPOLATED", "CIRCLE", "ELLIPSE"
    };
    static final String CLOSED_GRAPHIC_TYPES[] = {
        "CIRCLE", "ELLIPSE"
    };
    static final UserOption cGraphAnnotNotApplyToAll;
    static final ICondition isBoundingBox;
    static final ICondition noBoundingBox;
    static final ICondition isAnchorPoint;
    static final ICondition noAnchorPoint;
    static final Attribute textObjectSequenceModule[];
    static final Attribute graphicObjectSequenceModule[] = {
        new Attribute(1385, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(1385, ((Object []) (PIXEL_OR_DISPLAY)))))), new Attribute(1393, 1, ((ICondition) (null)), ((ICondition) (new IfEqualInt(1393, 2)))), new Attribute(1394, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1395, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1396, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(1396, ((Object []) (GRAPHIC_TYPES)))))), new Attribute(1397, 1, ((ICondition) (new IfEqual(1396, ((Object []) (CLOSED_GRAPHIC_TYPES))))), ((ICondition) (new IfEqual(1397, ((Object []) (YN))))))
    };
    static final Attribute graphicAnnotationSequenceModule[];
    static final Attribute graphicAnnotationModule[];
    static final int IMAGE_ROTATIONS[] = {
        0, 90, 180, 270
    };
    static final UserOption cSpatialTransformation;
    static final Attribute spatialTransformationModule[] = {
        new Attribute(1410, 1, ((ICondition) (null)), ((ICondition) (new IfEqualInt(1410, IMAGE_ROTATIONS)))), new Attribute(1398, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(1398, ((Object []) (YN))))))
    };
    static final Attribute graphicLayerSequenceModule[] = {
        new Attribute(1382, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1400, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1401, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1414, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1402, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute graphicLayerModule[] = {
        new Attribute(1399, 1, ((ICondition) (null)), ((ICondition) (null)), graphicLayerSequenceModule)
    };
    static final Attribute referencedSeriesSequence[];
    static final Attribute presentationStateModule[];
    static final UserOption cMaskSubtraction;
    static final Attribute maskSubtractionSequence[] = {
        new Attribute(518, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(518, ((Object []) (new String[] {
            "AVG_SUB", "TID"
        })))))), new Attribute(520, 1, ((ICondition) (new IfEqual(518, "AVG_SUB"))), ((ICondition) (null))), new Attribute(521, 1, ((ICondition) (new IfSizeInRange(520, 1, 0x7fffffff))), ((ICondition) (null))), new Attribute(522, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(523, 2, ((ICondition) (new IfEqual(518, "TID"))), ((ICondition) (null))), new Attribute(524, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute maskModule[] = {
        new Attribute(517, 1, ((ICondition) (null)), ((ICondition) (null)), maskSubtractionSequence), new Attribute(493, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(493, "SUB"))))
    };
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private GSPresentationStateIOD()
    {
    }

    static 
    {
        cDisplayedAreaNotApplyToAll = new UserOption("C:Displayed Area not apply to all");
        displayedAeraSelectionSequence = (new Attribute[] {
            new Attribute(113, 1, ((ICondition) (cDisplayedAreaNotApplyToAll)), ((ICondition) (null)), CommonImage.referencedMFImageSequence), new Attribute(1411, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1412, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1415, 1, ((ICondition) (null)), ((ICondition) (new IfEqual(1415, ((Object []) (presentationSizeModes)))))), new Attribute(1416, 1, ((ICondition) (new IfEqual(1415, "TRUE SIZE"))), ((ICondition) (null))), new Attribute(1417, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(1416)))))), ((ICondition) (null))), new Attribute(1418, 1, ((ICondition) (new IfEqual(1415, "MAGNIFY"))), ((ICondition) (null)))
        });
        displayedAeraModule = (new Attribute[] {
            new Attribute(1413, 1, ((ICondition) (null)), ((ICondition) (null)), displayedAeraSelectionSequence)
        });
        cGraphicAnnotationModule = new UserOption("C:Graphic Annotations");
        cGraphAnnotNotApplyToAll = new UserOption("C:Graphic Annotion not apply to all");
        isBoundingBox = ((ICondition) (new IfPresent(1389)));
        noBoundingBox = ((ICondition) (new IfNot(isBoundingBox)));
        isAnchorPoint = ((ICondition) (new IfPresent(1391)));
        noAnchorPoint = ((ICondition) (new IfNot(isAnchorPoint)));
        textObjectSequenceModule = (new Attribute[] {
            new Attribute(1383, 1, isBoundingBox, ((ICondition) (new IfEqual(1383, ((Object []) (PIXEL_OR_DISPLAY)))))), new Attribute(1384, 1, isAnchorPoint, ((ICondition) (new IfEqual(1384, ((Object []) (PIXEL_OR_DISPLAY)))))), new Attribute(1386, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1389, 1, noAnchorPoint, ((ICondition) (null))), new Attribute(1390, 1, noAnchorPoint, ((ICondition) (null))), new Attribute(1409, 1, isBoundingBox, ((ICondition) (new IfEqual(1409, ((Object []) (LEFT_RIGHT_CENTER)))))), new Attribute(1391, 1, noBoundingBox, ((ICondition) (null))), new Attribute(1392, 1, isAnchorPoint, ((ICondition) (new IfEqual(1392, ((Object []) (YN))))))
        });
        graphicAnnotationSequenceModule = (new Attribute[] {
            new Attribute(113, 1, ((ICondition) (cGraphAnnotNotApplyToAll)), ((ICondition) (null)), CommonImage.referencedMFImageSequence), new Attribute(1382, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1387, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(1388)))))), ((ICondition) (null)), textObjectSequenceModule), new Attribute(1388, 1, ((ICondition) (new IfNot(((ICondition) (new IfPresent(1387)))))), ((ICondition) (null)), graphicObjectSequenceModule)
        });
        graphicAnnotationModule = (new Attribute[] {
            new Attribute(1381, 1, ((ICondition) (null)), ((ICondition) (null)), graphicAnnotationSequenceModule)
        });
        cSpatialTransformation = new UserOption("C:Spatial Transformation applied");
        referencedSeriesSequence = (new Attribute[] {
            new Attribute(426, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(79, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(697, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(698, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(113, 1, ((ICondition) (null)), ((ICondition) (null)), CommonImage.referencedMFImageSequence)
        });
        presentationStateModule = (new Attribute[] {
            new Attribute(430, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1403, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1404, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1405, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1406, 1, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1407, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(109, 1, ((ICondition) (null)), ((ICondition) (null)), referencedSeriesSequence), new Attribute(1377, 1, ((ICondition) (new IfPresent(352))), ((ICondition) (null)))
        });
        cMaskSubtraction = new UserOption("C:Apply Mask Subtraction");
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(seriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(maskModule, ((ICondition) (cMaskSubtraction))), new CompositeIOD.ModuleTableItem(CommonImage.displayShutterModule, ((ICondition) (CommonImage.cDisplayShutter))), new CompositeIOD.ModuleTableItem(CommonImage.bitmapDisplayShutter, ((ICondition) (CommonImage.cBitmapDisplayShutter))), new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.cOverlayPlane))), 
            new CompositeIOD.ModuleTableItem(displayedAeraModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(graphicAnnotationModule, ((ICondition) (cGraphicAnnotationModule))), new CompositeIOD.ModuleTableItem(graphicLayerModule, ((ICondition) (cGraphicAnnotationModule))), new CompositeIOD.ModuleTableItem(spatialTransformationModule, ((ICondition) (cSpatialTransformation))), new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule, ((ICondition) (LUTModules.cModalityLUT))), new CompositeIOD.ModuleTableItem(LUTModules.softcopyVOILUTModule, ((ICondition) (LUTModules.cSoftcopyVOILUT))), new CompositeIOD.ModuleTableItem(LUTModules.presentationLUTModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(presentationStateModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, cMaskSubtraction, CommonImage.cDisplayShutter, CommonImage.cBitmapDisplayShutter, OverlayModules.cOverlayPlane, CommonImage.cRefMultiframeImage, cSpatialTransformation, cDisplayedAreaNotApplyToAll, cGraphicAnnotationModule, cGraphAnnotNotApplyToAll, 
            LUTModules.cModalityLUT, LUTModules.cSoftcopyVOILUT, LUTModules.cVOILUTNotApplyToAll, GeneralModules.cSpecificCharacterSet
        });
    }
}
