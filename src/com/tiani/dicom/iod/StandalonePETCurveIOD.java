// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StandalonePETCurveIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            PETCommonIOD, Attribute, UserOption, CompositeIOD, 
//            CommonImage, CurveModules, GeneralModules

public final class StandalonePETCurveIOD extends PETCommonIOD
{

    static final Attribute curveModule[];
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private StandalonePETCurveIOD()
    {
    }

    static 
    {
        curveModule = new Attribute[0];
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.seriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.isotopeModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(PETCommonIOD.multiGatedAcquisitionModule, PETCommonIOD.ifSeriesTypeGated), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CurveModules.curveIdentificationModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (null))), 
            new CompositeIOD.ModuleTableItem(curveModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, GeneralModules.cSpecificCharacterSet
        });
    }
}
