// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AttributeContext.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute

public class AttributeContext
{

    private AttributeContext parent;
    private int dname;
    private int index;

    public AttributeContext(AttributeContext attributecontext, int i, int j)
    {
        parent = attributecontext;
        dname = i;
        index = j;
    }

    public AttributeContext parent()
    {
        return parent;
    }

    public int dname()
    {
        return dname;
    }

    public int index()
    {
        return index;
    }

    public String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        if(parent != null)
            stringbuffer.append(parent.toString());
        stringbuffer.append(Attribute.getTagDescription(dname));
        stringbuffer.append('[');
        stringbuffer.append(index);
        stringbuffer.append("]>");
        return stringbuffer.toString();
    }
}
