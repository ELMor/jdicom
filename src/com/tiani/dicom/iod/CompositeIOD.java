// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CompositeIOD.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.iod:
//            CRImageIOD, CTImageIOD, MRImageIOD, NMImageIOD, 
//            USImageIOD, USMultiFrameImageIOD, SCImageIOD, StandaloneOverlayIOD, 
//            StandaloneCurveIOD, BasicStudyDescriptorIOD, StandaloneModalityLUTIOD, StandaloneVOILUTIOD, 
//            XAImageIOD, XRFImageIOD, PETImageIOD, StandalonePETCurveIOD, 
//            GSPresentationStateIOD, ICallbackUser, Attribute, ICondition

public final class CompositeIOD
{
    static final class ModuleTableItem
    {

        private Attribute module[];
        private ICondition cond;

        public final void copyTo(Hashtable hashtable)
        {
            for(int i = 0; i < module.length; i++)
            {
                Attribute attribute = module[i];
                hashtable.put(((Object) (new Integer(attribute.dname()))), ((Object) (new AttributeTableItem(attribute, cond))));
            }

        }

        public ModuleTableItem(Attribute aattribute[], ICondition icondition)
        {
            module = aattribute;
            cond = icondition;
        }
    }

    static final class AttributeTableItem
    {

        private Attribute attrib;
        private ICondition cond;

        public boolean validate(DicomObject dicomobject, Vector vector, ICallbackUser icallbackuser)
            throws DicomException
        {
            if(cond != null && !cond.isTrue(dicomobject, icallbackuser))
                return true;
            else
                return attrib.validate(dicomobject, vector, icallbackuser, ((AttributeContext) (null)));
        }

        public AttributeTableItem(Attribute attribute, ICondition icondition)
        {
            attrib = attribute;
            cond = icondition;
        }
    }


    private Hashtable table;
    public static final CompositeIOD crImage;
    public static final CompositeIOD ctImage;
    public static final CompositeIOD mrImage;
    public static final CompositeIOD nmImage;
    public static final CompositeIOD usImage;
    public static final CompositeIOD usMultiFrameImage;
    public static final CompositeIOD usCurve;
    public static final CompositeIOD usMultiFrameCurve;
    public static final CompositeIOD scImage;
    public static final CompositeIOD standaloneOverlay;
    public static final CompositeIOD standaloneCurve;
    public static final CompositeIOD basicStudyDescriptor;
    public static final CompositeIOD standaloneModalityLUT;
    public static final CompositeIOD standaloneVOILUT;
    public static final CompositeIOD xaImage;
    public static final CompositeIOD xrfImage;
    public static final CompositeIOD petImage;
    public static final CompositeIOD standalonePETCurve;
    public static final CompositeIOD gsPresentationState;

    public static CompositeIOD getIOD(String s, DicomObject dicomobject)
        throws DicomException
    {
        if(s != null && s.length() != 0)
        {
            if(s.equals("1.2.840.10008.5.1.4.1.1.1"))
                return crImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.2"))
                return ctImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.4"))
                return mrImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.20"))
                return nmImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.128"))
                return petImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.7"))
                return scImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.9"))
                return standaloneCurve;
            if(s.equals("1.2.840.10008.5.1.4.1.1.10"))
                return standaloneModalityLUT;
            if(s.equals("1.2.840.10008.5.1.4.1.1.8"))
                return standaloneOverlay;
            if(s.equals("1.2.840.10008.5.1.4.1.1.9"))
                return basicStudyDescriptor;
            if(s.equals("1.2.840.10008.5.1.4.1.1.11"))
                return standaloneVOILUT;
            if(s.equals("1.2.840.10008.5.1.4.1.1.129"))
                return standalonePETCurve;
            if(s.equals("1.2.840.10008.5.1.4.1.1.6.1"))
                if(dicomobject.getSize(1184) != -1)
                    return usImage;
                else
                    return usCurve;
            if(s.equals("1.2.840.10008.5.1.4.1.1.4.1"))
                if(dicomobject.getSize(1184) != -1)
                    return usMultiFrameImage;
                else
                    return usMultiFrameCurve;
            if(s.equals("1.2.840.10008.5.1.4.1.1.12.1"))
                return xaImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.12.2"))
                return xrfImage;
            if(s.equals("1.2.840.10008.5.1.4.1.1.11.1"))
                return gsPresentationState;
        }
        return null;
    }

    public CompositeIOD(ModuleTableItem amoduletableitem[])
    {
        table = new Hashtable();
        for(int i = 0; i < amoduletableitem.length; i++)
            amoduletableitem[i].copyTo(table);

    }

    public boolean validate(DicomObject dicomobject, Vector vector, ICallbackUser icallbackuser)
        throws DicomException
    {
        boolean flag = true;
        for(Enumeration enumeration = table.elements(); enumeration.hasMoreElements();)
        {
            AttributeTableItem attributetableitem = (AttributeTableItem)enumeration.nextElement();
            if(!attributetableitem.validate(dicomobject, vector, icallbackuser))
                flag = false;
        }

        return flag;
    }

    static 
    {
        crImage = new CompositeIOD(CRImageIOD.moduleTable);
        ctImage = new CompositeIOD(CTImageIOD.moduleTable);
        mrImage = new CompositeIOD(MRImageIOD.moduleTable);
        nmImage = new CompositeIOD(NMImageIOD.moduleTable);
        usImage = new CompositeIOD(USImageIOD.moduleTable);
        usMultiFrameImage = new CompositeIOD(USMultiFrameImageIOD.moduleTable);
        usCurve = new CompositeIOD(USImageIOD.moduleTableCurve);
        usMultiFrameCurve = new CompositeIOD(USMultiFrameImageIOD.moduleTableCurve);
        scImage = new CompositeIOD(SCImageIOD.moduleTable);
        standaloneOverlay = new CompositeIOD(StandaloneOverlayIOD.moduleTable);
        standaloneCurve = new CompositeIOD(StandaloneCurveIOD.moduleTable);
        basicStudyDescriptor = new CompositeIOD(BasicStudyDescriptorIOD.moduleTable);
        standaloneModalityLUT = new CompositeIOD(StandaloneModalityLUTIOD.moduleTable);
        standaloneVOILUT = new CompositeIOD(StandaloneVOILUTIOD.moduleTable);
        xaImage = new CompositeIOD(XAImageIOD.moduleTable);
        xrfImage = new CompositeIOD(XRFImageIOD.moduleTable);
        petImage = new CompositeIOD(PETImageIOD.moduleTable);
        standalonePETCurve = new CompositeIOD(StandalonePETCurveIOD.moduleTable);
        gsPresentationState = new CompositeIOD(GSPresentationStateIOD.moduleTable);
    }
}
