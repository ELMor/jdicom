// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfAnyEqualInt.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfAnyEqualInt
    implements ICondition
{

    private int dname;
    private int values[];

    public IfAnyEqualInt(int i, int ai[])
    {
        dname = i;
        values = ai;
    }

    public IfAnyEqualInt(int i, int j)
    {
        this(i, new int[] {
            j
        });
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        int i = dicomobject.getSize(dname);
        for(int j = 0; j < i; j++)
        {
            int k = dicomobject.getI(dname, j);
            for(int l = 0; l < values.length; l++)
                if(k == values[l])
                    return true;

        }

        return false;
    }
}
