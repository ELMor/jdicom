// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfEqual.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfEqual
    implements ICondition
{

    private int dname;
    private int index;
    private Object values[];

    public IfEqual(int i, int j, Object aobj[])
    {
        dname = i;
        index = j;
        values = aobj;
    }

    public IfEqual(int i, Object aobj[])
    {
        this(i, 0, aobj);
    }

    public IfEqual(int i, int j, Object obj)
    {
        this(i, j, new Object[] {
            obj
        });
    }

    public IfEqual(int i, Object obj)
    {
        this(i, 0, obj);
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        Object obj = dicomobject.get(dname, index);
        if(obj != null)
        {
            for(int i = 0; i < values.length; i++)
                if(obj.equals(values[i]))
                    return true;

        }
        return false;
    }
}
