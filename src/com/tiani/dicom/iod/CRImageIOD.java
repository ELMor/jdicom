// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CRImageIOD.java

package com.tiani.dicom.iod;


// Referenced classes of package com.tiani.dicom.iod:
//            Attribute, IfEqual, UserOption, CompositeIOD, 
//            CommonImage, OverlayModules, CurveModules, LUTModules, 
//            GeneralModules

public final class CRImageIOD
{

    static final Attribute seriesModule[] = {
        new Attribute(182, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(381, 2, ((ICondition) (null)), ((ICondition) (null))), new Attribute(305, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(311, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(316, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(325, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(326, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final Attribute imageModule[] = {
        new Attribute(206, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(229, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(277, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(278, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(298, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(299, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(300, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(1195, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(308, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(310, 3, ((ICondition) (null)), ((ICondition) (null))), 
        new Attribute(336, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(337, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(338, 3, ((ICondition) (null)), ((ICondition) (new IfEqual(338, ((Object []) (new String[] {
            "LANDSCAPE", "PORTRAIT"
        })))))), new Attribute(339, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(340, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(341, 3, ((ICondition) (null)), ((ICondition) (null))), new Attribute(384, 3, ((ICondition) (null)), ((ICondition) (null)))
    };
    static final CompositeIOD.ModuleTableItem moduleTable[];
    public static final UserOption userOptions[];

    private CRImageIOD()
    {
    }

    static 
    {
        moduleTable = (new CompositeIOD.ModuleTableItem[] {
            new CompositeIOD.ModuleTableItem(CommonImage.patientModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalStudyModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.patientStudyModule, ((ICondition) (CommonImage.uPatientStudy))), new CompositeIOD.ModuleTableItem(CommonImage.generalSeriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(seriesModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalEquipmentModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.generalImageModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.imagePixelModule, ((ICondition) (null))), new CompositeIOD.ModuleTableItem(CommonImage.contrastBolusModule, ((ICondition) (CommonImage.cContrastBolusUsed))), new CompositeIOD.ModuleTableItem(imageModule, ((ICondition) (null))), 
            new CompositeIOD.ModuleTableItem(OverlayModules.overlayPlaneModule, ((ICondition) (OverlayModules.uOverlayPlane))), new CompositeIOD.ModuleTableItem(CurveModules.curveModule, ((ICondition) (CurveModules.uCurve))), new CompositeIOD.ModuleTableItem(LUTModules.modalityLUTModule, ((ICondition) (LUTModules.uModalityLUT))), new CompositeIOD.ModuleTableItem(LUTModules.VOILUTModule, ((ICondition) (LUTModules.uVOILUT))), new CompositeIOD.ModuleTableItem(GeneralModules.SOP_COMMON, ((ICondition) (null)))
        });
        userOptions = (new UserOption[] {
            CommonImage.uPatientStudy, CommonImage.cImagesTemporallyRelated, CommonImage.cPixelAspectRatioNot11, CommonImage.cContrastBolusUsed, OverlayModules.uOverlayPlane, CurveModules.uCurve, LUTModules.uModalityLUT, LUTModules.uVOILUT, GeneralModules.cSpecificCharacterSet
        });
    }
}
