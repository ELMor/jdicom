// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IfPresent.java

package com.tiani.dicom.iod;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.iod:
//            ICondition, ICallbackUser

final class IfPresent
    implements ICondition
{

    private int dname;
    private int index;

    public IfPresent(int i, int j)
    {
        dname = i;
        index = j;
    }

    public IfPresent(int i)
    {
        this(i, 0);
    }

    public boolean isTrue(DicomObject dicomobject, ICallbackUser icallbackuser)
        throws DicomException
    {
        return dicomobject.getSize(dname) > index;
    }
}
