// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IODErrorSummary.java

package com.tiani.dicom.iod;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.iod:
//            IODError

public class IODErrorSummary
{

    private int count[][] = {
        {
            0, 0, 0
        }, {
            0, 0, 0
        }, {
            0, 0, 0
        }
    };
    private Vector offendingElements;

    public IODErrorSummary(Vector vector)
    {
        offendingElements = new Vector();
        IODError ioderror;
        for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); offendingElements.addElement(((Object) (ioderror.getAT()))))
        {
            ioderror = (IODError)enumeration.nextElement();
            count[ioderror.kind()][ioderror.type() - 1]++;
        }

    }

    public Enumeration offendingElements()
    {
        return offendingElements.elements();
    }

    public Integer[] getOffendingElements()
    {
        Integer ainteger[] = new Integer[offendingElements.size()];
        offendingElements.copyInto(((Object []) (ainteger)));
        return ainteger;
    }

    public final int missingAttrib(int i)
    {
        return count[0][i - 1];
    }

    public final int missingAttrib()
    {
        return count[0][0] + count[0][1] + count[0][2];
    }

    public final int emptyData(int i)
    {
        return count[1][i];
    }

    public final int emptyData()
    {
        return count[1][0] + count[1][1] + count[1][2];
    }

    public final int invalidData(int i)
    {
        return count[2][i - 1];
    }

    public final int invalidData()
    {
        return count[2][0] + count[2][1] + count[2][2];
    }

    public final int type(int i)
    {
        return missingAttrib(i) + emptyData(i) + invalidData(i);
    }
}
