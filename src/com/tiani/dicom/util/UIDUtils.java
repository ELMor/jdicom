// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UIDUtils.java

package com.tiani.dicom.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.rmi.server.UID;
import java.util.Random;

public final class UIDUtils
{

    public static String root = "1.2.40.0.13.0.";
    public static String lha = getLocalHostAddress() + '.';
    private static Random _random = new Random();

    public UIDUtils()
    {
    }

    public static String createUID(String s, String s1)
    {
        return (s == null ? root : s) + lha + toString(new UID()) + (s1 == null ? "" : s1);
    }

    public static String createUID(String s)
    {
        return createUID(s, "");
    }

    public static String createUID()
    {
        return createUID(root, "");
    }

    public static String createID(int i)
    {
        StringBuffer stringbuffer = new StringBuffer();
        for(int j = 0; j < i; j++)
            stringbuffer.append((_random.nextInt() & 0x7fffffff) % 10);

        return stringbuffer.toString();
    }

    public static String getLocalHostAddress()
    {
        try
        {
            return InetAddress.getLocalHost().getHostAddress();
        }
        catch(Exception exception)
        {
            return "0.0.0.0";
        }
    }

    private static String toString(UID uid)
    {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(14);
        try
        {
            uid.write(((java.io.DataOutput) (new DataOutputStream(((java.io.OutputStream) (bytearrayoutputstream))))));
        }
        catch(IOException ioexception)
        {
            throw new RuntimeException(((Throwable) (ioexception)).toString());
        }
        byte abyte0[] = bytearrayoutputstream.toByteArray();
        return "" + toLong(abyte0, 0, 4) + '.' + toLong(abyte0, 4, 8) + '.' + toLong(abyte0, 12, 2);
    }

    private static long toLong(byte abyte0[], int i, int j)
    {
        long l = 0L;
        for(int k = 0; k < j; k++)
        {
            l <<= 8;
            l |= abyte0[i++] & 0xff;
        }

        return l;
    }

}
