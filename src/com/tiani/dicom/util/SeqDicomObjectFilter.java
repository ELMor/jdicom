// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SeqDicomObjectFilter.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.util:
//            IDicomObjectFilter

public class SeqDicomObjectFilter
    implements IDicomObjectFilter
{

    private int _dname;
    private IDicomObjectFilter _seqFilter;

    public SeqDicomObjectFilter(int i, IDicomObjectFilter idicomobjectfilter)
    {
        _dname = i;
        _seqFilter = idicomobjectfilter;
    }

    public boolean accept(DicomObject dicomobject)
        throws DicomException
    {
        int i = dicomobject.getSize(_dname);
        for(int j = 0; j < i; j++)
            if(_seqFilter.accept((DicomObject)dicomobject.get(_dname)))
                return true;

        return false;
    }
}
