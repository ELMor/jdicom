// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ShortRampFactory.java

package com.tiani.dicom.util;


public final class ShortRampFactory
{

    private static final int CACHE_CAPACITY = 4;
    private static short _cache[][] = new short[4][];
    private static int _writePos = 0;

    public ShortRampFactory()
    {
    }

    public static void init(short aword0[], int i, int j)
    {
        int k = j - i;
        int l = aword0.length - 1;
        if(l == 0)
        {
            aword0[0] = (short)((i + j) / 2);
            return;
        }
        if(k > 0)
        {
            for(int i1 = 0; i1 <= l; i1++)
                aword0[i1] = (short)(i + (k * i1 + (i1 >> 1)) / l);

        } else
        {
            for(int j1 = 0; j1 <= l; j1++)
                aword0[j1] = (short)(i + (k * j1 - (j1 >> 1)) / l);

        }
    }

    public static short[] create(int i, int j, int k)
    {
        short aword0[] = lookupCache(i, j, k);
        if(aword0 != null)
        {
            return aword0;
        } else
        {
            short aword1[] = new short[i];
            init(aword1, j, k);
            updateCache(aword1);
            return aword1;
        }
    }

    public static void setCacheCapacity(int i)
    {
        if(i == _cache.length)
        {
            return;
        } else
        {
            short aword0[][] = new short[i][];
            System.arraycopy(((Object) (_cache)), 0, ((Object) (aword0)), 0, Math.min(_cache.length, i));
            _cache = aword0;
            _writePos = Math.min(_writePos, i - 1);
            return;
        }
    }

    private static void updateCache(short aword0[])
    {
        _cache[_writePos++] = aword0;
        _writePos %= _cache.length;
    }

    private static short[] lookupCache(int i, int j, int k)
    {
        for(int l = 0; l < _cache.length; l++)
        {
            short aword0[] = _cache[l];
            if(aword0 != null && aword0.length == i && (aword0[0] & 0xffff) == j && (aword0[i - 1] & 0xffff) == k)
            {
                _cache[l] = _cache[_writePos];
                updateCache(aword0);
                return aword0;
            }
        }

        return null;
    }

}
