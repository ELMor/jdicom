// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomObjectFilterPath.java

package com.tiani.dicom.util;

import java.util.Vector;

// Referenced classes of package com.tiani.dicom.util:
//            IDicomObjectFilter

/**
 * @deprecated Class DicomObjectFilterPath is deprecated
 */

public class DicomObjectFilterPath extends Vector
{

    public DicomObjectFilterPath()
    {
    }

    public IDicomObjectFilter first()
    {
        return (IDicomObjectFilter)((Vector)this).firstElement();
    }

    public IDicomObjectFilter last()
    {
        return (IDicomObjectFilter)((Vector)this).lastElement();
    }

    public IDicomObjectFilter getAt(int i)
    {
        return (IDicomObjectFilter)((Vector)this).elementAt(i);
    }

    public DicomObjectFilterPath remaining()
    {
        DicomObjectFilterPath dicomobjectfilterpath = (DicomObjectFilterPath)((Vector)this).clone();
        ((Vector) (dicomobjectfilterpath)).removeElementAt(0);
        return dicomobjectfilterpath;
    }
}
