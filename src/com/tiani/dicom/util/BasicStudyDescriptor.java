// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BasicStudyDescriptor.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import java.util.Hashtable;

public class BasicStudyDescriptor extends DicomObject
{

    private Hashtable _refSeries;
    private Hashtable _refImages;
    private String _studyInstanceUID;

    public BasicStudyDescriptor(String s)
        throws DicomException
    {
        _refSeries = new Hashtable();
        _refImages = new Hashtable();
        _studyInstanceUID = null;
        ((DicomObject)this).set(62, "1.2.840.10008.1.9");
        ((DicomObject)this).set(63, ((Object) (s)));
    }

    private void _init(DicomObject dicomobject)
        throws DicomException
    {
        ((DicomObject)this).set(147, dicomobject.get(147));
        ((DicomObject)this).set(148, dicomobject.get(148));
        _studyInstanceUID = dicomobject.getS(425);
        ((DicomObject)this).set(425, ((Object) (_studyInstanceUID)));
        ((DicomObject)this).set(427, dicomobject.get(427));
    }

    public boolean add(DicomObject dicomobject)
        throws DicomException
    {
        return _add(dicomobject, ((DicomObject) (null)), ((DicomObject) (null)));
    }

    public boolean addRetrSeries(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        return _add(dicomobject, dicomobject1, ((DicomObject) (null)));
    }

    public boolean addRetrImage(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        return _add(dicomobject, ((DicomObject) (null)), dicomobject1);
    }

    private boolean _add(DicomObject dicomobject, DicomObject dicomobject1, DicomObject dicomobject2)
        throws DicomException
    {
        String s = dicomobject.getS(63);
        if(_refImages.containsKey(((Object) (s))))
            return false;
        if(_studyInstanceUID == null)
            _init(dicomobject);
        else
        if(!_studyInstanceUID.equals(((Object) (dicomobject.getS(425)))))
            throw new DicomException("BasicStudyDescriptor.add: image belongs to different study");
        String s1 = dicomobject.getS(426);
        DicomObject dicomobject3 = _getRefSeries(dicomobject.getS(426), dicomobject.getS(1207), dicomobject.getS(97), dicomobject1);
        DicomObject dicomobject4 = new DicomObject();
        if(dicomobject2 != null)
            ((GroupList) (dicomobject4)).addGroups(dicomobject2);
        dicomobject4.set(115, ((Object) (dicomobject.getS(62))));
        dicomobject4.set(116, ((Object) (s)));
        dicomobject3.append(113, ((Object) (dicomobject4)));
        _refImages.put(((Object) (s)), ((Object) (dicomobject4)));
        return true;
    }

    private DicomObject _getRefSeries(String s, String s1, String s2, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = (DicomObject)_refSeries.get(((Object) (s)));
        if(dicomobject1 == null)
        {
            dicomobject1 = new DicomObject();
            if(dicomobject != null)
                ((GroupList) (dicomobject1)).addGroups(dicomobject);
            dicomobject1.set(426, ((Object) (s)));
            if(s2 != null)
                dicomobject1.set(97, ((Object) (s2)));
            if(s1 != null)
                dicomobject1.set(1207, ((Object) (s1)));
            ((DicomObject)this).append(109, ((Object) (dicomobject1)));
            _refSeries.put(((Object) (s)), ((Object) (dicomobject1)));
        }
        return dicomobject1;
    }

    private DicomObject _getRefSeries(String s, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = (DicomObject)_refSeries.get(((Object) (s)));
        if(dicomobject1 == null)
        {
            dicomobject1 = new DicomObject();
            if(dicomobject != null)
                ((GroupList) (dicomobject1)).addGroups(dicomobject);
            dicomobject1.set(426, ((Object) (s)));
            ((DicomObject)this).append(109, ((Object) (dicomobject1)));
            _refSeries.put(((Object) (s)), ((Object) (dicomobject1)));
        }
        return dicomobject1;
    }

    public static DicomObject getRetrieveInfo(String s, String s1, String s2)
    {
        try
        {
            DicomObject dicomobject = new DicomObject();
            dicomobject.set(79, ((Object) (s)));
            dicomobject.set(697, ((Object) (s1)));
            dicomobject.set(698, ((Object) (s2)));
            return dicomobject;
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public static DicomObject getRetrieveInfo(String s, String s1)
    {
        try
        {
            DicomObject dicomobject = new DicomObject();
            dicomobject.set(697, ((Object) (s)));
            dicomobject.set(698, ((Object) (s1)));
            return dicomobject;
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }

    public static DicomObject getRetrieveInfo(String s)
    {
        try
        {
            DicomObject dicomobject = new DicomObject();
            dicomobject.set(79, ((Object) (s)));
            return dicomobject;
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).toString());
        }
    }
}
