// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RescalePixelData.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;

// Referenced classes of package com.tiani.dicom.util:
//            PixelMatrix, ShortRampFactory

public class RescalePixelData
{

    private final PixelMatrix pixelMatrix;
    private final short rescaleRows[];
    private final short rescaleColumns[];
    private final int toRows;
    private final int toColumns;

    public RescalePixelData(PixelMatrix pixelmatrix, int i, int j)
        throws IllegalArgumentException, UnsupportedOperationException, DicomException
    {
        pixelMatrix = pixelmatrix;
        if(i < 1 || i > 10000)
        {
            throw new IllegalArgumentException();
        } else
        {
            toRows = i;
            toColumns = j;
            rescaleRows = ShortRampFactory.create(toRows, 0, pixelmatrix.rows - 1);
            rescaleColumns = ShortRampFactory.create(toColumns, 0, pixelmatrix.columns - 1);
            return;
        }
    }

    public byte[] rescale(byte abyte0[])
        throws IndexOutOfBoundsException, IllegalArgumentException
    {
        int i = toRows * pixelMatrix.numberOfFrames;
        abyte0 = checkDest(abyte0, i, toColumns);
        for(int j = 0; j < pixelMatrix.numberOfFrames; j++)
            rescaleFrame(j, abyte0, toRows * j, 0, i, toColumns);

        return abyte0;
    }

    public byte[] rescaleFrame(int i, byte abyte0[])
        throws IndexOutOfBoundsException, IllegalArgumentException
    {
        return rescaleFrame(i, abyte0, 0, 0, toRows, toColumns);
    }

    public byte[] rescaleFrame(int i, byte abyte0[], int j, int k, int l, int i1)
        throws IndexOutOfBoundsException, IllegalArgumentException
    {
        if(i >= pixelMatrix.numberOfFrames)
            throw new IndexOutOfBoundsException();
        if(j < 0 || j + toRows > l)
            throw new IllegalArgumentException();
        if(k < 0 || k + toColumns > i1)
        {
            throw new IllegalArgumentException();
        } else
        {
            abyte0 = checkDest(abyte0, l, i1);
            return pixelMatrix.planarConfiguration != 0 ? pixelMatrix.bytesAllocated != 1 ? rescaleWordDataByPlane(i, abyte0, j, k, l, i1) : rescaleByteDataByPlane(i, abyte0, j, k, l, i1) : rescaleSamplesByPixel(i, abyte0, j, k, l, i1);
        }
    }

    private byte[] checkDest(byte abyte0[], int i, int j)
    {
        int k = i * j * pixelMatrix.bytesAllocated * pixelMatrix.samplesPerPixel;
        if(abyte0 == null)
            abyte0 = new byte[k];
        else
        if(abyte0.length != k)
            throw new IllegalArgumentException("dest.length[" + abyte0.length + "] != totRows[" + i + " * totColumns[" + j + "] * " + pixelMatrix.bytesAllocated * pixelMatrix.samplesPerPixel);
        return abyte0;
    }

    private byte[] rescaleWordDataByPlane(int i, byte abyte0[], int j, int k, int l, int i1)
    {
        int j1 = l * i1 * 2;
        int k1 = pixelMatrix.columns * 2;
        byte abyte1[] = pixelMatrix.pixelData;
        for(int k2 = 0; k2 < pixelMatrix.samplesPerPixel; k2++)
        {
            for(int l2 = 0; l2 < toRows; l2++)
            {
                int l1 = k + (j + l2) * i1 << 1 + k2 * j1;
                int i2 = i * pixelMatrix.frameSize + k2 * pixelMatrix.planeSize + rescaleRows[l2] * k1;
                for(int i3 = 0; i3 < toColumns; i3++)
                {
                    int j2;
                    abyte0[l1++] = abyte1[j2 = i2 + (rescaleColumns[i3] << 1)];
                    abyte0[l1++] = abyte1[++j2];
                }

            }

        }

        return abyte0;
    }

    private byte[] rescaleByteDataByPlane(int i, byte abyte0[], int j, int k, int l, int i1)
    {
        int j1 = l * i1;
        int k1 = pixelMatrix.columns;
        byte abyte1[] = pixelMatrix.pixelData;
        for(int j2 = 0; j2 < pixelMatrix.samplesPerPixel; j2++)
        {
            for(int k2 = 0; k2 < toRows; k2++)
            {
                int l1 = k + (j + k2) * i1 + j2 * j1;
                int i2 = i * pixelMatrix.frameSize + j2 * pixelMatrix.planeSize + rescaleRows[k2] * k1;
                for(int l2 = 0; l2 < toColumns; l2++)
                    abyte0[l1++] = abyte1[i2 + rescaleColumns[l2]];

            }

        }

        return abyte0;
    }

    private byte[] rescaleSamplesByPixel(int i, byte abyte0[], int j, int k, int l, int i1)
    {
        int j1 = pixelMatrix.samplesPerPixel * pixelMatrix.bytesAllocated;
        int k1 = pixelMatrix.columns * j1;
        byte abyte1[] = pixelMatrix.pixelData;
        for(int k2 = 0; k2 < toRows; k2++)
        {
            int l1 = (k + (j + k2) * i1) * j1;
            int i2 = i * pixelMatrix.frameSize + rescaleRows[k2] * k1;
            for(int l2 = 0; l2 < toColumns; l2++)
            {
                int j2 = i2 + rescaleColumns[l2] * j1;
                for(int i3 = j1; i3-- > 0;)
                    abyte0[l1++] = abyte1[j2++];

            }

        }

        return abyte0;
    }
}
