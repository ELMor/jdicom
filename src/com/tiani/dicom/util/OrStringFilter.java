// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   OrStringFilter.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            IStringFilter

public class OrStringFilter
    implements IStringFilter
{

    private IStringFilter filter[];

    public OrStringFilter(IStringFilter aistringfilter[])
    {
        filter = aistringfilter;
    }

    public boolean accept(String s)
    {
        for(int i = 0; i < filter.length; i++)
            if(filter[i].accept(s))
                return true;

        return false;
    }
}
