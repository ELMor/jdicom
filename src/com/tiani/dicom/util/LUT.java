// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   LUT.java

package com.tiani.dicom.util;

import com.archimed.dicom.Debug;

// Referenced classes of package com.tiani.dicom.util:
//            ShortRampFactory, ByteRampFactory

public abstract class LUT {
  public static class Short
      extends LUT {

    int offset;
    int maxY;
    short y0;
    short yN;
    final short data[];

    public String toString() {
      return "ShortLUT: offset=" + offset + ", length=" + data.length +
          ", maxY=" + maxY + dumpdata();
    }

    private String dumpdata() {
      if (Debug.DEBUG < 3) {
        return "";
      }
      int i = Math.min(256, data.length);
      StringBuffer stringbuffer = new StringBuffer();
      stringbuffer.append(", data=");
      for (int j = 0; j < i; j++) {
        stringbuffer.append(data[j] & 0xffff);
        stringbuffer.append('\\');
      }

      if (i < data.length) {
        stringbuffer.append("..");
      }
      return stringbuffer.toString();
    }

    public final int offset() {
      return offset;
    }

    public final void setOffset(int i) {
      offset = i;
    }

    public final int maxY() {
      return maxY;
    }

    public final void setMaxY(int i) {
      maxY = i;
    }

    public final int length() {
      return data.length;
    }

    public final byte lookupByte(int i) {
      return (byte) lookupShort(i);
    }

    public final short lookupShort(int i) {
      return (i -= offset) > 0 ? i < data.length ? data[i] : yN : y0;
    }

    public final int lookupInt(int i) {
      return lookupShort(i) & 0xffff;
    }

    public final void reverse() {
      if (offset != 0) {
        throw new IllegalArgumentException("Cannot reverse LUT with offset " +
                                           offset);
      }
      int i = 0;
      for (int j = data.length; i < j; ) {
        short word0 = data[i];
        data[i++] = data[--j];
        data[j] = word0;
      }

      y0 = data[0];
      yN = data[data.length - 1];
    }

    public final void inverse() {
      for (int i = 0; i < data.length; i++) {
        data[i] = (short) (maxY - (data[i] & 0xffff));

      }
      y0 = data[0];
      yN = data[data.length - 1];
    }

    public final Byte1 rescaleToByte(int i) {
      byte abyte0[] = new byte[data.length];
      int j = LUT.rShift(maxY, i);
      if (j != 0x7fffffff) {
        for (int k = 0; k < data.length; k++) {
          abyte0[k] = (byte) ( (data[k] & 0xffff) >> j);

        }
      }
      else {
        for (int l = 0; l < data.length; l++) {
          abyte0[l] = (byte) ( ( (data[l] & 0xffff) * i) / maxY);

        }
      }
      return new Byte1(offset, abyte0, i);
    }

    public final Short rescaleToShort(int i) {
      int j = LUT.rShift(maxY, i);
      if (j == 0) {
        return this;
      }
      short aword0[] = new short[data.length];
      if (j != 0x7fffffff) {
        if (j > 0) {
          for (int k = 0; k < data.length; k++) {
            aword0[k] = (short) ( (data[k] & 0xffff) >> j);

          }
        }
        else {
          int l = -j;
          for (int j1 = 0; j1 < data.length; j1++) {
            aword0[j1] = (short) ( (data[j1] & 0xffff) << l);

          }
        }
      }
      else {
        for (int i1 = 0; i1 < data.length; i1++) {
          aword0[i1] = (short) ( ( (data[i1] & 0xffff) * i) / maxY);

        }
      }
      return new Short(offset, aword0, i);
    }

    public final LUT rescaleLength(int i) {
      if (offset != 0) {
        throw new IllegalArgumentException(
            "Cannot rescale length of LUT with offset " + offset);
      }
      short aword0[] = new short[i];
      if (data.length % i == 0) {
        int j = data.length / i;
        int l = 0;
        for (int i1 = 0; i1 < i; ) {
          aword0[i1] = data[l];
          i1++;
          l += j;
        }

      }
      else {
        int k = data.length - 1;
        float f = (float) data.length / (float) i;
        for (int k1 = 0; k1 < i; k1++) {
          float f1;
          int j1 = (int) (f1 = (float) k1 * f);
          if (j1 == k) {
            aword0[k1] = data[j1];
          }
          else {
            float f2 = f1 - (float) j1;
            aword0[k1] = (short) (int) ( (float) (data[j1] & 0xffff) *
                                        (1.0F - f2) +
                                        (float) (data[j1 + 1] & 0xffff) * f2);
          }
        }

      }
      return ( (LUT) (new Short(0, aword0, maxY)));
    }

    public Short(int i, short aword0[], int j) {
      offset = i;
      maxY = j;
      data = aword0;
      y0 = aword0[0];
      yN = aword0[aword0.length - 1];
    }

    public Short(int i, short aword0[]) {
      this(i, aword0, 16);
    }

    public Short(int i, byte abyte0[], int j) {
      this(i, LUT.copyByteToShort(abyte0, ( (short[]) (null))), j);
    }

    public Short(int i, int j, int k, int l) {
      this(j, ShortRampFactory.create(i, k, l), l);
    }

    public Short(int i, int j, int k, int l, boolean flag) {
      this(j, ShortRampFactory.create(i, flag ? l : k, flag ? k : l), l);
    }
  }

  public static class Byte1
      extends LUT {

    int offset;
    int maxY;
    byte y0;
    byte yN;
    final byte data[];

    public final int offset() {
      return offset;
    }

    public final void setOffset(int i) {
      offset = i;
    }

    public final int maxY() {
      return maxY;
    }

    public final void setMaxY(int i) {
      maxY = i;
    }

    public String toString() {
      return "ByteLUT: offset=" + offset + ", length=" + data.length +
          ", maxY=" + maxY + dumpdata();
    }

    private String dumpdata() {
      if (Debug.DEBUG < 3) {
        return "";
      }
      int i = Math.min(256, data.length);
      StringBuffer stringbuffer = new StringBuffer();
      stringbuffer.append(", data=");
      for (int j = 0; j < i; j++) {
        stringbuffer.append(data[j] & 0xff);
        stringbuffer.append('\\');
      }

      if (i < data.length) {
        stringbuffer.append("..");
      }
      return stringbuffer.toString();
    }

    public final int length() {
      return data.length;
    }

    public final byte lookupByte(int i) {
      return (i -= offset) > 0 ? i < data.length ? data[i] : yN : y0;
    }

    public final short lookupShort(int i) {
      return (short) lookupInt(i);
    }

    public final int lookupInt(int i) {
      return lookupByte(i) & 0xff;
    }

    public final void reverse() {
      if (offset != 0) {
        throw new IllegalArgumentException("Cannot reverse LUT with offset " +
                                           offset);
      }
      int i = 0;
      for (int j = data.length; i < j; ) {
        byte byte0 = data[i];
        data[i++] = data[--j];
        data[j] = byte0;
      }

      y0 = data[0];
      yN = data[data.length - 1];
    }

    public final void inverse() {
      for (int i = 0; i < data.length; i++) {
        data[i] = (byte) (maxY - (data[i] & 0xff));

      }
      y0 = data[0];
      yN = data[data.length - 1];
    }

    public final Byte1 rescaleToByte(int i) {
      int j = LUT.rShift(maxY, i);
      if (j == 0) {
        return this;
      }
      byte abyte0[] = new byte[data.length];
      if (j != 0x7fffffff) {
        for (int k = 0; k < data.length; k++) {
          abyte0[k] = (byte) ( (data[k] & 0xff) >> j);

        }
      }
      else {
        for (int l = 0; l < data.length; l++) {
          abyte0[l] = (byte) ( ( (data[l] & 0xff) * i) / maxY);

        }
      }
      return new Byte1(offset, abyte0, i);
    }

    public final Short rescaleToShort(int i) {
      short aword0[] = new short[data.length];
      int j = LUT.rShift(maxY, i);
      if (j != 0x7fffffff) {
        for (int k = 0; k < data.length; k++) {
          aword0[k] = (short) ( (data[k] & 0xff) >> j);

        }
      }
      else {
        for (int l = 0; l < data.length; l++) {
          aword0[l] = (short) ( ( (data[l] & 0xff) * i) / maxY);

        }
      }
      return new Short(offset, aword0, i);
    }

    public final LUT rescaleLength(int i) {
      if (offset != 0) {
        throw new IllegalArgumentException(
            "Cannot rescale length of LUT with offset " + offset);
      }
      byte abyte0[] = new byte[i];
      if (data.length % i == 0) {
        int j = data.length / i;
        int l = 0;
        for (int i1 = 0; i1 < i; ) {
          abyte0[i1] = data[l];
          i1++;
          l += j;
        }

      }
      else {
        int k = data.length - 1;
        float f = (float) data.length / (float) i;
        for (int k1 = 0; k1 < i; k1++) {
          float f1;
          int j1 = (int) (f1 = (float) k1 * f);
          if (j1 == k) {
            abyte0[k1] = data[j1];
          }
          else {
            float f2 = f1 - (float) j1;
            abyte0[k1] = (byte) (int) ( (float) (data[j1] & 0xff) * (1.0F - f2) +
                                       (float) (data[j1 + 1] & 0xff) * f2);
          }
        }

      }
      return ( (LUT) (new Byte1(0, abyte0, maxY)));
    }

    public Byte1(int i, byte abyte0[], int j) {
      offset = i;
      maxY = j;
      data = abyte0;
      y0 = abyte0[0];
      yN = abyte0[abyte0.length - 1];
    }

    public Byte1(int i, byte abyte0[]) {
      this(i, abyte0, 8);
    }

    public Byte1(int i, int j, int k, int l) {
      this(j, ByteRampFactory.create(i, k, l), l);
    }

    public Byte1(int i, int j, int k, int l, boolean flag) {
      this(j, ByteRampFactory.create(i, flag ? l : k, flag ? k : l), l);
    }
  }

  static final int MUST_MULTIPLY = 0x7fffffff;

  public LUT() {
  }

  static final int rShift(int i, int j) {
    int k = j - i;
    if (k == 0) {
      return 0;
    }
    int l = (k <= 0 ? j : i) + 1;
    int i1 = (k <= 0 ? i : j) + 1;
    int j1 = 0;
    for (; (i1 & 1) == 0; i1 >>= 1) {
      if (i1 == l) {
        return k <= 0 ? j1 : -j1;
          }
      j1++;
    }

    return 0x7fffffff;
  }

  public abstract int offset();

  public abstract void setOffset(int i);

  public abstract int maxY();

  public abstract void setMaxY(int i);

  public abstract int length();

  public abstract byte lookupByte(int i);

  public abstract short lookupShort(int i);

  public abstract int lookupInt(int i);

  public abstract void inverse();

  public abstract void reverse();

  public abstract Byte1 rescaleToByte(int i);

  public abstract Short rescaleToShort(int i);

  public abstract LUT rescaleLength(int i);

  public static int lookupChain(int i, LUT alut[]) {
    int j = alut.length;
    int k = i;
    for (int l = 0; l < j; l++) {
      LUT lut;
      if ( (lut = alut[l]) != null) {
        k = alut[l].lookupInt(k);
      }
    }

    return k;
  }

  public static LUT[] trimLUTs(LUT alut[]) {
    int i = 0;
    int j;
    for (j = alut.length - 1; i <= j && alut[i] == null; i++) {
      ;
    }
    for (; i <= j && alut[j] == null; j--) {
      ;
    }
    int k = (j - i) + 1;
    if (k == alut.length) {
      return alut;
    }
    else {
      LUT alut1[] = new LUT[k];
      System.arraycopy( ( (Object) (alut)), i, ( (Object) (alut1)), 0, k);
      return alut1;
    }
  }

  public static LUT join(LUT alut[]) {
    alut = trimLUTs(alut);
    switch (alut.length) {
      case 0: // '\0'
        return null;

      case 1: // '\001'
        return alut[0];
    }
    LUT lut = alut[0];
    LUT lut1 = alut[alut.length - 1];
    if (lut1 instanceof Byte1) {
      byte abyte0[] = new byte[lut.length()];
      int i = lut.offset();
      for (int k = 0; k < abyte0.length; ) {
        abyte0[k] = (byte) lookupChain(i, alut);
        k++;
        i++;
      }

      return ( (LUT) (new Byte1(lut.offset(), abyte0, lut1.maxY())));
    }
    short aword0[] = new short[lut.length()];
    int j = lut.offset();
    for (int l = 0; l < aword0.length; ) {
      aword0[l] = (short) lookupChain(j, alut);
      l++;
      j++;
    }

    return ( (LUT) (new Short(lut.offset(), aword0, lut1.maxY())));
  }

  public static LUT createLUT(int i, int j, int k, int l) {
    return ( (l <= 255 ? (LUT)new Byte1(i, j, 0, l) : (LUT)new Short(i, j, 0, l)));
  }

  public static LUT createLUT(int i, int j, int k, int l, boolean flag) {
    return ( (LUT) (l <= 255 ? (LUT)new Byte1(i, j, 0, l, flag) :
                    (LUT)new Short(i, j, 0, l, flag)));
  }

  public static short[] copyByteToShort(byte abyte0[], short aword0[]) {
    if (aword0 == null) {
      aword0 = new short[abyte0.length / 2];
    }
    int i = 0;
    for (int j = 0; j < aword0.length; ) {
      aword0[j] = (short) (abyte0[i] & 0xff | abyte0[i + 1] << 8);
      j++;
      i++;
      i++;
    }

    return aword0;
  }
}
