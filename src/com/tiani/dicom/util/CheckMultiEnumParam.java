// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;

import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.util:
//            CheckParam, CheckEnumParam, DefinedParam

class CheckMultiEnumParam extends CheckParam
{

    private final CheckEnumParam _checkEnum;

    public CheckMultiEnumParam(String as[])
    {
        super(3);
        _checkEnum = new CheckEnumParam(as, 1);
    }

    public String[] getTags()
    {
        return ((DefinedParam) (_checkEnum)).getTags();
    }

    public boolean isMulti()
    {
        return true;
    }

    public void check(String s)
        throws IllegalArgumentException
    {
        if(s == null || s.length() == 0)
            return;
        for(StringTokenizer stringtokenizer = new StringTokenizer(s, " ,/\\"); stringtokenizer.hasMoreTokens(); _checkEnum.check(stringtokenizer.nextToken()));
    }
}
