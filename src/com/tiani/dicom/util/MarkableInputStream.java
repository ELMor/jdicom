// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MarkableInputStream.java

package com.tiani.dicom.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarkableInputStream extends FilterInputStream
{

    private byte _buffer[];
    private int _pos;
    private int _len;

    public MarkableInputStream(InputStream inputstream)
    {
        super(inputstream);
        _buffer = null;
        _pos = 0;
        _len = 0;
    }

    public final int available()
        throws IOException
    {
        return (_len - _pos) + super.in.available();
    }

    public final void mark(int i)
    {
        if(super.in.markSupported())
        {
            super.in.mark(i);
            return;
        }
        int j = _len - _pos;
        byte abyte0[] = new byte[Math.max(i, j)];
        if(_buffer != null && j > 0)
            System.arraycopy(((Object) (_buffer)), _pos, ((Object) (abyte0)), 0, j);
        _pos = 0;
        _len = j;
        _buffer = abyte0;
    }

    public final boolean markSupported()
    {
        return true;
    }

    public final int read()
        throws IOException
    {
        if(_buffer == null)
            return super.in.read();
        if(_pos < _len)
            return ((int) (_buffer[_pos++]));
        if(_len < _buffer.length)
        {
            if(super.in.read(_buffer, _pos, 1) == -1)
            {
                return -1;
            } else
            {
                _len++;
                return ((int) (_buffer[_pos++]));
            }
        } else
        {
            _buffer = null;
            _pos = 0;
            _len = 0;
            return super.in.read();
        }
    }

    public final int read(byte abyte0[], int i, int j)
        throws IOException
    {
        if(_buffer == null)
            return super.in.read(abyte0, i, j);
        int k = _len - _pos;
        int l = j - k;
        if(l > 0)
        {
            if(l > _buffer.length - _len)
            {
                System.arraycopy(((Object) (_buffer)), _pos, ((Object) (abyte0)), i, k);
                _buffer = null;
                _pos = 0;
                _len = 0;
                int i1;
                if((i1 = super.in.read(abyte0, i + k, j - k)) == -1)
                    return -1;
                else
                    return k + i1;
            }
            int j1;
            if((j1 = super.in.read(_buffer, _len, l)) == -1)
                return -1;
            _len += j1;
        }
        int k1 = Math.min(j, _len - _pos);
        System.arraycopy(((Object) (_buffer)), _pos, ((Object) (abyte0)), i, k1);
        _pos += k1;
        return k1;
    }

    public final int read(byte abyte0[])
        throws IOException
    {
        return read(abyte0, 0, abyte0.length);
    }

    public final void reset()
        throws IOException
    {
        if(super.in.markSupported())
        {
            super.in.reset();
            return;
        }
        if(_buffer == null)
        {
            throw new IOException("Resetting to invalid mark");
        } else
        {
            _pos = 0;
            return;
        }
    }

    public final long skip(long l)
        throws IOException
    {
        if(_buffer == null)
            return super.in.skip(l);
        int i = _len - _pos;
        if(l < (long)i)
        {
            _pos += ((int) (l));
            return l;
        } else
        {
            _buffer = null;
            _pos = 0;
            _len = 0;
            return (long)i + super.in.skip(l - (long)i);
        }
    }
}
