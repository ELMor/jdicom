// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            CheckParam

class CheckDoubleParam extends CheckParam
{

    private double _min;
    private double _max;

    public CheckDoubleParam(double d, double d1, int i)
    {
        super(i);
        if(d > d1)
        {
            throw new IllegalArgumentException("not a valid intervall");
        } else
        {
            _min = d;
            _max = d1;
            return;
        }
    }

    public void check(String s)
        throws IllegalArgumentException
    {
        super.check(s);
        if(s == null || s.length() == 0)
            return;
        double d = Double.parseDouble(s);
        if(d < _min || d > _max)
            throw new IllegalArgumentException("outside [" + _min + ',' + _max + ']');
        else
            return;
    }
}
