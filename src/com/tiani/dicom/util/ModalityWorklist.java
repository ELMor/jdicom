// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ModalityWorklist.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public final class ModalityWorklist
{

    public ModalityWorklist()
    {
    }

    public static void initSPS(DicomObject dicomobject, String s, String s1, String s2, String s3, String s4, String s5)
        throws DicomException
    {
        dicomobject.set(582, ((Object) (s)));
        dicomobject.set(575, ((Object) (s1)));
        dicomobject.set(576, ((Object) (s2)));
        dicomobject.set(81, ((Object) (s3)));
        dicomobject.set(574, ((Object) (s4)));
        dicomobject.set(580, ((Object) (s5)));
    }

    public static DicomObject createSPS(String s, String s1, String s2, String s3, String s4, String s5)
        throws DicomException
    {
        DicomObject dicomobject = new DicomObject();
        initSPS(dicomobject, s, s1, s2, s3, s4, s5);
        return dicomobject;
    }

    public static void initItem(DicomObject dicomobject, String s, String s1, String s2, String s3, String s4, String s5, DicomObject dicomobject1)
        throws DicomException
    {
        dicomobject.set(147, ((Object) (s)));
        dicomobject.set(148, ((Object) (s1)));
        dicomobject.set(77, ((Object) (s2)));
        dicomobject.set(425, ((Object) (s3)));
        dicomobject.set(589, ((Object) (s4)));
        dicomobject.set(547, ((Object) (s5)));
        dicomobject.set(587, ((Object) (dicomobject1)));
    }

    public static DicomObject createItem(String s, String s1, String s2, String s3, String s4, String s5, DicomObject dicomobject)
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        initItem(dicomobject1, s, s1, s2, s3, s4, s5, dicomobject);
        return dicomobject1;
    }
}
