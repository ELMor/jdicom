// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Tag.java

package com.tiani.dicom.util;

import com.archimed.dicom.DDict;

// Referenced classes of package com.tiani.dicom.util:
//            ExtDDict

public final class Tag
{

    static ExtDDict extDDict = new ExtDDict();
    private int _gr;
    private int _el;
    private Tag _sqTags[];
    private int _typeCode;
    private String _tagPrompt;
    private String _typePrompt;
    private String _descPrompt;

    public Tag(int i, int j)
    {
        _sqTags = null;
        _gr = i;
        _el = j;
        _tagPrompt = '(' + _toHexString(_gr) + ',' + _toHexString(_el) + ')';
        _typeCode = DDict.getTypeCode(i, j);
        _typePrompt = vrPrompt(_typeCode);
        _descPrompt = (_gr & 1) == 0 ? DDict.getDescription(DDict.lookupDDict(_gr, _el)) : "Proprietary Tag";
    }

    public Tag(int i, int j, Tag atag[])
    {
        this(i, j);
        _sqTags = atag;
    }

    public int getGroup()
    {
        return _gr;
    }

    public int getElement()
    {
        return _el;
    }

    public Tag[] getSQTags()
    {
        return _sqTags;
    }

    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != (com.tiani.dicom.util.Tag.class))
        {
            return false;
        } else
        {
            Tag tag = (Tag)obj;
            return _gr == tag._gr && _el == tag._el;
        }
    }

    public int hashCode()
    {
        return (_gr << 16) + _el;
    }

    public String toString()
    {
        return _tagPrompt;
    }

    public int compareTo(Tag tag)
    {
        if(this == tag)
            return 0;
        else
            return hashCode() - tag.hashCode();
    }

    public int getTypeCode()
    {
        return _typeCode;
    }

    public boolean isTypeSQ()
    {
        return _typeCode == 10;
    }

    public String getTypePrompt()
    {
        return _typePrompt;
    }

    public String getDescription()
    {
        return _descPrompt;
    }

    private static String _toHexString(int i)
    {
        StringBuffer stringbuffer;
        for(stringbuffer = new StringBuffer(Integer.toHexString(i).toUpperCase()); stringbuffer.length() < 4; stringbuffer.insert(0, '0'));
        return stringbuffer.toString();
    }

    public static String vrPrompt(int i)
    {
        switch(i)
        {
        case 4: // '\004'
            return "AE";

        case 17: // '\021'
            return "AS";

        case 5: // '\005'
            return "AT";

        case 9: // '\t'
            return "CS";

        case 11: // '\013'
            return "DA";

        case 16: // '\020'
            return "DS";

        case 28: // '\034'
            return "DT";

        case 20: // '\024'
            return "FD";

        case 26: // '\032'
            return "FL";

        case 15: // '\017'
            return "IS";

        case 6: // '\006'
            return "LO";

        case 18: // '\022'
            return "LT";

        case 8: // '\b'
            return "OB";

        case 24: // '\030'
            return "OW";

        case 14: // '\016'
            return "PN";

        case 7: // '\007'
            return "SH";

        case 19: // '\023'
            return "SL";

        case 10: // '\n'
            return "SQ";

        case 23: // '\027'
            return "SS";

        case 13: // '\r'
            return "ST";

        case 12: // '\f'
            return "TM";

        case 2: // '\002'
            return "UI";

        case 0: // '\0'
            return "UN";

        case 1: // '\001'
            return "UL";

        case 3: // '\003'
            return "US";

        case 27: // '\033'
            return "UT";

        case 25: // '\031'
            return "NONE";

        case 21: // '\025'
            return "US|SS";

        case 22: // '\026'
            return "OW|OB";
        }
        return null;
    }

}
