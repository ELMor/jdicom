// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ByteRampFactory.java

package com.tiani.dicom.util;


public final class ByteRampFactory
{

    private static final int CACHE_CAPACITY = 4;
    private static byte _cache[][] = new byte[4][];
    private static int _writePos = 0;

    public ByteRampFactory()
    {
    }

    public static void init(byte abyte0[], int i, int j)
    {
        int k = j - i;
        int l = abyte0.length - 1;
        if(l == 0)
        {
            abyte0[0] = (byte)((i + j) / 2);
            return;
        }
        if(k > 0)
        {
            for(int i1 = 0; i1 <= l; i1++)
                abyte0[i1] = (byte)(i + (k * i1 + (i1 >> 1)) / l);

        } else
        {
            for(int j1 = 0; j1 <= l; j1++)
                abyte0[j1] = (byte)(i + (k * j1 - (j1 >> 1)) / l);

        }
    }

    public static byte[] create(int i, int j, int k)
    {
        byte abyte0[] = lookupCache(i, j, k);
        if(abyte0 != null)
        {
            return abyte0;
        } else
        {
            byte abyte1[] = new byte[i];
            init(abyte1, j, k);
            updateCache(abyte1);
            return abyte1;
        }
    }

    public static void setCacheCapacity(int i)
    {
        if(i == _cache.length)
        {
            return;
        } else
        {
            byte abyte0[][] = new byte[i][];
            System.arraycopy(((Object) (_cache)), 0, ((Object) (abyte0)), 0, Math.min(_cache.length, i));
            _cache = abyte0;
            _writePos = Math.min(_writePos, i - 1);
            return;
        }
    }

    private static void updateCache(byte abyte0[])
    {
        _cache[_writePos++] = abyte0;
        _writePos %= _cache.length;
    }

    private static byte[] lookupCache(int i, int j, int k)
    {
        for(int l = 0; l < _cache.length; l++)
        {
            byte abyte0[] = _cache[l];
            if(abyte0 != null && abyte0.length == i && (abyte0[0] & 0xff) == j && (abyte0[i - 1] & 0xff) == k)
            {
                _cache[l] = _cache[_writePos];
                updateCache(abyte0);
                return abyte0;
            }
        }

        return null;
    }

}
