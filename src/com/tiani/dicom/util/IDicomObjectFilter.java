// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IDicomObjectFilter.java

package com.tiani.dicom.util;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public interface IDicomObjectFilter
{

    public abstract boolean accept(DicomObject dicomobject)
        throws DicomException;
}
