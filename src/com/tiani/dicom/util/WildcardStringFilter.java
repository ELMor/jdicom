// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   WildcardStringFilter.java

package com.tiani.dicom.util;

import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.util:
//            IStringFilter

public class WildcardStringFilter
    implements IStringFilter
{

    private String _prefix;
    private String _middle[];
    private String _suffix;

    public WildcardStringFilter(String s)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, "*");
        int i = stringtokenizer.countTokens();
        if(i > 0)
        {
            if(s.charAt(0) != '*')
            {
                _prefix = stringtokenizer.nextToken();
                i--;
            }
            boolean flag = s.charAt(s.length() - 1) != '*';
            if(flag)
                i--;
            if(i > 0)
            {
                _middle = new String[i];
                for(int j = 0; j < i; j++)
                    _middle[j] = stringtokenizer.nextToken();

            }
            if(flag)
                _suffix = i >= 0 ? stringtokenizer.nextToken() : _prefix;
        }
    }

    public boolean accept(String s)
    {
        int i = 0;
        if(_prefix != null && (i = _prefix.length()) != 0 && !_match(s, _prefix))
            return false;
        int j = s.length();
        int k = 0;
        if(_suffix != null && (k = _suffix.length()) != 0 && ((j -= k) < 0 || !_match(s.substring(j), _suffix)))
            return false;
        if(_middle != null)
        {
            for(int l = 0; l < _middle.length; l++)
            {
                String s1 = _middle[l];
                int i1 = s1.length();
                int j1 = j - (i + i1);
                do
                    if(j1-- < 0)
                        return false;
                while(!_match(s.substring(i++), s1));
                i += i1 - 1;
            }

        }
        return true;
    }

    private static boolean _match(String s, String s1)
    {
        int i;
        if(s1 == null || (i = s1.length()) == 0)
            return true;
        if(s.length() < i)
            return false;
        for(int j = 0; j < i; j++)
        {
            char c = s1.charAt(j);
            if(c != '?' && c != s.charAt(j))
                return false;
        }

        return true;
    }
}
