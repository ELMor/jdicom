// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefDicomObjectFilter.java

package com.tiani.dicom.util;

import com.archimed.dicom.DDate;
import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDateTime;
import com.archimed.dicom.DDateTimeRange;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DTime;
import com.archimed.dicom.DTimeRange;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.TagValue;
import java.util.Enumeration;

// Referenced classes of package com.tiani.dicom.util:
//            RelationStringFilter, WildcardStringFilter, AndStringFilter, IStringFilter, 
//            IDicomObjectFilter

public class DefDicomObjectFilter
    implements IDicomObjectFilter
{

    private int _dname;
    private IStringFilter _strFilter;

    public DefDicomObjectFilter(int i, IStringFilter istringfilter)
    {
        _dname = i;
        _strFilter = istringfilter;
    }

    public boolean accept(DicomObject dicomobject)
        throws DicomException
    {
        int i = dicomobject.getSize(_dname);
        if(i <= 0)
            return true;
        for(int j = 0; j < i; j++)
            if(_strFilter.accept(dicomobject.getS(_dname)))
                return true;

        return false;
    }

    public static IDicomObjectFilter getSingleValueMatching(int i, String s)
    {
        return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(s, 0))))));
    }

    public static IDicomObjectFilter getWildCardMatching(int i, String s)
    {
        if(s.indexOf('*') == -1 && s.indexOf('?') == -1)
            return getSingleValueMatching(i, s);
        else
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new WildcardStringFilter(s))))));
    }

    public static IDicomObjectFilter getDateRangeMatching(int i, DDateRange ddaterange)
    {
        DDate ddate = ddaterange.getDate1();
        DDate ddate1 = ddaterange.getDate2();
        if(ddate != null && ddate1 != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new AndStringFilter(new IStringFilter[] {
                new RelationStringFilter(ddate.toDICOMString(), 1), new RelationStringFilter(ddate1.toDICOMString(), 3)
            }))))));
        if(ddate != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(ddate.toDICOMString(), 1))))));
        else
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(ddate1.toDICOMString(), 3))))));
    }

    public static IDicomObjectFilter getDateTimeRangeMatching(int i, DDateTimeRange ddatetimerange)
    {
        DDateTime ddatetime = ddatetimerange.getDateTime1();
        DDateTime ddatetime1 = ddatetimerange.getDateTime2();
        if(ddatetime != null && ddatetime1 != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new AndStringFilter(new IStringFilter[] {
                new RelationStringFilter(ddatetime.toDICOMString(), 1), new RelationStringFilter(ddatetime1.toDICOMString(), 3)
            }))))));
        if(ddatetime != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(ddatetime.toDICOMString(), 1))))));
        else
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(ddatetime1.toDICOMString(), 3))))));
    }

    public static IDicomObjectFilter getTimeRangeMatching(int i, DTimeRange dtimerange)
    {
        DTime dtime = dtimerange.getTime1();
        DTime dtime1 = dtimerange.getTime2();
        if(dtime != null && dtime1 != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new AndStringFilter(new IStringFilter[] {
                new RelationStringFilter(dtime.toDICOMString(), 1), new RelationStringFilter(dtime1.toDICOMString(), 3)
            }))))));
        if(dtime != null)
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(dtime.toDICOMString(), 1))))));
        else
            return ((IDicomObjectFilter) (new DefDicomObjectFilter(i, ((IStringFilter) (new RelationStringFilter(dtime1.toDICOMString(), 3))))));
    }

    public static DicomObject createReturn(DicomObject dicomobject, DicomObject dicomobject1)
        throws DicomException
    {
        return createReturn(dicomobject, new DicomObject[] {
            dicomobject1
        });
    }

    public static DicomObject createReturn(DicomObject dicomobject, DicomObject adicomobject[])
        throws DicomException
    {
        DicomObject dicomobject1 = new DicomObject();
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements();)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            int i = tagvalue.getGroup();
            int j = tagvalue.getElement();
            dicomobject1.set_ge(i, j, ((Object) (null)));
            DicomObject dicomobject2 = tagvalue.size() <= 0 || DDict.getTypeCode(i, j) != 10 ? null : (DicomObject)tagvalue.getValue(0);
            for(int k = 0; k < adicomobject.length; k++)
            {
                DicomObject dicomobject3 = adicomobject[k];
                int l = dicomobject3.getSize_ge(i, j);
                for(int i1 = 0; i1 < l; i1++)
                {
                    Object obj = dicomobject3.get_ge(i, j, i1);
                    dicomobject1.append_ge(i, j, dicomobject2 == null ? obj : ((Object) (createReturn(dicomobject2, (DicomObject)obj))));
                }

                if(dicomobject3.getSize(57) == 1)
                    dicomobject1.set(57, dicomobject3.get(57));
            }

        }

        return dicomobject1;
    }
}
