// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AETPermission.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            AET

public class AETPermission extends AET
{

    public boolean permit_store;
    public boolean permit_wl;
    public boolean permit_find;
    public boolean permit_move;

    public AETPermission(String s, String s1, int i)
    {
        super(s, s1, i);
        permit_store = false;
        permit_wl = false;
        permit_find = false;
        permit_move = false;
    }

    public void setStore(boolean flag)
    {
        permit_store = flag;
    }

    public boolean isStore()
    {
        return permit_store;
    }

    public void setWL(boolean flag)
    {
        permit_wl = flag;
    }

    public boolean isWL()
    {
        return permit_wl;
    }

    public void setFind(boolean flag)
    {
        permit_find = flag;
    }

    public boolean isFind()
    {
        return permit_find;
    }

    public void setMove(boolean flag)
    {
        permit_move = flag;
    }

    public boolean isMove()
    {
        return permit_move;
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + "[title=" + super.title + ",host=" + super.host + ",port=" + super.port + ",permit store=" + permit_store + ",permit wl=" + permit_wl + ",permit find=" + permit_find + ", permit_move=" + permit_move + "]";
    }
}
