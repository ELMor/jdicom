// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AET.java

package com.tiani.dicom.util;


public class AET
{

    public final String title;
    public final String host;
    public final int port;
    private final int hash;

    public AET(String s, String s1, int i)
    {
        title = s;
        host = s1;
        port = i;
        hash = s.hashCode() + s1.hashCode() + i;
    }

    public int hashCode()
    {
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null || !(obj instanceof AET))
        {
            return false;
        } else
        {
            AET aet = (AET)obj;
            return title.equals(((Object) (aet.title))) && host.equals(((Object) (aet.host))) && port == aet.port;
        }
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + "[title=" + title + ",host=" + host + ",port=" + port + "]";
    }
}
