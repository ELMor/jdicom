// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;

import java.net.MalformedURLException;
import java.net.URL;

// Referenced classes of package com.tiani.dicom.util:
//            CheckParam

class URLParam extends CheckParam
{

    public URLParam(int i)
    {
        super(i);
    }

    public void check(String s)
    {
        super.check(s);
        if(s == null || s.length() == 0)
            return;
        try
        {
            new URL(s);
        }
        catch(MalformedURLException malformedurlexception)
        {
            throw new IllegalArgumentException(((Throwable) (malformedurlexception)).toString());
        }
    }
}
