// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   PixelMatrix.java

package com.tiani.dicom.util;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.io.PrintStream;
import java.math.BigInteger;

// Referenced classes of package com.tiani.dicom.util:
//            PaletteColorLUTs, RescalePixelData, EnumPMI, LUT

public abstract class PixelMatrix
{
    public static class SignedShort extends PixelMatrix
    {

        protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            return ((PixelMatrix) (new SignedShort(pixelmatrix, i, j, k, l, abyte0)));
        }

        public int[] calcMinMax()
        {
            int i = 0x7fffffff;
            int j = 0x80000000;
            for(int l = 0; l < numberOfSamples; l++)
            {
                int k = getSample(l);
                i = Math.min(k, i);
                j = Math.max(k, j);
            }

            return (new int[] {
                i, j
            });
        }

        public final int getSample(int i)
        {
            int j = i << 1;
            int k = (pixelData[j] & 0xff | pixelData[j + 1] << 8) >> shift & bitmask;
            return (k & minVal) != 0 ? k | minVal : k;
        }

        protected void rgbPixelToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void rgbPlaneToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void paletteColorToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void doPreformatGrayscale(LUT.Byte1 byte1, byte abyte0[])
        {
            for(int i = 0; i < numberOfSamples; i++)
                abyte0[i] = byte1.lookupByte(getSample(i));

        }

        public BufferedImage createBufferedImage(int i)
        {
            throw new UnsupportedOperationException("Cannot create BufferedImage for " + this);
        }

        public String toString()
        {
            return "Signed Short" + toString();
        }

        public SignedShort(DicomObject dicomobject)
            throws DicomException
        {
            super(dicomobject);
            maxVal = bitmask >> 1;
            minVal = ~maxVal;
        }

        private SignedShort(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            super(pixelmatrix, i, j, k, l, abyte0);
        }
    }

    public static class UnsignedShort extends PixelMatrix
    {

        protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            return ((PixelMatrix) (new UnsignedShort(pixelmatrix, i, j, k, l, abyte0)));
        }

        public int[] calcMinMax()
        {
            int i = 0x7fffffff;
            int j = 0x80000000;
            for(int l = 0; l < numberOfSamples; l++)
            {
                int k = getSample(l);
                i = Math.min(k, i);
                j = Math.max(k, j);
            }

            return (new int[] {
                i, j
            });
        }

        public final int getSample(int i)
        {
            int j = i << 1;
            return (pixelData[j] & 0xff | pixelData[j + 1] << 8) >> shift & bitmask;
        }

        protected void rgbPixelToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void rgbPlaneToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void paletteColorToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void doPreformatGrayscale(LUT.Byte1 byte1, byte abyte0[])
        {
            for(int i = 0; i < numberOfSamples; i++)
                abyte0[i] = byte1.lookupByte(getSample(i));

        }

        public BufferedImage createBufferedImage(int i)
        {
            if(!isPaletteColor())
                throw new UnsupportedOperationException("Cannot create BufferedImage for " + this);
            DataBufferUShort databufferushort = new DataBufferUShort(pixelsPerFrame);
            short aword0[] = databufferushort.getData();
            int j = 0;
            for(int k = pixelsPerFrame * i; j < pixelsPerFrame; k++)
            {
                aword0[j] = (short)getSample(k);
                j++;
            }

            java.awt.image.WritableRaster writableraster = Raster.createInterleavedRaster(((java.awt.image.DataBuffer) (databufferushort)), columns, rows, columns, 1, PixelMatrix.INT0, ((java.awt.Point) (null)));
            IndexColorModel indexcolormodel = new IndexColorModel(bitsStored, redPalette.length, redPalette, greenPalette, bluePalette);
            return new BufferedImage(((ColorModel) (indexcolormodel)), writableraster, true, ((java.util.Hashtable) (null)));
        }

        public String toString()
        {
            return "Unsigned Short" + toString();
        }

        public UnsignedShort(DicomObject dicomobject)
            throws DicomException
        {
            super(dicomobject);
            minVal = 0;
            maxVal = bitmask;
        }

        private UnsignedShort(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            super(pixelmatrix, i, j, k, l, abyte0);
        }
    }

    public static class SignedByte extends PixelMatrix
    {

        protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            return ((PixelMatrix) (new SignedByte(pixelmatrix, i, j, k, l, abyte0)));
        }

        public final int getSample(int i)
        {
            int j = (pixelData[i] & 0xff) >> shift & bitmask;
            return (j & minVal) != 0 ? j | minVal : j;
        }

        public int[] calcMinMax()
        {
            int i = 0x7fffffff;
            int j = 0x80000000;
            for(int l = 0; l < numberOfSamples; l++)
            {
                int k = getSample(l);
                i = Math.min(k, i);
                j = Math.max(k, j);
            }

            return (new int[] {
                i, j
            });
        }

        protected void paletteColorToGrayscale(byte abyte0[])
        {
            throw new UnsupportedOperationException();
        }

        protected void rgbPixelToGrayscale(byte abyte0[])
        {
            int i = 0;
            for(int j = 0; j < abyte0.length; j++)
                abyte0[j] = (byte)((getSample(i++) + getSample(i++) + getSample(i++)) / 3);

        }

        protected void rgbPlaneToGrayscale(byte abyte0[])
        {
            int i = 0;
            for(int i1 = 0; i1 < numberOfFrames; i1++)
            {
                int j = i1 * frameSize;
                int k = j + planeSize;
                int l = k + planeSize;
                for(int j1 = 0; j1 < planeSize; j1++)
                    abyte0[i++] = (byte)((getSample(j++) + getSample(k++) + getSample(l++)) / 3);

            }

        }

        protected void doPreformatGrayscale(LUT.Byte1 byte1, byte abyte0[])
        {
            for(int i = 0; i < numberOfSamples; i++)
                abyte0[i] = byte1.lookupByte(getSample(i));

        }

        public BufferedImage createBufferedImage(int i)
        {
            throw new UnsupportedOperationException("Cannot create BufferedImage for " + this);
        }

        public String toString()
        {
            return "Signed Byte" + toString();
        }

        public SignedByte(DicomObject dicomobject)
            throws DicomException
        {
            super(dicomobject);
            maxVal = bitmask >> 1;
            minVal = ~maxVal;
        }

        private SignedByte(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            super(pixelmatrix, i, j, k, l, abyte0);
        }
    }

    public static class UnsignedByte extends PixelMatrix
    {

        protected PixelMatrix clone(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            return ((PixelMatrix) (new UnsignedByte(pixelmatrix, i, j, k, l, abyte0)));
        }

        public final int getSample(int i)
        {
            return (pixelData[i] & 0xff) >> shift & bitmask;
        }

        public int[] calcMinMax()
        {
            int i = 0x7fffffff;
            int j = 0x80000000;
            for(int l = 0; l < numberOfSamples; l++)
            {
                int k = getSample(l);
                i = Math.min(k, i);
                j = Math.max(k, j);
            }

            return (new int[] {
                i, j
            });
        }

        protected void paletteColorToGrayscale(byte abyte0[])
        {
            for(int j = 0; j < abyte0.length; j++)
            {
                int i = getSample(j);
                abyte0[j] = (byte)(((redPalette[i] & 0xff) + (greenPalette[i] & 0xff) + (bluePalette[i] & 0xff)) / 3);
            }

        }

        protected void rgbPixelToGrayscale(byte abyte0[])
        {
            int i = 0;
            for(int j = 0; j < abyte0.length; j++)
                abyte0[j] = (byte)((getSample(i++) + getSample(i++) + getSample(i++)) / 3);

        }

        protected void rgbPlaneToGrayscale(byte abyte0[])
        {
            int i = 0;
            for(int i1 = 0; i1 < numberOfFrames; i1++)
            {
                int j = i1 * frameSize;
                int k = j + planeSize;
                int l = k + planeSize;
                for(int j1 = 0; j1 < planeSize; j1++)
                    abyte0[i++] = (byte)((getSample(j++) + getSample(k++) + getSample(l++)) / 3);

            }

        }

        protected void doPreformatGrayscale(LUT.Byte1 byte1, byte abyte0[])
        {
            for(int i = 0; i < numberOfSamples; i++)
                abyte0[i] = byte1.lookupByte(getSample(i));

        }

        public BufferedImage createBufferedImage(int i)
        {
            DataBufferByte databufferbyte = new DataBufferByte(pixelData, frameSize, frameSize * i);
            Object obj;
            java.awt.image.WritableRaster writableraster;
            if(isMonochrome())
            {
                writableraster = Raster.createBandedRaster(((java.awt.image.DataBuffer) (databufferbyte)), columns, rows, columns, PixelMatrix.INT0, PixelMatrix.INT0, ((java.awt.Point) (null)));
                obj = ((Object) (PixelMatrix.GRAY8_COLORMODEL));
            } else
            if(isRGB())
            {
                if(isColorByPlane())
                    writableraster = Raster.createBandedRaster(((java.awt.image.DataBuffer) (databufferbyte)), columns, rows, columns, PixelMatrix.INT000, new int[] {
                        0, planeSize, planeSize << 1
                    }, ((java.awt.Point) (null)));
                else
                    writableraster = Raster.createInterleavedRaster(((java.awt.image.DataBuffer) (databufferbyte)), columns, rows, columns * 3, 3, PixelMatrix.INT012, ((java.awt.Point) (null)));
                obj = ((Object) (PixelMatrix.RGB_COLORMODEL));
            } else
            if(isPaletteColor())
            {
                writableraster = Raster.createBandedRaster(((java.awt.image.DataBuffer) (databufferbyte)), columns, rows, columns, PixelMatrix.INT0, PixelMatrix.INT0, ((java.awt.Point) (null)));
                obj = ((Object) (new IndexColorModel(8, redPalette.length, redPalette, greenPalette, bluePalette)));
            } else
            {
                throw new UnsupportedOperationException("Cannot create BufferedImage for " + this);
            }
            return new BufferedImage(((ColorModel) (obj)), writableraster, true, ((java.util.Hashtable) (null)));
        }

        public String toString()
        {
            return "Unsigned Byte" + toString();
        }

        public UnsignedByte(DicomObject dicomobject)
            throws DicomException
        {
            super(dicomobject);
            minVal = 0;
            maxVal = bitmask;
        }

        private UnsignedByte(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
        {
            super(pixelmatrix, i, j, k, l, abyte0);
        }

        private UnsignedByte(PixelMatrix pixelmatrix, byte abyte0[])
        {
            super(pixelmatrix, abyte0);
        }

    }

    public static class Dimension
    {

        public final int rows;
        public final int columns;

        public Dimension(int i, int j)
        {
            rows = i;
            columns = j;
        }
    }


    final int rows;
    final int columns;
    final int samplesPerPixel;
    final int numberOfSamples;
    final String photometricInterpretation;
    final int pmiConst;
    final int planarConfiguration;
    final int numberOfFrames;
    final int representativeFrameNumber;
    final int bytesAllocated;
    final int pixelsPerFrame;
    final int planeSize;
    final int frameSize;
    final int bitsStored;
    final int shift;
    final int bitmask;
    final int aspectRatio[];
    final float pixelSpacing[];
    final byte pixelData[];
    final byte redPalette[];
    final byte greenPalette[];
    final byte bluePalette[];
    int minVal;
    int maxVal;
    private static final ColorModel GRAY8_COLORMODEL = new ComponentColorModel(ColorSpace.getInstance(1003), new int[] {
        8
    }, false, false, 1, 0);
    private static final ColorModel RGB_COLORMODEL = new ComponentColorModel(ColorSpace.getInstance(1000), new int[] {
        8, 8, 8
    }, false, false, 1, 0);
    private static final int INT0[] = {
        0
    };
    private static final int INT000[] = {
        0, 0, 0
    };
    private static final int INT012[] = {
        0, 1, 2
    };

    public static PixelMatrix create(DicomObject dicomobject)
        throws DicomException
    {
        int i = getI(dicomobject, 478);
        int j = getI(dicomobject, 475);
        switch(j)
        {
        case 8: // '\b'
            return ( (i != 0 ? (PixelMatrix)new SignedByte(dicomobject) : (PixelMatrix)new UnsignedByte(dicomobject)));

        case 16: // '\020'
            return ((PixelMatrix) (i != 0 ? (PixelMatrix)new SignedShort(dicomobject) : (PixelMatrix)new UnsignedShort(dicomobject)));
        }
        throw new UnsupportedOperationException("" + j + " bits allocated not supported");
    }

    static int toEvenLength(int i)
    {
        return i + 1 & -2;
    }

    protected PixelMatrix(PixelMatrix pixelmatrix, byte abyte0[])
    {
        pixelData = abyte0;
        rows = pixelmatrix.rows;
        columns = pixelmatrix.columns;
        samplesPerPixel = 1;
        photometricInterpretation = "MONOCHROME2";
        pmiConst = 1;
        bytesAllocated = 1;
        planarConfiguration = 1;
        pixelsPerFrame = pixelmatrix.pixelsPerFrame;
        planeSize = pixelsPerFrame * bytesAllocated;
        frameSize = planeSize * samplesPerPixel;
        numberOfFrames = pixelmatrix.numberOfFrames;
        representativeFrameNumber = pixelmatrix.representativeFrameNumber;
        if(toEvenLength(abyte0.length) != toEvenLength(frameSize * numberOfFrames))
        {
            throw new RuntimeException("mismatch length of pixel pixelData");
        } else
        {
            numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
            bitsStored = 8;
            bitmask = 255;
            shift = 0;
            aspectRatio = pixelmatrix.aspectRatio;
            pixelSpacing = pixelmatrix.pixelSpacing;
            minVal = 0;
            maxVal = 255;
            redPalette = null;
            greenPalette = null;
            bluePalette = null;
            return;
        }
    }

    protected PixelMatrix(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[])
    {
        pixelData = abyte0;
        rows = i;
        columns = j;
        photometricInterpretation = pixelmatrix.photometricInterpretation;
        pmiConst = pixelmatrix.pmiConst;
        samplesPerPixel = pixelmatrix.samplesPerPixel;
        bytesAllocated = pixelmatrix.bytesAllocated;
        planarConfiguration = pixelmatrix.planarConfiguration;
        pixelsPerFrame = rows * columns;
        planeSize = pixelsPerFrame * bytesAllocated;
        frameSize = planeSize * samplesPerPixel;
        numberOfFrames = k;
        representativeFrameNumber = l;
        if(toEvenLength(abyte0.length) != toEvenLength(frameSize * numberOfFrames))
        {
            throw new RuntimeException("mismatch length of pixel pixelData");
        } else
        {
            numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
            bitsStored = pixelmatrix.bitsStored;
            bitmask = pixelmatrix.bitmask;
            shift = pixelmatrix.shift;
            pixelSpacing = pixelmatrix.calcPixelSpacingFor(rows, columns);
            aspectRatio = pixelSpacing == null ? pixelmatrix.calcAspectRatioFor(rows, columns) : getAspectRatioFrom(pixelSpacing);
            minVal = pixelmatrix.minVal;
            maxVal = pixelmatrix.maxVal;
            redPalette = pixelmatrix.redPalette;
            greenPalette = pixelmatrix.greenPalette;
            bluePalette = pixelmatrix.bluePalette;
            return;
        }
    }

    private static int[] getAspectRatioFrom(float af[])
    {
        return cancel((long)(af[0] * 1000F), (long)(af[1] * 1000F));
    }

    private static int[] cancel(long l, long l1)
    {
        BigInteger biginteger = BigInteger.valueOf(l);
        BigInteger biginteger1 = BigInteger.valueOf(l1);
        BigInteger biginteger2 = biginteger.gcd(biginteger1);
        return biginteger2.intValue() != 0 ? (new int[] {
            biginteger.divide(biginteger2).intValue(), biginteger1.divide(biginteger2).intValue()
        }) : (new int[] {
            (int)l, (int)l1
        });
    }

    private float[] calcPixelSpacingFor(int i, int j)
    {
        return pixelSpacing != null ? (new float[] {
            (pixelSpacing[0] * (float)rows) / (float)i, (pixelSpacing[1] * (float)columns) / (float)j
        }) : null;
    }

    private int[] calcAspectRatioFor(int i, int j)
    {
        int k = columns * i;
        int l = rows * j;
        return k != l ? cancel((long)aspectRatio[0] * (long)l, (long)aspectRatio[1] * (long)k) : aspectRatio;
    }

    protected PixelMatrix(DicomObject dicomobject)
        throws DicomException
    {
        switch(dicomobject.getSize(1184))
        {
        case -1:
        case 0: // '\0'
            throw new DicomException("missing pixel pixelData");

        default:
            throw new IllegalArgumentException("encapsulate pixel pixelData");

        case 1: // '\001'
            pixelData = (byte[])dicomobject.get(1184);
            break;
        }
        rows = getI(dicomobject, 466);
        columns = getI(dicomobject, 467);
        photometricInterpretation = (String)dicomobject.get(462);
        if(photometricInterpretation == null)
            throw new DicomException("missing photometric interpretation");
        pmiConst = EnumPMI.getConstant(photometricInterpretation);
        if(pmiConst == -1)
            throw new DicomException("unrecognized photometric interpretation " + photometricInterpretation);
        samplesPerPixel = getI(dicomobject, 461);
        if(samplesPerPixel != EnumPMI.SAMPLES[pmiConst])
            throw new DicomException("samples per pixel[" + samplesPerPixel + "] mismatch photometric interpretation[" + photometricInterpretation + "]");
        int i = getI(dicomobject, 475);
        if(i != 8 && i != 16)
            throw new UnsupportedOperationException("" + i + " bits allocated with not supported");
        bytesAllocated = i >> 3;
        planarConfiguration = getI(dicomobject, 463, 1);
        pixelsPerFrame = rows * columns;
        planeSize = pixelsPerFrame * bytesAllocated;
        frameSize = planeSize * samplesPerPixel;
        numberOfFrames = getI(dicomobject, 464, 1);
        representativeFrameNumber = getI(dicomobject, 512, 1);
        if(toEvenLength(pixelData.length) != toEvenLength(frameSize * numberOfFrames))
            throw new DicomException("mismatch length of pixel pixelData");
        numberOfSamples = pixelsPerFrame * samplesPerPixel * numberOfFrames;
        bitsStored = Math.min(getI(dicomobject, 476), i);
        bitmask = 65535 >> 16 - bitsStored;
        shift = Math.min(i - bitsStored, Math.max(0, bitsStored - getI(dicomobject, 477) - 1));
        pixelSpacing = getPixelSpacingFrom(dicomobject);
        aspectRatio = pixelSpacing == null ? getAspectRatioFrom(dicomobject) : getAspectRatioFrom(pixelSpacing);
        PaletteColorLUTs palettecolorluts = new PaletteColorLUTs(dicomobject);
        redPalette = palettecolorluts.getRedPaletteColorLUTData8Bit();
        greenPalette = palettecolorluts.getGreenPaletteColorLUTData8Bit();
        bluePalette = palettecolorluts.getBluePaletteColorLUTData8Bit();
        if(isPaletteColor() && (redPalette == null || greenPalette == null || bluePalette == null))
            throw new DicomException("Missing Palette Color LUT");
        else
            return;
    }

    public static int[] getAspectRatioFrom(DicomObject dicomobject)
    {
        int ai[] = {
            1, 1
        };
        if(dicomobject.getSize(473) == 2)
        {
            ai[0] = ((Integer)dicomobject.get(473, 0)).intValue();
            ai[1] = ((Integer)dicomobject.get(473, 1)).intValue();
            if(ai[0] == ai[1])
            {
                ai[0] = 1;
                ai[1] = 1;
            }
        }
        return ai;
    }

    public static float[] getPixelSpacingFrom(DicomObject dicomobject)
    {
        char c;
        if(dicomobject.getSize(((int) (c = '\u01D6'))) == 2 || dicomobject.getSize(((int) (c = '\u0134'))) == 2)
        {
            float af[] = {
                ((Float)dicomobject.get(((int) (c)), 0)).floatValue(), ((Float)dicomobject.get(((int) (c)), 1)).floatValue()
            };
            if(af[0] > 0.0F && af[1] > 0.0F)
                return af;
            Debug.out.println("jdicom: Illegal values of " + DDict.getDescription(((int) (c))) + ": " + af[0] + "\\" + af[1]);
        }
        return null;
    }

    public final byte[] getPixelData()
    {
        return pixelData;
    }

    public final byte[] getRedPalette()
    {
        return redPalette;
    }

    public final byte[] getGreenPalette()
    {
        return greenPalette;
    }

    public final byte[] getBluePalette()
    {
        return bluePalette;
    }

    public final int getRows()
    {
        return rows;
    }

    public final int getColumns()
    {
        return columns;
    }

    public final String getPhotometricInterpretation()
    {
        return photometricInterpretation;
    }

    public final int getSamplesPerPixel()
    {
        return samplesPerPixel;
    }

    public final boolean isMonochrome()
    {
        return pmiConst == 0 || pmiConst == 1;
    }

    public final boolean isMonochrome1()
    {
        return pmiConst == 0;
    }

    public final boolean isRGB()
    {
        return pmiConst == 3;
    }

    public final boolean isPaletteColor()
    {
        return pmiConst == 2;
    }

    public final boolean isColorByPlane()
    {
        return planarConfiguration != 0;
    }

    public final boolean isWordAllocated()
    {
        return bytesAllocated == 2;
    }

    public final int getBytesAllocated()
    {
        return bytesAllocated;
    }

    public final int getNumberOfFrames()
    {
        return numberOfFrames;
    }

    public final int getRepresentativeFrameNumber()
    {
        return representativeFrameNumber;
    }

    public final int getPixelsPerFrame()
    {
        return pixelsPerFrame;
    }

    public final int getPlaneSize()
    {
        return planeSize;
    }

    public final int getFrameSize()
    {
        return frameSize;
    }

    public final int[] getAspectRatio()
    {
        return aspectRatio;
    }

    public final float[] getPixelSpacing()
    {
        return pixelSpacing;
    }

    public final int getMinVal()
    {
        return minVal;
    }

    public final int getMaxVal()
    {
        return maxVal;
    }

    static int getI(DicomObject dicomobject, int i)
        throws DicomException
    {
        Integer integer = (Integer)dicomobject.get(i);
        if(integer == null)
            throw new DicomException("Missing " + DDict.getDescription(i));
        else
            return integer.intValue();
    }

    static int getI(DicomObject dicomobject, int i, int j)
    {
        Integer integer = (Integer)dicomobject.get(i);
        return integer == null ? j : integer.intValue();
    }

    public String toString()
    {
        return " Pixelmatrix " + photometricInterpretation + ", colorByPlane= " + planarConfiguration + ", rows=" + rows + ", columns=" + columns + ", frames=" + numberOfFrames + ", bits stored=" + bitsStored + ", shift=" + shift + ", min val=" + minVal + ", max val=" + maxVal + ", aspect ratio=" + aspectRatio[0] + ":" + aspectRatio[1] + (pixelSpacing == null ? "" : ", pixel spacing=" + pixelSpacing[0] + "\\" + pixelSpacing[1]);
    }

    public final int getNumberOfSamples()
    {
        return numberOfSamples;
    }

    public Dimension adjustRescaleDimension(int i, int j)
    {
        long l = rows * aspectRatio[0];
        long l1 = columns * aspectRatio[1];
        return l * (long)j <= l1 * (long)i ? new Dimension((int)(((long)j * l) / l1), j) : new Dimension(i, (int)(((long)i * l1) / l));
    }

    public PixelMatrix rescale(int i, int j, byte abyte0[])
        throws DicomException
    {
        if(i == rows && j == columns)
            return this;
        RescalePixelData rescalepixeldata = new RescalePixelData(this, i, j);
        PixelMatrix pixelmatrix = clone(this, i, j, numberOfFrames, representativeFrameNumber, rescalepixeldata.rescale(abyte0));
        if(Debug.DEBUG > 2)
        {
            Debug.out.println("jdicom: rescale pixel matrix: " + this);
            Debug.out.println("jdicom:                   to: " + pixelmatrix);
        }
        return pixelmatrix;
    }

    public PixelMatrix rescaleFrame(int i, int j, int k, byte abyte0[])
        throws DicomException
    {
        RescalePixelData rescalepixeldata = new RescalePixelData(this, j, k);
        PixelMatrix pixelmatrix = clone(this, j, k, 1, 0, rescalepixeldata.rescaleFrame(i, abyte0));
        if(Debug.DEBUG > 2)
        {
            Debug.out.println("jdicom: rescale pixel matrix frame #" + i + ": " + this);
            Debug.out.println("jdicom:                             to: " + pixelmatrix);
        }
        return pixelmatrix;
    }

    public PixelMatrix convertToPaletteColor(byte abyte0[])
    {
        switch(pmiConst)
        {
        case 0: // '\0'
        case 1: // '\001'
            throw new UnsupportedOperationException("cannot convert monochrome to palette color image");

        case 2: // '\002'
            return this;
        }
        throw new UnsupportedOperationException("convertToPaletteColor not yet implemented");
    }

    public PixelMatrix convertToGrayscale(byte abyte0[])
    {
        switch(pmiConst)
        {
        case 0: // '\0'
        case 1: // '\001'
            return this;

        case 3: // '\003'
            abyte0 = getByteArray(abyte0, pixelsPerFrame * numberOfFrames);
            if(planarConfiguration == 0)
                rgbPixelToGrayscale(abyte0);
            else
                rgbPlaneToGrayscale(abyte0);
            break;

        case 2: // '\002'
            abyte0 = getByteArray(abyte0, pixelsPerFrame * numberOfFrames);
            paletteColorToGrayscale(abyte0);
            break;

        default:
            throw new UnsupportedOperationException("convertToGrayscale not supported for - " + photometricInterpretation);
        }
        return ((PixelMatrix) (new UnsignedByte(this, abyte0)));
    }

    private static byte[] getByteArray(byte abyte0[], int i)
    {
        if(abyte0 == null)
            return new byte[i];
        if(abyte0.length != i)
            throw new IllegalArgumentException("illegal pixeldata.length -" + i);
        else
            return abyte0;
    }

    public PixelMatrix preformatGrayscale(LUT.Byte1 byte1, byte abyte0[])
    {
        if(byte1 == null)
            return this;
        abyte0 = getByteArray(abyte0, pixelsPerFrame * numberOfFrames);
        doPreformatGrayscale(byte1, abyte0);
        UnsignedByte unsignedbyte = new UnsignedByte(this, abyte0);
        if(Debug.DEBUG > 2)
        {
            Debug.out.println("jdicom: preformat pixel matrix: " + this);
            Debug.out.println("jdicom:                     to: " + unsignedbyte);
        }
        return ((PixelMatrix) (unsignedbyte));
    }

    public abstract int[] calcMinMax();

    public abstract int getSample(int i);

    protected abstract PixelMatrix clone(PixelMatrix pixelmatrix, int i, int j, int k, int l, byte abyte0[]);

    public abstract BufferedImage createBufferedImage(int i);

    protected abstract void rgbPixelToGrayscale(byte abyte0[]);

    protected abstract void rgbPlaneToGrayscale(byte abyte0[]);

    protected abstract void paletteColorToGrayscale(byte abyte0[]);

    protected abstract void doPreformatGrayscale(LUT.Byte1 byte1, byte abyte0[]);






}
