// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NotStringFilter.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            IStringFilter

public class NotStringFilter
    implements IStringFilter
{

    private IStringFilter filter;

    public NotStringFilter(IStringFilter istringfilter)
    {
        filter = istringfilter;
    }

    public boolean accept(String s)
    {
        return !filter.accept(s);
    }
}
