// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckParam.java

package com.tiani.dicom.util;


// Referenced classes of package com.tiani.dicom.util:
//            CheckParam

class DefinedParam extends CheckParam
{

    protected String _tags[];

    public DefinedParam(String as[], int i)
    {
        super(i);
        if(as == null || as.length == 0)
        {
            throw new IllegalArgumentException("enumeration must not be empty");
        } else
        {
            _tags = as;
            return;
        }
    }

    public void check(String s)
    {
        super.check(s);
    }

    public String[] getTags()
    {
        return _tags;
    }
}
