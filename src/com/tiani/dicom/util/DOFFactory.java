// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DOFFactory.java

package com.tiani.dicom.util;

import com.archimed.dicom.DDateRange;
import com.archimed.dicom.DDateTimeRange;
import com.archimed.dicom.DTimeRange;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.util:
//            AndDicomObjectFilter, IDicomObjectFilter, DefDicomObjectFilter, OrDicomObjectFilter, 
//            SeqDicomObjectFilter

public final class DOFFactory
{
    private abstract class AddMatching
    {

        public void getFilters(Vector vector, DicomObject dicomobject, int ai[])
            throws DicomException
        {
            if(ai != null)
            {
                for(int i = 0; i < ai.length; i++)
                {
                    IDicomObjectFilter idicomobjectfilter;
                    if(dicomobject.getSize(ai[i]) > 0 && (idicomobjectfilter = getFilter(dicomobject, ai[i])) != null)
                        vector.addElement(((Object) (idicomobjectfilter)));
                }

            }
        }

        protected abstract IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
            throws DicomException;

        private AddMatching()
        {
        }

    }


    private int _dSVmatching[];
    private int _dWCmatching[];
    private int _dDRmatching[];
    private int _dUIDmatching[];
    private int _dSQmatching[];
    private Hashtable _seqDOFFactory;
    private final AddMatching _addSingleValueMatching;
    private final AddMatching _addWildCardMatching;
    private final AddMatching _addRangeMatching;
    private final AddMatching _addListOfUIDMatching;
    private final AddMatching _addSequenceMatching;
    private static final Hashtable _mwlSeqDOFFactory;
    private static final DOFFactory _mwlDOFFactory;
    private static final DOFFactory _patientDOFFactory = new DOFFactory(new int[] {
        148, 152, 154
    }, new int[] {
        147, 155
    }, new int[] {
        150
    }, ((int []) (null)));
    private static final DOFFactory _studyDOFFactory = new DOFFactory(new int[] {
        77, 427
    }, new int[] {
        88, 101, 1115
    }, new int[] {
        64, 70
    }, new int[] {
        425
    });
    private static final DOFFactory _seriesDOFFactory = new DOFFactory(new int[] {
        81, 428, 182
    }, ((int []) (null)), ((int []) (null)), new int[] {
        426
    });
    private static final Hashtable _srSeqDOFFactory;
    private static final DOFFactory _imageDOFFactory;

    public DOFFactory(int ai[], int ai1[], int ai2[], int ai3[], int ai4[], Hashtable hashtable)
    {
        _addSingleValueMatching = ((AddMatching) (new AddMatching() {

            protected IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
                throws DicomException
            {
                String s = DOFFactory.removeWildCards(dicomobject.getS(i));
                return s.length() <= 0 ? null : DefDicomObjectFilter.getSingleValueMatching(i, s);
            }

        }));
        _addWildCardMatching = ((AddMatching) (new AddMatching() {

            protected IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
                throws DicomException
            {
                return DefDicomObjectFilter.getWildCardMatching(i, dicomobject.getS(i));
            }

        }));
        _addRangeMatching = ((AddMatching) (new AddMatching() {

            protected IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
                throws DicomException
            {
                Object obj = dicomobject.get(i);
                return (obj instanceof DDateRange) ? DefDicomObjectFilter.getDateRangeMatching(i, (DDateRange)obj) : (obj instanceof DTimeRange) ? DefDicomObjectFilter.getTimeRangeMatching(i, (DTimeRange)obj) : (obj instanceof DDateTimeRange) ? DefDicomObjectFilter.getDateTimeRangeMatching(i, (DDateTimeRange)obj) : DefDicomObjectFilter.getSingleValueMatching(i, dicomobject.getS(i));
            }

        }));
        _addListOfUIDMatching = ((AddMatching) (new AddMatching() {

            protected IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
                throws DicomException
            {
                IDicomObjectFilter aidicomobjectfilter[] = new IDicomObjectFilter[dicomobject.getSize(i)];
                for(int j = 0; j < aidicomobjectfilter.length; j++)
                    aidicomobjectfilter[j] = DefDicomObjectFilter.getSingleValueMatching(i, dicomobject.getS(i, j));

                return ((IDicomObjectFilter) (new OrDicomObjectFilter(aidicomobjectfilter)));
            }

        }));
        _addSequenceMatching = ((AddMatching) (new AddMatching() {

            protected IDicomObjectFilter getFilter(DicomObject dicomobject, int i)
                throws DicomException
            {
                DOFFactory doffactory = (DOFFactory)_seqDOFFactory.get(((Object) (new Integer(i))));
                return ((IDicomObjectFilter) (new SeqDicomObjectFilter(i, doffactory.getFilter((DicomObject)dicomobject.get(i)))));
            }

        }));
        _dSVmatching = ai;
        _dWCmatching = ai1;
        _dDRmatching = ai2;
        _dUIDmatching = ai3;
        _dSQmatching = ai4;
        _seqDOFFactory = hashtable;
    }

    public DOFFactory(int ai[], int ai1[], int ai2[], int ai3[])
    {
        this(ai, ai1, ai2, ai3, ((int []) (null)), ((Hashtable) (null)));
    }

    public IDicomObjectFilter getFilter(DicomObject dicomobject)
        throws DicomException
    {
        Vector vector = new Vector();
        _addSingleValueMatching.getFilters(vector, dicomobject, _dSVmatching);
        _addWildCardMatching.getFilters(vector, dicomobject, _dWCmatching);
        _addRangeMatching.getFilters(vector, dicomobject, _dDRmatching);
        _addListOfUIDMatching.getFilters(vector, dicomobject, _dUIDmatching);
        _addSequenceMatching.getFilters(vector, dicomobject, _dSQmatching);
        return ((IDicomObjectFilter) (new AndDicomObjectFilter(vector)));
    }

    private static String removeWildCards(String s)
    {
        int i;
        if(s == null || (i = s.indexOf('*')) == -1)
            return s;
        int j = s.length();
        StringBuffer stringbuffer = new StringBuffer(j);
        for(int k = 0; k < j; k++)
        {
            char c;
            if((c = s.charAt(k)) != '*')
                stringbuffer.append(c);
        }

        return stringbuffer.toString();
    }

    public static DOFFactory getMWLDOFFactory()
    {
        return _mwlDOFFactory;
    }

    public static DOFFactory getPatientDOFFactory()
    {
        return _patientDOFFactory;
    }

    public static DOFFactory getStudyDOFFactory()
    {
        return _studyDOFFactory;
    }

    public static DOFFactory getSeriesDOFFactory()
    {
        return _seriesDOFFactory;
    }

    public static DOFFactory getImageDOFFactory()
    {
        return _imageDOFFactory;
    }

    static 
    {
        _mwlSeqDOFFactory = new Hashtable(3);
        _mwlSeqDOFFactory.put(((Object) (new Integer(587))), ((Object) (new DOFFactory(new int[] {
            574, 81, 583, 584
        }, new int[] {
            579
        }, new int[] {
            575, 576
        }, ((int []) (null))))));
        _mwlDOFFactory = new DOFFactory(new int[] {
            148
        }, new int[] {
            147
        }, ((int []) (null)), ((int []) (null)), new int[] {
            587
        }, _mwlSeqDOFFactory);
        _srSeqDOFFactory = new Hashtable(3);
        _srSeqDOFFactory.put(((Object) (new Integer(1431))), ((Object) (new DOFFactory(((int []) (null)), new int[] {
            1432
        }, new int[] {
            1427
        }, ((int []) (null))))));
        _srSeqDOFFactory.put(((Object) (new Integer(1322))), ((Object) (new DOFFactory(new int[] {
            91, 92
        }, ((int []) (null)), ((int []) (null)), ((int []) (null))))));
        _imageDOFFactory = new DOFFactory(new int[] {
            430, 1446, 1448
        }, ((int []) (null)), ((int []) (null)), new int[] {
            63, 62
        }, new int[] {
            1431, 1322
        }, _srSeqDOFFactory);
    }


}
