// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   TianiInputStream.java

package com.tiani.dicom.legacy;

import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.media.FileMetaInformation;
import com.tiani.dicom.util.MarkableInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

// Referenced classes of package com.tiani.dicom.legacy:
//            TianiInvalidFormatException, BirniHistoryReader, BirniROIReader, TianiDecompressStream

public class TianiInputStream extends MarkableInputStream
{

    public static final int UNKNOWN = 0;
    public static final int DICOM_FILE = 1;
    public static final int DICOM_STREAM_IMPLVR_LE = 2;
    public static final int DICOM_STREAM_EXPLVR_LE = 3;
    public static final int DICOM_STREAM_EXPLVR_BE = 4;
    public static final int BIRNI_HISTORY = 5;
    public static final int BIRNI_ROI = 6;
    public static final int ENCAPSED_UNCOMPRESSED = 7;
    public static final int ENCAPSED_COMPRESSED = 8;
    public static final int ENCAPSED_JPEG_LOSSLESS = 9;
    private static final int MAX_FORMAT = 9;
    private static final String FORMAT_NAMES[] = {
        "Unknown", "DICOM File", "DICOM Stream ImplVR/LE", "DICOM Stream ExplVR/LE", "DICOM Stream ExplVR/BE", "BIRNI History", "BIRNI ROI", "TIANI Encapsed", "Encapsed uncompressed", "Encapsed compressed"
    };
    private static final byte ACRN_LE[] = {
        8, 0
    };
    private static final byte ACRN_BE[] = {
        0, 8
    };
    private static final byte DICM[] = {
        68, 73, 67, 77
    };
    private static final byte HISTORY[] = {
        91, 32, 72, 73
    };
    private static final byte ROI[] = {
        91, 32, 82, 79
    };
    private static final byte MAGI[] = {
        77, 65, 71, 73
    };
    private static final int TSID[] = {
        8193, 8194, 8195
    };
    private int _format;
    private Properties _encapseheader;
    private Thread _loadThread;

    public TianiInputStream(InputStream inputstream, int i)
    {
        super(inputstream);
        _format = 0;
        _encapseheader = new Properties();
        _loadThread = null;
        _format = i;
        if(_format <= 0 || _format > 9)
            throw new IllegalArgumentException("Invalid format:" + _format);
        else
            return;
    }

    public TianiInputStream(InputStream inputstream)
        throws IOException, TianiInvalidFormatException
    {
        super(inputstream);
        _format = 0;
        _encapseheader = new Properties();
        _loadThread = null;
        _format = detectFormat(((InputStream) (this)));
        if(_format == 0)
            throw new TianiInvalidFormatException("Unknown Format");
        else
            return;
    }

    private int readInt()
        throws IOException
    {
        InputStream inputstream = (FilterInputStream)in;
        int i = inputstream.read();
        int j = inputstream.read();
        int k = inputstream.read();
        int l = inputstream.read();
        if((i | j | k | l) < 0)
            throw new EOFException();
        else
            return (l << 24) + (k << 16) + (j << 8) + (i << 0);
    }

    private final int readTag()
        throws IOException
    {
        InputStream inputstream = (FilterInputStream)in;
        int i = inputstream.read();
        int j = inputstream.read();
        int k = inputstream.read();
        int l = inputstream.read();
        if((i | j | k | l) < 0)
            throw new EOFException();
        else
            return (j << 24) + (i << 16) + (l << 8) + (k << 0);
    }

    private static int readFully(InputStream inputstream, byte abyte0[])
        throws IOException
    {
        int i = abyte0.length;
        int j;
        int k;
        for(j = 0; j < i; j += k)
        {
            k = inputstream.read(abyte0, j, i - j);
            if(k < 0)
                return j;
        }

        return j;
    }

    public static int detectFormat(InputStream inputstream, Properties properties)
        throws IOException
    {
        byte abyte0[] = new byte[512];
        inputstream.mark(abyte0.length);
        int i = readFully(inputstream, abyte0);
        inputstream.reset();
        if(i < 8)
            return 0;
        if(_contains(abyte0, 128, DICM))
            return 1;
        if(_contains(abyte0, 0, ACRN_LE))
            return _isExplicit(abyte0) ? 3 : 2;
        if(_contains(abyte0, 0, ACRN_BE))
            return 4;
        if(_contains(abyte0, 0, HISTORY))
            return 5;
        if(_contains(abyte0, 0, ROI))
            return 6;
        if(_contains(abyte0, 0, MAGI))
            return _decapse(abyte0, i, inputstream, properties);
        return !canParseImplVRLE(abyte0) ? 0 : 2;
    }

    public static int detectFormat(InputStream inputstream)
        throws IOException
    {
        return detectFormat(inputstream, new Properties());
    }

    public int getFormat()
    {
        return _format;
    }

    public String getFormatName()
    {
        return FORMAT_NAMES[_format];
    }

    public Properties getEncapseHeader()
    {
        return _encapseheader;
    }

    public String toString()
    {
        return ((Object)this).toString() + '[' + FORMAT_NAMES[_format] + ']';
    }

    public void read(DicomObject dicomobject, boolean flag)
        throws IOException, TianiInvalidFormatException, DicomException
    {
        switch(_format)
        {
        case 1: // '\001'
            dicomobject.read(((InputStream) (this)), flag);
            return;

        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
            dicomobject.read(((InputStream) (this)), TSID[_format - 2], flag);
            return;

        case 5: // '\005'
            BirniHistoryReader birnihistoryreader = new BirniHistoryReader(((InputStream) (this)));
            birnihistoryreader.read(dicomobject);
            return;

        case 6: // '\006'
            BirniROIReader birniroireader = new BirniROIReader(((InputStream) (this)));
            birniroireader.read(dicomobject);
            return;

        case 7: // '\007'
            dicomobject.read(((InputStream) (this)), 8193, flag);
            return;

        case 9: // '\t'
            dicomobject.read(((InputStream) (this)), 8193, false);
            if(flag)
            {
                byte abyte0[];
                for(; readTag() == 0xfffee000; dicomobject.append(1184, ((Object) (abyte0))))
                {
                    abyte0 = new byte[readInt()];
                    readFully(((InputStream) (this)), abyte0);
                }

            }
            dicomobject.setFileMetaInformation(((DicomObject) (new FileMetaInformation(dicomobject, "1.2.840.10008.1.2.4.70"))));
            return;

        case 8: // '\b'
            dicomobject.read(((InputStream) (this)), 8193, false);
            if(flag)
            {
                int i = dicomobject.getI(466);
                int j = dicomobject.getI(467);
                int k = dicomobject.getI(475);
                int l = dicomobject.getI(461);
                int i1 = 1;
                if(dicomobject.getSize(464) > 0)
                    i1 = dicomobject.getI(464);
                TianiDecompressStream tianidecompressstream = new TianiDecompressStream(((InputStream) (this)));
                byte abyte1[] = new byte[i * j * (k >> 3) * l * i1];
                tianidecompressstream.readFully(abyte1);
                dicomobject.set(1184, ((Object) (abyte1)));
            }
            return;
        }
        throw new TianiInvalidFormatException("Unknown Format");
    }

    public void read(DicomObject dicomobject)
        throws IOException, TianiInvalidFormatException, DicomException
    {
        read(dicomobject, true);
    }

    public int getTransferSyntaxId(DicomObject dicomobject)
        throws DicomException, UnknownUIDException, TianiInvalidFormatException
    {
        switch(_format)
        {
        case 1: // '\001'
            DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
            return UID.getUIDEntry(dicomobject1.getS(31)).getConstant();

        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
            return TSID[_format - 2];

        case 7: // '\007'
        case 8: // '\b'
            return 8193;

        case 9: // '\t'
            return 8197;

        case 5: // '\005'
        case 6: // '\006'
        default:
            throw new RuntimeException("Invalid Format for prepareImage");
        }
    }

    private static int _decapse(byte abyte0[], int i, InputStream inputstream, Properties properties)
        throws IOException
    {
        BufferedReader bufferedreader = new BufferedReader(((java.io.Reader) (new InputStreamReader(((InputStream) (new ByteArrayInputStream(abyte0, 0, i)))))));
        int j = 0;
        String s;
        while((s = bufferedreader.readLine()) != null)
        {
            j += s.length() + 1;
            if(s.equals("ENDINFO"))
            {
                inputstream.skip(j);
                return _getEncapsedFormat(properties);
            }
            StringTokenizer stringtokenizer = new StringTokenizer(s);
            if(!stringtokenizer.hasMoreTokens())
                return 0;
            String s1 = stringtokenizer.nextToken();
            for(int k = 0; stringtokenizer.hasMoreTokens(); k++)
                ((Hashtable) (properties)).put(((Object) (s1 + k)), ((Object) (stringtokenizer.nextToken())));

        }
        return 0;
    }

    private static int _getEncapsedFormat(Properties properties)
    {
        String s = properties.getProperty("COMPRESSION0");
        return "NONE".equals(((Object) (s))) ? 7 : ((byte)("JPEG_LOSSLESS".equals(((Object) (s))) ? 9 : 8));
    }

    private static boolean canParseImplVRLE(byte abyte0[])
    {
        int i = abyte0[0] & 0xff | (abyte0[1] & 0xff) << 8;
        int j = abyte0[2] & 0xff | (abyte0[3] & 0xff) << 8;
        int k = abyte0[4] & 0xff | (abyte0[5] & 0xff) << 8 | (abyte0[6] & 0xff) << 16 | (abyte0[7] & 0xff) << 24;
        if(16 + k > abyte0.length)
        {
            return false;
        } else
        {
            int l = 8 + k;
            int i1 = abyte0[l] & 0xff | (abyte0[l + 1] & 0xff) << 8;
            int j1 = abyte0[l + 2] & 0xff | (abyte0[l + 3] & 0xff) << 8;
            return i == i1;
        }
    }

    private static boolean _contains(byte abyte0[], int i, byte abyte1[])
    {
        if(abyte0.length < abyte1.length + i)
            return false;
        int j = i;
        for(int k = 0; k < abyte1.length; k++)
        {
            if(abyte0[j] != abyte1[k])
                return false;
            j++;
        }

        return true;
    }

    private static boolean _isExplicit(byte abyte0[])
    {
        byte abyte1[] = _vrBytes(DDict.getTypeCode(abyte0[0] + (abyte0[1] << 8), abyte0[2] + (abyte0[3] << 8)));
        if(abyte1 == null)
            return false;
        else
            return _contains(abyte0, 4, abyte1);
    }

    private static byte[] _vrBytes(int i)
    {
        switch(i)
        {
        case 4: // '\004'
            return (new byte[] {
                65, 69
            });

        case 17: // '\021'
            return (new byte[] {
                65, 83
            });

        case 5: // '\005'
            return (new byte[] {
                65, 84
            });

        case 9: // '\t'
            return (new byte[] {
                67, 83
            });

        case 11: // '\013'
            return (new byte[] {
                68, 65
            });

        case 16: // '\020'
            return (new byte[] {
                68, 83
            });

        case 28: // '\034'
            return (new byte[] {
                68, 84
            });

        case 20: // '\024'
            return (new byte[] {
                70, 68
            });

        case 26: // '\032'
            return (new byte[] {
                70, 76
            });

        case 15: // '\017'
            return (new byte[] {
                73, 83
            });

        case 6: // '\006'
            return (new byte[] {
                76, 79
            });

        case 18: // '\022'
            return (new byte[] {
                76, 84
            });

        case 8: // '\b'
            return (new byte[] {
                79, 66
            });

        case 24: // '\030'
            return (new byte[] {
                79, 87
            });

        case 14: // '\016'
            return (new byte[] {
                80, 78
            });

        case 7: // '\007'
            return (new byte[] {
                83, 72
            });

        case 19: // '\023'
            return (new byte[] {
                83, 76
            });

        case 10: // '\n'
            return (new byte[] {
                83, 81
            });

        case 23: // '\027'
            return (new byte[] {
                83, 83
            });

        case 13: // '\r'
            return (new byte[] {
                83, 84
            });

        case 12: // '\f'
            return (new byte[] {
                84, 77
            });

        case 2: // '\002'
            return (new byte[] {
                85, 73
            });

        case 0: // '\0'
            return (new byte[] {
                85, 78
            });

        case 1: // '\001'
            return (new byte[] {
                85, 76
            });

        case 3: // '\003'
            return (new byte[] {
                85, 83
            });

        case 27: // '\033'
            return (new byte[] {
                85, 84
            });

        case 21: // '\025'
        case 22: // '\026'
        case 25: // '\031'
        default:
            return null;
        }
    }

}
