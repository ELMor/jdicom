// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TianiDecompressStream.java

package com.tiani.dicom.legacy;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.tiani.dicom.legacy:
//            TianiInvalidFormatException

public class TianiDecompressStream extends FilterInputStream
{

    public static final byte SH_INIT_SWAP = 1;
    public static final byte SH_INIT_NOSWAP = 2;
    public static final byte CH_INIT = 3;
    public static final byte CH_END = -2;
    public static final byte SH_END = -1;
    public static final byte SH_REP = 10;
    public static final byte SH_NIB_DIF = 12;
    public static final byte SH_CHAR_DIF = 14;
    public static final byte SH_INCOMP_12 = 16;
    public static final byte SH_INCOMP = 18;
    public static final byte CH_REP = 110;
    public static final byte CH_NIB_DIF = 112;
    public static final byte CH_INCOMP = 118;
    private byte lB;
    private short lS;
    private int outCnt;
    private byte outBuff[];
    private int outPos;
    private int outPosRead;
    private boolean endOfDecompress;
    private int inCnt;

    public TianiDecompressStream(InputStream inputstream)
        throws IOException
    {
        super(inputstream);
        lB = 0;
        lS = 0;
        outCnt = 0;
        outBuff = new byte[512];
        outPos = 0;
        outPosRead = 0;
        endOfDecompress = false;
        inCnt = 0;
        outBuff = new byte[512];
        decompress_init();
    }

    public final int read()
        throws IOException
    {
        if(outPosRead >= outPos)
        {
            outPos = 0;
            outPosRead = 0;
            decompress_more();
        }
        if(endOfDecompress)
        {
            return -1;
        } else
        {
            int i = ((int) (outBuff[outPosRead++]));
            i &= 0xff;
            return i;
        }
    }

    public final int read(byte abyte0[], int i, int j)
        throws IOException
    {
        if(outPosRead >= outPos)
        {
            outPos = 0;
            outPosRead = 0;
            decompress_more();
        }
        if(endOfDecompress && outPosRead >= outPos)
        {
            return -1;
        } else
        {
            int k = Math.min(j, outPos - outPosRead);
            System.arraycopy(((Object) (outBuff)), outPosRead, ((Object) (abyte0)), i, k);
            outPosRead += k;
            return k;
        }
    }

    public final int read(byte abyte0[])
        throws IOException
    {
        return read(abyte0, 0, abyte0.length);
    }

    public final void readFully(byte abyte0[])
        throws IOException
    {
        readFully(abyte0, 0, abyte0.length);
    }

    public final void readFully(byte abyte0[], int i, int j)
        throws IOException
    {
        if(j < 0)
            throw new IndexOutOfBoundsException();
        int l;
        for(int k = 0; k < j; k += l)
        {
            l = read(abyte0, i + k, j - k);
            if(l < 0)
                throw new EOFException();
        }

    }

    private final void decompress_init()
        throws IOException
    {
        boolean flag = false;
        byte byte0 = readByte();
        switch(byte0)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            int i = readByte() & 0xff;
            i <<= 8;
            i |= readByte() & 0xff;
            i <<= 8;
            i |= readByte() & 0xff;
            i <<= 8;
            i |= readByte() & 0xff;
            break;
        }
        switch(byte0)
        {
        case 3: // '\003'
            byte byte1 = readByte();
            out(byte1);
            break;

        case 1: // '\001'
        case 2: // '\002'
            byte byte2 = readByte();
            byte byte3 = readByte();
            out(byte2, byte3);
            break;
        }
    }

    private final void decompress_more()
        throws IOException
    {
        boolean flag = false;
        if(endOfDecompress)
            return;
        byte byte11 = readByte();
        switch(byte11)
        {
        case -2: 
            endOfDecompress = true;
            return;

        case -1: 
            endOfDecompress = true;
            return;
        }
        int i = readByte() & 0xff;
label0:
        switch(byte11)
        {
        case 118: // 'v'
            while(i-- > 0) 
            {
                byte byte0 = readByte();
                out(byte0);
            }
            break;

        case 18: // '\022'
            while(i-- > 0) 
            {
                byte byte1 = readByte();
                byte byte8 = readByte();
                out(byte1, byte8);
            }
            break;

        case 16: // '\020'
            do
            {
                byte byte2 = readByte();
                byte byte9 = readByte();
                byte byte12 = (byte)(byte2 >> 4 & 0xf);
                byte byte13 = (byte)(byte2 << 4 & 0xf0 | byte9 >> 4 & 0xf);
                out(byte12, byte13);
                if(--i == 0)
                    break label0;
                if(i < 0)
                    throw new TianiInvalidFormatException("length decoding mismatch");
                byte12 = (byte)(byte9 & 0xf);
                byte byte10 = readByte();
                out(byte12, byte10);
                if(--i == 0)
                    break label0;
            } while(i >= 0);
            throw new TianiInvalidFormatException("length decoding mismatch");

        case 14: // '\016'
            int j = ((int) (getLastShort()));
            j &= 0xffff;
            while(i-- > 0) 
            {
                byte byte3 = readByte();
                int l = ((int) (byte3));
                l &= 0xff;
                l -= 127;
                j += l;
                out((short)j);
            }
            break;

        case 112: // 'p'
            int i1 = ((int) (getLastByte()));
            i1 &= 0xff;
            do
            {
                byte byte4 = readByte();
                int j1 = ((int) (byte4));
                j1 >>= 4;
                j1 &= 0xf;
                j1 -= 7;
                i1 += j1;
                byte byte7 = (byte)i1;
                out(byte7);
                if(--i == 0)
                    break label0;
                j1 = ((int) (byte4));
                j1 &= 0xf;
                j1 -= 7;
                i1 += j1;
                byte7 = (byte)i1;
                out(byte7);
            } while(--i != 0);
            break;

        case 12: // '\f'
            int k = ((int) (getLastShort()));
            k &= 0xffff;
            do
            {
                byte byte5 = readByte();
                int k1 = ((int) (byte5));
                k1 >>= 4;
                k1 &= 0xf;
                k1 -= 7;
                k += k1;
                out((short)k);
                if(--i == 0)
                    break label0;
                k1 = ((int) (byte5));
                k1 &= 0xf;
                k1 -= 7;
                k += k1;
                out((short)k);
            } while(--i != 0);
            break;

        case 110: // 'n'
            byte byte6 = getLastByte();
            while(i-- > 0) 
                out(byte6);
            break;

        case 10: // '\n'
            short word0 = getLastShort();
            while(i-- > 0) 
                out(word0);
            break;

        default:
            throw new TianiInvalidFormatException("illegal function code while decompressing: " + byte11);
        }
    }

    private final void out(byte byte0)
    {
        lB = byte0;
        outCnt++;
        outBuff[outPos++] = byte0;
    }

    private final void out(short word0)
    {
        lS = word0;
        outCnt += 2;
        outBuff[outPos++] = (byte)(word0 & 0xff);
        outBuff[outPos++] = (byte)(word0 >> 8);
    }

    private final void out(byte byte0, byte byte1)
    {
        short word0 = (short)(byte0 << 8 | byte1 & 0xff);
        out(word0);
    }

    private final byte readByte()
        throws IOException
    {
        int i = super.read();
        if(i == -1)
        {
            throw new TianiInvalidFormatException("Unexpected end of file");
        } else
        {
            inCnt++;
            return (byte)i;
        }
    }

    private final byte getLastByte()
    {
        return lB;
    }

    private final short getLastShort()
    {
        return lS;
    }
}
