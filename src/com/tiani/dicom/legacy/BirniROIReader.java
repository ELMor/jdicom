// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BirniROIReader.java

package com.tiani.dicom.legacy;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.legacy:
//            TianiInvalidFormatException

public class BirniROIReader extends BufferedReader
{
    private static final class ROIText
    {

        public final int start[];
        public final String text;

        public ROIText(int ai[], String s)
        {
            start = ai;
            text = s;
        }
    }

    private static final class ROI
    {

        public final Vector draws;
        public final Vector texts;

        private ROI()
        {
            draws = new Vector();
            texts = new Vector();
        }

    }


    private static int _boxWidth = 0;
    private static int _boxHeight = 0;
    private String _line;
    private String _tokens[];
    private Hashtable _rois;

    public BirniROIReader(InputStream inputstream)
    {
        super(((java.io.Reader) (new InputStreamReader(inputstream))));
        _rois = new Hashtable();
    }

    public void read(DicomObject dicomobject)
        throws IOException, TianiInvalidFormatException, DicomException
    {
        parse();
        fill(dicomobject);
    }

    public static void setBoundingBoxDimension(int i, int j)
    {
        _boxWidth = i;
        _boxHeight = j;
    }

    private void parse()
        throws IOException, TianiInvalidFormatException
    {
        int i = 0;
        int j = 0;
        Vector vector = null;
        Vector vector1 = null;
        int ai[] = null;
        byte byte0 = 0;
        while((_line = ((BufferedReader)this).readLine()) != null) 
            if(byte0 == 0)
            {
                if(_line.equals("] BIRNI"))
                    return;
                if(_line.equals("[ OBJECT_LH"))
                    byte0 = 1;
                else
                if(_line.equals("[ TEXT_LH"))
                    byte0 = 9;
            } else
            {
                StringTokenizer stringtokenizer = new StringTokenizer(_line);
                int k = stringtokenizer.countTokens();
                if(k != 0)
                {
                    _tokens = new String[k];
                    for(int l = 0; l < k; l++)
                        _tokens[l] = stringtokenizer.nextToken();

                    switch(byte0)
                    {
                    default:
                        break;

                    case 1: // '\001'
                        if(isToken("[", "OBJECT"))
                        {
                            byte0 = 2;
                            break;
                        }
                        if(isToken("]", "OBJECT_LH"))
                            byte0 = 0;
                        break;

                    case 2: // '\002'
                        tkToInt("#IDENTIFY");
                        byte0 = 3;
                        break;

                    case 3: // '\003'
                        tkToInt("#COUNT");
                        byte0 = 4;
                        break;

                    case 4: // '\004'
                        vector = getROI(tkToKey("#CREATE")).draws;
                        byte0 = 5;
                        break;

                    case 5: // '\005'
                        i = tkToInt("[", "DRAWS");
                        byte0 = ((byte)(i <= 0 ? 8 : 6));
                        break;

                    case 6: // '\006'
                        vector.addElement(((Object) (tkTo2Int("#START"))));
                        byte0 = 7;
                        break;

                    case 7: // '\007'
                        int ai1[] = tkTo2Int("#END");
                        if(--i > 0)
                        {
                            byte0 = 6;
                        } else
                        {
                            vector.addElement(((Object) (ai1)));
                            byte0 = 8;
                        }
                        break;

                    case 8: // '\b'
                        checkToken("]", "DRAWS");
                        byte0 = 1;
                        break;

                    case 9: // '\t'
                        if(isToken("[", "TEXT"))
                        {
                            byte0 = 10;
                            break;
                        }
                        if(isToken("]", "TEXT_LH"))
                        {
                            byte0 = 0;
                            break;
                        }
                        if(isToken("[", "EDITOR_INFO"))
                            byte0 = 19;
                        break;

                    case 19: // '\023'
                        if(isToken("]", "EDITOR_INFO"))
                            byte0 = 9;
                        break;

                    case 10: // '\n'
                        tkToInt("#IDENTIFY");
                        byte0 = 11;
                        break;

                    case 11: // '\013'
                        tkToInt("#COUNT");
                        byte0 = 12;
                        break;

                    case 12: // '\f'
                        vector1 = getROI(tkToKey("#CREATE")).texts;
                        byte0 = 13;
                        break;

                    case 13: // '\r'
                        j = tkToInt("[", "WORDS");
                        byte0 = ((byte)(j <= 0 ? 18 : 14));
                        break;

                    case 14: // '\016'
                        ai = tkTo2Int("#START");
                        byte0 = 15;
                        break;

                    case 15: // '\017'
                        String s = tkToText();
                        if(s.length() > 0)
                            vector1.addElement(((Object) (new ROIText(ai, s))));
                        byte0 = 16;
                        break;

                    case 16: // '\020'
                        tkToInt("#FONT");
                        byte0 = 17;
                        break;

                    case 17: // '\021'
                        tkToInt("#DIR");
                        byte0 = ((byte)(--j <= 0 ? 18 : 14));
                        break;

                    case 18: // '\022'
                        checkToken("]", "WORDS");
                        byte0 = 9;
                        break;
                    }
                }
            }
        throw new TianiInvalidFormatException("Unexpected end of file");
    }

    private boolean isToken(String s, String s1)
    {
        return _tokens.length == 2 && _tokens[0].equals(((Object) (s))) && _tokens[1].equals(((Object) (s1)));
    }

    private void checkToken(String s, String s1)
        throws TianiInvalidFormatException
    {
        if(!isToken(s, s1))
            throw new TianiInvalidFormatException(_line);
        else
            return;
    }

    private String tkToKey(String s)
        throws TianiInvalidFormatException
    {
        if(_tokens.length != 2 || !_tokens[0].equals(((Object) (s))))
            throw new TianiInvalidFormatException(_line);
        else
            return _tokens[1];
    }

    private int tkToInt(String s)
        throws TianiInvalidFormatException
    {
        if(_tokens.length == 2 && _tokens[0].equals(((Object) (s))))
            try
            {
                return Integer.parseInt(_tokens[1]);
            }
            catch(NumberFormatException numberformatexception) { }
        throw new TianiInvalidFormatException(_line);
    }

    private int tkToInt(String s, String s1)
        throws TianiInvalidFormatException
    {
        if(_tokens.length == 3 && _tokens[0].equals(((Object) (s))) && _tokens[1].equals(((Object) (s1))))
            try
            {
                return Integer.parseInt(_tokens[2]);
            }
            catch(NumberFormatException numberformatexception) { }
        throw new TianiInvalidFormatException(_line);
    }

    private int[] tkTo2Int(String s)
        throws TianiInvalidFormatException
    {
        if(_tokens.length == 3 && _tokens[0].equals(((Object) (s))))
            try
            {
                return (new int[] {
                    Integer.parseInt(_tokens[1]), Integer.parseInt(_tokens[2])
                });
            }
            catch(NumberFormatException numberformatexception) { }
        throw new TianiInvalidFormatException(_line);
    }

    private String tkToText()
        throws TianiInvalidFormatException
    {
        if(!_tokens[0].equals("#TEXT"))
            throw new TianiInvalidFormatException(_line);
        else
            return _line.substring(_line.indexOf("#TEXT") + 6);
    }

    private ROI getROI(String s)
    {
        ROI roi = (ROI)_rois.get(((Object) (s)));
        if(roi == null)
        {
            roi = new ROI();
            _rois.put(((Object) (s)), ((Object) (roi)));
        }
        return roi;
    }

    private void fill(DicomObject dicomobject)
        throws DicomException
    {
        dicomobject.set(57, "ISO_IR 100");
        ROI roi;
        DicomObject dicomobject2;
        for(Enumeration enumeration = _rois.keys(); enumeration.hasMoreElements(); fillTexts(dicomobject2, roi.texts))
        {
            String s = (String)enumeration.nextElement();
            roi = (ROI)_rois.get(((Object) (s)));
            DicomObject dicomobject1 = new DicomObject();
            dicomobject.append(1399, ((Object) (dicomobject1)));
            int i = dicomobject.getSize(1399);
            dicomobject1.set(1382, ((Object) (s)));
            dicomobject1.set(1400, ((Object) (new Integer(i))));
            dicomobject1.set(1402, "BIRNI_ROI");
            dicomobject2 = new DicomObject();
            dicomobject.append(1381, ((Object) (dicomobject2)));
            dicomobject2.set(1382, ((Object) (s)));
            fillDraws(dicomobject2, roi.draws);
        }

    }

    private void fillDraws(DicomObject dicomobject, Vector vector)
        throws DicomException
    {
        int i = vector.size();
        if(i == 0)
            return;
        DicomObject dicomobject1 = new DicomObject();
        dicomobject.append(1388, ((Object) (dicomobject1)));
        dicomobject1.set(1385, "PIXEL");
        dicomobject1.set(1393, ((Object) (new Integer(2))));
        dicomobject1.set(1394, ((Object) (new Integer(i))));
        int ai[];
        for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); dicomobject1.append(1395, ((Object) (new Float(ai[1])))))
        {
            ai = (int[])enumeration.nextElement();
            dicomobject1.append(1395, ((Object) (new Float(ai[0]))));
        }

        dicomobject1.set(1396, "POLYLINE");
        dicomobject1.set(1397, "N");
    }

    private void fillTexts(DicomObject dicomobject, Vector vector)
        throws DicomException
    {
        int i = vector.size();
        if(i == 0)
            return;
        DicomObject dicomobject1;
        for(Enumeration enumeration = vector.elements(); enumeration.hasMoreElements(); dicomobject1.set(1409, "LEFT"))
        {
            dicomobject1 = new DicomObject();
            dicomobject.append(1387, ((Object) (dicomobject1)));
            dicomobject1.set(1383, "PIXEL");
            ROIText roitext = (ROIText)enumeration.nextElement();
            dicomobject1.set(1386, ((Object) (roitext.text)));
            int ai[] = roitext.start;
            dicomobject1.set(1389, ((Object) (new Float(ai[0]))), 0);
            dicomobject1.set(1389, ((Object) (new Float(ai[1] + _boxHeight))), 1);
            dicomobject1.set(1390, ((Object) (new Float(ai[0] + _boxWidth))), 0);
            dicomobject1.set(1390, ((Object) (new Float(ai[1]))), 1);
        }

    }

}
