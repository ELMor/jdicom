// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Acceptor.java

package com.tiani.dicom.framework;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.framework:
//            IAcceptorListener, IAcceptancePolicy, IAssociationListener, VerboseAssociation

public class Acceptor
    implements Runnable
{
    private class AcceptorChild
        implements Runnable
    {

        Socket _socket;
        VerboseAssociation _as;

        public void abort()
            throws IOException
        {
            if(_as == null || !_as.isOpen())
            {
                return;
            } else
            {
                _as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER, Abort.DICOM_UL_SERVICE_USER);
                _as.closesocket();
                return;
            }
        }

        public void run()
        {
            _acceptorChilds.addElement(((Object) (this)));
            try
            {
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _socket.getInetAddress() + " Enter AcceptorChild.run()");
                _as = new VerboseAssociation(_socket);
                for(Enumeration enumeration = _associationListeners.elements(); enumeration.hasMoreElements(); _as.addAssociationListener((IAssociationListener)enumeration.nextElement()));
                _socket.setSoTimeout(_artimAssoc);
                Request request;
                try
                {
                    request = _as.receiveAssociateRequest();
                }
                catch(InterruptedIOException interruptedioexception)
                {
                    _socket.close();
                    throw new DicomException("ARTIM timer expired before receiving A-ASSOCIATE-RQ");
                }
                _socket.setSoTimeout(0);
                Response response = _acceptancePolicy.prepareResponse(_as, request);
                _as.sendAssociateResponse(response);
                if(response instanceof Acknowledge)
                    _acceptorListener.handle((Acknowledge)response, _as);
                else
                    _as.closesocket(_artimRelease);
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _socket.getInetAddress() + " Leave AcceptorChild.run()");
            }
            catch(Exception exception)
            {
                ((Throwable) (exception)).printStackTrace(Debug.out);
            }
            finally
            {
                _as = null;
                _acceptorChilds.removeElement(((Object) (this)));
            }
        }

        AcceptorChild(Socket socket)
        {
            _as = null;
            _socket = socket;
        }
    }


    private ServerSocket _serverSocket;
    private boolean _startThread;
    private String _calledTitle;
    private String _callingTitles[];
    private int _artimRelease;
    private int _artimAssoc;
    private int _asids[];
    private int _maxOperationsInvoked;
    private int _maxOperationsPerformed;
    private IAcceptorListener _acceptorListener;
    private int _tsid;
    private boolean _rejectnocontext;
    private Vector _associationListeners;
    private Vector _acceptorChilds;
    private IAcceptancePolicy _acceptancePolicy = new IAcceptancePolicy() {

        public Response prepareResponse(VerboseAssociation verboseassociation, Request request)
            throws IllegalValueException
        {
            Response response = ResponsePolicy.prepareResponse(request, _calledTitle, _callingTitles, _asids, _tsid, _rejectnocontext);
            if(!(response instanceof Acknowledge))
            {
                return response;
            } else
            {
                int k = request.getMaxOperationsInvoked();
                int l = request.getMaxOperationsPerformed();
                int i1 = k != 0 && (_maxOperationsInvoked == 0 || k <= _maxOperationsInvoked) ? k : _maxOperationsInvoked;
                int j1 = l != 0 && (_maxOperationsPerformed == 0 || l <= _maxOperationsPerformed) ? l : _maxOperationsPerformed;
                Acknowledge acknowledge = (Acknowledge)response;
                acknowledge.setMaxOperationsInvoked(i1);
                acknowledge.setMaxOperationsPerformed(j1);
                return ((Response) (acknowledge));
            }
        }

    };

    /**
     * @deprecated Method Acceptor is deprecated
     */

    public Acceptor(ServerSocket serversocket, boolean flag, int ai[], int i, int j, IAcceptorListener iacceptorlistener)
    {
        _artimRelease = 1000;
        _artimAssoc = 2000;
        _asids = null;
        _maxOperationsInvoked = 1;
        _maxOperationsPerformed = 1;
        _tsid = 8193;
        _rejectnocontext = true;
        _associationListeners = new Vector();
        _acceptorChilds = new Vector();
        _serverSocket = serversocket;
        _startThread = flag;
        _asids = ai;
        _maxOperationsInvoked = i;
        _maxOperationsPerformed = j;
        _acceptorListener = iacceptorlistener;
    }

    public Acceptor(ServerSocket serversocket, boolean flag, IAcceptancePolicy iacceptancepolicy, IAcceptorListener iacceptorlistener)
    {
        _artimRelease = 1000;
        _artimAssoc = 2000;
        _asids = null;
        _maxOperationsInvoked = 1;
        _maxOperationsPerformed = 1;
        _tsid = 8193;
        _rejectnocontext = true;
        _associationListeners = new Vector();
        _acceptorChilds = new Vector();
        _serverSocket = serversocket;
        _startThread = flag;
        _acceptancePolicy = iacceptancepolicy;
        _acceptorListener = iacceptorlistener;
    }

    public void setARTIM(int i)
    {
        setARTIM(i, i);
    }

    public void setARTIM(int i, int j)
    {
        _artimAssoc = i;
        _artimRelease = j;
    }

    public void setStartThread(boolean flag)
    {
        _startThread = flag;
    }

    public boolean isStartThread(boolean flag)
    {
        return _startThread;
    }

    public void setAcceptorListener(IAcceptorListener iacceptorlistener)
    {
        _acceptorListener = iacceptorlistener;
    }

    /**
     * @deprecated Method setCalledTitle is deprecated
     */

    public void setCalledTitle(String s)
    {
        _calledTitle = s;
    }

    /**
     * @deprecated Method setCallingTitles is deprecated
     */

    public void setCallingTitles(String as[])
    {
        _callingTitles = as;
    }

    public void setAcceptancePolicy(IAcceptancePolicy iacceptancepolicy)
    {
        _acceptancePolicy = iacceptancepolicy;
    }

    public IAcceptancePolicy getAcceptancePolicy()
    {
        return _acceptancePolicy;
    }

    public void addAssociationListener(IAssociationListener iassociationlistener)
    {
        _associationListeners.addElement(((Object) (iassociationlistener)));
    }

    public void removeAssociationListener(IAssociationListener iassociationlistener)
    {
        _associationListeners.removeElement(((Object) (iassociationlistener)));
    }

    public void run()
    {
        try
        {
            do
            {
                Socket socket = _serverSocket.accept();
                AcceptorChild acceptorchild = new AcceptorChild(socket);
                if(_startThread)
                {
                    if(Debug.DEBUG > 1)
                        Debug.out.println("jdicom: " + socket.getInetAddress() + " Start new Thread for AcceptorChild.run()");
                    Thread thread = new Thread(((Runnable) (acceptorchild)));
                    thread.start();
                } else
                {
                    acceptorchild.run();
                }
            } while(true);
        }
        catch(Exception exception)
        {
            Debug.out.println("jdicom: " + exception);
        }
    }

    public void stop(boolean flag)
    {
        try
        {
            _serverSocket.close();
            if(!flag)
                return;
            AcceptorChild aacceptorchild[];
            synchronized(_acceptorChilds)
            {
                aacceptorchild = new AcceptorChild[_acceptorChilds.size()];
                _acceptorChilds.copyInto(((Object []) (aacceptorchild)));
            }
            for(int i = 0; i < aacceptorchild.length; i++)
                aacceptorchild[i].abort();

        }
        catch(IOException ioexception)
        {
            Debug.out.println("jdicom: " + ioexception);
        }
    }













}
