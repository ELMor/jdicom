// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DimseRqManager.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package com.tiani.dicom.framework:
//            IDimseRqListener, DicomMessage, DimseExchange, VerboseAssociation

public class DimseRqManager
{

    private Hashtable cEchoScpReg;
    private Hashtable cStoreScpReg;
    private Hashtable cFindScpReg;
    private Hashtable cGetScpReg;
    private Hashtable cMoveScpReg;
    private Hashtable nEventReportScuReg;
    private Hashtable nGetScpReg;
    private Hashtable nSetScpReg;
    private Hashtable nActionScpReg;
    private Hashtable nCreateScpReg;
    private Hashtable nDeleteScpReg;

    public DimseRqManager()
    {
        cEchoScpReg = new Hashtable();
        cStoreScpReg = new Hashtable();
        cFindScpReg = new Hashtable();
        cGetScpReg = new Hashtable();
        cMoveScpReg = new Hashtable();
        nEventReportScuReg = new Hashtable();
        nGetScpReg = new Hashtable();
        nSetScpReg = new Hashtable();
        nActionScpReg = new Hashtable();
        nCreateScpReg = new Hashtable();
        nDeleteScpReg = new Hashtable();
    }

    public void regCEchoScp(String s, IDimseRqListener idimserqlistener)
    {
        cEchoScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regCStoreScp(String s, IDimseRqListener idimserqlistener)
    {
        cStoreScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regCFindScp(String s, IDimseRqListener idimserqlistener)
    {
        cFindScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regCGetScp(String s, IDimseRqListener idimserqlistener)
    {
        cGetScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regCMoveScp(String s, IDimseRqListener idimserqlistener)
    {
        cMoveScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNEventReportScu(String s, IDimseRqListener idimserqlistener)
    {
        nEventReportScuReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNGetScp(String s, IDimseRqListener idimserqlistener)
    {
        nGetScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNSetScp(String s, IDimseRqListener idimserqlistener)
    {
        nSetScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNActionScp(String s, IDimseRqListener idimserqlistener)
    {
        nActionScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNCreateScp(String s, IDimseRqListener idimserqlistener)
    {
        nCreateScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void regNDeleteScp(String s, IDimseRqListener idimserqlistener)
    {
        nDeleteScpReg.put(((Object) (s)), ((Object) (idimserqlistener)));
    }

    public void handleRQ(DimseExchange dimseexchange, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        Object obj = null;
        Object obj1 = null;
        int i = ((DicomObject) (dicommessage)).getI(3);
        if(i == 4095)
        {
            int j = ((DicomObject) (dicommessage)).getI(5);
            boolean flag = handleCancelRQwith(dimseexchange, j, cFindScpReg) || handleCancelRQwith(dimseexchange, j, cGetScpReg) || handleCancelRQwith(dimseexchange, j, cMoveScpReg);
        } else
        {
            IDimseRqListener idimserqlistener;
            String s;
            switch(i)
            {
            case 48: // '0'
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)cEchoScpReg.get(((Object) (s)));
                break;

            case 1: // '\001'
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)cStoreScpReg.get(((Object) (s)));
                break;

            case 16: // '\020'
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)cGetScpReg.get(((Object) (s)));
                break;

            case 32: // ' '
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)cFindScpReg.get(((Object) (s)));
                break;

            case 33: // '!'
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)cMoveScpReg.get(((Object) (s)));
                break;

            case 256: 
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)nEventReportScuReg.get(((Object) (s)));
                break;

            case 320: 
                s = ((DicomObject) (dicommessage)).getS(1);
                idimserqlistener = (IDimseRqListener)nCreateScpReg.get(((Object) (s)));
                break;

            case 272: 
                s = ((DicomObject) (dicommessage)).getS(2);
                idimserqlistener = (IDimseRqListener)nGetScpReg.get(((Object) (s)));
                break;

            case 288: 
                s = ((DicomObject) (dicommessage)).getS(2);
                idimserqlistener = (IDimseRqListener)nSetScpReg.get(((Object) (s)));
                break;

            case 304: 
                s = ((DicomObject) (dicommessage)).getS(2);
                idimserqlistener = (IDimseRqListener)nActionScpReg.get(((Object) (s)));
                break;

            case 336: 
                s = ((DicomObject) (dicommessage)).getS(2);
                idimserqlistener = (IDimseRqListener)nDeleteScpReg.get(((Object) (s)));
                break;

            default:
                throw new DicomException();
            }
            int k = ((DicomObject) (dicommessage)).getI(4);
            if(idimserqlistener != null)
                idimserqlistener.handleRQ(dimseexchange, k, s, dicommessage);
            else
                sopClassNotSupported(dimseexchange, k, s, dicommessage);
        }
    }

    protected boolean handleCancelRQwith(DimseExchange dimseexchange, int i, Hashtable hashtable)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        for(Enumeration enumeration = hashtable.elements(); enumeration.hasMoreElements();)
            if(((IDimseRqListener)enumeration.nextElement()).handleCancelRQ(dimseexchange, i))
                return true;

        return false;
    }

    protected void sopClassNotSupported(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), ((DicomObject) (dicommessage)).getI(3) | 0x8000, i, ((DicomObject) (null)));
        dicommessage1.status(290);
        dimseexchange.getAssociation().sendMessage(dicommessage1);
    }
}
