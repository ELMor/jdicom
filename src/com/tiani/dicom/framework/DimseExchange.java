// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DimseExchange.java

package com.tiani.dicom.framework;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Association;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, FutureDimseRsp, IDimseRspListener, VerboseAssociation, 
//            DimseRqManager, Status, IAssociationListener

public class DimseExchange
    implements Runnable
{
    private final class Scheduler
        implements Runnable
    {

        public void run()
        {
            try
            {
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _remoteAET + " Enter DimseExchange.Scheduler.run()");
                DicomMessage dicommessage;
                while((dicommessage = (DicomMessage)_queue.peek()) != null) 
                {
                    if((((DicomObject) (dicommessage)).getI(3) & 0x8000) == 0)
                        _rqManager.handleRQ(DimseExchange.this, dicommessage);
                    else
                        handleRSP(DimseExchange.this, dicommessage);
                    _queue.pop();
                }
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _remoteAET + " Leave DimseExchange.Scheduler.run()");
            }
            catch(Exception exception)
            {
                if(Debug.DEBUG > 2)
                    ((Throwable) (exception)).printStackTrace(Debug.out);
                Debug.out.println("jdicom: Scheduler.run: " + ((Throwable) (exception)).toString() + ": " + ((Throwable) (exception)).getMessage());
            }
            finally
            {
                _queue = null;
            }
        }

        private Scheduler()
        {
        }

    }

    private final class BlockingHashtable extends Hashtable
    {

        private int _maxOperationsInvoked;

        public synchronized Object add(Object obj, Object obj1)
            throws InterruptedException
        {
            if(_maxOperationsInvoked != 0)
                for(; _as.isOpen() && super.size() >= _maxOperationsInvoked; ((Object)this).wait());
            return super.put(obj, obj1);
        }

        public synchronized Object remove(Object obj)
        {
            Object obj1 = super.remove(obj);
            ((Object)this).notifyAll();
            return obj1;
        }

        public synchronized void waitUntilEmpty()
            throws InterruptedException
        {
            for(; _as.isOpen() && !super.isEmpty(); ((Object)this).wait());
        }

        public BlockingHashtable(int i)
        {
            _maxOperationsInvoked = i;
        }
    }

    private final class BlockingVector extends Vector
    {

        public synchronized Object peek()
            throws InterruptedException
        {
            for(; ((Vector)this).isEmpty(); ((Object)this).wait());
            return ((Vector)this).firstElement();
        }

        public synchronized void pop()
        {
            ((Vector)this).removeElementAt(0);
            ((Object)this).notifyAll();
        }

        public synchronized void write(Object obj)
        {
            ((Vector)this).addElement(obj);
            ((Object)this).notifyAll();
        }

        public synchronized void waitUntilEmpty()
            throws InterruptedException
        {
            for(; !((Vector)this).isEmpty(); ((Object)this).wait());
        }

        private BlockingVector()
        {
        }

    }


    private static final IDimseRspListener _DEF_RSP_LISTENER = new IDimseRspListener() {

        public void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
        {
        }

    };
    private VerboseAssociation _as;
    private int _artim;
    private DimseRqManager _rqManager;
    private boolean _queueRQ;
    private boolean _queueRSP;
    private BlockingHashtable _dimseSCUs;
    private BlockingVector _queue;
    private Socket _socket;
    private String _remoteAET;
    private String _localAET;
    private Scheduler _scheduler;

    public DimseExchange(VerboseAssociation verboseassociation, DimseRqManager dimserqmanager, boolean flag, boolean flag1, int i)
    {
        _artim = 1000;
        _dimseSCUs = null;
        _queue = null;
        _scheduler = new Scheduler();
        _as = verboseassociation;
        _rqManager = dimserqmanager;
        _queueRQ = flag;
        _queueRSP = flag1;
        _dimseSCUs = new BlockingHashtable(i);
        _socket = _as.socket();
        _localAET = _as.localAET();
        _remoteAET = _as.remoteAET();
    }

    public void setARTIM(int i)
    {
        _artim = i;
    }

    public final VerboseAssociation getAssociation()
    {
        return _as;
    }

    public final String localAET()
    {
        return _localAET;
    }

    public final String remoteAET()
    {
        return _remoteAET;
    }

    public final int nextMessageID()
    {
        return _as.nextMessageID();
    }

    public final boolean isOpen()
    {
        return _as.isOpen();
    }

    public final byte getPresentationContext(int i, int j)
        throws IllegalValueException
    {
        return ((Association) (_as)).getPresentationContext(i, j);
    }

    public final byte getPresentationContext(int i)
        throws IllegalValueException
    {
        return ((Association) (_as)).getPresentationContext(i);
    }

    public final UIDEntry getCurrentAbstractSyntax()
        throws IllegalValueException
    {
        return ((Association) (_as)).getCurrentAbstractSyntax();
    }

    public final byte[] listAcceptedPresentationContexts(int i)
        throws IllegalValueException
    {
        return ((Association) (_as)).listAcceptedPresentationContexts(i);
    }

    public final UIDEntry[] listAcceptedTransferSyntaxes(int i)
        throws IllegalValueException
    {
        return ((Association) (_as)).listAcceptedTransferSyntaxes(i);
    }

    public final UIDEntry getTransferSyntax(byte byte0)
        throws IllegalValueException
    {
        return ((Association) (_as)).getTransferSyntax(byte0);
    }

    public void addAssociationListener(IAssociationListener iassociationlistener)
    {
        _as.addAssociationListener(iassociationlistener);
    }

    public void sendCancelRQ(int i, int j)
        throws IOException, IllegalValueException, DicomException
    {
        DicomMessage dicommessage = new DicomMessage(4095, i, ((DicomObject) (null)));
        ((Association) (_as)).send(j, ((DicomObject) (dicommessage)), ((DicomObject) (null)));
    }

    public void sendRQ(int i, int j, DicomMessage dicommessage, IDimseRspListener idimsersplistener)
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        _dimseSCUs.add(((Object) (new Integer(i))), ((Object) (idimsersplistener == null ? ((Object) (_DEF_RSP_LISTENER)) : ((Object) (idimsersplistener)))));
        ((Association) (_as)).send(j, ((DicomObject) (dicommessage)), dicommessage.getDataset());
    }

    public DicomMessage sendRQ(int i, int j, DicomMessage dicommessage)
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        FutureDimseRsp futuredimsersp = new FutureDimseRsp();
        _as.addAssociationListener(((IAssociationListener) (futuredimsersp)));
        try
        {
            sendRQ(i, j, dicommessage, ((IDimseRspListener) (futuredimsersp)));
            DicomMessage dicommessage1 = futuredimsersp.getResponse();
            return dicommessage1;
        }
        finally
        {
            _as.removeAssociationListener(((IAssociationListener) (futuredimsersp)));
        }
    }

    public void sendRQ(int i, DicomMessage dicommessage, IDimseRspListener idimsersplistener)
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        _dimseSCUs.add(((Object) (new Integer(i))), ((Object) (idimsersplistener == null ? ((Object) (_DEF_RSP_LISTENER)) : ((Object) (idimsersplistener)))));
        _as.sendInPresentationContext(dicommessage.getPresentationContext(), ((DicomObject) (dicommessage)), dicommessage.getDataset());
    }

    public DicomMessage sendRQ(int i, DicomMessage dicommessage)
        throws InterruptedException, IOException, IllegalValueException, DicomException
    {
        FutureDimseRsp futuredimsersp = new FutureDimseRsp();
        _as.addAssociationListener(((IAssociationListener) (futuredimsersp)));
        try
        {
            sendRQ(i, dicommessage, ((IDimseRspListener) (futuredimsersp)));
            DicomMessage dicommessage1 = futuredimsersp.getResponse();
            return dicommessage1;
        }
        finally
        {
            _as.removeAssociationListener(((IAssociationListener) (futuredimsersp)));
        }
    }

    public DicomMessage cecho()
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(48, i, ((DicomObject) (null)));
        dicommessage.affectedSOPclassUID("1.2.840.10008.1.1");
        return sendRQ(i, 4097, dicommessage);
    }

    public DicomMessage cstore(byte byte0, String s, String s1, int i, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 1, j, dicomobject);
        dicommessage.affectedSOP(s, s1);
        dicommessage.priority(i);
        return sendRQ(j, dicommessage);
    }

    public DicomMessage cstore(byte byte0, String s, String s1, int i, String s2, int j, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int k = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 1, k, dicomobject);
        dicommessage.affectedSOP(s, s1);
        dicommessage.priority(i);
        dicommessage.moveOriginator(s2, j);
        return sendRQ(k, dicommessage);
    }

    public DicomMessage neventReport(byte byte0, String s, String s1, int i, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 256, j, dicomobject);
        dicommessage.affectedSOP(s, s1);
        ((DicomObject) (dicommessage)).set(15, ((Object) (new Integer(i))));
        return sendRQ(j, dicommessage);
    }

    public DicomMessage ncreate(byte byte0, String s, String s1, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 320, i, dicomobject);
        dicommessage.affectedSOP(s, s1);
        return sendRQ(i, dicommessage);
    }

    public DicomMessage nset(byte byte0, String s, String s1, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 288, i, dicomobject);
        dicommessage.requestedSOP(s, s1);
        return sendRQ(i, dicommessage);
    }

    public DicomMessage nget(byte byte0, String s, String s1, int ai[])
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 272, i, ((DicomObject) (null)));
        dicommessage.requestedSOP(s, s1);
        if(ai != null)
            dicommessage.attributeIdList(ai);
        return sendRQ(i, dicommessage);
    }

    public DicomMessage naction(byte byte0, String s, String s1, int i, DicomObject dicomobject)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 304, j, dicomobject);
        dicommessage.requestedSOP(s, s1);
        ((DicomObject) (dicommessage)).set(18, ((Object) (new Integer(i))));
        return sendRQ(j, dicommessage);
    }

    public DicomMessage ndelete(byte byte0, String s, String s1)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 336, i, ((DicomObject) (null)));
        dicommessage.requestedSOP(s, s1);
        return sendRQ(i, dicommessage);
    }

    public void cstoreAsync(byte byte0, String s, String s1, int i, DicomObject dicomobject, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 1, j, dicomobject);
        dicommessage.affectedSOP(s, s1);
        dicommessage.priority(i);
        sendRQ(j, dicommessage, idimsersplistener);
    }

    public void cstoreAsync(byte byte0, String s, String s1, int i, String s2, int j, DicomObject dicomobject, 
            IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int k = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 1, k, dicomobject);
        dicommessage.affectedSOP(s, s1);
        dicommessage.priority(i);
        dicommessage.moveOriginator(s2, j);
        sendRQ(k, dicommessage, idimsersplistener);
    }

    public void neventReportAsync(byte byte0, String s, String s1, int i, DicomObject dicomobject, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 256, j, dicomobject);
        dicommessage.affectedSOP(s, s1);
        ((DicomObject) (dicommessage)).set(15, ((Object) (new Integer(i))));
        sendRQ(j, dicommessage, idimsersplistener);
    }

    public void ncreateAsync(byte byte0, String s, String s1, DicomObject dicomobject, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 320, i, dicomobject);
        dicommessage.affectedSOP(s, s1);
        sendRQ(i, dicommessage, idimsersplistener);
    }

    public void nsetAsync(byte byte0, String s, String s1, DicomObject dicomobject, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 288, i, dicomobject);
        dicommessage.requestedSOP(s, s1);
        sendRQ(i, dicommessage, idimsersplistener);
    }

    public void ngetAsync(byte byte0, String s, String s1, int ai[], IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 272, i, ((DicomObject) (null)));
        dicommessage.requestedSOP(s, s1);
        if(ai != null)
            dicommessage.attributeIdList(ai);
        sendRQ(i, dicommessage, idimsersplistener);
    }

    public void nactionAsync(byte byte0, String s, String s1, int i, DicomObject dicomobject, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int j = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 304, j, dicomobject);
        dicommessage.requestedSOP(s, s1);
        ((DicomObject) (dicommessage)).set(18, ((Object) (new Integer(i))));
        sendRQ(j, dicommessage, idimsersplistener);
    }

    public void ndeleteAsync(byte byte0, String s, String s1, IDimseRspListener idimsersplistener)
        throws IOException, IllegalValueException, DicomException, InterruptedException
    {
        int i = nextMessageID();
        DicomMessage dicommessage = new DicomMessage(byte0, 336, i, ((DicomObject) (null)));
        dicommessage.requestedSOP(s, s1);
        sendRQ(i, dicommessage, idimsersplistener);
    }

    public void waitForAllRSP()
        throws InterruptedException
    {
        _dimseSCUs.waitUntilEmpty();
    }

    public void run()
    {
        try
        {
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + _remoteAET + " Enter DimseExchange.run()");
            if(_queueRQ || _queueRSP)
            {
                Thread thread = new Thread(((Runnable) (_scheduler)));
                _queue = new BlockingVector();
                thread.start();
            }
            while(_as.isOpen()) 
            {
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _remoteAET + " Waiting for PDU");
                int i = ((Association) (_as)).peek();
                if(Debug.DEBUG > 1)
                    Debug.out.println("jdicom: " + _remoteAET + " PDU received");
                switch(i)
                {
                case 10: // '\n'
                    DicomObject dicomobject = _as.receiveCommand();
                    DicomObject dicomobject1 = null;
                    if(dicomobject.getI(8) != 257)
                        dicomobject1 = _as.receiveData();
                    DicomMessage dicommessage = new DicomMessage((byte)((Association) (_as)).getCurrentPresentationContext(), ((Association) (_as)).getCurrentAbstractSyntax(), dicomobject, dicomobject1);
                    int j = dicomobject.getI(3);
                    if((j & 0x8000) == 0)
                    {
                        if(_rqManager == null)
                            throw new DicomException("Error: Received unexpected DIMSE-RQ");
                        if(_queueRQ && j != 4095)
                            _queue.write(((Object) (dicommessage)));
                        else
                            _rqManager.handleRQ(this, dicommessage);
                    } else
                    if(_queueRSP)
                        _queue.write(((Object) (dicommessage)));
                    else
                        handleRSP(this, dicommessage);
                    break;

                case 11: // '\013'
                    _as.receiveReleaseRequest();
                    _as.sendReleaseResponse();
                    _as.closesocket(_artim);
                    return;

                case 13: // '\r'
                    _as.receiveReleaseResponse();
                    _as.closesocket();
                    return;

                case 12: // '\f'
                    _as.receiveAbort();
                    _as.closesocket();
                    return;

                default:
                    Debug.out.println("jdicom: DimseExchange.run: receive unrecognized PDU 0x" + Integer.toHexString(i));
                    _as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER, Abort.UNRECOGNIZED_PDU);
                    _as.closesocket(_artim);
                    return;
                }
            }
        }
        catch(Throwable throwable)
        {
            if(Debug.DEBUG > 1)
                throwable.printStackTrace(Debug.out);
            else
                Debug.out.println("jdicom: " + throwable.getMessage());
            if(_as.isOpen())
                try
                {
                    _as.sendAbort(Abort.DICOM_UL_SERVICE_PROVIDER, Abort.REASON_NOT_SPECIFIED);
                    _as.closesocket(_artim);
                }
                catch(Exception exception)
                {
                    if(_as.isOpen())
                        try
                        {
                            _as.closesocket();
                        }
                        catch(Exception exception1) { }
                }
        }
        finally
        {
            synchronized(_dimseSCUs)
            {
                ((Object) (_dimseSCUs)).notifyAll();
            }
            if(_queue != null)
                _queue.write(((Object) (null)));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + _remoteAET + " Leave DimseExchange.run()");
        }
    }

    public void releaseAssoc()
        throws InterruptedException, IOException, IllegalValueException
    {
        if(!_as.isOpen())
            throw new IOException("jdicom: Association already closed");
        if(Debug.DEBUG > 2)
            Debug.out.println("jdicom: Enter _dimseSCUs.waitUntilEmpty(");
        _dimseSCUs.waitUntilEmpty();
        if(_queue != null)
        {
            if(Debug.DEBUG > 2)
                Debug.out.println("jdicom: Enter _queue.waitUntilEmpty()");
            _queue.waitUntilEmpty();
        }
        if(_as.isOpen())
        {
            if(Debug.DEBUG > 2)
                Debug.out.println("jdicom: Enter _as.sendReleaseRequest()");
            _as.sendReleaseRequest();
        }
        if(Debug.DEBUG > 2)
            Debug.out.println("jdicom: Leave DimseExchange.releaseAssoc()");
    }

    private void handleRSP(DimseExchange dimseexchange, DicomMessage dicommessage)
        throws IOException, UnknownUIDException, IllegalValueException, DicomException
    {
        int i = ((DicomObject) (dicommessage)).getI(5);
        int j = ((DicomObject) (dicommessage)).getI(9);
        IDimseRspListener idimsersplistener = Status.isPending(j) ? (IDimseRspListener)((Hashtable) (_dimseSCUs)).get(((Object) (new Integer(i)))) : (IDimseRspListener)_dimseSCUs.remove(((Object) (new Integer(i))));
        if(idimsersplistener == null)
        {
            throw new DicomException("jdicom: Error: Received unexpected DIMSE-RSP");
        } else
        {
            idimsersplistener.handleRSP(dimseexchange, i, j, dicommessage);
            return;
        }
    }







}
