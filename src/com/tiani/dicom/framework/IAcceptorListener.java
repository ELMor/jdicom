// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IAcceptorListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Acknowledge;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            VerboseAssociation

public interface IAcceptorListener
{

    public abstract void handle(Acknowledge acknowledge, VerboseAssociation verboseassociation)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException;
}
