// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefAssociationListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

// Referenced classes of package com.tiani.dicom.framework:
//            IAssociationListener, VerboseAssociation

public class DefAssociationListener
    implements IAssociationListener
{

    public DefAssociationListener()
    {
    }

    public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void releaseRequestReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseRequestSent(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseSent(VerboseAssociation verboseassociation)
    {
    }

    public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
    {
    }

    public void abortSent(VerboseAssociation verboseassociation, int i, int j)
    {
    }

    public void socketClosed(VerboseAssociation verboseassociation)
    {
    }
}
