// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SingleResponse.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

// Referenced classes of package com.tiani.dicom.framework:
//            IMultiResponse, DimseExchange, DicomMessage

public class SingleResponse
    implements IMultiResponse
{

    public static final SingleResponse NO_MATCH = new SingleResponse(0);
    private final int status;
    private String errorComment;

    public SingleResponse(int i, String s)
    {
        status = i;
        errorComment = s;
    }

    public SingleResponse(int i)
    {
        this(i, ((String) (null)));
    }

    public int nextResponse(DimseExchange dimseexchange, DicomMessage dicommessage)
    {
        if(errorComment != null)
            try
            {
                ((DicomObject) (dicommessage)).set(11, ((Object) (errorComment)));
            }
            catch(DicomException dicomexception) { }
        return status;
    }

    public void cancel()
    {
    }

}
