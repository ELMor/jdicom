// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SimpleAcceptancePolicy.java

package com.tiani.dicom.framework;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;

// Referenced classes of package com.tiani.dicom.framework:
//            IAcceptancePolicy, VerboseAssociation

public class SimpleAcceptancePolicy
    implements IAcceptancePolicy
{

    private String _calledTitle;
    private String _callingTitles[];
    private int _asids[];
    private int _maxPduSize;
    private int _maxOpInvoked;
    private int _maxOpPerformed;

    public SimpleAcceptancePolicy(String s, String as[], int ai[], int i, int j, int k)
    {
        _calledTitle = s;
        _callingTitles = as;
        _asids = ai;
        _maxPduSize = i;
        _maxOpInvoked = j;
        _maxOpPerformed = k;
    }

    public void setCalledTitle(String s)
    {
        _calledTitle = s;
    }

    public void setCallingTitles(String as[])
    {
        _callingTitles = as;
    }

    public void setAbstractSyntaxes(int ai[])
    {
        _asids = ai;
    }

    public void setMaxPduSize(int i)
    {
        _maxPduSize = i;
    }

    public void setMaxOpInvoked(int i)
    {
        _maxOpInvoked = i;
    }

    public void setMaxOpPerformed(int i)
    {
        _maxOpPerformed = i;
    }

    public String getCalledTitle()
    {
        return _calledTitle;
    }

    public String[] getCallingTitles()
    {
        return _callingTitles;
    }

    public int[] getAbstractSyntaxes()
    {
        return _asids;
    }

    public int getMaxPduSize()
    {
        return _maxPduSize;
    }

    public int getMaxOpInvoked()
    {
        return _maxOpInvoked;
    }

    public int getMaxOpPerformed()
    {
        return _maxOpPerformed;
    }

    public Response prepareResponse(VerboseAssociation verboseassociation, Request request)
        throws IllegalValueException
    {
        Response response = ResponsePolicy.prepareResponse(request, _calledTitle, _callingTitles, _asids, 8193, true);
        if(!(response instanceof Acknowledge))
        {
            return response;
        } else
        {
            Acknowledge acknowledge = (Acknowledge)response;
            acknowledge.setMaxPduSize(_maxPduSize);
            int i = request.getMaxOperationsInvoked();
            int j = request.getMaxOperationsPerformed();
            int k = i != 0 && (_maxOpInvoked == 0 || i <= _maxOpInvoked) ? i : _maxOpInvoked;
            int l = j != 0 && (_maxOpPerformed == 0 || j <= _maxOpPerformed) ? j : _maxOpPerformed;
            acknowledge.setMaxOperationsInvoked(k);
            acknowledge.setMaxOperationsPerformed(l);
            return ((Response) (acknowledge));
        }
    }
}
