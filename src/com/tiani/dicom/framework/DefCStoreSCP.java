// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefCStoreSCP.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, IDimseRqListener, DimseExchange, VerboseAssociation

public class DefCStoreSCP
    implements IDimseRqListener
{

    private static IDimseRqListener _instance = new DefCStoreSCP();

    public DefCStoreSCP()
    {
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        String s1 = ((DicomObject) (dicommessage)).getS(13);
        DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 32769, i, ((DicomObject) (null)));
        dicommessage1.affectedSOP(s, s1);
        dicommessage1.status(store(dimseexchange, s, s1, dicommessage, dicommessage1));
        dimseexchange.getAssociation().sendMessage(dicommessage1);
    }

    protected int store(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        return 0;
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        throw new DicomException("Error: cancel request for C-STORE service");
    }

    public static IDimseRqListener getInstance()
    {
        return _instance;
    }

}
