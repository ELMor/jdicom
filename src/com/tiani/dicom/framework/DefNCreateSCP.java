// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefNCreateSCP.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import com.tiani.dicom.util.UIDUtils;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DicomMessage, IDimseRqListener, DimseExchange, VerboseAssociation

public class DefNCreateSCP
    implements IDimseRqListener
{

    public DefNCreateSCP()
    {
    }

    public void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException
    {
        String s1 = null;
        if(((DicomObject) (dicommessage)).getSize(13) > 0)
            s1 = ((DicomObject) (dicommessage)).getS(13);
        DicomMessage dicommessage1 = new DicomMessage(dicommessage.getPresentationContext(), dicommessage.getAbstractSyntax(), 33088, i, ((DicomObject) (null)));
        dicommessage1.affectedSOP(s, s1);
        dicommessage1.status(create(dimseexchange, s, s1, dicommessage, dicommessage1));
        dimseexchange.getAssociation().sendMessage(dicommessage1);
    }

    protected int create(DimseExchange dimseexchange, String s, String s1, DicomMessage dicommessage, DicomMessage dicommessage1)
        throws DicomException
    {
        if(s1 == null)
            ((DicomObject) (dicommessage1)).set(13, ((Object) (UIDUtils.createUID())));
        return 0;
    }

    public boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws DicomException
    {
        throw new DicomException("Error: cancel request for N-CREATE service");
    }
}
