// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IDimseRqListener.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.IOException;

// Referenced classes of package com.tiani.dicom.framework:
//            DimseExchange, DicomMessage

public interface IDimseRqListener
{

    public abstract void handleRQ(DimseExchange dimseexchange, int i, String s, DicomMessage dicommessage)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException;

    public abstract boolean handleCancelRQ(DimseExchange dimseexchange, int i)
        throws IOException, DicomException, IllegalValueException, UnknownUIDException;
}
