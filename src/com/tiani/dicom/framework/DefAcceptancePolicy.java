// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefAcceptancePolicy.java

package com.tiani.dicom.framework;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import com.archimed.dicom.network.ResponsePolicy;

// Referenced classes of package com.tiani.dicom.framework:
//            IAcceptancePolicy, VerboseAssociation

public class DefAcceptancePolicy
    implements IAcceptancePolicy
{

    private String _calledTitle;
    private String _callingTitles[];
    private int _asids[];
    private int _maxOpInvoked;
    private int _maxOpPerformed;
    private int _tsid;
    private boolean _rejectnocontext;

    public DefAcceptancePolicy(String s, String as[], int ai[], int i, int j, int k, boolean flag)
    {
        _calledTitle = s;
        _callingTitles = as;
        _asids = ai;
        _maxOpInvoked = i;
        _maxOpPerformed = j;
        _tsid = k;
        _rejectnocontext = flag;
    }

    public Response prepareResponse(VerboseAssociation verboseassociation, Request request)
        throws IllegalValueException
    {
        Response response = ResponsePolicy.prepareResponse(request, _calledTitle, _callingTitles, _asids, _tsid, _rejectnocontext);
        if(!(response instanceof Acknowledge))
        {
            return response;
        } else
        {
            int i = request.getMaxOperationsInvoked();
            int j = request.getMaxOperationsPerformed();
            int k = i != 0 && (_maxOpInvoked == 0 || i <= _maxOpInvoked) ? i : _maxOpInvoked;
            int l = j != 0 && (_maxOpPerformed == 0 || j <= _maxOpPerformed) ? j : _maxOpPerformed;
            Acknowledge acknowledge = (Acknowledge)response;
            acknowledge.setMaxOperationsInvoked(k);
            acknowledge.setMaxOperationsPerformed(l);
            return ((Response) (acknowledge));
        }
    }
}
