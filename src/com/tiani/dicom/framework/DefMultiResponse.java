// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefMultiResponse.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomObject;
import java.util.Enumeration;

// Referenced classes of package com.tiani.dicom.framework:
//            IMultiResponse, DicomMessage, DimseExchange

public class DefMultiResponse
    implements IMultiResponse
{

    private Enumeration enum;
    private boolean canceled;

    public DefMultiResponse(Enumeration enumeration)
    {
        canceled = false;
        enum = enumeration;
    }

    public int nextResponse(DimseExchange dimseexchange, DicomMessage dicommessage)
    {
        if(!enum.hasMoreElements())
            return 0;
        if(canceled)
        {
            return 65024;
        } else
        {
            dicommessage.setDataset((DicomObject)enum.nextElement());
            return 65280;
        }
    }

    public void cancel()
    {
        canceled = true;
    }
}
