// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   VerboseAssociation.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DDict;
import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Acknowledge;
import com.archimed.dicom.network.Association;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.tiani.dicom.framework:
//            IAssociationListener, DicomMessage, Status, StatusEntry

public class VerboseAssociation extends Association
{

    private static int _count = 0;
    private String _id;
    private int _msgID;
    private Thread _closeThread;
    private Vector associationListeners;
    private Socket _socket;
    private String _remoteAET;
    private String _localAET;
    private static int dumpTags[] = {
        147, 148, 427, 428, 430
    };

    public VerboseAssociation(Socket socket1)
        throws IOException
    {
        super(socket1.getInputStream(), socket1.getOutputStream());
        _id = "#" + ++_count + ":";
        _msgID = 0;
        _closeThread = null;
        associationListeners = new Vector();
        _remoteAET = "unknown";
        _localAET = "unknown";
        _socket = socket1;
    }

    public int nextMessageID()
    {
        return ++_msgID & 0xffff;
    }

    public boolean isOpen()
    {
        return _socket != null && _closeThread == null;
    }

    public String localAET()
    {
        return _localAET;
    }

    public String remoteAET()
    {
        return _remoteAET;
    }

    public Socket socket()
    {
        return _socket;
    }

    public static void setDumpTags(int ai[])
    {
        dumpTags = ai;
    }

    public void addAssociationListener(IAssociationListener iassociationlistener)
    {
        associationListeners.addElement(((Object) (iassociationlistener)));
    }

    public boolean removeAssociationListener(IAssociationListener iassociationlistener)
    {
        return associationListeners.removeElement(((Object) (iassociationlistener)));
    }

    public Request receiveAssociateRequest()
        throws IOException, UnknownUIDException, IllegalValueException
    {
        Request request = super.receiveAssociateRequest();
        _remoteAET = request.getCallingTitle();
        _localAET = request.getCalledTitle();
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).associateRequestReceived(this, request));
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> A-ASSOCIATE-RQ PDU");
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + request);
        }
        return request;
    }

    public Response receiveAssociateResponse()
        throws IOException, UnknownUIDException, IllegalValueException
    {
        Response response = super.receiveAssociateResponse();
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).associateResponseReceived(this, response));
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + ((response instanceof Acknowledge) ? " >> A-ASSOCIATE-AC PDU" : " >> A-ASSOCIATE-RJ PDU"));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + response);
        }
        return response;
    }

    public void sendAssociateRequest(Request request)
        throws IOException, IllegalValueException
    {
        _remoteAET = request.getCalledTitle();
        _localAET = request.getCallingTitle();
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " << A-ASSOCIATE-RQ PDU");
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + request);
        }
        super.sendAssociateRequest(request);
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).associateRequestSent(this, request));
    }

    public void sendAssociateResponse(Response response)
        throws IOException, IllegalValueException
    {
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + ((response instanceof Acknowledge) ? " << A-ASSOCIATE-AC PDU" : " << A-ASSOCIATE-RJ PDU"));
            if(Debug.DEBUG > 1)
                Debug.out.println("jdicom: " + response);
        }
        super.sendAssociateResponse(response);
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).associateResponseSent(this, response));
    }

    public DicomObject receiveData()
        throws IOException, IllegalValueException, DicomException, UnknownUIDException
    {
        DicomObject dicomobject = super.receiveData();
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> Dataset");
            if(Debug.DEBUG > 1)
                if(Debug.DEBUG > 2 || dicomobject.getSize(1184) == -1)
                {
                    dicomobject.dumpVRs(((java.io.OutputStream) (Debug.out)));
                    Debug.out.flush();
                } else
                {
                    for(int i = 0; i < dumpTags.length; i++)
                        Debug.out.println("jdicom: " + DDict.getDescription(dumpTags[i]) + "[" + dicomobject.getS(dumpTags[i]) + "]");

                }
        }
        return dicomobject;
    }

    public DicomObject receiveCommand()
        throws IOException, IllegalValueException, DicomException, UnknownUIDException
    {
        DicomObject dicomobject = super.receiveCommand();
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> " + cmdName(dicomobject));
            if(Debug.DEBUG > 1)
            {
                dicomobject.dumpVRs(((java.io.OutputStream) (Debug.out)));
                Debug.out.flush();
            }
        }
        return dicomobject;
    }

    public void sendMessage(DicomMessage dicommessage)
        throws IOException, IllegalValueException, DicomException
    {
        sendInPresentationContext(dicommessage.getPresentationContext(), ((DicomObject) (dicommessage)), dicommessage.getDataset());
    }

    public void sendInPresentationContext(byte byte0, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException
    {
        if(Debug.DEBUG > 0)
            logSend(dicomobject, dicomobject1);
        super.sendInPresentationContext(byte0, dicomobject, dicomobject1);
    }

    private void logSend(DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, DicomException
    {
        Debug.out.println("jdicom: " + _id + _remoteAET + " << " + cmdName(dicomobject));
        if(Debug.DEBUG > 1)
        {
            dicomobject.dumpVRs(((java.io.OutputStream) (Debug.out)));
            Debug.out.flush();
        }
        if(dicomobject1 != null)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " << Dataset");
            if(Debug.DEBUG > 1)
                if(Debug.DEBUG > 2 || dicomobject1.getSize(1184) == -1)
                {
                    dicomobject1.dumpVRs(((java.io.OutputStream) (Debug.out)));
                    Debug.out.flush();
                } else
                {
                    for(int i = 0; i < dumpTags.length; i++)
                        Debug.out.println("jdicom: " + DDict.getDescription(dumpTags[i]) + "[" + dicomobject1.getS(dumpTags[i]) + "]");

                }
        }
    }

    public void receiveReleaseRequest()
        throws IOException, IllegalValueException
    {
        super.receiveReleaseRequest();
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).releaseRequestReceived(this));
        if(Debug.DEBUG > 0)
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> A-RELEASE-RQ PDU");
    }

    public void receiveReleaseResponse()
        throws IOException, IllegalValueException
    {
        super.receiveReleaseResponse();
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).releaseResponseReceived(this));
        if(Debug.DEBUG > 0)
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> A-RELEASE-RP PDU");
    }

    public void sendReleaseRequest()
        throws IOException, IllegalValueException
    {
        if(Debug.DEBUG > 0)
            Debug.out.println("jdicom: " + _id + _remoteAET + " << A-RELEASE-RQ PDU");
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).releaseRequestSent(this));
        super.sendReleaseRequest();
    }

    public void sendReleaseResponse()
        throws IOException, IllegalValueException
    {
        if(Debug.DEBUG > 0)
            Debug.out.println("jdicom: " + _id + _remoteAET + " << A-RELEASE-RP PDU");
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).releaseResponseSent(this));
        super.sendReleaseResponse();
    }

    public Abort receiveAbort()
        throws IllegalValueException, IOException
    {
        Abort abort = super.receiveAbort();
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).abortReceived(this, abort));
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " >> A-ABORT PDU");
            Debug.out.println("jdicom: " + abort);
        }
        return abort;
    }

    public void sendAbort(int i, int j)
        throws IOException
    {
        super.sendAbort(i, j);
        if(Debug.DEBUG > 0)
        {
            Debug.out.println("jdicom: " + _id + _remoteAET + " << A-ABORT PDU:");
            Debug.out.println("jdicom: " + new Abort(i, j));
        }
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).abortSent(this, i, j));
    }

    public void closesocket(final int artim)
    {
        if(_socket == null)
        {
            return;
        } else
        {
            notifySocketClose();
            _closeThread = new Thread(new Runnable() {

                public void run()
                {
                    try
                    {
                        Thread.sleep(artim);
                        try
                        {
                            _socket.close();
                        }
                        finally
                        {
                            _socket = null;
                        }
                    }
                    catch(Exception exception1)
                    {
                        Debug.out.println("jdicom: " + ((Throwable) (exception1)).getMessage());
                    }
                }

            });
            _closeThread.start();
            return;
        }
    }

    private void notifySocketClose()
    {
        if(Debug.DEBUG > 0)
            Debug.out.println("jdicom: " + _id + _remoteAET + " closing socket");
        for(Enumeration enumeration = associationListeners.elements(); enumeration.hasMoreElements(); ((IAssociationListener)enumeration.nextElement()).socketClosed(this));
    }

    public void closesocket()
        throws IOException
    {
        if(_socket == null)
            return;
        notifySocketClose();
        try
        {
            _socket.close();
        }
        finally
        {
            _socket = null;
        }
    }

    private String cmdName(DicomObject dicomobject)
    {
        try
        {
            UIDEntry uidentry = getUIDEntry(dicomobject);
            StringBuffer stringbuffer = new StringBuffer();
            stringbuffer.append(cmdName(dicomobject.getI(3)));
            if(uidentry != null)
                stringbuffer.append(uidentry.getName());
            if(dicomobject.getSize(9) > 0)
            {
                int i = dicomobject.getI(9);
                StatusEntry statusentry = uidentry == null ? Status.getStatusEntry(i) : Status.getStatusEntry(uidentry.getConstant(), i);
                stringbuffer.append(", ");
                stringbuffer.append(Status.toString(i));
                stringbuffer.append("[");
                stringbuffer.append(statusentry.getMessage());
                stringbuffer.append("]");
                if(dicomobject.getSize(11) > 0)
                {
                    stringbuffer.append(", error comment: ");
                    stringbuffer.append(dicomobject.getS(11));
                }
            }
            return stringbuffer.toString();
        }
        catch(Exception exception)
        {
            return ((Throwable) (exception)).toString();
        }
    }

    private UIDEntry getUIDEntry(DicomObject dicomobject)
    {
        String s;
        try
        {
            if((s = (String)dicomobject.get(1)) != null || (s = (String)dicomobject.get(2)) != null)
                return UID.getUIDEntry(s);
        }
        catch(Exception exception) { }
        return null;
    }

    public static String cmdName(int i)
    {
        switch(i)
        {
        case 32816: 
            return "C-ECHO-RSP ";

        case 32769: 
            return "C-STORE-RSP ";

        case 32784: 
            return "C-GET-RSP ";

        case 32800: 
            return "C-FIND-RSP ";

        case 32801: 
            return "C-MOVE-RSP ";

        case 33024: 
            return "N-EVENTREPORT-RSP ";

        case 33088: 
            return "N-CREATE-RSP ";

        case 33040: 
            return "N-GET-RSP ";

        case 33056: 
            return "N-SET-RSP ";

        case 33072: 
            return "N-ACTION-RSP ";

        case 33104: 
            return "N-DELETE-RSP ";

        case 48: // '0'
            return "C-ECHO-RQ ";

        case 1: // '\001'
            return "C-STORE-RQ ";

        case 16: // '\020'
            return "C-GET-RQ ";

        case 32: // ' '
            return "C-FIND-RQ ";

        case 33: // '!'
            return "C-MOVE-RQ ";

        case 256: 
            return "N-EVENTREPORT-RQ ";

        case 320: 
            return "N-CREATE-RQ ";

        case 272: 
            return "N-GET-RQ ";

        case 288: 
            return "N-SET-RQ ";

        case 304: 
            return "N-ACTION-RQ ";

        case 336: 
            return "N-DELETE-RQ ";

        case 4095: 
            return "C-CANCEL-RQ ";
        }
        return "UNKNOWN COMMAND! ";
    }



}
