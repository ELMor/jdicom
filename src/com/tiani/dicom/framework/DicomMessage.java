// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DicomMessage.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.GroupList;
import com.archimed.dicom.UIDEntry;

public final class DicomMessage extends DicomObject
{

    public static final int CSTORERQ = 1;
    public static final int CSTORERSP = 32769;
    public static final int CGETRQ = 16;
    public static final int CGETRSP = 32784;
    public static final int CFINDRQ = 32;
    public static final int CFINDRSP = 32800;
    public static final int CMOVERQ = 33;
    public static final int CMOVERSP = 32801;
    public static final int CECHORQ = 48;
    public static final int CECHORSP = 32816;
    public static final int NEVENTREPORTRQ = 256;
    public static final int NEVENTREPORTRSP = 33024;
    public static final int NGETRQ = 272;
    public static final int NGETRSP = 33040;
    public static final int NSETRQ = 288;
    public static final int NSETRSP = 33056;
    public static final int NACTIONRQ = 304;
    public static final int NACTIONRSP = 33072;
    public static final int NCREATERQ = 320;
    public static final int NCREATERSP = 33088;
    public static final int NDELETERQ = 336;
    public static final int NDELETERSP = 33104;
    public static final int CCANCELRQ = 4095;
    public static final int LOW = 2;
    public static final int MEDIUM = 0;
    public static final int HIGH = 1;
    public static final int NODATASET = 257;
    public static final int YESDATASET = 65278;
    public static final int SOPCLASSNOTSUPPORTED = 290;
    private byte _pcid;
    private UIDEntry _asuid;
    private DicomObject _dataset;

    public DicomMessage(byte byte0, UIDEntry uidentry, DicomObject dicomobject, DicomObject dicomobject1)
    {
        _pcid = 0;
        _asuid = null;
        _dataset = null;
        _pcid = byte0;
        _asuid = uidentry;
        ((GroupList)this).addGroups(dicomobject);
        _dataset = dicomobject1;
    }

    public DicomMessage(byte byte0, UIDEntry uidentry, int i, int j, DicomObject dicomobject)
        throws DicomException
    {
        _pcid = 0;
        _asuid = null;
        _dataset = null;
        _pcid = byte0;
        _asuid = uidentry;
        ((DicomObject)this).set(3, ((Object) (new Integer(i))));
        ((DicomObject)this).set((i & 0x8e00) != 0 ? 5 : 4, ((Object) (new Integer(j))));
        setDataset(dicomobject);
    }

    public DicomMessage(byte byte0, int i, int j, DicomObject dicomobject)
        throws DicomException
    {
        this(byte0, ((UIDEntry) (null)), i, j, dicomobject);
    }

    public DicomMessage(int i, int j, DicomObject dicomobject)
        throws DicomException
    {
        this((byte)0, ((UIDEntry) (null)), i, j, dicomobject);
    }

    public void setDataset(DicomObject dicomobject)
    {
        try
        {
            ((DicomObject)this).set(8, ((Object) (new Integer((_dataset = dicomobject) != null ? 65278 : 257))));
        }
        catch(DicomException dicomexception)
        {
            throw new RuntimeException(((Throwable) (dicomexception)).getMessage());
        }
    }

    public byte getPresentationContext()
    {
        return _pcid;
    }

    public UIDEntry getAbstractSyntax()
    {
        return _asuid;
    }

    public DicomObject getDataset()
    {
        return _dataset;
    }

    public void affectedSOPclassUID(String s)
        throws DicomException
    {
        ((DicomObject)this).set(1, ((Object) (s)));
    }

    public void affectedSOP(String s, String s1)
        throws DicomException
    {
        ((DicomObject)this).set(1, ((Object) (s)));
        ((DicomObject)this).set(13, ((Object) (s1)));
    }

    public void requestedSOP(String s, String s1)
        throws DicomException
    {
        ((DicomObject)this).set(2, ((Object) (s)));
        ((DicomObject)this).set(14, ((Object) (s1)));
    }

    public void priority(int i)
        throws DicomException
    {
        ((DicomObject)this).set(7, ((Object) (new Integer(i))));
    }

    public void eventTypeID(int i)
        throws DicomException
    {
        ((DicomObject)this).set(15, ((Object) (new Integer(i))));
    }

    public void actionTypeID(int i)
        throws DicomException
    {
        ((DicomObject)this).set(18, ((Object) (new Integer(i))));
    }

    public void status(int i)
        throws DicomException
    {
        ((DicomObject)this).set(9, ((Object) (new Integer(i))));
    }

    public void moveDestination(String s)
        throws DicomException
    {
        ((DicomObject)this).set(6, ((Object) (s)));
    }

    public void attributeIdList(int ai[])
        throws DicomException
    {
        for(int i = 0; i < ai.length; i++)
            ((DicomObject)this).append(16, ((Object) (new Integer(ai[i]))));

    }

    public void moveOriginator(String s, int i)
        throws DicomException
    {
        ((DicomObject)this).set(23, ((Object) (s)));
        ((DicomObject)this).set(24, ((Object) (new Integer(i))));
    }
}
