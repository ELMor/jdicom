// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StatusEntry.java

package com.tiani.dicom.framework;


// Referenced classes of package com.tiani.dicom.framework:
//            Status

public class StatusEntry
{

    public static final int UNKNOWN = 0;
    public static final int SUCCESS = 1;
    public static final int PENDING = 2;
    public static final int CANCEL = 3;
    public static final int WARNING = 4;
    public static final int FAILURE = 5;
    private final int code;
    private final int type;
    private final String message;

    public StatusEntry(int i, int j, String s)
    {
        code = i;
        type = j;
        message = s;
    }

    public int getCode()
    {
        return code;
    }

    public int getType()
    {
        return type;
    }

    public String getMessage()
    {
        return message;
    }

    public int hashCode(Object obj)
    {
        return code;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof StatusEntry))
            return false;
        else
            return code == ((StatusEntry)obj).code;
    }

    public String toString()
    {
        return Status.toString(code) + " [" + message + "]";
    }
}
