// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FutureDimseRsp.java

package com.tiani.dicom.framework;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.network.Abort;
import com.archimed.dicom.network.Request;
import com.archimed.dicom.network.Response;

// Referenced classes of package com.tiani.dicom.framework:
//            IDimseRspListener, IAssociationListener, Status, DicomMessage, 
//            DimseExchange, VerboseAssociation

public class FutureDimseRsp
    implements IDimseRspListener, IAssociationListener
{

    private DicomMessage _rsp;
    private boolean _open;

    public FutureDimseRsp()
    {
        _rsp = null;
        _open = true;
    }

    public synchronized DicomMessage getResponse()
        throws InterruptedException
    {
        while(_open && _rsp == null) 
            ((Object)this).wait();
        DicomMessage dicommessage = _rsp;
        _rsp = null;
        return dicommessage;
    }

    public synchronized void handleRSP(DimseExchange dimseexchange, int i, int j, DicomMessage dicommessage)
        throws DicomException
    {
        if(!Status.isPending(j))
        {
            _rsp = dicommessage;
            ((Object)this).notifyAll();
        }
    }

    public synchronized void socketClosed(VerboseAssociation verboseassociation)
    {
        _open = false;
        ((Object)this).notifyAll();
    }

    public void associateRequestReceived(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseReceived(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void associateRequestSent(VerboseAssociation verboseassociation, Request request)
    {
    }

    public void associateResponseSent(VerboseAssociation verboseassociation, Response response)
    {
    }

    public void releaseRequestReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseReceived(VerboseAssociation verboseassociation)
    {
    }

    public void releaseRequestSent(VerboseAssociation verboseassociation)
    {
    }

    public void releaseResponseSent(VerboseAssociation verboseassociation)
    {
    }

    public void abortReceived(VerboseAssociation verboseassociation, Abort abort)
    {
    }

    public void abortSent(VerboseAssociation verboseassociation, int i, int j)
    {
    }
}
