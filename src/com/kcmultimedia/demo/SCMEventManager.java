// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SCMEventManager.java

package com.kcmultimedia.demo;

import java.util.Vector;

// Referenced classes of package com.kcmultimedia.demo:
//            SCMEvent, SCMEventListener

public class SCMEventManager extends Vector
{

    private static SCMEventManager h = null;

    private SCMEventManager()
    {
    }

    public static SCMEventManager getInstance()
    {
        if(null == h)
            h = new SCMEventManager();
        return h;
    }

    public void dispatchSCMEvent(int i)
    {
        SCMEvent scmevent = new SCMEvent(i);
        for(int j = 0; j < ((Vector)this).size(); j++)
        {
            Object obj = ((Vector)this).get(j);
            if(null != obj)
                ((SCMEventListener)obj).handleSCMEvent(scmevent);
        }

    }

    public void addSCMEventListener(SCMEventListener scmeventlistener)
    {
        if(!((Vector)this).contains(((Object) (scmeventlistener))))
            ((Vector)this).add(((Object) (scmeventlistener)));
    }

    public void removeSCMEventListener(SCMEventListener scmeventlistener)
    {
        ((Vector)this).remove(((Object) (scmeventlistener)));
    }

}
