// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Hashtable;

// Referenced classes of package com.archimed.dicom:
//            UID, UIDEntry

public class SOPInstance extends UID
{

    public static final int StorageCommitmentPushModel = 16385;
    public static final int StorageCommitmentPullModel = 16386;
    public static final int Printer = 16387;
    public static final int PrintQueue = 16388;
    public static final int PrinterConfigurationRetrieval = 16389;

    public SOPInstance()
    {
    }

    static void a()
    {
        UID.a.put(((Object) (new Integer(16385))), ((Object) (new UIDEntry(16385, "1.2.840.10008.1.20.1.1", "Storage Commitment Push Model SOP Instance", "SP", 2))));
        UID.a.put(((Object) (new Integer(16386))), ((Object) (new UIDEntry(16386, "1.2.840.10008.1.20.2.1", "Storage Commitment Pull Model SOP Instance", "SP", 2))));
        UID.a.put(((Object) (new Integer(16387))), ((Object) (new UIDEntry(16387, "1.2.840.10008.5.1.1.17", "Printer SOP Instance", "P ", 2))));
        UID.a.put(((Object) (new Integer(16388))), ((Object) (new UIDEntry(16388, "1.2.840.10008.5.1.1.25", "Print Queue SOP Instance", "DP", 2))));
        UID.a.put(((Object) (new Integer(16389))), ((Object) (new UIDEntry(16389, "1.2.840.10008.5.1.1.17.376", "Printer Configuration Retrieval SOP Class", "CP", 1))));
    }
}
