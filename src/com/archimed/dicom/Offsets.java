// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            GroupList, VR, DicomObject, TagValue, 
//            DicomException, DDict

public class Offsets
{

    DicomObject a;

    public Offsets(DicomObject dicomobject)
    {
        a = dicomobject;
    }

    private int a(DicomObject dicomobject, boolean flag, int i, boolean flag1)
    {
        int j = 0;
        for(Enumeration enumeration = ((GroupList) (dicomobject)).a(flag, i, flag1, true); enumeration.hasMoreElements();)
        {
            VR vr = (VR)enumeration.nextElement();
            j += vr.headerLen + vr.dataLen;
        }

        return j;
    }

    public long calculateOffset_ge(DicomObject dicomobject, int i, int j, int k, boolean flag, boolean flag1, boolean flag2)
    {
        VR vr = ((GroupList) (dicomobject)).a(i, j);
        long l = 0L;
        if(vr == null)
            return 0L;
        if(flag2)
        {
            try
            {
                dicomobject.a(k);
            }
            catch(DicomException dicomexception)
            {
                ((Throwable) (dicomexception)).printStackTrace();
            }
            DicomObject dicomobject1 = dicomobject.getFileMetaInformation();
            if(dicomobject1 != null)
                l = 132 + a(dicomobject1, true, 8194, flag);
        }
        for(Enumeration enumeration = ((GroupList) (dicomobject)).a(flag1, k, flag, true); enumeration.hasMoreElements();)
        {
            VR vr1 = (VR)enumeration.nextElement();
            if(((TagValue) (vr1)).group == i && ((TagValue) (vr1)).element == j)
                break;
            l += vr1.dataLen + vr1.headerLen;
        }

        return l;
    }

    public long calculateOffset_ge(int i, int j, int k, boolean flag, boolean flag1, boolean flag2)
    {
        return calculateOffset_ge(a, i, j, k, flag, flag1, flag2);
    }

    public long calculateOffset(int i, int j, boolean flag, boolean flag1, boolean flag2)
    {
        int k = DDict.getGroup(i);
        int l = DDict.getElement(i);
        return calculateOffset_ge(k, l, j, flag, flag1, flag2);
    }

    public long calculateOffset(int i, int j, int k, boolean flag, boolean flag1, boolean flag2)
    {
        long l = calculateOffset(i, k, flag, flag1, flag2);
        VR vr = ((GroupList) (a)).a(DDict.getGroup(i), DDict.getElement(i));
        if(j >= ((TagValue) (vr)).val.size())
            return 0L;
        if(vr.dcm_type != 10)
            return 0L;
        l += vr.headerLen;
        for(int i1 = 0; i1 < j; i1++)
            l += ((DicomObject)((TagValue) (vr)).val.elementAt(i1)).c;

        return l;
    }

    public long calculateOffset(int i, int j, int k, int l, boolean flag, boolean flag1, boolean flag2)
    {
        long l1 = calculateOffset(i, j, l, flag, flag1, flag2);
        VR vr = ((GroupList) (a)).a(DDict.getGroup(i), DDict.getElement(i));
        DicomObject dicomobject = (DicomObject)((TagValue) (vr)).val.elementAt(j);
        l1 += 8L + calculateOffset_ge(dicomobject, DDict.getGroup(k), DDict.getElement(k), l, flag, false, false);
        return l1;
    }
}
