// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Dictionary;
import java.util.Enumeration;

// Referenced classes of package com.archimed.dicom:
//            IntHashtableEntry, IntHashtableEnumerator

class IntHashtable
    extends Dictionary
    implements Cloneable {

  private IntHashtableEntry a[];
  private int b;
  private int c;
  private float d;

  public IntHashtable(int i, float f) {
    if (i <= 0 || (double) f <= 0.0D) {
      throw new IllegalArgumentException();
    }
    else {
      d = f;
      a = new IntHashtableEntry[i];
      c = (int) ( (float) i * f);
      return;
    }
  }

  public IntHashtable(int i) {
    this(i, 0.75F);
  }

  public IntHashtable() {
    this(101, 0.75F);
  }

  public int size() {
    return b;
  }

  public boolean isEmpty() {
    return b == 0;
  }

  public synchronized Enumeration keys() {
    return ( (Enumeration) (new IntHashtableEnumerator(a, true)));
  }

  public synchronized Enumeration elements() {
    return ( (Enumeration) (new IntHashtableEnumerator(a, false)));
  }

  public synchronized boolean contains(Object obj) {
    if (obj == null) {
      throw new NullPointerException();
    }
    IntHashtableEntry ainthashtableentry[] = a;
    for (int i = ainthashtableentry.length; i-- > 0; ) {
      for (IntHashtableEntry inthashtableentry = ainthashtableentry[i];
           inthashtableentry != null; inthashtableentry = inthashtableentry.d) {
        if (inthashtableentry.c.equals(obj)) {
          return true;
        }
      }

    }

    return false;
  }

  public synchronized boolean containsKey(int i) {
    IntHashtableEntry ainthashtableentry[] = a;
    int j = i;
    int k = (j & 0x7fffffff) % ainthashtableentry.length;
    for (IntHashtableEntry inthashtableentry = ainthashtableentry[k];
         inthashtableentry != null; inthashtableentry = inthashtableentry.d) {
      if (inthashtableentry.a == j && inthashtableentry.b == i) {
        return true;
      }
    }

    return false;
  }

  public synchronized Object get(int i) {
    IntHashtableEntry ainthashtableentry[] = a;
    int j = i;
    int k = (j & 0x7fffffff) % ainthashtableentry.length;
    for (IntHashtableEntry inthashtableentry = ainthashtableentry[k];
         inthashtableentry != null; inthashtableentry = inthashtableentry.d) {
      if (inthashtableentry.a == j && inthashtableentry.b == i) {
        return inthashtableentry.c;
      }
    }

    return ( (Object) (null));
  }

  public Object get(Object obj) {
    if (! (obj instanceof Integer)) {
      throw new InternalError("key is not an Integer");
    }
    else {
      Integer integer = (Integer) obj;
      int i = integer.intValue();
      return get(i);
    }
  }

  protected void rehash() {
    int i = a.length;
    IntHashtableEntry ainthashtableentry[] = a;
    int j = i * 2 + 1;
    IntHashtableEntry ainthashtableentry1[] = new IntHashtableEntry[j];
    c = (int) ( (float) j * d);
    a = ainthashtableentry1;
    for (int k = i; k-- > 0; ) {
      for (IntHashtableEntry inthashtableentry = ainthashtableentry[k];
           inthashtableentry != null; ) {
        IntHashtableEntry inthashtableentry1 = inthashtableentry;
        inthashtableentry = inthashtableentry.d;
        int l = (inthashtableentry1.a & 0x7fffffff) % j;
        inthashtableentry1.d = ainthashtableentry1[l];
        ainthashtableentry1[l] = inthashtableentry1;
      }

    }

  }

  public synchronized Object put(int i, Object obj) {
    if (obj == null) {
      throw new NullPointerException();
    }
    IntHashtableEntry ainthashtableentry[] = a;
    int j = i;
    int k = (j & 0x7fffffff) % ainthashtableentry.length;
    for (IntHashtableEntry inthashtableentry = ainthashtableentry[k];
         inthashtableentry != null; inthashtableentry = inthashtableentry.d) {
      if (inthashtableentry.a == j && inthashtableentry.b == i) {
        Object obj1 = inthashtableentry.c;
        inthashtableentry.c = obj;
        return obj1;
      }
    }

    if (b >= c) {
      rehash();
      return put(i, obj);
    }
    else {
      IntHashtableEntry inthashtableentry1 = new IntHashtableEntry();
      inthashtableentry1.a = j;
      inthashtableentry1.b = i;
      inthashtableentry1.c = obj;
      inthashtableentry1.d = ainthashtableentry[k];
      ainthashtableentry[k] = inthashtableentry1;
      b++;
      return ( (Object) (null));
    }
  }

  public Object put(Object obj, Object obj1) {
    if (! (obj instanceof Integer)) {
      throw new InternalError("key is not an Integer");
    }
    else {
      Integer integer = (Integer) obj;
      int i = integer.intValue();
      return put(i, obj1);
    }
  }

  public synchronized Object remove(int i) {
    IntHashtableEntry ainthashtableentry[] = a;
    int j = i;
    int k = (j & 0x7fffffff) % ainthashtableentry.length;
    IntHashtableEntry inthashtableentry = ainthashtableentry[k];
    IntHashtableEntry inthashtableentry1 = null;
    for (; inthashtableentry != null; inthashtableentry = inthashtableentry.d) {
      if (inthashtableentry.a == j && inthashtableentry.b == i) {
        if (inthashtableentry1 != null) {
          inthashtableentry1.d = inthashtableentry.d;
        }
        else {
          ainthashtableentry[k] = inthashtableentry.d;
        }
        b--;
        return inthashtableentry.c;
      }
      inthashtableentry1 = inthashtableentry;
    }

    return ( (Object) (null));
  }

  public Object remove(Object obj) {
    if (! (obj instanceof Integer)) {
      throw new InternalError("key is not an Integer");
    }
    else {
      Integer integer = (Integer) obj;
      int i = integer.intValue();
      return remove(i);
    }
  }

  public synchronized void clear() {
    IntHashtableEntry ainthashtableentry[] = a;
    for (int i = ainthashtableentry.length; --i >= 0; ) {
      ainthashtableentry[i] = null;

    }
    b = 0;
  }

  public synchronized Object clone() {
    IntHashtable inthashtable = (IntHashtable) clone();
    inthashtable.a = new IntHashtableEntry[a.length];
    for (int i = a.length; i-- > 0; ) {
      inthashtable.a[i] = a[i] == null ? null : (IntHashtableEntry) a[i].clone();

        }
    return ( (Object) (inthashtable));
  }

  public synchronized String toString() {
    int i = size() - 1;
    StringBuffer stringbuffer = new StringBuffer();
    Enumeration enumeration = keys();
    Enumeration enumeration1 = elements();
    stringbuffer.append("{");
    for (int j = 0; j <= i; j++) {
      String s = enumeration.nextElement().toString();
      String s1 = enumeration1.nextElement().toString();
      stringbuffer.append(s + "=" + s1);
      if (j < i) {
        stringbuffer.append(", ");
      }
    }

    stringbuffer.append("}");
    return stringbuffer.toString();
  }
}
