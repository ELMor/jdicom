// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            GroupList, TagValue, Debug, DicomException, 
//            VR, e, Person, DDate, 
//            DDateRange, DTime, DTimeRange, DDateTime, 
//            DDateTimeRange, DDict, Offsets, f, 
//            h, OffsetInputStream, UID, UIDEntry, 
//            UnknownUIDException, i, k, OffsetOutputStream, 
//            IllegalValueException, DumpUtils

public class DicomObject extends GroupList
    implements Cloneable
{

    public static int dumpLineLen = 72;
    DicomObject a;
    long b;
    int c;
    int d;

    public Object clone()
    {
        DicomObject dicomobject = new DicomObject();
        for(Enumeration enumeration = ((GroupList)this).enumerateVRs(false, false); enumeration.hasMoreElements();)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            int j = tagvalue.getGroup();
            int l = tagvalue.getElement();
            int i1 = tagvalue.size();
            try
            {
                if(i1 == 0)
                {
                    dicomobject.set_ge(j, l, ((Object) (null)));
                } else
                {
                    for(int j1 = 0; j1 < i1; j1++)
                    {
                        Object obj = tagvalue.getValue(j1);
                        if(obj instanceof byte[])
                        {
                            byte abyte0[] = (byte[])obj;
                            obj = ((Object) (new byte[abyte0.length]));
                            System.arraycopy(((Object) (abyte0)), 0, obj, 0, abyte0.length);
                        } else
                        if(obj instanceof DicomObject)
                        {
                            DicomObject dicomobject1 = (DicomObject)obj;
                            obj = dicomobject1.clone();
                        }
                        dicomobject.append_ge(j, l, obj);
                    }

                }
            }
            catch(DicomException dicomexception)
            {
                Debug.out.println("Error cloning attribute (" + Integer.toHexString(j) + "," + Integer.toHexString(l) + ": " + dicomexception);
            }
        }

        return ((Object) (dicomobject));
    }

    public void setVRs(DicomObject dicomobject)
        throws DicomException
    {
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements();)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            int i1 = tagvalue.size();
            if(i1 >= 0)
            {
                int j = tagvalue.getGroup();
                int l = tagvalue.getElement();
                set_ge(j, l, ((Object) (null)));
                for(int j1 = 0; j1 < i1; j1++)
                    set_ge(j, l, tagvalue.getValue(j1), j1);

            }
        }

    }

    public void appendVRs(DicomObject dicomobject)
        throws DicomException
    {
        for(Enumeration enumeration = ((GroupList) (dicomobject)).enumerateVRs(false, false); enumeration.hasMoreElements();)
        {
            TagValue tagvalue = (TagValue)enumeration.nextElement();
            int i1 = tagvalue.size();
            if(i1 > 0)
            {
                int j = tagvalue.getGroup();
                int l = tagvalue.getElement();
                int j1 = Math.max(0, getSize_ge(j, l));
                for(int k1 = 0; k1 < i1; k1++)
                    set_ge(j, l, tagvalue.getValue(k1), j1 + k1);

            }
        }

    }

    public DicomObject()
    {
        a = null;
        b = 0L;
        c = 0;
        d = 0;
    }

    private void a(int j, int l, int i1, Object obj, int j1)
        throws DicomException
    {
        Object obj1 = null;
        boolean flag = true;
        VR vr = ((GroupList)this).a(j, l);
        if(vr == null)
        {
            if(Debug.DEBUG >= 5)
                if(obj != null)
                    Debug.printMessage("DicomObject.addValue <new>, index: " + j1 + ", type: " + obj.getClass().getName(), j, l);
                else
                    Debug.printMessage("DicomObject.addValue <new>, index: " + j1 + ", type: null", j, l);
            vr = new VR(j, l, i1);
            flag = false;
        } else
        if(Debug.DEBUG >= 5)
            if(obj != null)
                Debug.printMessage("DicomObject.addValue <old>, index: " + j1 + ", type: " + obj.getClass().getName(), j, l);
            else
                Debug.printMessage("DicomObject.addValue <old>, index: " + j1 + ", type: null", j, l);
        if(j1 > ((TagValue) (vr)).val.size())
            throw new DicomException("Index (" + j1 + ") exceeds bounds of Data Element");
        try
        {
            if(obj == null)
                obj1 = null;
            else
            if(obj instanceof String)
                obj1 = e.a((String)obj, vr.dcm_type);
            else
            if(obj instanceof byte[])
                obj1 = e.a((byte[])obj, vr.dcm_type);
            else
            if(obj instanceof Integer)
                obj1 = e.a((Integer)obj, vr.dcm_type);
            else
            if(obj instanceof Float)
                obj1 = e.a((Float)obj, vr.dcm_type);
            else
            if(obj instanceof Double)
                obj1 = e.a((Double)obj, vr.dcm_type);
            else
            if(obj instanceof Person)
                obj1 = e.a((Person)obj, vr.dcm_type);
            else
            if(obj instanceof DDate)
                obj1 = e.a((DDate)obj, vr.dcm_type);
            else
            if(obj instanceof DDateRange)
                obj1 = e.a((DDateRange)obj, vr.dcm_type);
            else
            if(obj instanceof DTime)
                obj1 = e.a((DTime)obj, vr.dcm_type);
            else
            if(obj instanceof DTimeRange)
                obj1 = e.a((DTimeRange)obj, vr.dcm_type);
            else
            if(obj instanceof DDateTime)
                obj1 = e.a((DDateTime)obj, vr.dcm_type);
            else
            if(obj instanceof DDateTimeRange)
                obj1 = e.a((DDateTimeRange)obj, vr.dcm_type);
            else
            if(obj instanceof Short)
                obj1 = e.a((Short)obj, vr.dcm_type);
            else
            if(obj instanceof Long)
                obj1 = e.a((Long)obj, vr.dcm_type);
            else
            if(obj instanceof int[])
                obj1 = e.a((int[])obj, vr.dcm_type);
            else
            if(obj instanceof DicomObject)
                obj1 = e.a((DicomObject)obj, vr.dcm_type);
            else
                throw new DicomException("Unsupported type given: " + obj.getClass().getName());
        }
        catch(DicomException dicomexception)
        {
            throw new DicomException(j, l, ((Throwable) (dicomexception)).getMessage());
        }
        if(!flag)
        {
            if(obj1 == null)
            {
                vr.val = new Vector();
                vr.empty = true;
            } else
            {
                ((TagValue) (vr)).val.addElement(obj1);
                vr.empty = false;
            }
            ((GroupList)this).a(vr);
        } else
        if(obj1 != null)
        {
            if(j1 < ((TagValue) (vr)).val.size())
                ((TagValue) (vr)).val.setElementAt(obj1, j1);
            else
                ((TagValue) (vr)).val.addElement(obj1);
            if(vr.empty)
                vr.empty = false;
        }
    }

    public final int getPixelDataLength()
    {
        return d;
    }

    public void set(int j, Object obj)
        throws DicomException
    {
        int l = DDict.getGroup(j);
        int i1 = DDict.getElement(j);
        set_ge(l, i1, obj);
    }

    public void set_ge(int j, int l, Object obj)
        throws DicomException
    {
        deleteItem_ge(j, l);
        set_ge(j, l, obj, 0);
    }

    public void append(int j, Object obj)
        throws DicomException
    {
        int l = DDict.getGroup(j);
        int i1 = DDict.getElement(j);
        append_ge(l, i1, obj);
    }

    public void append_ge(int j, int l, Object obj)
        throws DicomException
    {
        if(obj != null)
        {
            int i1 = getSize_ge(j, l);
            if(i1 == -1)
                i1 = 0;
            set_ge(j, l, obj, i1);
        }
    }

    public void set(int j, Object obj, int l)
        throws DicomException
    {
        int i1 = DDict.getGroup(j);
        int j1 = DDict.getElement(j);
        set_ge(i1, j1, obj, l);
    }

    public void set_ge(int j, int l, Object obj, int i1)
        throws DicomException
    {
        int j1 = DDict.getTypeCode(j, l);
        a(j, l, j1, obj, i1);
    }

    public int getSize(int j)
    {
        int l = DDict.getGroup(j);
        int i1 = DDict.getElement(j);
        return getSize_ge(l, i1);
    }

    public int getSize_ge(int j, int l)
    {
        VR vr = ((GroupList)this).a(j, l);
        if(vr == null)
            return -1;
        else
            return ((TagValue) (vr)).val.size();
    }

    public long calculateOffset(int j, int l, int i1, boolean flag, boolean flag1)
    {
        return (new Offsets(this)).calculateOffset(j, l, i1, flag, flag1, true);
    }

    public long getOffset(int j, int l)
    {
        VR vr = ((GroupList)this).a(DDict.getGroup(j), DDict.getElement(j));
        if(vr == (VR)null)
            return 0L;
        if(l >= ((TagValue) (vr)).val.size())
            return 0L;
        if(vr.dcm_type != 10)
            return 0L;
        else
            return ((DicomObject)((TagValue) (vr)).val.elementAt(l)).b;
    }

    public Object get(int j)
    {
        return get(j, 0);
    }

    public Object get_ge(int j, int l)
    {
        return get_ge(j, l, 0);
    }

    public Object get(int j, int l)
    {
        int i1 = DDict.getGroup(j);
        int j1 = DDict.getElement(j);
        return get_ge(i1, j1, l);
    }

    public Object get_ge(int j, int l, int i1)
    {
        VR vr = ((GroupList)this).a(j, l);
        if(vr == (VR)null || vr.empty)
            return ((Object) (null));
        if(i1 >= ((TagValue) (vr)).val.size())
            return ((Object) (null));
        else
            return ((TagValue) (vr)).val.elementAt(i1);
    }

    public String getS(int j)
        throws DicomException
    {
        return getS(j, 0);
    }

    public String getS_ge(int j, int l)
        throws DicomException
    {
        return getS_ge(j, l, 0);
    }

    public String getS(int j, int l)
        throws DicomException
    {
        int i1 = DDict.getGroup(j);
        int j1 = DDict.getElement(j);
        return getS_ge(i1, j1, l);
    }

    public String getS_ge(int j, int l, int i1)
        throws DicomException
    {
        Object obj = get_ge(j, l, i1);
        if(obj != null)
        {
            int j1 = ((GroupList)this).a(j, l).dcm_type;
            return e.a(obj, j1);
        } else
        {
            return null;
        }
    }

    public int getI(int j)
        throws DicomException
    {
        return getI(j, 0);
    }

    public int getI_ge(int j, int l)
        throws DicomException
    {
        return getI_ge(j, l, 0);
    }

    public int getI(int j, int l)
        throws DicomException
    {
        int i1 = DDict.getGroup(j);
        int j1 = DDict.getElement(j);
        return getI_ge(i1, j1, l);
    }

    public int getI_ge(int j, int l, int i1)
        throws DicomException
    {
        Object obj = get_ge(j, l, i1);
        if(obj != null)
        {
            int j1 = ((GroupList)this).a(j, l).dcm_type;
            return e.b(obj, j1);
        } else
        {
            return 0x7fffffff;
        }
    }

    public Vector deleteItem(int j)
    {
        int l = DDict.getGroup(j);
        int i1 = DDict.getElement(j);
        return deleteItem_ge(l, i1);
    }

    public Vector deleteItem_ge(int j, int l)
    {
        VR vr = ((GroupList)this).b(j, l);
        if(vr == (VR)null)
            return null;
        else
            return ((TagValue) (vr)).val;
    }

    public Object deleteItem(int j, int l)
    {
        int i1 = DDict.getGroup(j);
        int j1 = DDict.getElement(j);
        return deleteItem_ge(i1, j1, l);
    }

    public Object deleteItem_ge(int j, int l, int i1)
    {
        VR vr = ((GroupList)this).a(j, l);
        if(i1 >= ((TagValue) (vr)).val.size())
        {
            return ((Object) (null));
        } else
        {
            Object obj = ((TagValue) (vr)).val.elementAt(i1);
            ((TagValue) (vr)).val.removeElementAt(i1);
            return obj;
        }
    }

    public void read(InputStream inputstream)
        throws IOException, DicomException
    {
        read(inputstream, true);
    }

    public void read(InputStream inputstream, boolean flag)
        throws IOException, DicomException
    {
        Object obj;
        if(inputstream.markSupported())
            obj = ((Object) (inputstream));
        else
            obj = ((Object) (new BufferedInputStream(inputstream)));
        ((InputStream) (obj)).mark(256);
        boolean flag1;
        try
        {
            flag1 = b(((InputStream) (obj)));
        }
        catch(Exception exception)
        {
            flag1 = false;
        }
        ((InputStream) (obj)).reset();
        if(flag1)
            a(((InputStream) (obj)), flag);
        else
            a(0L, ((InputStream) (obj)), flag);
    }

    long a(InputStream inputstream)
        throws IOException, DicomException
    {
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.readHeader.");
        byte abyte0[] = new byte[128];
        byte abyte1[] = new byte[4];
        f f1 = new f(inputstream);
        ((h) (f1)).c(8194);
        ((OffsetInputStream) (f1)).readFully(abyte0);
        ((OffsetInputStream) (f1)).readFully(abyte1);
        if(abyte1[0] != 68 || abyte1[1] != 73 || abyte1[2] != 67 || abyte1[3] != 77)
            throw new DicomException("Not a valid Dicom-file");
        a = new DicomObject();
        VR vr = new VR();
        vr.readVRHeader(((h) (f1)));
        vr.readVRData(f1);
        for(long l = ((Long)((TagValue) (vr)).val.elementAt(0)).longValue() + ((OffsetInputStream) (f1)).getOffset(); ((OffsetInputStream) (f1)).getOffset() < l; ((GroupList) (a)).a(vr))
        {
            vr = new VR();
            vr.readVRHeader(((h) (f1)));
            vr.readVRData(f1);
        }

        return ((OffsetInputStream) (f1)).getOffset();
    }

    void a(InputStream inputstream, boolean flag)
        throws IOException, DicomException
    {
        long l = a(inputstream);
        a(l, inputstream, flag);
    }

    void a(long l, InputStream inputstream, boolean flag)
        throws IOException, DicomException
    {
        a(l, inputstream, 8193, flag);
    }

    public void read(InputStream inputstream, int j, boolean flag)
        throws IOException, DicomException
    {
        a(0L, inputstream, j, flag);
    }

    void a(long l, InputStream inputstream, int j, boolean flag)
        throws IOException, DicomException
    {
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.readDICOMStream.");
        if(a != null)
            try
            {
                j = UID.getUIDEntry(a.getS(31).trim()).getConstant();
            }
            catch(UnknownUIDException unknownuidexception)
            {
                j = 8192;
            }
        f f1 = new f(inputstream, (int)l);
        ((h) (f1)).c(j);
        a(f1, flag);
    }

    void a(f f1, boolean flag)
        throws IOException, DicomException
    {
        do
        {
            VR vr = new VR();
            try
            {
                vr.readVRHeader(((h) (f1)));
                boolean flag1 = ((TagValue) (vr)).group == 32736 && ((TagValue) (vr)).element == 16;
                if(flag1)
                {
                    d = vr.dataLen;
                    if(!flag)
                        return;
                }
                vr.readVRData(f1);
                if(((TagValue) (vr)).element != 0)
                    ((GroupList)this).a(vr);
                if(flag1)
                    return;
            }
            catch(EOFException eofexception)
            {
                return;
            }
        } while(true);
    }

    boolean b(InputStream inputstream)
        throws IOException, DicomException
    {
        byte abyte0[] = new byte[128];
        byte abyte1[] = new byte[4];
        DataInputStream datainputstream = new DataInputStream(inputstream);
        datainputstream.readFully(abyte0);
        datainputstream.readFully(abyte1);
        return (new String(abyte1)).equals("DICM");
    }

    public void write(OutputStream outputstream, boolean flag)
        throws DicomException, IOException
    {
        if(flag)
        {
            if(a == null)
                a(8193);
            a(outputstream);
        }
        int j;
        try
        {
            j = UID.getUIDEntry(a.getS(31)).getConstant();
        }
        catch(Exception exception)
        {
            j = 8193;
        }
        i l = new i(outputstream);
        ((k) (l)).a(false);
        ((k) (l)).a(j);
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.writeTags. transfer syntax: " + j);
        a(l, true);
    }

    void a(OutputStream outputstream)
        throws IOException, DicomException
    {
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.writeHeader.");
        byte abyte0[] = new byte[128];
        i j = new i(outputstream);
        ((k) (j)).a(8194);
        for(int l = 0; l < 128; l++)
            abyte0[l] = 0;

        ((OffsetOutputStream) (j)).write(abyte0);
        ((OffsetOutputStream) (j)).write(68);
        ((OffsetOutputStream) (j)).write(73);
        ((OffsetOutputStream) (j)).write(67);
        ((OffsetOutputStream) (j)).write(77);
        a.a(j, true);
    }

    public void write(OutputStream outputstream, boolean flag, int j, boolean flag1)
        throws DicomException, IOException
    {
        if(flag)
        {
            if(a == null)
                a(j);
            a(outputstream);
        }
        i l = new i(outputstream);
        ((k) (l)).a(flag1);
        ((k) (l)).a(j);
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.writeTags. transfer syntax: " + j);
        a(l, true);
    }

    public void write(OutputStream outputstream, boolean flag, int j, boolean flag1, boolean flag2)
        throws DicomException, IOException
    {
        if(flag)
        {
            if(a == null)
                a(j);
            a(outputstream);
        }
        i l = new i(outputstream);
        ((k) (l)).a(flag1);
        ((k) (l)).a(j);
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.writeTags. transfer syntax: " + j);
        a(l, flag2);
    }

    void a(i j, boolean flag)
        throws IOException, DicomException
    {
        VR vr;
        for(Enumeration enumeration = ((GroupList)this).a(flag, ((k) (j)).a(), ((k) (j)).b(), true); enumeration.hasMoreElements(); vr.writeVRData(j))
        {
            vr = (VR)enumeration.nextElement();
            vr.writeVRHeader(((k) (j)));
        }

    }

    void a(int j)
        throws DicomException
    {
        if(a == null)
            b(j);
        else
            try
            {
                a.set(31, ((Object) (UID.getUIDEntry(j).getValue())), 0);
            }
            catch(IllegalValueException illegalvalueexception)
            {
                ((Throwable) (illegalvalueexception)).printStackTrace();
            }
    }

    private void b(int j)
        throws DicomException
    {
        if(Debug.DEBUG > 3)
            Debug.a("DicomObject.makeDICOMFileMetaInformation. transfer syntax: " + j);
        if(a == null)
            a = new DicomObject();
        if(getSize(62) != 1 || getSize(63) != 1)
            throw new DicomException("This DicomObject can't be written to a Dicom File: Unable to find SOP Common Info");
        byte abyte0[] = {
            0, 1
        };
        a.set(28, ((Object) (abyte0)), 0);
        a.set(29, get(62), 0);
        a.set(30, get(63), 0);
        String s = a.getS(31);
        if(s != null)
        {
            int l = 0;
            try
            {
                l = UID.getUIDEntry(s).getConstant();
            }
            catch(UnknownUIDException unknownuidexception)
            {
                throw new DicomException("Cannot change unknown transfersyntax into new one");
            }
            if(l != 8193 && l != 8194 && l != 8195)
                throw new DicomException("Cannot change encapsulated transfersyntax, use Compression.decompress().");
        }
        try
        {
            a.set(31, ((Object) (UID.getUIDEntry(j).getValue())), 0);
        }
        catch(IllegalValueException illegalvalueexception)
        {
            ((Throwable) (illegalvalueexception)).printStackTrace();
        }
        if(a.getSize(32) == 0)
            a.set(32, "1.2.826.0.1.3680043.2.60.0.1.888");
        if(a.getSize(33) == 0)
            a.set(33, "SoftLink JDT 1.0");
    }

    public DicomObject getFileMetaInformation()
    {
        return a;
    }

    public DicomObject setFileMetaInformation(DicomObject dicomobject)
    {
        DicomObject dicomobject1 = a;
        a = dicomobject;
        return dicomobject1;
    }

    public void dumpVRs(OutputStream outputstream)
        throws IOException
    {
        dumpVRs(outputstream, false);
    }

    public void dumpVRs(OutputStream outputstream, boolean flag)
        throws IOException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        if(flag && a != null)
            a.dumpVRs(outputstream);
        VR vr;
        for(Enumeration enumeration = ((GroupList)this).a(false, 8193, true, true); enumeration.hasMoreElements(); dataoutputstream.writeBytes(DumpUtils.dumpVR(vr, 0)))
            vr = (VR)enumeration.nextElement();

    }

}
