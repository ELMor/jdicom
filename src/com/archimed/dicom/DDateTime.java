// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

// Referenced classes of package com.archimed.dicom:
//            DDate

public class DDateTime
{

    private Calendar a;
    private boolean b;

    public DDateTime()
    {
        b = false;
        b = true;
    }

    public DDateTime(String s)
        throws NumberFormatException
    {
        b = false;
        s = s.trim();
        int i = s.length();
        if(i == 0)
        {
            b = true;
            return;
        }
        int k;
        int l;
        int i1;
        int j1;
        int k1;
        int l1;
        int j = k = l = i1 = j1 = k1 = l1 = 0;
        switch(Math.min(i, 15))
        {
        case 15: // '\017'
            if(s.charAt(14) != '.')
                throw new NumberFormatException("Cannot parse string into DDateTime");
            if(i > 15)
                l1 = (int)(Float.parseFloat(s.substring(14)) * 1000F);
            // fall through

        case 14: // '\016'
            k1 = Integer.parseInt(s.substring(12, 14));
            // fall through

        case 12: // '\f'
            j1 = Integer.parseInt(s.substring(10, 12));
            // fall through

        case 10: // '\n'
            i1 = Integer.parseInt(s.substring(8, 10));
            // fall through

        case 8: // '\b'
            l = Integer.parseInt(s.substring(6, 8));
            k = Integer.parseInt(s.substring(4, 6));
            j = Integer.parseInt(s.substring(0, 4));
            DDate.a(j, k, l);
            break;

        case 9: // '\t'
        case 11: // '\013'
        case 13: // '\r'
        default:
            throw new NumberFormatException("Cannot parse string into DDateTime");
        }
        a = ((Calendar) (new GregorianCalendar(j, k - 1, l)));
        a.set(11, i1);
        a.set(12, j1);
        a.set(13, k1);
        a.set(14, l1);
    }

    public String toString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss.SSS");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public String toString(DateFormat dateformat)
    {
        if(b)
            return "";
        else
            return dateformat.format(a.getTime());
    }

    public String toDICOMString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public boolean isEmpty()
    {
        return b;
    }
}
