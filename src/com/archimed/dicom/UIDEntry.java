// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import com.archimed.tool.n;

public class UIDEntry
{

    public static final int SOPClass = 1;
    public static final int WellknownSOPInstance = 2;
    public static final int TransferSyntax = 3;
    public static final int MetaSOPClass = 4;
    public static final int ApplicationContextName = 5;
    private int a;
    private String b;
    private String c;
    private String d;
    private int e;

    public UIDEntry(int i, String s, String s1, String s2, int j)
    {
        a = i;
        b = s;
        c = s1;
        d = s2;
        e = j;
    }

    public int getConstant()
    {
        return a;
    }

    public String getValue()
    {
        return b;
    }

    public String getName()
    {
        return c;
    }

    public String getShortName()
    {
        return d;
    }

    public int getType()
    {
        return e;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof UIDEntry))
            return false;
        else
            return a == ((UIDEntry)obj).a;
    }

    public String toString()
    {
        return b;
    }

    public String toLongString()
    {
        return n.fstr(b, 30) + n.fstr(d, 6) + n.fstr(c, 40);
    }
}
