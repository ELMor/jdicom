// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Vector;

class DicomUtils
{

    DicomUtils()
    {
    }

    static void a(byte abyte0[])
    {
        for(int i = 0; i < abyte0.length; i += 2)
        {
            byte byte0 = abyte0[i];
            abyte0[i] = abyte0[i + 1];
            abyte0[i + 1] = byte0;
        }

    }

    public static Vector bubbleSort(Vector vector)
    {
        int i = vector.size();
        for(int j = 0; j < i; j++)
        {
            boolean flag = true;
            for(int k = 0; k < i - 1; k++)
                if(((Integer)vector.elementAt(k)).intValue() > ((Integer)vector.elementAt(k + 1)).intValue())
                {
                    Integer integer = (Integer)vector.elementAt(k + 1);
                    vector.setElementAt(vector.elementAt(k), k + 1);
                    vector.setElementAt(((Object) (integer)), k);
                    flag &= false;
                }

            if(flag)
                j = i;
        }

        return vector;
    }
}
