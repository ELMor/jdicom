// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.PrintStream;

// Referenced classes of package com.archimed.dicom:
//            DumpUtils, DDict

public class Debug
{

    public static PrintStream out;
    public static String dumpCmdsetIntoDir = null;
    public static String dumpDatasetIntoDir = null;
    public static int DEBUG = 10;

    public Debug()
    {
    }

    public static void printMessage(String s, int i, int j)
    {
        out.println(s + ": (" + DumpUtils.a(i, 4) + "," + DumpUtils.a(j, 4) + ")  " + DDict.getDescription(DDict.lookupDDict(i, j)) + ".");
    }

    static void a(String s)
    {
        out.println(s);
    }

    static
    {
        out = System.out;
    }
}
