// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class OffsetOutputStream extends FilterOutputStream
{

    private long a;

    public OffsetOutputStream(OutputStream outputstream)
    {
        this(outputstream, 0);
    }

    public OffsetOutputStream(OutputStream outputstream, int i)
    {
        super(outputstream);
        a = 0L;
        a = i;
    }

    public void write(byte abyte0[], int i, int j)
        throws IOException
    {
        a += j;
        super.out.write(abyte0, i, j);
    }

    public void write(int i)
        throws IOException
    {
        a++;
        super.out.write(i);
    }

    public void write(byte abyte0[])
        throws IOException
    {
        write(abyte0, 0, abyte0.length);
    }
}
