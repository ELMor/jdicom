// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.archimed.dicom:
//            h

class g extends h
{

    g(InputStream inputstream)
    {
        super(inputstream);
    }

    g(InputStream inputstream, int j)
    {
        super(inputstream, j);
    }

    String a()
        throws IOException
    {
        return ((h)this).d(4);
    }

    Integer b()
        throws IOException
    {
        return new Integer((((h)this).k() << 16) + ((h)this).k());
    }

    Float c()
        throws IOException
    {
        return new Float(Float.intBitsToFloat(((h)this).m()));
    }

    Double d()
        throws IOException
    {
        return new Double(Double.longBitsToDouble(((h)this).o()));
    }

    Long e()
        throws IOException
    {
        return new Long(((h)this).n());
    }

    Short f()
        throws IOException
    {
        return new Short((short)((h)this).l());
    }

    Long g()
        throws IOException
    {
        return new Long(((h)this).m());
    }

    Integer h()
        throws IOException
    {
        return new Integer(((h)this).k());
    }

    Integer i()
        throws IOException
    {
        return new Integer(((h)this).k());
    }

    Object b(int j)
        throws IOException
    {
        switch(j)
        {
        case 17: // '\021'
            return ((Object) (a()));

        case 5: // '\005'
            return ((Object) (b()));

        case 26: // '\032'
            return ((Object) (c()));

        case 20: // '\024'
            return ((Object) (d()));

        case 19: // '\023'
            return ((Object) (e()));

        case 23: // '\027'
            return ((Object) (f()));

        case 1: // '\001'
            return ((Object) (g()));

        case 3: // '\003'
            return ((Object) (h()));

        case 21: // '\025'
            return ((Object) (i()));

        case 2: // '\002'
        case 4: // '\004'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 11: // '\013'
        case 12: // '\f'
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 18: // '\022'
        case 22: // '\026'
        case 24: // '\030'
        case 25: // '\031'
        default:
            return ((Object) (null));
        }
    }
}
