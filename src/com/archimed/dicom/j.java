// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.OutputStream;

// Referenced classes of package com.archimed.dicom:
//            k

class j extends k
{

    j(OutputStream outputstream)
    {
        super(outputstream);
    }

    j(OutputStream outputstream, int l)
    {
        super(outputstream, l);
    }

    int a(Object obj)
        throws IOException
    {
        return ((k)this).a((String)obj);
    }

    int b(Object obj)
        throws IOException
    {
        int l = ((Integer)obj).intValue();
        return ((k)this).b(l >>> 16) + ((k)this).b(l);
    }

    int c(Object obj)
        throws IOException
    {
        return ((k)this).c(Float.floatToIntBits(((Float)obj).floatValue()));
    }

    int d(Object obj)
        throws IOException
    {
        return ((k)this).a(Double.doubleToLongBits(((Double)obj).doubleValue()));
    }

    int e(Object obj)
        throws IOException
    {
        return ((k)this).c(((Long)obj).intValue());
    }

    int f(Object obj)
        throws IOException
    {
        return ((k)this).b(((Short)obj).intValue());
    }

    int g(Object obj)
        throws IOException
    {
        return ((k)this).c(((Long)obj).intValue());
    }

    int h(Object obj)
        throws IOException
    {
        return ((k)this).b(((Integer)obj).intValue());
    }

    int i(Object obj)
        throws IOException
    {
        return ((k)this).b(((Integer)obj).intValue());
    }

    int b(Object obj, int l)
        throws IOException
    {
        switch(l)
        {
        case 17: // '\021'
            return a(obj);

        case 5: // '\005'
            return b(obj);

        case 26: // '\032'
            return c(obj);

        case 20: // '\024'
            return d(obj);

        case 19: // '\023'
            return e(obj);

        case 23: // '\027'
            return f(obj);

        case 1: // '\001'
            return g(obj);

        case 3: // '\003'
            return h(obj);

        case 21: // '\025'
            return i(obj);

        case 2: // '\002'
        case 4: // '\004'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 11: // '\013'
        case 12: // '\f'
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 18: // '\022'
        case 22: // '\026'
        case 24: // '\030'
        case 25: // '\031'
        default:
            return 0;
        }
    }
}
