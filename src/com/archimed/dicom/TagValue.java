// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            DicomException

public class TagValue
{

    protected int group;
    protected int element;
    protected Vector val;

    public TagValue()
    {
        group = 0;
        element = 0;
        val = new Vector();
    }

    public int getGroup()
    {
        return group;
    }

    public int getElement()
    {
        return element;
    }

    public int size()
    {
        return val.size();
    }

    public Object getValue(int i)
        throws DicomException
    {
        if(i < val.size())
            return val.elementAt(i);
        else
            throw new DicomException("Index exceeds bounds of array.");
    }

    public Object getValue()
    {
        try
        {
            return getValue(0);
        }
        catch(DicomException dicomexception)
        {
            return ((Object) (null));
        }
    }
}
