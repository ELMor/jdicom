// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem

class AsynchronousOperationsWindowSubItem extends SubItem
{

    private int a;
    private int b;
    private int c;

    public AsynchronousOperationsWindowSubItem()
    {
        a = 83;
    }

    public AsynchronousOperationsWindowSubItem(int i, int j)
    {
        a = 83;
        b = i;
        c = j;
    }

    int a()
    {
        return b;
    }

    int b()
    {
        return c;
    }

    void a(int i)
    {
        b = i;
    }

    void b(int i)
    {
        c = i;
    }

    public int getLength()
    {
        return 8;
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("wrong itemtype of abstract syntax");
        ((FilterInputStream) (datainputstream)).read();
        char c1 = datainputstream.readChar();
        if(c1 != '\004')
        {
            throw new IllegalValueException("length of AsynchronousOperationsWindowSubItem should be 4");
        } else
        {
            b = ((int) (datainputstream.readChar()));
            c = ((int) (datainputstream.readChar()));
            return 8;
        }
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(4);
        dataoutputstream.writeChar(b);
        dataoutputstream.writeChar(c);
    }
}
