// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package com.archimed.dicom.network:
//            TransferSyntaxItem

class PresentationContextResponseItem
{

    private byte a;
    private int b;
    private int c;
    private int d;
    TransferSyntaxItem e;

    public PresentationContextResponseItem()
    {
        a = 33;
        e = new TransferSyntaxItem();
    }

    public PresentationContextResponseItem(int i, int j, TransferSyntaxItem transfersyntaxitem)
    {
        a = 33;
        e = new TransferSyntaxItem();
        b = i;
        d = j;
        e = transfersyntaxitem;
        c = 4;
        if(transfersyntaxitem != null)
            c += transfersyntaxitem.getLength();
    }

    public int getID()
    {
        return b;
    }

    public int getResult()
    {
        return d;
    }

    public UIDEntry getTransferSyntax()
    {
        return e.getTransferSyntax();
    }

    public int getLength()
    {
        return c + 4;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException, IllegalValueException
    {
        dataoutputstream.write(((int) (a)));
        dataoutputstream.write(0);
        dataoutputstream.writeChar(c);
        dataoutputstream.write(b);
        dataoutputstream.write(0);
        dataoutputstream.write(d);
        dataoutputstream.write(0);
        if(e != null)
            e.a(dataoutputstream);
    }

    public void read(DataInputStream datainputstream)
        throws IllegalValueException, IOException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("itemtype field of received Presentation Context Item  not " + a);
        ((FilterInputStream) (datainputstream)).skip(1L);
        c = ((int) (datainputstream.readChar()));
        if(Debug.DEBUG > 3)
            Debug.out.println("PresentationContextResponseItem length: " + c);
        b = ((FilterInputStream) (datainputstream)).read();
        ((FilterInputStream) (datainputstream)).skip(1L);
        d = ((FilterInputStream) (datainputstream)).read();
        ((FilterInputStream) (datainputstream)).skip(1L);
        if(d == 0)
            e.a(datainputstream);
        else
            ((FilterInputStream) (datainputstream)).skip(c - 4);
    }
}
