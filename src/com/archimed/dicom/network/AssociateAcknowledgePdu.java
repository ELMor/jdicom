// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PushbackInputStream;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            Acknowledge, ApplicationContextItem, PresentationContextResponseItem, TransferSyntaxItem, 
//            UserInfoItem, ImplementationClassUIDSubItem, ImplementationVersionNameSubItem, MaximumLengthSubItem, 
//            ScuScpRoleSubItem, AsynchronousOperationsWindowSubItem, SubItem

class AssociateAcknowledgePdu
{

    private int a;
    private int b;
    private int c;
    private String d;
    private String e;
    private ApplicationContextItem f;
    private UserInfoItem g;
    private PresentationContextResponseItem h;
    private Vector i;
    private Acknowledge j;

    public AssociateAcknowledgePdu()
    {
        a = 2;
        i = new Vector();
    }

    public AssociateAcknowledgePdu(Acknowledge acknowledge)
        throws IllegalValueException
    {
        a = 2;
        d = acknowledge.getCalledTitle();
        e = acknowledge.getCallingTitle();
        f = new ApplicationContextItem();
        i = new Vector();
        for(int k = 0; k < acknowledge.getPresentationContexts(); k++)
        {
            UIDEntry uidentry = acknowledge.getTransferSyntax(k);
            if(uidentry == null)
                h = new PresentationContextResponseItem(((int) (acknowledge.getPresentationContextID(k))), acknowledge.getResult(k), ((TransferSyntaxItem) (null)));
            else
                h = new PresentationContextResponseItem(((int) (acknowledge.getPresentationContextID(k))), acknowledge.getResult(k), new TransferSyntaxItem(uidentry));
            i.addElement(((Object) (h)));
        }

        g = new UserInfoItem();
        ImplementationClassUIDSubItem implementationclassuidsubitem = new ImplementationClassUIDSubItem(acknowledge.getImplementationClassUID());
        ImplementationVersionNameSubItem implementationversionnamesubitem = new ImplementationVersionNameSubItem(acknowledge.getImplementationVersionName());
        MaximumLengthSubItem maximumlengthsubitem = new MaximumLengthSubItem(acknowledge.getMaxPduSize());
        g.addSubItem(((SubItem) (maximumlengthsubitem)));
        g.addSubItem(((SubItem) (implementationclassuidsubitem)));
        for(int j1 = 0; j1 < acknowledge.a(); j1++)
        {
            UIDEntry uidentry1 = acknowledge.a(j1);
            int l = acknowledge.getScuRole(uidentry1.getConstant());
            int i1 = acknowledge.getScpRole(uidentry1.getConstant());
            if(l != -1 && i1 != -1)
            {
                ScuScpRoleSubItem scuscprolesubitem = new ScuScpRoleSubItem(uidentry1, l, i1);
                g.addSubItem(((SubItem) (scuscprolesubitem)));
            }
        }

        g.addSubItem(((SubItem) (implementationversionnamesubitem)));
        if(acknowledge.getMaxOperationsInvoked() != 1 || acknowledge.getMaxOperationsPerformed() != 1)
        {
            AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem = new AsynchronousOperationsWindowSubItem();
            asynchronousoperationswindowsubitem.a(acknowledge.getMaxOperationsInvoked());
            asynchronousoperationswindowsubitem.b(acknowledge.getMaxOperationsPerformed());
            g.addSubItem(((SubItem) (asynchronousoperationswindowsubitem)));
        }
        b = 68;
        b += f.a();
        for(int k1 = 0; k1 < i.size(); k1++)
            b += ((PresentationContextResponseItem)i.elementAt(k1)).getLength();

        b += g.getLength();
    }

    public Acknowledge getAcknowledge()
    {
        return j;
    }

    private void a()
        throws IllegalValueException
    {
        j = new Acknowledge();
        j.setCalledTitle(d);
        j.setCallingTitle(e);
        j.setApplicationContextUid(f.getUid());
        Vector vector = g.getSubItems();
        for(int k = 0; k < i.size(); k++)
        {
            PresentationContextResponseItem presentationcontextresponseitem = (PresentationContextResponseItem)i.elementAt(k);
            if(presentationcontextresponseitem.getResult() == 0)
                j.addPresentationContext((byte)presentationcontextresponseitem.getID(), presentationcontextresponseitem.getResult(), presentationcontextresponseitem.getTransferSyntax().getConstant());
            else
                j.addPresentationContext((byte)presentationcontextresponseitem.getID(), presentationcontextresponseitem.getResult(), 8193);
        }

        for(int l = 0; l < vector.size(); l++)
        {
            SubItem subitem = (SubItem)vector.elementAt(l);
            if(subitem instanceof ImplementationClassUIDSubItem)
                j.a(((ImplementationClassUIDSubItem)subitem).getImplementationClassUID());
            else
            if(subitem instanceof ImplementationVersionNameSubItem)
                j.b(((ImplementationVersionNameSubItem)subitem).getImplementationVersionName());
            else
            if(subitem instanceof MaximumLengthSubItem)
                j.setMaxPduSize(((MaximumLengthSubItem)subitem).getMaxPduSize());
            else
            if(subitem instanceof ScuScpRoleSubItem)
            {
                ScuScpRoleSubItem scuscprolesubitem = (ScuScpRoleSubItem)subitem;
                j.setScuScpRoleSelection(scuscprolesubitem.getAbstractSyntax().getConstant(), scuscprolesubitem.getScuRole(), scuscprolesubitem.getScpRole());
            } else
            if(subitem instanceof AsynchronousOperationsWindowSubItem)
            {
                AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem = (AsynchronousOperationsWindowSubItem)subitem;
                j.setMaxOperationsInvoked(asynchronousoperationswindowsubitem.a());
                j.setMaxOperationsPerformed(asynchronousoperationswindowsubitem.b());
            }
        }

    }

    public int getLength()
    {
        return b + 6;
    }

    public Vector getPresentationContextResponseItems()
    {
        return i;
    }

    public void write(OutputStream outputstream)
        throws IOException, IllegalValueException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
        outputstream.write(a);
        outputstream.write(0);
        dataoutputstream.writeInt(getLength() - 6);
        dataoutputstream.writeChar(1);
        dataoutputstream.writeChar(0);
        outputstream.write(d.getBytes());
        for(int k = 0; k < 16 - d.length(); k++)
            outputstream.write(32);

        outputstream.write(e.getBytes());
        for(int l = 0; l < 16 - e.length(); l++)
            outputstream.write(32);

        outputstream.write(new byte[32]);
        f.a(dataoutputstream);
        for(int i1 = 0; i1 < i.size(); i1++)
        {
            PresentationContextResponseItem presentationcontextresponseitem = (PresentationContextResponseItem)i.elementAt(i1);
            presentationcontextresponseitem.write(dataoutputstream);
        }

        g.write(dataoutputstream);
    }

    public void read(InputStream inputstream)
        throws IOException, IllegalValueException, UnknownUIDException
    {
        PushbackInputStream pushbackinputstream = new PushbackInputStream(inputstream);
        DataInputStream datainputstream = new DataInputStream(((InputStream) (pushbackinputstream)));
        int k = ((FilterInputStream) (datainputstream)).read();
        if(k != a)
            throw new IllegalValueException("PDU-type field of Associate Acknowledge PDU not " + a);
        ((FilterInputStream) (datainputstream)).read();
        b = datainputstream.readInt();
        if(Debug.DEBUG > 3)
            Debug.out.println("PDU length: " + b);
        c = ((int) (datainputstream.readChar()));
        b -= 2;
        inputstream.skip(66L);
        b -= 66;
        f = new ApplicationContextItem();
        for(b = b - f.read(datainputstream); b > 0;)
        {
            int l = ((FilterInputStream) (datainputstream)).read();
            if(l == 33)
            {
                pushbackinputstream.unread(33);
                h = new PresentationContextResponseItem();
                h.read(datainputstream);
                i.addElement(((Object) (h)));
                b = b - h.getLength();
            } else
            if(l == 80)
            {
                pushbackinputstream.unread(80);
                g = new UserInfoItem();
                int i1 = g.read(pushbackinputstream);
                b = b - i1;
            }
            if(Debug.DEBUG > 3)
                Debug.out.println("Remaining PDU length: " + b);
        }

        a();
    }
}
