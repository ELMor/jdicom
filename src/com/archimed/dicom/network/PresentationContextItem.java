// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            AbstractSyntaxItem, TransferSyntaxItem

class PresentationContextItem
{

    private byte a;
    private int b;
    private Vector c;
    private AbstractSyntaxItem d;

    public PresentationContextItem()
    {
        a = 32;
    }

    public PresentationContextItem(int i, AbstractSyntaxItem abstractsyntaxitem, Vector vector)
    {
        a = 32;
        b = i;
        d = abstractsyntaxitem;
        c = vector;
    }

    public int getID()
    {
        return b;
    }

    public int getLength()
    {
        return 4 + a();
    }

    public AbstractSyntaxItem getAbstractSyntaxItem()
    {
        return d;
    }

    public Vector getTransferSyntaxes()
    {
        return c;
    }

    public void addTransferSyntaxItem(TransferSyntaxItem transfersyntaxitem)
    {
        c.addElement(((Object) (transfersyntaxitem)));
    }

    private int a()
    {
        int i = 4;
        i += d.getLength();
        for(int j = 0; j < c.size(); j++)
        {
            TransferSyntaxItem transfersyntaxitem = (TransferSyntaxItem)c.elementAt(j);
            i += transfersyntaxitem.getLength();
        }

        return i;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(((int) (a)));
        dataoutputstream.write(0);
        dataoutputstream.writeChar(a());
        dataoutputstream.write(b);
        dataoutputstream.write(0);
        dataoutputstream.write(0);
        dataoutputstream.write(0);
        d.write(dataoutputstream);
        for(int i = 0; i < c.size(); i++)
            ((TransferSyntaxItem)c.elementAt(i)).a(dataoutputstream);

    }

    int a(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("wrong item type of presentation context item");
        ((FilterInputStream) (datainputstream)).read();
        int j = ((int) (datainputstream.readChar()));
        int k = j;
        b = ((FilterInputStream) (datainputstream)).read();
        ((FilterInputStream) (datainputstream)).read();
        ((FilterInputStream) (datainputstream)).read();
        ((FilterInputStream) (datainputstream)).read();
        j -= 4;
        d = new AbstractSyntaxItem();
        int l = d.read(datainputstream);
        j -= l;
        c = new Vector();
        int i1;
        for(; j > 0; j -= i1)
        {
            TransferSyntaxItem transfersyntaxitem = new TransferSyntaxItem();
            i1 = transfersyntaxitem.a(datainputstream);
            c.addElement(((Object) (transfersyntaxitem)));
        }

        return k + 4;
    }
}
