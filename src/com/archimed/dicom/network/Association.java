// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.y;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PushbackInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            Abort, AbortPdu, Reject, AssociateRejectPdu, 
//            AssociateAcknowledgePdu, Acknowledge, Request, PduOutputStream, 
//            AssociateRequestPdu, ResponsePolicy, Response

public class Association
{

    private static final int a = 8;
    private static final int b = 9;
    public static final int PDATA_PDU = 10;
    public static final int RELEASE_REQUEST = 11;
    public static final int ABORT = 12;
    public static final int RELEASE_RESPONSE = 13;
    public static final int ASSOCIATE_REQUEST = 14;
    public static final int ASSOCIATE_ACKNOWLEDGE = 15;
    public static final int ASSOCIATE_REJECT = 16;
    static final String c = "1.2.826.0.1.3680043.2.60.0.1";
    static final String d = "softlink_jdt103";
    private boolean e;
    private boolean f;
    private String g;
    private String h;
    private int i;
    private InputStream j;
    private OutputStream k;
    private PushbackInputStream l;
    private Request m;
    private Acknowledge n;
    private ByteArrayOutputStream o;
    private ByteArrayOutputStream p;
    private AssociateRequestPdu q;
    private AssociateAcknowledgePdu r;
    private Vector s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private static final SimpleDateFormat x = new SimpleDateFormat("yyyyMMddHHmmss.SSS");

    public Association(InputStream inputstream, OutputStream outputstream)
    {
        e = true;
        f = false;
        t = false;
        u = false;
        w = 0;
        j = inputstream;
        k = outputstream;
        l = new PushbackInputStream(inputstream);
        o = new ByteArrayOutputStream();
        p = new ByteArrayOutputStream();
    }

    public void setGrouplens(boolean flag)
    {
        e = flag;
    }

    public void setSeqUndef(boolean flag)
    {
        f = flag;
    }

    public DicomObject receiveCommand()
        throws IOException, IllegalValueException, DicomException, UnknownUIDException
    {
        byte abyte0[] = receiveCommandAsByteArray();
        if(abyte0 != null)
        {
            DicomObject dicomobject = new DicomObject();
            dicomobject.read(((InputStream) (new ByteArrayInputStream(abyte0))), true);
            abyte0 = null;
            return dicomobject;
        } else
        {
            return null;
        }
    }

    public byte[] receiveCommandAsByteArray()
        throws IOException, IllegalValueException
    {
        if(t || a() == 8)
        {
            byte abyte0[] = o.toByteArray();
            o.reset();
            t = false;
            if(Debug.dumpCmdsetIntoDir != null && Debug.dumpCmdsetIntoDir.length() != 0)
                a(abyte0, Debug.dumpCmdsetIntoDir, ".cmd.dcm");
            return abyte0;
        } else
        {
            return null;
        }
    }

    public DicomObject receiveData()
        throws IOException, IllegalValueException, DicomException, UnknownUIDException
    {
        byte abyte0[] = receiveDataAsByteArray();
        if(abyte0 != null)
        {
            UIDEntry uidentry = getTransferSyntax((byte)v);
            int i1 = a(abyte0, uidentry);
            if(i1 != 0)
                Debug.out.println("WARNING: received dataset contains (0002,xxxx) File Meta Elements [will be skipped]");
            DicomObject dicomobject = new DicomObject();
            dicomobject.read(((InputStream) (new ByteArrayInputStream(abyte0, i1, abyte0.length - i1))), uidentry.getConstant(), true);
            abyte0 = null;
            return dicomobject;
        } else
        {
            return null;
        }
    }

    private int a(byte abyte0[], UIDEntry uidentry)
    {
        switch(uidentry.getConstant())
        {
        case 8195: 
            if(((abyte0[0] & 0xff) << 8) + (abyte0[1] & 0xff) == 2)
                return 12 + ((abyte0[8] & 0xff) << 24) + ((abyte0[9] & 0xff) << 32) + ((abyte0[10] & 0xff) << 8) + (abyte0[11] & 0xff);
            break;

        case 8193: 
        default:
            if(((abyte0[1] & 0xff) << 8) + (abyte0[0] & 0xff) == 2)
                return 12 + ((abyte0[11] & 0xff) << 24) + ((abyte0[10] & 0xff) << 32) + ((abyte0[9] & 0xff) << 8) + (abyte0[8] & 0xff);
            break;
        }
        return 0;
    }

    private void a(byte abyte0[], String s1, String s2)
    {
        try
        {
            File file = new File(s1);
            if(!file.exists())
                file.mkdirs();
            File file1 = new File(file, ((DateFormat) (x)).format(new Date()) + s2);
            FileOutputStream fileoutputstream = new FileOutputStream(file1);
            try
            {
                ((OutputStream) (fileoutputstream)).write(abyte0);
            }
            finally
            {
                ((OutputStream) (fileoutputstream)).close();
            }
        }
        catch(Exception exception)
        {
            Debug.out.println(((Object) (exception)));
        }
    }

    public byte[] receiveDataAsByteArray()
        throws IOException, IllegalValueException
    {
        if(u || a() == 9)
        {
            byte abyte0[] = p.toByteArray();
            p.reset();
            u = false;
            if(Debug.dumpDatasetIntoDir != null && Debug.dumpDatasetIntoDir.length() != 0)
                a(abyte0, Debug.dumpDatasetIntoDir, ".data.dcm");
            return abyte0;
        } else
        {
            return null;
        }
    }

    public void sendAssociateResponse(Response response)
        throws IOException, IllegalValueException
    {
        if(response instanceof Abort)
        {
            Abort abort = (Abort)response;
            AbortPdu abortpdu = new AbortPdu(abort.getSource(), abort.getReason());
            abortpdu.write(k);
        } else
        if(response instanceof Reject)
        {
            Reject reject = (Reject)response;
            AssociateRejectPdu associaterejectpdu = new AssociateRejectPdu(reject.getResult(), reject.getSource(), reject.getReason());
            associaterejectpdu.write(k);
        } else
        if(response instanceof Acknowledge)
        {
            n = (Acknowledge)response;
            r = new AssociateAcknowledgePdu(n);
            r.write(k);
            s = r.getPresentationContextResponseItems();
        }
    }

    public void sendReleaseResponse()
        throws IOException, IllegalValueException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(k);
        k.write(6);
        k.write(0);
        dataoutputstream.writeInt(4);
        dataoutputstream.writeInt(0);
    }

    public byte getPresentationContext(int i1, int j1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        if(uidentry.getType() != 4 && uidentry.getType() != 1)
            throw new IllegalValueException("first argument should be Meta SOP Class or SOP Class");
        UIDEntry uidentry2 = UID.getUIDEntry(j1);
        if(uidentry2.getType() != 3)
            throw new IllegalValueException("second argument should be Transfer Syntax");
        for(int k1 = 0; k1 < m.getPresentationContexts(); k1++)
        {
            UIDEntry uidentry1 = m.getAbstractSyntax(k1);
            if(uidentry1.getConstant() == i1)
            {
                byte byte0 = m.getPresentationContextID(k1);
                for(int l1 = 0; l1 < n.getPresentationContexts(); l1++)
                {
                    byte byte1 = n.getPresentationContextID(l1);
                    if(byte1 == byte0 && n.getResult(l1) == 0)
                    {
                        UIDEntry uidentry3 = n.getTransferSyntax(l1);
                        if(uidentry3.getConstant() == j1)
                            return byte0;
                    }
                }

            }
        }

        throw new IllegalValueException("no accepted presentation context with specified abstract syntax");
    }

    public byte[] listAcceptedPresentationContexts(int i1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        if(uidentry.getType() != 4 && uidentry.getType() != 1)
            throw new IllegalValueException("first argument should be Meta SOP Class or SOP Class");
        int j1 = 0;
        int k1 = m.getPresentationContexts();
        byte abyte0[] = new byte[k1];
        for(int l1 = 0; l1 < k1; l1++)
        {
            UIDEntry uidentry1 = m.getAbstractSyntax(l1);
            if(uidentry1.getConstant() == i1)
            {
                byte byte0 = m.getPresentationContextID(l1);
                for(int i2 = 0; i2 < n.getPresentationContexts(); i2++)
                {
                    byte byte1 = n.getPresentationContextID(i2);
                    if(byte1 != byte0)
                        continue;
                    if(n.getResult(i2) == 0)
                        abyte0[j1++] = byte0;
                    break;
                }

            }
        }

        byte abyte1[] = new byte[j1];
        System.arraycopy(((Object) (abyte0)), 0, ((Object) (abyte1)), 0, j1);
        return abyte1;
    }

    public UIDEntry[] listAcceptedTransferSyntaxes(int i1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        if(uidentry.getType() != 4 && uidentry.getType() != 1)
            throw new IllegalValueException("first argument should be Meta SOP Class or SOP Class");
        int j1 = 0;
        int k1 = m.getPresentationContexts();
        UIDEntry auidentry[] = new UIDEntry[k1];
        for(int l1 = 0; l1 < k1; l1++)
        {
            UIDEntry uidentry1 = m.getAbstractSyntax(l1);
            if(uidentry1.getConstant() == i1)
            {
                byte byte0 = m.getPresentationContextID(l1);
                for(int i2 = 0; i2 < n.getPresentationContexts(); i2++)
                {
                    byte byte1 = n.getPresentationContextID(i2);
                    if(byte1 != byte0)
                        continue;
                    if(n.getResult(i2) == 0)
                        auidentry[j1++] = n.getTransferSyntax(i2);
                    break;
                }

            }
        }

        UIDEntry auidentry1[] = new UIDEntry[j1];
        System.arraycopy(((Object) (auidentry)), 0, ((Object) (auidentry1)), 0, j1);
        return auidentry1;
    }

    public byte getPresentationContext(int i1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        if(uidentry.getType() != 4 && uidentry.getType() != 1)
            throw new IllegalValueException("first argument should be Meta SOP Class or SOP Class");
        int j1 = m.getPresentationContexts();
        for(int k1 = 0; k1 < j1; k1++)
        {
            UIDEntry uidentry1 = m.getAbstractSyntax(k1);
            if(uidentry1.getConstant() == i1)
            {
                byte byte0 = m.getPresentationContextID(k1);
                for(int l1 = 0; l1 < n.getPresentationContexts(); l1++)
                {
                    byte byte1 = n.getPresentationContextID(l1);
                    if(byte1 == byte0 && n.getResult(l1) == 0)
                        return byte0;
                }

            }
        }

        throw new IllegalValueException("no accepted presentation context with specified abstract syntax");
    }

    public void send(int i1, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException
    {
        byte byte0 = getPresentationContext(i1);
        sendInPresentationContext(byte0, dicomobject, dicomobject1);
    }

    /**
     * @deprecated Method send is deprecated
     */

    public void send(byte byte0, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException
    {
        sendInPresentationContext(byte0, dicomobject, dicomobject1);
    }

    public void sendInPresentationContext(byte byte0, DicomObject dicomobject, DicomObject dicomobject1)
        throws IOException, IllegalValueException, DicomException
    {
        UIDEntry uidentry = getTransferSyntax(byte0);
        if(uidentry == null)
            Debug.out.println("NULL");
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        y y1 = new y();
        PduOutputStream pduoutputstream = new PduOutputStream(k, i, byte0, (byte)1);
        dicomobject.write(((OutputStream) (pduoutputstream)), false, 8193, true);
        pduoutputstream.close();
        if(dicomobject1 != null)
        {
            PduOutputStream pduoutputstream1 = new PduOutputStream(k, i, byte0, (byte)0);
            dicomobject1.write(((OutputStream) (pduoutputstream1)), false, uidentry.getConstant(), f, e);
            pduoutputstream1.close();
        }
    }

    public UIDEntry getTransferSyntax(byte byte0)
        throws IllegalValueException
    {
        int i1 = -1;
        for(int j1 = 0; j1 < n.getPresentationContexts(); j1++)
            if(byte0 == n.getPresentationContextID(j1))
                i1 = j1;

        if(i1 == -1)
            throw new IllegalValueException("presentation context with specified id not present in Acknowledge");
        if(n.getResult(i1) != 0)
            throw new IllegalValueException("not an accepted presentation context");
        else
            return n.getTransferSyntax(i1);
    }

    private void a(byte abyte0[], int i1, int j1, byte byte0, byte byte1)
        throws IOException
    {
        int k1 = i1;
        BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(k, 8192);
        DataOutputStream dataoutputstream = new DataOutputStream(((OutputStream) (bufferedoutputstream)));
        int l1 = j1;
        int i2;
        if(i != 0)
            i2 = i - 6;
        else
            i2 = l1;
        for(; l1 > i2; l1 -= i2)
        {
            dataoutputstream.writeByte(4);
            dataoutputstream.writeByte(0);
            dataoutputstream.writeInt(i);
            dataoutputstream.writeInt(i - 4);
            dataoutputstream.writeByte(((int) (byte0)));
            dataoutputstream.writeByte(((int) (byte1)));
            dataoutputstream.write(abyte0, k1, i2);
            k1 += i2;
        }

        dataoutputstream.writeByte(4);
        dataoutputstream.writeByte(0);
        dataoutputstream.writeInt(l1 + 6);
        dataoutputstream.writeInt(l1 + 2);
        dataoutputstream.writeByte(((int) (byte0)));
        dataoutputstream.writeByte(byte1 + 2);
        dataoutputstream.write(abyte0, k1, l1);
        bufferedoutputstream.flush();
    }

    public int peek()
        throws IOException, IllegalValueException
    {
        int i1 = l.read();
        if(i1 == -1)
            throw new IOException("end of stream reached");
        l.unread(i1);
        if(i1 == 1)
            return 14;
        if(i1 == 2)
            return 15;
        if(i1 == 3)
            return 16;
        if(i1 == 4)
            return 10;
        if(i1 == 5)
            return 11;
        if(i1 == 6)
            return 13;
        if(i1 == 7)
            return 12;
        else
            throw new IllegalValueException("Unexpected byte read: " + i1);
    }

    private int a()
        throws IOException, IllegalValueException
    {
        DataInputStream datainputstream = new DataInputStream(((InputStream) (l)));
        DataOutputStream dataoutputstream = new DataOutputStream(k);
        byte byte1 = 4;
        do
        {
            int k1 = l.read();
            if(Debug.DEBUG > 3)
            {
                Debug.out.println("incoming PDU");
                Debug.out.println("PDU type: " + k1);
            }
            if(k1 != byte1)
                throw new IllegalValueException("first byte of PDATA-PDU not 0x04 but '" + k1 + "'");
            datainputstream.skipBytes(1);
            int j1 = datainputstream.readInt();
            if(Debug.DEBUG > 3)
                Debug.out.println("PDU length: " + j1);
            while(j1 > 0) 
            {
                int i1 = datainputstream.readInt();
                if(Debug.DEBUG > 3)
                    Debug.out.println("item length: " + i1);
                byte abyte0[] = new byte[i1 - 2];
                j1 = j1 - i1 - 4;
                v = ((int) (datainputstream.readByte()));
                if(Debug.DEBUG > 3)
                    Debug.out.println("presentation context id: " + v);
                byte byte0 = datainputstream.readByte();
                if(Debug.DEBUG > 3)
                    Debug.out.println("header value: " + byte0);
                datainputstream.readFully(abyte0);
                if(byte0 == 0)
                    p.write(abyte0, 0, abyte0.length);
                else
                if(byte0 == 1)
                    o.write(abyte0, 0, abyte0.length);
                else
                if(byte0 == 2)
                {
                    p.write(abyte0, 0, abyte0.length);
                    u = true;
                } else
                if(byte0 == 3)
                {
                    o.write(abyte0, 0, abyte0.length);
                    t = true;
                } else
                {
                    throw new IllegalValueException("illegal value for header of PDV item");
                }
            }
            if(t)
                return 8;
        } while(!u);
        return 9;
    }

    public int getCurrentPresentationContext()
    {
        return v;
    }

    public UIDEntry getCurrentAbstractSyntax()
        throws IllegalValueException
    {
        for(int i1 = 0; i1 < m.getPresentationContexts(); i1++)
            if(v == m.getPresentationContextID(i1))
                return m.getAbstractSyntax(i1);

        throw new IllegalValueException("no abstract syntax in request found for received presentation context id: " + v);
    }

    public Request receiveAssociateRequest()
        throws IOException, UnknownUIDException, IllegalValueException
    {
        q = new AssociateRequestPdu();
        q.read(((InputStream) (l)));
        i = q.getRequest().getMaxPduSize();
        m = q.getRequest();
        return m;
    }

    public Response receiveAssociateResponse()
        throws IOException, UnknownUIDException, IllegalValueException
    {
        if(Debug.DEBUG > 1)
            Debug.out.println("Waiting for AssociationRsp");
        int i1 = l.read();
        l.unread(i1);
        if(i1 == 2)
        {
            if(Debug.DEBUG > 1)
                Debug.out.println("ASSOCIATE_ACKNOWLEDGE detected");
            r = new AssociateAcknowledgePdu();
            r.read(((InputStream) (l)));
            i = r.getAcknowledge().getMaxPduSize();
            s = r.getPresentationContextResponseItems();
            n = r.getAcknowledge();
            return ((Response) (r.getAcknowledge()));
        }
        if(i1 == 3)
        {
            if(Debug.DEBUG > 1)
                Debug.out.println("ASSOCIATE_REJECT detected");
            AssociateRejectPdu associaterejectpdu = new AssociateRejectPdu();
            associaterejectpdu.read(((InputStream) (l)));
            return ((Response) (associaterejectpdu.getReject()));
        }
        if(i1 == 7)
        {
            if(Debug.DEBUG > 1)
                Debug.out.println("ABORT detected");
            AbortPdu abortpdu = new AbortPdu();
            abortpdu.read(((InputStream) (l)));
            return ((Response) (abortpdu.getAbort()));
        } else
        {
            throw new IllegalValueException("first byte of PDATA-PDU not 0x04 but '" + i1 + "'");
        }
    }

    public void sendAssociateRequest(Request request)
        throws IOException, IllegalValueException
    {
        m = request;
        m.a("1.2.826.0.1.3680043.2.60.0.1");
        m.b("softlink_jdt103");
        q = new AssociateRequestPdu(m);
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        DataOutputStream dataoutputstream = new DataOutputStream(((OutputStream) (bytearrayoutputstream)));
        q.write(dataoutputstream);
        k.write(bytearrayoutputstream.toByteArray());
    }

    public void sendReleaseRequest()
        throws IOException, IllegalValueException
    {
        DataOutputStream dataoutputstream = new DataOutputStream(k);
        k.write(5);
        k.write(0);
        dataoutputstream.writeInt(4);
        dataoutputstream.writeInt(0);
    }

    public void receiveReleaseRequest()
        throws IOException, IllegalValueException
    {
        DataInputStream datainputstream = new DataInputStream(((InputStream) (l)));
        int i1 = l.read();
        if(i1 != 5)
        {
            throw new IllegalValueException("wrong first byte for expected Release Request: " + i1);
        } else
        {
            l.read();
            datainputstream.readInt();
            datainputstream.readInt();
            return;
        }
    }

    public void receiveReleaseResponse()
        throws IOException, IllegalValueException
    {
        DataInputStream datainputstream = new DataInputStream(((InputStream) (l)));
        int i1 = l.read();
        if(i1 != 6)
        {
            throw new IllegalValueException("wrong first byte for expected Release Response: " + i1);
        } else
        {
            l.read();
            datainputstream.readInt();
            datainputstream.readInt();
            return;
        }
    }

    public void sendAbort(int i1, int j1)
        throws IOException
    {
        AbortPdu abortpdu = new AbortPdu(i1, j1);
        abortpdu.write(k);
    }

    public Abort receiveAbort()
        throws IllegalValueException, IOException
    {
        AbortPdu abortpdu = new AbortPdu();
        abortpdu.read(((InputStream) (l)));
        return abortpdu.getAbort();
    }

    public int getResultForAbstractSyntax(int i1)
        throws IllegalValueException
    {
        return ResponsePolicy.getResultForAbstractSyntax(m, n, i1);
    }

}
