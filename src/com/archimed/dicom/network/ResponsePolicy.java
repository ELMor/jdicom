// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;

// Referenced classes of package com.archimed.dicom.network:
//            Request, Reject, Acknowledge, Response

public class ResponsePolicy
{

    public ResponsePolicy()
    {
    }

    public static Response prepareResponse(Request request, String s, String as[], int ai[], int i, boolean flag)
        throws IllegalValueException
    {
        if(request.isPrivateApplicationContext())
            return ((Response) (new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER, Reject.USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED)));
        if(s != null && !request.getCalledTitle().equals(((Object) (s))))
            return ((Response) (new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER, Reject.USER_CALLED_AETITLE_NOT_RECOGNIZED)));
        boolean flag1 = true;
        if(as != null)
        {
            for(int j = 0; j < as.length; j++)
                if(request.getCallingTitle().equals(((Object) (as[j]))))
                    flag1 = false;

            if(flag1)
                return ((Response) (new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER, Reject.USER_CALLING_AETITLE_NOT_RECOGNIZED)));
        }
        Acknowledge acknowledge = new Acknowledge();
        acknowledge.setCalledTitle(request.getCalledTitle());
        acknowledge.setCallingTitle(request.getCallingTitle());
label0:
        for(int k = 0; k < request.getPresentationContexts(); k++)
        {
            UIDEntry uidentry = request.getAbstractSyntax(k);
            for(int l = 0; l < ai.length; l++)
            {
                if(!uidentry.equals(((Object) (UID.getUIDEntry(ai[l])))))
                    continue;
                for(int j1 = 0; j1 < request.getTransferSyntaxes(k); j1++)
                {
                    UIDEntry uidentry1 = request.getTransferSyntax(k, j1);
                    if(uidentry1.getConstant() != 8193)
                        continue;
                    acknowledge.addPresentationContext(request.getPresentationContextID(k), 0, i);
                    continue label0;
                }

                acknowledge.addPresentationContext(request.getPresentationContextID(k), 4, i);
                continue label0;
            }

            acknowledge.addPresentationContext(request.getPresentationContextID(k), 3, i);
        }

        if(flag)
        {
            boolean flag2 = true;
            for(int i1 = 0; i1 < acknowledge.getPresentationContexts(); i1++)
                if(acknowledge.getResult(i1) == 0)
                    flag2 = false;

            if(flag2)
                return ((Response) (new Reject(Reject.REJECTED_PERMANENT, Reject.DICOM_UL_SERVICE_USER, Reject.USER_NO_REASON_GIVEN)));
        }
        return ((Response) (acknowledge));
    }

    public static int getResultForAbstractSyntax(Request request, Acknowledge acknowledge, int i)
        throws IllegalValueException
    {
        int j = request.getPresentationContexts();
        for(int k = 0; k < j; k++)
        {
            UIDEntry uidentry = request.getAbstractSyntax(k);
            if(uidentry.getConstant() == i)
            {
                byte byte0 = request.getPresentationContextID(k);
                for(int l = 0; l < acknowledge.getPresentationContexts(); l++)
                {
                    byte byte1 = acknowledge.getPresentationContextID(l);
                    if(byte0 == byte1)
                        return acknowledge.getResult(l);
                }

                throw new IllegalValueException("no matching presentation context id in request and acknowledge for specified abstract syntax");
            }
        }

        throw new IllegalValueException("abstract syntax not in request");
    }
}
