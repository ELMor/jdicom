// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.n;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            Response, ApplicationContextItem

public class Acknowledge extends Response
{

    public static final int NO_SUPPORT_SCU_ROLE = 0;
    public static final int SUPPORT_SCU_ROLE = 1;
    public static final int NO_SUPPORT_SCP_ROLE = 0;
    public static final int SUPPORT_SCP_ROLE = 1;
    public static final int DEFAULT = -1;
    public static final int ACCEPTANCE = 0;
    public static final int USER_REJECTION = 1;
    public static final int NO_REASON = 2;
    public static final int ABSTRACT_SYNTAX_NOT_SUPPORTED = 3;
    public static final int TRANSFER_SYNTAXES_NOT_SUPPORTED = 4;
    private String a;
    private String b;
    private String c;
    private Vector d;
    private Vector e;
    private Vector f;
    private String g;
    private String h;
    private Vector i;
    private int j;
    private int k;
    private int l;
    private Vector m;
    private Vector n;
    private Vector o;

    public Acknowledge()
    {
        j = 32768;
        k = 1;
        l = 1;
        m = new Vector();
        d = new Vector();
        e = new Vector();
        f = new Vector();
        i = new Vector();
        n = new Vector();
        o = new Vector();
        a("1.2.826.0.1.3680043.2.60.0.1");
        b("softlink_jdt103");
    }

    public String getCallingTitle()
    {
        return b;
    }

    public String getCalledTitle()
    {
        return a;
    }

    public String getApplicationContextUid()
    {
        return c;
    }

    public boolean isPrivateApplicationContext()
    {
        return !ApplicationContextItem.isPrivate(c);
    }

    public void setApplicationContextUid(String s)
    {
        c = s;
    }

    public void setCalledTitle(String s)
    {
        a = s;
    }

    public void setCallingTitle(String s)
    {
        b = s;
    }

    public String getImplementationClassUID()
    {
        return g;
    }

    public String getImplementationVersionName()
    {
        return h;
    }

    void a(String s)
    {
        g = s;
    }

    void b(String s)
    {
        h = s;
    }

    public int getMaxPduSize()
    {
        return j;
    }

    public void setMaxPduSize(int i1)
    {
        j = i1;
    }

    public void setMaxOperationsInvoked(int i1)
    {
        k = i1;
    }

    public int getMaxOperationsInvoked()
    {
        return k;
    }

    public void setMaxOperationsPerformed(int i1)
    {
        l = i1;
    }

    public int getMaxOperationsPerformed()
    {
        return l;
    }

    public void setScuScpRoleSelection(int i1, int j1, int k1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        int l1 = m.indexOf(((Object) (uidentry)));
        if(l1 != -1)
        {
            n.setElementAt(((Object) (new Integer(j1))), l1);
            o.setElementAt(((Object) (new Integer(k1))), l1);
        } else
        {
            m.addElement(((Object) (uidentry)));
            n.addElement(((Object) (new Integer(j1))));
            o.addElement(((Object) (new Integer(k1))));
        }
    }

    public final int getScuRole(int i1)
        throws IllegalValueException
    {
        return getScuRole(UID.getUIDEntry(i1));
    }

    public int getScuRole(UIDEntry uidentry)
    {
        int i1 = m.indexOf(((Object) (uidentry)));
        if(i1 == -1)
            return -1;
        else
            return ((Integer)n.elementAt(i1)).intValue();
    }

    public int getScpRole(int i1)
        throws IllegalValueException
    {
        return getScpRole(UID.getUIDEntry(i1));
    }

    public int getScpRole(UIDEntry uidentry)
    {
        int i1 = m.indexOf(((Object) (uidentry)));
        if(i1 == -1)
            return -1;
        else
            return ((Integer)o.elementAt(i1)).intValue();
    }

    UIDEntry a(int i1)
    {
        return (UIDEntry)m.elementAt(i1);
    }

    int a()
    {
        return m.size();
    }

    public byte getPresentationContextID(int i1)
    {
        return ((Byte)f.elementAt(i1)).byteValue();
    }

    public int getResult(int i1)
    {
        return ((Integer)e.elementAt(i1)).intValue();
    }

    public int getPresentationContexts()
    {
        return f.size();
    }

    public UIDEntry getTransferSyntax(int i1)
    {
        return (UIDEntry)d.elementAt(i1);
    }

    public void addPresentationContext(byte byte0, int i1, int j1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(j1);
        f.addElement(((Object) (new Byte(byte0))));
        e.addElement(((Object) (new Integer(i1))));
        d.addElement(((Object) (uidentry)));
    }

    public String toString()
    {
        String s = "*** acknowledge ***\n";
        s = s + "max pdu size: " + j + "\n";
        s = s + "max operation invoked: " + k + "\n";
        s = s + "max operation performed: " + l + "\n";
        s = s + "implementation class UID: " + g + "\n";
        s = s + "implementation version name: " + h + "\n";
        s = s + com.archimed.tool.n.fstr("abstract syntax", 32) + com.archimed.tool.n.fstr("scu", 4) + com.archimed.tool.n.fstr("scp", 4) + "\n";
        for(int i1 = 0; i1 < a(); i1++)
        {
            UIDEntry uidentry = a(i1);
            s = s + com.archimed.tool.n.fstr(uidentry.getValue(), 32) + com.archimed.tool.n.fstr("" + getScuRole(uidentry), 4) + com.archimed.tool.n.fstr("" + getScpRole(uidentry), 4) + "\n";
        }

        s = s + com.archimed.tool.n.fstr("nr", 5) + com.archimed.tool.n.fstr("pcid", 6) + com.archimed.tool.n.fstr("result", 32) + com.archimed.tool.n.fstr("transfer syntax", 20) + "\n";
        for(int j1 = 0; j1 < getPresentationContexts(); j1++)
            if(getResult(j1) == 0)
                s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr("accepted", 32) + com.archimed.tool.n.fstr("" + getTransferSyntax(j1), 20) + "\n";
            else
            if(getResult(j1) == 3)
                s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr("unsupported abstract syntax", 32) + "\n";
            else
            if(getResult(j1) == 4)
                s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr("unsupported transfer syntaxes", 32) + "\n";
            else
            if(getResult(j1) == 1)
                s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr("user rejection", 32) + "\n";
            else
            if(getResult(j1) == 2)
                s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr("no reason", 32) + "\n";

        s = s + "*******************";
        return s;
    }

    public int indexOf(byte byte0)
    {
        return f.indexOf(((Object) (new Byte(byte0))));
    }
}
