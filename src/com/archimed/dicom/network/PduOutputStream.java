// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class PduOutputStream extends FilterOutputStream
{

    public static final byte COMMAND = 1;
    public static final byte DATA = 0;
    private int a;
    private DataOutputStream b;
    private byte c[];
    private int d;
    private byte e;
    private byte f;

    public PduOutputStream(OutputStream outputstream, int i, byte byte0, byte byte1)
    {
        super(outputstream);
        b = new DataOutputStream(outputstream);
        if(i == 0)
            i = 0x186a0;
        else
            a = i;
        e = byte1;
        f = byte0;
        c = new byte[i - 6];
    }

    public synchronized void write(int i)
        throws IOException
    {
        if(d >= c.length)
            a();
        c[d++] = (byte)i;
    }

    public synchronized void write(byte abyte0[])
        throws IOException
    {
        write(abyte0, 0, abyte0.length);
    }

    public synchronized void write(byte abyte0[], int i, int j)
        throws IOException
    {
        if(d >= c.length)
            a();
        if(j <= c.length - d)
        {
            System.arraycopy(((Object) (abyte0)), i, ((Object) (c)), d, j);
            d += j;
        } else
        {
            System.arraycopy(((Object) (abyte0)), i, ((Object) (c)), d, c.length - d);
            i += c.length - d;
            j -= c.length - d;
            d += c.length - d;
            a();
            int k = j / c.length;
            for(int l = 0; l < k; l++)
            {
                System.arraycopy(((Object) (abyte0)), i, ((Object) (c)), 0, c.length);
                i += c.length;
                j -= c.length;
                d += c.length;
                a();
            }

            if(j > 0)
            {
                System.arraycopy(((Object) (abyte0)), i, ((Object) (c)), 0, j);
                d += j;
            }
        }
    }

    private void a()
        throws IOException
    {
        b.writeByte(4);
        b.writeByte(0);
        b.writeInt(d + 6);
        b.writeInt(d + 2);
        b.writeByte(((int) (f)));
        b.writeByte(((int) (e)));
        super.out.write(c, 0, d);
        d = 0;
    }

    public void close()
        throws IOException
    {
        if(d > 0)
        {
            b.writeByte(4);
            b.writeByte(0);
            b.writeInt(d + 6);
            b.writeInt(d + 2);
            b.writeByte(((int) (f)));
            b.writeByte(e + 2);
            super.out.write(c, 0, d);
        } else
        {
            b.writeByte(4);
            b.writeByte(0);
            b.writeInt(6);
            b.writeInt(2);
            b.writeByte(((int) (f)));
            b.writeByte(e + 2);
        }
    }
}
