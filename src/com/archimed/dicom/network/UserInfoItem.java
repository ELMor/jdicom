// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PushbackInputStream;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem, MaximumLengthSubItem, ImplementationClassUIDSubItem, AsynchronousOperationsWindowSubItem, 
//            ScuScpRoleSubItem, ImplementationVersionNameSubItem, ExtendedNegotiationSubItem

class UserInfoItem
{

    private int a;
    private Vector b;
    private int c;

    public UserInfoItem()
    {
        a = 80;
        c = 0;
        b = new Vector();
    }

    public void addSubItem(SubItem subitem)
    {
        b.addElement(((Object) (subitem)));
    }

    public Vector getSubItems()
    {
        return b;
    }

    public int getLength()
    {
        int i = 4;
        for(int j = 0; j < b.size(); j++)
            i += ((SubItem)b.elementAt(j)).getLength();

        i += c;
        return i;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(getLength() - 4);
        for(int i = 0; i < b.size(); i++)
            ((SubItem)b.elementAt(i)).write(dataoutputstream);

    }

    public int read(PushbackInputStream pushbackinputstream)
        throws IOException, IllegalValueException, UnknownUIDException
    {
        DataInputStream datainputstream = new DataInputStream(((java.io.InputStream) (pushbackinputstream)));
        int l1 = ((FilterInputStream) (datainputstream)).read();
        if(l1 != a)
            throw new IllegalValueException("itemtype field of received User Info Item  not " + a);
        ((FilterInputStream) (datainputstream)).read();
        int j2 = ((int) (datainputstream.readChar()));
        if(Debug.DEBUG > 3)
            Debug.out.println("UserInfoItem length: " + j2);
        int i = j2;
        while(j2 > 0) 
        {
            int i2 = ((FilterInputStream) (datainputstream)).read();
            pushbackinputstream.unread(i2);
            if(i2 == 81)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect MaximumLengthSubItem");
                MaximumLengthSubItem maximumlengthsubitem = new MaximumLengthSubItem();
                int j = maximumlengthsubitem.read(datainputstream);
                addSubItem(((SubItem) (maximumlengthsubitem)));
                j2 -= j;
            } else
            if(i2 == 82)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect ImplementationClassUIDSubItem");
                ImplementationClassUIDSubItem implementationclassuidsubitem = new ImplementationClassUIDSubItem();
                int k = implementationclassuidsubitem.read(datainputstream);
                addSubItem(((SubItem) (implementationclassuidsubitem)));
                j2 -= k;
            } else
            if(i2 == 83)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect AsynchronousOperationsWindowSubItem");
                AsynchronousOperationsWindowSubItem asynchronousoperationswindowsubitem = new AsynchronousOperationsWindowSubItem();
                int l = asynchronousoperationswindowsubitem.read(datainputstream);
                addSubItem(((SubItem) (asynchronousoperationswindowsubitem)));
                j2 -= l;
            } else
            if(i2 == 84)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect ScuScpRoleSubItem");
                ScuScpRoleSubItem scuscprolesubitem = new ScuScpRoleSubItem();
                int i1 = scuscprolesubitem.read(datainputstream);
                addSubItem(((SubItem) (scuscprolesubitem)));
                j2 -= i1;
            } else
            if(i2 == 85)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect ImplementationVersionNameSubItem");
                ImplementationVersionNameSubItem implementationversionnamesubitem = new ImplementationVersionNameSubItem();
                int j1 = implementationversionnamesubitem.read(datainputstream);
                addSubItem(((SubItem) (implementationversionnamesubitem)));
                j2 -= j1;
            } else
            if(i2 == 86)
            {
                if(Debug.DEBUG > 3)
                    Debug.out.println("Detect ExtendedNegotiationSubItem");
                ExtendedNegotiationSubItem extendednegotiationsubitem = new ExtendedNegotiationSubItem();
                int k1 = extendednegotiationsubitem.read(datainputstream);
                addSubItem(((SubItem) (extendednegotiationsubitem)));
                j2 -= k1;
            } else
            {
                ((FilterInputStream) (datainputstream)).read();
                ((FilterInputStream) (datainputstream)).read();
                int k2 = ((int) (datainputstream.readChar()));
                ((FilterInputStream) (datainputstream)).skip(k2);
                j2 = j2 - k2 - 4;
                c += k2 + 4;
            }
            if(Debug.DEBUG > 3)
                Debug.out.println("UserInfoItem remaining length: " + j2);
        }
        return 4 + i;
    }
}
