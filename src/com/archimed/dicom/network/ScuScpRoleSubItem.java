// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem

class ScuScpRoleSubItem extends SubItem
{

    private int a;
    private int b;
    private int c;
    private UIDEntry d;

    public ScuScpRoleSubItem()
    {
        a = 84;
    }

    public ScuScpRoleSubItem(UIDEntry uidentry, int i, int j)
    {
        a = 84;
        d = uidentry;
        b = i;
        c = j;
    }

    public int getLength()
    {
        int i = 8;
        if(d != null)
            i += d.getValue().length();
        return i;
    }

    public UIDEntry getAbstractSyntax()
    {
        return d;
    }

    public int getScuRole()
    {
        return b;
    }

    public int getScpRole()
    {
        return c;
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(d.getValue().length() + 4);
        dataoutputstream.writeChar(d.getValue().length());
        ((FilterOutputStream) (dataoutputstream)).write(d.getValue().getBytes());
        dataoutputstream.write(b);
        dataoutputstream.write(c);
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
            throw new IllegalValueException("itemtype field of received ScuScpRoleSubItem  not " + a);
        ((FilterInputStream) (datainputstream)).read();
        char c1 = datainputstream.readChar();
        char c2 = datainputstream.readChar();
        byte abyte0[] = new byte[c2];
        datainputstream.readFully(abyte0);
        abyte0 = n.trimZeros(abyte0);
        String s = new String(abyte0);
        try
        {
            d = UID.getUIDEntry(s);
        }
        catch(UnknownUIDException unknownuidexception)
        {
            d = new UIDEntry(0, s, "unknown uid", "??", 1);
        }
        b = ((FilterInputStream) (datainputstream)).read();
        c = ((FilterInputStream) (datainputstream)).read();
        return 6 + c2 + 2;
    }
}
