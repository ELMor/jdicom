// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

// Referenced classes of package com.archimed.dicom.network:
//            SubItem

class ImplementationVersionNameSubItem extends SubItem
{

    private int a;
    private String b;

    public ImplementationVersionNameSubItem()
    {
        a = 85;
    }

    public ImplementationVersionNameSubItem(String s)
    {
        a = 85;
        b = s;
    }

    public String getImplementationVersionName()
    {
        return b;
    }

    public int getLength()
    {
        return 4 + b.length();
    }

    public void write(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(a);
        dataoutputstream.write(0);
        dataoutputstream.writeChar(b.length());
        ((FilterOutputStream) (dataoutputstream)).write(b.getBytes());
    }

    public int read(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != a)
        {
            throw new IllegalValueException("itemtype field of received Implementation Version Name Item  not " + a);
        } else
        {
            ((FilterInputStream) (datainputstream)).read();
            char c = datainputstream.readChar();
            byte abyte0[] = new byte[c];
            datainputstream.readFully(abyte0);
            abyte0 = n.trimZeros(abyte0);
            b = new String(abyte0);
            return 4 + c;
        }
    }
}
