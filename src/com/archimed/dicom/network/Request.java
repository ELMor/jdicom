// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.n;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.network:
//            ApplicationContextItem

public class Request
{

    public static final int NO_SUPPORT_SCU_ROLE = 0;
    public static final int SUPPORT_SCU_ROLE = 1;
    public static final int NO_SUPPORT_SCP_ROLE = 0;
    public static final int SUPPORT_SCP_ROLE = 1;
    public static final int DEFAULT = -1;
    private String a;
    private String b;
    private String c;
    private Vector d;
    private Vector e;
    private Vector f;
    private String g;
    private String h;
    private Vector i;
    private int j;
    private int k;
    private int l;
    private Vector m;
    private Vector n;
    private Vector o;

    public Request()
    {
        j = 32768;
        k = 1;
        l = 1;
        e = new Vector();
        f = new Vector();
        d = new Vector();
        i = new Vector();
        k = 1;
        l = 1;
        m = new Vector();
        n = new Vector();
        o = new Vector();
        a("1.2.826.0.1.3680043.2.60.0.1");
        b("softlink_jdt103");
    }

    public String getCallingTitle()
    {
        return c;
    }

    public String getApplicationContextUid()
    {
        return a;
    }

    public boolean isPrivateApplicationContext()
    {
        return !ApplicationContextItem.isPrivate(a);
    }

    public void setApplicationContextUid(String s)
    {
        a = s;
    }

    public String getCalledTitle()
    {
        return b;
    }

    public void setCalledTitle(String s)
    {
        b = s;
    }

    public void setCallingTitle(String s)
    {
        c = s;
    }

    public String getImplementationClassUID()
    {
        return g;
    }

    public String getImplementationVersionName()
    {
        return h;
    }

    void a(String s)
    {
        g = s;
    }

    void b(String s)
    {
        h = s;
    }

    public int getMaxPduSize()
    {
        return j;
    }

    public void setMaxPduSize(int i1)
    {
        j = i1;
    }

    public void setMaxOperationsInvoked(int i1)
    {
        k = i1;
    }

    public int getMaxOperationsInvoked()
    {
        return k;
    }

    public void setMaxOperationsPerformed(int i1)
    {
        l = i1;
    }

    public int getMaxOperationsPerformed()
    {
        return l;
    }

    public final int getScuRole(int i1)
        throws IllegalValueException
    {
        return getScuRole(UID.getUIDEntry(i1));
    }

    public int getScuRole(UIDEntry uidentry)
    {
        int i1 = m.indexOf(((Object) (uidentry)));
        if(i1 == -1)
            return -1;
        else
            return ((Integer)n.elementAt(i1)).intValue();
    }

    public UIDEntry getAbstractSyntaxForRole(int i1)
    {
        return (UIDEntry)m.elementAt(i1);
    }

    public int getRoles()
    {
        return m.size();
    }

    public int getScpRole(int i1)
        throws IllegalValueException
    {
        return getScpRole(UID.getUIDEntry(i1));
    }

    public int getScpRole(UIDEntry uidentry)
    {
        int i1 = m.indexOf(((Object) (uidentry)));
        if(i1 == -1)
            return -1;
        else
            return ((Integer)o.elementAt(i1)).intValue();
    }

    public byte getPresentationContextID(int i1)
    {
        return ((Byte)d.elementAt(i1)).byteValue();
    }

    public int getPresentationContexts()
    {
        return d.size();
    }

    public UIDEntry getAbstractSyntax(int i1)
    {
        return (UIDEntry)e.elementAt(i1);
    }

    public int getTransferSyntaxes(int i1)
    {
        return ((Vector)f.elementAt(i1)).size();
    }

    public UIDEntry getTransferSyntax(int i1, int j1)
    {
        return (UIDEntry)((Vector)f.elementAt(i1)).elementAt(j1);
    }

    public byte[] getExtendedNegotiationData(int i1)
    {
        return (byte[])i.elementAt(i1);
    }

    public byte addPresentationContext(int i1, int ai[])
        throws IllegalValueException
    {
        byte byte0 = (byte)(2 * e.size() + 1);
        a(byte0, i1, ai);
        return byte0;
    }

    void a(byte byte0, int i1, int ai[])
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        UIDEntry auidentry[] = new UIDEntry[ai.length];
        for(int j1 = 0; j1 < ai.length; j1++)
            auidentry[j1] = UID.getUIDEntry(ai[j1]);

        a(byte0, uidentry, auidentry);
    }

    void a(byte byte0, UIDEntry uidentry, UIDEntry auidentry[])
    {
        e.addElement(((Object) (uidentry)));
        m.addElement(((Object) (uidentry)));
        n.addElement(((Object) (new Integer(-1))));
        o.addElement(((Object) (new Integer(-1))));
        i.addElement(((Object) (null)));
        d.addElement(((Object) (new Byte(byte0))));
        Vector vector = new Vector();
        for(int i1 = 0; i1 < auidentry.length; i1++)
            vector.addElement(((Object) (auidentry[i1])));

        f.addElement(((Object) (vector)));
    }

    void a(UIDEntry uidentry, int i1, int j1)
    {
        int k1 = e.indexOf(((Object) (uidentry)));
        if(k1 != -1)
        {
            n.setElementAt(((Object) (new Integer(i1))), k1);
            o.setElementAt(((Object) (new Integer(j1))), k1);
        }
    }

    public void setExtendedNegotiationData(int i1, byte abyte0[])
    {
        i.setElementAt(((Object) (abyte0)), i1);
    }

    public void setScuScpRoleSelection(int i1, int j1, int k1)
        throws IllegalValueException
    {
        UIDEntry uidentry = UID.getUIDEntry(i1);
        int l1 = m.indexOf(((Object) (uidentry)));
        if(l1 != -1)
        {
            n.setElementAt(((Object) (new Integer(j1))), l1);
            o.setElementAt(((Object) (new Integer(k1))), l1);
        } else
        {
            m.addElement(((Object) (uidentry)));
            n.addElement(((Object) (new Integer(j1))));
            o.addElement(((Object) (new Integer(k1))));
        }
    }

    public String toString()
    {
        String s = "*** request ***\n";
        s = s + "application context UID: " + a + "\n";
        s = s + "called title: " + b + "\n";
        s = s + "calling title: " + c + "\n";
        s = s + "max pdu size: " + j + "\n";
        s = s + "max operation invoked: " + k + "\n";
        s = s + "max operation performed: " + l + "\n";
        s = s + "implementation class UID: " + g + "\n";
        s = s + "implementation version Name: " + h + "\n";
        s = s + com.archimed.tool.n.fstr("abstract syntax", 32) + com.archimed.tool.n.fstr("scu", 4) + com.archimed.tool.n.fstr("scp", 4) + "\n";
        for(int i1 = 0; i1 < getRoles(); i1++)
        {
            UIDEntry uidentry = getAbstractSyntaxForRole(i1);
            s = s + com.archimed.tool.n.fstr(uidentry.getValue(), 32) + com.archimed.tool.n.fstr("" + getScuRole(uidentry), 4) + com.archimed.tool.n.fstr("" + getScpRole(uidentry), 4) + "\n";
        }

        s = s + com.archimed.tool.n.fstr("nr", 5) + com.archimed.tool.n.fstr("abstract syntax", 32) + com.archimed.tool.n.fstr("pcid", 6) + com.archimed.tool.n.fstr("description", 55) + "\n";
        for(int j1 = 0; j1 < getPresentationContexts(); j1++)
        {
            UIDEntry uidentry1 = getAbstractSyntax(j1);
            s = s + com.archimed.tool.n.fstr(j1 + "", 5) + com.archimed.tool.n.fstr(uidentry1.getValue(), 32) + com.archimed.tool.n.fstr("" + getPresentationContextID(j1), 6) + com.archimed.tool.n.fstr(uidentry1.getName(), 55) + "\n";
            if(Debug.DEBUG > 1)
            {
                int k1 = getTransferSyntaxes(j1);
                for(int l1 = 0; l1 < k1; l1++)
                {
                    UIDEntry uidentry2 = getTransferSyntax(j1, l1);
                    s = s + com.archimed.tool.n.fstr("  ts-" + l1, 11) + com.archimed.tool.n.fstr(uidentry2.getValue(), 32) + com.archimed.tool.n.fstr(uidentry2.getName(), 55) + "\n";
                }

            }
        }

        s = s + "***************";
        return s;
    }

    public int indexOf(byte byte0)
    {
        return d.indexOf(((Object) (new Byte(byte0))));
    }
}
