// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.network;

import com.archimed.dicom.Debug;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.n;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.PrintStream;

class TransferSyntaxItem
{

    private byte a;
    private UIDEntry b;

    public TransferSyntaxItem()
    {
        a = 64;
    }

    public TransferSyntaxItem(UIDEntry uidentry)
    {
        a = 64;
        b = uidentry;
    }

    public UIDEntry getTransferSyntax()
    {
        return b;
    }

    public int getLength()
    {
        return b.getValue().length() + 4;
    }

    void a(DataOutputStream dataoutputstream)
        throws IOException
    {
        dataoutputstream.write(((int) (a)));
        dataoutputstream.write(0);
        dataoutputstream.writeChar(b.getValue().length());
        ((FilterOutputStream) (dataoutputstream)).write(b.getValue().getBytes());
    }

    int a(DataInputStream datainputstream)
        throws IOException, IllegalValueException
    {
        int i = ((FilterInputStream) (datainputstream)).read();
        if(i != 64)
            throw new IllegalValueException("itemtype field of received Transfer Syntax Item not " + a);
        ((FilterInputStream) (datainputstream)).skip(1L);
        char c = datainputstream.readChar();
        if(Debug.DEBUG > 3)
            Debug.out.println("TransferSyntaxItem length: " + (int)c);
        byte abyte0[] = new byte[c];
        datainputstream.readFully(abyte0);
        abyte0 = n.trimZeros(abyte0);
        String s = new String(abyte0);
        try
        {
            b = UID.getUIDEntry(s);
        }
        catch(UnknownUIDException unknownuidexception)
        {
            b = new UIDEntry(0, s, "unknown uid", "??", 3);
        }
        return 4 + c;
    }
}
