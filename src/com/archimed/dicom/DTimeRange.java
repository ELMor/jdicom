// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;

// Referenced classes of package com.archimed.dicom:
//            DTime

public class DTimeRange
{

    DTime a;
    DTime b;

    public DTimeRange(DTime dtime, DTime dtime1)
    {
        if(dtime != null)
            a = dtime;
        else
            a = new DTime();
        if(dtime1 != null)
            b = dtime1;
        else
            b = new DTime();
    }

    public DTimeRange(String s)
        throws NumberFormatException
    {
        int i = s.indexOf('-');
        if(i == -1)
        {
            throw new NumberFormatException(s + " cannot parse this string into DTimeRange");
        } else
        {
            a = new DTime(s.substring(0, i));
            b = new DTime(s.substring(i + 1));
            return;
        }
    }

    public String toString()
    {
        return a.toString() + " - " + b.toString();
    }

    public String toString(DateFormat dateformat)
    {
        return a.toString(dateformat) + " - " + b.toString(dateformat);
    }

    public String toDICOMString()
    {
        return a.toDICOMString() + "-" + b.toDICOMString();
    }

    public DTime getTime1()
    {
        if(!a.isEmpty())
            return a;
        else
            return null;
    }

    public DTime getTime2()
    {
        if(!b.isEmpty())
            return b;
        else
            return null;
    }
}
