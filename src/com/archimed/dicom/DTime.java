// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DTime
{

    private Calendar a;
    private boolean b;

    public DTime()
    {
        b = false;
        b = true;
    }

    public DTime(String s)
        throws NumberFormatException
    {
        b = false;
        s = s.trim();
        int i = s.length();
        if(i == 0)
        {
            b = true;
            return;
        }
        int k;
        int l;
        int i1;
        int j = k = l = i1 = 0;
        int j1 = ((int) (s.indexOf(':') == -1 ? 0 : 1));
        int k1 = 2 + j1;
        if(i < k1 + 2)
            throw new NumberFormatException("Cannot parse string into DTime");
        j = Integer.parseInt(s.substring(0, 2));
        k = Integer.parseInt(s.substring(k1, k1 + 2));
        k1 += 2 + j1;
        if(k1 < i)
        {
            if(i < k1 + 2)
                throw new NumberFormatException("Cannot parse string into DTime");
            l = Integer.parseInt(s.substring(k1, k1 + 2));
            if((k1 += 3) < i)
            {
                if(s.charAt(k1 - 1) != '.')
                    throw new NumberFormatException("Cannot parse string into DTime");
                i1 = (int)(Float.parseFloat(s.substring(k1 - 1)) * 1000F);
            }
        }
        a = ((Calendar) (new GregorianCalendar()));
        a.set(11, j);
        a.set(12, k);
        a.set(13, l);
        a.set(14, i1);
    }

    public String toString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("HH:mm:ss.SSS");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public String toString(DateFormat dateformat)
    {
        if(b)
            return "";
        else
            return dateformat.format(a.getTime());
    }

    public String toDICOMString()
    {
        if(b)
        {
            return "";
        } else
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("HHmmss.SSS");
            return ((DateFormat) (simpledateformat)).format(a.getTime());
        }
    }

    public boolean isEmpty()
    {
        return b;
    }
}
