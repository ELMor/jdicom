// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.NoSuchElementException;

// Referenced classes of package com.archimed.dicom:
//            IntHashtableEntry

class IntHashtableEnumerator
    implements Enumeration
{

    boolean a;
    int b;
    IntHashtableEntry c[];
    IntHashtableEntry d;

    IntHashtableEnumerator(IntHashtableEntry ainthashtableentry[], boolean flag)
    {
        c = ainthashtableentry;
        a = flag;
        b = ainthashtableentry.length;
    }

    public boolean hasMoreElements()
    {
        if(d != null)
            return true;
        while(b-- > 0) 
            if((d = c[b]) != null)
                return true;
        return false;
    }

    public Object nextElement()
    {
        if(d == null)
            while(b-- > 0 && (d = c[b]) == null) ;
        if(d != null)
        {
            IntHashtableEntry inthashtableentry = d;
            d = inthashtableentry.d;
            return a ? new Integer(inthashtableentry.b) : inthashtableentry.c;
        } else
        {
            throw new NoSuchElementException("IntHashtableEnumerator");
        }
    }
}
