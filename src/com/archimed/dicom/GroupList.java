// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            IntHashtable, TagValue, VR, b, 
//            DicomUtils, DicomObject

public class GroupList
{

    private IntHashtable a;

    public GroupList()
    {
        a = new IntHashtable();
    }

    void a(VR vr)
    {
        if(vr == null)
            return;
        if(((TagValue) (vr)).element == 0)
            return;
        IntHashtable inthashtable = (IntHashtable)a.get(((TagValue) (vr)).group);
        if(inthashtable == null)
        {
            inthashtable = new IntHashtable();
            a.put(((TagValue) (vr)).group, ((Object) (inthashtable)));
        }
        inthashtable.put(((TagValue) (vr)).element, ((Object) (vr)));
    }

    public void clear()
    {
        IntHashtable inthashtable;
        for(Enumeration enumeration = a.elements(); enumeration.hasMoreElements(); inthashtable.clear())
            inthashtable = (IntHashtable)enumeration.nextElement();

        a.clear();
    }

    VR a(int i, int j)
    {
        IntHashtable inthashtable = (IntHashtable)a.get(i);
        if(inthashtable == (IntHashtable)null)
        {
            return null;
        } else
        {
            VR vr = (VR)inthashtable.get(j);
            return vr;
        }
    }

    VR b(int i, int j)
    {
        IntHashtable inthashtable = (IntHashtable)a.get(i);
        if(inthashtable == null)
        {
            return null;
        } else
        {
            VR vr = (VR)inthashtable.remove(j);
            return vr;
        }
    }

    public boolean isEmpty()
    {
        return a.isEmpty();
    }

    private void a(int i, boolean flag)
    {
        for(Enumeration enumeration1 = a.keys(); enumeration1.hasMoreElements();)
        {
            Integer integer = (Integer)enumeration1.nextElement();
            Enumeration enumeration = ((IntHashtable)a.get(((Object) (integer)))).elements();
            int j;
            if(integer.intValue() == 2)
                j = 8194;
            else
                j = i;
            while(enumeration.hasMoreElements()) 
            {
                VR vr = (VR)enumeration.nextElement();
                vr.dataLen = com.archimed.dicom.b.a(((TagValue) (vr)).val, vr.dcm_type, j, flag);
                vr.headerLen = com.archimed.dicom.b.b(((TagValue) (vr)).val, vr.dcm_type, j, flag);
            }
        }

    }

    public Enumeration enumerateVRs(boolean flag, boolean flag1)
    {
        if(flag && !flag1)
            throw new IllegalArgumentException();
        else
            return a(flag, 8193, true, flag1);
    }

    Enumeration a(boolean flag, int i, boolean flag1, boolean flag2)
    {
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        if(flag || flag2)
            a(i, flag1);
        for(Enumeration enumeration = a.keys(); enumeration.hasMoreElements(); vector2.addElement(enumeration.nextElement()));
        if(flag2)
            vector2 = DicomUtils.bubbleSort(vector2);
        for(int i1 = 0; i1 < vector2.size(); i1++)
        {
            Vector vector1 = new Vector();
            int l = ((Integer)vector2.elementAt(i1)).intValue();
            IntHashtable inthashtable = (IntHashtable)a.get(l);
            if(flag)
            {
                int j;
                if(l == 2)
                    j = 8194;
                else
                    j = i;
                int k = 0;
                for(Enumeration enumeration1 = inthashtable.elements(); enumeration1.hasMoreElements();)
                {
                    VR vr = (VR)enumeration1.nextElement();
                    k += vr.headerLen + vr.dataLen;
                }

                vector3.addElement(((Object) (new VR(l, 0, 1, ((Object) (new Long(k))), 4))));
            }
            for(Enumeration enumeration2 = inthashtable.keys(); enumeration2.hasMoreElements(); vector1.addElement(enumeration2.nextElement()));
            vector1 = DicomUtils.bubbleSort(vector1);
            for(int j1 = 0; j1 < vector1.size(); j1++)
            {
                VR vr1 = (VR)inthashtable.get(vector1.elementAt(j1));
                vector3.addElement(((Object) (vr1)));
            }

        }

        return vector3.elements();
    }

    public DicomObject copyGroup(int i)
    {
        DicomObject dicomobject = new DicomObject();
        IntHashtable inthashtable = (IntHashtable)a.get(i);
        VR vr;
        for(Enumeration enumeration = inthashtable.keys(); enumeration.hasMoreElements(); ((GroupList) (dicomobject)).a(vr))
            vr = (VR)inthashtable.get(enumeration.nextElement());

        return dicomobject;
    }

    public DicomObject removeGroup(int i)
    {
        DicomObject dicomobject = new DicomObject();
        if(a == null)
            return null;
        IntHashtable inthashtable = (IntHashtable)a.get(i);
        if(inthashtable == null)
            return null;
        VR vr;
        for(Enumeration enumeration = inthashtable.keys(); enumeration.hasMoreElements(); ((GroupList) (dicomobject)).a(vr))
            vr = (VR)inthashtable.get(enumeration.nextElement());

        a.remove(i);
        System.gc();
        return dicomobject;
    }

    public boolean containsGroup(int i)
    {
        return a.containsKey(i);
    }

    void a(DicomObject dicomobject)
    {
        Enumeration enumeration = ((GroupList) (dicomobject)).a.keys();
        int i = ((Integer)enumeration.nextElement()).intValue();
        if(!containsGroup(i))
            a.put(i, ((GroupList) (dicomobject)).a.get(i));
    }

    public void addGroups(DicomObject dicomobject)
    {
        DicomObject dicomobject1;
        for(Enumeration enumeration = ((GroupList) (dicomobject)).a.keys(); enumeration.hasMoreElements(); a(dicomobject1))
        {
            int i = ((Integer)enumeration.nextElement()).intValue();
            dicomobject1 = ((GroupList) (dicomobject)).copyGroup(i);
        }

    }

    public int numberOfGroups()
    {
        return a.size();
    }

    public int numberOfElements()
    {
        int i = 0;
        for(Enumeration enumeration = a.elements(); enumeration.hasMoreElements();)
        {
            IntHashtable inthashtable = (IntHashtable)enumeration.nextElement();
            i += inthashtable.size();
        }

        return i;
    }
}
