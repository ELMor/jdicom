// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            TagValue, DDict, Debug, k, 
//            OffsetOutputStream, DicomException, GroupList, b, 
//            DicomObject, i, h, UID, 
//            UIDEntry, OffsetInputStream, IllegalValueException, f, 
//            DumpUtils

class VR extends TagValue
{

    protected int dataLen;
    protected int headerLen;
    protected int dcm_type;
    protected boolean empty;

    protected VR()
    {
        dataLen = 0;
        headerLen = 0;
        dcm_type = 0;
        empty = false;
        super.group = 0;
        super.element = 0;
        super.val = new Vector();
        dcm_type = 0;
        dataLen = 0;
        headerLen = 0;
        empty = true;
    }

    protected VR(int j, int l, int i1)
    {
        dataLen = 0;
        headerLen = 0;
        dcm_type = 0;
        empty = false;
        super.group = j;
        super.element = l;
        dcm_type = i1;
        empty = true;
    }

    protected VR(int j, int l, int i1, Object obj, int j1)
    {
        dataLen = 0;
        headerLen = 0;
        dcm_type = 0;
        empty = false;
        super.group = j;
        super.element = l;
        super.val = new Vector();
        dcm_type = i1;
        super.val.addElement(obj);
        dataLen = j1;
        empty = false;
        headerLen = 8;
    }

    protected int getDDType()
    {
        int j = DDict.lookupDDict(super.group, super.element);
        return j;
    }

    protected void writeVRHeader(k k1)
        throws IOException, DicomException
    {
        if(Debug.DEBUG >= 5)
            Debug.printMessage("VR.writeVRHeader", super.group, super.element);
        int j = k1.a();
        boolean flag = k1.b();
        int l = 0;
        byte abyte0[] = {
            0, 0
        };
        k1.b(super.group);
        k1.b(super.element);
        if(super.group == 2)
            j = 8194;
        l = dataLen;
        if(empty)
            l = 0;
        else
        if(dcm_type == 10 && flag)
            l = -1;
        if((dcm_type == 24 || dcm_type == 8 || dcm_type == 22) && super.val.size() > 1)
        {
            dcm_type = 8;
            l = -1;
        }
        if(j == 8193)
        {
            k1.c(l);
        } else
        {
            String s = DDict.a(dcm_type);
            k1.a(s.substring(0, 2));
            switch(dcm_type)
            {
            case 0: // '\0'
            case 8: // '\b'
            case 10: // '\n'
            case 22: // '\026'
            case 24: // '\030'
            case 27: // '\033'
                ((OffsetOutputStream) (k1)).write(abyte0);
                k1.c(l);
                break;

            default:
                k1.b(l);
                break;
            }
        }
    }

    private void a(i j, DicomObject dicomobject)
        throws IOException, DicomException
    {
        VR vr;
        for(Enumeration enumeration = ((GroupList) (dicomobject)).a(false, ((k) (j)).a(), ((k) (j)).b(), true); enumeration.hasMoreElements(); vr.writeVRData(j))
        {
            vr = (VR)enumeration.nextElement();
            vr.writeVRHeader(((k) (j)));
        }

    }

    private void a(i j)
        throws DicomException, IOException
    {
        if(((k) (j)).b())
        {
            for(int l = 0; l < super.val.size(); l++)
            {
                DicomObject dicomobject = (DicomObject)super.val.elementAt(l);
                ((k) (j)).b(65534);
                ((k) (j)).b(57344);
                ((k) (j)).c(-1);
                a(j, dicomobject);
                ((k) (j)).b(65534);
                ((k) (j)).b(57357);
                ((k) (j)).c(0);
            }

            ((k) (j)).b(65534);
            ((k) (j)).b(57565);
            ((k) (j)).c(0);
        } else
        {
            for(int i1 = 0; i1 < super.val.size(); i1++)
            {
                DicomObject dicomobject1 = (DicomObject)super.val.elementAt(i1);
                ((k) (j)).b(65534);
                ((k) (j)).b(57344);
                ((k) (j)).c(com.archimed.dicom.b.a(dicomobject1, ((k) (j)).a(), ((k) (j)).b()) - 8);
                a(j, dicomobject1);
            }

        }
    }

    private void b(i j)
        throws IOException, DicomException
    {
        for(int l = 0; l < super.val.size(); l++)
        {
            byte abyte0[] = (byte[])super.val.elementAt(l);
            ((k) (j)).b(65534);
            ((k) (j)).b(57344);
            ((k) (j)).c(abyte0.length);
            if(abyte0.length != 0)
                ((OffsetOutputStream) (j)).write(abyte0);
        }

        ((k) (j)).b(65534);
        ((k) (j)).b(57565);
        ((k) (j)).c(0);
    }

    protected void writeVRData(i j)
        throws IOException, DicomException
    {
        if(dataLen == 0 || empty)
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.writeVRData <empty>", super.group, super.element);
            return;
        }
        if(dcm_type == 10)
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.writeVRData <sequence>", super.group, super.element);
            a(j);
            return;
        }
        if((dcm_type == 24 || dcm_type == 8 || dcm_type == 22) && super.val.size() > 1)
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.writeVRData <undefined length>", super.group, super.element);
            b(j);
            return;
        }
        if(Debug.DEBUG >= 5)
            Debug.printMessage("VR.writeVRData <normal>", super.group, super.element);
        j.d(super.val, dcm_type);
    }

    protected boolean readVRHeader(h h1)
        throws IOException, DicomException
    {
        int j = h1.j();
        super.group = h1.k();
        super.element = h1.k();
        if(Debug.DEBUG >= 5)
            try
            {
                Debug.printMessage("VR.readVRHeader (ts: " + UID.getUIDEntry(h1.j()).toString() + ")", super.group, super.element);
            }
            catch(IllegalValueException illegalvalueexception) { }
        if(super.group == 2)
            j = 8194;
        if(super.element == 0)
            dcm_type = 1;
        else
            dcm_type = DDict.getTypeCode(super.group, super.element);
        if(j == 8193)
        {
            dataLen = h1.m();
            headerLen = 8;
        } else
        {
            String s = h1.d(2);
            if(super.group != 65534 || super.element != 57357)
                dcm_type = DDict.a(s);
            switch(dcm_type)
            {
            case 0: // '\0'
            case 8: // '\b'
            case 10: // '\n'
            case 24: // '\030'
            case 27: // '\033'
                ((OffsetInputStream) (h1)).skipBytes(2);
                dataLen = h1.m();
                headerLen = 12;
                break;

            default:
                dataLen = h1.k();
                headerLen = 8;
                break;
            }
        }
        int l = DDict.c(dcm_type);
        if(l != 0 && dataLen % l != 0)
        {
            ((OffsetInputStream) (h1)).skipBytes(dataLen);
            Debug.out.println("Data element: (" + super.group + "," + super.element + "): " + DDict.getDescription(DDict.lookupDDict(super.group, super.element)) + " has an invalid length (" + dataLen + ")\n    Skipping...");
            return false;
        } else
        {
            return true;
        }
    }

    private DicomObject a(f f1)
        throws IOException, DicomException
    {
        DicomObject dicomobject = new DicomObject();
        dicomobject.b = ((OffsetInputStream) (f1)).getOffset() - 4L;
        long l = ((h) (f1)).m();
        if(l != -1L)
        {
            dicomobject.c = 0;
            for(long l1 = ((OffsetInputStream) (f1)).getOffset() + l; ((OffsetInputStream) (f1)).getOffset() != l1;)
            {
                VR vr = new VR();
                if(vr.readVRHeader(((h) (f1))) && vr.readVRData(f1))
                {
                    dicomobject.c += vr.headerLen;
                    dicomobject.c += vr.dataLen;
                    ((GroupList) (dicomobject)).a(vr);
                }
            }

        } else
        {
            boolean flag = false;
            dicomobject.c = 0;
            while(!flag) 
            {
                VR vr1 = new VR();
                if(vr1.readVRHeader(((h) (f1))))
                    if(((TagValue) (vr1)).group == 65534 && ((TagValue) (vr1)).element == 57357)
                        flag = true;
                    else
                    if(vr1.readVRData(f1))
                    {
                        dicomobject.c += vr1.headerLen;
                        dicomobject.c += vr1.dataLen;
                        ((GroupList) (dicomobject)).a(vr1);
                    }
            }
        }
        dicomobject.c += 16;
        return dicomobject;
    }

    private Vector b(f f1)
        throws DicomException, IOException
    {
        Vector vector = new Vector();
        if(dataLen != -1)
        {
            long l1 = ((OffsetInputStream) (f1)).getOffset() + (long)dataLen;
            dataLen = 0;
            while(((OffsetInputStream) (f1)).getOffset() != l1) 
            {
                int j = ((h) (f1)).k();
                int i1 = ((h) (f1)).k();
                if(j == 65534 && i1 == 57344)
                {
                    DicomObject dicomobject = a(f1);
                    if(dicomobject != null)
                    {
                        dataLen += dicomobject.c;
                        vector.addElement(((Object) (dicomobject)));
                    }
                }
            }
        } else
        {
            boolean flag = false;
            dataLen = 0;
            while(!flag) 
            {
                int l = ((h) (f1)).k();
                int j1 = ((h) (f1)).k();
                if(l == 65534 && j1 == 57344)
                {
                    DicomObject dicomobject1 = a(f1);
                    if(dicomobject1 != null)
                    {
                        dataLen += dicomobject1.c;
                        vector.addElement(((Object) (dicomobject1)));
                    }
                } else
                if(l == 65534 && j1 == 57565)
                {
                    ((OffsetInputStream) (f1)).skipBytes(4);
                    flag = true;
                } else
                {
                    throw new DicomException("Unexpected tag read: (" + l + "," + j1 + ") at offset " + ((OffsetInputStream) (f1)).getOffset());
                }
            }
        }
        dataLen += 8;
        return vector;
    }

    private Vector c(f f1)
        throws DicomException, IOException
    {
        Vector vector = new Vector();
        dataLen = 0;
        do
        {
            int j = ((h) (f1)).k();
            int l = ((h) (f1)).k();
            dataLen += 4;
            if(j == 65534 && l == 57565)
            {
                ((OffsetInputStream) (f1)).skipBytes(4);
                dataLen += 4;
                return vector;
            }
            if(j == 65534 && l == 57344)
            {
                int i1 = ((h) (f1)).m();
                dataLen += 4;
                byte abyte0[] = new byte[i1];
                ((OffsetInputStream) (f1)).readFully(abyte0);
                dataLen += i1;
                vector.addElement(((Object) (abyte0)));
            } else
            {
                throw new DicomException("Unexpected tag encountered: (" + j + "," + l + ").");
            }
        } while(true);
    }

    protected boolean readVRData(f f1)
        throws IOException, DicomException
    {
        if(dataLen == 0)
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.readVRData <empty>", super.group, super.element);
            empty = true;
            super.val = new Vector();
            return true;
        }
        empty = false;
        if(dataLen == -1)
        {
            if(dcm_type == 24 || dcm_type == 8 || dcm_type == 22)
            {
                if(Debug.DEBUG >= 5)
                    Debug.printMessage("VR.readVRData <undefined length>", super.group, super.element);
                super.val = c(f1);
                return super.val != null;
            }
            dcm_type = 10;
        }
        if(dcm_type == 10)
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.readVRData <sequence>", super.group, super.element);
            super.val = b(f1);
            return super.val != null;
        }
        try
        {
            if(Debug.DEBUG >= 5)
                Debug.printMessage("VR.readVRData <normal>", super.group, super.element);
            super.val = f1.d(dcm_type, dataLen);
        }
        catch(Exception exception)
        {
            if(Debug.DEBUG >= 1)
            {
                Debug.out.print("Encoding error in data of +(");
                Debug.out.print(DumpUtils.a(super.group, 4) + ",");
                Debug.out.print(DumpUtils.a(super.element, 4) + "), ");
                Debug.out.println("tag excluded from dataset");
                if(Debug.DEBUG > 2)
                    ((Throwable) (exception)).printStackTrace(Debug.out);
            }
            return false;
        }
        return true;
    }
}
