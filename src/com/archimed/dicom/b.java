// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            Person, DDateTime, DDateTimeRange, DTime, 
//            DTimeRange, DDateRange, GroupList, TagValue, 
//            VR, DicomObject, DDict, IntHashtable

class b
{

    b()
    {
    }

    static int a(int k)
    {
        if((k & 1) == 1)
            return k + 1;
        else
            return k;
    }

    private static int a(Vector vector)
    {
        if(vector.size() > 0)
            return vector.size() - 1;
        else
            return 0;
    }

    private static int b(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
            k += ((String)vector.elementAt(l)).length();

        return k;
    }

    private static int c(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
            k += ((Float)vector.elementAt(l)).toString().length();

        return k;
    }

    private static int d(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
            k += ((Integer)vector.elementAt(l)).toString().length();

        return k;
    }

    private static int e(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
            k += ((Person)vector.elementAt(l)).a('^').length();

        return k;
    }

    private static int f(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
        {
            Object obj = vector.elementAt(l);
            if(obj instanceof DDateTime)
                k += ((DDateTime)obj).toDICOMString().length();
            else
                k += ((DDateTimeRange)obj).toDICOMString().length();
        }

        return k;
    }

    private static int g(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
        {
            Object obj = vector.elementAt(l);
            if(obj instanceof DTime)
                k += ((DTime)obj).toDICOMString().length();
            else
                k += ((DTimeRange)obj).toDICOMString().length();
        }

        return k;
    }

    private static int h(Vector vector)
    {
        int k = 0;
        if(vector.size() == 0)
            return 0;
        try
        {
            k = ((DDateRange)vector.elementAt(0)).toDICOMString().length();
        }
        catch(ClassCastException classcastexception)
        {
            k = vector.size() * 8;
        }
        return k;
    }

    private static int i(Vector vector)
    {
        int k = 0;
        for(int l = 0; l < vector.size(); l++)
            k += ((byte[])vector.elementAt(l)).length;

        return k;
    }

    static int a(DicomObject dicomobject, int k, boolean flag)
    {
        int l = 0;
        if(flag)
            l = 16;
        else
            l = 8;
        for(Enumeration enumeration = ((GroupList) (dicomobject)).a(false, k, flag, true); enumeration.hasMoreElements();)
        {
            VR vr = (VR)enumeration.nextElement();
            l += b(((TagValue) (vr)).val, vr.dcm_type, k, flag);
            l += a(((TagValue) (vr)).val, vr.dcm_type, k, flag);
        }

        return l;
    }

    private static int a(Vector vector, int k, boolean flag)
    {
        int l = 0;
        for(int i1 = 0; i1 < vector.size(); i1++)
        {
            DicomObject dicomobject = (DicomObject)vector.elementAt(i1);
            dicomobject.c = a(dicomobject, k, flag);
            l += dicomobject.c;
        }

        return l;
    }

    private static int a(Vector vector, boolean flag)
    {
        byte byte2;
        if(flag)
        {
            byte2 = 8;
            byte byte0 = 16;
        } else
        {
            byte2 = 0;
            byte byte1 = 8;
        }
        return ((int) (byte2));
    }

    private static int j(Vector vector)
    {
        if(vector.size() == 1 || vector.size() == 0)
            return 0;
        else
            return 8 + vector.size() * 8;
    }

    static int a(Vector vector, int k, int l, boolean flag)
    {
        int i1 = DDict.c(k);
        int j1 = 0;
        if(i1 != 0)
            return i1 * vector.size();
        switch(k)
        {
        case 2: // '\002'
        case 4: // '\004'
        case 6: // '\006'
        case 7: // '\007'
        case 9: // '\t'
        case 13: // '\r'
        case 18: // '\022'
        case 27: // '\033'
            j1 = b(vector);
            break;

        case 12: // '\f'
            j1 = g(vector);
            break;

        case 28: // '\034'
            j1 = f(vector);
            break;

        case 16: // '\020'
            j1 = c(vector);
            break;

        case 15: // '\017'
            j1 = d(vector);
            break;

        case 14: // '\016'
            j1 = e(vector);
            break;

        case 11: // '\013'
            j1 = h(vector);
            break;

        case 10: // '\n'
            if(vector.isEmpty())
                return 0;
            else
                return a(a(vector, l, flag) + a(vector, flag));

        case 8: // '\b'
        case 22: // '\026'
        case 24: // '\030'
            return a(i(vector) + j(vector));

        case 3: // '\003'
        case 5: // '\005'
        case 17: // '\021'
        case 19: // '\023'
        case 20: // '\024'
        case 21: // '\025'
        case 23: // '\027'
        case 25: // '\031'
        case 26: // '\032'
        default:
            j1 = i(vector);
            break;
        }
        j1 += a(vector);
        return a(j1);
    }

    private static int a(int k, int l)
    {
        if(l == 8193)
            return 8;
        switch(k)
        {
        case 0: // '\0'
        case 8: // '\b'
        case 10: // '\n'
        case 22: // '\026'
        case 24: // '\030'
        case 27: // '\033'
            return 12;
        }
        return 8;
    }

    static int b(Vector vector, int k, int l, boolean flag)
    {
        return a(k, l);
    }

    static int a(IntHashtable inthashtable, int k, boolean flag)
    {
        int l = 0;
        for(Enumeration enumeration = inthashtable.elements(); enumeration.hasMoreElements();)
        {
            VR vr = (VR)enumeration.nextElement();
            l += b(((TagValue) (vr)).val, vr.dcm_type, k, flag);
            l += a(((TagValue) (vr)).val, vr.dcm_type, k, flag);
        }

        return l;
    }
}
