// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom:
//            DDateTime, DDateTimeRange, DTime, DTimeRange, 
//            Person, DDate, DDateRange, GroupList, 
//            DicomObject, VR, DDict, TagValue

class DumpUtils
{

    DumpUtils()
    {
    }

    public static String objectToDescription(Object obj, int i)
    {
        String s = "";
        if(obj == null)
            return null;
        switch(i)
        {
        case 2: // '\002'
        case 4: // '\004'
        case 6: // '\006'
        case 7: // '\007'
        case 9: // '\t'
        case 13: // '\r'
        case 17: // '\021'
        case 18: // '\022'
        case 27: // '\033'
            s = ((String)obj).trim();
            break;

        case 28: // '\034'
            if(obj instanceof DDateTime)
                s = ((DDateTime)obj).toDICOMString();
            else
                s = ((DDateTimeRange)obj).toDICOMString();
            break;

        case 12: // '\f'
            if(obj instanceof DTime)
                s = ((DTime)obj).toDICOMString();
            else
                s = ((DTimeRange)obj).toDICOMString();
            break;

        case 14: // '\016'
            s = ((Person)obj).a('^');
            break;

        case 11: // '\013'
            if(obj instanceof DDate)
                s = ((DDate)obj).toDICOMString();
            else
                s = ((DDateRange)obj).toDICOMString();
            break;

        case 16: // '\020'
        case 26: // '\032'
            s = ((Float)obj).toString();
            break;

        case 3: // '\003'
        case 15: // '\017'
        case 21: // '\025'
            s = ((Integer)obj).toString();
            break;

        case 23: // '\027'
            s = ((Short)obj).toString();
            break;

        case 1: // '\001'
        case 19: // '\023'
            s = ((Long)obj).toString();
            break;

        case 20: // '\024'
            s = ((Double)obj).toString();
            break;

        case 0: // '\0'
            s = new String((byte[])obj);
            break;

        case 8: // '\b'
        case 22: // '\026'
        case 24: // '\030'
            s = "";
            int j = ((byte[])obj).length;
            if(j > 12)
                j = 12;
            for(int k = 0; k < j; k++)
                if(k != j - 1)
                    s = s + a(((byte[])obj)[k] & 0xff, 2) + "\\";
                else
                    s = s + a(((byte[])obj)[k] & 0xff, 2);

            if(j == 12)
                s = s + "...";
            break;

        case 5: // '\005'
            int l = ((Integer)obj).intValue() >> 16;
            int i1 = ((Integer)obj).intValue() & 0xffff;
            s = "(" + a(l, 4) + "," + a(i1, 4) + ")";
            break;

        case 10: // '\n'
        case 25: // '\031'
        default:
            return null;
        }
        boolean flag = true;
        for(int j1 = 0; j1 < s.length(); j1++)
            if(' ' > s.charAt(j1) && s.charAt(j1) <= '~')
            {
                flag = false;
                j1 = s.length();
            }

        if(flag)
            return s;
        else
            return "Not printable";
    }

    public static String seqToDescription(Object obj, int i, int j)
    {
        String s = "";
        if(obj == null)
            return null;
        switch(i)
        {
        case 10: // '\n'
            s = s + "\n";
            for(Enumeration enumeration = ((GroupList) ((DicomObject)obj)).enumerateVRs(false, true); enumeration.hasMoreElements();)
                s = s + dumpVR((VR)enumeration.nextElement(), j + 1);

            return s;
        }
        return null;
    }

    public static String dumpVR(VR vr, int i)
    {
        String s = "";
        boolean flag = vr.dcm_type == 10;
        byte byte0 = 6;
        for(int j = 0; j < i * byte0; j++)
            s = s + " ";

        s = s + "(" + a(((TagValue) (vr)).group, 4) + "," + a(((TagValue) (vr)).element, 4) + ")";
        s = s + " " + DDict.a(vr.dcm_type).substring(0, 2) + " ";
        s = s + "[";
        String s3 = "";
        for(int k = 0; k < ((TagValue) (vr)).val.size(); k++)
        {
            if(flag)
            {
                String s1 = seqToDescription(((TagValue) (vr)).val.elementAt(k), vr.dcm_type, i);
                if(s1 != null)
                    s3 = s3 + s1;
            } else
            {
                String s2 = objectToDescription(((TagValue) (vr)).val.elementAt(k), vr.dcm_type);
                if(s2 != null)
                    s3 = s3 + s2;
            }
            if(k != ((TagValue) (vr)).val.size() - 1)
            {
                if(flag)
                {
                    for(int l = 0; l < i * byte0; l++)
                        s3 = s3 + " ";

                }
                s3 = s3 + "\\";
            }
        }

        if(flag)
        {
            for(int i1 = 0; i1 < i * byte0; i1++)
                s3 = s3 + " ";

        } else
        if(s3.length() > DicomObject.dumpLineLen - i * byte0)
        {
            s3 = s3.substring(0, DicomObject.dumpLineLen - i * byte0);
            s3 = s3 + "...";
        }
        s = s + s3 + "]";
        int j1;
        if((j1 = s.lastIndexOf("\n")) == -1)
            j1 = 0;
        else
            j1++;
        String s4 = s.substring(j1, s.length());
        for(int k1 = 0; k1 < 60 - s4.length(); k1++)
            s = s + " ";

        s = s + "#  " + vr.dataLen;
        for(int l1 = 0; l1 < 8 - (new Integer(vr.dataLen)).toString().length(); l1++)
            s = s + " ";

        s = s + ((TagValue) (vr)).val.size();
        for(int i2 = 0; i2 < 3 - (new Integer(((TagValue) (vr)).val.size())).toString().length(); i2++)
            s = s + " ";

        String s5 = DDict.getDescription(DDict.lookupDDict(((TagValue) (vr)).group, ((TagValue) (vr)).element));
        if(s5.equals("Undefined") && ((TagValue) (vr)).group % 2 == 1)
            s5 = "Proprietary Tag";
        s = s + s5 + "\n";
        return s;
    }

    static String a(int i, int j)
    {
        String s = "";
        String s1 = Integer.toHexString(i);
        if(j < s1.length())
            s1 = s1.substring(0, j);
        for(int k = 0; k < j - s1.length(); k++)
            s = s + "0";

        return s + s1;
    }
}
