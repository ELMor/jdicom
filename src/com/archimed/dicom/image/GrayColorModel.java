// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.image;

import java.awt.image.ColorModel;

// Referenced classes of package com.archimed.dicom.image:
//            WL

public class GrayColorModel extends ColorModel
{

    private int a;
    private int b;
    private int c[];

    private void a()
    {
        c = new int[(a + 1) - b];
        if(a + 1 == 256)
        {
            for(int i = 0; i < 256; i++)
                c[i] = 0xff000000 | (i & 0xff) << 16 | (i & 0xff) << 8 | i & 0xff;

        } else
        {
            boolean flag = false;
            for(int k = b; k < a + 1; k++)
            {
                int j = ((k - b) * 255) / (a - b);
                c[k - b] = 0xff000000 | (j & 0xff) << 16 | (j & 0xff) << 8 | j & 0xff;
            }

        }
    }

    private void a(int i, int j)
    {
        for(int l = 0; l < c.length; l++)
            if(l < j - b)
                c[l] = 0xff000000;
            else
            if(l > (j - b) + i)
            {
                c[l] = -1;
            } else
            {
                int k = ((l - (j - b)) * 255) / i;
                c[l] = 0xff000000 | (k & 0xff) << 16 | (k & 0xff) << 8 | k & 0xff;
            }

    }

    public GrayColorModel(int i)
    {
        this(i, (1 << i) - 1);
    }

    public GrayColorModel(int i, int j)
    {
        super(i);
        a = 0;
        b = 0;
        super.pixel_bits = i;
        a = j;
        a();
    }

    public GrayColorModel(int i, int j, int k)
    {
        super(i);
        a = 0;
        b = 0;
        super.pixel_bits = i;
        b = j;
        a = k;
        a();
    }

    public void setWindowLevel(WL wl)
    {
        a(wl.window, wl.level);
    }

    public int getAlpha(int i)
    {
        return 255;
    }

    public int getRed(int i)
    {
        return c[i - b] & 0xff;
    }

    public int getGreen(int i)
    {
        return c[i - b] & 0xff;
    }

    public int getBlue(int i)
    {
        return c[i - b] & 0xff;
    }

    public int getRGB(int i)
    {
        return c[i - b];
    }

    public int getMax()
    {
        return a;
    }

    public int getMin()
    {
        return b;
    }
}
