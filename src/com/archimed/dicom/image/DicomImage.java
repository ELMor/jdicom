// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.image;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import java.io.PrintStream;

public class DicomImage extends DicomObject
{

    public DicomImage()
    {
    }

    public void patientData(String s, String s1, String s2, String s3)
        throws DicomException
    {
        try
        {
            ((DicomObject)this).set(147, ((Object) (s)));
            ((DicomObject)this).set(148, ((Object) (s1)));
            ((DicomObject)this).set(152, ((Object) (s3)));
        }
        catch(DicomException dicomexception)
        {
            System.err.println(((Object) (dicomexception)));
        }
        try
        {
            ((DicomObject)this).set(150, ((Object) (s2)));
        }
        catch(DicomException dicomexception1)
        {
            throw new DicomException("Invalid Date format");
        }
    }

    public void generalStudyData(String s, String s1, String s2, String s3, String s4, String s5)
        throws DicomException
    {
        ((DicomObject)this).set(425, ((Object) (s)));
        ((DicomObject)this).set(64, ((Object) (s1)));
        ((DicomObject)this).set(70, ((Object) (s2)));
        ((DicomObject)this).set(88, ((Object) (s3)));
        ((DicomObject)this).set(427, ((Object) (s4)));
        ((DicomObject)this).set(77, ((Object) (s5)));
    }

    public void generalSeriesData(String s, String s1, String s2)
        throws DicomException
    {
        ((DicomObject)this).set(81, ((Object) (s)));
        ((DicomObject)this).set(426, ((Object) (s1)));
        ((DicomObject)this).set(428, ((Object) (s2)));
    }

    public void generalEquipmentData(String s)
    {
        try
        {
            ((DicomObject)this).set(84, ((Object) (s)));
        }
        catch(DicomException dicomexception) { }
    }

    public void generalImageData(String s)
        throws DicomException
    {
        try
        {
            ((DicomObject)this).set(430, ((Object) (s)));
        }
        catch(DicomException dicomexception)
        {
            throw new DicomException("Invalid Integer format");
        }
    }

    public void imagePixelData(int i, int j, int k, int l, int i1, byte abyte0[])
        throws DicomException
    {
        ((DicomObject)this).set(461, ((Object) (new Integer(1))));
        ((DicomObject)this).set(462, "MONOCHROME2");
        ((DicomObject)this).set(466, ((Object) (new Integer(i))));
        ((DicomObject)this).set(467, ((Object) (new Integer(j))));
        if(k != 8 && k != 16 && k != 32)
            throw new DicomException("BitsAllocated " + k + ": other than 8, 16 or 32");
        if(l > k)
            throw new DicomException("BitsStored > BitsAllocated");
        if(i1 >= l)
            throw new DicomException("HighBit >= BitsStored");
        if(abyte0 == null)
        {
            throw new DicomException("PixelData empty");
        } else
        {
            ((DicomObject)this).set(475, ((Object) (new Integer(k))));
            ((DicomObject)this).set(476, ((Object) (new Integer(l))));
            ((DicomObject)this).set(477, ((Object) (new Integer(i1))));
            ((DicomObject)this).set(478, ((Object) (new Integer(0))));
            ((DicomObject)this).set(1184, ((Object) (abyte0)));
            return;
        }
    }

    public void imagePixelData(int i, int j, int k, int l, int i1, int ai[])
        throws DicomException
    {
        ((DicomObject)this).set(461, ((Object) (new Integer(1))));
        ((DicomObject)this).set(462, "MONOCHROME2");
        ((DicomObject)this).set(466, ((Object) (new Integer(i))));
        ((DicomObject)this).set(467, ((Object) (new Integer(j))));
        if(k != 8 && k != 16 && k != 24 && k != 32)
            throw new DicomException("BitsAllocated " + k + ": other than 8, 16 or 32");
        if(l > k)
            throw new DicomException("BitsStored > BitsAllocated");
        if(i1 >= l)
            throw new DicomException("HighBit >= BitsStored");
        if(ai == null)
            throw new DicomException("PixelData empty");
        ((DicomObject)this).set(475, ((Object) (new Integer(k))));
        ((DicomObject)this).set(476, ((Object) (new Integer(l))));
        ((DicomObject)this).set(477, ((Object) (new Integer(i1))));
        ((DicomObject)this).set(478, ((Object) (new Integer(0))));
        int j1 = k / 8;
        byte abyte0[] = new byte[ai.length * j1];
        for(int k1 = 0; k1 < ai.length; k1++)
        {
            for(int l1 = 0; l1 < j1; l1++)
                abyte0[j1 * k1 + l1] = (byte)((ai[k1] & 255 << 8 * l1) >> 8 * l1);

        }

        ((DicomObject)this).set(1184, ((Object) (abyte0)));
    }

    public void imagePixelData(int i, int j, int k, byte abyte0[])
        throws DicomException
    {
        if(k != 0 && k != 1)
            throw new DicomException("Planar Configuration has to be 0 or 1");
        if(abyte0 == null)
        {
            throw new DicomException("PixelData empty");
        } else
        {
            ((DicomObject)this).set(461, ((Object) (new Integer(3))));
            ((DicomObject)this).set(462, "RGB");
            ((DicomObject)this).set(466, ((Object) (new Integer(i))));
            ((DicomObject)this).set(467, ((Object) (new Integer(j))));
            ((DicomObject)this).set(475, ((Object) (new Integer(8))));
            ((DicomObject)this).set(476, ((Object) (new Integer(8))));
            ((DicomObject)this).set(477, ((Object) (new Integer(7))));
            ((DicomObject)this).set(478, ((Object) (new Integer(0))));
            ((DicomObject)this).set(463, ((Object) (new Integer(k))));
            ((DicomObject)this).set(1184, ((Object) (abyte0)));
            return;
        }
    }

    public void imagePixelData(int i, int j, int k, int ai[])
        throws DicomException
    {
        if(k != 0 && k != 1)
            throw new DicomException("Planar Configuration has to be 0 or 1");
        if(ai == null)
            throw new DicomException("PixelData empty");
        ((DicomObject)this).set(461, ((Object) (new Integer(3))));
        ((DicomObject)this).set(462, "RGB");
        ((DicomObject)this).set(466, ((Object) (new Integer(i))));
        ((DicomObject)this).set(467, ((Object) (new Integer(j))));
        ((DicomObject)this).set(475, ((Object) (new Integer(8))));
        ((DicomObject)this).set(476, ((Object) (new Integer(8))));
        ((DicomObject)this).set(477, ((Object) (new Integer(7))));
        ((DicomObject)this).set(478, ((Object) (new Integer(0))));
        ((DicomObject)this).set(463, ((Object) (new Integer(k))));
        byte abyte0[] = new byte[ai.length * 3];
        if(k == 0)
        {
            for(int l = 0; l < ai.length; l++)
            {
                abyte0[3 * l] = (byte)((ai[l] & 0xff0000) >> 16);
                abyte0[3 * l + 1] = (byte)((ai[l] & 0xff00) >> 8);
                abyte0[3 * l + 2] = (byte)(ai[l] & 0xff);
            }

        } else
        {
            for(int i1 = 0; i1 < ai.length; i1++)
            {
                abyte0[i1] = (byte)((ai[i1] & 0xff0000) >> 16);
                abyte0[ai.length + i1] = (byte)((ai[i1] & 0xff00) >> 8);
                abyte0[2 * ai.length + i1] = (byte)(ai[i1] & 0xff);
            }

        }
        ((DicomObject)this).set(1184, ((Object) (abyte0)));
    }

    public void imagePixelData(int i, int j, byte abyte0[], byte abyte1[], byte abyte2[], byte abyte3[])
        throws DicomException
    {
        if(abyte0 == null)
            throw new DicomException("PixelData empty");
        if(abyte1 == null)
            throw new DicomException("Red Palette empty");
        if(abyte2 == null)
            throw new DicomException("Green Palette empty");
        if(abyte3 == null)
            throw new DicomException("Blue Palette empty");
        ((DicomObject)this).set(466, ((Object) (new Integer(i))));
        ((DicomObject)this).set(467, ((Object) (new Integer(j))));
        ((DicomObject)this).set(475, ((Object) (new Integer(8))));
        ((DicomObject)this).set(476, ((Object) (new Integer(8))));
        ((DicomObject)this).set(477, ((Object) (new Integer(7))));
        ((DicomObject)this).set(494, ((Object) (new Integer(256))), 0);
        ((DicomObject)this).set(494, ((Object) (new Integer(0))), 1);
        ((DicomObject)this).set(494, ((Object) (new Integer(16))), 2);
        ((DicomObject)this).set(495, ((Object) (new Integer(256))), 0);
        ((DicomObject)this).set(495, ((Object) (new Integer(0))), 1);
        ((DicomObject)this).set(495, ((Object) (new Integer(16))), 2);
        ((DicomObject)this).set(496, ((Object) (new Integer(256))), 0);
        ((DicomObject)this).set(496, ((Object) (new Integer(0))), 1);
        ((DicomObject)this).set(496, ((Object) (new Integer(16))), 2);
        ((DicomObject)this).set(461, ((Object) (new Integer(1))));
        ((DicomObject)this).set(462, "PALETTE COLOR");
        ((DicomObject)this).set(478, ((Object) (new Integer(0))));
        byte abyte4[] = new byte[512];
        byte abyte5[] = new byte[512];
        byte abyte6[] = new byte[512];
        for(int k = 0; k < abyte1.length; k++)
            abyte4[2 * k + 1] = abyte1[k];

        for(int l = 0; l < abyte2.length; l++)
            abyte5[2 * l + 1] = abyte2[l];

        for(int i1 = 0; i1 < abyte3.length; i1++)
            abyte6[2 * i1 + 1] = abyte3[i1];

        ((DicomObject)this).set(498, ((Object) (abyte4)));
        ((DicomObject)this).set(499, ((Object) (abyte5)));
        ((DicomObject)this).set(500, ((Object) (abyte6)));
        ((DicomObject)this).set(1184, ((Object) (abyte0)));
    }

    public void sopCommonData(String s, String s1)
    {
        try
        {
            if(s == null)
                ((DicomObject)this).set(62, "1.2.840.10008.5.1.4.1.1.7");
            else
                ((DicomObject)this).set(62, ((Object) (s)));
            ((DicomObject)this).set(63, ((Object) (s1)));
        }
        catch(DicomException dicomexception) { }
    }
}
