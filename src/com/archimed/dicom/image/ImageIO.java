// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom.image;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package com.archimed.dicom.image:
//            x, GrayColorModel, DICOMImageConsumer, DicomImage

public class ImageIO
{

    DicomImage a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    String j;
    boolean k;
    int l;

    void a(int i1)
    {
        switch(i1)
        {
        case 8193: 
        case 8194: 
        case 8195: 
            l = 0;
            break;

        default:
            l = i1;
            break;
        }
    }

    Object b(int i1)
    {
        if(l != 0)
        {
            try
            {
                return ((Object) (Compression.decompressFrame(l, (byte[])((DicomObject) (a)).get(1184, i1 + 1), b, c, ((byte []) (null)))));
            }
            catch(DicomException dicomexception)
            {
                if(Debug.DEBUG > 0)
                    ((Throwable) (dicomexception)).printStackTrace();
                return ((Object) (null));
            }
            catch(IOException ioexception)
            {
                if(Debug.DEBUG > 0)
                    ((Throwable) (ioexception)).printStackTrace();
            }
            return ((Object) (null));
        }
        byte abyte0[];
        if(f == 1)
            abyte0 = (byte[])((DicomObject) (a)).get(1184);
        else
        if(d > 8)
        {
            abyte0 = new byte[(b * c * d) / 8];
            System.arraycopy(((DicomObject) (a)).get(1184), (b * c * i1 * d) / 8, ((Object) (abyte0)), 0, (b * c * d) / 8);
        } else
        if(j.equals("RGB"))
        {
            abyte0 = new byte[b * c * 3];
            System.arraycopy(((DicomObject) (a)).get(1184), b * c * i1 * 3, ((Object) (abyte0)), 0, b * c * 3);
        } else
        {
            abyte0 = new byte[b * c];
            System.arraycopy(((DicomObject) (a)).get(1184), b * c * i1, ((Object) (abyte0)), 0, b * c);
        }
        if(j.equals("PALETTE COLOR"))
            return ((Object) (abyte0));
        if(j.startsWith("MONOCHROME"))
        {
            if(d == 8)
                return ((Object) (abyte0));
            if(d == 16)
            {
                int ai[] = new int[b * c];
                for(int j1 = 0; j1 < b * c; j1++)
                {
                    ai[j1] = x.a(abyte0[2 * j1]);
                    ai[j1] += x.a(abyte0[2 * j1 + 1]) << 8;
                    ai[j1] &= i;
                }

                return ((Object) (ai));
            }
            if(d == 24)
            {
                int ai1[] = new int[b * c];
                for(int k1 = 0; k1 < b * c; k1++)
                {
                    ai1[k1] = x.a(abyte0[3 * k1]);
                    ai1[k1] += x.a(abyte0[3 * k1 + 1]) << 8;
                    ai1[k1] += x.a(abyte0[3 * k1 + 2]) << 16;
                    ai1[k1] &= i;
                }

                return ((Object) (ai1));
            }
            if(d == 32)
            {
                int ai2[] = new int[b * c];
                for(int l1 = 0; l1 < b * c; l1++)
                {
                    ai2[l1] = x.a(abyte0[4 * l1]);
                    ai2[l1] += x.a(abyte0[4 * l1 + 1]) << 8;
                    ai2[l1] += x.a(abyte0[4 * l1 + 2]) << 16;
                    ai2[l1] += x.a(abyte0[4 * l1 + 3]) << 24;
                    ai2[l1] &= i;
                }

                return ((Object) (ai2));
            }
            System.out.println("BA: " + d);
        }
        if(j.equals("RGB"))
        {
            int ai3[] = new int[b * c];
            if(h == 0)
            {
                for(int i2 = 0; i2 < b * c; i2++)
                {
                    ai3[i2] = x.a(abyte0[3 * i2]) << 16;
                    ai3[i2] += x.a(abyte0[3 * i2 + 1]) << 8;
                    ai3[i2] += x.a(abyte0[3 * i2 + 2]);
                }

            } else
            {
                for(int j2 = 0; j2 < b * c; j2++)
                {
                    ai3[j2] = x.a(abyte0[j2]) << 16;
                    ai3[j2] += x.a(abyte0[b * c + j2]) << 8;
                    ai3[j2] += x.a(abyte0[2 * b * c + j2]);
                }

            }
            return ((Object) (ai3));
        } else
        {
            return ((Object) (null));
        }
    }

    byte[] a(int i1, int j1)
        throws DicomException
    {
        int j3 = ((DicomObject) (a)).getI(j1, 0);
        int k3 = ((DicomObject) (a)).getI(j1, 1);
        byte abyte0[] = new byte[256];
        int k2;
        if((k2 = ((DicomObject) (a)).getSize(i1)) == 1)
        {
            byte abyte1[] = (byte[])((DicomObject) (a)).get(i1);
            int k1 = x.a(abyte1[0]);
            k1 += x.a(abyte1[1]) << 8;
            for(int l2 = 0; l2 < k3; l2++)
                abyte0[l2] = (byte)(k1 >> 8);

            for(int i4 = 0; i4 < j3; i4++)
            {
                int l1 = x.a(abyte1[2 * i4]);
                l1 += x.a(abyte1[2 * i4 + 1]) << 8;
                abyte0[i4 + k3] = (byte)(l1 >> 8);
            }

        } else
        {
            int i2 = ((DicomObject) (a)).getI(i1, 0);
            int i3;
            for(i3 = 0; i3 < k3; i3++)
                abyte0[i3] = (byte)(i2 >> 8);

            for(int l3 = 0; l3 < i3; l3++)
            {
                int j2 = ((DicomObject) (a)).getI(i1, l3);
                abyte0[l3 + k3] = (byte)(j2 >> 8);
            }

        }
        return abyte0;
    }

    int a(int ai[])
    {
        int i1 = 0;
        for(int j1 = 0; j1 < ai.length; j1++)
            if((ai[j1] & i) > i1)
                i1 = ai[j1] & i;

        return i1;
    }

    int a(byte abyte0[])
    {
        int i1 = 0;
        if(d == 8)
            i1 = 255;
        else
        if(d == 16)
        {
            boolean flag = false;
            for(int l1 = 0; l1 < abyte0.length / 2; l1++)
            {
                int j1 = x.a(abyte0[2 * l1]) + (x.a(abyte0[2 * l1 + 1]) << 8);
                if((j1 & i) > i1)
                    i1 = j1 & i;
            }

        } else
        if(d == 32)
        {
            boolean flag1 = false;
            for(int i2 = 0; i2 < abyte0.length / 4; i2++)
            {
                int k1 = x.a(abyte0[2 * i2]) + (x.a(abyte0[2 * i2 + 1]) << 8) + (x.a(abyte0[2 * i2 + 2]) << 16) + (x.a(abyte0[2 * i2 + 3]) << 24);
                if((k1 & i) > i1)
                    i1 = k1 & i;
            }

        }
        return i1;
    }

    public ImageIO(DicomImage dicomimage)
        throws DicomException
    {
        k = false;
        a = dicomimage;
        b = ((DicomObject) (a)).getI(467);
        c = ((DicomObject) (a)).getI(466);
        d = ((DicomObject) (a)).getI(475);
        e = ((DicomObject) (a)).getI(476);
        i = (1 << e) - 1;
        h = ((DicomObject) (a)).getI(463);
        if((j = ((DicomObject) (a)).getS(462)) != null)
            j = j.trim();
        if((f = ((DicomObject) (a)).getI(464)) == 0x7fffffff)
            f = 1;
        if((g = ((DicomObject) (a)).getI(480)) == 0x7fffffff)
            g = 0;
        try
        {
            int i1 = 0;
            String s = null;
            try
            {
                s = ((DicomObject) (a)).getFileMetaInformation().getS(31);
            }
            catch(Exception exception)
            {
                s = null;
            }
            if(s != null)
                i1 = UID.getUIDEntry(s).getConstant();
            a(i1);
        }
        catch(UnknownUIDException unknownuidexception)
        {
            throw new DicomException("ImageIO not possible: " + ((Throwable) (unknownuidexception)).getMessage());
        }
    }

    public ImageProducer getImageProducer()
        throws DicomException
    {
        return getImageProducer(0);
    }

    public ImageProducer getImageProducer(int i1)
        throws DicomException
    {
        if(i1 >= f)
            throw new DicomException("Index exceeds number of frames");
        if(l == 8196)
            return Toolkit.getDefaultToolkit().createImage((byte[])((DicomObject) (a)).get(1184, i1 + 1)).getSource();
        Object obj = b(i1);
        if(obj == null)
            throw new DicomException("Error ocurred building ImageProducer " + i1);
        if(j.trim().equals("MONOCHROME1"))
        {
            if(d == 8)
            {
                GrayColorModel graycolormodel = new GrayColorModel(8);
                return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (graycolormodel)), (byte[])obj, 0, b)));
            }
            if(g == 0)
                g = a((byte[])((DicomObject) (a)).get(1184));
            GrayColorModel graycolormodel1 = new GrayColorModel(d, g);
            return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (graycolormodel1)), (int[])obj, 0, b)));
        }
        if(j.trim().equals("MONOCHROME2"))
        {
            if(d == 8)
            {
                byte abyte0[] = new byte[256];
                byte abyte2[] = new byte[256];
                byte abyte4[] = new byte[256];
                for(int j1 = 0; j1 < 256; j1++)
                    abyte0[j1] = abyte2[j1] = abyte4[j1] = (byte)j1;

                IndexColorModel indexcolormodel1 = new IndexColorModel(8, 256, abyte0, abyte2, abyte4, -1);
                return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (indexcolormodel1)), (byte[])obj, 0, b)));
            }
            if(g == 0)
                g = a((byte[])((DicomObject) (a)).get(1184));
            GrayColorModel graycolormodel2 = new GrayColorModel(d, g);
            return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (graycolormodel2)), (int[])obj, 0, b)));
        }
        if(j.trim().equals("PALETTE COLOR"))
        {
            byte abyte1[] = a(498, 494);
            byte abyte3[] = a(499, 495);
            byte abyte5[] = a(500, 496);
            IndexColorModel indexcolormodel = new IndexColorModel(8, 256, abyte1, abyte3, abyte5);
            return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (indexcolormodel)), (byte[])obj, 0, b)));
        }
        if(j.equals("RGB"))
        {
            DirectColorModel directcolormodel = new DirectColorModel(24, 0xff0000, 65280, 255);
            return ((ImageProducer) (new MemoryImageSource(b, c, ((java.awt.image.ColorModel) (directcolormodel)), (int[])obj, 0, b)));
        } else
        {
            return null;
        }
    }

    public Vector getImageProducers()
        throws DicomException
    {
        Vector vector = new Vector(f);
        for(int i1 = 0; i1 < f; i1++)
            vector.addElement(((Object) (getImageProducer(i1))));

        return vector;
    }

    public synchronized void setImageProducer(ImageProducer imageproducer)
    {
        DICOMImageConsumer dicomimageconsumer = new DICOMImageConsumer(this);
        imageproducer.startProduction(((java.awt.image.ImageConsumer) (dicomimageconsumer)));
        if(!dicomimageconsumer.ready)
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException interruptedexception)
            {
                ((Throwable) (interruptedexception)).printStackTrace();
            }
        imageproducer.removeConsumer(((java.awt.image.ImageConsumer) (dicomimageconsumer)));
    }

    public int size()
    {
        return f;
    }

    public DicomImage getSource()
    {
        return a;
    }

    boolean a()
    {
        return k;
    }
}
