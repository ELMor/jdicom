// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

// Referenced classes of package com.archimed.dicom:
//            DDateTime, DDateTimeRange, DTime, DTimeRange,
//            DicomException, Person, DDate, DDateRange,
//            DDict, DumpUtils, DicomObject

class e {

  e() {
  }

  static Object a(String s, int i) throws DicomException {
    s = s.trim();
    if (s.equals("")) {
      return ( (Object) (null));
    }
    switch (i) {
      case 2: // '\002'
      case 4: // '\004'
      case 6: // '\006'
      case 7: // '\007'
      case 9: // '\t'
      case 13: // '\r'
      case 18: // '\022'
      case 27: // '\033'
        return ( (Object) (s));

      case 28: // '\034'
        return s.indexOf('-') != -1 ? (Object)new DDateTimeRange(s) :
            (Object)new DDateTime(s);

      case 12: // '\f'
        return s.indexOf('-') != -1 ? (Object)new DTimeRange(s) :
            (Object)new DTime(s);

      case 17: // '\021'
        if (s.length() != 0 && s.length() != 4) {
          throw new DicomException("Type AS has to have 4 bytes fixed.");
        }
        else {
          return ( (Object) (s));
        }

      case 14: // '\016'
        return ( (Object) (new Person(s.trim())));

      case 11: // '\013'
        return s.indexOf('-') != -1 ? (Object)new DDateRange(s) :
            (Object)new DDate(s);

      case 16: // '\020'
      case 26: // '\032'
        return ( (Object) (new Float(s)));

      case 15: // '\017'
        return ( (Object) (new Integer(s)));

      case 23: // '\027'
        return ( (Object) (new Short(s)));

      case 3: // '\003'
      case 21: // '\025'
        return ( (Object) (new Integer(s)));

      case 1: // '\001'
      case 19: // '\023'
        return ( (Object) (new Long(s)));

      case 20: // '\024'
        return ( (Object) (new Double(s)));

      case 0: // '\0'
      case 8: // '\b'
      case 22: // '\026'
      case 24: // '\030'
        return ( (Object) (s.getBytes()));

      case 5: // '\005'
      case 10: // '\n'
      case 25: // '\031'
      default:
        throw new DicomException("String used for type: " + DDict.a(i));
    }
  }

  static Object a(byte abyte0[], int i) throws DicomException {
    switch (i) {
      case 0: // '\0'
      case 8: // '\b'
      case 22: // '\026'
      case 24: // '\030'
        return ( (Object) (abyte0));
    }
    throw new DicomException("byte[] used for type: " + DDict.a(i));
  }

  static Object a(Integer integer, int i) throws DicomException {
    if (integer == null) {
      return ( (Object) (null));
    }
    switch (i) {
      case 23: // '\027'
        return ( (Object) (new Short(integer.shortValue())));

      case 3: // '\003'
      case 5: // '\005'
      case 15: // '\017'
      case 21: // '\025'
        return ( (Object) (integer));

      case 1: // '\001'
      case 19: // '\023'
        return ( (Object) (new Long(integer.intValue())));

      case 2: // '\002'
      case 4: // '\004'
      case 6: // '\006'
      case 7: // '\007'
      case 8: // '\b'
      case 9: // '\t'
      case 10: // '\n'
      case 11: // '\013'
      case 12: // '\f'
      case 13: // '\r'
      case 14: // '\016'
      case 16: // '\020'
      case 17: // '\021'
      case 18: // '\022'
      case 20: // '\024'
      case 22: // '\026'
      default:
        throw new DicomException("Integer used for type: " + DDict.a(i));
    }
  }

  static Object a(Float float1, int i) throws DicomException {
    if (float1 == null) {
      return ( (Object) (null));
    }
    switch (i) {
      case 16: // '\020'
      case 26: // '\032'
        return ( (Object) (float1));

      case 20: // '\024'
        return ( (Object) (new Double(float1.doubleValue())));
    }
    throw new DicomException("Float used for type: " + DDict.a(i));
  }

  static Object a(Double double1, int i) throws DicomException {
    if (double1 == null) {
      return ( (Object) (null));
    }
    switch (i) {
      case 20: // '\024'
        return ( (Object) (double1));

      case 16: // '\020'
      case 26: // '\032'
        return ( (Object) (new Float(double1.floatValue())));
    }
    throw new DicomException("Double used for type: " + DDict.a(i));
  }

  static Object a(Person person, int i) throws DicomException {
    switch (i) {
      case 14: // '\016'
        return ( (Object) (person));
    }
    throw new DicomException("Person used for type: " + DDict.a(i));
  }

  static Object a(DDate ddate, int i) throws DicomException {
    switch (i) {
      case 11: // '\013'
        return ( (Object) (ddate));
    }
    throw new DicomException("DDate used for type: " + DDict.a(i));
  }

  static Object a(DDateRange ddaterange, int i) throws DicomException {
    switch (i) {
      case 11: // '\013'
        return ( (Object) (ddaterange));
    }
    throw new DicomException("DDateRange used for type: " + DDict.a(i));
  }

  static Object a(DTime dtime, int i) throws DicomException {
    switch (i) {
      case 12: // '\f'
        return ( (Object) (dtime));
    }
    throw new DicomException("DTime used for type: " + DDict.a(i));
  }

  static Object a(DTimeRange dtimerange, int i) throws DicomException {
    switch (i) {
      case 12: // '\f'
        return ( (Object) (dtimerange));
    }
    throw new DicomException("DTimeRange used for type: " + DDict.a(i));
  }

  static Object a(DDateTime ddatetime, int i) throws DicomException {
    switch (i) {
      case 28: // '\034'
        return ( (Object) (ddatetime));
    }
    throw new DicomException("DDateTime used for type: " + DDict.a(i));
  }

  static Object a(DDateTimeRange ddatetimerange, int i) throws DicomException {
    switch (i) {
      case 28: // '\034'
        return ( (Object) (ddatetimerange));
    }
    throw new DicomException("DDateTimeRange used for type: " + DDict.a(i));
  }

  static Object a(Short short1, int i) throws DicomException {
    switch (i) {
      case 23: // '\027'
        return ( (Object) (short1));
    }
    throw new DicomException("Short used for type: " + DDict.a(i));
  }

  static Object a(Long long1, int i) throws DicomException {
    switch (i) {
      case 1: // '\001'
      case 19: // '\023'
        return ( (Object) (long1));
    }
    throw new DicomException("Long used for type: " + DDict.a(i));
  }

  static Object a(int ai[], int i) throws DicomException {
    if (ai == null) {
      return ( (Object) (null));
    }
    if (ai.length != 2) {
      throw new DicomException("Only int[2] can be parsed");
    }
    switch (i) {
      case 5: // '\005'
        return ( (Object) (new Integer( (ai[0] << 16) + ai[1])));
    }
    throw new DicomException("2 ints used for type: " + DDict.a(i));
  }

  static Object a(DicomObject dicomobject, int i) throws DicomException {
    if (i == 10) {
      return ( (Object) (dicomobject));
    }
    else {
      throw new DicomException("DicomObject used for type: " + DDict.a(i));
    }
  }

  static String a(Object obj, int i) {
    String s = "";
    if (obj == null) {
      return null;
    }
    switch (i) {
      case 2: // '\002'
      case 4: // '\004'
      case 6: // '\006'
      case 7: // '\007'
      case 9: // '\t'
      case 13: // '\r'
      case 17: // '\021'
      case 18: // '\022'
      case 27: // '\033'
        s = ( (String) obj).trim();
        break;

      case 14: // '\016'
        s = ( (Person) obj).a('^');
        break;

      case 11: // '\013'
        try {
          s = ( (DDate) obj).toDICOMString();
        }
        catch (ClassCastException classcastexception) {
          s = ( (DDateRange) obj).toDICOMString();
        }
        break;

      case 28: // '\034'
        try {
          s = ( (DDateTime) obj).toDICOMString();
        }
        catch (ClassCastException classcastexception1) {
          s = ( (DDateTimeRange) obj).toDICOMString();
        }
        break;

      case 12: // '\f'
        try {
          s = ( (DTime) obj).toDICOMString();
        }
        catch (ClassCastException classcastexception2) {
          s = ( (DTimeRange) obj).toDICOMString();
        }
        break;

      case 16: // '\020'
      case 26: // '\032'
        s = ( (Float) obj).toString();
        break;

      case 3: // '\003'
      case 15: // '\017'
      case 21: // '\025'
        s = ( (Integer) obj).toString();
        break;

      case 23: // '\027'
        s = ( (Short) obj).toString();
        break;

      case 1: // '\001'
      case 19: // '\023'
        s = ( (Long) obj).toString();
        break;

      case 20: // '\024'
        s = ( (Double) obj).toString();
        break;

      case 0: // '\0'
        s = new String( (byte[]) obj);
        break;

      case 8: // '\b'
      case 22: // '\026'
      case 24: // '\030'
        s = "";
        int j = ( (byte[]) obj).length;
        if (j > 12) {
          j = 12;
        }
        for (int k = 0; k < j; k++) {
          if (k != j - 1) {
            s = s + DumpUtils.a( ( (int) ( ( (byte[]) obj)[k])), 2) + "\\";
          }
          else {
            s = s + DumpUtils.a( ( (int) ( ( (byte[]) obj)[k])), 2);

          }
        }
        if (j == 12) {
          s = s + "...";
        }
        break;

      case 5: // '\005'
        int l = ( (Integer) obj).intValue() >> 16;
        int i1 = ( (Integer) obj).intValue() & 0xffff;
        s = "(" + DumpUtils.a(l, 4) + "," + DumpUtils.a(i1, 4) + ")";
        break;

      case 10: // '\n'
      case 25: // '\031'
      default:
        return null;
    }
    return s;
  }

  static int b(Object obj, int i) throws DicomException {
    if (obj == null) {
      return 0x7fffffff;
    }
    switch (i) {
      case 23: // '\027'
        return ( (Short) obj).intValue();

      case 3: // '\003'
      case 5: // '\005'
      case 15: // '\017'
      case 21: // '\025'
        return ( (Integer) obj).intValue();

      case 1: // '\001'
      case 19: // '\023'
        return ( (Long) obj).intValue();

      case 2: // '\002'
      case 4: // '\004'
      case 6: // '\006'
      case 7: // '\007'
      case 8: // '\b'
      case 9: // '\t'
      case 10: // '\n'
      case 11: // '\013'
      case 12: // '\f'
      case 13: // '\r'
      case 14: // '\016'
      case 16: // '\020'
      case 17: // '\021'
      case 18: // '\022'
      case 20: // '\024'
      case 22: // '\026'
      default:
        throw new DicomException("unable to convert " + DDict.a(i) + " to int");
    }
  }
}
