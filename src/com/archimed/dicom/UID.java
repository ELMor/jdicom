// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package com.archimed.dicom:
//            IllegalValueException, UIDEntry, UnknownUIDException, DicomException, 
//            TransferSyntax, SOPClass, MetaSOPClass, SOPInstance

public class UID
{

    public static final int UnknownUID = 0;
    public static final int DICOMApplicationContextName = 20481;
    static Hashtable a;

    public UID()
    {
    }

    public static UIDEntry getUIDEntry(int i)
        throws IllegalValueException
    {
        UIDEntry uidentry = (UIDEntry)a.get(((Object) (new Integer(i))));
        if(uidentry == null)
            throw new IllegalValueException("UID not found");
        else
            return uidentry;
    }

    public static UIDEntry getUIDEntry(String s)
        throws UnknownUIDException
    {
        for(Enumeration enumeration = a.elements(); enumeration.hasMoreElements();)
        {
            UIDEntry uidentry = (UIDEntry)enumeration.nextElement();
            if(uidentry.getValue().equals(((Object) (s))))
                return uidentry;
        }

        throw new UnknownUIDException(s, "Unknown UID: '" + s + "'");
    }

    static UIDEntry a(String s)
        throws UnknownUIDException
    {
        for(Enumeration enumeration = a.elements(); enumeration.hasMoreElements();)
        {
            UIDEntry uidentry = (UIDEntry)enumeration.nextElement();
            if(uidentry.getShortName().equals(((Object) (s))))
                return uidentry;
        }

        throw new UnknownUIDException(s, "Unknown UID: '" + s + "'");
    }

    public static void addEntry(int i, UIDEntry uidentry)
        throws DicomException
    {
        Integer integer = new Integer(i);
        if(a.contains(((Object) (integer))))
        {
            throw new DicomException("Entry with id = 0x" + Integer.toHexString(i) + " already present in UID");
        } else
        {
            a.put(((Object) (integer)), ((Object) (uidentry)));
            return;
        }
    }

    public static String toString(int i)
        throws IllegalValueException
    {
        return getUIDEntry(i).getValue();
    }

    static 
    {
        a = new Hashtable();
        TransferSyntax.a();
        SOPClass.a();
        MetaSOPClass.a();
        SOPInstance.a();
        a.put(((Object) (new Integer(20481))), ((Object) (new UIDEntry(20481, "1.2.840.10008.3.1.1.1", "DICOM Application Context Name", "AC", 5))));
    }
}
