// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.util.Hashtable;

// Referenced classes of package com.archimed.dicom:
//            UID, UIDEntry

public class SOPClass extends UID
{

    public static final int Verification = 4097;
    public static final int MediaStorageDirectoryStorage = 4098;
    public static final int BasicStudyContentNotification = 4099;
    public static final int StorageCommitmentPushModel = 4100;
    public static final int StorageCommitmentPullModel = 4101;
    public static final int DetachedPatientManagement = 4102;
    public static final int DetachedVisitManagement = 4103;
    public static final int DetachedStudyManagement = 4104;
    public static final int StudyComponentManagement = 4105;
    public static final int DetachedResultsManagement = 4106;
    public static final int DetachedInterpretationManagement = 4107;
    public static final int BasicFilmSession = 4108;
    public static final int BasicFilmBox = 4109;
    public static final int BasicGrayscaleImageBox = 4110;
    public static final int BasicColorImageBox = 4111;
    public static final int ReferencedImageBox = 4112;
    public static final int PrintJob = 4113;
    public static final int BasicAnnotationBox = 4114;
    public static final int Printer = 4115;
    public static final int VOILUTBox = 4116;
    public static final int ImageOverlayBox = 4117;
    public static final int ComputedRadiographyImageStorage = 4118;
    public static final int CTImageStorage = 4119;
    public static final int UltrasoundMultiframeImageStorage = 4120;
    public static final int MRImageStorage = 4121;
    public static final int UltrasoundImageStorage = 4122;
    public static final int SecondaryCaptureImageStorage = 4123;
    public static final int StandaloneOverlayStorage = 4124;
    public static final int StandaloneCurveStorage = 4125;
    public static final int StandaloneModalityLUTStorage = 4126;
    public static final int StandaloneVOILUTStorage = 4127;
    public static final int XRayAngiographicImageStorage = 4128;
    public static final int XRayRadiofluoroscopicImageStorage = 4129;
    public static final int XRayAngiographicBiPlaneImageStorage = 4130;
    public static final int NuclearMedicineImageStorage = 4131;
    public static final int PatientRootQueryRetrieveInformationModelFIND = 4132;
    public static final int PatientRootQueryRetrieveInformationModelMOVE = 4133;
    public static final int PatientRootQueryRetrieveInformationModelGET = 4134;
    public static final int StudyRootQueryRetrieveInformationModelFIND = 4135;
    public static final int StudyRootQueryRetrieveInformationModelMOVE = 4136;
    public static final int StudyRootQueryRetrieveInformationModelGET = 4137;
    public static final int PatientStudyOnlyQueryRetrieveInformationModelFIND = 4138;
    public static final int PatientStudyOnlyQueryRetrieveInformationModelMOVE = 4139;
    public static final int PatientStudyOnlyQueryRetrieveInformationModelGET = 4140;
    public static final int ModalityWorklistInformationModelFIND = 4141;
    public static final int ModalityPerformedProcedureStep = 4142;
    public static final int ModalityPerformedProcedureStepRetrieve = 4143;
    public static final int ModalityPerformedProcedureStepNotification = 4144;
    public static final int PresentationLUT = 4145;
    public static final int PrintQueueManagement = 4146;
    public static final int StoredPrintStorage = 4147;
    public static final int HardcopyGrayscaleImageStorage = 4148;
    public static final int HardcopyColorImageStorage = 4149;
    public static final int PullPrintRequest = 4150;
    public static final int PositronEmissionTomographyImageStorage = 4151;
    public static final int StandalonePETCurveStorage = 4152;
    public static final int RTImageStorage = 4153;
    public static final int RTDoseStorage = 4154;
    public static final int RTStructureSetStorage = 4155;
    public static final int RTPlanStorage = 4156;
    public static final int UltrasoundMultiframeImageStorage_Retired = 4157;
    public static final int NuclearMedicineImageStorage_Retired = 4158;
    public static final int UltrasoundImageStorage_Retired = 4159;
    public static final int BasicPrintImageOverlayBox = 4160;
    public static final int VLEndoscopicImageStorage = 4161;
    public static final int VLMicroscopicImageStorage = 4162;
    public static final int VLSlideCoordinatesMicroscopicImageStorage = 4163;
    public static final int VLPhotographicImageStorage = 4164;
    public static final int GrayscaleSoftcopyPresentationState = 4165;
    public static final int BasicTextSR = 4166;
    public static final int EnhancedSR = 4167;
    public static final int ComprehensiveSR = 4168;
    public static final int TwelveLeadECGWaveformStorage = 4169;
    public static final int GeneralECGWaveformStorage = 4170;
    public static final int AmbulatoryECGWaveformStorage = 4171;
    public static final int HemodynamicWaveformStorage = 4172;
    public static final int CardiacElectrophysiologyWaveformStorage = 4173;
    public static final int BasicVoiceAudioWaveformStorage = 4174;
    public static final int PrinterConfigurationRetrieval = 4175;
    public static final int DXImageStorageForPresentation = 4176;
    public static final int DXImageStorageForProcessing = 4177;
    public static final int DXMammographyImageStorageForPresentation = 4178;
    public static final int DXMammographyImageStorageForProcessing = 4179;
    public static final int DXIntraOralImageStorageForPresentation = 4180;
    public static final int DXIntraOralImageStorageForProcessing = 4181;
    public static final int RTBeamsTreatmentRecordStorage = 4182;
    public static final int RTBrachyTreatmentRecordStorage = 4183;
    public static final int RTTreatmentRecordSummaryStorage = 4184;
    public static final int TianiMediaStorageDirectoryStorage = 4185;
    public static final int TianiSecondaryCaptureImageStorage = 4192;
    public static final int TianiStudyRootQueryRetrieveInformationModelMOVE = 4193;
    public static final int KeyObjectSelectionDocument = 4194;
    public static final int ElimpexRawDataStorage = 4195;
    public static final int RawDataStorage = 4196;
    public static final int SCNDeleteObjectTransferSyntax = 4197;

    public SOPClass()
    {
    }

    static void a()
    {
        UID.a.put(((Object) (new Integer(4097))), ((Object) (new UIDEntry(4097, "1.2.840.10008.1.1", "Verification SOP Class", "VE", 1))));
        UID.a.put(((Object) (new Integer(4098))), ((Object) (new UIDEntry(4098, "1.2.840.10008.1.3.10", "Media Storage Directory Storage SOP Class", "MS", 1))));
        UID.a.put(((Object) (new Integer(4099))), ((Object) (new UIDEntry(4099, "1.2.840.10008.1.9", "Basic Study Content Notification SOP Class", "CN", 1))));
        UID.a.put(((Object) (new Integer(4100))), ((Object) (new UIDEntry(4100, "1.2.840.10008.1.20.1", "Storage Commitment Push Model SOP Class", "SP", 1))));
        UID.a.put(((Object) (new Integer(4101))), ((Object) (new UIDEntry(4101, "1.2.840.10008.1.20.2", "Storage Commitment Pull Model SOP Class", "SP", 1))));
        UID.a.put(((Object) (new Integer(4102))), ((Object) (new UIDEntry(4102, "1.2.840.10008.3.1.2.1.1", "Detached Patient Management SOP Class", "PM", 1))));
        UID.a.put(((Object) (new Integer(4103))), ((Object) (new UIDEntry(4103, "1.2.840.10008.3.1.2.2.1", "Detached Visit Management SOP Class", "VM", 1))));
        UID.a.put(((Object) (new Integer(4104))), ((Object) (new UIDEntry(4104, "1.2.840.10008.3.1.2.3.1", "Detached Study Management SOP Class", "SM", 1))));
        UID.a.put(((Object) (new Integer(4105))), ((Object) (new UIDEntry(4105, "1.2.840.10008.3.1.2.3.2", "Study Component Management SOP Class", "SM", 1))));
        UID.a.put(((Object) (new Integer(4106))), ((Object) (new UIDEntry(4106, "1.2.840.10008.3.1.2.5.1", "Detached Results Management SOP Class", "RM", 1))));
        UID.a.put(((Object) (new Integer(4107))), ((Object) (new UIDEntry(4107, "1.2.840.10008.3.1.2.6.1", "Detached Interpretation Management SOP Class", "IM", 1))));
        UID.a.put(((Object) (new Integer(4108))), ((Object) (new UIDEntry(4108, "1.2.840.10008.5.1.1.1", "Basic Film Session SOP Class", "FS", 1))));
        UID.a.put(((Object) (new Integer(4109))), ((Object) (new UIDEntry(4109, "1.2.840.10008.5.1.1.2", "Basic Film Box SOP Class", "FB", 1))));
        UID.a.put(((Object) (new Integer(4110))), ((Object) (new UIDEntry(4110, "1.2.840.10008.5.1.1.4", "Basic Grayscale Image Box SOP Class", "GI", 1))));
        UID.a.put(((Object) (new Integer(4111))), ((Object) (new UIDEntry(4111, "1.2.840.10008.5.1.1.4.1", "Basic Color ImageBox SOP Class", "CI", 1))));
        UID.a.put(((Object) (new Integer(4112))), ((Object) (new UIDEntry(4112, "1.2.840.10008.5.1.1.4.2", "Referenced Image Box SOP Class", "RI", 1))));
        UID.a.put(((Object) (new Integer(4113))), ((Object) (new UIDEntry(4113, "1.2.840.10008.5.1.1.14", "Print Job SOP Class", "PJ", 1))));
        UID.a.put(((Object) (new Integer(4114))), ((Object) (new UIDEntry(4114, "1.2.840.10008.5.1.1.15", "Basic Annotation Box SOP Class", "AB", 1))));
        UID.a.put(((Object) (new Integer(4115))), ((Object) (new UIDEntry(4115, "1.2.840.10008.5.1.1.16", "Printer SOP Class", "P ", 1))));
        UID.a.put(((Object) (new Integer(4116))), ((Object) (new UIDEntry(4116, "1.2.840.10008.5.1.1.22", "VOI LUT Box SOP Class", "VL", 1))));
        UID.a.put(((Object) (new Integer(4117))), ((Object) (new UIDEntry(4117, "1.2.840.10008.5.1.1.24", "Image Overlay Box SOP Class", "IO", 1))));
        UID.a.put(((Object) (new Integer(4118))), ((Object) (new UIDEntry(4118, "1.2.840.10008.5.1.4.1.1.1", "Computed Radiography Image Storage SOP Class", "CR", 1))));
        UID.a.put(((Object) (new Integer(4119))), ((Object) (new UIDEntry(4119, "1.2.840.10008.5.1.4.1.1.2", "CT Image Storage SOP Class", "CT", 1))));
        UID.a.put(((Object) (new Integer(4120))), ((Object) (new UIDEntry(4120, "1.2.840.10008.5.1.4.1.1.3.1", "Ultrasound Multi-frame Image Storage SOP Class", "US", 1))));
        UID.a.put(((Object) (new Integer(4121))), ((Object) (new UIDEntry(4121, "1.2.840.10008.5.1.4.1.1.4", "MR Image Storage SOP Class", "MR", 1))));
        UID.a.put(((Object) (new Integer(4122))), ((Object) (new UIDEntry(4122, "1.2.840.10008.5.1.4.1.1.6.1", "Ultrasound Image Storage SOP Class", "US", 1))));
        UID.a.put(((Object) (new Integer(4123))), ((Object) (new UIDEntry(4123, "1.2.840.10008.5.1.4.1.1.7", "Secondary Capture Image Storage SOP Class", "SC", 1))));
        UID.a.put(((Object) (new Integer(4124))), ((Object) (new UIDEntry(4124, "1.2.840.10008.5.1.4.1.1.8", "Standalone Overlay Storage SOP Class", "SO", 1))));
        UID.a.put(((Object) (new Integer(4125))), ((Object) (new UIDEntry(4125, "1.2.840.10008.5.1.4.1.1.9", "Standalone Curve Storage SOP Class", "SC", 1))));
        UID.a.put(((Object) (new Integer(4126))), ((Object) (new UIDEntry(4126, "1.2.840.10008.5.1.4.1.1.10", "Standalone Modality LUT Storage SOP Class", "ML", 1))));
        UID.a.put(((Object) (new Integer(4127))), ((Object) (new UIDEntry(4127, "1.2.840.10008.5.1.4.1.1.11", "Standalone VOI LUT Storage SOP Class", "VL", 1))));
        UID.a.put(((Object) (new Integer(4128))), ((Object) (new UIDEntry(4128, "1.2.840.10008.5.1.4.1.1.12.1", "X-Ray Angiographic Image Storage SOP Class", "XA", 1))));
        UID.a.put(((Object) (new Integer(4129))), ((Object) (new UIDEntry(4129, "1.2.840.10008.5.1.4.1.1.12.2", "X-Ray Radiofluoroscopic Image Storage SOP Class", "XR", 1))));
        UID.a.put(((Object) (new Integer(4130))), ((Object) (new UIDEntry(4130, "1.2.840.10008.5.1.4.1.1.12.3", "X-Ray Angiographic Bi-Plane Image Storage SOP Class", "XA", 1))));
        UID.a.put(((Object) (new Integer(4131))), ((Object) (new UIDEntry(4131, "1.2.840.10008.5.1.4.1.1.20", "Nuclear Medicine Image Storage SOP Class", "NM", 1))));
        UID.a.put(((Object) (new Integer(4132))), ((Object) (new UIDEntry(4132, "1.2.840.10008.5.1.4.1.2.1.1", "Patient Root Query/Retrieve Information Model - FIND SOP Class", "PF", 1))));
        UID.a.put(((Object) (new Integer(4133))), ((Object) (new UIDEntry(4133, "1.2.840.10008.5.1.4.1.2.1.2", "Patient Root Query/Retrieve Information Model - MOVE SOP Class", "PM", 1))));
        UID.a.put(((Object) (new Integer(4134))), ((Object) (new UIDEntry(4134, "1.2.840.10008.5.1.4.1.2.1.3", "Patient Root Query/Retrieve Information Model - GET SOP Class", "PG", 1))));
        UID.a.put(((Object) (new Integer(4135))), ((Object) (new UIDEntry(4135, "1.2.840.10008.5.1.4.1.2.2.1", "Study Root Query/Retrieve Information Model - FIND SOP Class", "SF", 1))));
        UID.a.put(((Object) (new Integer(4136))), ((Object) (new UIDEntry(4136, "1.2.840.10008.5.1.4.1.2.2.2", "Study Root Query/Retrieve Information Model - MOVE SOP Class", "SM", 1))));
        UID.a.put(((Object) (new Integer(4137))), ((Object) (new UIDEntry(4137, "1.2.840.10008.5.1.4.1.2.2.3", "Study Root Query/Retrieve Information Model - GET SOP Class", "SG", 1))));
        UID.a.put(((Object) (new Integer(4138))), ((Object) (new UIDEntry(4138, "1.2.840.10008.5.1.4.1.2.3.1", "Patient/Study Only Query/Retrieve Information Model - FIND SOP Class", "F ", 1))));
        UID.a.put(((Object) (new Integer(4139))), ((Object) (new UIDEntry(4139, "1.2.840.10008.5.1.4.1.2.3.2", "Patient/Study Only Query/Retrieve Information Model - MOVE SOP Class", "M ", 1))));
        UID.a.put(((Object) (new Integer(4140))), ((Object) (new UIDEntry(4140, "1.2.840.10008.5.1.4.1.2.3.3", "Patient/Study Only Query/Retrieve Information Model - GET SOP Class", "G ", 1))));
        UID.a.put(((Object) (new Integer(4141))), ((Object) (new UIDEntry(4141, "1.2.840.10008.5.1.4.31", "Modality Worklist Information Model - FIND SOP Class", "MW", 1))));
        UID.a.put(((Object) (new Integer(4142))), ((Object) (new UIDEntry(4142, "1.2.840.10008.3.1.2.3.3", "Modality Performed Procedure Step SOP Class", "MP", 1))));
        UID.a.put(((Object) (new Integer(4143))), ((Object) (new UIDEntry(4143, "1.2.840.10008.3.1.2.3.4", "Modality Performed Procedure Step Retrieve  SOP Class", "PR", 1))));
        UID.a.put(((Object) (new Integer(4144))), ((Object) (new UIDEntry(4144, "1.2.840.10008.3.1.2.3.5", "Modality Performed Procedure Step Notification SOP Class", "PN", 1))));
        UID.a.put(((Object) (new Integer(4145))), ((Object) (new UIDEntry(4145, "1.2.840.10008.5.1.1.23", "Presentation LUT SOP Class", "PL", 1))));
        UID.a.put(((Object) (new Integer(4146))), ((Object) (new UIDEntry(4146, "1.2.840.10008.5.1.1.26", "Print Queue Management SOP Class", "PQ", 1))));
        UID.a.put(((Object) (new Integer(4147))), ((Object) (new UIDEntry(4147, "1.2.840.10008.5.1.1.27", "Stored Print Storage SOP Class", "SP", 1))));
        UID.a.put(((Object) (new Integer(4148))), ((Object) (new UIDEntry(4148, "1.2.840.10008.5.1.1.29", "Hardcopy Grayscale Image Storage SOP Class", "HG", 1))));
        UID.a.put(((Object) (new Integer(4149))), ((Object) (new UIDEntry(4149, "1.2.840.10008.5.1.1.30", "Hardcopy Color Image Storage SOP Class", "HC", 1))));
        UID.a.put(((Object) (new Integer(4150))), ((Object) (new UIDEntry(4150, "1.2.840.10008.5.1.1.31", "Pull Print Request SOP Class", "PP", 1))));
        UID.a.put(((Object) (new Integer(4151))), ((Object) (new UIDEntry(4151, "1.2.840.10008.5.1.4.1.1.128", "Positron Emission Tomography Image Storage SOP Class", "PE", 1))));
        UID.a.put(((Object) (new Integer(4152))), ((Object) (new UIDEntry(4152, "1.2.840.10008.5.1.4.1.1.129", "Standalone PET Curve Storage SOP Class", "PC", 1))));
        UID.a.put(((Object) (new Integer(4153))), ((Object) (new UIDEntry(4153, "1.2.840.10008.5.1.4.1.1.481.1", "RT Image Storage SOP Class", "RI", 1))));
        UID.a.put(((Object) (new Integer(4154))), ((Object) (new UIDEntry(4154, "1.2.840.10008.5.1.4.1.1.481.2", "RT Dose Storage SOP Class", "RD", 1))));
        UID.a.put(((Object) (new Integer(4155))), ((Object) (new UIDEntry(4155, "1.2.840.10008.5.1.4.1.1.481.3", "RT Structure Set Storage SOP Class", "RS", 1))));
        UID.a.put(((Object) (new Integer(4156))), ((Object) (new UIDEntry(4156, "1.2.840.10008.5.1.4.1.1.481.5", "RT Plan Storage SOP Class", "RP", 1))));
        UID.a.put(((Object) (new Integer(4157))), ((Object) (new UIDEntry(4157, "1.2.840.10008.5.1.4.1.1.3", "Ultrasound Multi-frame Image Storage Retired SOP Class", "UM", 1))));
        UID.a.put(((Object) (new Integer(4158))), ((Object) (new UIDEntry(4158, "1.2.840.10008.5.1.4.1.1.5", "Nuclear Medicine Image Storage Retired SOP Class", "NM", 1))));
        UID.a.put(((Object) (new Integer(4159))), ((Object) (new UIDEntry(4159, "1.2.840.10008.5.1.4.1.1.6", "Ultrasound Image Storage Retired SOP Class", "US", 1))));
        UID.a.put(((Object) (new Integer(4160))), ((Object) (new UIDEntry(4160, "1.2.840.10008.5.1.1.24.1", "Basic Print Image Overlay Box SOP Class", "OB", 1))));
        UID.a.put(((Object) (new Integer(4161))), ((Object) (new UIDEntry(4161, "1.2.840.10008.5.1.4.1.1.77.1.1", "Visible Light Endoscopic Image Storage SOP Class", "VL", 1))));
        UID.a.put(((Object) (new Integer(4162))), ((Object) (new UIDEntry(4162, "1.2.840.10008.5.1.4.1.1.77.1.2", "Visible Light Microscopic Image Storage SOP Class", "VL", 1))));
        UID.a.put(((Object) (new Integer(4163))), ((Object) (new UIDEntry(4163, "1.2.840.10008.5.1.4.1.1.77.1.3", "Visible Light Slide Coordinates Microscopic Image Storage SOP Class", "VL", 1))));
        UID.a.put(((Object) (new Integer(4164))), ((Object) (new UIDEntry(4164, "1.2.840.10008.5.1.4.1.1.77.1.4", "Visible Light Photographic Image Storage SOP Class", "US", 1))));
        UID.a.put(((Object) (new Integer(4165))), ((Object) (new UIDEntry(4165, "1.2.840.10008.5.1.4.1.1.11.1", "Grayscale Softcopy Presentation State SOP Class", "PS", 1))));
        UID.a.put(((Object) (new Integer(4166))), ((Object) (new UIDEntry(4166, "1.2.840.10008.5.1.4.1.1.88.11", "Basic Text SR SOP Class", "BT", 1))));
        UID.a.put(((Object) (new Integer(4167))), ((Object) (new UIDEntry(4167, "1.2.840.10008.5.1.4.1.1.88.22", "Enhanced SR SOP Class", "ES", 1))));
        UID.a.put(((Object) (new Integer(4168))), ((Object) (new UIDEntry(4168, "1.2.840.10008.5.1.4.1.1.88.33", "Comprehensive SR SOP Class", "CS", 1))));
        UID.a.put(((Object) (new Integer(4169))), ((Object) (new UIDEntry(4169, "1.2.840.10008.5.1.4.1.1.9.1.1", "12-lead ECG Waveform Storage SOP Class", "TW", 1))));
        UID.a.put(((Object) (new Integer(4170))), ((Object) (new UIDEntry(4170, "1.2.840.10008.5.1.4.1.1.9.1.2", "General ECG Waveform Storage SOP Class", "GW", 1))));
        UID.a.put(((Object) (new Integer(4171))), ((Object) (new UIDEntry(4171, "1.2.840.10008.5.1.4.1.1.9.1.3", "Ambulatory ECG Waveform Storage SOP Class", "AW", 1))));
        UID.a.put(((Object) (new Integer(4172))), ((Object) (new UIDEntry(4172, "1.2.840.10008.5.1.4.1.1.9.2.1", "Hemodynamic Waveform Storage SOP Class", "HW", 1))));
        UID.a.put(((Object) (new Integer(4173))), ((Object) (new UIDEntry(4173, "1.2.840.10008.5.1.4.1.1.9.3.1", "Cardiac Electrophysiology Waveform Storage SOP Class", "CW", 1))));
        UID.a.put(((Object) (new Integer(4174))), ((Object) (new UIDEntry(4174, "1.2.840.10008.5.1.4.1.1.9.4.1", "Basic Voice Audio Waveform Storage SOP Class", "VW", 1))));
        UID.a.put(((Object) (new Integer(4175))), ((Object) (new UIDEntry(4175, "1.2.840.10008.5.1.1.16.376", "Printer Configuration Retrieval SOP Class", "CP", 1))));
        UID.a.put(((Object) (new Integer(4176))), ((Object) (new UIDEntry(4176, "1.2.840.10008.5.1.4.1.1.1.1", "Digital X-Ray Image Storage - For Presentation", "X ", 1))));
        UID.a.put(((Object) (new Integer(4177))), ((Object) (new UIDEntry(4177, "1.2.840.10008.5.1.4.1.1.1.1.1", "Digital X-Ray Image Storage - For Processing", "XP", 1))));
        UID.a.put(((Object) (new Integer(4178))), ((Object) (new UIDEntry(4178, "1.2.840.10008.5.1.4.1.1.1.2", "Digital Mammography Image Storage - For Presentation", "M ", 1))));
        UID.a.put(((Object) (new Integer(4179))), ((Object) (new UIDEntry(4179, "1.2.840.10008.5.1.4.1.1.1.2.1", "Digital Mammography Image Storage - For Processing", "MP", 1))));
        UID.a.put(((Object) (new Integer(4180))), ((Object) (new UIDEntry(4180, "1.2.840.10008.5.1.4.1.1.1.3", "Digital Intra-oral X-Ray Image Storage - For Presentation", "O ", 1))));
        UID.a.put(((Object) (new Integer(4181))), ((Object) (new UIDEntry(4181, "1.2.840.10008.5.1.4.1.1.1.3.1", "Digital Intra-oral X-Ray Image Storage - For Processing", "OP", 1))));
        UID.a.put(((Object) (new Integer(4182))), ((Object) (new UIDEntry(4182, "1.2.840.10008.5.1.4.1.1.481.4", "RT Beams Treatment Record Storage", "T1", 1))));
        UID.a.put(((Object) (new Integer(4183))), ((Object) (new UIDEntry(4183, "1.2.840.10008.5.1.4.1.1.481.6", "RT Brachy Treatment Record Storage", "T2", 1))));
        UID.a.put(((Object) (new Integer(4184))), ((Object) (new UIDEntry(4184, "1.2.840.10008.5.1.4.1.1.481.7", "RT Treatment Summary Record Storage", "T3", 1))));
        UID.a.put(((Object) (new Integer(4185))), ((Object) (new UIDEntry(4185, "1.2.40.0.13.0.1.3.10", "Tiani Media Storage Directory Storage SOP Class", "MS", 1))));
        UID.a.put(((Object) (new Integer(4192))), ((Object) (new UIDEntry(4192, "1.2.40.0.13.0.5.1.4.1.1.7", "Tiani Secondary Capture Image Storage SOP Class", "SC", 1))));
        UID.a.put(((Object) (new Integer(4193))), ((Object) (new UIDEntry(4193, "1.2.40.0.13.0.5.1.4.1.2.2.2", "Tiani Study Root Query/Retrieve Information Model - MOVE SOP Class", "SM", 1))));
        UID.a.put(((Object) (new Integer(4194))), ((Object) (new UIDEntry(4194, "1.2.840.10008.5.1.4.1.1.88.59", "Key Object Selection Document", "KO", 1))));
        UID.a.put(((Object) (new Integer(4195))), ((Object) (new UIDEntry(4195, "1.2.826.0.1.3680043.2.242.1.1", "Elimpex Raw Data Storage", "EX", 1))));
        UID.a.put(((Object) (new Integer(4196))), ((Object) (new UIDEntry(4196, "1.2.840.10008.5.1.4.1.1.66", "Raw Data Storage", "RW", 1))));
        UID.a.put(((Object) (new Integer(4197))), ((Object) (new UIDEntry(4197, "1.2.40.0.13.0.1.9", "Scn Delete Object", "SD", 1))));
    }
}
