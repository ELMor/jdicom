// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.archimed.dicom:
//            OffsetInputStream

class h extends OffsetInputStream
{

    int a;
    int b;
    byte c[];
    byte d[];
    byte e[];
    byte f[][];

    h(InputStream inputstream)
    {
        super(inputstream);
        a = 8193;
        b = 0;
        c = new byte[2];
        d = new byte[4];
        e = new byte[8];
        f = (new byte[][] {
            c, d, new byte[6], e, new byte[10], new byte[12], new byte[14], new byte[16]
        });
    }

    h(InputStream inputstream, int i)
    {
        super(inputstream, i);
        a = 8193;
        b = 0;
        c = new byte[2];
        d = new byte[4];
        e = new byte[8];
        f = (new byte[][] {
            c, d, new byte[6], e, new byte[10], new byte[12], new byte[14], new byte[16]
        });
    }

    private int a(byte byte0)
    {
        return byte0 & 0xff;
    }

    void c(int i)
    {
        a = i;
    }

    int j()
    {
        return a;
    }

    int k()
        throws IOException
    {
        ((OffsetInputStream)this).readFully(c);
        if(a == 8195)
            return (a(c[0]) << 8) + a(c[1]);
        else
            return (a(c[1]) << 8) + a(c[0]);
    }

    int l()
        throws IOException
    {
        ((OffsetInputStream)this).readFully(c);
        if(a == 8195)
            return (c[0] << 8) + a(c[1]);
        else
            return (c[1] << 8) + a(c[0]);
    }

    int m()
        throws IOException
    {
        ((OffsetInputStream)this).readFully(d);
        if(a == 8195)
        {
            b = a(d[3]);
            b += a(d[2]) << 8;
            b += a(d[1]) << 16;
            b += a(d[0]) << 24;
        } else
        {
            b = a(d[0]);
            b += a(d[1]) << 8;
            b += a(d[2]) << 16;
            b += a(d[3]) << 24;
        }
        return b;
    }

    int n()
        throws IOException
    {
        ((OffsetInputStream)this).readFully(d);
        if(a == 8195)
        {
            b = a(d[3]);
            b += a(d[2]) << 8;
            b += a(d[1]) << 16;
            b += d[0] << 24;
        } else
        {
            b = a(d[0]);
            b += a(d[1]) << 8;
            b += a(d[2]) << 16;
            b += d[3] << 24;
        }
        return b;
    }

    long o()
        throws IOException
    {
        ((OffsetInputStream)this).readFully(e);
        long l1;
        if(a == 8195)
        {
            l1 = a(e[7]);
            l1 += a(e[6]) << 8;
            l1 += a(e[5]) << 16;
            l1 += a(e[4]) << 24;
            l1 += (long)a(e[3]) << 32;
            l1 += (long)a(e[2]) << 40;
            l1 += (long)a(e[1]) << 48;
            l1 += (long)a(e[0]) << 56;
        } else
        {
            l1 = a(e[0]);
            l1 += a(e[1]) << 8;
            l1 += a(e[2]) << 16;
            l1 += a(e[3]) << 24;
            l1 += (long)a(e[4]) << 32;
            l1 += (long)a(e[5]) << 40;
            l1 += (long)a(e[6]) << 48;
            l1 += (long)a(e[7]) << 56;
        }
        return l1;
    }

    String d(int i)
        throws IOException
    {
        byte abyte0[] = i <= 16 ? f[(i >> 1) - 1] : new byte[i];
        ((OffsetInputStream)this).readFully(abyte0);
        return new String(abyte0);
    }
}
