// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;

// Referenced classes of package com.archimed.dicom:
//            DDate

public class DDateRange
{

    DDate a;
    DDate b;

    public DDateRange(DDate ddate, DDate ddate1)
    {
        if(ddate != null)
            a = ddate;
        else
            a = new DDate();
        if(ddate1 != null)
            b = ddate1;
        else
            b = new DDate();
    }

    public DDateRange(String s)
        throws NumberFormatException
    {
        int i = s.indexOf('-');
        if(i == -1)
        {
            throw new NumberFormatException(s + " cannot parse this string into DDateRange");
        } else
        {
            a = new DDate(s.substring(0, i));
            b = new DDate(s.substring(i + 1));
            return;
        }
    }

    public String toString()
    {
        return a.toString() + " - " + b.toString();
    }

    public String toString(DateFormat dateformat)
    {
        return a.toString(dateformat) + " - " + b.toString(dateformat);
    }

    public String toDICOMString()
    {
        return a.toDICOMString() + "-" + b.toDICOMString();
    }

    public DDate getDate1()
    {
        if(!a.isEmpty())
            return a;
        else
            return null;
    }

    public DDate getDate2()
    {
        if(!b.isEmpty())
            return b;
        else
            return null;
    }
}
