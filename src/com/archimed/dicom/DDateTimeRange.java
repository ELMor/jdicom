// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.text.DateFormat;

// Referenced classes of package com.archimed.dicom:
//            DDateTime

public class DDateTimeRange
{

    DDateTime a;
    DDateTime b;

    public DDateTimeRange(DDateTime ddatetime, DDateTime ddatetime1)
    {
        if(ddatetime != null)
            a = ddatetime;
        else
            a = new DDateTime();
        if(ddatetime1 != null)
            b = ddatetime1;
        else
            b = new DDateTime();
    }

    public DDateTimeRange(String s)
        throws NumberFormatException
    {
        int i = s.indexOf('-');
        if(i == -1)
        {
            throw new NumberFormatException(s + " cannot parse this string into DDateTimeRange");
        } else
        {
            a = new DDateTime(s.substring(0, i));
            b = new DDateTime(s.substring(i + 1));
            return;
        }
    }

    public String toString()
    {
        return a.toString() + " - " + b.toString();
    }

    public String toString(DateFormat dateformat)
    {
        return a.toString(dateformat) + " - " + b.toString(dateformat);
    }

    public String toDICOMString()
    {
        return a.toDICOMString() + "-" + b.toDICOMString();
    }

    public DDateTime getDateTime1()
    {
        if(!a.isEmpty())
            return a;
        else
            return null;
    }

    public DDateTime getDateTime2()
    {
        if(!b.isEmpty())
            return b;
        else
            return null;
    }
}
