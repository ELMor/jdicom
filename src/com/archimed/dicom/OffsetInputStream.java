// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.dicom;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

class OffsetInputStream extends FilterInputStream
{

    private long a;
    private long b;
    protected long counter;

    public OffsetInputStream(InputStream inputstream)
    {
        this(inputstream, 0);
    }

    public OffsetInputStream(InputStream inputstream, int i)
    {
        super(inputstream);
        a = 0L;
        counter = 0L;
        a = i;
    }

    public int read()
        throws IOException
    {
        a++;
        return super.in.read();
    }

    public int read(byte abyte0[])
        throws IOException
    {
        return read(abyte0, 0, abyte0.length);
    }

    public int read(byte abyte0[], int i, int j)
        throws IOException
    {
        b = super.in.read(abyte0, i, j);
        a += (long)i + b;
        return (int)b;
    }

    public long skip(long l)
        throws IOException
    {
        b = super.in.skip(l);
        a += b;
        return b;
    }

    public boolean markSupported()
    {
        return false;
    }

    public void readFully(byte abyte0[])
        throws IOException
    {
        readFully(abyte0, 0, abyte0.length);
    }

    public void readFully(byte abyte0[], int i, int j)
        throws IOException
    {
        a += i + j;
        InputStream inputstream = super.in;
        int l;
        for(int k = 0; k < j; k += l)
        {
            l = inputstream.read(abyte0, i + k, j - k);
            if(l < 0)
                throw new EOFException();
        }

    }

    public int skipBytes(int i)
        throws IOException
    {
        InputStream inputstream = super.in;
        for(int j = 0; j < i; j += (int)inputstream.skip(i - j));
        a += i;
        return i;
    }

    public long getOffset()
    {
        return a;
    }

    protected void resetCounter()
    {
        counter = a;
    }

    protected long getCounter()
    {
        if(a > counter)
            return a - counter;
        else
            return 0L;
    }
}
