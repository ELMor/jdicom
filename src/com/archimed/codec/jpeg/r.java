// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.codec.jpeg;

import java.io.IOException;

public class r
{

    int a;
    int b;
    byte c[];
    byte d[];
    int e[];
    byte f[];
    int g[];
    int h[];
    int i[];
    int j[];
    int k[];
    static int l[] = {
        -1, 0x7fffffff, 0x3fffffff, 0x1fffffff, 0xfffffff, 0x7ffffff, 0x3ffffff, 0x1ffffff, 0xffffff, 0x7fffff, 
        0x3fffff, 0x1fffff, 0xfffff, 0x7ffff, 0x3ffff, 0x1ffff, 65535, 32767, 16383, 8191, 
        4095, 2047, 1023, 511, 255, 127, 63, 31, 15, 7, 
        3, 1
    };

    public r()
    {
        c = new byte[17];
        d = new byte[256];
        e = new int[257];
        f = new byte[257];
        g = new int[17];
        h = new int[18];
        i = new int[17];
        j = new int[256];
        k = new int[256];
    }

    public int read(byte abyte0[], int i1)
        throws IOException
    {
        boolean flag = false;
        boolean flag1 = false;
        boolean flag2 = false;
        b = abyte0[i1++] << 8;
        b += ((int) (abyte0[i1++]));
        for(b = b - 2; b > 0;)
        {
            int j1 = ((int) (abyte0[i1++]));
            c[0] = 0;
            int k1 = 0;
            for(int l1 = 1; l1 <= 16; l1++)
            {
                c[l1] = abyte0[i1++];
                k1 += ((int) (c[l1]));
            }

            if(k1 > 256)
                throw new RuntimeException("Bogus DHT counts");
            for(int l2 = 0; l2 < k1; l2++)
                d[l2] = abyte0[i1++];

            b -= 17 + k1;
            if((j1 & 0x10) == 16)
                throw new RuntimeException("Huffman table for lossless JPEG is not defined");
            a = j1;
            if(j1 < 0 || j1 >= 4)
                throw new RuntimeException("Bogus DHT index " + j1);
        }

        int i2 = 0;
        boolean flag3 = true;
        int l3 = 1;
        for(int i3 = 1; i3 <= 16; i3++)
            for(l3 = 1; l3 <= c[i3]; l3++)
                f[i2++] = (byte)i3;


        f[i2] = 0;
        int i4 = i2;
        int j4 = 0;
        int l4 = ((int) (f[0]));
        for(int j2 = 0; f[j2] != 0;)
        {
            while(f[j2] == l4) 
            {
                e[j2++] = j4;
                j4++;
            }
            j4 <<= 1;
            l4++;
        }

        l3 = 0;
        for(int j3 = 1; j3 <= 16; j3++)
            if(c[j3] != 0)
            {
                i[j3] = l3;
                g[j3] = e[l3];
                l3 += ((int) (c[j3]));
                h[j3] = e[l3 - 1];
            } else
            {
                h[j3] = -1;
            }

        for(int k2 = 0; k2 < i4; k2++)
        {
            byte byte0 = f[k2];
            if(byte0 <= 8)
            {
                byte byte1 = d[k2];
                int k4 = e[k2];
                int i5 = k4 << 8 - byte0;
                int j5;
                if(byte0 < 8)
                    j5 = i5 | l[24 + byte0];
                else
                    j5 = i5;
                for(int k3 = i5; k3 <= j5; k3++)
                {
                    j[k3] = ((int) (byte0));
                    k[k3] = ((int) (byte1));
                }

            }
        }

        return i1;
    }

}
