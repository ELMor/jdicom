// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.codec.jpeg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class w
{

    private static int a[] = {
        0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 
        1023, 2047, 4095, 8191, 16383, 32767, 65535, 0x1ffff, 0x3ffff, 0x7ffff, 
        0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff, 
        0x3fffffff, 0x7fffffff, -1
    };
    private static final byte b[] = {
        0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1
    };
    private static final byte c[] = {
        0, 2, 3, 1, 4, 5, 6, 7, 8, 9, 
        10, 11, 12, 13, 14, 15, 16
    };
    private static final int d = 216;
    private static final int e = 217;
    private static final int f = 195;
    private static final int g = 218;
    private static final int h = 196;
    private static int i[];
    private static byte j[];
    private static int k[];
    private static byte l[];
    private static int m[];
    private int n;
    private int o;
    private ByteArrayOutputStream p;

    public w()
    {
        n = 0;
        o = 0;
    }

    private void a(int i1)
        throws IOException
    {
        p.write(i1);
    }

    private void a(short word0, int i1)
        throws IOException
    {
        int k1 = i1;
        int j1 = word0 & a[k1];
        k1 += o;
        j1 <<= 24 - k1;
        j1 |= n;
        for(; k1 >= 8; k1 -= 8)
        {
            int l1 = j1 >> 16 & 0xff;
            a(l1);
            if(l1 == 255)
                a(0);
            j1 <<= 8;
        }

        n = j1;
        o = k1;
    }

    private void b(int i1)
        throws IOException
    {
        int j1 = i1 % 0x10000;
        if(j1 > 32768)
            j1 -= 0x10000;
        else
        if(j1 < -32767)
            j1 += 0x10000;
        int k1 = j1;
        if(j1 < 0)
        {
            j1 = -j1;
            k1--;
        }
        int l1 = 0;
        if(j1 != 0)
        {
            for(; j1 >= 256; j1 >>= 8)
                l1 += 8;

            l1 += m[j1 & 0xff];
        }
        a((short)k[l1], ((int) (l[l1])));
        if(l1 != 0 && l1 != 16)
            a((short)k1, l1);
    }

    private void a(int i1, OutputStream outputstream)
        throws IOException
    {
        outputstream.write(255);
        outputstream.write(i1);
    }

    private void a(int i1, int j1, int k1, int l1)
        throws IOException
    {
        p = new ByteArrayOutputStream();
        int i2 = 8 + 3 * l1;
        int j2 = 6 + 2 * l1;
        int k2 = 19;
        for(int l2 = 0; l2 < b.length; l2++)
            k2 += ((int) (b[l2]));

        DataOutputStream dataoutputstream = new DataOutputStream(((OutputStream) (p)));
        a(216, ((OutputStream) (p)));
        a(195, ((OutputStream) (p)));
        dataoutputstream.writeShort(i2);
        p.write(k1);
        dataoutputstream.writeShort(j1);
        dataoutputstream.writeShort(i1);
        p.write(l1);
        for(int i3 = 0; i3 < l1; i3++)
        {
            p.write(i3);
            p.write(17);
            p.write(0);
        }

        a(196, ((OutputStream) (p)));
        dataoutputstream.writeShort(k2);
        p.write(0);
        for(int j3 = 0; j3 < b.length; j3++)
            p.write(((int) (b[j3])));

        for(int k3 = 0; k3 < c.length; k3++)
            p.write(((int) (c[k3])));

        a(218, ((OutputStream) (p)));
        dataoutputstream.writeShort(j2);
        p.write(l1);
        for(int l3 = 0; l3 < l1; l3++)
        {
            p.write(l3);
            p.write(0);
        }

        p.write(1);
        p.write(0);
        p.write(0);
    }

    public byte[] encode8bitGrayscale(byte abyte0[], int i1, int j1)
        throws IOException
    {
        a(i1, j1, 8, 1);
        byte byte0 = (byte)(abyte0[0] - 128);
        b(((int) (byte0)));
        for(int k1 = 1; k1 < i1; k1++)
        {
            byte byte1 = (byte)(abyte0[k1] - abyte0[k1 - 1]);
            b(((int) (byte1)));
        }

        for(int l1 = 1; l1 < j1; l1++)
        {
            byte byte2 = (byte)(abyte0[i1 * l1] - abyte0[i1 * (l1 - 1)]);
            b(((int) (byte2)));
            for(int i2 = 1; i2 < i1; i2++)
            {
                byte byte3 = (byte)(abyte0[i1 * l1 + i2] - abyte0[(i1 * l1 + i2) - 1]);
                b(((int) (byte3)));
            }

        }

        a((short)127, 7);
        a(217, ((OutputStream) (p)));
        return p.toByteArray();
    }

    public byte[] encode16bitGrayscale(byte abyte0[], int i1, int j1)
        throws IOException
    {
        a(i1, j1, 16, 1);
        short word4 = (short)((abyte0[1] << 8) + (abyte0[0] & 0xff));
        short word0 = (short)(word4 - 32768);
        b(((int) (word0)));
        for(int k1 = 1; k1 < i1; k1++)
        {
            short word5 = (short)((abyte0[2 * k1 + 1] << 8) + (abyte0[2 * k1] & 0xff));
            short word8 = (short)((abyte0[2 * k1 - 1] << 8) + (abyte0[2 * k1 - 2] & 0xff));
            short word1 = (short)(word5 - word8);
            b(((int) (word1)));
        }

        for(int l1 = 1; l1 < j1; l1++)
        {
            short word6 = (short)((abyte0[2 * i1 * l1 + 1] << 8) + (abyte0[2 * i1 * l1] & 0xff));
            short word9 = (short)((abyte0[2 * i1 * (l1 - 1) + 1] << 8) + (abyte0[2 * i1 * (l1 - 1)] & 0xff));
            short word2 = (short)(word6 - word9);
            b(((int) (word2)));
            for(int i2 = 1; i2 < i1; i2++)
            {
                short word7 = (short)((abyte0[2 * (i1 * l1 + i2) + 1] << 8) + (abyte0[2 * (i1 * l1 + i2)] & 0xff));
                short word10 = (short)((abyte0[2 * (i1 * l1 + i2) - 1] << 8) + (abyte0[2 * (i1 * l1 + i2) - 2] & 0xff));
                short word3 = (short)(word7 - word10);
                b(((int) (word3)));
            }

        }

        a((short)127, 7);
        a(217, ((OutputStream) (p)));
        if((p.size() & 1) != 0)
            a(0);
        return p.toByteArray();
    }

    public byte[] encode16bitGrayscale(short aword0[], int i1, int j1)
        throws IOException
    {
        a(i1, j1, 16, 1);
        short word0 = (short)(aword0[0] - 32768);
        b(((int) (word0)));
        for(int k1 = 1; k1 < i1; k1++)
        {
            short word1 = (short)(aword0[k1] - aword0[k1 - 1]);
            b(((int) (word1)));
        }

        for(int l1 = 1; l1 < j1; l1++)
        {
            short word2 = (short)(aword0[i1 * l1] - aword0[i1 * (l1 - 1)]);
            b(((int) (word2)));
            for(int i2 = 1; i2 < i1; i2++)
            {
                short word3 = (short)(aword0[i1 * l1 + i2] - aword0[(i1 * l1 + i2) - 1]);
                b(((int) (word3)));
            }

        }

        a((short)127, 7);
        if((p.size() & 1) != 0)
            a(0);
        return p.toByteArray();
    }

    public byte[] encode24bitColor(byte abyte0[], byte abyte1[], byte abyte2[], int i1, int j1)
        throws IOException
    {
        a(i1, j1, 8, 3);
        byte byte0 = (byte)(abyte0[0] - 128);
        b(((int) (byte0)));
        byte0 = (byte)(abyte1[0] - 128);
        b(((int) (byte0)));
        byte0 = (byte)(abyte2[0] - 128);
        b(((int) (byte0)));
        for(int k1 = 1; k1 < i1; k1++)
        {
            byte byte1 = (byte)(abyte0[k1] - abyte0[k1 - 1]);
            b(((int) (byte1)));
            byte1 = (byte)(abyte1[k1] - abyte1[k1 - 1]);
            b(((int) (byte1)));
            byte1 = (byte)(abyte2[k1] - abyte2[k1 - 1]);
            b(((int) (byte1)));
        }

        for(int l1 = 1; l1 < j1; l1++)
        {
            byte byte2 = (byte)(abyte0[i1 * l1] - abyte0[i1 * (l1 - 1)]);
            b(((int) (byte2)));
            byte2 = (byte)(abyte1[i1 * l1] - abyte1[i1 * (l1 - 1)]);
            b(((int) (byte2)));
            byte2 = (byte)(abyte2[i1 * l1] - abyte2[i1 * (l1 - 1)]);
            b(((int) (byte2)));
            for(int i2 = 1; i2 < i1; i2++)
            {
                byte byte3 = (byte)(abyte0[i1 * l1 + i2] - abyte0[(i1 * l1 + i2) - 1]);
                b(((int) (byte3)));
                byte3 = (byte)(abyte1[i1 * l1 + i2] - abyte1[(i1 * l1 + i2) - 1]);
                b(((int) (byte3)));
                byte3 = (byte)(abyte2[i1 * l1 + i2] - abyte2[(i1 * l1 + i2) - 1]);
                b(((int) (byte3)));
            }

        }

        a((short)127, 7);
        a(217, ((OutputStream) (p)));
        if((p.size() & 1) != 0)
            a(0);
        return p.toByteArray();
    }

    static 
    {
        i = new int[257];
        j = new byte[257];
        k = new int[257];
        l = new byte[257];
        m = new int[256];
        for(int k1 = 0; k1 < 256; k1++)
        {
            int i1 = k1;
            int j1;
            for(j1 = 1; (i1 >>= 1) != 0; j1++);
            m[k1] = j1;
        }

        int l1 = 0;
        boolean flag = true;
        boolean flag1 = true;
        for(int i2 = 0; i2 < 16; i2++)
        {
            for(int j2 = 1; j2 <= b[i2]; j2++)
            {
                j[l1] = (byte)(i2 + 1);
                l1++;
            }

        }

        j[l1] = 0;
        int k2 = l1;
        int l2 = 0;
        int i3 = ((int) (j[0]));
        for(l1 = 0; j[l1] != 0;)
        {
            while(j[l1] == i3) 
            {
                i[l1] = l2;
                l1++;
                l2++;
            }
            l2 <<= 1;
            i3++;
        }

        l1 = 0;
        do
        {
            byte byte0 = c[l1];
            k[byte0] = i[l1];
            l[byte0] = j[l1];
        } while(++l1 < k2);
    }
}
