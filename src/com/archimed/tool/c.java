// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.tool;

import java.util.Enumeration;

// Referenced classes of package com.archimed.tool:
//            m, z

public class c
{

    private m a[];
    private int b;
    private int c;
    private float d;

    public c(int i, float f)
    {
        if(i <= 0 || (double)f <= 0.0D)
        {
            throw new IllegalArgumentException();
        } else
        {
            d = f;
            a = new m[i];
            c = (int)((float)i * f);
            return;
        }
    }

    public c(int i)
    {
        this(i, 0.75F);
    }

    public c()
    {
        this(101, 0.75F);
    }

    public int size()
    {
        return b;
    }

    public boolean isEmpty()
    {
        return b == 0;
    }

    public Enumeration keys()
    {
        return ((Enumeration) (new z(a, true)));
    }

    public Enumeration elements()
    {
        return ((Enumeration) (new z(a, false)));
    }

    public boolean contains(int i)
    {
        m am[] = a;
        for(int j = am.length; j-- > 0;)
        {
            for(m m1 = am[j]; m1 != null; m1 = m1.d)
                if(m1.c == i)
                    return true;

        }

        return false;
    }

    public boolean containsKey(int i)
    {
        m am[] = a;
        int j = i;
        int k = (j & 0x7fffffff) % am.length;
        for(m m1 = am[k]; m1 != null; m1 = m1.d)
            if(m1.a == j && m1.b == i)
                return true;

        return false;
    }

    public int get(int i)
    {
        m am[] = a;
        int j = i;
        int k = (j & 0x7fffffff) % am.length;
        for(m m1 = am[k]; m1 != null; m1 = m1.d)
            if(m1.a == j && m1.b == i)
                return m1.c;

        return 0x7fffffff;
    }

    protected void a()
    {
        int i = a.length;
        m am[] = a;
        int j = i * 2 + 1;
        m am1[] = new m[j];
        c = (int)((float)j * d);
        a = am1;
        for(int k = i; k-- > 0;)
        {
            for(m m1 = am[k]; m1 != null;)
            {
                m m2 = m1;
                m1 = m1.d;
                int l = (m2.a & 0x7fffffff) % j;
                m2.d = am1[l];
                am1[l] = m2;
            }

        }

    }

    public int put(int i, int j)
    {
        m am[] = a;
        int k = i;
        int l = (k & 0x7fffffff) % am.length;
        for(m m1 = am[l]; m1 != null; m1 = m1.d)
            if(m1.a == k && m1.b == i)
            {
                int i1 = m1.c;
                m1.c = j;
                return i1;
            }

        if(b >= c)
        {
            a();
            return put(i, j);
        } else
        {
            m m2 = new m();
            m2.a = k;
            m2.b = i;
            m2.c = j;
            m2.d = am[l];
            am[l] = m2;
            b++;
            return 0x7fffffff;
        }
    }

    public int remove(int i)
    {
        m am[] = a;
        int j = i;
        int k = (j & 0x7fffffff) % am.length;
        m m1 = am[k];
        m m2 = null;
        for(; m1 != null; m1 = m1.d)
        {
            if(m1.a == j && m1.b == i)
            {
                if(m2 != null)
                    m2.d = m1.d;
                else
                    am[k] = m1.d;
                b--;
                return m1.c;
            }
            m2 = m1;
        }

        return 0x7fffffff;
    }

    public synchronized void clear()
    {
        m am[] = a;
        for(int i = am.length; --i >= 0;)
            am[i] = null;

        b = 0;
    }
}
