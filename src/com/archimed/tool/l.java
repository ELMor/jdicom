// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.tool;

import java.util.Enumeration;
import java.util.NoSuchElementException;

// Referenced classes of package com.archimed.tool:
//            d

final class l
    implements Enumeration
{

    d a;
    int b;

    l(d d1)
    {
        a = d1;
        b = 0;
    }

    public boolean hasMoreElements()
    {
        return b < a.b;
    }

    public Object nextElement()
    {
        if(b < a.b)
            return a.a[b++];
        else
            throw new NoSuchElementException("ArrayListEnumerator");
    }
}
