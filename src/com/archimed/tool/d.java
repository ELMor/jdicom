// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   [DashoPro-V1.2-120198]

package com.archimed.tool;

import java.util.Enumeration;

// Referenced classes of package com.archimed.tool:
//            l

public class d
{

    protected Object a[];
    protected int b;

    public d(int i)
    {
        a = new Object[i];
    }

    public d()
    {
        this(10);
    }

    public d(Object aobj[], int i)
    {
        b = i;
        a = new Object[b];
        System.arraycopy(((Object) (aobj)), 0, ((Object) (a)), 0, b);
    }

    public d(Object aobj[])
    {
        this(aobj, aobj.length);
    }

    public void trimToSize()
    {
        int i = a.length;
        if(b < i)
        {
            Object aobj[] = a;
            a = new Object[b];
            System.arraycopy(((Object) (aobj)), 0, ((Object) (a)), 0, b);
        }
    }

    public void ensureCapacity(int i)
    {
        int j = a.length;
        if(i > j)
        {
            Object aobj[] = a;
            int k = (j * 3) / 2 + 1;
            if(k < i)
                k = i;
            a = new Object[k];
            System.arraycopy(((Object) (aobj)), 0, ((Object) (a)), 0, b);
        }
    }

    public int size()
    {
        return b;
    }

    public boolean isEmpty()
    {
        return b == 0;
    }

    public boolean contains(Object obj)
    {
        return indexOf(obj) >= 0;
    }

    public int indexOf(Object obj)
    {
        if(obj == null)
        {
            for(int i = 0; i < b; i++)
                if(a[i] == null)
                    return i;

        } else
        {
            for(int j = 0; j < b; j++)
                if(obj.equals(a[j]))
                    return j;

        }
        return -1;
    }

    public int lastIndexOf(Object obj)
    {
        if(obj == null)
        {
            for(int i = b - 1; i >= 0; i--)
                if(a[i] == null)
                    return i;

        } else
        {
            for(int j = b - 1; j >= 0; j--)
                if(obj.equals(a[j]))
                    return j;

        }
        return -1;
    }

    public Object[] toArray()
    {
        Object aobj[] = new Object[b];
        System.arraycopy(((Object) (a)), 0, ((Object) (aobj)), 0, b);
        return aobj;
    }

    public Object get(int i)
    {
        a(i);
        return a[i];
    }

    public Object set(int i, Object obj)
    {
        a(i);
        Object obj1 = a[i];
        a[i] = obj;
        return obj1;
    }

    public boolean add(Object obj)
    {
        ensureCapacity(b + 1);
        a[b++] = obj;
        return true;
    }

    public void add(int i, Object obj)
    {
        if(i > b || i < 0)
        {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + b);
        } else
        {
            ensureCapacity(b + 1);
            System.arraycopy(((Object) (a)), i, ((Object) (a)), i + 1, b - i);
            a[i] = obj;
            b++;
            return;
        }
    }

    public Object remove(int i)
    {
        a(i);
        Object obj = a[i];
        int j = b - i - 1;
        if(j > 0)
            System.arraycopy(((Object) (a)), i + 1, ((Object) (a)), i, j);
        a[--b] = null;
        return obj;
    }

    public void clear()
    {
        for(int i = 0; i < b; i++)
            a[i] = null;

        b = 0;
    }

    public Enumeration elements()
    {
        return ((Enumeration) (new l(this)));
    }

    private void a(int i)
    {
        if(i >= b || i < 0)
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + b);
        else
            return;
    }
}
